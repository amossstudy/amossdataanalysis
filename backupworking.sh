#!/bin/bash

type_filter_list=""
for a in "$@"
do
  type_filter_list+="$a|"
done

files=`ls -b 'working' | awk -v type_filter_list=\`echo $type_filter_list\` 'BEGIN {
    #FS = "/"
    #base_output_path = "extracted"
    #system("mkdir -p \"" base_output_path "\"")
    if (length(type_filter_list) > 0) {
      split(substr(type_filter_list, 1, length(type_filter_list)-1), type_filters, "|")
    } else {
      type_filters[1] = ""
    }
  }
  /\.mat$/{
    filename_base = $0
    gsub(/.mat$/, "", filename_base)
    for (x in type_filters) {
      type_filter = type_filters[x]
      if (length(type_filter) > 0) {
        if (match(filename_base, type_filter)) {
          print $0
        }
      } else {
        print $0
      }
    }
    #for (i = 1; i < ARGV; i++) 
    #  print ARGV[i]
    #}
  }' - `

output_path=backup-`date +%Y%m%d%H%M%S`

if [ -d "working/$output_path" ]; then
  echo "ERROR: Backup directory already exists."
else
  mkdir "working/$output_path"
  
  IFS=$'\n'
  for file in $files; do
    echo "Copying $file..."
    cp "working/$file" "working/$output_path/$file"
  done
fi
