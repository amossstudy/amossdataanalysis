#!/bin/bash

filter=*-annotation*.mat
outputname=annotations.zip

if [ ! -z "$1" ]; then
  filter=$1-annotation*.mat
  outputname=$1-annotations.zip
fi

find processed/*/$filter | zip -o $outputname -@
