<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title></title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="location-annotator.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/bootstrap.min.js"></script> -->

<?php
  if (isset($_GET["participant"])) {
    $participant = $_GET["participant"];
  } else {
    $participant = '';
  }
  $data_output_dir = getcwd();
  $data_output_dir = substr($data_output_dir, 0, strrpos($data_output_dir, '/') + 1);
  $data_output_dir = $data_output_dir . 'working/output';
  $location_annotator_dir = $data_output_dir . '/location/annotator';
  $features_raw_dir = $data_output_dir . '/location/v2/features-raw';

  $files = scandir($location_annotator_dir);

  class FileDetails {
    public $date;
    public $date_raw;
    public $date_type;
    public $is_combined;
    public $variant;
  }
  
  function compare_file_details($a, $b)
  {
    if (!is_null($a->date) && !is_null($b->date)) {
      $diff = $a->date->getTimestamp() - $b->date->getTimestamp();
    } elseif (is_null($a->date) && is_null($b->date)) {
      $diff = 0;
    } else {
      $diff = is_null($a->date) - is_null($b->date);
    }
    if ($diff == 0) {
      $diff = strcmp($a->date_type, $b->date_type);
    }
    if ($diff == 0) {
      $diff = $a->is_combined - $b->is_combined;
    }
    if ($diff == 0) {
      $diff = strcmp($a->variant, $b->variant);
    }
    return $diff;
  }
  
  function compare_file_details_row($a, $b)
  {
    if (!is_null($a->date) && !is_null($b->date)) {
      $diff = $a->date->getTimestamp() - $b->date->getTimestamp();
    } elseif (is_null($a->date) && is_null($b->date)) {
      $diff = 0;
    } else {
      $diff = is_null($a->date) - is_null($b->date);
    }
    if ($diff == 0) {
      $diff = strcmp($a->date_type, $b->date_type);
    }
    return $diff;
  }
  
  $unique_participants = array();
  $participant_files = array();
  $participant_file_details = array();
  $previous_participant = '';
  $next_participant = '';
  
  foreach ($files as $file) {
    if (strcmp(substr($file, 0, 1), '.') !== 0) {
      $this_participant = $file;
      if (strpos($this_participant, '_') !== FALSE) {
        $this_participant = substr($this_participant, 0, strpos($this_participant, '_'));
        
        if (strcmp($this_participant, end($unique_participants)) !== 0) {
          if (!empty($participant) && (strcmp($this_participant, $participant) < 0)) {
            $previous_participant = $this_participant;
          } elseif (!empty($participant) && empty($next_participant) && (strcmp($this_participant, $participant) > 0)) {
            $next_participant = $this_participant;
          }
          
          $unique_participants[] = $this_participant;
        }
        
        if (strcmp($this_participant, $participant) === 0) {
          $participant_files[] = $file;
          
          $this_file_details = new FileDetails();
          
          $file_details = split('[_\.]', substr($file, strpos($file, '_') + 1));
          
          $valid_file = TRUE;
          
          if (count($file_details) >= 2) {
            $i = 1;
            $date_raw_prefix = '';
            if (strcmp($file_details[$i], 'wb') === 0) {
              $date_raw_prefix = $file_details[$i] . '_';
              $this_file_details->date_type = 'week beginning';
              $i = 2;
            }
            if (preg_match('/^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$/', $file_details[$i])) {
              $this_file_details->date_raw = $date_raw_prefix . $file_details[$i];
              $this_file_details->date = date_create_from_format('Y-m-d', $file_details[$i]);
              
            } else {
              $valid_file = FALSE;
            }
          }
          $i++;
          if (($valid_file == TRUE) && (count($file_details) >= $i)) {
            $this_file_details->is_combined = FALSE;
            if (strcmp($file_details[$i], 'combined') === 0) {
              if (count($file_details) >= ($i + 2)) {
                $this_file_details->variant = $file_details[$i] . '_' . $file_details[$i + 1] . '_' . $file_details[$i + 2];
                $this_file_details->is_combined = TRUE;
              } else {
                $valid_file = FALSE;
              }
            } elseif (strcmp($file_details[$i], 'gms') === 0) {
              $this_file_details->variant = $file_details[$i];
            } else {
              $this_file_details->variant = '';
            }
            if ($valid_file == TRUE) {
              $participant_file_details[] = $this_file_details;
            }
          }
        }
      }
    }
  }
?>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="nav-item nav-item-prev">
        <?php if (!empty($previous_participant)) { echo '<a href="?participant=' . $previous_participant . '">'; } ?>
        <span class="glyphicon glyphicon-chevron-left"></span>
        <?php if (!empty($previous_participant)) { echo $previous_participant . '</a>'; } ?>
      </div>
      <div class="nav-item nav-item-next">
        <?php if (!empty($next_participant)) { echo '<a href="?participant=' . $next_participant . '">' . $next_participant; } ?>
        <span class="glyphicon glyphicon-chevron-right"></span>
        <?php if (!empty($next_participant)) { echo '</a>'; } ?>
      </div>
      <div class="nav-item nav-item-current">
        <?php echo $participant; ?>
      </div>
    </nav>
    <div class="participant-list">
      <hr />
<?php
  foreach ($unique_participants as $this_participant) {
    if (strcmp($this_participant, $participant) === 0) {
      echo '      <div class="participant-list-item participant-list-item-selected">' . $this_participant . '</div>' . "\n";
    } else {
      echo '      <a href="?participant=' . $this_participant . '"><div class="participant-list-item">' . $this_participant . '</div></a>' . "\n";
    }
  }
?>
    </div>
<?php
  if (count($participant_file_details) == 0) {
        echo '    <div class="data-list data-list-first">' . "\n";
        echo '      <hr />' . "\n"; 
        echo '      <div style="text-align: center; font-style: italic;" class="data-list-row">' . "\n";
        echo '        No data available' . "\n"; 
        echo '      </div>' . "\n";
        echo '    </div>' . "\n";
  }
  usort($participant_file_details, "compare_file_details");
  $last_file_details = null;
  foreach ($participant_file_details as $file) {
    if (is_null($last_file_details) || (compare_file_details($last_file_details, $file) !== 0)) {
      $filename_base_distance = $participant . '_distance_' . $file->date_raw;
      $filename_base_features = $participant . '_features_' . $file->date_raw;
      if (!empty($file->variant)) {
        $filename_base_distance = $filename_base_distance . '_' . $file->variant;
        $filename_base_features = $filename_base_features . '_' . $file->variant;
      }
      $filename_features = $features_raw_dir . '/' . $filename_base_features . '.txt';
      
      if (!is_null($last_file_details)) {
        echo '      </div>' . "\n";
        echo '    </div>' . "\n";
      }
      if (is_null($last_file_details) || (compare_file_details_row($last_file_details, $file) !== 0)) {
        echo '    <div class="data-list data-list-first">' . "\n";
        echo '      <hr />' . "\n"; 
        echo '      <div class="data-list-row data-list-row-first';
      } else {
        echo '    <div class="data-list">' . "\n";
        echo '      <div class="data-list-row';
      }
      if (file_exists($filename_features)) {
        echo ' data-list-row-valid';
      }
      echo '">' . "\n";;
      
      echo '        <div class="data-list-row-cell-title">' . "\n";
      echo '          <div class="data-list-row-cell-title-contents">';
      echo $file->date_type . '<br />' . date_format($file->date, 'd/m/Y') . '<br />' . $file->variant . "\n";
      if (file_exists($filename_features)) {
        $features = file_get_contents($filename_features);
        echo '            <hr />' . "\n";
        echo '            <pre>';
        echo $features;
        echo '</pre>' . "\n";
      }
      echo '          </div>' . "\n";
      echo '        </div>' . "\n";
      echo '        <div class="data-list-row-cell-figure">' . "\n";
      $this_file = $filename_base_distance . '_normalised.png';
      if (file_exists($location_annotator_dir . '/' . $this_file)) {
        echo '          <a href="output/location/annotator/' . $this_file . '" target="_blank"><img class="distance" src="output/location/annotator/' . $this_file . '" /></a>' . "\n";
      }
      echo '        </div>' . "\n";
      echo '        <div class="data-list-row-cell-figure">' . "\n";
      $this_file = $filename_base_distance . '_normalised_filtered_segmented.png';
      if (file_exists($location_annotator_dir . '/' . $this_file)) {
        echo '          <a href="output/location/annotator/' . $this_file . '" target="_blank"><img class="distance" src="output/location/annotator/' . $this_file . '" /></a>' . "\n";
      }
      echo '        </div>' . "\n";
      echo '        <div class="data-list-row-cell-figure">' . "\n";
      $this_file = $filename_base_distance . '_normalised_filtered_segmented-update.png';
      if (file_exists($location_annotator_dir . '/' . $this_file)) {
        echo '          <a href="output/location/annotator/' . $this_file . '" target="_blank"><img class="distance" src="output/location/annotator/' . $this_file . '" /></a>' . "\n";
      }
      echo '        </div>' . "\n";
      $last_file_details = $file;
    }
  }
  if (!is_null($last_file_details)) {
    echo '      </div>' . "\n";
    echo '      <hr />' . "\n" . '    </div>' . "\n";
  }
  
  if (count($participant_files) > 0) {
    echo '    <div class="container">' . "\n";
    echo '      <pre>';
    foreach ($participant_files as $file) {
      echo '<a href="output/location/annotator/' . $file . '" target="_blank">' . $file . '</a>' . "\n";
    }
    echo '</pre>' . "\n";
    echo '    </div>' . "\n";
  }
?>
  </body>
</html>
