#Introduction

This directory contains php scripts for viewing selected analysis output files through a web browser interface.

#Setup

In order to work correctly, this directory must be configured to be available through a web server such as apache. The simplest way to achieve this is to place a symlink in the standard `htdocs` folder, often in `/srv/htdocs/www`. Create a symlink using the following command:

    cd /srv/htdocs/www
    ln -s /path/to/this/directory/

Additionally, in order to serve the output files correctly, a symlink to the `working/output` directory should be created in this directory using the following command:

    cd /path/to/this/directory/
    ln -s ../working/output

#Optional configurations

This file is protected through the `.htaccess` configuration file. In order for this to take effect, the `AllowOverride` option for the directory should be configured in the `httpd.conf` configuration file as follows:

    AllowOverride All
