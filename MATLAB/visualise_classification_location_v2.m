function visualise_classification_location_v2(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 02-Oct-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/classification';
    
    params = get_configuration('features_location_v2', varargin{:});
    
    MAX_FEATURES = get_argument({'max_features', 'maxfeatures'}, NaN, varargin{:});
    
    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    
    label_opts = {};
    if latex_labels
        label_opts = {'Interpreter', 'latex'};
    end
    
    y_lim = get_argument({'ylim', 'y_lim'}, [0.5 1], varargin{:});
    
    classification_data = load_classification_data_location_v2(varargin{:});
    
    [classification_params, classification_results, ~, preds] = preprocess_classification_data_location_v2(classification_data);
    
    if isnan(MAX_FEATURES)
        features_to_show = classification_params.FEATURES;
    else
        features_to_show = MAX_FEATURES;
    end
    
    prediction_types_with_reps = {'loo_eq', 'ind_features', 'pca'};
    prediction_types_fs = {'loo_fs', 'cv_5f_fs', 'cv_10f_fs', 'cv_3f_fs', 'time_fs'};
    
    for type_cell = fieldnames(classification_results)'
        if strcmp(type_cell, 'meta')
            continue;
        end
        
        fprintf('Test: %s\n', type_cell{:});
        
        q_valid_idx = [ismember(classification_params.Q, classification_params.Q_TO_TEST) false(1, classification_params.Q_EXTRA_MAX)];
        for t = classification_params.TESTS_TO_PERFORM
            this_test_q_valid_idx = q_valid_idx & classification_results.meta.q_valid(t, :);
            if sum(this_test_q_valid_idx) > 0
                for stat_cell = {'Accuracy', 'Sensitivity', 'Specificity'}
                    stat_type = stat_cell{:};
                    stat_type_lc = lower(stat_type);

                    h_max = features_to_show;
                    
                    if strcmp(stat_type, 'Accuracy')
                        h_max = h_max + 1;
                    end
                    
                    h = zeros(h_max, 1);
                    str = cell(h_max, 1);
                    h_valid = false(h_max, 1);

                    figure
                    hold on;

                    if strcmp(type_cell{:}, 'ind_features')
                        clr = hsv(features_to_show);
                    else
                        clr = autumn(features_to_show);
                    end

                    for f = 1:features_to_show
                        if sum(~isnan(classification_results.(type_cell{:}).(stat_type_lc)(t, this_test_q_valid_idx, f))) > 0
                            h_valid(f) = true;
                            if isfield(classification_results.(type_cell{:}), [stat_type_lc '_std'])
                                h(f) = errorbar(classification_params.Q(this_test_q_valid_idx), classification_results.(type_cell{:}).(stat_type_lc)(t, this_test_q_valid_idx, f), classification_results.(type_cell{:}).([stat_type_lc '_std'])(t, this_test_q_valid_idx, f), classification_results.(type_cell{:}).([stat_type_lc '_std'])(t, this_test_q_valid_idx, f), 'Color', clr(f, :));
                            elseif sum(this_test_q_valid_idx) == 1
                                h(f) = scatter(classification_params.Q(this_test_q_valid_idx), classification_results.(type_cell{:}).(stat_type_lc)(t, this_test_q_valid_idx, f), 100, clr(f, :), 'x');
                            else
                                h(f) = plot(classification_params.Q(this_test_q_valid_idx), classification_results.(type_cell{:}).(stat_type_lc)(t, this_test_q_valid_idx, f), 'Color', clr(f, :));
                            end
                            if strcmp(type_cell, 'ind_features')
                                str{f} = params.feature_short{params.feature_order(f)};
                            elseif strcmp(type_cell, 'pca')
                                str{f} = sprintf('PC%i', f);
                            else
                                str{f} = sprintf('f%i', f);
                            end
                        end
                    end

                    if strcmp(stat_type, 'Accuracy')
                        h_valid(features_to_show + 1) = true;
                        if sum(this_test_q_valid_idx) == 1
                            h(features_to_show + 1) = scatter(classification_params.Q(this_test_q_valid_idx), classification_data.results.baseline(t, this_test_q_valid_idx), 100, lines(1), '+');
                        else
                            h(features_to_show + 1) = plot(classification_params.Q(this_test_q_valid_idx), classification_data.results.baseline(t, this_test_q_valid_idx), 'Color', lines(1));
                        end

                        str{features_to_show + 1} = 'b';
                    end
                    
                    legend(h(h_valid), str(h_valid), 'Location', 'EastOutside', label_opts{:});
                    set(gca, 'xlim', [classification_params.Q_MIN - 0.5, classification_params.Q_MAX + 0.5]);
                    set(gca, 'ylim', y_lim);

                    title(sprintf('%s (%s)', classification_results.(type_cell{:}).title, params.test_description{t}), label_opts{:});
                    xlabel('QIDS Score', label_opts{:});
                    ylabel(['Classification ' stat_type], label_opts{:});
                    hold off;

                    if latex_ticks
                        set(gca, 'TickLabelInterpreter', 'latex')
                    end

                    save_figure(sprintf('classification_%s_%s_%s', stat_type_lc, type_cell{:}, params.test_description_filename{t}), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
                end
            end
            
            if ismember(type_cell, [prediction_types_with_reps prediction_types_fs])
                q_all = [classification_params.Q, classification_params.Q_MAX + classification_params.Q_EXTRA_MAX];
                q_to_display = ismember(q_all, [6, 10, 11, classification_params.Q_MAX + classification_params.Q_EXTRA_TO_TEST]);
                for q = find(q_to_display & classification_results.meta.q_valid(t, :))
                    if q <= numel(classification_params.Q)
                        fprintf(' Q %s %i\n', char(8805), classification_params.Q(q));
                        
                        if latex_labels
                            this_q_desc = sprintf('QIDS $\\geq$ %i', classification_params.Q(q));
                        else
                            this_q_desc = sprintf('QIDS %s %i', char(8805), classification_params.Q(q));
                        end
                        this_q_filename_suffix = sprintf('qids_%.2i', classification_params.Q(q));
                    elseif q == (numel(classification_params.Q) + 1)
                        fprintf(' Cohort\n');
                        
                        this_q_desc = 'Cohort classification';
                        this_q_filename_suffix = 'cohort';
                    end
                    
                    classes_unique = classification_results.(type_cell{:}).classes_unique;
                    
                    if numel(classes_unique) == 2
                        boxes = 3;
                    elseif numel(classes_unique) > 2
                        boxes = numel(classes_unique) + 1;
                    end
                    
                    data_boxplot = NaN(classification_params.REPS, classification_params.FEATURES * (boxes + 1) + 1);
                    
                    if isfield(classification_results.(type_cell{:}), 'roc')
                        roc_points = classification_results.(type_cell{:}).roc.points;
                        data_roc = NaN(classification_params.REPS, classification_params.FEATURES, roc_points, 2);
                        data_auc = NaN(classification_params.REPS, classification_params.FEATURES);
                        data_auc_overall = NaN(classification_params.FEATURES, 1);
                    end
                    
                    max_valid_features = 0;

                    str = cell(classification_params.FEATURES, 1);
                    
                    clr_all = lines(7);
                    
                    for f = 1:min(classification_params.FEATURES, size(classification_results.(type_cell{:}).feature_rank, 3))
                        if numel(classes_unique) == 2
                            data_boxplot(:, (f - 1) * (boxes + 1) + 2) = classification_results.(type_cell{:}).accuracy_raw(t, q, f, :);
                            data_boxplot(:, (f - 1) * (boxes + 1) + 3) = classification_results.(type_cell{:}).sensitivity_raw(t, q, f, :);
                            data_boxplot(:, (f - 1) * (boxes + 1) + 4) = classification_results.(type_cell{:}).specificity_raw(t, q, f, :);
                        elseif numel(classes_unique) > 2
                            data_boxplot(:, (f - 1) * (boxes + 1) + 2) = classification_results.(type_cell{:}).accuracy_raw(t, q, f, :);
                            for c_i = 1:numel(classes_unique)
                                data_boxplot(:, (f - 1) * (boxes + 1) + 2 + c_i) = classification_results.(type_cell{:}).sensitivity_class_raw(t, q, f, :, c_i);
                            end
                        end
                        
                        if isfield(classification_results.(type_cell{:}), 'roc') && ...
                                ~isempty(classification_results.(type_cell{:}).roc.tpr{t, q, f}) && ...
                                ~isempty(classification_results.(type_cell{:}).roc.fpr{t, q, f})
                            data_roc(:, f, :, 1) = reshape(classification_results.(type_cell{:}).roc.tpr{t, q, f}', classification_params.REPS, 1, roc_points);
                            data_roc(:, f, :, 2) = reshape(classification_results.(type_cell{:}).roc.fpr{t, q, f}', classification_params.REPS, 1, roc_points);
                            
                            figure;
                            
                            for r = 1:(classification_params.REPS + 1)
                                if r <= classification_params.REPS
                                    tpr = squeeze(data_roc(r, f, :, 1));
                                    fpr = squeeze(data_roc(r, f, :, 2));
                                    
                                    line_clr = ones(1, 3) * 0.5;
                                    line_width = 1;
                                elseif r == classification_params.REPS + 1
                                    tpr = classification_results.(type_cell{:}).roc.tpr_all{t, q, f};
                                    fpr = classification_results.(type_cell{:}).roc.fpr_all{t, q, f};
                                    
                                    line_clr = clr_all(7, :);
                                    line_width = 2;
                                end
                                
                                plot(fpr, tpr, 'Color', line_clr, 'LineWidth', line_width);
                                
                                if r == 1
                                    hold on;
                                end
                                
                                auc = 0;
                                
                                for roc_i = 1:(numel(tpr) - 1)
                                    dx = fpr(roc_i) - fpr(roc_i + 1);
                                    dy = tpr(roc_i) - tpr(roc_i + 1);
                                    
                                    auc = auc + (dx * tpr(roc_i + 1)) + ((dx * dy) / 2);
                                end
                                
                                if r <= classification_params.REPS
                                    data_auc(r, f) = auc;
                                elseif r == classification_params.REPS + 1
                                    data_auc_overall(f) = auc;
                                end
                            end
                            hold off;
                            
                            if f == 1
                                title(sprintf('ROC (%i Feature, %s)', f, this_q_desc), label_opts{:});
                            else
                                title(sprintf('ROC (%i Features, %s)', f, this_q_desc), label_opts{:});
                            end
                            
                            xlabel('FPR', label_opts{:});
                            ylabel('TPR', label_opts{:});
                            
                            if latex_ticks
                                set(gca, 'TickLabelInterpreter', 'latex')
                            end

                            save_figure(sprintf('roc_%s_%s_%s_%.2i', this_q_filename_suffix, type_cell{:}, params.test_description_filename{t}, f), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', 30);
                        end
                        
                        if q <= numel(classification_params.Q)
                            figure;
                            
                            this_qids = preds.(type_cell{:}){t,q,f}.qids;
                            this_prob = mean(preds.(type_cell{:}){t,q,f}.pred_prob(:, :, 2), 2);
                            CP = sum(this_qids >= classification_params.Q(q));
                            CN = sum(this_qids < classification_params.Q(q));

                            idx = this_qids >= classification_params.Q(q) &  this_prob >= 0.5;
                            scatter(this_qids(idx), this_prob(idx), 100, 'x')
                            text(27 * 0.99, 0.5 + 1.2 * 0.02, sprintf('TP ($n=%i$; %.2f)', sum(idx), sum(idx) / CP), 'verticalalign', 'bottom', 'horizontalalign', 'right', 'fontsize', 18, label_opts{:});
                            hold on;
                            idx = this_qids >= classification_params.Q(q) &  this_prob < 0.5;
                            scatter(this_qids(idx), this_prob(idx), 100, 'x')
                            text(27 * 0.99, 0.5 - 1.2 * 0.02, sprintf('FN ($n=%i$; %.2f)', sum(idx), sum(idx) / CP), 'verticalalign', 'top', 'horizontalalign', 'right', 'fontsize', 18, label_opts{:});
                            idx = this_qids < classification_params.Q(q) &  this_prob >= 0.5;
                            scatter(this_qids(idx), this_prob(idx), 100, 'x')
                            text(27 * 0.01, 1.2 * 0.99 - 0.1, sprintf('FP ($n=%i$; %.2f)', sum(idx), sum(idx) / CN), 'verticalalign', 'top', 'horizontalalign', 'left', 'fontsize', 18, label_opts{:});
                            idx = this_qids < classification_params.Q(q) &  this_prob < 0.5;
                            scatter(this_qids(idx), this_prob(idx), 100, 'x')
                            text(27 * 0.01, 1.2 * 0.01 - 0.1, sprintf('TN ($n=%i$; %.2f)', sum(idx), sum(idx) / CN), 'verticalalign', 'bottom', 'horizontalalign', 'left', 'fontsize', 18, label_opts{:});
                            
                            plot([0, 27], [0.5, 0.5], '--', 'Color', [1 1 1] * 0.5, 'LineWidth', 2)
                            plot([1, 1] * classification_params.Q(q), [-0.1 1.1], '--', 'Color', [1 1 1] * 0.5, 'LineWidth', 2)
                            hold off;
                            
                            set(gca, 'xlim', [0, 27])
                            set(gca, 'ylim', [-0.1, 1.1])
                            set(gca, 'FontSize', 20)

                            if f == 1
                                title(sprintf('Probability of Being Depressed (%i Feature, %s)', f, this_q_desc), label_opts{:}, 'FontSize', 20);
                            else
                                title(sprintf('Probability of Being Depressed (%i Features, %s)', f, this_q_desc), label_opts{:}, 'FontSize', 20);
                            end

                            xlabel('QIDS Score', label_opts{:}, 'FontSize', 20);
                            ylabel(sprintf('Prob. QIDS $\\geq$ %i', classification_params.Q(q)), label_opts{:}, 'FontSize', 20);

                            if latex_ticks
                                set(gca, 'TickLabelInterpreter', 'latex')
                            end

                            save_figure(sprintf('prob_depressed_%s_%s_%s_%.2i', this_q_filename_suffix, type_cell{:}, params.test_description_filename{t}, f), ...
                                        'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize');
                        end

                        if strcmp(type_cell, 'ind_features')
                            str{f} = params.feature_short{params.feature_order(f)};
                        elseif strcmp(type_cell, 'pca')
                            str{f} = sprintf('PC%i', f);
                        else
                            if ~isnan(classification_results.(type_cell{:}).feature_rank(t, q, f))
                                str{f} = params.feature_short{classification_results.(type_cell{:}).feature_rank(t, q, f)};
                            end
                        end
                        
                        if sum(sum(~isnan(data_boxplot(:, (f - 1) * (boxes + 1) + (1:boxes) + 1)))) > 0
                            max_valid_features = f;
                        end
                        
                        fprintf('  Feature %i (%s)\n', f, str{f});
                        
                        if (numel(classes_unique) == 2)
                            stats_to_show = {'Accuracy', 'Sensitivity', 'Specificity', 'F1', 'AUC'};
                        else
                            stats_to_show = {'Accuracy', 'Sensitivity', 'F1'};
                        end
                        
                        for stat_cell = stats_to_show
                            if (numel(classes_unique) == 2)
                                fprintf('   %s:', stat_cell{:});
                            else
                                fprintf('   Overall %s:', stat_cell{:});
                            end
                            if strcmp(stat_cell{:}, 'AUC')
                                if isfield(classification_results.(type_cell{:}), 'roc')
                                    stat_data = data_auc(:, f);
                                else
                                    fprintf(' N/A\n');
                                    continue
                                end
                            else
                                stat_raw = sprintf('%s_raw', lower(stat_cell{:}));
                                stat_data = classification_results.(type_cell{:}).(stat_raw)(t, q, f, :);
                            end
                            fprintf(' mean %0.4f', mean(stat_data));
                            fprintf(' (sd %0.4f);', std(stat_data));
                            fprintf(' median %0.4f', median(stat_data));
                            fprintf(' (iqr %0.4f),', iqr(stat_data));
                            fprintf(' max %0.4f;', max(stat_data));
                            fprintf(' min %0.4f;', min(stat_data));
                            fprintf(' median%siqr: %0.3f\\pm%0.3f\n', char(177), median(stat_data), iqr(stat_data));
                            
                            if strcmp(stat_cell{:}, 'AUC')
                                fprintf('   %s Overall:', stat_cell{:});
                                if isfield(classification_results.(type_cell{:}), 'roc')
                                    fprintf(' %0.4f\n', data_auc_overall(f));
                                else
                                    fprintf(' N/A\n');
                                    continue
                                end
                            end
                            
                            if (numel(classes_unique) > 2) && ismember(stat_cell{:}, {'Sensitivity', 'F1'})
                                stat_raw = sprintf('%s_class_raw', lower(stat_cell{:}));
                                for c1 = 1:(numel(classes_unique))
                                    fprintf('     %s %s:', params.test_description{classes_unique(c1) + 1}, stat_cell{:});

                                    stat_data = classification_results.(type_cell{:}).(stat_raw)(t, q, f, :, c1);

                                    fprintf(' mean %0.4f', mean(stat_data));
                                    fprintf(' (sd %0.4f);', std(stat_data));
                                    fprintf(' median %0.4f', median(stat_data));
                                    fprintf(' (iqr %0.4f),', iqr(stat_data));
                                    fprintf(' max %0.4f;', max(stat_data));
                                    fprintf(' min %0.4f;', min(stat_data));
                                    fprintf(' median%siqr: %0.3f\\pm%0.3f\n', char(177), median(stat_data), iqr(stat_data));
                                end
                            end
                        end
                        
                        if isstruct(preds.(type_cell{:}){t, q, f})
                            classes_unique = sort(unique(preds.(type_cell{:}){t, q, f}.classes));
                            s_len = numel(num2str(numel(preds.(type_cell{:}){t, q, f}.pred)));
                        else
                            classes_unique = [];
                        end
                        
                        class_count = NaN(numel(classes_unique));
                        
                        s_desc_max_len = 0;
                        
                        for c1 = 1:(numel(classes_unique) + 1)
                            if c1 <= numel(classes_unique)
                                c_true = classes_unique(c1);
                                c_idx = preds.(type_cell{:}){t, q, f}.classes == c_true;
                            elseif (c1 == (numel(classes_unique) + 1))
                                c_true = NaN;
                                c_idx = ismember(preds.(type_cell{:}){t, q, f}.classes, classes_unique);
                            end
                            c_pred = preds.(type_cell{:}){t, q, f}.pred(c_idx, :);
                            
                            s_desc = NaN;
                            if q <= numel(classification_params.Q)
                                if c_true == 1
                                    s_desc = sprintf('Q < %i', classification_params.Q(q));
                                elseif c_true == 2
                                    s_desc = sprintf('Q %s %i', char(8805), classification_params.Q(q));
                                end
                            elseif c1 <= numel(classes_unique)
                                s_desc = params.test_description{c_true + 1};
                            end
                            
                            if ~isnan(s_desc)
                                s_desc_max_len = max(s_desc_max_len, numel(s_desc));
                                fprintf('   %s:\t', s_desc);
                            elseif isnan(c_true)
                                fprintf(repmat('\t', 1, ceil((s_desc_max_len + 4) / 4)));
                                fprintf(repmat(sprintf('  %s', repmat('-', 1, s_len)), 1, numel(classes_unique)));
                                fprintf(' |  %s  |', repmat('-', 1, s_len));
                                fprintf(repmat(sprintf('  %s', repmat('-', 1, 5)), 1, numel(classes_unique)));
                                fprintf('\n');
                                fprintf(repmat('\t', 1, ceil((s_desc_max_len + 4) / 4)));
                            end

                            for c2 = 1:numel(classes_unique)
                                class_count(c1, c2) = sum(sum(c_pred == classes_unique(c2)));
                                s = num2str(class_count(c1, c2));
                                fprintf('  %s%s', repmat(' ', 1, s_len - numel(s)), s);
                            end
                            
                            s = num2str(sum(class_count(c1, :)));
                            fprintf(' |  %s%s  |', repmat(' ', 1, s_len - numel(s)), s);
                            
                            for c2 = 1:numel(classes_unique)
                                fprintf('  %.3f', class_count(c1, c2) / sum(class_count(c1, :)));
                            end
                            
                            fprintf('\n');
                        end
                    end

                    if ~isnan(MAX_FEATURES)
                        max_valid_features = MAX_FEATURES;
                    end
                    
                    figure;

                    if numel(classes_unique) == 2
                        clr_boxplot = [0 0 0; clr_all([2 4 5], :)];
                        
                        str_legend = {'Accuracy', 'Sensitivity', 'Specificity'};
                    else
                        if t == 1
                            clr_boxplot = [0 0 0; params.clr_cohort(1:boxes, :)];

                            str_legend = [{'Overall Accuracy'} strcat(params.test_description(2:boxes), ' Sensitivity')];
                        else
                            clr_boxplot = [0 0 0; clr_all(1:boxes, :)];

                            str_legend = {'Overall Accuracy'};
                        end
                    end
                    
                    clr_cohort = clr_all([1 6 7 2], :);
                    
                    boxplot(data_boxplot(:, 1:(max_valid_features * (boxes + 1) + 1)), 'Colors', clr_boxplot);
                    
                    h = findall(gca,'Tag','Box');
                    h_legend = h((boxes + 1):-1:2);
                    
                    %hold on;
                    %h_baseline = plot([0.5 ((params.FEATURES * 4) + 1.5)], [0 0] + baseline(t, q), 'Color', lines(1));
                    %uistack(h_baseline, 'bottom');
                    %hold off;
                    
                    set(gca, 'ylim', y_lim);
                    set(gca, 'xtick', ((boxes / 2) + 1.5):(boxes + 1):(max_valid_features * (boxes + 1)));
                    
                    set(gca, 'xticklabel', str(1:max_valid_features), 'FontSize', 18);
                    
                    if max_valid_features <= 10
                        set(gca, 'XTickLabelRotation', 20)
                    else
                        set(gca, 'XTickLabelRotation', 35)
                    end
                    
                    title(sprintf('Classification Results (%s, %s)', params.test_description{t}, this_q_desc), 'FontSize', 30, label_opts{:});
                    
                    if strcmp(type_cell, 'ind_features')
                        xlabel('Feature', 'FontSize', 30, label_opts{:});
                    else
                        if strcmp(type_cell, 'pca')
                            xlabel('Principal Components Cumulatively Presented to Classifier', 'FontSize', 20, label_opts{:});
                        else
                            xlabel('Features Cumulatively Presented to Classifier', 'FontSize', 20, label_opts{:});
                        end
                        
                        hold on;
                        
                        median_ac = reshape(nanmedian(classification_results.(type_cell{:}).accuracy_raw(t, q, :, :), 4), classification_params.FEATURES, 1);
                        
                        h = plot(2:(boxes + 1):(max_valid_features * (boxes + 1)), median_ac(1:max_valid_features), 'Color', clr_boxplot(2, :), 'LineWidth', 1);
                        uistack(h, 'bottom');
                        
                        if numel(classes_unique) == 2
                            median_f1 = reshape(nanmedian(classification_results.(type_cell{:}).f1_raw(t, q, :, :), 4), classification_params.FEATURES, 1);
                            
                            h = plot(2:(boxes + 1):(max_valid_features * (boxes + 1)), median_f1(1:max_valid_features), 'Color', clr_boxplot(1, :), 'LineWidth', 1);
                            uistack(h, 'bottom');

                            h_legend = [h_legend; h]; %#ok<AGROW>
                            if latex_labels
                                str_legend = [str_legend {'F$_1$ Score'}]; %#ok<AGROW>
                            else
                                str_legend = [str_legend {['F' char(8321) ' Score']}]; %#ok<AGROW>
                            end
                        elseif numel(classes_unique) > 2
                            h_legend(1) = h;
                            for c1 = 1:numel(classes_unique)
                                median_ac = reshape(nanmedian(classification_results.(type_cell{:}).sensitivity_class_raw(t, q, :, :, c1), 4), classification_params.FEATURES, 1);

                                h = plot((c1 + 2):(boxes + 1):(max_valid_features * (boxes + 1)), median_ac(1:max_valid_features), '--', 'Color', clr_boxplot(c1 + 2, :), 'LineWidth', 1);
                                uistack(h, 'bottom');
                                
                                h_legend(c1 + 1) = h;
                            end
                        end
                        hold off;
                    end
                    
                    if nanmean(median_ac) > 0.7
                        legend(h_legend, str_legend, 'Location', 'South', 'Orientation', 'horizontal', 'FontSize', 18, label_opts{:})
                    else
                        legend(h_legend, str_legend, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 18, label_opts{:})
                    end
                    
                    if latex_ticks
                        set(gca, 'TickLabelInterpreter', 'latex')
                    end

                    p = get(gcf, 'PaperPosition');
                    p(3) = 2 * p(3);
                    set(gcf, 'PaperPosition', p);
                    
                    save_figure(sprintf('classification_results_%s_%s_%s', this_q_filename_suffix, type_cell{:}, params.test_description_filename{t}), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);
                    
                    if isfield(classification_results.meta, 'participant') && (numel(classes_unique) == 2)
                        test_participants = classification_results.meta.participant{t};
                        test_participants_cohort = classification_results.meta.participant_class_cohort{t};
                        
                        if t == 6
                            valid_idx = (test_participants_cohort == 1);
                        else
                            valid_idx = true(size(test_participants_cohort));
                        end
                        
                        test_participants_unique = unique(test_participants(valid_idx));
                        test_participants_employment_status = NaN(size(test_participants));

                        for p = test_participants_unique'
                            participant_employment_status = get_participant_property(p{:}, 'employment_status', varargin{:});

                            if isempty(participant_employment_status)
                                participant_employment_status = 0;
                            end

                            test_participants_employment_status(strcmp(test_participants, p)) = participant_employment_status;
                        end

                        test_participants_employment_status_unique = unique(test_participants_employment_status);
                        test_participants_employment_status_unique = test_participants_employment_status_unique(test_participants_employment_status_unique > 0);
                        clr_employment_status = parula(numel(params.employment_status_id));

                        employment_status_acc = NaN(numel(test_participants_employment_status_unique), classification_params.FEATURES, classification_params.REPS);
                        
                        for this_employment_status_i = 1:numel(test_participants_employment_status_unique)
                            this_employment_status = test_participants_employment_status_unique(this_employment_status_i);
                            for f = 1:min(classification_params.FEATURES, size(classification_results.(type_cell{:}).feature_rank, 3))
                                if strcmp(type_cell, 'ind_features')
                                    f_sel = params.feature_order(f);
                                elseif strcmp(type_cell, 'pca')
                                    f_sel = f;
                                else
                                    f_sel = classification_results.(type_cell{:}).feature_rank(t, q, f);
                                end
                                if ismember(type_cell, prediction_types_fs) && ~isnan(f_sel)
                                    if isstruct(classification_data.results.predictions.(type_cell{:}){t, q, f, f_sel})
                                        class_labels = classification_data.results.predictions.(type_cell{:}){t, q, f, f_sel}.class(test_participants_employment_status == this_employment_status);
                                        for r = 1:classification_params.REPS
                                            this_rep_pred = classification_data.results.predictions.(type_cell{:}){t, q, f, f_sel}.pred(test_participants_employment_status == this_employment_status, r);

                                            employment_status_acc(this_employment_status_i, f, r) = sum(class_labels == this_rep_pred) / numel(class_labels);
                                        end
                                    end
                                elseif ~isnan(f_sel)
                                    if isstruct(classification_data.results.predictions.(type_cell{:}){t, q, f_sel})
                                        class_labels = classification_data.results.predictions.(type_cell{:}){t, q, f_sel}.class(test_participants_employment_status == this_employment_status);
                                        for r = 1:classification_params.REPS
                                            this_rep_pred = classification_data.results.predictions.(type_cell{:}){t, q, f_sel}.pred(test_participants_employment_status == this_employment_status, r);

                                            employment_status_acc(this_employment_status_i, f, r) = sum(class_labels == this_rep_pred) / numel(class_labels);
                                        end
                                    end
                                end
                            end
                        end
                        
                        hold on
                        for this_employment_status = [1 3]
                            this_employment_status_i = test_participants_employment_status_unique == this_employment_status;
                            
                            h = plot(2:4:(max_valid_features * 4), median(reshape(employment_status_acc(this_employment_status_i, 1:max_valid_features, :), max_valid_features, classification_params.REPS), 2), 'x--', 'color', clr_employment_status(this_employment_status_i, :), 'LineWidth', 1);

                            this_employment_status_description = params.employment_status_description{params.employment_status_id == this_employment_status};
                            
                            h_legend = [h_legend; h]; %#ok<AGROW>
                            str_legend = [str_legend {this_employment_status_description}]; %#ok<AGROW>
                        end
                        hold off;
                        
                        if nanmean(median_ac) > 0.7
                            legend(h_legend, str_legend, 'Location', 'South', 'Orientation', 'horizontal', 'FontSize', 18, label_opts{:})
                        else
                            legend(h_legend, str_legend, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 18, label_opts{:})
                        end

                        if latex_ticks
                            set(gca, 'TickLabelInterpreter', 'latex')
                        end

                        save_figure(sprintf('classification_results_%s_%s_%s_with_employment_status_combined', this_q_filename_suffix, type_cell{:}, params.test_description_filename{t}), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);

                        figure;
                        
                        h_legend = cell(numel(test_participants_employment_status_unique) + 1, 1);
                        str_legend = cell(numel(test_participants_employment_status_unique) + 1, 1);
                        
                        h_legend{1} = plot(1:max_valid_features, median_ac(1:max_valid_features), 'Color', clr_boxplot(2, :), 'LineWidth', 2);

                        if numel(classification_results.meta.classes{t}) == 1
                            str_legend{1} = sprintf('All %s', params.test_description{t});
                        elseif t == 6
                            str_legend{1} = sprintf('All %s', params.test_description{3});
                        else
                            str_legend{1} = params.test_description{t};
                        end

                        hold on
                        for this_employment_status_i = 1:numel(test_participants_employment_status_unique)
                            this_employment_status = test_participants_employment_status_unique(this_employment_status_i);
                            
                            h_legend{this_employment_status_i + 1} = plot(1:max_valid_features, median(reshape(employment_status_acc(this_employment_status_i, 1:max_valid_features, :), max_valid_features, classification_params.REPS), 2), 'color', clr_employment_status(this_employment_status_i, :), 'LineWidth', 1);

                            str_legend{this_employment_status_i + 1} = params.employment_status_description{params.employment_status_id == this_employment_status};
                            
                            if latex_labels
                                str_legend{this_employment_status_i + 1} = strrep(str_legend{this_employment_status_i + 1}, '&', '\&');
                            end
                        end
                        hold off;

                        if nanmean(median_ac) > 0.7
                            legend([h_legend{:}], str_legend, 'Location', 'South', 'Orientation', 'horizontal', 'FontSize', 18, label_opts{:})
                        else
                            legend([h_legend{:}], str_legend, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 18, label_opts{:})
                        end
                        
                        set(gca, 'ylim', y_lim);
                        set(gca, 'xlim', [0.5, max_valid_features + 0.5]);
                        set(gca, 'xtick', 1:max_valid_features);

                        set(gca, 'xticklabel', str(1:max_valid_features), 'FontSize', 18);

                        if max_valid_features <= 10
                            set(gca, 'XTickLabelRotation', 20)
                        else
                            set(gca, 'XTickLabelRotation', 35)
                        end

                        title(sprintf('Classification Results (%s, %s)', params.test_description{t}, this_q_desc), 'FontSize', 30, label_opts{:});

                        xlabel('Features Cumulatively Presented to Classifier', 'FontSize', 20, label_opts{:});
                        ylabel('Classification Accuracy', 'FontSize', 20, label_opts{:});

                        if latex_ticks
                            set(gca, 'TickLabelInterpreter', 'latex')
                        end

                        p = get(gcf, 'PaperPosition');
                        p(3) = 2 * p(3);
                        set(gcf, 'PaperPosition', p);

                        save_figure(sprintf('classification_results_%s_%s_%s_with_employment_status', this_q_filename_suffix, type_cell{:}, params.test_description_filename{t}), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);
                    end
                    
                    if (numel(classification_results.meta.classes{t}) > 1) && (q <= numel(classification_params.Q))
                        hold on
                        for this_class = classification_results.meta.classes{t}'
                            if (t == 6) && (this_class == 1)
                                continue;
                            end
                            this_class_acc = NaN(classification_params.FEATURES, classification_params.REPS);
                            for f = 1:min(classification_params.FEATURES, size(classification_results.(type_cell{:}).feature_rank, 3))
                                if strcmp(type_cell, 'ind_features')
                                    f_sel = params.feature_order(f);
                                elseif strcmp(type_cell, 'pca')
                                    f_sel = f;
                                else
                                    f_sel = classification_results.(type_cell{:}).feature_rank(t, q, f);
                                end
                                if ismember(type_cell, prediction_types_fs) && ~isnan(f_sel)
                                    if isstruct(classification_data.results.predictions.(type_cell{:}){t, q, f, f_sel})
                                        class_labels = classification_data.results.predictions.(type_cell{:}){t, q, f, f_sel}.class(classification_results.meta.participant_class_cohort{t} == this_class);
                                        for r = 1:classification_params.REPS
                                            this_rep_pred = classification_data.results.predictions.(type_cell{:}){t, q, f, f_sel}.pred(classification_results.meta.participant_class_cohort{t} == this_class, r);

                                            this_class_acc(f, r) = sum(class_labels == this_rep_pred) / numel(class_labels);
                                        end
                                    end
                                elseif ~isnan(f_sel)
                                    if isstruct(classification_data.results.predictions.(type_cell{:}){t, q, f_sel})
                                        class_labels = classification_data.results.predictions.(type_cell{:}){t, q, f_sel}.class(classification_results.meta.participant_class_cohort{t} == this_class);
                                        for r = 1:classification_params.REPS
                                            this_rep_pred = classification_data.results.predictions.(type_cell{:}){t, q, f_sel}.pred(classification_results.meta.participant_class_cohort{t} == this_class, r);

                                            this_class_acc(f, r) = sum(class_labels == this_rep_pred) / numel(class_labels);
                                        end
                                    end
                                end
                            end
                            
                            %h = plot(2:4:(classification_params.FEATURES * 4), median(this_class_acc, 2), '--', 'color', clr_cohort(this_class + 1, :), 'LineWidth', 1);
                            h = plot(1:max_valid_features, median(this_class_acc(1:max_valid_features, :), 2), '--', 'color', clr_cohort(this_class + 1, :), 'LineWidth', 1);
                            
                            h_legend = [h_legend; {h}]; %#ok<AGROW>
                            str_legend = [str_legend; {sprintf('%s', params.test_description{this_class + 2})}]; %#ok<AGROW>
                        end
                        hold off;
                        
                        if nanmean(median_ac) > 0.7
                            legend([h_legend{:}], str_legend, 'Location', 'South', 'Orientation', 'horizontal', 'FontSize', 18, label_opts{:})
                        else
                            legend([h_legend{:}], str_legend, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 18, label_opts{:})
                        end
                        
                        if latex_ticks
                            set(gca, 'TickLabelInterpreter', 'latex')
                        end

                        save_figure(sprintf('classification_results_%s_%s_%s_with_employment_status_cohorts', this_q_filename_suffix, type_cell{:}, params.test_description_filename{t}), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);
                    end
                end
            end
        end
    end
end
