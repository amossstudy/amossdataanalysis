function [classification_data, classification_class, classification_properties] = load_features_location_episode(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 01-May-2015

    classification_data = [];
    classification_class = [];
    classification_properties = [];
    
    for file = get_participants(varargin{:})'
        features = load_location_features(file.name, varargin{:});

        if ~isempty(fieldnames(features)) && isfield(features, 'episodes')
            valid_records = features.episodes.is_valid;
            this_class = features.episodes.class(valid_records)';
            this_data = features.episodes.basic(:, valid_records);

            features_mse = NaN(3, sum(valid_records));
            
            if isfield(features.episodes, 'mse_is_valid') && sum(features.episodes.mse_is_valid) > 0
                features_mse(:, features.episodes.mse_is_valid) = features.episodes.mse(:, features.episodes.mse_is_valid);
            end
            
            this_data = [this_data; features_mse]; %#ok<AGROW>
            
            features_dfa = NaN(3, sum(valid_records));
            
            if isfield(features.episodes, 'dfa_is_valid') && sum(features.episodes.dfa_is_valid) > 0
                features_dfa(:, features.episodes.dfa_is_valid) = features.episodes.dfa(:, features.episodes.dfa_is_valid);
            end
            
            this_data = [this_data; features_dfa]; %#ok<AGROW>
            
            classification_class = [classification_class; this_class]; %#ok<AGROW>

            classification_data = [classification_data this_data]; %#ok<AGROW>
            
            for ep_id = find(valid_records)'
                this_epoch_properties = struct();
                this_epoch_properties.participant = file.name;
                this_epoch_properties.start = features.episodes.start(ep_id);
                this_epoch_properties.end = features.episodes.end(ep_id);

                classification_properties = [classification_properties, this_epoch_properties]; %#ok<AGROW>
            end
        end
    end
    
    valid_data = ~isnan(classification_class)';
    classification_data = classification_data(:, valid_data);
    %classification_data(isnan(classification_data)) = 0;
    classification_class = classification_class(valid_data);
end
