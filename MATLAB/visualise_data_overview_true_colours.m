function visualise_data_overview_true_colours(participant, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 24-Mar-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-overview/true-colours';
    
    data = load_true_colours_data(participant, varargin{:});
    
    questionaires = {'ALTMAN', 'GAD-7', 'QIDS', 'EQ-5D'};
    
    t_min = inf;
    t_max = 0;
    
    h_plot = zeros(numel(questionaires), 1);
    
    %%
    if ~isempty(data)
        for q = 1:numel(questionaires)
            this_questionaire_str = questionaires{q};
            this_questionaire = strrep(this_questionaire_str, '-', '_');
            
            this_questionnaire_data = data.(this_questionaire).data;
            this_questionnaire_time = data.(this_questionaire).time;
            this_questionnaire_time = this_questionnaire_time(this_questionnaire_data > -1);
            this_questionnaire_data = this_questionnaire_data(this_questionnaire_data > -1);
            this_questionnaire_time_diff = [1; diff(this_questionnaire_time)];
            this_questionnaire_time = this_questionnaire_time(this_questionnaire_time_diff > (1/24));
            this_questionnaire_data = this_questionnaire_data(this_questionnaire_time_diff > (1/24));
            
            t_min = min(t_min, floor(min(this_questionnaire_time)));
            t_max = max(t_max, ceil(max(this_questionnaire_time)));
            
            if strcmp(this_questionaire, 'ALTMAN') == 1
                q_max = 20;
            elseif strcmp(this_questionaire, 'EQ_5D') == 1
                q_max = 100;
            elseif strcmp(this_questionaire, 'GAD_7') == 1
                q_max = 21;
            elseif strcmp(this_questionaire, 'QIDS') == 1
                q_max = 27;
            end
            
            h_plot(q) = subplot(numel(questionaires), 1, q);
            
            stairs(this_questionnaire_time, this_questionnaire_data);
            hold on;
            scatter(this_questionnaire_time, this_questionnaire_data);
            hold off;
            
            ylim([0 q_max]);
            
            ylabel(this_questionaire_str);
            
            if q == 1;
                title('True Colours');
            end
            
            if q < numel(questionaires)
                set(gca, 'xticklabel', []);
            else
                [xticks, xticks_str] = get_xticks(t_min, t_max, varargin{:});
                
                set(gca, 'xticklabel', xticks_str);
                
                for h = h_plot'
                    set(h, 'xtick', xticks);
                    xlim(h, [t_min, t_max]);
                end
            end
            
            p = get(gca, 'Position');
            p_diff = p(4) * 0.2;
            p(4) = p(4) + p_diff;
            p(2) = p(2) - ((3 - (q - 1)) * (p_diff / 3));
            set(gca, 'Position', p);
        end
        
        save_figure([participant '-true-colours'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    else
        fprintf('  No data loaded\n');
    end
end