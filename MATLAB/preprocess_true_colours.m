function preprocess_true_colours(varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 28-Dec-2014

    for file = get_participants(varargin{:})'
        fprintf('Preprocessing %s\n', file.name)

        data = load_true_colours_data(file.name, varargin{:});

        if ~isempty(fieldnames(data))
            questionaires = {'ALTMAN', 'EQ_5D', 'GAD_7', 'QIDS'};
            for this_questionaire_cell = questionaires
                this_questionaire = this_questionaire_cell{1};
                if isfield(data, this_questionaire)
                    data_days = floor(data.(this_questionaire).time);

                    regularised_time = (min(data_days) - 6):max(data_days);
                    regularised_data = NaN(size(regularised_time));

                    for t = length(data.(this_questionaire).time):-1:1
                        if data.(this_questionaire).data(t) > -1
                            regularised_data(ismember(regularised_time, (data_days(t)-6):data_days(t))) = data.(this_questionaire).data(t);
                        end
                    end

                    data.(this_questionaire).regularised.time = regularised_time;
                    data.(this_questionaire).regularised.data = regularised_data;

                    data_weeks_start = regularised_time(1);
                    data_weeks_start = data_weeks_start - weekday(data_weeks_start) + 2; % Start on the previous Monday
                    data_weeks_end = regularised_time(end);
                    weekly_time = ((0:floor((data_weeks_end - data_weeks_start) / 7)) * 7) + data_weeks_start;
                    weekly_data = NaN(size(weekly_time));

                    for w = weekly_time
                        week_data = regularised_data(ismember(regularised_time, w + (0:6)));
                        if sum(~isnan(week_data)) >= 4
                            weekly_data(weekly_time == w) = mode(week_data);
                        end
                    end
                end

                data.(this_questionaire).weekly.time = weekly_time;
                data.(this_questionaire).weekly.data = weekly_data;
            end
        else
            fprintf('  No data loaded\n');
        end

        save_data([file.name '-true-colours-preprocessed'], data, varargin{:});
    end
end