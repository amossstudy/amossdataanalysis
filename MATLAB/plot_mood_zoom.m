function h_out = plot_mood_zoom(data, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    t_min = get_argument({'tmin', 't_min'}, floor(data.time(1)), varargin{:});
    t_max = get_argument({'tmax', 't_max'}, ceil(data.time(end)), varargin{:});
    h_in = get_argument({'happend', 'h_append'}, [], varargin{:});
    
    [xticks, xticks_str] = get_xticks(t_min, t_max, varargin{:});
    
    if isempty(h_in)
        figure
    end
    
    a = zeros(1, 4);
    a(1) = t_min;
    a(2) = t_max;
    a(3) = 0.5;
    a(4) = 7.5;
    
    fields = flipud(fieldnames(data));
    fields = fields(~strcmp(fields, 'time'));
    
    clr = lines(numel(fields));
    
    h_out = cell(numel(fields), 1);
    
    for i = 1:numel(fields)
        if isempty(h_in) || ~isobject(h_in(i))
            h_out{i} = subplot(numel(fields), 1, i);
        else
            h_out{i} = subplot(h_in(i));
            hold on;
        end
        stairs(data.time, data.(fields{i}), 'LineWidth', 2, 'color', clr(i, :));
        hold on;
        scatter(data.time, data.(fields{i}), 750, 'LineWidth', 2, 'Marker', 'x', 'MarkerEdgeColor', clr(i, :));
        hold off;
        if isempty(h_in)
            axis(a);
            set(gca,'YTick',[1, 7])
            set(gca,'YTickLabel',[])
            set(gca,'XTick',xticks)
            set(gca,'XTickLabel',[])
            set(gca,'FontSize', 20)
            ylabel_text = regexprep(regexp(fields{i}, '_', 'split')','(\<\w)','${upper($1)}');
            hy = ylabel(ylabel_text, 'Rotation', 0, 'FontSize', 30);
            set(hy, 'Units', 'Normalized');
            pos = get(hy, 'Position');
            set(hy, 'Position', pos + [-0.055, -0.15 * numel(ylabel_text), 0]);
            p = get(h_out{i}, 'pos');
            p(3) = 0.7450;
            p(4) = p(4) + (0.2 / numel(fields));
            set(h_out{i}, 'pos', p);
            if i == 1
                title('Mood Levels', 'FontSize', 30);
            end
        end
    end
    
    if isempty(h_in)
        set(gca,'XTickLabel',xticks_str)
        set(gca,'FontSize', 30)

        p = get(gcf, 'Position');
        p(3) = 3 * p(3);
        p(4) = 2 * p(4);
        set(gcf, 'Position', p);

        p = get(gcf, 'PaperPosition');
        p(3) = 3 * p(3);
        p(4) = 2 * p(4);
        set(gcf, 'PaperPosition', p);
    end
    
    h_out = [h_out{:}]';
end