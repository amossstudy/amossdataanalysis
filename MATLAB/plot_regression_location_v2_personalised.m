function [h] = plot_regression_location_v2_personalised(x, features, qids, pred, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 29-Apr-2016

    params = get_configuration('features_location_v2', varargin{:});

    FEATURES = size(features, 2);

    clr = lines(FEATURES);

    feature_corr = NaN(FEATURES, 1);
    
    h = cell(2, 1);

    h{1} = subplot(4, 1, 1:3);
    for i = 1:FEATURES
        qids_data_valid = ~isnan(qids) & ~isnan(features(:, i));

        if sum(qids_data_valid) > 0
            feature_corr(i) = corr(qids(qids_data_valid), features(qids_data_valid, i));
        end
    end

    [~, feature_corr_sort_idx] = sort(abs(feature_corr), 'descend');

    features_best_variant = NaN(1, 5);

    for i = 1:10
        features_best_variant(i) = feature_corr_sort_idx(find(~isnan(feature_corr_sort_idx), 1, 'first'));

        feature_corr_sort_idx(ismember(feature_corr_sort_idx, rem(features_best_variant(i), params.features) + (0:params.features:max(feature_corr_sort_idx)))) = NaN;
    end

    features_to_plot = features_best_variant(1:5);

    h_feature = cell(size(features_to_plot));
    text_feature = params.feature_short(features_to_plot);

    for i = features_to_plot
        scatter(x, features(:, i), 50, clr(i, :))
        if i == features_to_plot(1)
            hold on;
        end
        h_feature{features_to_plot == i} = plot(x, features(:, i), 'Color', clr(i, :));
        plot_missing_data(x, features(:, i), ':', 'Color', clr(i, :));
    end
    hold off;
    
    legend([h_feature{:}], text_feature, 'location', 'NorthOutside', 'orientation', 'horizontal');
    
    set(gca, 'xlim', x([1, end]) + [-1 1])

    title(' ')
    
    xticks_lim_vec = datevec(x([1 end]));
    xticks_lim_vec(:, 3) = 1;
    xticks_lim_vec(2, 2) = xticks_lim_vec(2, 2) + 1;
    xticks_lim = datenum(xticks_lim_vec);

    [xticks, xticks_str] = get_xticks(xticks_lim(1), xticks_lim(2), varargin{:});

    set(gca, 'XTick', xticks)
    set(gca, 'XTickLabel', [])

    p = get(gca, 'Position');
    p_diff = p(4) * (0.1 / 3);
    p(4) = p(4) + p_diff;
    p(2) = p(2) - p_diff;
    set(gca, 'Position', p);

    h{2} = subplot(4, 1, 4);
    scatter(x, qids, 50, lines(1));
    hold on;
    plot(x, qids, 'Color', lines(1));
    plot_missing_data(x, qids, ':', 'Color', lines(1));

    scatter(x, pred, 50, 'x', 'MarkerEdgeColor', [1 0 0])
    plot(x, pred, 'Color', [1 0 0]);
    plot_missing_data(x, pred', ':', 'Color', [1 0 0]);

    plot(x([1, end]) + [-1 1], [11 11], 'Color', ones(3, 1) / 2);
    hold off;

    set(gca, 'xlim', x([1, end]) + [-1 1])

    set(gca, 'XTick', xticks)
    set(gca, 'XTickLabel', xticks_str)

    set(gca, 'ylim', [0 30])

    p = get(gca, 'Position');
    p_diff = p(4) * 0.1;
    p(4) = p(4) + p_diff;
    set(gca, 'Position', p);
end