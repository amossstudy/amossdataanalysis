function data = load_mood_zoom_records(record_cell)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 25-Jan-2016

    data = struct();
    
    if size(record_cell{1}, 1) == 0
        data.data = [];
        data.time = [];
    else
        t = datenum(record_cell{1}, 'yyyy-mm-dd HH:MM:SS.FFF');

        %%%%%
        % This adjusts the date as per the time zone based on 
        % https://github.com/maximosipov/actant/blob/master/formats/localtime.m
        % I don't think that this is actually required, but left just in case.
        %zh_num = tmp{2}/(24);
        %zm_num = tmp{3}/(24*60) .* sign(zh_num);
        %
        %t = t - zh_num - zm_num;
        %%%%%

        % Although this is a larage data set, it is almost sorted with only a
        % few exceptions so this doesn't add much computation.
        [t, t_sort_order] = sort(t);

        fields = (length(record_cell) - 3) / 2;
        
        has_valid = false(size(t));
        
        for i = 4:(fields + 3)
            recordnames = lower(strrep(record_cell{i + fields}, ' ', '_'));
            recordnames_unique = unique(recordnames);
            
            for this_recordnames_cell = recordnames_unique'
                if (double(this_recordnames_cell{:}(1)) > 57) || (double(this_recordnames_cell{:}(1)) < 49)
                    if length(recordnames_unique) == 1
                        data.(this_recordnames_cell{:}) = record_cell{i};
                        has_valid(:) = true;
                    else
                        data.(this_recordnames_cell{:}) = NaN(size(has_valid));
                        field_idx = strcmp(recordnames, this_recordnames_cell);
                        data.(this_recordnames_cell{:})(field_idx) = record_cell{i}(field_idx);
                        has_valid(field_idx) = true;
                    end
                    data.(this_recordnames_cell{:}) = data.(this_recordnames_cell{:})(t_sort_order);
                end
            end
        end
        
        if sum(~has_valid) == 0
            data.time = t;
        elseif sum(has_valid) == 0
            data.data = [];
            data.time = [];
        else
            for this_fieldname_cell = fieldnames(data)'
                data.(this_fieldname_cell{:}) = data.(this_fieldname_cell{:})(has_valid(t_sort_order));
            end
            data.time = t(has_valid(t_sort_order));
        end
    end
end
