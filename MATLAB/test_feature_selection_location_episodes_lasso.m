function [feature_rank, best_result] = test_feature_selection_location_episodes_lasso(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 07-May-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location-episode-feature-selection';
    
    show_graph = get_argument({'showgraph', 'show_graph'}, true, varargin{:});
    
    [data_raw, class_3c, class_properties] = load_features_location_episodes(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    EXCLUDE_FEATURES = [15, 16];
    
    features_all = 1:size(data_raw, 1);
    
    included_features_idx = ~ismember(features_all, EXCLUDE_FEATURES);
    included_features_num = features_all(included_features_idx);
    
    data_raw = data_raw(included_features_idx, :);
    
    data_processed = double(data_raw);
    
    for i = 1:size(data_processed, 1)
        if sum(isnan(data_processed(i, :))) > 0
            data_processed(i, isnan(data_processed(i, :))) = nanmean(data_processed(i, :));
        end
        % optionally zero mean and scale each column
        %data_processed(i, :) = data_processed(i, :) - mean(data_processed(i, :));
        %data_processed(i, :) = data_processed(i, :) / std(data_processed(i, :));
    end
    
    data_full = data_raw';
    
    class_2c = ismember(class_3c, [1, 2, 3]);
    
    FEATURES = size(data_full, 2);
    
    feature_rank = NaN(FEATURES, 1);
    
    fprintf('Testing feature selection using LASSO\n');
    
    B = lasso(data_full, class_2c);
    
    features_count = sum(B ~= 0, 1);
    
    for i = 1:FEATURES
        features_used = find(B(:, find(features_count == i, 1, 'last')) ~= 0);
        new_features_used = features_used(~ismember(features_used, feature_rank));
        
        if ~isempty(new_features_used)
            feature_rank(i - length(new_features_used) + 1:i) = new_features_used;
        end
    end
    
    if (show_graph == true)
        data_sorted = data_processed(feature_rank, :)';
        
        best_result = test_feature_selection(data_sorted, class_2c, 'LASSO', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        best_result.properties = class_properties;
    end
    
    feature_rank = included_features_num(feature_rank);
end

