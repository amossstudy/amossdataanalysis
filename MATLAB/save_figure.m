function save_figure(filename, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 27-Jan-2015

    paths = get_paths(varargin{:});
    
    output_path = paths.output_path;
    
    font_size = get_argument({'fontsize', 'font_size'}, 16, varargin{:});
    filename_suffix = get_argument({'filenamesuffix', 'filename_suffix', 'filesuffix', 'file_suffix'}, '', varargin{:});
    save_figure = get_argument({'savefigure', 'save_figure', 'savefigures', 'save_figures'}, true, varargin{:});
    save_eps = get_argument({'saveeps', 'save_eps'}, true, varargin{:});
    save_fig = get_argument({'savefig', 'save_fig'}, true, varargin{:});
    save_png = get_argument({'savepng', 'save_png'}, true, varargin{:});
    save_pdf = get_argument({'savepdf', 'save_pdf'}, false, varargin{:});
    figure_handle = get_argument({'figurehandle', 'figure_handle'}, gcf, varargin{:});
    remove_title_opt = get_argument({'removetitle', 'remove_title'}, true, varargin{:});
    set_painters = get_argument({'setpainters', 'set_painters'}, false, varargin{:});
    
    if set_painters
        set(figure_handle, 'Renderer', 'painters');
    end
    
    haxes = get(figure_handle,'children');
    
    if ~isempty(font_size)
        set(haxes, 'FontSize', font_size);
        set(findall(figure_handle, 'Type', 'text'), 'FontSize', font_size)
    end
    
    if save_figure && exist(fileparts([output_path filename]), 'dir') ~= 7
        mkdir(fileparts([output_path filename]));
    end
    
    axes_idx = ismember([{} get(haxes, 'type')], 'axes');
    real_axis_count = sum(axes_idx);
    
    current_title = cell(real_axis_count, 1);
    remove_title = false;
    
    if (save_eps || save_pdf) && real_axis_count >= 1 && remove_title_opt
        t = get(haxes(axes_idx), 'title');
        if real_axis_count == 1
            current_title{1} = get(t, 'String');
            remove_title = true;
        else
            for i = 1:real_axis_count
                current_title{i} = get(t{i}, 'String');
            end
            remove_title = true;
            for i = 1:(real_axis_count - 1)
                this_text = current_title{i};
                if iscell(this_text) && (numel(this_text) > 1 || (~isempty(this_text{1}) && ~strcmp(this_text{1}, char(160))))
                    remove_title = false;
                elseif ~isempty(current_title{i}) && ~strcmp(current_title{i}, char(160))
                    remove_title = false;
                end
            end
        end
    end
    
    if save_figure
        i = 1;
        while i > 0
            try
                if save_png
                    saveas(figure_handle, [output_path filename filename_suffix], 'png');
                end
                if save_fig
                    saveas(figure_handle, [output_path filename filename_suffix], 'fig');
                end
                if (save_eps || save_pdf)
                    if remove_title && real_axis_count == 1
                        title(haxes(axes_idx), '');
                    elseif remove_title && real_axis_count > 1
                        for ax = find(axes_idx)'
                            title(haxes(ax), '');
                        end
                    end
                end
                if save_eps
                    saveas(figure_handle, [output_path filename filename_suffix], 'epsc');
                end
                if save_pdf
                    saveas(figure_handle, [output_path filename filename_suffix], 'pdf');
                end
                i = 0;
            catch err
                warning(err.message);
                i = i + 1;
                if i >= 100
                    rethrow(err);
                end
                pause(0.1);
            end
            if (save_eps || save_pdf) && remove_title && real_axis_count >= 1
                j = 1;
                for ax = find(axes_idx)'
                    title(haxes(ax), current_title{j});
                    j = j + 1;
                end
            end
        end
    end
end
