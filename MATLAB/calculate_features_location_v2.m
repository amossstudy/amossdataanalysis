function [features] = calculate_features_location_v2(data, week_start, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 07-Aug-2015

% References:
% * Saeb S., Zhang M., Karr C.J., Schueller S.M., Corden M.E., Kording
%   K.P., Mohr D.C. (2015) Mobile Phone Sensor Correlates of Depressive 
%   Symptom Severity in Daily-Life Behavior: An Exploratory Study. Journal
%   of Medical Internet Research, 17(7), e175. doi:10.2196/jmir.4273
%   Retrieved from http://www.jmir.org/2015/7/e175/

    params = get_configuration('features_location_v2', varargin{:});
    
    data_subset_to_calculate = get_argument({'feature_type', 'featuretype', 'feature_types', 'featuretypes', ...
                                             'data_subset', 'datasubset', 'data_subsets', 'datasubsets'}, 'all', varargin{:});
    
    valid_std_multiple = get_argument({'valid_std_multiple', 'validstdmultiple'}, [1.75, 1.75, 1, 1.75, 0.75, 1, 1, 1.75, 1.75, 1.75], varargin{:});
    
    assert(numel(valid_std_multiple) == 1 || numel(valid_std_multiple) == params.features, sprintf('The valid_std_multiple option must have one or exactly %i elements.', params.features));
    
    D_MAX = 10;
    D = 0:D_MAX;
    
    if iscell(data_subset_to_calculate) || ~strcmpi(data_subset_to_calculate, 'all')
        if ~iscell(data_subset_to_calculate)
            data_subset_to_calculate = {data_subset_to_calculate};
        end
        D_valid = false(size(D));
        
        for ft_i = 1:numel(data_subset_to_calculate)
            switch lower(data_subset_to_calculate{ft_i})
                case 'basic'
                    D_valid(D == 0) = true;
                case 'weekday'
                    D_valid(D == 8) = true;
                case 'weekend'
                    D_valid(D == 9) = true;
                case 'median'
                    D_valid(D <= 9) = true;
                case 'valid'
                    D_valid(D == 10) = true;
            end
        end
        D = D(D_valid);
    end
    
    features = struct();
    features.basic = NaN(params.features, 1);
    features.basic_calculated = false;
    features.weekday = NaN(params.features, 1);
    features.weekday_calculated = false;
    features.weekend = NaN(params.features, 1);
    features.weekend_calculated = false;
    features.median = NaN(params.features, 1);
    features.median_calculated = false;
    features.valid = NaN(params.features, 1);
    features.valid_calculated = false;
    features.is_valid = false;
    features.week_start = NaN;

    if isempty(data)
       return;
    end
    
    features.week_start = week_start;
    
    time_days = floor(data.time);

    location_data = [data.lat data.lon]; % for normalised coordinates, this is in km from home
    location_data_dist = sqrt(sum(location_data .^ 2, 2)); % for normalised coordinates, this is in km from home
    location_time = min([1/(24 * 60); diff(data.time)], 1/(24 * 12));

    data_median = NaN(params.features, 10);

    for d = D
        if d == 0
            % All days
            week_days = ismember(time_days, week_start + (0:6));
        elseif d < 8
            % Exclude day at d - 1.
            week_days = ismember(time_days, week_start + (0:6)) & ~(time_days == (week_start + d - 1));
        elseif d == 8
            % Weekdays only.
            week_days = ismember(time_days, week_start + (0:4));
        elseif d == 9
            % Weekends only.
            week_days = ismember(time_days, week_start + (5:6));
        elseif d == 10
            max_dist = zeros(7, 1);
            
            for d2 = 0:6
                day_idx = ismember(time_days, week_start + d2);
                
                day_dist_max = max(location_data_dist(day_idx & (data.state == 1)));
                
                if ~isempty(day_dist_max)
                    max_dist(d2 + 1) = day_dist_max;
                end
            end
            
            if numel(valid_std_multiple) == 1
                valid_days = find(max_dist <= (median(max_dist) + (valid_std_multiple * std(max_dist)))) - 1;

                week_days = ismember(time_days, week_start + valid_days);
            elseif numel(valid_std_multiple) == params.features
                week_days = false(size(time_days, 1), params.features);
                
                for f = 1:params.features
                    valid_days = find(max_dist <= (median(max_dist) + (valid_std_multiple(f) * std(max_dist)))) - 1;
                    
                    week_days(:, f) = ismember(time_days, week_start + valid_days);
                end
            end
        end

        if sum(sum(week_days)) > 0
            features.is_valid = true;
            
            for f = 1:params.features
                if size(week_days, 2) == 1
                    this_week_days = week_days;
                elseif size(week_days, 2) == params.features
                    this_week_days = week_days(:, f);
                end

                week_location_time = location_time(this_week_days);
                week_location_data = location_data(this_week_days, :);

                week_state = data.state(this_week_days);
                week_cluster_idx = data.cluster_idx(this_week_days);
                
                if isfield(data, 'cluster_valid');
                    valid_clusters = data.cluster_valid(this_week_days);
                    week_state(week_state == 1) = valid_clusters(week_state == 1);
                end

                week_stationary_cluster_idx = week_cluster_idx(week_state == 1);
                week_stationary_cluster_idx_unique = unique(week_stationary_cluster_idx);

                feature = NaN;

                switch f
                    case 1  % Location Variance

                        var_lat = var(data.lat(this_week_days), week_location_time);
                        var_lon = var(data.lon(this_week_days), week_location_time);

                        feature = log(var_lat + var_lon);


                    case 2  % Number of Clusters

                        feature = numel(week_stationary_cluster_idx_unique);


                    case 3  % Entropy

                        week_cluster_time = zeros(numel(week_stationary_cluster_idx_unique), 1);

                        for i = 1:numel(week_stationary_cluster_idx_unique)
                            week_cluster_time(i) = sum(week_location_time(week_cluster_idx == week_stationary_cluster_idx_unique(i)));
                        end

                        week_cluster_time = week_cluster_time / sum(week_cluster_time);

                        entropy_sum = 0;

                        for i = 1:numel(week_stationary_cluster_idx_unique)
                            entropy_sum = entropy_sum + (week_cluster_time(i) * log(week_cluster_time(i)));
                        end

                        entropy = -entropy_sum;

                        feature = entropy;


                    case 4  % Normalized Entropy

                        if numel(week_stationary_cluster_idx_unique) == 1
                            feature = 0;
                        else
                            week_cluster_time = zeros(numel(week_stationary_cluster_idx_unique), 1);

                            for i = 1:numel(week_stationary_cluster_idx_unique)
                                week_cluster_time(i) = sum(week_location_time(week_cluster_idx == week_stationary_cluster_idx_unique(i)));
                            end

                            week_cluster_time = week_cluster_time / sum(week_cluster_time);

                            entropy_sum = 0;

                            for i = 1:numel(week_stationary_cluster_idx_unique)
                                entropy_sum = entropy_sum + (week_cluster_time(i) * log(week_cluster_time(i)));
                            end

                            entropy = -entropy_sum;

                            feature = entropy / log(numel(week_stationary_cluster_idx_unique));
                        end


                    case 5  % Home Stay

                        if ~isempty(week_stationary_cluster_idx_unique)
                            cluster_locations = data.clusters(week_stationary_cluster_idx_unique, :);

                            cluster_location_dist = sqrt(sum(cluster_locations .^ 2, 2));

                            [~, cluster_home] = min(cluster_location_dist);

                            cluster_home_idx = week_stationary_cluster_idx_unique(cluster_home);

                            time_home = sum(week_location_time(week_cluster_idx == cluster_home_idx & week_state == 1));

                            time_away = sum(week_location_time(week_cluster_idx ~= cluster_home_idx & week_state == 1));
                        else
                            time_home = 0;
                            time_away = 0;
                        end
                        
                        feature = time_home / (time_home + time_away);


                    case {6, 9, 10}  % Circadian Movement

                        if sum(this_week_days) > 0
                            f_min = ceil((24 / 24.5) * 100) / 100;
                            f_max = floor((24 / 23.5) * 100) / 100;

                            f_range = f_min:0.001:f_max;

                            this_lat = data.lat(this_week_days);
                            this_lon = data.lon(this_week_days);

                            if f == 6 || f == 9
                                if f == 9
                                    this_lat = this_lat - mean(this_lat);
                                    this_lon = this_lon - mean(this_lon);

                                    if std(this_lat) > 0.001
                                        this_lat = this_lat / std(this_lat);
                                    end
                                    if std(this_lon) > 0.001
                                        this_lon = this_lon / std(this_lon);
                                    end
                                end

                                psd_lat = plomb(this_lat, data.time(this_week_days), f_range);
                                psd_lon = plomb(this_lon, data.time(this_week_days), f_range);

                                E_lat = sum(psd_lat) / numel(f_range);
                                E_lon = sum(psd_lon) / numel(f_range);

                                feature = log(E_lat + E_lon);
                            elseif f == 10
                                this_data = [this_lat this_lon];
                                this_data = sqrt(sum(this_data .^ 2, 2));

                                this_data = this_data - mean(this_data);

                                if std(this_lon) > 0.001
                                    this_data = this_data / std(this_data);
                                end

                                psd = plomb(this_data, data.time(this_week_days), f_range);

                                E = sum(psd) / numel(f_range);

                                feature = log(E);
                            end
                        end

                    case 7  % Transition Time

                        feature = sum(week_location_time(week_state == 2)) / sum(week_location_time);


                    case 8  % Total Distance

                        location_data_diff = (week_location_data(2:end, :) - week_location_data(1:end-1, :));
                        location_data_diff = sqrt(sum(location_data_diff .^ 2, 2)); % Distance travelled in km

                        feature = sum(location_data_diff);
                end

                if d == 0
                    features.basic(f) = feature;
                    features.basic_calculated = true;
                end

                data_median(f, d + 1) = feature;

                if d == 8
                    features.weekday(f) = feature;
                    features.weekday_calculated = true;
                elseif d == 9
                    features.weekend(f) = feature;
                    features.weekend_calculated = true;
                    if sum(~ismember(0:9, D)) == 0
                        features.median(f) = nanmedian(data_median(f, :));
                        features.median_calculated = true;
                    end
                elseif d == 10
                    features.valid(f) = feature;
                    features.valid_calculated = true;
                end
            end
        end
    end
end
