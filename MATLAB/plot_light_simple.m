function plot_light_simple(data, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    t_min = get_argument({'tmin', 't_min'}, floor(data.time(1)), varargin{:});
    t_max = get_argument({'tmax', 't_max'}, ceil(data.time(end)), varargin{:});
    
    [xticks, xticks_str] = get_xticks(t_min, t_max, varargin{:});
    
    light_data = data.data;
    
    light_data = log10(light_data + 1);
    
    light_nan = isnan(light_data);
    
    % restrict values to between 0 and 10000.
    light_data = min(light_data, log10(10000));
    
    light_data = light_data / log10(10000);
    
    light_data(light_nan) = NaN;
    
    figure
    stairs(data.time, light_data, 'LineWidth', 2);
    set(gca,'XTick',xticks)
    set(gca,'XTickLabel',xticks_str)
    set(gca,'YTick',[0 1])
    set(gca,'YTickLabel',[])
    
    %ylabel('Ambient light level \rightarrow', 'FontSize', 30);
    ylabel_text = {'Light'; 'Level'};
    hy = ylabel(ylabel_text, 'Rotation', 0, 'FontSize', 30);
    set(hy, 'Units', 'Normalized');
    pos = get(hy, 'Position');
    set(hy, 'Position', pos + [-0.055, -0.05 * numel(ylabel_text), 0]);
    
    title('Ambient Light Level', 'FontSize', 30);
    
    set(gca, 'FontSize', 30);
    
    p = get(gca, 'pos');
    % When changing width we also need to change bottom and height.
    p(2) = 0.1798;
    p(3) = 0.7450;
    p(4) = 0.7081;
    set(gca, 'pos', p);
    
    p = get(gcf, 'Position');
    p(3) = 3 * p(3);
    set(gcf, 'Position', p);
    
    p = get(gcf, 'PaperPosition');
    p(3) = 3 * p(3);
    set(gcf, 'PaperPosition', p);
    
    a = axis();
    a(1) = t_min;
    a(2) = t_max;
    a(3) = -0.01;
    a(4) = 1.01;
    axis(a);
end