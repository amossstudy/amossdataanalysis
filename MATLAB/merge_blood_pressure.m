function blood_pressure = merge_blood_pressure(blood_pressure1, blood_pressure2, sort_data)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 10-May-2016

    if nargin < 3
        sort_data = 1;
    end
    
    blood_pressure.time = [blood_pressure1.time; blood_pressure2.time];
    blood_pressure.systolic = [blood_pressure1.systolic; blood_pressure2.systolic];
    blood_pressure.diastolic = [blood_pressure1.diastolic; blood_pressure2.diastolic];
    blood_pressure.hr = [blood_pressure1.hr; blood_pressure2.hr];
    
    if (sort_data)
        [blood_pressure.time, t_sort_order] = sort(blood_pressure.time);
        blood_pressure.systolic = blood_pressure.systolic(t_sort_order, :);
        blood_pressure.diastolic = blood_pressure.diastolic(t_sort_order, :);
        blood_pressure.hr = blood_pressure.hr(t_sort_order, :);
    end
end
