function visualise_classification_location_v2_data_subsets(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 19-Nov-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/classification/data_subsets';
    
    params = get_configuration('features_location_v2', varargin{:});
    
    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    
    compare_to_variant = get_argument({'comparetovariant', 'compare_to_variant'}, NaN, varargin{:});
    
    show_horizontal_lines = get_argument({'showhorizontallines', 'show_horizontal_lines'}, false, varargin{:});
    
    show_combined = get_argument({'showcombined', 'show_combined'}, false, varargin{:});
    
    if ischar(compare_to_variant)
        y_lim = get_argument({'ylim', 'y_lim'}, [-0.2 0.2], varargin{:});
    else
        y_lim = get_argument({'ylim', 'y_lim'}, [0.5 1], varargin{:});
    end
    
    classification_data = load_classification_data_location_v2(varargin{:});
    
    [classification_params, classification_results] = preprocess_classification_data_location_v2(classification_data);
    
    if ischar(compare_to_variant)
        classification_data = load_classification_data_location_v2(varargin{:}, 'variant', compare_to_variant, 'warn_not_found', true);
        [~, compare_to_results] = preprocess_classification_data_location_v2(classification_data);
    end
    
    label_opts = {};
    if latex_labels
        label_opts = {'Interpreter', 'latex'};
    end
    
    for type_cell = fieldnames(classification_results)'
        if sum(strcmp(type_cell, {'ind_features'})) > 0
            for t = classification_params.TESTS_TO_PERFORM
                q_all = [classification_params.Q, classification_params.Q_MAX + classification_params.Q_EXTRA_MAX];
                q_to_display = ismember(q_all, [6, 10, 11, classification_params.Q_MAX + classification_params.Q_EXTRA_TO_TEST]);
                for q = find(q_to_display & classification_results.meta.q_valid(t, :))
                    num_groups_base = 10;
                    num_groups = num_groups_base;
                    feature_bars_base = classification_params.FEATURES + 22;
                    feature_bars = feature_bars_base;
                    
                    if show_combined
                        feature_bars = feature_bars + 7;
                        num_groups = num_groups + 1;
                    end

                    data_boxplot_ac = NaN(classification_params.REPS, feature_bars);
                    data_boxplot_f1 = NaN(classification_params.REPS, feature_bars);

                    str = cell(num_groups, 1);
                    
                    for f = 1:num_groups_base
                        f_disp = params.feature_display_order(f);
                        for ft = 1:5
                            data_boxplot_ac(:, (f_disp - 1) * 7 + ft + 2) = classification_results.(type_cell{:}).accuracy_raw(t, q, f + ((ft - 1) * 10), :);
                            data_boxplot_f1(:, (f_disp - 1) * 7 + ft + 2) = classification_results.(type_cell{:}).f1_raw(t, q, f + ((ft - 1) * 10), :);
                            
                            if show_combined
                                data_boxplot_ac(f, feature_bars_base + ft) = median(data_boxplot_ac(:, (f_disp - 1) * 7 + ft + 2));
                                data_boxplot_f1(f, feature_bars_base + ft) = median(data_boxplot_f1(:, (f_disp - 1) * 7 + ft + 2));
                            end
                            
                            if ischar(compare_to_variant)
                                if show_combined
                                    data_boxplot_ac(f, feature_bars_base + ft) = data_boxplot_ac(f, feature_bars_base + ft) - median(squeeze(compare_to_results.(type_cell{:}).accuracy_raw(t, q, f + ((ft - 1) * 10), :)));
                                    data_boxplot_f1(f, feature_bars_base + ft) = data_boxplot_f1(f, feature_bars_base + ft) - median(squeeze(compare_to_results.(type_cell{:}).f1_raw(t, q, f + ((ft - 1) * 10), :)));
                                end
                                
                                data_boxplot_ac(:, (f_disp - 1) * 7 + ft + 2) = data_boxplot_ac(:, (f_disp - 1) * 7 + ft + 2) - squeeze(compare_to_results.(type_cell{:}).accuracy_raw(t, q, f + ((ft - 1) * 10), :));
                                data_boxplot_f1(:, (f_disp - 1) * 7 + ft + 2) = data_boxplot_f1(:, (f_disp - 1) * 7 + ft + 2) - squeeze(compare_to_results.(type_cell{:}).f1_raw(t, q, f + ((ft - 1) * 10), :));
                            end
                        end
                        
                        str{f_disp} = params.feature_short{params.feature_order(f)};
                    end
                    
                    if show_combined
                        str{num_groups_base + 1} = 'Combined';
                    end

                    clr_boxplot = [0 0 0; 0 0 0; params.clr_data_subset];
                    
                    for ot = 1:2
                        if ot == 1
                            data_boxplot = data_boxplot_ac;
                        elseif ot == 2
                            data_boxplot = data_boxplot_f1;
                        end
                        
                        figure;

                        boxplot(data_boxplot, 'Colors', clr_boxplot, 'plotstyle', 'compact');

                        hold on;

                        for f = 1:(num_groups - 1)
                            plot([8.5 8.5] + (f - 1) * 7, [-1 1], 'Color', [0.5 0.5 0.5])
                        end
                        
                        if show_horizontal_lines
                            for y = -1:0.05:1
                                if y > y_lim(1) && y < y_lim(2)
                                    if ischar(compare_to_variant) && y == 0
                                        plot([1.5, (feature_bars - 0.5)], [y, y], 'Color', [0.5 0.5 0.5])
                                    else
                                        plot([1.5, (feature_bars - 0.5)], [y, y], ':', 'Color', [0.5 0.5 0.5])
                                    end
                                end
                            end
                        end

                        %h_baseline = plot([0.5 ((params.FEATURES * 4) + 1.5)], [0 0] + baseline(t, q), 'Color', lines(1));
                        %uistack(h_baseline, 'bottom');

                        hold off;

                        h = findall(gca,'Tag','Box');
                        legend(h(7:-1:3), params.data_subset_name, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 20, label_opts{:})

                        set(gca, 'xlim', [1.5, (feature_bars - 0.5)]);
                        set(gca, 'ylim', y_lim);
                        set(gca, 'xtick', 5:7:feature_bars);

                        set(gca, 'xticklabel', str, 'FontSize', 18);

                        %set(gca, 'XTickLabelRotation', 35)

                        if q <= numel(classification_params.Q)
                            if latex_labels
                                this_q_desc = sprintf('QIDS $\\geq$ %i', classification_params.Q(q));
                            else
                                this_q_desc = sprintf('QIDS %s %i', char(8805), classification_params.Q(q));
                            end
                            this_q_filename_suffix = sprintf('qids_%.2i', classification_params.Q(q));
                        elseif q == (numel(classification_params.Q) + 1)
                            this_q_desc = 'Cohort classification';
                            this_q_filename_suffix = 'cohort';
                        end

                        title(sprintf('Classification Results (%s, %s)', params.test_description{t}, this_q_desc), 'FontSize', 30, label_opts{:});

                        xlabel('Feature', 'FontSize', 30, label_opts{:});
                        
                        ylabel_prefix = 'Classification';

                        if ischar(compare_to_variant)
                            ylabel_prefix = 'Change in classification';
                        end
                        if ot == 1
                            ylabel(sprintf('%s accuracy', ylabel_prefix), 'FontSize', 20, label_opts{:});
                            this_q_filename_suffix = sprintf('%s_ac', this_q_filename_suffix);
                        elseif ot == 2
                            if latex_labels
                                ylabel(sprintf('%s F$_1$ Score', ylabel_prefix), 'FontSize', 20, label_opts{:});
                            else
                                ylabel(sprintf('%s F1 Score', ylabel_prefix), 'FontSize', 20, label_opts{:});
                            end
                            this_q_filename_suffix = sprintf('%s_f1', this_q_filename_suffix);
                        end

                        if latex_ticks
                            set(gca, 'TickLabelInterpreter', 'latex')
                        end
                        
                        p = get(gcf, 'PaperPosition');
                        p(3) = 2 * p(3);
                        set(gcf, 'PaperPosition', p);

                        save_figure(sprintf('data_subsets_%s_%s_%s', this_q_filename_suffix, type_cell{:}, params.test_description_filename{t}), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);
                    end
                end
            end
        end
    end
end
