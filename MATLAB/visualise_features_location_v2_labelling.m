function visualise_features_location_v2_labelling(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 19-Sep-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/labelling';
    
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, false, varargin{:});
    
    clr = lines(7);
    
    for file = get_participants(varargin{:})'
        class = get_participant_property(file.name, 'classification', varargin{:});
        
        if ~isempty(class)
            for variant_cell = {'', 'gms', 'combined_std_gms'}
                annotations = load_location_annotations(file.name, varargin{:}, 'variant', variant_cell{:});
                
                if ~isempty(fieldnames(annotations)) && isfield(annotations, 'week_annotations') && (sum(annotations.week_annotations.valid) > 0)
                    features = load_location_features(file.name, varargin{:}, 'variant', variant_cell{:});
                    
                    if ~isempty(fieldnames(features)) && isfield(features, 'v2') && sum(features.v2.is_valid) > 0
                        tc_data = load_true_colours_data(file.name, varargin{:});
                        
                        valid_weeks = find(annotations.week_annotations.valid);
                        
                        for w = 1:numel(valid_weeks)
                            this_week_start = annotations.week_annotations.start(valid_weeks(w));
                            
                            h_bottom = cell(3, 1);
                        
                            if isfield(tc_data, 'QIDS')
                                valid_qids = (tc_data.QIDS.time >= (this_week_start - 14)) & ...
                                             (tc_data.QIDS.time <= (this_week_start + 7 + 14)) & ...
                                             (tc_data.QIDS.data > -1);
                                
                                valid_qids_time = tc_data.QIDS.time(valid_qids);
                                valid_qids_data = tc_data.QIDS.data(valid_qids);
                                
                                selected_qids = NaN;
                                
                                if (sum(valid_qids) >= 1)
                                    figure;
                                    
                                    h_bottom{3} = patch(this_week_start + [0 7 7 0], [0 0 25 25], [0.9 0.9 0.9], 'linestyle', 'none');
                                    
                                    set(gca, 'xlim', this_week_start + [-14, (7 + 14)]);
                                    set(gca, 'ylim', [0 25]);
                                    set(gca, 'box', 'on');
                                    set(gca, 'xtick', this_week_start + (-14:7:21));
                                    set(gca, 'xticklabel', (-14:7:21));
                                    
                                    hold on;
                                    
                                    h_bottom{2} = scatter(valid_qids_time, valid_qids_data, 200, 'x', 'k', 'LineWidth', 2);
                                end
                                
                                if (sum(valid_qids) == 1) && ...
                                        (valid_qids_time <= (this_week_start + 7 + 3.5)) && ...
                                        (valid_qids_time >= (this_week_start - 3.5))
                                    
                                    selected_qids = valid_qids_data;
                                    
                                    scatter(valid_qids_time, valid_qids_data, 200, clr(7, :), 'x', 'LineWidth', 2.5);
                                elseif (sum(valid_qids) > 1) && ...
                                        (valid_qids_time(1) >= (this_week_start + 7)) && ...
                                        (valid_qids_time(1) <= (this_week_start + 7 + 3.5))
                                    
                                    selected_qids = valid_qids_data(1);
                                    
                                    scatter(valid_qids_time(1), valid_qids_data(1), 200, clr(7, :), 'x', 'LineWidth', 2.5);
                                elseif (sum(valid_qids) > 1) && ...
                                        (valid_qids_time(end) <= this_week_start) && ...
                                        (valid_qids_time(end) >= (this_week_start - 3.5))
                                    
                                    selected_qids = valid_qids_data(end);
                                    
                                    scatter(valid_qids_time(end), valid_qids_data(end), 200, clr(7, :), 'x', 'LineWidth', 2.5);
                                elseif (sum(valid_qids) > 1) && ...
                                          (valid_qids_time(1) <= (this_week_start + 7 + 7)) && ...
                                          (valid_qids_time(end) >= (this_week_start - 3.5))
                                      
                                      while sum(diff(valid_qids_time) < (1 / (24 * 60))) > 0
                                          i_equal = find(diff(valid_qids_time) < (1 / (24 * 60)));
                                          if numel(i_equal) > 1
                                              i_last_equal = i_equal(find([diff(i_equal); 2] > 1, 1, 'first')) + 1;
                                          else
                                              i_last_equal = i_equal + 1;
                                          end
                                          
                                          valid_qids_data(i_equal(1)) = mean(valid_qids_data(i_equal(1):i_last_equal));
                                          
                                          if i_last_equal == numel(valid_qids_time)
                                              r_valid = 1:i_equal(1);
                                          else
                                              r_valid = [1:i_equal(1) (i_last_equal + 1):numel(valid_qids_time)];
                                          end
                                          
                                          valid_qids_data = valid_qids_data(r_valid);
                                          valid_qids_time = valid_qids_time(r_valid);
                                      end
                                      
                                      interp_time = (0:0.5:7) + this_week_start;
                                      
                                      interp_time = interp_time(interp_time >= valid_qids_time(1) & interp_time <= valid_qids_time(end));
                                      
                                      qids_val = interp1(valid_qids_time, valid_qids_data, interp_time, 'linear');
                                      
                                      scatter(interp_time, qids_val, 75, clr(7, :), '+', 'LineWidth', 1);
                                      
                                      if valid_qids_time(1) > this_week_start
                                          interp_time = [valid_qids_time(1), interp_time]; %#ok<AGROW>
                                          
                                          scatter(valid_qids_time(1), valid_qids_data(1), 200, clr(7, :), 'x', 'LineWidth', 2.5);
                                      end
                                      
                                      if valid_qids_time(end) < this_week_start + 7
                                          interp_time = [interp_time, valid_qids_time(end)]; %#ok<AGROW>
                                          
                                          scatter(valid_qids_time(end), valid_qids_data(end), 200, clr(7, :), 'x', 'LineWidth', 2.5);
                                      end
                                      
                                      qids_val = interp1(valid_qids_time, valid_qids_data, interp_time, 'linear');
                                      
                                      plot(valid_qids_time, valid_qids_data, ':', 'Color', clr(7, :), 'LineWidth', 1.5);
                                      
                                      selected_qids = mean(qids_val);
                                end
                                
                                if (sum(valid_qids) >= 1)
                                    if ~isnan(selected_qids)
                                        h_bottom{1} = plot(this_week_start + [-14, (7 + 14)], [0 0] + selected_qids, 'Color', clr(4, :), 'LineWidth', 1.5);
                                        
                                        uistack([h_bottom{:}], 'bottom');
                                    end
                                    
                                    if latex_ticks
                                        set(gca, 'TickLabelInterpreter', 'latex')
                                    end
                                    
                                    set(gca, 'Layer','top')
                                    
                                    p = get(gcf, 'PaperPosition');
                                    p2 = p;
                                    p2(4) = p2(4) * 0.315;
                                    set(gcf, 'PaperPosition', p2);
                                    
                                    save_figure(sprintf('%s_wb_%s', file.name, datestr(this_week_start, 'yyyymmdd')), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 30, varargin{:});
                                    
                                    set(gca, 'xticklabel', []);
                                    
                                    p2 = p;
                                    p2(4) = p2(4) * 0.25;
                                    set(gcf, 'PaperPosition', p2);
                                    
                                    save_figure(sprintf('%s_wb_%s_no_xticklabel', file.name, datestr(this_week_start, 'yyyymmdd')), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 30, varargin{:});
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
