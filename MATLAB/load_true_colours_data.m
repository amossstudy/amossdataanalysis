function data = load_true_colours_data(participant, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 26-Jan-2015

    paths = get_paths(varargin{:});
    
    data = struct;
    
    working_filename = [participant '-true-colours.mat'];
    
    if (paths.prefer_working) && (exist([paths.working_path working_filename], 'file') == 2)
        if ~isempty(paths.temp_path) && (exist(paths.temp_path, 'dir') == 7) && ...
                exist([paths.temp_path working_filename], 'file') == 2
            fprintf('  Loading %s (from temp): ', working_filename);
            tic; data_loaded = load([paths.temp_path working_filename]); toc;
        else
            fprintf('  Loading %s: ', working_filename);
            tic; data_loaded = load([paths.working_path working_filename]); toc;
        end
        data = data_loaded.data;
    else
        max_file = [];
        max_file_date_time = 0;
        
        input_path = [paths.unprocessed_data_path participant '/'];
        
        for file = dir([input_path 'true_colours*.csv'])'
            if strcmpi(file.name, 'true_colours.csv') || ~isempty(regexpi(file.name, 'true_colours-[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9].csv'))
                [file_date_time_string, ~] = regexp(file.name, 'true_colours-([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9]).csv','tokens','match');
                
                file_date_time = datenum(file_date_time_string{1}, 'yyyy-mm-dd-HH-MM-SS');
                
                if file_date_time > max_file_date_time
                    max_file = file.name;
                    max_file_date_time = file_date_time;
                end
            end
        end
        
        if ~isempty(max_file)
            fprintf('  Loading %s: ', max_file);
            tic; data = load_true_colours_file([input_path max_file]); toc;
        end
    end
end
