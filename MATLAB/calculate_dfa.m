function dfa = calculate_dfa(data, n_range)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 23-Dec-2014

% References:
% * http://www.physionet.org/physiotools/dfa/
% * Peng, C.-K., Buldyrev, S. V., Havlin, S., Simons, M., Stanley, H. E.,
%   & Goldberger, A. L. (1994). Mosaic organization of DNA nucleotides.
%   Phys. Rev. E, 49(2), 1685�1689.
%   Retrieved from http://dx.doi.org/10.1103/PhysRevE.49.1685
% * Peng, C.-K., Havlin, S., Stanley, H. E., & Goldberger, A. L. (1995).
%   Quantification of scaling exponents and crossover phenomena in
%   nonstationary heartbeat time series. Chaos, 5(1), 82�87.
%   Retrieved from http://dx.doi.org/10.1063/1.166141

    N = length(data);
    data_valid = data;
    data_valid(isnan(data)) = 0;
    data_mean = mean(data_valid);
    
    data_zero_mean = data_valid - data_mean;
    data_zero_mean(isnan(data)) = 0;
    
    data_integrated = cumsum(data_zero_mean);
    
    dfa = zeros(length(n_range), 1);
    
    for i = 1:length(n_range)
        n = n_range(i);
        F_sum = 0;
        n_total = 0;
        for j = 1:floor((N/n))
            data_range = (((j-1)*n)+1):(j*n);
            data_input = data(data_range);
            data_values = data_integrated(data_range);
            
            p_params = polyfit(data_range', data_values, 1);
            
            p_values = polyval(p_params, data_range');
            
            F_sum = F_sum + sum(((data_values - p_values) .^ 2));
            
            n_total = n_total + sum(~isnan(data_input));
        end
        F_sum = F_sum / n_total;
        dfa(i) = F_sum;
    end
end
