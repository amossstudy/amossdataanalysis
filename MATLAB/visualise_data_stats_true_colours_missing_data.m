function visualise_data_stats_true_colours_missing_data(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 20-Mar-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-stats';
    
    files = get_participants(varargin{:});
    
    questionaires = {'ALTMAN', 'EQ-5D', 'GAD-7', 'QIDS'};
    
    class_desc = {'Combined', 'HC', 'BD', 'BPD'};
    
    responses = struct();
    
    max_length = struct();
    
    for q_cell = questionaires
        this_questionaire = strrep(q_cell{1}, '-', '_');
        responses.(this_questionaire) = struct('data', NaN, 'class', NaN, 'j', 1);
        responses.(this_questionaire).data = cell(size(files));
        responses.(this_questionaire).class = NaN(size(files));
        
        max_length.(this_questionaire) = zeros(4, 1);
    end
    
    for file = files'
        fprintf('Loading %s\n', file.name)

        class = get_participant_property(file.name, 'classification', varargin{:});

        if ~isempty(class)
            data_tc = load_true_colours_data(file.name);

            if ~isempty(data_tc)
                for q_cell = questionaires
                    this_questionaire = strrep(q_cell{1}, '-', '_');
            
                    if isfield(data_tc, this_questionaire) && isfield(data_tc.(this_questionaire), 'data') && isfield(data_tc.(this_questionaire), 'time')
                        this_questionnaire_data = data_tc.(this_questionaire).data;
                        this_questionnaire_time = data_tc.(this_questionaire).time;
                        this_questionnaire_time_responded = this_questionnaire_time(this_questionnaire_data > -1);
                        t_min = floor(min(this_questionnaire_time));
                        t_max = ceil(max(this_questionnaire_time));
                        t_range = t_min:7:t_max;
                        
                        if length(t_range) > 10
                            this_j = responses.(this_questionaire).j;
                            
                            responses.(this_questionaire).data{this_j} = NaN(length(t_range) - 1, 1);
                            responses.(this_questionaire).class(this_j) = class;

                            for t = 1:(length(t_range) - 1)
                                responses.(this_questionaire).data{this_j}(t) = sum(this_questionnaire_time_responded >= t_range(t) & this_questionnaire_time_responded < t_range(t + 1));
                            end

                            responses.(this_questionaire).j = this_j + 1;
                            
                            max_length.(this_questionaire)(1) = max(max_length.(this_questionaire)(1), length(t_range) - 1);
                            max_length.(this_questionaire)(class + 2) = max(max_length.(this_questionaire)(class + 2), length(t_range) - 1);
                        end
                    end
                end
            end
        end
    end
    
    for q_cell = questionaires
        this_questionaire = strrep(q_cell{1}, '-', '_');
        this_questionaire_str = strrep(this_questionaire, '_', '\_');
        this_questionaire_lc = lower(this_questionaire);
        
        figure;

        class_data = cell(4, 1);
        
        for i = 0:3
            subplot(4, 1, i + 1);

            if i == 0
                valid_idx = 1:(responses.(this_questionaire).j - 1);
            else
                valid_idx = find(responses.(this_questionaire).class == (i - 1))';
            end
            
            class_data{i + 1} = zeros(length(valid_idx), 1);
            
            k = 1;
            
            for j = valid_idx
                participant_data = responses.(this_questionaire).data{j};
                class_data{i + 1}(k) = sum(participant_data == 0) / length(participant_data);
                k = k + 1;
            end
            
            histogram(class_data{i + 1}, -0.05:0.1:1.05);
            
            if i == 0
                title([this_questionaire_str ' Missing Data']);
            elseif i == 3
                xlabel('Weeks with no data (%)');
            end
            
            ylabel(class_desc{i + 1});
            
            if i < 3
                set(gca, 'xticklabel', []);
            end
            set(gca, 'yticklabel', []);
            set(gca, 'xlim', [-0.05, 1.05]);
        end
        
        save_figure(['true_colours_missing_data_' this_questionaire_lc '_total'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    end
end
