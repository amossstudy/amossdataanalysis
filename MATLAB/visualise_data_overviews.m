function visualise_data_overviews(participant, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    %%
    %varargin = {};
    %varargin = {'tmin', datenum([2014 9 1 0 0 0])};
    %varargin = {'tmin', datenum([2014 9 25 0 0 0])};
    %varargin = {'tmax', datenum([2014 10 5 0 0 0])};
    %varargin = {'tmax', datenum([2014 10 21 0 0 0])};
    %varargin = {'tmin', datenum([2014 2 1 0 0 0]), 'tmax', datenum([2014 10 1 0 0 0])};
    %varargin = {'tmin', datenum([2014 9 1 0 0 0]), 'tmax', datenum([2014 11 1 0 0 0])};
    %varargin = {'tmin', datenum([2014 9 23 0 0 0]), 'tmax', datenum([2014 10 7 0 0 0])};
    %varargin = {'tmin', datenum([2014 9 20 0 0 0]), 'tmax', datenum([2014 10 10 0 0 0])};
    %%
    fprintf(' Exporting activity\n');
    visualise_data_overview_activity(participant, varargin{:});
    %%
    fprintf(' Exporting battery\n');
    visualise_data_overview_battery(participant, varargin{:});
    %%
    fprintf(' Exporting Calls & Texts\n');
    visualise_data_overview_calls_texts(participant, varargin{:});
    %%
    fprintf(' Exporting light\n');
    visualise_data_overview_light(participant, varargin{:});
    %%
    fprintf(' Exporting location\n');
    visualise_data_overview_location(participant, varargin{:});
    %%
    fprintf(' Exporting mood zoom\n');
    visualise_data_overview_mood_zoom(participant, varargin{:});
end
