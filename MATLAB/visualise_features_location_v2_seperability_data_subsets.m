function visualise_features_location_v2_seperability_data_subsets(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 19-Nov-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats/seperability';
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    data_full = data_raw';
    
    FEATURES = size(data_full, 2);
    
    valid_qids = ~isnan(classification_properties.QIDS);
    
    params = get_configuration('features_location_v2', varargin{:});
    
    participant_qids_score = classification_properties.QIDS(valid_qids);
    participant_qids = (participant_qids_score >= params.qids_threshold);
    participant_class = classification_properties.class(valid_qids);
    
    function prepare_and_save_figure(subdir, filename, varargin)
        save_figure(filename, 'output_sub_dir', [OUTPUT_SUB_DIR '/' subdir], 'FontSize', 50, varargin{:});
    end
    
    for t = 1:5
        if t == 1
            test_valid_qids = valid_qids;
            test_participant_qids = participant_qids;
        elseif t == 5
            test_classes = 0:1;
            
            test_participants_idx = ismember(participant_class, test_classes);
            
            test_valid_qids = valid_qids & ismember(classification_properties.class, test_classes);
            test_participant_qids = participant_qids(test_participants_idx);
        else
            test_classes = t - 2;
            
            test_participants_idx = (participant_class == test_classes);
            
            test_valid_qids = valid_qids & (classification_properties.class == test_classes);
            test_participant_qids = participant_qids(test_participants_idx);
        end
        
        this_test_description = params.test_description{t};
        this_test_description_filename = params.test_description_filename{t};

        data_seperability = NaN(1, FEATURES);
        data_classification_accuracy = NaN(1, FEATURES);
        data_classification_f1 = NaN(1, FEATURES);

        for i = 1:FEATURES
            this_data = data_full(test_valid_qids, params.feature_order(i));

            seperability_sum = 0;
            
            for j = 1:size(this_data, 1)
                this_diff = abs(this_data - this_data(j));
                this_diff(j) = NaN;
                
                [~, min_diff] = min(this_diff);
                
                seperability_sum = seperability_sum + mod(test_participant_qids(j) + test_participant_qids(min_diff) + 1, 2);
            end
            
            data_seperability(i) = seperability_sum / size(this_data, 1);
            
            if numel(unique(test_participant_qids)) > 1
                B = mnrfit(this_data, test_participant_qids + 1);

                pred_prob = mnrval(B, this_data);
                pred = pred_prob(:, 2) > sum(test_participant_qids) / numel(test_participant_qids);
                pred = pred + 1;
                % ** OR **
                % Mdl = fitcnb(this_data, test_participant_qids + 1, 'Prior', 'uniform');
                % Mdl = fitcdiscr(this_data, test_participant_qids + 1, 'DiscrimType', 'quadratic');
                % pred = predict(Mdl, this_data);

                cp = classperf(test_participant_qids + 1);
                classperf(cp, pred);
                
                data_classification_accuracy(i) = cp.CorrectRate;
                data_classification_f1(i) = 2 * ((cp.Sensitivity * cp.Specificity) / (cp.Sensitivity + cp.Specificity));
            else
                data_classification_accuracy(i) = 1;
                data_classification_f1(i) = 1;
            end
        end

        for s = 1:3
            figure;

            for i = 0:4
                if s == 1
                    scatter((i + 3):7:(FEATURES + 22), data_seperability((1:10) + i), 'MarkerEdgeColor', params.clr_data_subset(i + 1, :), 'MarkerFaceColor', params.clr_data_subset(i + 1, :));
                elseif s == 2
                    scatter((i + 3):7:(FEATURES + 22), data_classification_accuracy((1:10) + i), 'MarkerEdgeColor', params.clr_data_subset(i + 1, :), 'MarkerFaceColor', params.clr_data_subset(i + 1, :));
                elseif s == 3
                    scatter((i + 3):7:(FEATURES + 22), data_classification_f1((1:10) + i), 'MarkerEdgeColor', params.clr_data_subset(i + 1, :), 'MarkerFaceColor', params.clr_data_subset(i + 1, :));
                end
                if i == 0
                    hold on;
                end
            end
            
            for f = 1:9
                plot([8.5 8.5] + (f - 1) * 7, [0 1], 'Color', [0.5 0.5 0.5])
            end

            hold off;

            legend(params.data_subset_name, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 20)
            
            set(gca, 'xlim', [1.5, (FEATURES + 21.5)]);
            set(gca, 'ylim', [0.5, 1]);
            set(gca, 'xtick', 5:7:(FEATURES + 22));

            set(gca, 'xticklabel', params.feature_short(params.feature_order(1:10)), 'FontSize', 18);
            %set(gca, 'XTickLabelRotation', 35)

            title(sprintf('Class Seperability (%s, QIDS %s %i)', this_test_description, char(8805), params.qids_threshold), 'FontSize', 30);
            
            xlabel('Feature', 'FontSize', 20);
            
            if s == 1
                ylabel('Seperability Index', 'FontSize', 20);
                this_test_suffix = 'sep';
            elseif s == 2
                ylabel('Classification Accuracy', 'FontSize', 20);
                this_test_suffix = 'ac';
            elseif s == 3
                ylabel('Classification F1 Score', 'FontSize', 20);
                this_test_suffix = 'f1';
            end

            p = get(gcf, 'PaperPosition');
            p(3) = 2 * p(3);
            set(gcf, 'PaperPosition', p);

            prepare_and_save_figure('', sprintf('features_qids_%.2i_seperability_%s_%s', params.qids_threshold, this_test_description_filename, this_test_suffix), varargin{:}, 'FontSize', []);
        end
    end
end
