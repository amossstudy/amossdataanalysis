function visualise_data_quality_stats_activity_light(participant)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 19-Mar-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-quality';

    data_battery = load_battery_data(participant);
    data_activity = load_activity_data_raw(participant);
    data_light = load_light_data(participant);

    if ~isempty(data_activity)
        t_min = floor(min(data_activity.time));
        t_max = ceil(max(data_activity.time));
        downsampled_time = (t_min:(1/(24*60)):t_max)';
    else
        downsampled_time = [];
    end

    data_battery_downsampled = struct('time', downsampled_time', 'data', NaN(size(downsampled_time)));
    data_activity_samples = struct('time', downsampled_time', 'data', NaN(size(downsampled_time)));
    data_light_samples = struct('time', downsampled_time', 'data', NaN(size(downsampled_time)));

    if ~isempty(data_battery)
        T_DIFF = (5/(24*60));

        j = 1;

        for i = 1:numel(data_battery_downsampled.time);
            while (j <= numel(data_battery.time)) && (data_battery.time(j) < (data_battery_downsampled.time(i) - T_DIFF))
                j = j + 1;
            end

            if (j <= numel(data_battery.time)) && (data_battery.time(j) <= (data_battery_downsampled.time(i) + T_DIFF))
                k = j;
                while (k <= numel(data_battery.time)) && (data_battery.time(k) <= (data_battery_downsampled.time(i) + T_DIFF))
                    k = k + 1;
                end

                k = k - 1;

                time_range = data_battery.time(j:k);
                data_range = data_battery.data(j:k);

                [~, nearest_time_idx] = min(abs(time_range - data_battery_downsampled.time(i)));
                data_battery_downsampled.data(i) = data_range(nearest_time_idx);
            end
        end
    end

    data_activity_samples.data(~isnan(data_battery_downsampled.data)) = 0;

    T_DIFF = (1/(24*60*2));

    j = 1;

    for i = 1:numel(data_activity_samples.time);
        while (j <= numel(data_activity.time)) && (data_activity.time(j) < (data_activity_samples.time(i) - T_DIFF))
            j = j + 1;
        end

        if (j <= numel(data_activity.time)) && (data_activity.time(j) <= (data_activity_samples.time(i) + T_DIFF))
            k = j;
            while (k <= numel(data_activity.time)) && (data_activity.time(k) <= (data_activity_samples.time(i) + T_DIFF))
                k = k + 1;
            end

            data_activity_samples.data(i) = k - j;
        end
    end

    data_light_samples.data(~isnan(data_battery_downsampled.data)) = 0;

    j = 1;

    for i = 1:numel(data_light_samples.time);
        while (j <= numel(data_light.time)) && (data_light.time(j) < (data_light_samples.time(i) - T_DIFF))
            j = j + 1;
        end

        if (j <= numel(data_light.time)) && (data_light.time(j) <= (data_light_samples.time(i) + T_DIFF))
            k = j;
            while (k <= numel(data_light.time)) && (data_light.time(k) <= (data_light_samples.time(i) + T_DIFF))
                k = k + 1;
            end

            data_light_samples.data(i) = k - j;
        end
    end

    figure
    subplot(2, 1, 1);
    histogram(data_activity_samples.data(data_activity_samples.data > 0), 0:5:1000);
    set(gca, 'xlim', [0 1000]);
    title('Activity data sample rates')
    set(gca, 'xticklabel', []);

    p = get(gca, 'Position');
    p_diff = p(4) * 0.075;
    p(4) = p(4) + p_diff;
    p(2) = p(2) - p_diff;
    set(gca, 'Position', p);

    fprintf('Minutes with no activity data: %d\n', sum(data_activity_samples.data == 0));
    fprintf('Missing activity data rate: %f\n', sum(data_activity_samples.data == 0) / sum(~isnan(data_activity_samples.data)));

    subplot(2, 1, 2);
    histogram(data_light_samples.data(data_light_samples.data > 0), 0:5:1000);
    set(gca, 'xlim', [0 1000]);
    title('Light data sample rates')
    xlabel('Samples per minute')

    p = get(gca, 'Position');
    p(4) = p(4) + p_diff;
    set(gca, 'Position', p);

    fprintf('Minutes with no light data: %d\n', sum(data_light_samples.data == 0));
    fprintf('Missing light data rate: %f\n', sum(data_light_samples.data == 0) / sum(~isnan(data_light_samples.data)));

    save_figure([participant '-activity-light-sample-rates-hist'], 'output_sub_dir', OUTPUT_SUB_DIR);
end
