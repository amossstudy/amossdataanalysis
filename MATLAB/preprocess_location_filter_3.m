function preprocess_location_filter_3(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 29-Jul-2015

    variant = get_argument('variant', '', varargin{:});
    combine_variants = get_argument({'combinevariants', 'combine_variants'}, {}, varargin{:});
    
    if isempty(variant) && ~isempty(combine_variants)
        variant = 'combined';

        for i = 1:length(combine_variants)
            this_variant_name = combine_variants{i};

            if isempty(this_variant_name)
                this_variant_name = 'std';
            end

            variant = [variant '_' this_variant_name]; %#ok<AGROW>
        end
    end

    variant_out = '';
    
    if ~isempty(variant)
        variant_out = ['_' variant];
    end

    for file = get_participants(varargin{:})'
        fprintf('Processing %s\n', file.name)

        data = load_location_preprocessed(file.name, varargin{:}, 'variant', variant);

        if ~isempty(data) && isfield(data, 'normalised') && ~isempty(data.normalised.time)
            fprintf('  Filtering data:\n');
            tic;

            data.normalised.filtered = struct();
            data.normalised.filtered.time = data.normalised.time;
            data.normalised.filtered.data = data.normalised.data;
            data.normalised.filtered.lat = data.normalised.lat;
            data.normalised.filtered.lon = data.normalised.lon;
            
            final = false;
            
            for i = 1:100
                fprintf('    Itteration %i', i);
                
                location_data = [data.normalised.filtered.lat data.normalised.filtered.lon]; % for normalised coordinates, this is in km from home
                location_data_rounded = round(location_data * 100) / 100;
                
                data_differentiated = calculate_location_differential(data.normalised.filtered);
                
                invalid_diff = find(data_differentiated.data > 100);
                
                if ~isempty(invalid_diff)
                    fprintf(' (');
                    
                    invalid_source = location_data_rounded(invalid_diff - 1, :);
                    invalid_dest = location_data_rounded(invalid_diff, :);

                    [sources, ~, sources_idx] = unique(invalid_source, 'rows');
                    [dests, ~, dests_idx] = unique(invalid_dest, 'rows');

                    [sources_sorted, sources_sorted_idx] = sort(histc(sources_idx, sort(unique(sources_idx))), 'descend');
                    [dests_sorted, dests_sorted_idx] = sort(histc(dests_idx, sort(unique(dests_idx))), 'descend');

                    for j1 = 1:(numel(sources_sorted) + 1)
                        if (j1 > numel(sources_sorted))
                            break
                        else
                            valid_distance_from_home = true;
                            if (sum(sources(sources_sorted_idx(j1), :) .^2, 2) <= 0.05)
                                if ~final || (sum(mean(invalid_dest(sources_idx == sources_sorted_idx(j1), :), 1) .^2, 2) <= 0.1)
                                    valid_distance_from_home = false;
                                end
                            end
                            if valid_distance_from_home && ((sources_sorted(j1) / sum(ismember(location_data_rounded, sources(sources_sorted_idx(j1), :), 'rows'))) > 0.01)
                                break;
                            end
                        end
                    end
                    
                    for j2 = 1:(numel(dests_sorted) + 1)
                        if (j2 > numel(dests_sorted))
                            break;
                        else
                            valid_distance_from_home = true;
                            if (sum(dests(dests_sorted_idx(j2), :) .^2, 2) <= 0.05)
                                if ~final || (sum(mean(invalid_source(dests_idx == dests_sorted_idx(j2), :), 1) .^2, 2) <= 0.1)
                                    valid_distance_from_home = false;
                                end
                            end
                            if valid_distance_from_home && ((dests_sorted(j2) / sum(ismember(location_data_rounded, dests(dests_sorted_idx(j2), :), 'rows'))) > 0.01)
                                break;
                            end
                        end
                    end
                    
                    if (j1 <= numel(sources_sorted))
                        fprintf('max %i sources (%f)', sources_sorted(j1), sources_sorted(j1) / sum(ismember(location_data_rounded, sources(sources_sorted_idx(j1), :), 'rows')));
                    end
                    if (j2 <= numel(dests_sorted))
                        if (j1 <= numel(sources_sorted))
                            fprintf(', ');
                        end
                        fprintf('max %i dests (%f)', dests_sorted(j2), dests_sorted(j2) / sum(ismember(location_data_rounded, dests(dests_sorted_idx(j2), :), 'rows')));
                    end
                    fprintf(')');
                    
                    if ((j1 <= numel(sources_sorted)) && (j2 <= numel(dests_sorted)) && (max(sources_sorted(j1), dests_sorted(j2)) >= 10)) || ...
                            ((j1 <= numel(sources_sorted)) && (sources_sorted(j1) >= 10)) || ...
                            ((j2 <= numel(dests_sorted)) && (dests_sorted(j2) >= 10))
                        is_valid = true(size(data.normalised.filtered.time));

                        if (j1 <= numel(sources_sorted)) && (j2 <= numel(dests_sorted)) && (sources_sorted(j1) > dests_sorted(j2)) || ...
                                (j2 > numel(dests_sorted))
                            locations_at_source_mode = ismember(location_data_rounded, sources(sources_sorted_idx(j1), :), 'rows');
                            is_valid(locations_at_source_mode) = false;
                        else
                            locations_at_dest_mode = ismember(location_data_rounded, dests(dests_sorted_idx(j2), :), 'rows');
                            is_valid(locations_at_dest_mode) = false;
                        end

                        data.normalised.filtered.time = data.normalised.filtered.time(is_valid);
                        data.normalised.filtered.data = data.normalised.filtered.data(is_valid);
                        data.normalised.filtered.lat = data.normalised.filtered.lat(is_valid);
                        data.normalised.filtered.lon = data.normalised.filtered.lon(is_valid);
                    elseif ~final
                        final = true;
                    else
                        break;
                    end
                    
                    fprintf('\n');
                elseif ~final
                    final = true;
                else
                    break;
                end
            end
            
            fprintf('\n    Cleaning up duplicates');
            
            location_time_diff = [(1 / (24 * 4)); diff(data.normalised.filtered.time)];
            location_data = [data.normalised.filtered.lat data.normalised.filtered.lon]; % for normalised coordinates, this is in km from home
            location_data_rounded = round(location_data * 100) / 100;

            is_valid = true(size(data.normalised.filtered.time));
            
            % Process any duplicate times
            for i_dup = find(location_time_diff == 0)'
                t_min = i_dup - 1;
                t_max = i_dup;
                while data.normalised.filtered.time(i_dup) == data.normalised.filtered.time(t_max)
                    t_max = t_max + 1;
                    if t_max > numel(data.normalised.filtered.time)
                        break;
                    end
                end
                t_max = t_max - 1;
                
                for t1 = t_min:t_max
                    for t2 = t_min:t_max
                        if is_valid(t1) && is_valid(t2) && (t2 ~= t1)
                            if location_data_rounded(t1) == location_data_rounded(t2)
                                is_valid(t2) = false;
                            end
                        end
                    end
                end
                
                if sum(is_valid(t_min:t_max)) > 1
                    % There were missmatches
                    location_surrounding = NaN(2, 2);
                    if (t_min > 1) && (location_time_diff(t_min - 1) <= (1 / (24 * 4)))
                        location_surrounding(1, :) = location_data(t_min - 1, :);
                    end
                    if (t_max < numel(data.normalised.filtered.time)) && (location_time_diff(t_max + 1) <= (1 / (24 * 4)))
                        location_surrounding(2, :) = location_data(t_max + 1, :);
                    end
                    location_mean = nanmean(location_surrounding, 1);
                    
                    location_max_t = find(is_valid(t_min:t_max) == true, 1, 'first');
                    location_max_p = 0;
                    
                    if sum(isnan(location_mean)) == 0
                        for t = find(is_valid(t_min:t_max))'
                            location_prob = mvnpdf(location_data(t_min + t - 1, :), location_mean, [0.1, 0; 0, 0.1] .^ 2);
                            if location_prob > location_max_p
                                location_max_t = t;
                                location_max_p = location_prob;
                            end
                        end
                    end
                    is_valid(t_min:t_max) = false;
                    is_valid(t_min + location_max_t - 1) = true;
                end
            end
            
            data.normalised.filtered.time = data.normalised.filtered.time(is_valid);
            data.normalised.filtered.data = data.normalised.filtered.data(is_valid);
            data.normalised.filtered.lat = data.normalised.filtered.lat(is_valid);
            data.normalised.filtered.lon = data.normalised.filtered.lon(is_valid);
            
            data.normalised.filtered.differentiated = calculate_location_differential(data.normalised.filtered);
            
            fprintf('\n    Filtering remaining short transitions: ');
            
            location_time_diff = [(1 / (24 * 4)); diff(data.normalised.filtered.time)];
            location_data = [data.normalised.filtered.lat data.normalised.filtered.lon]; % for normalised coordinates, this is in km from home
            
            is_valid = true(size(data.normalised.filtered.time));
            
            t_start = 1;
            
            t_range = 1:numel(data.normalised.filtered.time);
            
            last_status_text = '';
            
            for t = t_range
                if (t == 1) || (rem(t, 500) == 0) || (t == t_range(end))
                    if ~isempty(last_status_text)
                        fprintf(repmat('\b', 1, length(last_status_text) - 1))
                    end
                    last_status_text = sprintf('%.2f%%%%', (t / t_range(end)) * 100);
                    fprintf(last_status_text);
                end
                
                if location_time_diff(t) > (1 / (24 * 4))
                    t_start = t;
                end
                
                % Consider max last 15 minutes
                while data.normalised.filtered.time(t_start) < data.normalised.filtered.time(t) - (1 / (24 * 4));
                    t_start = t_start + 1;
                end
                
                t_start_tau = t_start;
                
                for tau = [6 12 20 40]
                    % Consider max last 60/tau minutes
                    while data.normalised.filtered.time(t_start_tau) < data.normalised.filtered.time(t) - (1 / (24 * tau));
                        t_start_tau = t_start_tau + 1;
                    end

                    if is_valid(t) && ((t - t_start_tau) > 1) && (sum(is_valid(t_start_tau:t)) > 5)
                        locations_range = location_data(t_start_tau:(t - 1), :);
                        locations_range = locations_range(is_valid(t_start_tau:(t - 1)), :);

                        locations_mean = mean(locations_range, 1);
                        locations_cov = cov(locations_range) + (eye(2) * 0.1 ^ 2);

                        t_prob = mvnpdf(location_data(t, :), locations_mean, locations_cov) / mvnpdf(locations_mean, locations_mean, locations_cov);

                        if t_prob < 0.05
                            t2 = t;
                            while (t2 < numel(data.normalised.filtered.time)) && (data.normalised.filtered.time(t2 + 1) <= data.normalised.filtered.time(t) + (1 / (24 * tau * 2)))
                                t2 = t2 + 1;

                                t2_prob = mvnpdf(location_data(t2, :), location_data(t, :), [0.1, 0; 0, 0.1] .^ 2) / mvnpdf(location_data(t, :), location_data(t, :), [0.1, 0; 0, 0.1] .^ 2);

                                if t2_prob < 0.1
                                    break;
                                end
                            end

                            t2_prob = mvnpdf(location_data(t2, :), locations_mean, locations_cov) / mvnpdf(locations_mean, locations_mean, locations_cov);
                            t2_prob_last_t = mvnpdf(location_data(t2, :), location_data(t - 1, :), locations_cov) / mvnpdf(location_data(t - 1, :), location_data(t - 1, :), locations_cov);

                            if (t2_prob > 0.75) || (t2_prob_last_t > 0.9)
                                is_valid(t:(t2 - 1)) = false;
                                break;
                            end
                        end
                    end
                end
            end
            
            data.normalised.filtered.time = data.normalised.filtered.time(is_valid);
            data.normalised.filtered.data = data.normalised.filtered.data(is_valid);
            data.normalised.filtered.lat = data.normalised.filtered.lat(is_valid);
            data.normalised.filtered.lon = data.normalised.filtered.lon(is_valid);
            
            data.normalised.filtered.differentiated = calculate_location_differential(data.normalised.filtered);
            
            if ~isempty(last_status_text)
                fprintf(repmat('\b', 1, length(last_status_text) - 1))
            end
            fprintf('done');
            
            fprintf('\n    Completed: ');
            toc;
            
            save_data([file.name '-location' variant_out '-preprocessed'], data, varargin{:});
        else
            fprintf('  No data loaded\n');
        end
    end
end