function visualise_features_location_episodes(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 01-May-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/features-raw/episodes';
    
    [data_all, ~] = load_features_location('classification_filter', 1, varargin{:});
    
    assert(size(data_all, 2) > 0, 'No data loaded.');
    
    [data_episodes, episode_class] = load_features_location_episodes('classification_filter', 1, varargin{:});
    
    episode_indexes = struct();
    episode_indexes.euthymic = (episode_class == 0);
    episode_indexes.depressed = (episode_class == 1);
    episode_indexes.manic = (episode_class == 2);
    episode_indexes.mixed = (episode_class == 3);
    episode_indexes.total_episodes = ismember(episode_class, 1:3);
    episode_indexes.total = true(size(episode_class));
    
    fprintf('Usable episodes:\n')
    
    for episode_cell = fieldnames(episode_indexes)'
        episode_name = strrep(episode_cell{:}, '_', ' ');
        episode_name(1) = upper(episode_name(1));
        fprintf('  %s: %i\n', episode_name, sum(episode_indexes.(episode_cell{:})))
    end
    
    function prepare_and_save_figure(filename)
        save_figure(filename, 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    end

    function plot_graphs(i, str_title)
        figure;
        
        edge_max = -inf;
        edge_min = inf;
        
        [~, edges] = histcounts(data_all(i, :));
        if numel(edges) > 2
            edge_min = min(edge_min, edges(1));
            edge_max = max(edge_max, edges(end));
        end
        [~, edges] = histcounts(data_episodes(i, :));
        if numel(edges) > 2
            edge_min = min(edge_min, edges(1));
            edge_max = max(edge_max, edges(end));
        end
        [~, edges] = histcounts(data_episodes(i, episode_indexes.euthymic));
        if numel(edges) > 2
            edge_min = min(edge_min, edges(1));
            edge_max = max(edge_max, edges(end));
        end
        [~, edges] = histcounts(data_episodes(i, episode_indexes.depressed));
        if numel(edges) > 2
            edge_min = min(edge_min, edges(1));
            edge_max = max(edge_max, edges(end));
        end
        [~, edges] = histcounts(data_episodes(i, episode_indexes.manic));
        if numel(edges) > 2
            edge_min = min(edge_min, edges(1));
            edge_max = max(edge_max, edges(end));
        end
        
        edges = linspace(edge_min, edge_max, 11);
        
        subplots = 5;
        h_plot = zeros(subplots, 1);
        
        h_plot(1) = subplot(subplots, 1, 1);
        histogram(data_all(i, :), edges, 'Normalization', 'probability');
        title(str_title);
        ylabel('Full');
        h_plot(2) = subplot(subplots, 1, 2);
        histogram(data_episodes(i, :), edges, 'Normalization', 'probability');
        ylabel('Combined');
        h_plot(3) = subplot(subplots, 1, 3);
        histogram(data_episodes(i, episode_indexes.euthymic), edges, 'Normalization', 'probability');
        ylabel('Euthymic');
        h_plot(4) = subplot(subplots, 1, 4);
        histogram(data_episodes(i, episode_indexes.depressed), edges, 'Normalization', 'probability');
        ylabel('Depressed');
        h_plot(5) = subplot(subplots, 1, 5);
        histogram(data_episodes(i, episode_indexes.manic), edges, 'Normalization', 'probability');
        ylabel('Manic');

        for j = 1:subplots
            subplot(h_plot(j));

            set(gca, 'ylim', [0 1]);
            if j < subplots
                set(gca, 'xticklabel', []);
            end

            p = get(gca, 'Position');
            p_diff = p(4) * 0.175;
            p(4) = p(4) + p_diff;
            p(2) = p(2) - ((subplots - j) * (p_diff / (subplots - 1)));
            set(gca, 'Position', p);
        end

        p = get(gcf, 'Position');
        p(3) = 0.625 * p(3);
        p(4) = 1.5 * p(4);
        set(gcf, 'Position', p);

        p = get(gcf, 'PaperPosition');
        p(3) = 0.625 * p(3);
        p(4) = 1.5 * p(4);
        set(gcf, 'PaperPosition', p);
    end
    
    plot_graphs(1, 'Mean % > 5km (Weekdays)');
    prepare_and_save_figure('01_mean_5km_weekdays');

    plot_graphs(2, 'Mean % > 5km (Weekends)');
    prepare_and_save_figure('02_mean_5km_weekends');

    plot_graphs(3, 'Mean % > 20km (Weekdays)');
    prepare_and_save_figure('03_mean_20km_weekdays');

    plot_graphs(4, 'Mean % > 20km (Weekends)');
    prepare_and_save_figure('04_mean_20km_weekends');

    plot_graphs(5, 'Variance % > 5km (Weekdays)');
    prepare_and_save_figure('05_var_5km_weekdays');

    plot_graphs(6, 'Variance % > 5km (Weekends)');
    prepare_and_save_figure('06_var_5km_weekends');

    plot_graphs(7, 'Mean no. locations (Weekdays)');
    prepare_and_save_figure('07_mean_locations_weekdays');

    plot_graphs(8, 'Mean no. locations (Weekends)');
    prepare_and_save_figure('08_mean_locations_weekends');

    plot_graphs(9, 'Variance no. locations (Weekdays)');
    prepare_and_save_figure('09_var_locations_weekdays');

    plot_graphs(10, 'Variance no. locations (Weekends)');
    prepare_and_save_figure('10_var_locations_weekends');

    plot_graphs(11, 'Mean % days home (Weekdays)');
    prepare_and_save_figure('11_mean_stay_home_weekdays');

    plot_graphs(12, 'Mean % days home (Weekends)');
    prepare_and_save_figure('12_mean_stay_home_weekends');

    plot_graphs(13, 'Variance % days home (Weekdays)');
    prepare_and_save_figure('13_var_stay_home_weekdays');

    plot_graphs(14, 'Variance % days home (Weekends)');
    prepare_and_save_figure('14_var_stay_home_weekends');

    plot_graphs(15, 'Variance time leaving (Weekdays)');
    prepare_and_save_figure('15_var_leave_home_weekdays');

    plot_graphs(16, 'Variance time leaving (Weekends)');
    prepare_and_save_figure('16_var_leave_home_weekends');
end

