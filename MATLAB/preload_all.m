function preload_all(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 28-Jun-2015

    close all;

    preload_calls_texts(varargin{:});
    preload_location(varargin{:});
    preload_location(varargin{:}, 'variant', 'gms');

    preload_activity(varargin{:});
    preload_battery(varargin{:});
    preload_light(varargin{:});
    preload_mood_zoom(varargin{:});
    preload_true_colours(varargin{:});
    
    preprocess_location(varargin{:});
    preprocess_location(varargin{:}, 'variant', 'gms');
    preprocess_location(varargin{:}, 'combine_variants', {'', 'gms'});

    preprocess_location_filter_3('min_app_version', 1031, varargin{:}, 'variant', '');
    preprocess_location_filter_3('min_app_version', 1031, varargin{:}, 'variant', 'gms');
    preprocess_location_filter_3('min_app_version', 1031, varargin{:}, 'variant', 'combined_std_gms');

    preprocess_location_downsample_3('min_app_version', 1031, varargin{:}, 'variant', '');
    preprocess_location_downsample_3('min_app_version', 1031, varargin{:}, 'variant', 'gms');
    preprocess_location_downsample_3('min_app_version', 1031, varargin{:}, 'variant', 'combined_std_gms');

    preprocess_location_segment_3('min_app_version', 1031, varargin{:}, 'variant', '');
    preprocess_location_segment_3('min_app_version', 1031, varargin{:}, 'variant', 'gms');
    preprocess_location_segment_3('min_app_version', 1031, varargin{:}, 'variant', 'combined_std_gms');

    [~, this_prop] = load_features_location_v2(opts{:}, 'participant_filter', '14*');
    
    for participant = unique(this_prop.participant)'
        for variant = unique(this_prop.variant(strcmp(this_prop.participant, participant)))'
            extract_location_clusters_2(opts{:}, 'min_app_version', 1031, 'participant_filter', participant{:}, 'variant', variant{:});

            extract_features_location_v2(opts{:}, 'min_app_version', 1031, 'participant_filter', participant{:}, 'variant', variant{:});
        end
    end

    extract_location_clusters_2('min_app_version', 1031, 'participant_filter', '14*', 'variant', '');
    extract_location_clusters_2('min_app_version', 1031, 'participant_filter', '14*', 'variant', 'gms');
    extract_location_clusters_2('min_app_version', 1031, 'participant_filter', '14*', 'variant', 'combined_std_gms');

    extract_features_location_v2('min_app_version', 1031, 'participant_filter', '14*', 'variant', '');
    extract_features_location_v2('min_app_version', 1031, 'participant_filter', '14*', 'variant', 'gms');
    extract_features_location_v2('min_app_version', 1031, 'participant_filter', '14*', 'variant', 'combined_std_gms');

    extract_battery_discharges(varargin{:});
end
