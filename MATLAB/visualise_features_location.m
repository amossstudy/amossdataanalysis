function visualise_features_location(varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Dec-2014

    close all;
    
    OUTPUT_SUB_DIR = 'location/features-raw';
    
    [data_raw, class_3c] = load_features_location(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    params = get_configuration('features_location_v1', varargin{:});
    
    data_full = data_raw';
    
    clr = lines(3);
    
    function prepare_and_save_figure(filename, varargin)
        save_figure(filename, 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 50, varargin{:});
    end
    
    for f = 1:size(data_raw, 1)
        figure
        boxplot(data_full(:, f), class_3c)
        title(sprintf('%s %s', params.feature_title{f}, params.feature_subset{f}));
        set(gca, 'xticklabel', params.cohort_name)
        prepare_and_save_figure(sprintf('feature_individual_%.2i_%s_%s', f, params.feature_filename{f}, params.feature_subset_filename{f}), varargin{:});
    end
    
    for f = [1 2; 17 18]'
        figure;
        scatter(data_full(:, f(1)), data_full(:, f(2)), 10, clr(class_3c + 1, :));
        title(params.feature_title_full{f(1)});
        for i = 1:2
            str = params.feature_subset{f(i)};
            str = regexprep(str, '^at ', '');
            str(1) = upper(str(1));
            if i == 1
                xlabel(str);
            elseif i == 2
                ylabel(str);
            end
        end
        prepare_and_save_figure(sprintf('features_scatter_%.2i_%.2i_%s_%s_%s', f(1), f(2), params.feature_filename{f(1)}, params.feature_subset_filename{f(1)}, params.feature_subset_filename{f(2)}), varargin{:}, 'FontSize', 20);
    end
end
