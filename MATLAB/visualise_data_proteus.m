function visualise_data_proteus(participant, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 4-Nov-2014

    close all;
    
    OUTPUT_SUB_DIR = 'data-overview';
    
    data_acc = load_proteus_acc_data(participant, varargin{:});
    data_heart_rate = load_proteus_heart_rate_data(participant, varargin{:});
    data_skin_temp = load_proteus_skin_temp_data(participant, varargin{:});
    
    %%
    if ~isempty(data_acc)
        %%
        fprintf('  Plotting (acc) : ');
        tic; plot_proteus_acc_simple(data_acc, varargin{:}); toc;
        %%
        fprintf('  Saving (acc) : ');
        tic;
        save_figure([participant '-proteus-acc'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        save_data([participant '-proteus-acc'], data_acc, 'root_path_type', 'output_path', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
        %%
        fprintf('  Plotting (steps) : ');
        tic; plot_proteus_steps_simple(data_acc, varargin{:}); toc;
        %%
        fprintf('  Saving (steps) : ');
        tic;
        save_figure([participant '-proteus-steps'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        toc;
        %%
        fprintf('  Plotting (ref angle) : ');
        tic; plot_proteus_ref_angle_simple(data_acc, varargin{:}); toc;
        %%
        fprintf('  Saving (ref angle) : ');
        tic;
        save_figure([participant '-proteus-ref-angle'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        toc;
        %%
        fprintf('  Plotting (heart rate) : ');
        tic; plot_proteus_heart_rate_simple(data_heart_rate, varargin{:}); toc;
        %%
        fprintf('  Saving (heart rate) : ');
        tic;
        save_figure([participant '-proteus-heart-rate'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        save_data([participant '-proteus-heart-rate'], data_acc, 'root_path_type', 'output_path', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
        %%
        fprintf('  Plotting (skin temp) : ');
        tic; plot_proteus_skin_temp_simple(data_skin_temp, varargin{:}); toc;
        %%
        fprintf('  Saving (skin temp) : ');
        tic;
        save_figure([participant '-proteus-skin-temp'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        save_data([participant '-proteus-skin-temp'], data_skin_temp, 'root_path_type', 'output_path', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
    else
        fprintf('  No data loaded\n');
    end
end