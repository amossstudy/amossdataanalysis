function [normality_order, stability_order] = visualise_data_overview_all_true_colours_normality(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 01-Mar-2015

    close all;
    
    OUTPUT_SUB_DIR = 'true-colours';
    
    files = get_participants(varargin{:});
    
    data_dates = datenum([2014, 1, 1]):datenum([2015, 3, 1]);
    
    questionaires = {'ALTMAN', 'EQ_5D', 'GAD_7', 'QIDS'};
    
    class_desc = {'HC', 'BD', 'BPD'};
    
    data_dist = load_generic_data('true-colours-distributions');
    
    if ~isempty(data_dist)
        data = struct;
        data.class = ones(length(files), 1);
        data.normality = ones(length(files), 1);
        data.stability = ones(length(files), 1);
        data.label = cell(length(files), 1);
        
        j = 1;
        
        for file = files'
            fprintf('Loading %s\n', file.name)

            class = get_participant_property(file.name, 'classification', varargin{:});

            if ~isempty(class)
                data_tc = load_true_colours_preprocessed(file.name);

                if ~isempty(data_tc)
                    j_new = j;
                    
                    data.class(j) = class;
                    data.label{j} = file.name;
                    for this_questionaire_cell = questionaires
                        this_questionaire = this_questionaire_cell{1};

                        if isfield(data_tc, this_questionaire) && isfield(data_tc.(this_questionaire), 'regularised') && isfield(data_tc.(this_questionaire), 'weekly')
                            data_full = NaN(length(data_dates), 1);

                            A = ismember(data_dates, data_tc.(this_questionaire).regularised.time);
                            B = ismember(data_tc.(this_questionaire).regularised.time, data_dates);

                            data_full(A) = data_tc.(this_questionaire).regularised.data(B);
                            
                            data_valid = data_full(~isnan(data_full));
                            data_mean = mean(data_valid);
                            data_std = std(data_valid);
                            data_iqr = iqr(data_valid);
                            
                            data.normality(j) = data.normality(j) * get_likelihood(data_dist.(this_questionaire).mean.HC, data_mean);
                            data.stability(j) = data.stability(j) * get_likelihood(data_dist.(this_questionaire).std.HC, data_std);
                            data.stability(j) = data.stability(j) * get_likelihood(data_dist.(this_questionaire).iqr.HC, data_iqr);
                            
                            j_new = j + 1;
                        end
                    end
                    j = j_new;
                end
            end
        end

        clr = lines(4);
        clr(3, :) = clr(4, :);
        
        figure;
        hold on;
        
        for i = 1:3
            idx = (data.class == (i - 1)) & (1:(numel(data.class)) < j)';
            scatter(data.normality(idx), data.stability(idx), [], clr(data.class(idx) + 1, :), 'filled');
        end
        
        title('Normality and Stability of Participants');
        xlabel('Normality \rightarrow');
        ylabel('Stability \rightarrow');
        legend(class_desc, 'Location', 'SouthWest');
        
        set(gca,'XScale','log');
        set(gca,'YScale','log');
        
        set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'on');
        set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'on');
        
        save_figure('normaility_stability_overview', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        
        text(data.normality(1:(j-1)), data.stability(1:(j-1)), char(data.label{1:(j-1)}), 'horizontal','left', 'vertical','bottom')
        
        save_figure('normaility_stability_annotated_1', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        
        axis([10^-20, 10^-1, 10^-22, 10^-4]);
        save_figure('normaility_stability_annotated_2', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        
        axis([10^-13, 10^-2, 10^-13, 10^-4]);
        save_figure('normaility_stability_annotated_3', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

        axis([10^-11.5, 10^-2, 10^-10, 10^-4.5]);
        save_figure('normaility_stability_annotated_3', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        
        label_valid = data.label(1:(j-1));
        normality_idx_valid = data.normality(1:(j-1));
        stability_idx_valid = data.stability(1:(j-1));
        [~, normality_idx_ordered] = sort(normality_idx_valid);
        [~, stability_idx_ordered] = sort(stability_idx_valid);
        normality_order = label_valid(normality_idx_ordered);
        stability_order = label_valid(stability_idx_ordered);
    else
        fprintf('True Colours data distributions not loaded\n');
    end
    
    function likelihood = get_likelihood(dist, data)
        unicdf = @(x, lower, upper) cdf(makedist('Uniform', 'lower', min(lower, upper), 'upper', max(lower, upper)), x);

        if strcmp(dist.type, 'exp') == 1
            likelihood = expcdf  (data + dist.offset + 0.5, dist.mu)                - expcdf  (data + dist.offset - 0.5, dist.mu);
        elseif strcmp(dist.type, 'logn') == 1
            likelihood = logncdf (data + dist.offset + 0.5, dist.mu, dist.sigma)    - logncdf (data + dist.offset - 0.5, dist.mu, dist.sigma);
        elseif strcmp(dist.type, 'poiss') == 1
            likelihood = poisspdf(data, dist.lambda);
        elseif strcmp(dist.type, 'uni') == 1
            likelihood = unicdf  (data + dist.offset + 0.5, dist.lower, dist.upper) - unicdf  (data + dist.offset - 0.5, dist.lower, dist.upper);
        elseif strcmp(dist.type, 'gam') == 1
            likelihood = gamcdf  (data + dist.offset + 0.5, dist.a, dist.b)         - gamcdf  (data + dist.offset - 0.5, dist.a, dist.b);
        elseif strcmp(dist.type, 'f') == 1
            likelihood = fcdf    (data + dist.offset + 0.5, dist.v1, dist.v2)       - fcdf    (data + dist.offset - 0.5, dist.v1, dist.v2);
        elseif strcmp(dist.type, 'chi2') == 1
            likelihood = chi2cdf (data + dist.offset + 0.5, dist.v)                 - chi2cdf (data + dist.offset - 0.5, dist.v);
        end
    end
end
