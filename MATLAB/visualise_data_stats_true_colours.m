function visualise_data_stats_true_colours(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 10-Mar-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-stats';
    
    files = get_participants(varargin{:});
    
    today_vec = datevec(floor(now));
    
    data_dates = [2014, 1, 1; today_vec(1:3)];
    
    cohort_params = get_configuration('cohorts');
    cohort_names = cohort_params.cohort_name;
    
    questionaire_params = get_configuration('questionaires');
    questionaires = sort(fieldnames(questionaire_params))';
    
    stats = {'raw', 'mean', 'std', 'iqr'};
    
    days_from_start = get_argument({'only_days_from_start', 'onlydaysfromstart', ...
        'days_from_start', 'daysfromstart', ...
        'first_days', 'firstdays'}, -1, varargin{:});
    
    true_colours_data_type = get_argument({'true_colours_data_type', 'truecoloursdatatype'}, 'raw', varargin{:});
    plot_distribution_style = get_argument({'plot_distribution_style', 'plotdistributionstyle'}, 'continuous', varargin{:});
    
    filename_suffix = '';
    
    if days_from_start > 0
        filename_suffix = ['_first_' num2str(days_from_start) '_days'];
    end
    
    data_class = struct();
    for this_questionaire_cell = questionaires
        data_class.(this_questionaire_cell{:}) = cell(3, 1);
        for i = 1:3
            if days_from_start > 0
                data_class.(this_questionaire_cell{:}){i} = struct('data', NaN(length(files), days_from_start * 2), 'j', 1);
            else
                data_class.(this_questionaire_cell{:}){i} = struct('data', NaN(length(files), (datenum(data_dates(2,:)) - datenum(data_dates(1,:))) * 2), 'j', 1);
            end
        end
    end

    for file = files'
        fprintf('Loading %s\n', file.name)

        class = get_participant_property(file.name, 'classification', varargin{:});

        if ~isempty(class)
            if strcmpi(true_colours_data_type, 'raw')
                data_tc = load_true_colours_data(file.name);
            elseif strcmpi(true_colours_data_type, 'resampled')
                data_tc = load_true_colours_resampled(file.name);
            else
                error('Invalid True Colours data type: %s', true_colours_data_type);
            end

            for this_questionaire_cell = questionaires
                this_questionaire = this_questionaire_cell{1};
                if ~isempty(data_tc) && isfield(data_tc, this_questionaire) && isfield(data_tc.(this_questionaire), 'data')
                    if days_from_start > 0
                        date_entered = get_participant_property(file.name, 'date_entered', varargin{:});
                        if ~isempty(date_entered)
                            this_questionnaire_valid = (data_tc.(this_questionaire).time >= datenum(date_entered) & ...
                                data_tc.(this_questionaire).time < (datenum(date_entered) + days_from_start));
                            this_questionnaire_data = data_tc.(this_questionaire).data(this_questionnaire_valid);
                        else
                            this_questionnaire_data = NaN(1);
                        end
                    else
                        this_questionnaire_data = data_tc.(this_questionaire).data;
                    end
                    this_questionnaire_data = this_questionnaire_data(this_questionnaire_data > -1);

                    if ~isempty(this_questionnaire_data)
                        data_range = 1:length(this_questionnaire_data);

                        data_class.(this_questionaire){class + 1}.data(data_class.(this_questionaire){class + 1}.j, data_range) = this_questionnaire_data;

                        data_class.(this_questionaire){class + 1}.j = data_class.(this_questionaire){class + 1}.j + 1;
                    end
                end
            end
        end
    end

    for s = 1:numel(stats)
        this_stat = stats{s};
        if strcmpi(this_stat, 'raw');
            this_stat_filename_suffix = '';
        else
            this_stat_filename_suffix = ['_' this_stat];
        end
        
        for this_questionaire_cell = questionaires
            q = find(strcmp(this_questionaire_cell{:}, questionaires), 1, 'first');

            figure(s);

            this_questionaire = this_questionaire_cell{1};

            offset = 0;

            if strcmpi(this_stat, 'raw')
                max_hist = questionaire_params.(this_questionaire).max;

                d_hist = 1;
                if strcmp(this_questionaire, 'EQ_5D') == 1
                    d_hist = 5;
                end

                offset = d_hist / 2;

                xlabel_str = 'Score';
            elseif strcmpi(this_stat, 'mean')
                max_hist = questionaire_params.(this_questionaire).max;
                max_hist = (ceil(max_hist / 2) * 2);

                d_hist = 2;
                if strcmp(this_questionaire, 'EQ_5D') == 1
                    d_hist = 5;
                end

                xlabel_str = 'Mean Score';
            elseif strcmpi(this_stat, 'std')
                max_hist = 10;

                d_hist = 1;
                if strcmp(this_questionaire, 'EQ_5D') == 1
                    d_hist = 2;
                    max_hist = 30;
                end

                xlabel_str = 'Score Std';
            elseif strcmpi(this_stat, 'iqr')
                max_hist = 14;

                d_hist = 2;
                if strcmp(this_questionaire, 'EQ_5D') == 1
                    d_hist = 5;
                    max_hist = 50;
                end

                xlabel_str = 'Score IQR';
            end

            hist_edges = (-offset):d_hist:(max_hist + offset);

            class_dist = cell(3, 1);
            
            for i = 1:3
                j_range_class = 1:(data_class.(this_questionaire){i}.j - 1);
                class_data = data_class.(this_questionaire){i}.data(j_range_class, :);

                if strcmpi(this_stat, 'raw')
                    this_data = class_data(:);
                elseif strcmpi(this_stat, 'mean')
                    this_data = nanmean(class_data, 2);
                elseif strcmpi(this_stat, 'std')
                    this_data = nanstd(class_data, 0, 2);
                elseif strcmpi(this_stat, 'iqr')
                    this_data = iqr(class_data, 2);
                end
                
                subplot(3, numel(questionaires), ((i - 1) * numel(questionaires)) + q);

                histogram(this_data, hist_edges);

                hold on;
                axis manual;

                dist_data = fit_distributions(this_data, hist_edges, varargin{:}, 'generate_figures', false, 'offset', offset);

                class_dist{i} = dist_data;

                line_color = get_fitted_distribution_property(dist_data, 'line_color');
                line_style = get_fitted_distribution_property(dist_data, 'line_style');

                data_points = nansum(this_data > hist_edges(1) & this_data < hist_edges(end));

                if isempty(plot_distribution_style) || strcmpi(plot_distribution_style, 'none')
                elseif strcmpi(plot_distribution_style, 'continuous')
                    data_x = hist_edges(1):0.1:hist_edges(end);
                    data_y = get_fitted_distribution_property(dist_data, 'pdf', data_x);

                    plot(data_x, data_y * data_points * d_hist, line_style, 'color', line_color, 'LineWidth', 3);
                elseif strcmpi(plot_distribution_style, 'discretised')
                    data_x = mean(hist_edges(1:2)):d_hist:(mean(hist_edges(end-1:end)) + d_hist);
                    data_y = get_fitted_distribution_property(dist_data, 'pdf_int', data_x, 'pdf_int_width', d_hist);

                    stairs(data_x - dist_data.offset, data_y * data_points, line_style, 'color', line_color, 'LineWidth', 3);
                else
                    warning('Unknown distribution style "%s"', plot_distribution_style);
                end

                if i < 3
                  set(gca, 'xticklabel', []);
                end
                set(gca, 'ytick', []);
                set(gca, 'xlim', [hist_edges(1), hist_edges(end)]);
                if i == 1
                    title(questionaire_params.(this_questionaire).display_name);
                elseif i == 3
                    xlabel(xlabel_str);
                end
                if q == 1
                    ylabel(cohort_names{i});
                end
                p = get(gca, 'Position');
                p_diff = p(4) * 0.2;
                p(4) = p(4) + p_diff;
                if i == 1
                    p(2) = p(2) - p_diff;
                elseif i < 3
                    p(2) = p(2) - (p_diff / 2);
                end
                p_diff = p(3) * 0.15;
                p(3) = p(3) + p_diff;
                if q == numel(questionaires)
                    p(1) = p(1) - p_diff;
                elseif q > 1
                    p(1) = p(1) - ((q - 1) * (p_diff / (numel(questionaires) - 1)));
                end
                set(gca, 'Position', p);
            end

            if q == numel(questionaires)
                save_figure(sprintf('true_colours_score%s_hist%s', this_stat_filename_suffix, filename_suffix), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
            end

            if sum(strcmpi(plot_distribution_style, {'continuous', 'discretised'}) > 0)
                figure(s + numel(questionaires));

                for i = 1:2
                    subplot(2, (numel(questionaires) * 2) + 1, ((i - 1) * ((numel(questionaires) * 2) + 1)) + (q * 2) - [1 0]);

                    if i == 1
                        classes = 1:3;
                    elseif i == 2
                        classes = 2:3;
                    end

                    if strcmpi(plot_distribution_style, 'continuous')
                        data_x = hist_edges(1):0.1:hist_edges(end);
                        data_x_plot = data_x;
                        
                        data_y_classes = zeros(numel(data_x), 3);
                    elseif strcmpi(plot_distribution_style, 'discretised')
                        data_x = mean(hist_edges(1:2)):d_hist:(mean(hist_edges(end-1:end)) + d_hist);
                        data_x_plot = data_x - class_dist{1}.offset;
                        data_x_plot = reshape([data_x_plot(1:(end-1)); data_x_plot(2:end)], (numel(data_x_plot) - 1) * 2, 1);
                        
                        data_y_classes = zeros(numel(data_x_plot), 3);
                    end
                    
                    group_description = '';
                    
                    for j = classes
                        if strcmpi(plot_distribution_style, 'continuous')
                            data_y_classes(:, j) = get_fitted_distribution_property(class_dist{j}, 'pdf', data_x);
                        elseif strcmpi(plot_distribution_style, 'discretised')
                            this_data_y = get_fitted_distribution_property(class_dist{j}, 'pdf_int', data_x, 'pdf_int_width', d_hist);
                            
                            data_y_classes(:, j) = reshape([this_data_y(1:(end-1)); this_data_y(1:(end-1))], (numel(this_data_y) - 1) * 2, 1);
                        end
                        
                        if ~isempty(group_description)
                            group_description = [group_description '/']; %#ok<AGROW>
                        end
                        
                        group_description = [group_description cohort_names{j}]; %#ok<AGROW>
                    end
                    
                    area(data_x_plot, data_y_classes ./ repmat(sum(data_y_classes, 2), 1, 3));
                    
                    colormap(cohort_params.cohort_clr);
                    
                    set(gca, 'Layer', 'top');
                    if i < 2
                      set(gca, 'xticklabel', []);
                    end
                    if q > 1
                      set(gca, 'yticklabel', []);
                    end
                    set(gca, 'ylim', [0 1]);
                    set(gca, 'xlim', data_x_plot([1 end]));
                    if i == 1
                        title(questionaire_params.(this_questionaire).display_name);
                    elseif i == 2
                        xlabel(xlabel_str);
                    end
                    if q == 1
                        if i == 1
                            ylabel('All');
                        else
                            ylabel(group_description);
                        end
                    end
                    p = get(gca, 'Position');
                    p_diff = p(4) * 0.15;
                    p(4) = p(4) + p_diff;
                    if i == 1
                        p(2) = p(2) - p_diff;
                    elseif i < 2
                        p(2) = p(2) - (p_diff / 2);
                    end
                    p_diff = p(3) * 0.01;
                    p(3) = p(3) + p_diff;
                    if q == numel(questionaires)
                        p(1) = p(1) - p_diff;
                    elseif q > 1
                        p(1) = p(1) - ((q - 1) * (p_diff / (numel(questionaires) - 1)));
                    end
                    set(gca, 'Position', p);
                end
                
                subplot(2, (numel(questionaires) * 2) + 1, ((1:2) * ((numel(questionaires) * 2) + 1)));
                
                hcb = colorbar;
                axis off;
                
                p = get(hcb, 'Position');
                p(3) = p(3) * 4;
                p(1) = p(1) - 0.02;
                set(hcb, 'Position', p);
                
                set(hcb, 'ticks', (0:(1/3):(2/3)) + (1/6));
                set(hcb, 'ticklabels', cohort_names);
                set(hcb, 'AxisLocation', 'out');
                
                if q == numel(questionaires)
                    save_figure(sprintf('true_colours_cohorts_score%s_hist%s', this_stat_filename_suffix, filename_suffix), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
                end
            end
        end
    end
end
