function dist_data = fit_distributions(data_points, hist_edges, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 10-Mar-2015

    generate_figures = get_argument({'generatefigures', 'generate_figures'}, true, varargin{:});
    show_legend = get_argument({'showlegend', 'show_legend'}, true, varargin{:});
    data_type = get_argument({'datatype', 'data_type'}, 'points', varargin{:});
    x_offset = get_argument({'offset', 'xoffset', 'x_offset'}, 0, varargin{:});

    if strcmpi(data_type, 'points') && ~isempty(hist_edges)
        hist_counts = histcounts(data_points, hist_edges);
        hist_centers = (hist_edges(1:end-1)+hist_edges(2:end)) / 2;
        bin_width = hist_centers(2) - hist_centers(1);
        
        hist_centers = hist_centers(hist_counts > 0);
        hist_counts = hist_counts(hist_counts > 0);

        sample_min = hist_centers - (bin_width / 2) + x_offset;
        sample_max = hist_centers + (bin_width / 2) + x_offset;
        
        data_area = sum(hist_counts) * bin_width;
    elseif strcmpi(data_type, 'points') && isempty(hist_edges)
        data_area = get_argument({'dataarea', 'data_area'}, 1, varargin{:});
        x_lim = get_argument({'xlim', 'x_lim'}, [floor(min(data_points)) ceil(max(data_points))], varargin{:});
        assert(numel(x_lim) == 2, 'Invalid x limits specified');
        assert(x_lim(1) < x_lim(2), 'Invalid x limits specified');
        assert(x_offset == 0, 'Offset not currently supported');
    elseif strcmpi(data_type, 'pdf') && ~isempty(hist_edges)
        hist_counts = [((data_points(2:end) + data_points(1:(end-1))) / 2) .* (hist_edges(2:end) - hist_edges(1:(end-1))), 0];
        hist_counts(end) = (1 - sum(hist_counts));
        
        data_area = 1;
        
        sample_min = hist_edges;
        sample_max = [hist_edges(2:end), inf];
        
        assert(x_offset == 0, 'Offset not currently supported');
    elseif strcmpi(data_type, 'pdf') && isempty(hist_edges)
        error('Not currently supported');
    end

    dist_names = {'Exp', 'Log Normal', 'Poisson', 'Uniform', 'Gamma', 'F', 'Chi^2', 'Normal'};

    %unipdf          = @(x, lower, upper) pdf(makedist('Uniform', 'lower', lower, 'upper', upper), x);
    %unicdf          = @(x, lower, upper) cdf(makedist('Uniform', 'lower', min(lower, upper), 'upper', max(lower, upper)), x);

    function ret = plim(p)
        ret = p;
        ret(isinf(ret) & (sign(ret) == 1)) = 0;
        ret = max(1e-100, ret);
    end
    function ret = con_x_gt_0(x, val)
        ret = val;
        if (sum(x <= 0) > 0) ret = inf; end; %#ok<SEPEX>
    end
    function ret = con_x2_gt_x1(x, val) %#ok<DEFNU>
        ret = val;
        if (x(2) <= x(1)) ret = inf; end; %#ok<SEPEX>
    end
    if strcmpi(data_type, 'points') && isempty(hist_edges)
        fitfun_exp      = @(x) con_x_gt_0(x,    -(nansum((log(plim(exppdf(data_points, x)))))));
        fitfun_logn     = @(x) con_x_gt_0(x(2), -(nansum((log(plim(lognpdf(data_points, x(1), x(2))))))));
        %fitfun_poiss    = @(x) con_x_gt_0(x,    -(nansum((log(plim(poisspdf(data_points, x)))))));
        %fitfun_uni      = @(x) con_x2_gt_x1(x,  -(nansum((log(plim(unipdf  (data_points, x(1), x(2))))))));
        fitfun_gamma    = @(x) con_x_gt_0(x,    -(nansum((log(plim(gampdf(data_points, x(1), x(2))))))));
        %fitfun_f        = @(x) con_x_gt_0(x,    -(nansum((log(plim(fpdf    (data_points, x(1), x(2))))))));
        fitfun_chi2     = @(x) con_x_gt_0(x,    -(nansum((log(plim(chi2pdf(data_points, x)))))));
        fitfun_norm     = @(x) con_x_gt_0(x(2), -(nansum((log(plim(normpdf(data_points, x(1), x(2))))))));
        
        x_dist_cont = linspace(x_lim(1), x_lim(2), 5000);
        x_dist_desc = floor(min(data_points)):ceil(max(data_points) + 1);
    else
        fitfun_exp      = @(x) con_x_gt_0(x,    -(sum(hist_counts .* (log(plim(expcdf  (sample_max, x)          - expcdf  (sample_min, x)))))));
        fitfun_logn     = @(x) con_x_gt_0(x(2), -(sum(hist_counts .* (log(plim(logncdf (sample_max, x(1), x(2)) - logncdf (sample_min, x(1), x(2))))))));
        %fitfun_poiss    = @(x) con_x_gt_0(x,    -(sum(hist_counts .* (log(plim(poisspdf(hist_centers, x)))))));
        %fitfun_uni      = @(x) con_x2_gt_x1(x,  -(sum(hist_counts .* (log(plim(unicdf  (sample_max, x(1), x(2)) - unicdf  (sample_min, x(1), x(2))))))));
        fitfun_gamma    = @(x) con_x_gt_0(x,    -(sum(hist_counts .* (log(plim(gamcdf  (sample_max, x(1), x(2)) - gamcdf  (sample_min, x(1), x(2))))))));
        %fitfun_f        = @(x) con_x_gt_0(x,    -(sum(hist_counts .* (log(plim(fcdf    (sample_max, x(1), x(2)) - fcdf    (sample_min, x(1), x(2))))))));
        fitfun_chi2     = @(x) con_x_gt_0(x,    -(sum(hist_counts .* (log(plim(chi2cdf (sample_max, x)          - chi2cdf (sample_min, x)))))));
        fitfun_norm     = @(x) con_x_gt_0(x(2), -(sum(hist_counts .* (log(plim(normcdf (sample_max, x(1), x(2)) - normcdf (sample_min, x(1), x(2))))))));
        
        x_dist_cont = hist_edges(1):(range(hist_edges) / (numel(hist_edges) * 50)):hist_edges(end);
        x_dist_desc = hist_centers(1):(hist_centers(end) + 1);
    end
    
    fmin_options = optimset('Display','off');

    param_exp   = fminsearch(fitfun_exp,   20, fmin_options);
    param_logn  = fminsearch(fitfun_logn,  [1 1], fmin_options);
    %param_poiss = fminsearch(fitfun_poiss, 1, fmin_options);
    %param_uni   = fminsearch(fitfun_uni,   [0 160], fmin_options);
    param_gamma = fminsearch(fitfun_gamma, [20 20], fmin_options);
    %param_f     = fminsearch(fitfun_f,     [1 1], fmin_options);
    param_chi2  = fminsearch(fitfun_chi2,  50, fmin_options);
    param_norm  = fminsearch(fitfun_norm,  [nanmean(data_points) 1], fmin_options);

    l_exp   = -fitfun_exp   (param_exp);
    l_logn  = -fitfun_logn  (param_logn);
    %l_poiss = -fitfun_poiss (param_poiss);
    %l_uni   = -fitfun_uni   (param_uni);
    l_gamma = -fitfun_gamma (param_gamma);
    %l_f     = -fitfun_f     (param_f);
    l_chi2  = -fitfun_chi2  (param_chi2);
    l_norm  = -fitfun_norm  (param_norm);

    l = [l_exp, l_logn, -inf, -inf, l_gamma, -inf, l_chi2, l_norm];
    [~, dist_min] = max(l);

    dist_data = struct;

    dist_data.offset = x_offset;
    dist_data.discrete = false;

    switch dist_min
        case 1
            dist_data.type = 'exp';
            dist_data.mu = param_exp;
        case 2
            dist_data.type = 'logn';
            dist_data.mu = param_logn(1);
            dist_data.sigma = param_logn(2);
        case 3
            dist_data.type = 'poiss';
            dist_data.discrete = true;
            dist_data.lambda = param_poiss;
            dist_data.offset = 0;
        case 4
            dist_data.type = 'uni';
            dist_data.lower = param_uni(1);
            dist_data.upper = param_uni(2);
        case 5
            dist_data.type = 'gam';
            dist_data.a = param_gamma(1);
            dist_data.b = param_gamma(2);
        case 6
            dist_data.type = 'f';
            dist_data.v1 = param_f(1);
            dist_data.v1 = param_f(2);
        case 7
            dist_data.type = 'chi2';
            dist_data.v = param_chi2;
        case 8
            dist_data.type = 'norm';
            dist_data.mu = param_norm(1);
            dist_data.sigma = param_norm(2);
    end

    param_text = ['\newline{' get_fitted_distribution_property(dist_data, 'formatted_parameters') '}'];
    
    dist_data.description = [dist_names{dist_min} param_text];
    
    if (generate_figures == true)
        line_color = get_fitted_distribution_property(dist_data, 'line_color');
        line_style = get_fitted_distribution_property(dist_data, 'line_style');
        
        line_color = get_argument({'linecolor', 'line_color'}, line_color, varargin{:});
        
        if dist_data.discrete
            y_dist = get_fitted_distribution_property(dist_data, 'pdf', x_dist_desc);
            h_dist = stairs(x_dist_desc - 0.5, y_dist * data_area, line_style, 'Color', line_color, 'LineWidth', 3);
        else
            y_dist = get_fitted_distribution_property(dist_data, 'pdf', x_dist_cont);
            h_dist = plot(x_dist_cont, y_dist * data_area, line_style, 'Color', line_color, 'LineWidth', 3);
        end
        if (show_legend == true)
            legend(h_dist, [dist_names{dist_min} param_text]);
        end
    end
    
    fprintf(' %s\n', get_fitted_distribution_property(dist_data, 'description'));
end
