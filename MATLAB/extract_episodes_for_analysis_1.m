function extract_episodes_for_analysis_1(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 29-Mar-2015

% Criteria:
%   * Only include episodes within official AMoSS participation (max 12 months)
%   * Only non-overlapping manic and depressive episodes
%   * Exclude first week of episode or normal period
%   * Minimum epsode length 28 days (after exclusion of first week)
%
% M: ������|_______|���������|________________________
% D: �������|______|������|__________|����������������
%      ____    ____   ____      _____   _____________
%     | M  |  | D  | | M  |    |  D  | |   NORMAL    |

    files = get_participants(varargin{:});
    
    for file=files'
        fprintf('Extracting episodes for analysis for %s\n', file.name)
        
        data = load_true_colours_data(file.name, varargin{:});
        episodes = load_episodes(file.name, varargin{:});

        if ~isempty(data) && isstruct(data) && ~isempty(fields(data)) && ...
                ~isempty(episodes) && isstruct(episodes) && ~isempty(fields(episodes))

            date_entered = get_participant_property(file.name, 'date_entered', varargin{:});
            date_left = get_participant_property(file.name, 'date_left', varargin{:});

            if ~isempty(date_entered)
                episodes_for_analysis_min = datenum(date_entered);
            else
                episodes_for_analysis_min = episodes.time(1);
            end

            if ~isempty(date_left)
                episodes_for_analysis_max = datenum(date_left);
            else
                episodes_for_analysis_max = episodes.time(end) + 1;
            end

            valid_time = (episodes.time >= episodes_for_analysis_min) & (episodes.time < episodes_for_analysis_max);

            episodes_for_analysis = struct();
            episodes_for_analysis.time = episodes.time;
            episodes_for_analysis.manic = episodes.manic & ~episodes.depressed & valid_time;
            episodes_for_analysis.depressed = episodes.depressed & ~episodes.manic & valid_time;
            episodes_for_analysis.euthymic = ~episodes.depressed & ~episodes.manic & ~isnan(episodes.depressed_severity) & ~isnan(episodes.manic_severity);
            episodes_for_analysis.epochs = [];

            any_episode = episodes.manic | episodes.depressed | episodes_for_analysis.euthymic;
            any_changes = diff(any_episode);
            
            if sum(abs(any_changes)) > 0
                any_changes_idx = find(abs(any_changes) > 0);

                if any_changes(any_changes_idx(1)) == 1
                    any_changes_idx = any_changes_idx(2:end);
                end

                for i = 1:(length(any_changes_idx) - 1)
                    if (any_changes_idx(i + 1) - any_changes_idx(i)) < 21
                        if episodes_for_analysis.euthymic(any_changes_idx(i)) && episodes_for_analysis.euthymic(any_changes_idx(i + 1) + 1)
                            episodes_for_analysis.euthymic((any_changes_idx(i) + 1):any_changes_idx(i + 1)) = true;
                        end
                    end
                end
            end
            
            episodes_for_analysis.euthymic = episodes_for_analysis.euthymic & valid_time;
            
            l = 1;
            
            for mood_cell = {'manic', 'depressed', 'euthymic'}
                for i = find(diff([0; episodes_for_analysis.(mood_cell{:})]) > 0)'
                    episodes_for_analysis.(mood_cell{:})(i:(i + 6)) = false;
                end

                episode_changes = diff([0; episodes_for_analysis.(mood_cell{:})]);
                episode_changes_idx = find(abs(episode_changes) > 0);

                i = 1;

                episode_min = episodes_for_analysis.time(1);
                episode_min_idx = 1;

                while i <= length(episode_changes_idx)
                    if episode_changes(episode_changes_idx(i)) > 0
                        episode_min = episodes_for_analysis.time(episode_changes_idx(i));
                        episode_min_idx = episode_changes_idx(i);

                        if i == length(episode_changes_idx)
                            episode_max = episodes_for_analysis.time(end) + 1;
                            episode_max_idx = length(episodes_for_analysis.(mood_cell{:}));
                        end
                    else
                        episode_max = episodes_for_analysis.time(episode_changes_idx(i));
                        episode_max_idx = episode_changes_idx(i) - 1;
                    end
                    if (episode_changes(episode_changes_idx(i)) < 0) || (i == length(episode_changes_idx))
                        if (episode_max - episode_min) < 28
                            episodes_for_analysis.(mood_cell{:})(episode_min_idx:episode_max_idx) = false;
                        else
                            if l == 1
                                episodes_for_analysis.epochs = struct('type', '', 'start', 0, 'end', 0);
                            end
                            
                            episodes_for_analysis.epochs(l).type = mood_cell{:};
                            episodes_for_analysis.epochs(l).start = episode_min;
                            episodes_for_analysis.epochs(l).end = episode_max - 1;
                            
                            l = l + 1;
                        end
                    end

                    i = i + 1;
                end
            end

            save_data([file.name '-episodes-analysis'], episodes_for_analysis, varargin{:});
        else
            fprintf('  No data loaded\n');
        end
    end
end