function visualise_features_location_v2_gad_7(varargin)
% Copyright (c) 2017, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 27-Feb-2017

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats';
    
    [~, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(numel(classification_properties.participant) > 0, 'No data loaded.');
    
    valid_gad_7 = ~isnan(classification_properties.GAD_7);
    
    params = get_configuration('features_location_v2', varargin{:});
    params_q = get_configuration('questionaires', varargin{:});
    
    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    
    label_opts = {};
    if latex_labels
        label_opts = {'Interpreter', 'latex'};
    end
    
    show_x_label = get_argument({'showxlabel', 'show_xlabel', 'show_x_label'}, true, varargin{:});
    show_y_label = get_argument({'showylabel', 'show_ylabel', 'show_y_label'}, true, varargin{:});
    show_legend = get_argument({'showlegend', 'show_legend'}, true, varargin{:});
    
    plot_threshold = get_argument({'plotthreshold', 'plot_threshold'}, false, varargin{:});
    
    function prepare_and_save_figure(subdir, filename, varargin)
        save_figure(filename, 'output_sub_dir', [OUTPUT_SUB_DIR '/' subdir], 'FontSize', 50, varargin{:});
    end

    hist_max = 0;
    
    for t = 1:5
        if t == 1
            test_valid_gad_7 = valid_gad_7;
        elseif t == 5
            test_classes = 0:1;
            
            test_valid_gad_7 = valid_gad_7 & ismember(classification_properties.class, test_classes);
        else
            test_classes = t - 2;
            
            test_valid_gad_7 = valid_gad_7 & (classification_properties.class == test_classes);
        end
        
        this_test_description = params.test_description{t};
        this_test_description_filename = params.test_description_filename{t};

        figure
        h = histogram(classification_properties.GAD_7(test_valid_gad_7), (params_q.GAD_7.min - 0.5):1:(params_q.GAD_7.max + 0.5));
        
        set(h, 'FaceColor', params.clr_cohort(t, :));
        set(h, 'FaceAlpha', 1);
        
        set(gca, 'xlim', [(params_q.GAD_7.min - 0.5) (params_q.GAD_7.max + 0.5)]);
        y_lim = get(gca, 'ylim');
        hist_max = max(hist_max, y_lim(2));
        y_lim(2) = hist_max;
        set(gca, 'ylim', y_lim);

        title(sprintf('GAD-7 Distribution (%s)', this_test_description), label_opts{:});
        if show_x_label
            xlabel('GAD-7 score', label_opts{:});
        end
        if show_y_label
            ylabel('Labelled weeks', label_opts{:});
        end

        if latex_ticks
            set(gca, 'TickLabelInterpreter', 'latex')
        end

        if plot_threshold
            this_threshold = params.gad_7_threshold;
            this_threshold_x = this_threshold - 0.5;
            
            this_threshold_y_min = max(h.Values(find(h.BinEdges <= this_threshold_x, 2, 'last') - 1));
            this_threshold_y_max = hist_max;
            
            this_threshold_y_min = this_threshold_y_min + ceil(hist_max / 40);
            this_threshold_y_max = this_threshold_y_max - ceil(hist_max / 40);
            
            hold on;
            plot([1 1] * this_threshold_x, [this_threshold_y_max this_threshold_y_min], '--', 'Color', [0.5 0.5 0.5], 'LineWidth', 3);
            hold off;
        end

        prepare_and_save_figure('gad-7', sprintf('gad_7_distribution_%s', this_test_description_filename), 'FontSize', 30, varargin{:});
        
        idx_below_threshold = classification_properties.GAD_7 < params.gad_7_threshold;
        idx_above_threshold = classification_properties.GAD_7 >= params.gad_7_threshold;
        
        participants_below_threshold = numel(unique(classification_properties.participant(test_valid_gad_7 & idx_below_threshold)));
        participants_above_threshold = numel(unique(classification_properties.participant(test_valid_gad_7 & idx_above_threshold)));
        participants_total = numel(unique(classification_properties.participant(test_valid_gad_7)));
        
        weeks_below_threshold = sum(test_valid_gad_7 & idx_below_threshold);
        weeks_above_threshold = sum(test_valid_gad_7 & idx_above_threshold);
        weeks_total = sum(test_valid_gad_7);
        
        pad_space = '';
        if params.gad_7_threshold < 10
            pad_space = ' ';
        end
        fprintf('%s GAD-7 < %i: %s %5i  (%.4f)  (From %i Participants)\n', this_test_description, params.gad_7_threshold, pad_space, weeks_below_threshold, weeks_below_threshold / weeks_total, participants_below_threshold);
        fprintf('%s GAD-7 %s %i: %s %5i  (%.4f)  (From %i Participants)\n', this_test_description, char(8805), params.gad_7_threshold, pad_space, weeks_above_threshold, weeks_above_threshold / weeks_total, participants_above_threshold);
        fprintf('%s GAD-7 Total: %5i            (From %i Participants)\n', this_test_description, weeks_total, participants_total);
    end

    for j = 2:-1:1
        figure
        legend_strings = cell(j + 2, 1);
        filename_string = '';

        for i = 0:j
            iter_valid_gad_7 = valid_gad_7 & ismember(classification_properties.class, i:j);

            h = histogram(classification_properties.GAD_7(iter_valid_gad_7), (params_q.GAD_7.min - 0.5):1:(params_q.GAD_7.max + 0.5));

            set(h, 'FaceColor', params.clr_cohort(i + 2, :));
            set(h, 'FaceAlpha', 1);

            if i == 0
                max_values = h.Values;
            end

            legend_strings{i + 1} = params.test_description{i + 2};
            
            filename_string = sprintf('%s_%s', filename_string, params.test_description_filename{i + 2});

            if i == 0
                hold on;
            end
        end

%         iter_valid_gad_7 = valid_gad_7 & ismember(classification_properties.class, 0:j);
%         
%         if j == 2
%             clr_total = params.clr_cohort(1, :);
%         else
%             clr_total = params.clr_cohort(5, :);
%         end
%         histogram(classification_properties.GAD_7(iter_valid_gad_7), (params_q.GAD_7.min - 0.5):1:(params_q.GAD_7.max + 0.5), 'DisplayStyle', 'stairs', 'EdgeColor', clr_total);
%         legend_strings{j + 2} = 'Total';
        hold off;

        if show_legend
            legend(legend_strings(1:end-1), label_opts{:});
        end

        set(gca, 'xlim', [(params_q.GAD_7.min - 0.5) (params_q.GAD_7.max + 0.5)]);
        y_lim = get(gca, 'ylim');
        hist_max = max(hist_max, y_lim(2));
        y_lim(2) = hist_max;
        set(gca, 'ylim', y_lim);

        title(sprintf('GAD-7 Distribution Stacked'), label_opts{:});
        if show_x_label
            xlabel('GAD-7 score', label_opts{:});
        end
        if show_y_label
            ylabel('Labelled weeks', label_opts{:});
        end

        if latex_ticks
            set(gca, 'TickLabelInterpreter', 'latex')
        end

        if plot_threshold
            this_threshold = params.gad_7_threshold;
            this_threshold_x = this_threshold - 0.5;
            
            this_threshold_y_min = max(max_values(find(h.BinEdges <= this_threshold_x, 2, 'last') - 1));
            this_threshold_y_max = hist_max;
            
            this_threshold_y_min = this_threshold_y_min + ceil(hist_max / 40);
            this_threshold_y_max = this_threshold_y_max - ceil(hist_max / 40);
            
            hold on;
            plot([1 1] * this_threshold_x, [this_threshold_y_max this_threshold_y_min], '--', 'Color', [0.5 0.5 0.5], 'LineWidth', 3);
            hold off;
        end

        prepare_and_save_figure('gad-7', sprintf('gad_7_distribution_stacked%s', filename_string), 'FontSize', 16, varargin{:});
    end
end
