function extract_battery_discharges(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Jun-2015

    files = get_participants(varargin{:});
    
    for file=files'
        fprintf('Extracting battery discharges for %s\n', file.name)
        
        data = load_battery_data(file.name, varargin{:});

        if ~isempty(data)
            % In case there is overlap between different data files. Usually
            % this would not be required.
            battery_ds = downsample_second(data);

            data_diff = diff([battery_ds.data; battery_ds.data(end)]);
            time_diff = diff([battery_ds.time; battery_ds.time(end)]);

            max_idx = (battery_ds.data > 0.995);
            last_max_idx = find(diff([max_idx; 1]) < 0);

            battery_discharges = struct();
            battery_discharges.cycles = cell(length(last_max_idx), 1);
            battery_discharges.start = struct('time', battery_ds.time(last_max_idx), 'data', battery_ds.data(last_max_idx));
            battery_discharges.raw = struct('time', NaN(size(battery_ds.time)), 'data', NaN(size(battery_ds.data)));

            j = 1;

            for i = 1:length(last_max_idx)
                idx_min = last_max_idx(i);
                if i < length(last_max_idx)
                    idx_max = last_max_idx(i + 1);
                else
                    idx_max = length(battery_ds.data);
                end

                idx_first_increase = idx_min + find(data_diff(idx_min:idx_max) > 0 | time_diff(idx_min:idx_max) > 0.05, 1, 'first') - 1;

                if isempty(idx_first_increase)
                    idx_first_increase = idx_max;
                end

                battery_discharges.cycles{i} = struct();

                this_discharge = (data_diff(idx_min:idx_first_increase) < 0);

                battery_discharges.cycles{i}.time = battery_ds.time(idx_min:idx_first_increase);
                battery_discharges.cycles{i}.time = battery_discharges.cycles{i}.time(this_discharge);
                battery_discharges.cycles{i}.data = battery_ds.data(idx_min:idx_first_increase);
                battery_discharges.cycles{i}.data = battery_discharges.cycles{i}.data(this_discharge);

                battery_discharges.raw.time(j:(j + sum(this_discharge) - 1)) = battery_discharges.cycles{i}.time;
                battery_discharges.raw.data(j:(j + sum(this_discharge) - 1)) = battery_discharges.cycles{i}.data;

                j = j + sum(this_discharge) + 1;

                battery_discharges.cycles{i}.time = battery_discharges.cycles{i}.time - battery_discharges.cycles{i}.time(1);
            end
            
            save_data([file.name '-battery-discharges'], battery_discharges, varargin{:});
        else
            fprintf('  No data loaded\n');
        end
    end
end
