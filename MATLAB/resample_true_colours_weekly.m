function resample_true_colours_weekly(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 22-Feb-2016

    close all;
    
    for file = get_participants(varargin{:}, 'prefer_working', 0)'
        fprintf('Resampling True Colours for %s\n', file.name)

        data = load_true_colours_data(file.name, varargin{:});
        
        if numel(fieldnames(data)) == 0
            fprintf('  No data loaded.\n');
            continue;
        end

        fprintf('  Resampling data: ');

        tic;
        
        p_resampled = struct();

        for questionnaire_cell = fieldnames(data)'
            [p_time, sort_idx] = sort(data.(questionnaire_cell{:}).time);
            p_data = data.(questionnaire_cell{:}).data(sort_idx);
            
            weeks_start = floor(min(p_time));
            weeks_start = (weeks_start - 1) - weekday(weeks_start - 1) + 2; % Start on the previous Monday
            weeks_end = ceil(max(p_time));
            
            q_resampled = struct();
            q_resampled.time = ((0:floor((weeks_end - weeks_start) / 7))' * 7) + weeks_start;
            q_resampled.data = NaN(size(q_resampled.time));
            
            for w = 1:numel(q_resampled.time)
                this_week_start = q_resampled.time(w);

                valid_q = (p_time >= (this_week_start - 14)) & ...
                          (p_time <= (this_week_start + 7 + 14)) & ...
                          (p_data > -1);

                valid_p_time = p_time(valid_q);
                valid_q_data = p_data(valid_q);

                while (numel(valid_p_time) > 1) && sum(diff(valid_p_time) < (1 / (24 * 60))) > 0
                    i_equal = find(diff(valid_p_time) < (1 / (24 * 60)));
                    if numel(i_equal) > 1
                        i_last_equal = i_equal(find([diff(i_equal); 2] > 1, 1, 'first')) + 1;
                    else
                        i_last_equal = i_equal + 1;
                    end

                    valid_q_data(i_equal(1)) = mean(valid_q_data(i_equal(1):i_last_equal));

                    if i_last_equal == numel(valid_p_time)
                        r_valid = 1:i_equal(1);
                    else
                        r_valid = [1:i_equal(1) (i_last_equal + 1):numel(valid_p_time)];
                    end

                    valid_q_data = valid_q_data(r_valid);
                    valid_p_time = valid_p_time(r_valid);
                end
                
                if (numel(valid_p_time) == 1) && ...
                        (valid_p_time <= (this_week_start + 7 + 3.5)) && ...
                        (valid_p_time >= (this_week_start - 3.5))

                    q_resampled.data(w) = valid_q_data;
                elseif (numel(valid_p_time) > 1) && ...
                        (valid_p_time(1) >= (this_week_start + 7)) && ...
                        (valid_p_time(1) <= (this_week_start + 7 + 3.5))

                    q_resampled.data(w) = valid_q_data(1);
                elseif (numel(valid_p_time) > 1) && ...
                        (valid_p_time(end) <= this_week_start) && ...
                        (valid_p_time(end) >= (this_week_start - 3.5))

                    q_resampled.data(w) = valid_q_data(end);
                elseif (numel(valid_p_time) > 1) && ...
                          (valid_p_time(1) <= (this_week_start + 7 + 7)) && ...
                          (valid_p_time(end) >= (this_week_start - 3.5))

                    interp_time = (0:0.5:7) + this_week_start;

                    interp_time = interp_time(interp_time >= valid_p_time(1) & interp_time <= valid_p_time(end));

                    if valid_p_time(1) > this_week_start
                        interp_time = [valid_p_time(1), interp_time]; %#ok<AGROW>
                    end

                    if valid_p_time(end) < this_week_start + 7
                        interp_time = [interp_time, valid_p_time(end)]; %#ok<AGROW>
                    end

                    qids_val = interp1(valid_p_time, valid_q_data, interp_time, 'linear');

                    q_resampled.data(w) = mean(qids_val);
                end
            end
            
            p_resampled.(questionnaire_cell{:}) = q_resampled;
        end
        
        data = p_resampled;
        
        toc;

        fprintf('  Saving true-colours-resampled.mat: ');

        tic;
        save_data([file.name '-true-colours-resampled'], data, varargin{:});
        toc;
    end
end
