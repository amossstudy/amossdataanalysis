function [params, results, cp, predictions] = preprocess_classification_data_location_v2(classification_data, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 07-Oct-2015

    preprocess_loo_fs_raw = get_argument({'preprocess_loo_fs_raw'}, false, varargin{:});
    
    params = struct();
    cp = struct();
    results = struct();
    if nargout >= 4
        predictions = struct();
    end
    
    has_roc_data = struct();
    roc_data = struct();
    
    params.Q = classification_data.parameters.q;
    params.TESTS = classification_data.parameters.tests;
    params.FEATURES = classification_data.parameters.features;
    
    params.REPS = classification_data.options.reps;
    params.TESTS_TO_PERFORM = classification_data.options.tests_performed;
    params.Q_TO_TEST = classification_data.options.q_tested;
    params.Q_EXTRA_TO_TEST = classification_data.options.q_extra_tested;
    
    params.Q_MIN = min(params.Q);
    params.Q_MAX = max(params.Q);
    
    q_extra = classification_data.meta.q_extra;
    
    prediction_types = fieldnames(classification_data.results.predictions);
    
    params.Q_EXTRA_MAX = size(classification_data.results.predictions.(prediction_types{1}), 2) - numel(params.Q);
    
    ROC_POINTS = 101;
    
    prediction_types_no_reps = {'full', 'loo'};
    prediction_types_with_reps = {'loo_eq', 'ind_features', 'pca'};
    prediction_types_fs = {'loo_fs', 'cv_5f_fs', 'cv_10f_fs', 'cv_3f_fs', 'time_fs'};
    prediction_types_fs_raw = strcat(prediction_types_fs, '_raw');
    prediction_types_all = [prediction_types_no_reps, prediction_types_fs, prediction_types_with_reps];
    
    params_features = get_configuration('features_location_v2');
    
    for this_prediction_type = prediction_types_all
        if ismember(this_prediction_type, prediction_types)
            this_prediction_type_subtypes = this_prediction_type;
            cp_size = size(classification_data.results.predictions.(this_prediction_type{:}));
            if ismember(this_prediction_type, prediction_types_fs)
                if preprocess_loo_fs_raw
                    if nargout >= 4
                        predictions.([this_prediction_type{:} '_raw']) = cell(cp_size);
                    end
                    
                    cp_size_raw = [cp_size params.REPS];
                    
                    cp.([this_prediction_type{:} '_raw']) = cell(cp_size_raw);
                    results.([this_prediction_type{:} '_raw']) = struct();
                    
                    this_prediction_type_subtypes = [this_prediction_type_subtypes, [this_prediction_type{:} '_raw']]; %#ok<AGROW>
                end
                
                cp_size = cp_size([1 2 4]);
            end
            
            has_roc_data.(this_prediction_type{:}) = false;
            roc_data.(this_prediction_type{:}) = cell(cp_size);
            
            if nargout >= 4
                predictions.(this_prediction_type{:}) = cell(cp_size);
            end
            
            if ismember(this_prediction_type, [prediction_types_fs, prediction_types_with_reps])
                cp_size = [cp_size params.REPS]; %#ok<AGROW>
            end
            
            cp.(this_prediction_type{:}) = cell(cp_size);
            results.(this_prediction_type{:}) = struct();
            
            for this_prediction_subtype = this_prediction_type_subtypes
                switch this_prediction_subtype{:}
                    case 'full'
                        results.(this_prediction_subtype{:}).title = 'Full data set';
                        results.(this_prediction_subtype{:}).title_short = 'Full data set';
                    case 'loo'
                        results.(this_prediction_subtype{:}).title = 'Leave one participant out';
                        results.(this_prediction_subtype{:}).title_short = 'Leave one participant out';
                    case {'loo_eq', 'loo_fs', 'loo_fs_raw'}
                        results.(this_prediction_subtype{:}).title = sprintf('Leave one participant out, equalised training data, %i reps', params.REPS);
                        results.(this_prediction_subtype{:}).title_short = 'Cumulative features';
                    case {'cv_5f_fs', 'cv_5f_fs_raw'}
                        results.(this_prediction_subtype{:}).title = sprintf('5-fold cross validation, equalised training data, %i reps', params.REPS);
                        results.(this_prediction_subtype{:}).title_short = 'Cumulative features';
                    case {'cv_10f_fs', 'cv_10f_fs_raw'}
                        results.(this_prediction_subtype{:}).title = sprintf('10-fold cross validation, equalised training data, %i reps', params.REPS);
                        results.(this_prediction_subtype{:}).title_short = 'Cumulative features';
                    case {'cv_3f_fs', 'cv_3f_fs_raw', 'time_fs', 'time_fs_raw'}
                        results.(this_prediction_subtype{:}).title = sprintf('3-fold cross validation, equalised training data, %i reps', params.REPS);
                        results.(this_prediction_subtype{:}).title_short = 'Cumulative features';
                    case 'ind_features'
                        results.(this_prediction_subtype{:}).title = sprintf('Leave one participant out, individual features, %i reps', params.REPS);
                        results.(this_prediction_subtype{:}).title_short = 'Individual features';
                    case 'pca'
                        results.(this_prediction_subtype{:}).title = sprintf('Leave one participant out, PCA, %i reps', params.REPS);
                        results.(this_prediction_subtype{:}).title_short = 'PCA';
                end
            end
            
            results_size_raw = size(classification_data.results.predictions.(this_prediction_type{:}));

            if ismember(this_prediction_subtype, prediction_types_fs)
                feature_rank_size = results_size_raw([1 2 4]);
                feature_rank_size(3) = size(classification_data.results.feature_rank, numel(size(classification_data.results.feature_rank)));
            else
                feature_rank_size = results_size_raw;
            end
            
            results.(this_prediction_subtype{:}).feature_rank = NaN(feature_rank_size);
        end
    end
    
    results.meta = classification_data.meta;
    
    for t = params.TESTS_TO_PERFORM
        for q = find(ismember([params.Q, q_extra{t}], [params.Q_TO_TEST, params.Q_MAX + params.Q_EXTRA_TO_TEST]))
            if results.meta.q_valid(t, q)
                for this_prediction_type = prediction_types_fs
                    if isfield(cp, this_prediction_type)
                        if numel(size(classification_data.results.feature_rank)) == 2
                            if find(params.TESTS_TO_PERFORM == t) < numel(params.TESTS_TO_PERFORM)
                                this_feature_rank = NaN(1, size(classification_data.results.predictions.(this_prediction_type{:}), 3));

                                for i = 2:size(classification_data.results.predictions.(this_prediction_type{:}), 3)
                                    for j = 1:size(classification_data.results.predictions.(this_prediction_type{:}), 4)
                                        if sum(this_feature_rank == j) == 0
                                            if ~isstruct(classification_data.results.predictions.(this_prediction_type{:}){t, q, i, j});
                                                this_feature_rank(i - 1) = j;
                                                break;
                                            end
                                        end
                                    end
                                end

                                results.(this_prediction_type{:}).feature_rank(t, q, :) = this_feature_rank;
                                
                                warning('Could only determine feature rank of first %i selected features in classification of %s participants.', size(this_feature_rank, 2) - 1, params_features.test_description{t});
                            else
                                results.(this_prediction_type{:}).feature_rank(t, q, :) = classification_data.results.feature_rank(q, :);
                            end
                        elseif numel(size(classification_data.results.feature_rank)) == 3
                            results.(this_prediction_type{:}).feature_rank(t, q, :) = classification_data.results.feature_rank(t, q, :);
                        end
                    end
                end
                
                classes = results.meta.participant_class_q{t}(q, :);
                
                qids = classification_data.participants.qids(results.meta.participant_idx{t});
                
                valid_participants = true(size(classes));
                
                if t == 6
                    valid_participants = (results.meta.participant_class_cohort{t} == 1);
                    
                    classes = classes(valid_participants);
                    qids = qids(valid_participants);
                end
                
                classes_unique = unique(classes);
                class_count = numel(classes_unique);

                for f = 1:params.FEATURES
                    for this_prediction_type = prediction_types_no_reps
                        if isfield(cp, this_prediction_type)
                            if isstruct(classification_data.results.predictions.(this_prediction_type{:}){t, q, f})
                                this_pred = classification_data.results.predictions.(this_prediction_type{:}){t, q, f}.pred(valid_participants);
                                
                                if (class_count == 2)
                                    cp.(this_prediction_type{:}){t, q, f} = classperf(classes);
                                    classperf(cp.(this_prediction_type{:}){t, q, f}, this_pred);
                                elseif (class_count > 2)
                                    cp.(this_prediction_type{:}){t, q, f} = cell(class_count, 1);
                                    for c_i = 1:class_count
                                        cp.(this_prediction_type{:}){t, q, f}{c_i} = classperf(classes, 'Positive', classes_unique(c_i), 'Negative', classes_unique((1:class_count) ~= c_i));
                                        classperf(cp.(this_prediction_type{:}){t, q, f}{c_i}, this_pred);
                                    end
                                end
                                
                                if nargout >= 4
                                    predictions.(this_prediction_type{:}){t, q, f} = struct();
                                    predictions.(this_prediction_type{:}){t, q, f}.classes = classes';
                                    predictions.(this_prediction_type{:}){t, q, f}.qids = qids;
                                    predictions.(this_prediction_type{:}){t, q, f}.pred = this_pred;
                                end
                            end
                        end
                    end
                    
                    for this_prediction_type = prediction_types_with_reps
                        if isfield(cp, this_prediction_type)
                            if isstruct(classification_data.results.predictions.(this_prediction_type{:}){t, q, f})
                                if nargout >= 4
                                    predictions.(this_prediction_type{:}){t, q, f} = struct();
                                    predictions.(this_prediction_type{:}){t, q, f}.classes = classes';
                                    predictions.(this_prediction_type{:}){t, q, f}.qids = qids;
                                    predictions.(this_prediction_type{:}){t, q, f}.pred = NaN(numel(classes), params.REPS);
                                end
                                for r = 1:params.REPS
                                    this_pred = classification_data.results.predictions.(this_prediction_type{:}){t, q, f}.pred(valid_participants, r);
                                    
                                    if (class_count == 2)
                                        cp.(this_prediction_type{:}){t, q, f, r} = classperf(classes);
                                        classperf(cp.(this_prediction_type{:}){t, q, f, r}, this_pred);
                                    elseif (class_count > 2)
                                        cp.(this_prediction_type{:}){t, q, f, r} = cell(class_count, 1);
                                        for c_i = 1:class_count
                                            cp.(this_prediction_type{:}){t, q, f, r}{c_i} = classperf(classes, 'Positive', classes_unique(c_i), 'Negative', classes_unique((1:class_count) ~= c_i));
                                            classperf(cp.(this_prediction_type{:}){t, q, f, r}{c_i}, this_pred);
                                        end
                                    end
                                    
                                    if nargout >= 4
                                        predictions.(this_prediction_type{:}){t, q, f}.pred(:, r) = this_pred;
                                    end
                                end
                            end
                        end
                    end
                    
                    for this_prediction_type = prediction_types_fs;
                        if isfield(cp, this_prediction_type)
                            if f <= size(results.(this_prediction_type{:}).feature_rank, 3)
                                selected_feature = results.(this_prediction_type{:}).feature_rank(t, q, f);
                                if ~isnan(selected_feature) && isstruct(classification_data.results.predictions.(this_prediction_type{:}){t, q, f, selected_feature})
                                    if nargout >= 4
                                        predictions.(this_prediction_type{:}){t, q, f} = struct();
                                        predictions.(this_prediction_type{:}){t, q, f}.classes = classes';
                                        predictions.(this_prediction_type{:}){t, q, f}.qids = qids;
                                        predictions.(this_prediction_type{:}){t, q, f}.pred = NaN(numel(classes), params.REPS);
                                        predictions.(this_prediction_type{:}){t, q, f}.pred_prob = NaN(numel(classes), params.REPS, 2);
                                    end
                                    if isfield(classification_data.results.predictions.(this_prediction_type{:}){t, q, f, selected_feature}, 'pred_prob') && (class_count == 2)
                                        has_roc_data.(this_prediction_type{:}) = true;
                                        roc_data.(this_prediction_type{:}){t, q, f} = struct();
                                        roc_data.(this_prediction_type{:}){t, q, f}.tpr = NaN(ROC_POINTS, params.REPS);
                                        roc_data.(this_prediction_type{:}){t, q, f}.fpr = NaN(ROC_POINTS, params.REPS);
                                        roc_data.(this_prediction_type{:}){t, q, f}.tpr_all = NaN(ROC_POINTS, 1);
                                        roc_data.(this_prediction_type{:}){t, q, f}.fpr_all = NaN(ROC_POINTS, 1);
                                        roc_data.(this_prediction_type{:}){t, q, f}.pred_class_all = NaN(ROC_POINTS, numel(classes) * params.REPS);
                                        roc_data.(this_prediction_type{:}){t, q, f}.classes_all = NaN(ROC_POINTS, numel(classes) * params.REPS);
                                    end
                                    for r = 1:params.REPS
                                        this_pred = classification_data.results.predictions.(this_prediction_type{:}){t, q, f, selected_feature}.pred(valid_participants, r);
                                        
                                        if (class_count == 2)
                                            cp.(this_prediction_type{:}){t, q, f, r} = classperf(classes);
                                            classperf(cp.(this_prediction_type{:}){t, q, f, r}, this_pred);
                                        elseif (class_count > 2)
                                            cp.(this_prediction_type{:}){t, q, f, r} = cell(class_count, 1);
                                            for c_i = 1:class_count
                                                cp.(this_prediction_type{:}){t, q, f, r}{c_i} = classperf(classes, 'Positive', classes_unique(c_i), 'Negative', classes_unique((1:class_count) ~= c_i));
                                                classperf(cp.(this_prediction_type{:}){t, q, f, r}{c_i}, this_pred);
                                            end
                                        end
                                        
                                        if nargout >= 4
                                            predictions.(this_prediction_type{:}){t, q, f}.pred(:, r) = this_pred;
                                        end
                                        if has_roc_data.(this_prediction_type{:})
                                            roc_prior = linspace(0, 1, ROC_POINTS);

                                            pred_prob = classification_data.results.predictions.(this_prediction_type{:}){t, q, f, selected_feature}.pred_prob(valid_participants, :, r);

                                            for roc_i = 1:ROC_POINTS
                                                pred_class = (pred_prob(:, 2) >= roc_prior(roc_i)) + 1;

                                                roc_tpr = sum((pred_class == classes') & (classes' == 2)) / sum(classes' == 2);
                                                roc_fpr = sum((pred_class == 2) & (classes' == 1)) / sum(classes' == 1);

                                                roc_data.(this_prediction_type{:}){t, q, f}.tpr(roc_i, r) = roc_tpr;
                                                roc_data.(this_prediction_type{:}){t, q, f}.fpr(roc_i, r) = roc_fpr;
                                                
                                                roc_data.(this_prediction_type{:}){t, q, f}.pred_class_all(roc_i, ((r - 1) * numel(classes) + 1):(r * numel(classes))) = pred_class;
                                                roc_data.(this_prediction_type{:}){t, q, f}.classes_all(roc_i, ((r - 1) * numel(classes) + 1):(r * numel(classes))) = classes';
                                            end
                                            if nargout >= 4
                                                predictions.(this_prediction_type{:}){t, q, f}.pred_prob(:, r, :) = pred_prob;
                                            end
                                        end
                                    end
                                    if has_roc_data.(this_prediction_type{:})
                                        for roc_i = 1:ROC_POINTS
                                            pred_class_all = roc_data.(this_prediction_type{:}){t, q, f}.pred_class_all(roc_i, :);
                                            classes_all = roc_data.(this_prediction_type{:}){t, q, f}.classes_all(roc_i, :);
                                            
                                            roc_tpr = sum((pred_class_all == classes_all) & (classes_all == 2)) / sum(classes_all == 2);
                                            roc_fpr = sum((pred_class_all == 2) & (classes_all == 1)) / sum(classes_all == 1);

                                            roc_data.(this_prediction_type{:}){t, q, f}.tpr_all(roc_i) = roc_tpr;
                                            roc_data.(this_prediction_type{:}){t, q, f}.fpr_all(roc_i) = roc_fpr;
                                        end
                                    end
                                end
                            end
                        end
                        
                        this_prediction_subtype = [this_prediction_type{:} '_raw'];
                        
                        if isfield(cp, this_prediction_subtype)
                            for fn = 1:size(results.(this_prediction_type{:}).feature_rank, 3)
                                if isstruct(classification_data.results.predictions.(this_prediction_type{:}){t, q, fn, f})
                                    if nargout >= 4
                                        predictions.(this_prediction_subtype){t, q, fn, f} = struct();
                                        predictions.(this_prediction_subtype){t, q, fn, f}.classes = classes';
                                        predictions.(this_prediction_subtype){t, q, fn, f}.qids = qids;
                                        predictions.(this_prediction_subtype){t, q, fn, f}.pred = NaN(numel(classes), params.REPS);
                                    end
                                    for r = 1:params.REPS
                                        this_pred = classification_data.results.predictions.(this_prediction_type{:}){t, q, fn, f}.pred(valid_participants, r);
                                        
                                        if (class_count == 2)
                                            cp.(this_prediction_subtype){t, q, fn, f, r} = classperf(classes);
                                            classperf(cp.(this_prediction_subtype){t, q, fn, f, r}, this_pred);
                                        elseif (class_count > 2)
                                            cp.(this_prediction_subtype){t, q, fn, f, r} = cell(class_count, 1);
                                            for c_i = 1:class_count
                                                cp.(this_prediction_subtype){t, q, fn, f, r}{c_i} = classperf(classes, 'Positive', classes_unique(c_i), 'Negative', classes_unique((1:class_count) ~= c_i));
                                                classperf(cp.(this_prediction_subtype){t, q, fn, f, r}{c_i}, this_pred);
                                            end
                                        end
                                        
                                        if nargout >= 4
                                            predictions.(this_prediction_subtype){t, q, fn, f}.pred(:, r) = this_pred;
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    
    for this_prediction_type = prediction_types_all
        if ismember(this_prediction_type, prediction_types)
            results_size_raw = size(classification_data.results.predictions.(this_prediction_type{:}));
            this_prediction_type_subtypes = this_prediction_type;
            if ismember(this_prediction_type, prediction_types_fs) && preprocess_loo_fs_raw
                this_prediction_type_subtypes = [this_prediction_type_subtypes, [this_prediction_type{:} '_raw']]; %#ok<AGROW>
            end
            for this_prediction_subtype = this_prediction_type_subtypes
                if ismember(this_prediction_subtype, prediction_types_fs)
                    results_size = results_size_raw([1 2 4]);
                else
                    results_size = results_size_raw;
                end
                results.(this_prediction_subtype{:}).classes_unique = classes_unique;
                results.(this_prediction_subtype{:}).accuracy    = NaN(results_size);
                results.(this_prediction_subtype{:}).sensitivity = NaN(results_size);
                results.(this_prediction_subtype{:}).specificity = NaN(results_size);
                results.(this_prediction_subtype{:}).f1          = NaN(results_size);
                if (class_count > 2)
                    results_size_class = [results_size class_count];
                    results.(this_prediction_subtype{:}).sensitivity_class = NaN(results_size_class);
                    results.(this_prediction_subtype{:}).specificity_class = NaN(results_size_class);
                    results.(this_prediction_subtype{:}).f1_class          = NaN(results_size_class);
                end
                if ismember(this_prediction_type, [prediction_types_fs, prediction_types_with_reps])
                    results_size_reps = [results_size params.REPS];

                    results.(this_prediction_subtype{:}).accuracy_raw    = NaN(results_size_reps);
                    results.(this_prediction_subtype{:}).accuracy_std    = NaN(results_size);
                    results.(this_prediction_subtype{:}).sensitivity_raw = NaN(results_size_reps);
                    results.(this_prediction_subtype{:}).sensitivity_std = NaN(results_size);
                    results.(this_prediction_subtype{:}).specificity_raw = NaN(results_size_reps);
                    results.(this_prediction_subtype{:}).specificity_std = NaN(results_size);
                    results.(this_prediction_subtype{:}).f1_raw          = NaN(results_size_reps);
                    results.(this_prediction_subtype{:}).f1_std          = NaN(results_size);
                    
                    if (class_count > 2)
                        results_size_reps_class = [results_size_reps class_count];
                        results.(this_prediction_subtype{:}).sensitivity_class_raw = NaN(results_size_reps_class);
                        results.(this_prediction_subtype{:}).sensitivity_class_std = NaN(results_size_class);
                        results.(this_prediction_subtype{:}).specificity_class_raw = NaN(results_size_reps_class);
                        results.(this_prediction_subtype{:}).specificity_class_std = NaN(results_size_class);
                        results.(this_prediction_subtype{:}).f1_class_raw          = NaN(results_size_reps_class);
                        results.(this_prediction_subtype{:}).f1_class_std          = NaN(results_size_class);
                    end
                    
                    if isfield(has_roc_data, this_prediction_subtype{:}) && has_roc_data.(this_prediction_subtype{:})
                        results.(this_prediction_subtype{:}).roc         = struct();
                        results.(this_prediction_subtype{:}).roc.tpr     = cell(results_size);
                        results.(this_prediction_subtype{:}).roc.fpr     = cell(results_size);
                        results.(this_prediction_subtype{:}).roc.tpr_all = cell(results_size);
                        results.(this_prediction_subtype{:}).roc.fpr_all = cell(results_size);
                    end
                end
            end
        end
    end
    
    for t = params.TESTS_TO_PERFORM
        for q = find(results.meta.q_valid(t, :))
            for f = 1:params.FEATURES
                for this_prediction_type = prediction_types_no_reps
                    if isfield(cp, this_prediction_type)
                        if ~isempty(cp.(this_prediction_type{:}){t, q, f})
                            this_cp = cp.(this_prediction_type{:}){t, q, f};
                            accuracy_raw    = 0;
                            sensitivity_raw = 0;
                            specificity_raw = 0;
                            f1_raw          = 0;
                            if (class_count == 2)
                                accuracy_raw    = this_cp.CorrectRate;
                                sensitivity_raw = this_cp.Sensitivity;
                                specificity_raw = this_cp.Specificity;
                                f1_raw          = 2 * (specificity_raw * sensitivity_raw) / (specificity_raw + sensitivity_raw);
                            elseif (class_count > 2)
                                accuracy_raw    = this_cp{1}.CorrectRate;
                                for c_i = 1:class_count
                                    results.(this_prediction_type{:}).sensitivity_class(t, q, f, c_i) = this_cp{c_i}.Sensitivity;
                                    results.(this_prediction_type{:}).specificity_class(t, q, f, c_i) = this_cp{c_i}.Specificity;
                                    results.(this_prediction_type{:}).f1_class(t, q, f, c_i) = 2 * (this_cp{c_i}.Specificity * this_cp{c_i}.Sensitivity) / (this_cp{c_i}.Specificity + this_cp{c_i}.Sensitivity);
                                    
                                    sensitivity_raw = sensitivity_raw + this_cp{c_i}.Sensitivity;
                                    specificity_raw = specificity_raw + this_cp{c_i}.Specificity;
                                    f1_raw          = f1_raw + results.(this_prediction_type{:}).f1_class(t, q, f, c_i);
                                end
                                sensitivity_raw = sensitivity_raw / class_count;
                                specificity_raw = specificity_raw / class_count;
                                f1_raw          = f1_raw / class_count;
                            end
                            results.(this_prediction_type{:}).accuracy(t, q, f)    = accuracy_raw;
                            results.(this_prediction_type{:}).sensitivity(t, q, f) = sensitivity_raw;
                            results.(this_prediction_type{:}).specificity(t, q, f) = specificity_raw;
                            results.(this_prediction_type{:}).f1(t, q, f)          = f1_raw;
                        end
                    end
                end
                
                for this_prediction_type = [prediction_types_fs, prediction_types_with_reps]
                    if isfield(cp, this_prediction_type)
                        for r = 1:params.REPS
                            if ~isempty(cp.(this_prediction_type{:}){t, q, f, r})
                                this_cp = cp.(this_prediction_type{:}){t, q, f, r};
                                accuracy_raw    = 0;
                                sensitivity_raw = 0;
                                specificity_raw = 0;
                                f1_raw          = 0;
                                if (class_count == 2)
                                    accuracy_raw    = this_cp.CorrectRate;
                                    sensitivity_raw = this_cp.Sensitivity;
                                    specificity_raw = this_cp.Specificity;
                                    f1_raw          = 2 * (specificity_raw * sensitivity_raw) / (specificity_raw + sensitivity_raw);
                                elseif (class_count > 2)
                                    accuracy_raw    = this_cp{1}.CorrectRate;
                                    for c_i = 1:class_count
                                        results.(this_prediction_type{:}).sensitivity_class_raw(t, q, f, r, c_i) = this_cp{c_i}.Sensitivity;
                                        results.(this_prediction_type{:}).specificity_class_raw(t, q, f, r, c_i) = this_cp{c_i}.Specificity;
                                        results.(this_prediction_type{:}).f1_class_raw(t, q, f, r, c_i) = 2 * (this_cp{c_i}.Specificity * this_cp{c_i}.Sensitivity) / (this_cp{c_i}.Specificity + this_cp{c_i}.Sensitivity);

                                        sensitivity_raw = sensitivity_raw + this_cp{c_i}.Sensitivity;
                                        specificity_raw = specificity_raw + this_cp{c_i}.Specificity;
                                        f1_raw          = f1_raw + results.(this_prediction_type{:}).f1_class_raw(t, q, f, r, c_i);
                                    end
                                    sensitivity_raw = sensitivity_raw / class_count;
                                    specificity_raw = specificity_raw / class_count;
                                    f1_raw          = f1_raw / class_count;
                                end
                                results.(this_prediction_type{:}).accuracy_raw(t, q, f, r)    = accuracy_raw;
                                results.(this_prediction_type{:}).sensitivity_raw(t, q, f, r) = sensitivity_raw;
                                results.(this_prediction_type{:}).specificity_raw(t, q, f, r) = specificity_raw;
                                results.(this_prediction_type{:}).f1_raw(t, q, f, r)          = f1_raw;
                            end
                        end

                        results.(this_prediction_type{:}).accuracy(t, q, f)        = nanmean(results.(this_prediction_type{:}).accuracy_raw(t, q, f, :));
                        results.(this_prediction_type{:}).accuracy_std(t, q, f)    = nanstd(results.(this_prediction_type{:}).accuracy_raw(t, q, f, :));
                        results.(this_prediction_type{:}).sensitivity(t, q, f)     = nanmean(results.(this_prediction_type{:}).sensitivity_raw(t, q, f, :));
                        results.(this_prediction_type{:}).sensitivity_std(t, q, f) = nanstd(results.(this_prediction_type{:}).sensitivity_raw(t, q, f, :));
                        results.(this_prediction_type{:}).specificity(t, q, f)     = nanmean(results.(this_prediction_type{:}).specificity_raw(t, q, f, :));
                        results.(this_prediction_type{:}).specificity_std(t, q, f) = nanstd(results.(this_prediction_type{:}).specificity_raw(t, q, f, :));
                        results.(this_prediction_type{:}).f1(t, q, f)              = nanmean(results.(this_prediction_type{:}).f1_raw(t, q, f, :));
                        results.(this_prediction_type{:}).f1_std(t, q, f)          = nanstd(results.(this_prediction_type{:}).f1_raw(t, q, f, :));
                        
                        if (class_count > 2)
                            for c_i = 1:class_count
                                results.(this_prediction_type{:}).sensitivity_class(t, q, f, c_i)     = nanmean(results.(this_prediction_type{:}).sensitivity_class_raw(t, q, f, :, c_i));
                                results.(this_prediction_type{:}).sensitivity_class_std(t, q, f, c_i) = nanstd(results.(this_prediction_type{:}).sensitivity_class_raw(t, q, f, :, c_i));
                                results.(this_prediction_type{:}).specificity_class(t, q, f, c_i)     = nanmean(results.(this_prediction_type{:}).specificity_class_raw(t, q, f, :, c_i));
                                results.(this_prediction_type{:}).specificity_class_std(t, q, f, c_i) = nanstd(results.(this_prediction_type{:}).specificity_class_raw(t, q, f, :, c_i));
                                results.(this_prediction_type{:}).f1_class(t, q, f, c_i)              = nanmean(results.(this_prediction_type{:}).f1_class_raw(t, q, f, :, c_i));
                                results.(this_prediction_type{:}).f1_class_std(t, q, f, c_i)          = nanstd(results.(this_prediction_type{:}).f1_class_raw(t, q, f, :, c_i));
                            end
                        end
                        
                        if isfield(has_roc_data, this_prediction_subtype{:}) && has_roc_data.(this_prediction_subtype{:})
                            if isstruct(roc_data.(this_prediction_type{:}){t, q, f})
                                results.(this_prediction_type{:}).roc.points = ROC_POINTS;
                                results.(this_prediction_type{:}).roc.tpr{t, q, f}     = roc_data.(this_prediction_type{:}){t, q, f}.tpr;
                                results.(this_prediction_type{:}).roc.fpr{t, q, f}     = roc_data.(this_prediction_type{:}){t, q, f}.fpr;
                                results.(this_prediction_type{:}).roc.tpr_all{t, q, f} = roc_data.(this_prediction_type{:}){t, q, f}.tpr_all;
                                results.(this_prediction_type{:}).roc.fpr_all{t, q, f} = roc_data.(this_prediction_type{:}){t, q, f}.fpr_all;
                            end
                        end
                    end
                end
                
                for this_prediction_type = prediction_types_fs_raw;
                    if isfield(cp, this_prediction_type)
                        for fn = 1:size(results.(this_prediction_type{:}).feature_rank, 3)
                            for r = 1:params.REPS
                                if ~isempty(cp.(this_prediction_type{:}){t, q, fn, f, r})
                                    this_cp = cp.(this_prediction_type{:}){t, q, fn, f, r};
                                    accuracy_raw    = 0;
                                    sensitivity_raw = 0;
                                    specificity_raw = 0;
                                    f1_raw          = 0;
                                    if (class_count == 2)
                                        accuracy_raw    = this_cp.CorrectRate;
                                        sensitivity_raw = this_cp.Sensitivity;
                                        specificity_raw = this_cp.Specificity;
                                        f1_raw          = 2 * (specificity_raw * sensitivity_raw) / (specificity_raw + sensitivity_raw);
                                    elseif (class_count > 2)
                                        accuracy_raw    = this_cp{1}.CorrectRate;
                                        for c_i = 1:class_count
                                            results.(this_prediction_type{:}).sensitivity_class_raw(t, q, fn, f, r, c_i) = this_cp{c_i}.Sensitivity;
                                            results.(this_prediction_type{:}).specificity_class_raw(t, q, fn, f, r, c_i) = this_cp{c_i}.Specificity;
                                            results.(this_prediction_type{:}).f1_class_raw(t, q, fn, f, r, c_i) = 2 * (this_cp{c_i}.Specificity * this_cp{c_i}.Sensitivity) / (this_cp{c_i}.Specificity + this_cp{c_i}.Sensitivity);

                                            sensitivity_raw = sensitivity_raw + this_cp{c_i}.Sensitivity;
                                            specificity_raw = specificity_raw + this_cp{c_i}.Specificity;
                                            f1_raw          = f1_raw + results.(this_prediction_type{:}).f1_class_raw(t, q, fn, f, r, c_i);
                                        end
                                        sensitivity_raw = sensitivity_raw / class_count;
                                        specificity_raw = specificity_raw / class_count;
                                        f1_raw          = f1_raw / class_count;
                                    end
                                    results.(this_prediction_type{:}).accuracy_raw(t, q, fn, f, r)    = accuracy_raw;
                                    results.(this_prediction_type{:}).sensitivity_raw(t, q, fn, f, r) = sensitivity_raw;
                                    results.(this_prediction_type{:}).specificity_raw(t, q, fn, f, r) = specificity_raw;
                                    results.(this_prediction_type{:}).f1_raw(t, q, fn, f, r)          = f1_raw;
                                end
                            end

                            results.(this_prediction_type{:}).accuracy(t, q, fn, f)        = nanmean(results.(this_prediction_type{:}).accuracy_raw(t, q, fn, f, :));
                            results.(this_prediction_type{:}).accuracy_std(t, q, fn, f)    = nanstd(results.(this_prediction_type{:}).accuracy_raw(t, q, fn, f, :));
                            results.(this_prediction_type{:}).sensitivity(t, q, fn, f)     = nanmean(results.(this_prediction_type{:}).sensitivity_raw(t, q, fn, f, :));
                            results.(this_prediction_type{:}).sensitivity_std(t, q, fn, f) = nanstd(results.(this_prediction_type{:}).sensitivity_raw(t, q, fn, f, :));
                            results.(this_prediction_type{:}).specificity(t, q, fn, f)     = nanmean(results.(this_prediction_type{:}).specificity_raw(t, q, fn, f, :));
                            results.(this_prediction_type{:}).specificity_std(t, q, fn, f) = nanstd(results.(this_prediction_type{:}).specificity_raw(t, q, fn, f, :));
                            results.(this_prediction_type{:}).f1(t, q, fn, f)              = nanmean(results.(this_prediction_type{:}).f1_raw(t, q, fn, f, :));
                            results.(this_prediction_type{:}).f1_std(t, q, fn, f)          = nanstd(results.(this_prediction_type{:}).f1_raw(t, q, fn, f, :));
                            
                            if (class_count > 2)
                                for c_i = 1:class_count
                                    results.(this_prediction_type{:}).sensitivity_class(t, q, fn, f, c_i)     = nanmean(results.(this_prediction_type{:}).sensitivity_class_raw(t, q, fn, f, :, c_i));
                                    results.(this_prediction_type{:}).sensitivity_class_std(t, q, fn, f, c_i) = nanstd(results.(this_prediction_type{:}).sensitivity_class_raw(t, q, fn, f, :, c_i));
                                    results.(this_prediction_type{:}).specificity_class(t, q, fn, f, c_i)     = nanmean(results.(this_prediction_type{:}).specificity_class_raw(t, q, fn, f, :, c_i));
                                    results.(this_prediction_type{:}).specificity_class_std(t, q, fn, f, c_i) = nanstd(results.(this_prediction_type{:}).specificity_class_raw(t, q, fn, f, :, c_i));
                                    results.(this_prediction_type{:}).f1_class(t, q, fn, f, c_i)              = nanmean(results.(this_prediction_type{:}).f1_class_raw(t, q, fn, f, :, c_i));
                                    results.(this_prediction_type{:}).f1_class_std(t, q, fn, f, c_i)          = nanstd(results.(this_prediction_type{:}).f1_class_raw(t, q, fn, f, :, c_i));
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
