function test_classification_feature_selection_location_v2(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 12-Nov-2015

    variant_out = get_argument({'variant_output', 'variantoutput', 'variant_out', 'variantout', 'output_variant', 'outputvariant'}, '', varargin{:});

    if ~isempty(variant_out)
        variant_out = ['_' variant_out];
    end
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    valid_qids = ~isnan(classification_properties.QIDS);
    
    data_full = data_raw(:, valid_qids)';
    
    participants = classification_properties.participant(valid_qids);
    participant_variant = classification_properties.variant(valid_qids);
    participant_week_start = classification_properties.week_start(valid_qids);
    participant_class = classification_properties.class(valid_qids);
    participant_qids = classification_properties.QIDS(valid_qids);
    
    TESTS = 7;
    TESTS_SUPPORTED = 1:TESTS;
    Q_MIN = 2;
    Q_MAX = 16;
    Q = Q_MIN:1:Q_MAX;
    Q_EXTRA_MAX = 1;
    FEATURES = size(data_raw, 1);
    MAX_FEATURE_COMB = get_argument({'max_feature_comb', 'maxfeaturecomb', 'max_features_to_test', 'maxfeaturestotest', 'max_features_to_select', 'maxfeaturestoselect'}, 20, varargin{:});
    SCORES = 4;
    
    CV_MODE = upper(get_argument({'cv_mode', 'cv_mode', 'cv'}, 'LOO', varargin{:}));
    assert(ismember(CV_MODE, {'LOO', 'CV_5F', 'CV_10F', 'CV_3F', 'TIME'}), 'Invalid cross validation mode: %s', CV_MODE);
    if strcmp(CV_MODE, 'TIME')
        CV_MODE = 'CV_3F';
    end
    CV = sprintf('%s_fs', lower(CV_MODE));
    
    TESTS_TO_PERFORM = get_argument({'run_tests', 'runtests'}, TESTS_SUPPORTED, varargin{:});
    Q_TO_TEST = get_argument({'test_q', 'testq'}, Q, varargin{:});
    Q_EXTRA_TO_TEST = get_argument({'test_q_extra', 'testqextra'}, Q_EXTRA_MAX, varargin{:});
    REPS = get_argument({'cv_reps', 'cvreps', 'reps'}, 10, varargin{:});
    
    CLASSIFICATION_METHOD = upper(get_argument({'classification_method', 'classificationmethod'}, 'LR', varargin{:}));
    
    assert(ismember(CLASSIFICATION_METHOD, ...
        {'LR', 'LR_LASSO', 'LDA', 'NB'}), ...
        sprintf('Invalid classification method: %s', CLASSIFICATION_METHOD));
    
    TRAIN_WITH_LASSO = get_argument({'train_lasso', 'trainlasso'}, strcmp(CLASSIFICATION_METHOD, 'LR_LASSO'), varargin{:});
    
    if strcmp(CLASSIFICATION_METHOD, 'LR') && TRAIN_WITH_LASSO
        warning(sprintf('Using LASSO with logistic regression classification method.\nConsider setting classification method to LR_LASSO.')); %#ok<SPWRN>
    end
        
    assert(~(~TRAIN_WITH_LASSO && strcmp(CLASSIFICATION_METHOD, 'LR_LASSO')), 'If classification method is set to logistic regression with LASSO then ''train_lasso'' option must be true or unspecified.');
    
    LASSO_CV_NUM_LAMBDA = get_argument({'lasso_cv', 'lasso_cv'}, 10, varargin{:});
    
    LDA_OPTS = {'DiscrimType', 'quadratic', 'Prior', 'uniform'};
    
    baseline = zeros(TESTS, numel(Q) + 1);
    
    q_extra = cell(TESTS, 1);
    
    q_valid = false(TESTS, numel(Q) + Q_EXTRA_MAX);
    
    predictions = struct();
    
    predictions.(CV) = cell(TESTS, numel(Q) + Q_EXTRA_MAX, MAX_FEATURE_COMB, FEATURES);
    
    feature_rank = NaN(numel(Q) + Q_EXTRA_MAX, MAX_FEATURE_COMB);
    
    classification_data = struct();
    classification_data.meta = struct();
    classification_data.meta.classes = cell(TESTS, 1);
    classification_data.meta.participant = cell(TESTS, 1);
    classification_data.meta.variant = cell(TESTS, 1);
    classification_data.meta.week_start = cell(TESTS, 1);
    classification_data.meta.participant_idx = cell(TESTS, 1);
    classification_data.meta.participant_class_cohort = cell(TESTS, 1);
    classification_data.meta.participant_class_q = cell(TESTS, 1);
    
    for t = TESTS_TO_PERFORM
        if t == 1
            test_participants_idx = true(size(participant_class));
        elseif (t >= 2) && (t <= 4)
            test_participants_idx = (participant_class == (t - 2));
        elseif t == 5 || t == 6
            test_participants_idx = ismember(participant_class, 0:1);
        elseif t == 7
            test_participants_idx = ismember(participant_class, 1:2);
        end
        
        test_data = data_full(test_participants_idx, :);
        test_participants = participants(test_participants_idx);
        test_variant = participant_variant(test_participants_idx);
        test_week_start = participant_week_start(test_participants_idx);
        test_participant_class = participant_class(test_participants_idx);
        test_participant_qids = participant_qids(test_participants_idx);
        
        test_train_idx = true(size(test_participant_class));
        if t == 6
            test_train_idx = (test_participant_class == 1);
        end
        
        test_unique_participant_classes = unique(test_participant_class);
        test_train_unique_participants = unique(test_participants(test_train_idx));
        
        if strcmp(CV_MODE, 'LOO')
            cv_factor = 1 / numel(test_train_unique_participants);
        elseif strcmp(CV_MODE, 'CV_5F')
            cv_factor = 0.2;
        elseif strcmp(CV_MODE, 'CV_10F')
            cv_factor = 0.1;
        elseif strcmp(CV_MODE, 'CV_3F')
            cv_factor = 1/3;
        end
        
        CV_REPS = ceil(numel(test_train_unique_participants) * cv_factor);
        
        classification_data.meta.classes{t} = test_unique_participant_classes;
        classification_data.meta.participant{t} = test_participants;
        classification_data.meta.variant{t} = test_variant;
        classification_data.meta.week_start{t} = test_week_start;
        classification_data.meta.participant_idx{t} = test_participants_idx;
        classification_data.meta.participant_class_cohort{t} = test_participant_class;
        classification_data.meta.participant_class_q{t} = NaN(numel(Q), sum(test_participants_idx));
        
        if numel(test_unique_participant_classes) > 1 && t ~= 6
            q_extra{t} = Q_MAX + 1;
        end
        
        for q = find(ismember([Q, q_extra{t}], [Q_TO_TEST, Q_MAX + Q_EXTRA_TO_TEST]))
            if q <= numel(Q)
                this_q_desc = sprintf('t = %i, Q = %i', t, Q(q));
                
                classes = (test_participant_qids >= Q(q)) + 1;
                
                class_count = 2;
            elseif q == (numel(Q) + 1)
                this_q_desc = sprintf('t = %i, cohort', t);
                
                classes = zeros(size(test_participant_class));
                
                class_count = numel(test_unique_participant_classes);
                
                for this_class_i = 1:class_count
                    classes(test_participant_class == test_unique_participant_classes(this_class_i)) = this_class_i;
                end
            end
            
            classification_data.meta.participant_class_q{t}(q, :) = classes;
            
            fprintf('%s', this_q_desc)
            
            total_class_count = histcounts(classes, (0:class_count) + 0.5);

            [min_total_class_count, min_total_class] = min(total_class_count);
            
            baseline(t, q) = max(total_class_count) / sum(total_class_count);

            if numel(total_class_count) > 1 && min_total_class_count >= 5 && numel(unique(test_participants(classes == min_total_class))) >= 2
                fprintf('\n')
                q_valid(t, q) = true;
                for fn = 1:MAX_FEATURE_COMB
                    valid_features = 1:FEATURES;
                    valid_features = valid_features(~ismember(valid_features, feature_rank(q, 1:(fn - 1))));
                    
                    feature_scores = NaN(FEATURES, SCORES);
                    
                    for f = valid_features
                        fprintf('%s, f = %i\n', this_q_desc, f);

                        predictions.(CV){t, q, fn, f}.qids = test_participant_qids;
                        predictions.(CV){t, q, fn, f}.class = classes;
                        predictions.(CV){t, q, fn, f}.pred = NaN(numel(test_participant_qids), REPS * CV_REPS);
                        predictions.(CV){t, q, fn, f}.pred_prob = NaN(numel(test_participant_qids), class_count, REPS * CV_REPS);
                        predictions.(CV){t, q, fn, f}.score = NaN(1, SCORES);

                        for r1 = 1:CV_REPS
                            i_retry_1 = 1;
                            
                            while i_retry_1 > 0
                                test_participant_grp = zeros(1, numel(test_participant_qids));
                                if ~strcmp(CV_MODE, 'CV_3F')
                                    train_participant_grp = ceil((1:numel(test_train_unique_participants)) / (numel(test_train_unique_participants) * cv_factor));
                                    train_participant_grp = train_participant_grp(randperm(numel(train_participant_grp)));
                                    for g = unique(train_participant_grp)
                                        test_participant_grp(ismember(test_participants, test_train_unique_participants(train_participant_grp == g))) = g;
                                    end
                                else
                                    for g = test_train_unique_participants'
                                        this_participant_idx = ismember(test_participants, g);
                                        this_participant_data = sum(this_participant_idx);
                                        this_participant_grp = ceil((1:this_participant_data) / (this_participant_data * cv_factor));
                                        this_participant_grp = this_participant_grp(randperm(numel(this_participant_grp)));
                                        this_participant_grp(this_participant_grp == 4) = 3;
                                        test_participant_grp(this_participant_idx) = this_participant_grp;
                                    end
                                end

                                try
                                    for g = unique(test_participant_grp)
                                        test_idx = test_participant_grp == g;
                                        train_idx_full = ~test_idx' & test_train_idx;

                                        train_class_count = histcounts(classes(train_idx_full), (0:class_count) + 0.5);
                                        
                                        [min_train_class_count, min_train_class] = min(train_class_count);

                                        for r2 = 1:REPS
                                            i_retry_2 = 1;

                                            while i_retry_2 > 0
                                                train_idx = train_idx_full & classes == min_train_class;

                                                for c_i = find((1:class_count) ~= min_train_class)
                                                    min_class_valid = find(train_idx_full & classes == c_i);
                                                    min_class_valid = min_class_valid(randperm(length(min_class_valid)));
                                                    train_idx(min_class_valid(1:min_train_class_count)) = true;
                                                end

                                                this_feature_set = [feature_rank(q, 1:(fn - 1)) f];

                                                try
                                                    if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'}) && TRAIN_WITH_LASSO
                                                        [B_glm, Fit_glm] = lassoglm(test_data(train_idx, this_feature_set), classes(train_idx) - 1, 'binomial', 'CV', 10, 'NumLambda', LASSO_CV_NUM_LAMBDA);

                                                        B = -[Fit_glm.Intercept(Fit_glm.IndexMinDeviance); B_glm(:, Fit_glm.IndexMinDeviance)];
                                                    else
                                                        train_opts = {test_data(train_idx, this_feature_set), classes(train_idx)};

                                                        if strcmp(CLASSIFICATION_METHOD, 'LR')
                                                            B = mnrfit(train_opts{:});
                                                        elseif strcmp(CLASSIFICATION_METHOD, 'NB')
                                                            Mdl = fitcnb(train_opts{:});
                                                        elseif strcmp(CLASSIFICATION_METHOD, 'LDA')
                                                            Mdl = fitcdiscr(train_opts{:}, LDA_OPTS{:});
                                                        end
                                                    end

                                                    if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'})
                                                        pred_prob = mnrval(B, test_data(test_idx, this_feature_set));
                                                        [~, pred] = max(pred_prob, [], 2);
                                                    elseif ismember(CLASSIFICATION_METHOD, {'NB', 'LDA'})
                                                        [pred, pred_prob] = predict(Mdl, test_data(test_idx, this_feature_set));
                                                    end

                                                    predictions.(CV){t, q, fn, f}.pred(test_idx, ((r1 - 1) * REPS) + r2) = pred;
                                                    predictions.(CV){t, q, fn, f}.pred_prob(test_idx, :, ((r1 - 1) * REPS) + r2) = pred_prob;

                                                    i_retry_2 = 0;
                                                catch err
                                                    warning(err.message);
                                                    i_retry_2 = i_retry_2 + 1;
                                                    if i_retry_2 >= 10
                                                        rethrow(err);
                                                    end
                                                end
                                            end
                                        end
                                    end
                                    
                                    i_retry_1 = 0;
                                catch err
                                    warning(err.message);
                                    i_retry_1 = i_retry_1 + 1;
                                    if i_retry_1 >= 10
                                        rethrow(err);
                                    end
                                end
                            end
                        end
                        
                        accuracy_raw = NaN(REPS * CV_REPS, 1);
                        specificity_raw = NaN(REPS * CV_REPS, 1);
                        sensitivity_raw = NaN(REPS * CV_REPS, 1);
                        F1_raw = NaN(REPS * CV_REPS, 1);
                        
                        for r = 1:(REPS * CV_REPS)
                            if (class_count == 2)
                                cp = classperf(classes(test_train_idx), 'Positive', 2, 'Negative', 1);
                                classperf(cp, predictions.(CV){t, q, fn, f}.pred(test_train_idx, r));

                                accuracy_raw(r) = cp.CorrectRate;
                                specificity_raw(r) = cp.Specificity;
                                sensitivity_raw(r) = cp.Sensitivity;
                                F1_raw(r) = 2 * ((cp.Sensitivity * cp.Specificity) / (cp.Sensitivity + cp.Specificity));
                            else
                                cp = classperf(classes(test_train_idx));
                                classperf(cp, predictions.(CV){t, q, fn, f}.pred(test_train_idx, r));

                                accuracy_raw(r) = cp.CorrectRate;
                                
                                specificity_raw(r) = 0;
                                sensitivity_raw(r) = 0;
                                F1_raw(r) = 0;
                                
                                for c_i = 1:class_count
                                    cp = classperf(classes(test_train_idx), 'Positive', c_i, 'Negative', find((1:class_count) ~= c_i));
                                    classperf(cp, predictions.(CV){t, q, fn, f}.pred(test_train_idx, r));

                                    specificity_raw(r) = specificity_raw(r) + cp.Specificity;
                                    sensitivity_raw(r) = sensitivity_raw(r) + cp.Sensitivity;
                                    F1_raw(r) = F1_raw(r) + (2 * ((cp.Sensitivity * cp.Specificity) / (cp.Sensitivity + cp.Specificity)));
                                end
                                
                                specificity_raw(r) = specificity_raw(r) / class_count;
                                sensitivity_raw(r) = sensitivity_raw(r) / class_count;
                                F1_raw(r) = F1_raw(r) / class_count;
                            end
                        end
                        
                        feature_scores(f, 1) = nanmedian(F1_raw);
                        feature_scores(f, 2) = nanmedian(accuracy_raw);
                        feature_scores(f, 3) = nanmedian(specificity_raw);
                        feature_scores(f, 4) = nanmedian(sensitivity_raw);
                        
                        predictions.(CV){t, q, fn, f}.score = feature_scores;
                    end
                    
                    feature_scores_idx = 1:FEATURES;
                    
                    for s = SCORES:-1:1
                        [~, sort_idx] = sort(feature_scores(:, s), 'descend');
                        feature_scores_idx = feature_scores_idx(sort_idx);
                        feature_scores = feature_scores(sort_idx, :);
                    end
                    
                    for f = 1:FEATURES
                        if ~isnan(feature_scores(f, 1))
                            break
                        end
                    end
                    
                    fprintf(' Selected %s feature: %i\n', nth(f), feature_scores_idx(f));
                    
                    feature_rank(q, fn) = feature_scores_idx(f);
                end
            else
                fprintf(' (INVALID)\n')
            end
        end
    end
    
    classification_data.parameters = struct();
    classification_data.parameters.tests = TESTS;
    classification_data.parameters.q = Q;
    classification_data.parameters.features = FEATURES;
    
    classification_data.options = struct();
    classification_data.options.tests_performed = TESTS_TO_PERFORM;
    classification_data.options.q_tested = Q_TO_TEST;
    classification_data.options.q_extra_tested = Q_EXTRA_TO_TEST;
    classification_data.options.reps = REPS * CV_REPS;
    
    classification_data.participants = struct();
    classification_data.participants.feature_data = data_full;
    classification_data.participants.class = participant_class;
    classification_data.participants.qids = participant_qids;
    
    classification_data.meta.q_extra = q_extra;
    classification_data.meta.q_valid = q_valid;
    
    classification_data.results = struct();
    classification_data.results.feature_rank = feature_rank;
    classification_data.results.baseline = baseline;
    classification_data.results.predictions = predictions;

    save_data(['classification-feature-selection-location-v2' variant_out '-' datestr(now, 'yyyymmddhhMMss')], classification_data, varargin{:});
end

function str = nth(n)
    if mod(n, 10) == 1 && mod(n, 100) ~= 11
        str = sprintf('%ist', n);
    elseif mod(n, 10) == 2 && mod(n, 100) ~= 12
        str = sprintf('%ind', n);
    elseif mod(n, 10) == 3 && mod(n, 100) ~= 13
        str = sprintf('%ird', n);
    else
        str = sprintf('%ith', n);
    end
end
