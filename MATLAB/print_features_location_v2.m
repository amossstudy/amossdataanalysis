function print_features_location_v2(participant_filter, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 18-Oct-2015

    OUTPUT_SUB_DIR = 'location/v2/features-raw';
    
    [data_raw, classification_properties] = load_features_location_v2('participant_filter', participant_filter, varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    FEATURES = size(data_raw, 1);
    
    feature_short = {'LV', 'NC', 'ENT', 'NENT', 'HS', 'CM', 'TT', 'TD'};
    feature_format = {'%f', '%i', '%f', '%f', '%f', '%f', '%f', '%f'};
    
    feature_order = [3 4 1 5 7 8 6 2];
    
    text_filename = 'features';
    
    save_text(text_filename, [{'output_sub_dir', OUTPUT_SUB_DIR}, varargin, {'append', false}], '%s\n', datestr(now));
    
    save_text_params = {text_filename, [{'output_sub_dir', OUTPUT_SUB_DIR}, varargin, {'append', true}]};
    
    for p = unique(classification_properties.participant)'
        valid_idx = strcmp(classification_properties.participant, p);
        
        this_participant = struct();
        
        for field_cell = fieldnames(classification_properties)'
            this_participant.(field_cell{:}) = classification_properties.(field_cell{:})(valid_idx);
        end
        
        this_participant.data = data_raw(:, valid_idx)';
        
        [~, sort_ixd] = sort(this_participant.week_start);
        
        lines_col_header = repmat({''}, FEATURES + 2, 1);
        lines_col_header_min = repmat({''}, FEATURES, 1);
        
        lines_col_header{1} = sprintf('%s   w/b : ', p{:});
        lines_col_header{2} = sprintf('%svariant : ', repmat(' ', 1, numel(lines_col_header{1}) - 10));
        
        max_line_length = max(cellfun(@numel, lines_col_header(1:2)));
        max_line_length_min = max(cellfun(@numel, feature_short)) + 3;
        
        for f = 1:FEATURES
            lines_col_header{f + 2} = sprintf('%s%s : ', repmat(' ', 1, max_line_length - numel(feature_short{feature_order(f)}) - 3), feature_short{feature_order(f)});
            lines_col_header_min{f} = sprintf('%s%s : ', repmat(' ', 1, max_line_length_min - numel(feature_short{feature_order(f)}) - 3), feature_short{feature_order(f)});
        end
        
        lines_col_save = lines_col_header;
        
        for i = 1:5:numel(sort_ixd)
            lines_col_print = lines_col_header;
            
            for j = i:min(i + 5, numel(sort_ixd))
                lines_col_save_this = lines_col_header_min;
                
                lines_col = repmat({''}, FEATURES + 2, 1);
                lines_col_min = cell(FEATURES, 1);
                
                this_idx = sort_ixd(j);
                lines_col{1} = datestr(this_participant.week_start(this_idx), 'dd/mm/yyyy');
                lines_col{2} = this_participant.variant{this_idx};
                
                for f = 1:FEATURES
                    lines_col{f + 2} = sprintf(feature_format{feature_order(f)}, this_participant.data(this_idx, feature_order(f)));
                end
                
                max_col_length = max(cellfun(@numel, lines_col));
                max_col_length_min = max(cellfun(@numel, lines_col(3:end)));
                
                for f = 1:FEATURES + 2
                    if f > 2
                        lines_col_min{f - 2} = sprintf('%s%s%s', ...
                            repmat(' ', 1, floor((max_col_length_min - numel(lines_col{f})) / 2) ), ...
                            lines_col{f}, ...
                            repmat(' ', 1, ceil((max_col_length_min - numel(lines_col{f})) / 2) ));
                    
                        lines_col_save_this{f - 2} = sprintf('%s%s', lines_col_save_this{f - 2}, lines_col_min{f - 2});
                    end
                    
                    lines_col{f} = sprintf(' %s%s%s ', ...
                        repmat(' ', 1, floor((max_col_length - numel(lines_col{f})) / 2) ), ...
                        lines_col{f}, ...
                        repmat(' ', 1, ceil((max_col_length - numel(lines_col{f})) / 2) ));
                    
                    lines_col_print{f} = sprintf('%s%s', lines_col_print{f}, lines_col{f});
                    lines_col_save{f} = sprintf('%s%s', lines_col_save{f}, lines_col{f});
                end
                
                date_str = ['wb_' datestr(this_participant.week_start(this_idx), 'yyyy-mm-dd')];
                variant_str = '';
                if ~isempty(this_participant.variant{this_idx})
                    variant_str = ['_' this_participant.variant{this_idx}];
                end
                this_filename = sprintf('%s_features_%s%s%s', p{:}, date_str, variant_str);
                save_text(this_filename, [{'output_sub_dir', OUTPUT_SUB_DIR}, varargin, {'append', false}], '');
                for f = 1:FEATURES
                    save_text(this_filename, [{'output_sub_dir', OUTPUT_SUB_DIR}, varargin, {'append', true}], '%s\n', lines_col_save_this{f});
                end
            end
            
            max_line_length = max(cellfun(@numel, lines_col_print));
            
            fprintf('%s\n', repmat('=', 1, max_line_length));
            fprintf('%s\n', lines_col_print{1});
            fprintf('%s\n', lines_col_print{2});
            fprintf('%s\n', repmat('-', 1, max_line_length));
            for f = 1:FEATURES
                fprintf('%s\n', lines_col_print{f + 2});
            end
            fprintf('%s\n', repmat('=', 1, max_line_length));
        end
        
        max_line_length = max(cellfun(@numel, lines_col_save));
        
        save_text(save_text_params{:}, '%s\n', repmat('=', 1, max_line_length));
        save_text(save_text_params{:}, '%s\n', lines_col_save{1});
        save_text(save_text_params{:}, '%s\n', lines_col_save{2});
        save_text(save_text_params{:}, '%s\n', repmat('-', 1, max_line_length));
        for f = 1:FEATURES
            save_text(save_text_params{:}, '%s\n', lines_col_save{f + 2});
        end
        save_text(save_text_params{:}, '%s\n', repmat('=', 1, max_line_length));
    end
end
