function visualise_data_battery_discharge(participant, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 18-Mar-2015

    close all;

    OUTPUT_SUB_DIR = 'battery-discharge';

    data = load_battery_data(participant, varargin{:});

    if ~isempty(data)
        battery_discharges = load_battery_discharges(participant, varargin{:});

        if isempty(data.time) || (length(data.time) == 1 && data.time == 0)
            data_min = 0;
            data_max = 1;
        else
            data_min = data.time(1);
            data_max = data.time(end);
        end

        data_min = get_argument({'tmin', 't_min'}, data_min, varargin{:});
        data_max = get_argument({'tmax', 't_max'}, data_max, varargin{:});

        t_min = floor(data_min);
        t_max = ceil(data_max);

        [xticks, xticks_str] = get_xticks(t_min, t_max, varargin{:});

        data_idx = (data.time >= data_min) & (data.time <= data_max);
        discharge_raw_idx = ((battery_discharges.raw.time >= data_min) & (battery_discharges.raw.time <= data_max)) | isnan(battery_discharges.raw.time);
        discharge_cycles_idx = (battery_discharges.start.time >= data_min) & (battery_discharges.start.time <= data_max);
        
        figure;

        stairs(data.time(data_idx), data.data(data_idx), 'LineWidth', 1);
        if sum(discharge_cycles_idx) > 0
            hold on;
            scatter(battery_discharges.start.time(discharge_cycles_idx), ones(sum(discharge_cycles_idx), 1), 'x', 'LineWidth', 1);
            stairs(battery_discharges.raw.time(discharge_raw_idx), battery_discharges.raw.data(discharge_raw_idx), 'LineWidth', 2);
            hold off;
        end
        xlim([t_min t_max])
        ylim([0 1]);
        set(gca, 'XTick', xticks)
        set(gca, 'XTickLabel', xticks_str)
        title('Battery Level')
        ylabel('Battery Level (%)')
        xlabel('Time')
        if sum(discharge_cycles_idx) > 0
            legend('Battery level', 'Discharge start', 'Discharge', 'Location', 'SouthWest');
        else
            legend('Battery level');
        end
        
        save_figure(['battery-level-' participant], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

        figure;

        hold on;
        for i = find(discharge_cycles_idx)'
            plot(battery_discharges.cycles{i}.time, battery_discharges.cycles{i}.data, 'LineWidth', 1);
        end
        hold off;
        xlim([0 1]);
        ylim([0 1]);
        xticks = 0:2:24;
        xticks_str = num2str(xticks');
        set(gca, 'XTick', xticks / 24)
        set(gca, 'XTickLabel', xticks_str)
        title('Battery Discharges From Full')
        ylabel('Battery Level (%)')
        xlabel('Discharge time (hours)')
        save_figure(['battery-discharges-' participant], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

        figure;

        hold on;
        h = NaN(5, 1);
        for i = find(discharge_cycles_idx)'
            h(1) = plot(battery_discharges.cycles{i}.time, battery_discharges.cycles{i}.data, 'Color', [0.75 0.75 0.75], 'LineWidth', 1);
        end

        [mean_by_time, mean_by_level, mean_pseudo] = calculate_battery_discharge_means(battery_discharges, discharge_cycles_idx);

        if sum(discharge_cycles_idx) > 0
            clr = lines(2);
            h(2) = plot(mean_by_time.valid.mean_time, mean_by_time.valid.level, 'Color', clr(1, :), 'LineWidth', 3);
            h(3) = plot(mean_by_level.valid.time, mean_by_level.valid.mean_level, 'Color', clr(2, :), 'LineWidth', 3);
            legend(h(1:3), 'Discharges', 'Mean time to level', 'Mean level at time', 'Location', 'SouthWest')
        end
        hold off;
        xlim([0 1]);
        ylim([0 1]);
        xticks = 0:2:24;
        xticks_str = num2str(xticks');
        set(gca, 'XTick', xticks / 24)
        set(gca, 'XTickLabel', xticks_str)
        title('Battery Discharges From Full')
        ylabel('Battery Level (%)')
        xlabel('Discharge time (hours)')
        save_figure(['battery-discharges-mean-' participant], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        
        if ~isempty(mean_pseudo.crossover)
            delete(h(2:3));

            hold on;
            if sum(discharge_cycles_idx) > 0
                clr = lines(5);
                h(4) = plot([0 1], (mean_pseudo.a * [0 1]) + mean_pseudo.b, 'Color', clr(5, :), 'LineWidth', 3);
                h(2) = plot(mean_by_time.valid.mean_time, mean_by_time.valid.level, 'Color', clr(1, :), 'LineWidth', 3);
                h(3) = plot(mean_by_level.valid.time, mean_by_level.valid.mean_level, 'Color', clr(2, :), 'LineWidth', 3);
                h(5) = scatter(mean_pseudo.crossover(1), mean_pseudo.crossover(2), 200, 'x', 'MarkerEdgeColor', clr(3, :), 'LineWidth', 3);
                legend(h, 'Discharges', 'Mean time to level', 'Mean level at time', 'Pseudo-mean', 'Crossover', 'Location', 'SouthWest')
            end
            hold off;
            xlim([0 1]);
            ylim([0 1]);
            save_figure(['battery-discharges-pseudo-mean-' participant], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        end
    else
        fprintf('  No data loaded\n');
    end
end
