function h_out = visualise_data_overview_mood_zoom(participant, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    h_in = get_argument({'happend', 'h_append'}, [], varargin{:});
    
    if isempty(h_in)
        close all;
    end
    
    OUTPUT_SUB_DIR = 'data-overview';
    
    data = load_mood_zoom_data(participant, varargin{:});
    
    %%
    if ~isempty(data)
        mood_zoom = data;
        
        fprintf('  Plotting : ');
        tic; h_out = plot_mood_zoom(mood_zoom, varargin{:}); toc;
        %%
        fprintf('  Saving : ');
        tic;
        save_figure([participant '-mood-zoom'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        save_data([participant '-mood-zoom'], mood_zoom, 'root_path_type', 'output_path', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
    else
        fprintf('  No data loaded\n');
        h_out = h_in;
    end
end