function visualise_data_quantity_location_v2(varargin)
% Copyright (c) 2017, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 20-Feb-2017

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/data-quantity';
    
    paths = get_paths(varargin{:});
    
    [~, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(numel(classification_properties.participant) > 0, 'No data loaded.');
    
    params = get_configuration('features_location_v2', varargin{:});
    
    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    
    files = get_participants(varargin{:});
        
    data_dates = datenum([2014, 1, 1]):datenum([2017, 1, 1]);
    start_end_wb = (data_dates([1 end]) - 1) - weekday(data_dates([1 end]) - 1) + 2;
    data_dates_week_edges = (start_end_wb(1) - 0.5):7:(start_end_wb(end) + 6.5);
    
    enrolled_data_matrix = NaN(length(files), length(data_dates));
    participant_data_matrix = NaN(length(files), length(data_dates));
    location_data_matrix = NaN(length(files), length(data_dates));
    tc_data_matrix = NaN(length(files), length(data_dates));
    
    participant_entered = NaN(length(files), 1);
    participant_left = NaN(length(files), 1);
    participant_upgraded = NaN(length(files), 1);
    participant_stopped = NaN(length(files), 1);
    
    participant_has_upgraded = false(length(files), 1);
    min_valid_data = NaN(length(files), 1);
    max_valid_data = NaN(length(files), 1);
    
    participant_id = cell(length(files), 1);
    participant_class = NaN(length(files), 1);
    participant_weeks_total = NaN(length(files), 1);
    participant_weeks_upgraded = NaN(length(files), 1);
    participant_weeks_upgraded_valid = NaN(length(files), 1);
    participant_weeks_location = NaN(length(files), 1);
    participant_weeks_location_tc = NaN(length(files), 1);
    
    i = 1;
    
    for file = files'
        fprintf('Loading %s\n', file.name)

        properties = get_participant_property(file.name, [], varargin{:});

        if ~isempty(properties) && isstruct(properties) && ...
                isfield(properties, 'classification') && ...
                ~isempty(properties.classification)
            
            participant_class(i) = properties.classification;
            participant_id{i} = file.name;
            
            if ~isempty(properties.date_entered)
                participant_entered(i) = datenum(properties.date_entered);
            end
            
            if isnan(participant_entered(i)) || (participant_entered(i) < datenum([2014 03 01]))
                input_path = [paths.unprocessed_data_path file.name '/'];

                events_files = dir([input_path 'events-*.csv']);
                events_filenames = sort({events_files.name});
                
                for events_file = events_filenames
                    [~, ~, datepos] = regexpi(events_file{:}, 'events-([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]).csv');

                    if ~isempty(datepos)
                        thisdate = datenum(events_file{:}(datepos{1}(1):datepos{1}(2)), 'yyyy-mm-dd');
                        if thisdate >= participant_entered(i)
                            participant_entered(i) = thisdate;
                            break
                        end
                    end
                end
            end
            
            if ~isempty(properties.date_left)
                participant_left(i) = datenum(properties.date_left);
            end
            
            reporting = load_reporting_data(file.name, varargin{:});

            if isstruct(reporting)
                reporting_versions_idx = strcmp([reporting.unsorted.setting], 'version');
                reporting_versions = cellfun(@str2num, reporting.unsorted.value(reporting_versions_idx));
                reporting_versions_upgraded = reporting_versions >= 1031;

                upgrade_idx = find(reporting_versions_upgraded, 1, 'first');

                this_participant_upgraded = NaN;
                
                if ~isempty(upgrade_idx) && (isnan(participant_entered(i)) || participant_entered(i) < datenum([2015 05 01]))
                    reporting_versions_idx = find(reporting_versions_idx);

                    this_participant_upgraded = floor(reporting.unsorted.time(reporting_versions_idx(upgrade_idx)));
                    
                    if (~isnan(participant_left(i)) && this_participant_upgraded >= participant_left(i)) || ...
                            (~isnan(participant_entered(i)) && this_participant_upgraded <= participant_entered(i))
                        
                        participant_upgraded(i) = NaN;
                    else
                        participant_upgraded(i) = this_participant_upgraded;
                    end
                elseif ~isnan(participant_entered(i)) && participant_entered(i) >= datenum([2015 05 01])
                    this_participant_upgraded = participant_entered(i);
                end
                
                this_data_days_shared = NaN(size(data_dates));

                share_location_on = sort(unique(floor(reporting.time(strcmp([reporting.setting], 'setting_share_location') & strcmp([reporting.value], 'true')))));
                this_data_days_shared(ismember(data_dates, share_location_on)) = 1;

                share_location_off = sort(unique(floor(reporting.time(strcmp([reporting.setting], 'setting_share_location') & strcmp([reporting.value], 'false')))));
                this_data_days_shared(ismember(data_dates, share_location_off)) = 0;

                for k = share_location_on(ismember(share_location_on, share_location_off))'
                    values = reporting.value(strcmp([reporting.setting], 'setting_share_location') & floor(reporting.time) == k);
                    lvalues = NaN(size(values));
                    lvalues(strcmp(values, 'true')) = true;
                    lvalues(strcmp(values, 'false')) = false;
                    lvalues = lvalues(~isnan(lvalues));
                    if ~isempty(lvalues)
                        this_data_days_shared(data_dates == k) = lvalues(end);
                    end
                end
                
                this_data_days_shared_valid = find(~isnan(this_data_days_shared));
                this_data_days_shared_missing = this_data_days_shared_valid(diff(this_data_days_shared_valid) > 1);

                for k = 1:numel(this_data_days_shared_missing)
                    k1 = this_data_days_shared_missing(k);
                    k2 = this_data_days_shared_valid(find(this_data_days_shared_valid > k1, 1, 'first'));
                    if this_data_days_shared(k1) ~= this_data_days_shared(k2)
                        this_data_days_shared((k1+1):(k2-1)) = -2;
                    elseif (k2 - k1 - 1) >= 7
                        continue;
                    else
                        this_data_days_shared(k1:k2) = this_data_days_shared(k1);
                    end
                end

                if ~isnan(participant_left(i))
                    if ~isnan(this_data_days_shared(find(data_dates <= participant_left(i), 1, 'last')))
                        this_data_days_shared(data_dates > participant_left(i)) = 0;
                    end
                end
                if ~isnan(participant_entered(i))
                    this_data_days_shared(data_dates < participant_entered(i)) = 0;
                end
                
                if sum(isnan(this_data_days_shared)) > 0
                    access_log = load_access_log_data(file.name, varargin{:});

                    if ~isempty(access_log)
                        access_location = unique(access_log.file_date(strncmp(access_log.file_type, 'location', 8)))';

                        this_data_days_shared(ismember(data_dates, access_location)) = 1;
                    end
                    
                    if ~isnan(participant_entered(i)) && (participant_entered(i) < datenum([2014 10 01]))
                        input_path = [paths.unprocessed_data_path file.name '/'];

                        for location_file = dir([input_path 'location-*.csv'])'
                            [~, ~, datepos] = regexpi(location_file.name, 'location-([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]).csv');

                            if ~isempty(datepos)
                                thisdate = datenum(location_file.name(datepos{1}(1):datepos{1}(2)), 'yyyy-mm-dd');
                                if isnan(this_data_days_shared(ismember(data_dates, thisdate)))
                                    this_data_days_shared(ismember(data_dates, thisdate)) = 1;
                                end
                            end
                        end
                    end
                end
                
                if isnan(this_data_days_shared(end))
                    k1 = find(~isnan(this_data_days_shared), 1, 'last');
                    this_data_days_shared((k1 + 1):end) = 0;
                end

                this_data_days_shared(this_data_days_shared == -2) = 0;
                
                this_data_days_shared_valid = find(~isnan(this_data_days_shared));
                this_data_days_shared_missing = this_data_days_shared_valid(diff(this_data_days_shared_valid) > 1);

                for k = 1:numel(this_data_days_shared_missing)
                    k1 = this_data_days_shared_missing(k);
                    k2 = this_data_days_shared_valid(find(this_data_days_shared_valid > k1, 1, 'first'));
                    if (k2 - k1 - 1) >= 7
                        continue;
                    elseif this_data_days_shared(k1) == this_data_days_shared(k2)
                        this_data_days_shared(k1:k2) = this_data_days_shared(k1);
                    else
                        this_data_days_shared((k1+1):(k2-1)) = 1;
                    end
                end
                
                if ~isnan(participant_left(i))
                    this_data_days_shared(data_dates > participant_left(i)) = 0;
                end
                
                if sum(this_data_days_shared > 0) > 0
                    participant_stopped(i) = data_dates(find(this_data_days_shared > 0, 1, 'last'));
                    
                    this_data_days_shared(data_dates > participant_entered(i) & data_dates < participant_stopped(i) & ...
                            (this_data_days_shared == 0 | isnan(this_data_days_shared))) = 3;
                    
                    if participant_stopped(i) == participant_left(i) || participant_stopped(i) == data_dates(end)
                        participant_stopped(i) = NaN;
                    end
                end
                
                if ~isnan(this_participant_upgraded)
                    this_data_days_shared((this_data_days_shared == 1) & (data_dates >= this_participant_upgraded)) = 2;
                    
                    if sum(this_data_days_shared == 2) >= 10
                        participant_has_upgraded(i) = true;
                    end
                end
                
                if ~isnan(participant_stopped(i)) && ~isnan(participant_left(i)) && (participant_stopped(i) ~= participant_left(i))
                    this_data_days_shared((data_dates > participant_stopped(i)) & (data_dates <= participant_left(i))) = 3;
                end
                
                this_data_days_shared_enrolled = this_data_days_shared;
                this_data_days_shared_enrolled(this_data_days_shared_enrolled == 2) = 1;
                
                enrolled_data_matrix(i, :) = this_data_days_shared_enrolled;
                
                participant_data_matrix(i, :) = this_data_days_shared;
                
                this_location_data_days = this_data_days_shared;
                this_tc_data_days = this_data_days_shared;
                
                for w = find(strcmp(classification_properties.participant, file.name))'
                    wb = classification_properties.week_start(w);
                    
                    idx = data_dates >= wb & data_dates <= wb + 6;
                    
                    if ~isnan(participant_stopped(i))
                        idx = idx & data_dates <= participant_stopped(i);
                    elseif ~isnan(participant_left(i))
                        idx = idx & data_dates <= participant_left(i);
                    end
                    
                    if sum(idx) >= 1
                        this_location_data_days(idx) = 4;
                        this_tc_data_days(idx) = 4;
                        
                        if ~isnan(classification_properties.QIDS(w))
                            this_tc_data_days(idx) = 5;
                        end
                    end
                end
                
                idx = this_location_data_days == 4;
                if sum(idx) > 0
                    min_valid_data(i) = data_dates(find(idx, 1, 'first'));
                    max_valid_data(i) = data_dates(find(idx, 1, 'last'));
                end
                
                location_data_matrix(i, :) = this_location_data_days;
                tc_data_matrix(i, :) = this_tc_data_days;
                
                participant_weeks_total(i) = sum(histcounts(data_dates(ismember(this_data_days_shared, [1, 2])), data_dates_week_edges) >= 4);
                participant_weeks_upgraded(i) = sum(histcounts(data_dates(this_data_days_shared == 2), data_dates_week_edges) >= 4);
                
                participant_weeks_location(i) = sum(histcounts(data_dates(this_location_data_days == 4), data_dates_week_edges) >= 4);
                participant_weeks_location_tc(i) = sum(histcounts(data_dates(this_tc_data_days == 5), data_dates_week_edges) >= 3);
            end
            i = i + 1;
        end
    end
    
    t_min_full = data_dates(1);
    t_max_full = data_dates(end);
    
    if sum(~isnan(min_valid_data)) > 0
        t_min_valid = min(min_valid_data);
    else
        t_min_valid = t_min_full;
    end
    if sum(~isnan(max_valid_data)) > 0
        t_max_valid = max(max_valid_data);
    else
        t_max_valid = t_max_full;
    end
    
    for j = find(participant_has_upgraded)'
        participant_weeks_upgraded_valid(j) = sum(histcounts(data_dates((participant_data_matrix(j, :) == 2) & (data_dates <= t_max_valid)), data_dates_week_edges) >= 4);
    end
    
    t_min_valid_vec = datevec(t_min_valid);
    t_min_valid_vec(3) = 1;
    t_min_valid = datenum(t_min_valid_vec);
    
    t_max_valid_vec = datevec(t_max_valid);
    t_max_valid_vec(3) = 1;
    t_max_valid_vec(2) = t_max_valid_vec(2) + 1;
    if t_max_valid_vec(2) > 12
        t_max_valid_vec(2) = 1;
        t_max_valid_vec(1) = t_max_valid_vec(1) + 1;
    end
    t_max_valid = datenum(t_max_valid_vec);
    
    [xticks_full, xticks_full_str] = get_xticks(t_min_full, t_max_full, varargin{:});
    [xticks_valid, xticks_valid_str] = get_xticks(t_min_valid, t_max_valid, varargin{:});
    
    participant_data_matrix(isnan(participant_data_matrix)) = -1;

    data_order = 1:(i - 1);

    for i = 0:9
        if i == 0
            this_data_matrix = enrolled_data_matrix;
            
            filename = 'participants_enrolled_all';
        elseif ismember(i, [1 2 3])
            this_data_matrix = participant_data_matrix;
            filename = 'participants_enrolled';
        elseif ismember(i, [4 5 6])
            this_data_matrix = location_data_matrix;
            filename = 'participants_enrolled_with_location';
        elseif ismember(i, [7 8 9])
            this_data_matrix = tc_data_matrix;
            filename = 'participants_enrolled_with_location_tc';
        end
        if ismember(i, [0 1 4 7])
            this_data_order = data_order;
            participants_shown = numel(this_data_order);
            date_idx = true(size(data_dates));
            filename_suffix = '';
        elseif ismember(i, [2 5 8])
            this_data_order = participant_has_upgraded;
            participants_shown = sum(participant_has_upgraded);
            date_idx = true(size(data_dates));
            filename_suffix = '_upgraded';
        elseif ismember(i, [3 6 9])
            this_data_order = participant_has_upgraded;
            participants_shown = sum(participant_has_upgraded);
            date_idx = data_dates >= t_min_valid & data_dates <= t_max_valid;
            filename_suffix = '_upgraded_valid';
        end
        
        this_data_order_idx = find(this_data_order);
        
        for j = 1:numel(this_data_order_idx)
            p = this_data_order_idx(j);
            this_data_matrix(p, data_dates < participant_entered(p)) = 0;
        end
        
        data_range = 1:participants_shown;
        
        for marker_adjustment = [0, -1]
            figure;
            imagesc(data_dates(date_idx), data_range, this_data_matrix(this_data_order, date_idx));
            if ismember(i, [3 6 9])
                set(gca, 'xtick', xticks_valid);
                set(gca, 'xticklabel', xticks_valid_str);
            else
                set(gca, 'xtick', xticks_full);
                set(gca, 'xticklabel', xticks_full_str);
            end
            set(gca, 'ytick', 1:participants_shown);
            set(gca, 'yticklabel', []);
            set(gca, 'clim', [-0.5 5.5]);
            colormap([0.8 0.8 0.8; min([lines(1) * 1.7; 1 1 1]); lines(1) * 1.3; 0.65 0.65 0.65; 1 1 0; 0 0.8 0]);
            if latex_ticks
                set(gca, 'TickLabelInterpreter', 'latex')
            end
            hlbl = ylabel('Participant index');
            if latex_labels
                set(hlbl, 'Interpreter', 'latex');
            end
            p = get(hlbl, 'Position');
            p(1) = p(1) - sum(date_idx) / 20;
            set(hlbl, 'Position', p);
            set(gca, 'FontSize', 30);

            hold on;
            participant_date_entered_x = reshape([repmat(participant_entered(this_data_order) - 0.5 + marker_adjustment, 1, 2) NaN(participants_shown, 1)]', participants_shown * 3, 1);
            participant_date_entered_y = reshape([data_range - 0.5; data_range + 0.5; NaN(1, participants_shown)], participants_shown * 3, 1);
            plot(participant_date_entered_x, participant_date_entered_y, 'LineWidth', 1, 'Color', [0 0 0])
            participant_date_left_x = reshape([repmat(participant_left(this_data_order) + 0.5 + marker_adjustment, 1, 2) NaN(participants_shown, 1)]', participants_shown * 3, 1);
            participant_date_left_y = reshape([data_range - 0.5; data_range + 0.5; NaN(1, participants_shown)], participants_shown * 3, 1);
            plot(participant_date_left_x, participant_date_left_y, 'LineWidth', 1, 'Color', [0 0 0])
            participant_stopped_x = reshape([repmat(participant_stopped(this_data_order) + 0.5 + marker_adjustment, 1, 2) NaN(participants_shown, 1)]', participants_shown * 3, 1);
            participant_stopped_y = reshape([data_range - 0.5; data_range + 0.5; NaN(1, participants_shown)], participants_shown * 3, 1);
            plot(participant_stopped_x, participant_stopped_y, 'LineWidth', 1, 'Color', [0 0 0])
            if i > 0
                scatter(participant_upgraded(this_data_order) - 0.5 + marker_adjustment, data_range, 'x', 'MarkerEdgeColor', [1 0 0]);
            end
            hold off;

            if marker_adjustment == 0
                this_filename_suffix = filename_suffix;
            elseif marker_adjustment == -1
                this_filename_suffix = [filename_suffix '_with_marker_adjustment'];
            else
                this_filename_suffix = [filename_suffix '_with_marker_adjustment_' num2str(marker_adjustment)];
            end
            save_figure([filename this_filename_suffix], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        end
    end
    
    for j = 1:5
        if j == 1
            this_data = participant_weeks_total;
            this_desc = 'enrolled';
        elseif j == 2
            this_data = participant_weeks_upgraded;
            this_desc = 'upgraded';
        elseif j == 3
            this_data = participant_weeks_upgraded_valid;
            this_data(isnan(this_data)) = 0;
            this_desc = 'upgraded within analysis period';
        elseif j == 4
            this_data = participant_weeks_location;
            this_desc = 'upgraded with location data';
        elseif j == 5
            this_data = participant_weeks_location_tc;
            this_desc = 'upgraded with location data and QIDS';
        end
        
        cohort_dist_participants = zeros(3, 1);
        cohort_dist_weeks = zeros(3, 1);
        cohort_weeks_med = zeros(3, 1);
        cohort_weeks_iqr = zeros(3, 1);
        
        for i = 1:3
            if j == 1
                cohort_dist_participants(i) = sum(participant_class == (i - 1));
            else
                cohort_dist_participants(i) = sum(this_data(participant_class == (i - 1)) > 0);
            end
            cohort_dist_weeks(i) = sum(this_data(participant_class == (i - 1)));
            cohort_weeks_med(i) = median(this_data(participant_class == (i - 1) & (this_data > 0)));
            cohort_weeks_iqr(i) = iqr(this_data(participant_class == (i - 1) & (this_data > 0)));
        end
        total_med = median(this_data(this_data > 0));
        total_iqr = iqr(this_data(this_data > 0));
        
        cohort_dist_str_participants = '';
        cohort_dist_str_weeks = '';
        cohort_dist_str_med_iqr = '';
        
        for i = 1:3
            cohort_dist_str_participants = sprintf('%s%s: %i; ', cohort_dist_str_participants, params.test_description{i + 1}, cohort_dist_participants(i));
            cohort_dist_str_weeks = sprintf('%s%s: %i; ', cohort_dist_str_weeks, params.test_description{i + 1}, cohort_dist_weeks(i));
            cohort_dist_str_med_iqr = sprintf('%s%s: %.4f%s%.4f; ', cohort_dist_str_med_iqr, params.test_description{i + 1}, cohort_weeks_med(i), char(177), cohort_weeks_iqr(i));
        end

        fprintf('Participants %s: %i  (%s)\n', this_desc, sum(cohort_dist_participants), cohort_dist_str_participants(1:end-2));
        fprintf('Weeks %s: %i  (%s)\n', this_desc, sum(cohort_dist_weeks), cohort_dist_str_weeks(1:end-2));
        fprintf('Median %s: %.4f%s%.4f  (%s)\n', this_desc, total_med, char(177), total_iqr, cohort_dist_str_med_iqr(1:end-2));
    end
    
    participant_age = NaN(length(files), 1);
    participant_bmi = NaN(length(files), 1);
    participant_gender = NaN(length(files), 1);
    participant_employment_status = NaN(length(files), 1);
    
    for p = find(participant_weeks_location_tc > 0)'
        properties = get_participant_property(participant_id{p}, [], varargin{:});

        participant_age(p) = properties.age;
        participant_gender(p) = properties.gender;
        participant_bmi(p) = properties.bmi;
        if ~isempty(properties.employment_status)
            participant_employment_status(p) = properties.employment_status;
        else
            participant_employment_status(p) = 0;
        end
    end

    fprintf('\nDemographics:\n');

    for j = 1:4
        for i = 0:3
            if i == 0
                idx = this_data > 0;
                desc = '';
            else
                idx = participant_class == (i - 1) & (this_data > 0);
                desc = ['  ' params.test_description{i + 1}];
            end

            if j == 1
                this_gender_male = sum(participant_gender(idx) == 1);
                this_gender_female = sum(participant_gender(idx) == 2);

                fprintf('%s Gender: %i male; %i female\n', desc, this_gender_male, this_gender_female);
            elseif j == 2
                this_age_med = median(participant_age(idx));
                this_age_iqr = iqr(participant_age(idx));

                fprintf('%s Age: %.4f%s%.4f\n', desc, this_age_med, char(177), this_age_iqr);
            elseif j == 3
                this_bmi_med = median(participant_bmi(idx));
                this_bmi_iqr = iqr(participant_bmi(idx));

                fprintf('%s BMI: %.4f%s%.4f\n', desc, this_bmi_med, char(177), this_bmi_iqr);
            elseif j == 4
                this_employment_status = participant_employment_status(idx);
                this_employment_status_unique = unique(this_employment_status);
                
                fprintf('%s Employment Status: ', desc);
                
                first = true;
                
                for emp = sort(this_employment_status_unique)'
                    if ~first
                        fprintf('; ');
                    end
                    first = false;
                    
                    emp_desc = params.employment_status_description{params.employment_status_id == emp};
                    fprintf('%s: %i', emp_desc, sum(this_employment_status == emp));
                end
                fprintf('\n')
            end
        end
    end
end
