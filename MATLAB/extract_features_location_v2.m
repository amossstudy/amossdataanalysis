function extract_features_location_v2(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 07-Aug-2015

% References:
% * Saeb S., Zhang M., Karr C.J., Schueller S.M., Corden M.E., Kording
%   K.P., Mohr D.C. (2015) Mobile Phone Sensor Correlates of Depressive 
%   Symptom Severity in Daily-Life Behavior: An Exploratory Study. Journal
%   of Medical Internet Research, 17(7), e175. doi:10.2196/jmir.4273
%   Retrieved from http://www.jmir.org/2015/7/e175/

    variant = get_argument('variant', '', varargin{:});
    combine_variants = get_argument({'combinevariants', 'combine_variants'}, {}, varargin{:});
    
    if isempty(variant) && ~isempty(combine_variants)
        variant = 'combined';

        for i = 1:length(combine_variants)
            this_variant_name = combine_variants{i};

            if isempty(this_variant_name)
                this_variant_name = 'std';
            end

            variant = [variant '_' this_variant_name]; %#ok<AGROW>
        end
    end

    variant_out = '';
    
    if ~isempty(variant)
        variant_out = ['_' variant];
    end

    for file = get_participants(varargin{:})'
        fprintf('Extracting features for %s\n', file.name)

        reporting = load_reporting_data(file.name, varargin{:});

        reporting_versions_idx = strcmp([reporting.setting], 'version');
        reporting_versions = cellfun(@str2num, reporting.value(reporting_versions_idx));
        reporting_versions_upgraded = reporting_versions >= 1031;
        
        if sum(reporting_versions_upgraded) > 0
            upgrade_idx = find(diff([0; reporting_versions_upgraded]), 1, 'last');

            reporting_versions_idx = find(reporting_versions_idx);

            upgrade_time = reporting.time(reporting_versions_idx(upgrade_idx));
            
            t_min = floor(upgrade_time) + 1;
        else
            fprintf('  Not upgraded\n');
            
            continue;
        end
        
        features = load_location_features(file.name, varargin{:});

        features.v2 = struct;

        features.v2.class = get_participant_property(file.name, 'classification', varargin{:});
        
        features_template = calculate_features_location_v2([]);

        data = load_location_preprocessed(file.name, varargin{:});

        if ~isempty(data) && isfield(data, 'normalised') && isfield(data.normalised, 'filtered') && ...
                isfield(data.normalised.filtered, 'segmented') && ~isempty(data.normalised.filtered.segmented.time)
            
            time_days = floor(data.normalised.filtered.segmented.time);
            time_days_valid = time_days(time_days >= t_min);
            time_days_unique = time_days_valid(diff([time_days_valid; time_days_valid(end) + 1]) ~= 0);
            
            weeks_start = time_days_unique(1);
            weeks_start = (weeks_start - 1) - weekday(weeks_start - 1) + 2; % Start on the previous Monday
            weeks_end = time_days_unique(end);
            weeks = ((0:floor((weeks_end - weeks_start) / 7)) * 7) + weeks_start;
            
            for property = fieldnames(features_template)'
                features.v2.(property{:}) = repmat(features_template.(property{:}), 1, length(weeks));
            end
            
            for w = 1:length(weeks)
                week_features = calculate_features_location_v2(data.normalised.filtered.segmented, weeks(w));
                
                for property = fieldnames(week_features)'
                    features.v2.(property{:})(:, w) = week_features.(property{:});
                end
            end
            
            features.v2.is_valid = features.v2.is_valid';
        else
            fprintf('  No data loaded\n');
        end

        save_data([file.name '-location' variant_out '-features'], features, varargin{:});
    end
end
