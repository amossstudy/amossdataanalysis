function visualise_regression_location_v2_personalised_individuals(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 07-Apr-2016

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/regression/personalised/';
    
    params = get_configuration('features_location_v2', varargin{:});
    
    regression_data = load_regression_data_location_v2_personalised(varargin{:});
    
    for participant_idx = 1:numel(regression_data.id)
        fprintf(' Processing participant %i of %i (%s): ', participant_idx, numel(regression_data.id), regression_data.id{participant_idx});
        
        if (regression_data.qids.count(participant_idx) <= 5) || ...
                (regression_data.qids.range(participant_idx) <= 3) || ...
                isnan(regression_data.class(participant_idx))
            fprintf('Skipping.\n');
            continue;
        end
        
        tic;
        
        figure;
        
        week_start = regression_data.data.week_start{participant_idx};
        data_normalised = regression_data.data.data_normalised{participant_idx};
        
        qids = regression_data.qids.data{participant_idx};
        
        if isfield(regression_data.model.full, 'pred') && iscell(regression_data.model.full.pred)
            pred = regression_data.model.full.pred{participant_idx};

            h = plot_regression_location_v2_personalised(week_start, data_normalised, qids, pred, varargin);

            title(h{1}, sprintf('%s (%s)', regression_data.id{participant_idx}, params.test_description{regression_data.class(participant_idx) + 2}));

            save_figure(sprintf('full_fit_%s', regression_data.id{participant_idx}), 'output_sub_dir', OUTPUT_SUB_DIR, 'font_size', 14, varargin{:});
        end
        
        pred = regression_data.results.pred{participant_idx};
        
        h = plot_regression_location_v2_personalised(week_start, data_normalised, qids, pred, varargin);
        
        title(h{1}, sprintf('%s (%s)', regression_data.id{participant_idx}, params.test_description{regression_data.class(participant_idx) + 2}));

        save_figure(sprintf('loo_fit_%s', regression_data.id{participant_idx}), 'output_sub_dir', OUTPUT_SUB_DIR, 'font_size', 14, varargin{:});
        
        B_full = regression_data.model.full.B{participant_idx};
        FitInfo_full = regression_data.model.full.FitInfo{participant_idx};
        
        cv_index_min_deviance = regression_data.model.min_deviance_idx(participant_idx);
        
        lassoPlot(B_full, FitInfo_full, 'plottype', 'CV');

        if cv_index_min_deviance ~= FitInfo_full.IndexMinDeviance
            a = axis;
            hold on;
            plot([1 1] * FitInfo_full.Lambda(cv_index_min_deviance), a(3:4));
            hold off;
        end

        save_figure(sprintf('cv_deviance_full_%s', regression_data.id{participant_idx}), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

        toc;
    end
end
