function plot_regression_location_v2_qids(x, qids, pred, varargin)
% Copyright (c) 2017, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 29-Mar-2017

    pred = {pred};
    
    while numel(varargin) > 0 && ~ischar(varargin{1})
        pred = [pred, varargin(1)]; %#ok<AGROW>
        varargin = varargin(2:end);
    end
    
    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    times_labels = get_argument({'timeslabels', 'times_labels'}, false, varargin{:});
    times_ticks = get_argument({'timesticks', 'times_ticks'}, times_labels, varargin{:});
    
    label_opts = {};
    if latex_labels
        label_opts = {'Interpreter', 'latex'};
    elseif times_labels
        label_opts = {'FontName', 'Times New Roman', 'Interpreter', 'none'};
    end
    
    y_min = get_argument({'y_min', 'ymin'}, 0, varargin{:});
    y_max = get_argument({'y_max', 'ymax'}, 28, varargin{:});
    
    training_elements = get_argument('training_elements', [], varargin{:});
    
    error_bars = get_argument({'errorbars', 'error_bars'}, false, varargin{:});
    
    assert(isempty(training_elements) || (numel(training_elements) == numel(qids)), 'The vector of training elements must be the same as the number of data points.')
    
    x_full = min(x):7:max(x);
    [~, x_sorted_idx] = ismember(x, x_full);
    
    qids_full = NaN(numel(x_full), 1);
    qids_full(x_sorted_idx) = qids;
    x_lim = x_full([1, end]) + [-1 1];
    
    xticks_lim_vec = datevec(x_full([1 end]));
    xticks_lim_vec(:, 3) = 1;
    xticks_lim_vec(2, 2) = xticks_lim_vec(2, 2) + 1;
    xticks_lim = datenum(xticks_lim_vec);

    [xticks, xticks_str] = get_xticks(xticks_lim(1), xticks_lim(2), varargin{:});

    training_elements_full = false(numel(x_full), 1);
    
    if ~isempty(training_elements)
        training_elements_full(x_sorted_idx) = training_elements;
    end

    clr = lines(7);
    
    for i = 1:numel(pred)
        pred_full = NaN(numel(x_full), 1);
        error_neg_full = NaN(numel(x_full), 1);
        error_pos_full = NaN(numel(x_full), 1);

        plot_line_width = 2;
        scatter_marker_size = 200;

        if size(pred{i}, 1) == 1 || size(pred{i}, 2) == 1
            pred_full(x_sorted_idx) = pred{i};
            if i == 1
                this_clr = [clr(1, :); 1 0 0];
            else
                this_clr = clr([1 i + 3], :);
            end
            error_bars = false;
        elseif size(pred{i}, 1) == numel(qids) && size(pred{i}, 2) > 0 && ~error_bars
            assert(i == 1, 'Multiple predictions are not supported for histogram coloured plots.');
            
            pred_full(x_sorted_idx) = mean(pred{i}, 2);

            y_range = linspace(y_min, y_max, 100);

            y_hist = NaN(numel(x_full), numel(y_range));

            if size(pred{i}, 2) > 1
                for m = 1:numel(x)
                    y_hist(x_full == x(m), :) = histc(pred{i}(m, :), y_range);
                end
            end

            imagesc(x_full, y_range, y_hist');
            clr_hist = flipud(autumn);
            clr_hist(1, :) = 1;
            colormap(clr_hist);
            set(gca, 'YDir', 'normal')

            hold on;

            x_lim = x_full([1, end]) + [-3.5 3.5];

            this_clr = clr([1 4], :);
        elseif size(pred{i}, 1) == numel(qids) && size(pred{i}, 2) > 0 && error_bars
            pred_full(x_sorted_idx) = mean(pred{i}, 2);
            error_neg_full(x_sorted_idx) = pred_full(x_sorted_idx) - quantile(pred{i}, 0.25, 2);
            error_pos_full(x_sorted_idx) = quantile(pred{i}, 0.75, 2) - pred_full(x_sorted_idx);

            this_clr = [0 0 0; clr(i, :)];

            plot_line_width = 1;
            scatter_marker_size = 100;
        end

        if i == 1
            scatter(x_full(~training_elements_full), qids_full(~training_elements_full), scatter_marker_size * 0.75, this_clr(1, :), 'LineWidth', plot_line_width);
            hold on;
            scatter(x_full(training_elements_full), qids_full(training_elements_full), scatter_marker_size, 'd', 'MarkerEdgeColor', this_clr(1, :), 'LineWidth', plot_line_width);
            plot(x_full, qids_full, 'Color', this_clr(1, :), 'LineWidth', plot_line_width);
            plot_missing_data(x_full, qids_full, ':', 'Color', this_clr(1, :), 'LineWidth', 1.5);
        end
        
        if error_bars
            errorbar(x_full, pred_full, error_neg_full, error_pos_full, 'Color', this_clr(2, :), 'LineWidth', plot_line_width);
        else
            scatter(x_full, pred_full, 150, 'x', 'MarkerEdgeColor', this_clr(2, :), 'LineWidth', plot_line_width)
            plot(x_full, pred_full, 'Color', this_clr(2, :), 'LineWidth', plot_line_width);
        end
        plot_missing_data(x_full, pred_full, ':', 'Color', this_clr(2, :), 'LineWidth', 1.5);
    end
    
    plot(x_lim, [11 11], '--', 'Color', [1 1 1] * 0.5, 'LineWidth', 2);
    hold off;

    set(gca, 'xlim', x_lim)

    set(gca, 'XTick', xticks)
    set(gca, 'XTickLabel', xticks_str)

    set(gca, 'ylim', [y_min y_max])

    if latex_ticks
        set(gca, 'TickLabelInterpreter', 'latex')
    elseif times_ticks
        set(gca, 'FontName', 'Times New Roman');
    end
    
    ylabel('QIDS Score', label_opts{:});
    
    p = get(gcf, 'PaperPosition');
    p(4) = p(4) / 2;
    set(gcf, 'PaperPosition', p);
end
