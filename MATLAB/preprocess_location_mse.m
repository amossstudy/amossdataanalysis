function preprocess_location_mse(varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 22-Dec-2014

    variant = get_argument('variant', '', varargin{:});
    
    if ~isempty(variant)
        variant = ['_' variant];
    end
    
    for file = get_participants(varargin{:})'
        fprintf('Preprocessing MSE for %s\n', file.name)

        data = load_location_preprocessed(file.name, varargin{:});

        episodes_for_analysis = load_episodes_for_analysis(file.name, varargin{:});

        if ~isempty(data) && isfield(data, 'regularised') && ~isempty(data.regularised.time)
            mse = struct();

            mse.scales = 1:10000;
            mse.m_range = 1:5;

            mse.result = zeros(length(mse.scales), max(mse.m_range));

            epoch_count = 0;
            valid_epoch_count = 0;
            if ~isempty(episodes_for_analysis) && isfield(episodes_for_analysis, 'epochs')
                epoch_count = size(episodes_for_analysis.epochs, 2);
                for epoch = episodes_for_analysis.epochs
                    if epoch.valid
                        valid_epoch_count = valid_epoch_count + 1;
                    end
                end
            end

            mse.episodes = struct;
            mse.episodes.scales = 1:1000;
            mse.episodes.m_range = 1:5;
            mse.episodes.result = NaN(length(mse.episodes.scales), max(mse.episodes.m_range), valid_epoch_count);
            mse.episodes.start = NaN(1, valid_epoch_count);
            mse.episodes.end = NaN(1, valid_epoch_count);

            ep_invalid = 0;

            for ep_id = 0:epoch_count
                tic;

                if ep_id == 0
                    fprintf('  Full data: ');
                else
                    fprintf('  Epoch %d of %d: ', ep_id, epoch_count);

                    if ~episodes_for_analysis.epochs(ep_id).valid
                        ep_invalid = ep_invalid + 1;
                        fprintf('Invalid.\n');
                        continue;
                    end
                    valid_days = (data.regularised.time >= episodes_for_analysis.epochs(ep_id).start) & (data.regularised.time < episodes_for_analysis.epochs(ep_id).end + 1);
                end

                if ep_id == 0
                    for m = mse.m_range
                        mse.result(:, m) = calculate_mse(data.regularised.data, m, 3, mse.scales);
                    end
                else
                    for m = mse.episodes.m_range
                        mse.episodes.result(:, m, ep_id - ep_invalid) = calculate_mse(data.regularised.data(valid_days), m, 3, mse.episodes.scales);
                    end
                    mse.episodes.start(ep_id - ep_invalid) = episodes_for_analysis.epochs(ep_id).start;
                    mse.episodes.end(ep_id - ep_invalid) = episodes_for_analysis.epochs(ep_id).end;
                end

                toc;
            end

            save_data([file.name '-location' variant '-mse'], mse, varargin{:});
        else
            fprintf('  No data loaded\n');
        end
    end
end
