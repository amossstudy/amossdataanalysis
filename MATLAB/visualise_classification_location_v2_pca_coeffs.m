function visualise_classification_location_v2_pca_coeffs(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 05-Oct-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/classification';
    
    classification_data = load_classification_data_location_v2(varargin{:});
    
    assert(isfield(classification_data, 'meta') && isfield(classification_data.meta, 'pca_coeffs'), 'No PCA coefficients in data.');
    
    feature_short = {'LV', 'NC', 'ENT', 'NENT', 'HS', 'CM', 'TT', 'TD'};
    feature_order = [3 4 1 5 7 8 6 2];
    
    Q = classification_data.parameters.q;
    FEATURES = classification_data.parameters.features;
    
    TESTS_TO_PERFORM = classification_data.options.tests_performed;
    Q_TO_TEST = classification_data.options.q_tested;
    Q_EXTRA_TO_TEST = classification_data.options.q_extra_tested;
    
    Q_MAX = max(Q);
    
    q_extra = classification_data.meta.q_extra;
    
    test_description = {'All', 'HC', 'BD', 'BPD', 'HC/BD'};
    
    for t = TESTS_TO_PERFORM
        for coeff_type_cell = fieldnames(classification_data.meta.pca_coeffs)'
            coeff_type_desc = strrep(coeff_type_cell{:}, '_', ' ');
            coeff_type_desc([1 (find(coeff_type_desc == ' ') + 1)]) = upper(coeff_type_desc([1 (find(coeff_type_desc == ' ') + 1)]));
            
            this_coeffs = classification_data.meta.pca_coeffs.(coeff_type_cell{:}){t};
            
            if ismatrix(this_coeffs)
                q_opts = 0;
            else
                q_opts = find(ismember([Q, q_extra{t}], [Q_TO_TEST, Q_MAX + Q_EXTRA_TO_TEST]));
            end
            
            for q = q_opts
                pca_coeffs_data = cell(FEATURES, FEATURES);
                pca_coeffs_mean = NaN(FEATURES, FEATURES);
                pca_coeffs_std = NaN(FEATURES, FEATURES);

                if q == 0
                    pca_coeffs_mean(:, :) = this_coeffs;
                    pca_coeffs_std = zeros(FEATURES, FEATURES);
                    for i = 1:FEATURES
                        for j = 1:FEATURES
                            pca_coeffs_data{i, j} = pca_coeffs_mean(i, j);
                        end
                    end
                    this_q_desc = '';
                    this_q_filename_suffix = '';
                else
                    for i = 1:FEATURES
                        for j = 1:FEATURES
                            if ndims(this_coeffs) == 5
                                pca_coeffs_data_temp = this_coeffs(q, :, :, i, j);
                            elseif ndims(this_coeffs) == 6
                                pca_coeffs_data_temp = this_coeffs(q, :, :, :, i, j);
                            end
                            pca_coeffs_data{i, j} = reshape(pca_coeffs_data_temp, numel(pca_coeffs_data_temp), 1);
                            pca_coeffs_mean(i, j) = mean(pca_coeffs_data{i, j});
                            pca_coeffs_std(i, j) = std(pca_coeffs_data{i, j});
                        end
                    end

                    if q <= numel(Q)
                        this_q_desc = sprintf(', QIDS %s %i', char(8805), Q(q));
                        this_q_filename_suffix = sprintf('_qids_%.2i', Q(q));
                    elseif q == (numel(Q) + 1)
                        this_q_desc = ', Cohort';
                        this_q_filename_suffix = '_cohort';
                    end
                end
                
                figure;

                imagesc(pca_coeffs_mean(feature_order, :));

                set(gca, 'clim', [-1 1]);

                colormap(colormap_redblue(63, 0.4))

                h = colorbar;

                ylabel(h, 'Feature Weight', 'FontSize', 20);

                box off;

                for i = 1:FEATURES
                    for j = 1:FEATURES
                        rectangle('position', [(i - 0.5) (j - 0.5) 1 1]);

                        text(i, j, sprintf('%0.2f', pca_coeffs_mean(feature_order(j), i)), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FontSize', 14);
                    end
                end

                set(gca, 'xtick', 1:FEATURES);
                set(gca, 'ytick', 1:FEATURES);
                set(gca, 'yticklabel', feature_short(feature_order), 'FontSize', 20);

                title(sprintf('PCA Coefficients %s (%s%s)', coeff_type_desc, test_description{t}, this_q_desc), 'FontSize', 20);
                xlabel('Principal Component', 'FontSize', 20);
                ylabel('Feature', 'FontSize', 20);

                save_figure(sprintf('pca_coefficients_%s%s_%s', coeff_type_cell{:}, this_q_filename_suffix, strrep(lower(test_description{t}), '/', '_')), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);

                figure;

                imagesc(pca_coeffs_std(feature_order, :));

                set(gca, 'clim', [0 0.5]);

                colormap(colormap_redwhite(64, 0.4))

                h = colorbar;

                ylabel(h, 'Feature Weight Standard Deviation', 'FontSize', 20);

                box off;

                for i = 1:FEATURES
                    for j = 1:FEATURES
                        rectangle('position', [(i - 0.5) (j - 0.5) 1 1]);

                        text(i, j, sprintf('%0.2f', pca_coeffs_std(feature_order(j), i)), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FontSize', 14);
                    end
                end

                set(gca, 'xtick', 1:FEATURES);
                set(gca, 'ytick', 1:FEATURES);
                set(gca, 'yticklabel', feature_short(feature_order), 'FontSize', 20);

                title(sprintf('PCA Coefficient SD %s (%s%s)', coeff_type_desc, test_description{t}, this_q_desc), 'FontSize', 20);
                xlabel('Principal Component', 'FontSize', 20);
                ylabel('Feature', 'FontSize', 20);

                save_figure(sprintf('pca_coefficients_std_%s%s_%s', coeff_type_cell{:}, this_q_filename_suffix, strrep(lower(test_description{t}), '/', '_')), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);

                figure;

                pos = NaN(FEATURES, FEATURES, 4);
                h = NaN(FEATURES, FEATURES);

                for i = 1:FEATURES
                    for j = 1:FEATURES
                        h(i, j) = subplot(FEATURES, FEATURES, (i - 1) * FEATURES + j);

                        histogram(pca_coeffs_data{feature_order(i), j}, -1:0.05:1);
                        set(gca, 'xtick', []);
                        set(gca, 'ytick', []);

                        if i == FEATURES
                            xlabel(num2str(j), 'FontSize', 20)
                        end

                        if j == 1
                            ylabel(feature_short(feature_order(i)), 'Rotation', 0, 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'right', 'FontSize', 20)
                        end

                        pos(i, j, :) = get(gca, 'Position');

                        ax = axis;

                        hold on;
                        h_plot = plot([0 0], ax(3:4), '--', 'LineWidth', 0.5, 'Color', ones(3, 1) * 0.4);
                        
                        if isfield(classification_data.meta.pca_coeffs, 'per_test')
                            clr = lines(2);
                            plot([0 0] + classification_data.meta.pca_coeffs.per_test{t}(feature_order(i), j), ax(3:4), 'LineWidth', 0.5, 'Color', clr(2, :));
                        end
                        hold off;

                        uistack(h_plot, 'bottom');
                    end
                end

                old_width = pos(1, 1, 3);
                new_width = ((pos(1, FEATURES, 1) + pos(1, FEATURES, 3) - pos(1, 1, 1)) / FEATURES) * 0.95;
                old_height = pos(1, 1, 4);
                new_height = ((pos(1, 1, 2) + pos(1, 1, 4) - pos(FEATURES, 1, 2)) / FEATURES) * 0.95;
                pos(:, :, 3) = new_width;
                pos(:, :, 1) = pos(:, :, 1) - repmat((new_width - old_width) * ((0:(FEATURES - 1)) / (FEATURES - 1)), FEATURES, 1);
                pos(:, :, 4) = new_height;
                pos(:, :, 2) = pos(:, :, 2) - repmat((new_height - old_height) * (flipud((0:(FEATURES - 1))') / (FEATURES - 1)), 1, FEATURES);

                for i = 1:FEATURES
                    for j = 1:FEATURES
                        set(h(i, j), 'Position', pos(i, j, :));
                    end
                end

                save_figure(sprintf('pca_coefficients_dist_%s%s_%s', coeff_type_cell{:}, this_q_filename_suffix, strrep(lower(test_description{t}), '/', '_')), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);
            end
        end
    end
end
