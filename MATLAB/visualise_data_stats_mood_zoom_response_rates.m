function visualise_data_stats_mood_zoom_response_rates(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 19-Mar-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-stats';
    
    files = get_participants(varargin{:});
    
    class_desc = {'Combined', 'HC', 'BD', 'BPD'};
    
    response_rates = struct('data', NaN, 'time_diff', NaN, 'class', NaN, 'j', 1);
    response_rates.data = cell(size(files));
    response_rates.time_diff = cell(size(files));
    response_rates.class = NaN(size(files));

    max_length = zeros(4, 1);
    max_length_diff = zeros(4, 1);
    
    for file = files'
        fprintf('Loading %s\n', file.name)

        class = get_participant_property(file.name, 'classification', varargin{:});

        if ~isempty(class)
            data_mz = load_mood_zoom_data(file.name);

            if ~isempty(data_mz)
                if isfield(data_mz, 'time')
                    this_questionnaire_time = data_mz.time;
                    t_min = floor(min(this_questionnaire_time));
                    t_max = ceil(max(this_questionnaire_time));
                    t_range = t_min:7:t_max;

                    if (length(t_range) > 1)
                        this_j = response_rates.j;

                        this_questionnaire_time = sort(unique(floor(this_questionnaire_time)));
                        this_questionnaire_time_responded_diff = diff(this_questionnaire_time);
                        
                        response_rates.data{this_j} = NaN(length(t_range) - 1, 1);
                        response_rates.time_diff{this_j} = this_questionnaire_time_responded_diff;
                        response_rates.class(this_j) = class;

                        for t = 1:(length(t_range) - 1)
                            response_rates.data{this_j}(t) = sum(this_questionnaire_time >= t_range(t) & this_questionnaire_time < t_range(t + 1));
                        end

                        response_rates.j = this_j + 1;

                        max_length(1) = max(max_length(1), length(t_range) - 1);
                        max_length(class + 2) = max(max_length(class + 2), length(t_range) - 1);
                        max_length_diff(1) = max(max_length_diff(1), length(this_questionnaire_time_responded_diff));
                        max_length_diff(class + 2) = max(max_length_diff(class + 2), length(this_questionnaire_time_responded_diff));
                    end
                end
            end
        end
    end
    
    clr = lines(10);
    
    h_plot = zeros(4, 1);
    h_hist = zeros(4, 1);

    figure;

    class_data = cell(4, 1);

    for i = 0:3
        h_plot(i + 1) = subplot(4, 4, (1:3) + (i * 4));
        hold on;

        if i == 0
            valid_idx = 1:(response_rates.j - 1);
        else
            valid_idx = find(response_rates.class == (i - 1))';
        end

        class_data{i + 1} = NaN(length(valid_idx), max_length(i + 1));

        k = 1;

        for j = valid_idx
            participant_data = response_rates.data{j};
            plot((1:length(participant_data)) * 4, participant_data, 'Color', [0.5, 0.5, 0.5]);
            class_data{i + 1}(k, 1:length(participant_data)) = participant_data;
            k = k + 1;
        end

        plot((1:max_length(i + 1)) * 4, nanmean(class_data{i + 1}, 1), 'Color', clr(1, :), 'LineWidth', 2);
        plot((1:max_length(i + 1)) * 4, nanmedian(class_data{i + 1}, 1), 'Color', clr(2, :), 'LineWidth', 2);

        if i == 0
            title('Mood Zoom Response Rate Over 7 Days');
        elseif i == 3
            xlabel('Weeks');
        end
        ylabel(class_desc{i + 1});

        xlim_val(1) = 4;
        xlim_val(2) = floor(max_length(i + 1) * 4 / 10) * 10;
        set(gca, 'xlim', xlim_val);

        ylim_val = get(gca, 'ylim') + [-0.5, 0.5];
        set(gca, 'ylim', ylim_val);
        hold off;

        h_hist(i + 1) = subplot(4, 4, (i + 1) * 4);
        histogram(class_data{i + 1}(:), 'Orientation', 'horizontal');
        set(gca, 'xticklabel', []);
        set(gca, 'ylim', ylim_val);
    end

    save_figure('mood_zoom_response_rate_total', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

    for i = 0:3
        set(h_plot(i + 1), 'xlim', [4 40]);
        ylim_val = get(h_plot(i + 1), 'ylim');

        if i < 3
            set(h_plot(i + 1), 'xticklabel', []);
        end

        p = get(h_plot(i + 1), 'Position');
        p_diff = p(4) * 0.2;
        p(4) = p(4) + p_diff;
        p(2) = p(2) - ((3 - i) * (p_diff / 3));
        p_diff = (p(3) / 3) * 0.1;
        p(3) = p(3) + p_diff;
        set(h_plot(i + 1), 'Position', p);

        h_hist(i + 1) = subplot(4, 4, (i + 1) * 4);
        class_data_40 = class_data{i + 1}(:, 1:10);
        histogram(class_data_40(:), 'Orientation', 'horizontal');
        set(gca, 'xticklabel', []);
        set(gca, 'yticklabel', []);
        set(gca, 'ylim', ylim_val);

        p = get(gca, 'Position');
        p_diff = p(4) * 0.2;
        p(4) = p(4) + p_diff;
        p(2) = p(2) - ((3 - i) * (p_diff / 3));
        p_diff = p(3) * 0.125;
        p(3) = p(3) + p_diff;
        p(1) = p(1) - p_diff;
        set(gca, 'Position', p);
    end

    save_figure('mood_zoom_response_rate_40_weeks', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

    for i = 0:3
        ylim_val = get(h_plot(i + 1), 'ylim');
        ylim_val(2) = 10.5;
        set(h_plot(i + 1), 'ylim', ylim_val);
        set(h_hist(i + 1), 'ylim', ylim_val);
    end

    save_figure('mood_zoom_response_rate_40_weeks_max_10', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

    figure;

    for i = 0:3
        class_data_40 = class_data{i + 1}(:, 1:40);

        class_data_40_hist = NaN(11, 40);

        for j = 1:40
            class_data_40_hist(:, j) = histcounts(class_data_40(:, j), [-0.5:1:9.5 inf]);
            class_data_40_hist(:, j) = class_data_40_hist(:, j) / sum(class_data_40_hist(:, j));
        end

        subplot(4, 6, (i * 6) + (1:5));

        imagesc(4:4:40, 0:10, class_data_40_hist, [0 1]);

        hold on;
        class_mean = nanmean(class_data{i + 1}(:, 1:41), 1);
        class_std = nanstd(class_data{i + 1}(:, 1:41), 1);

        plot(1:41, class_mean, 'Color', clr(1, :), 'LineWidth', 2)
        plot(1:41, class_mean + class_std, 'Color', clr(4, :), 'LineWidth', 1)
        plot(1:41, class_mean - class_std, 'Color', clr(4, :), 'LineWidth', 1)
        hold off;

        set(gca,'YDir','normal')

        if i == 0
            title('Mood Zoom Response Rate Over 7 Days');
        end
        if i < 3
            set(gca, 'xticklabel', []);
        elseif i == 3
            set(gca, 'xtick', 4:8:40);
            xlabel('Weeks');
        end

        ylabel(class_desc{i + 1});

        p = get(gca, 'Position');
        p_diff = p(4) * 0.2;
        p(4) = p(4) + p_diff;
        p(2) = p(2) - ((3 - i) * (p_diff / 3));
        set(gca, 'Position', p);
    end

    colormap(flipud(colormap('autumn')))

    subplot(4, 6, 6:6:(4*6));

    hcb = colorbar;
    p = get(hcb, 'Position');
    p_diff = p(3) * 2;
    p(3) = p(3) + p_diff;
    set(hcb, 'Position', p);
    ylabel(hcb, 'Response Rate Probability');
    set(hcb, 'AxisLocation', 'out'); % Ticks and label should be on the right.
    axis off

    save_figure('mood_zoom_response_rate_matrix', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

    figure;

    for i = 0:3
        subplot(4, 1, i + 1);

        if i == 0
            valid_idx = 1:(response_rates.j - 1);
        else
            valid_idx = find(response_rates.class == (i - 1))';
        end

        class_data{i + 1} = NaN(length(valid_idx), max_length_diff(i + 1));

        k = 1;

        for j = valid_idx
            participant_data = response_rates.time_diff{j};
            class_data{i + 1}(k, 1:length(participant_data)) = participant_data;
            k = k + 1;
        end

        histogram(class_data{i + 1}, [-0.5:1:13.5 inf]);
        xlim([-0.5, 15.5]);

        if i == 0
            title('Mood Zoom Days Between Responses');
        end
        if i < 3
            set(gca, 'xticklabel', []);
        elseif i == 3
            xlabel('Days');
        end

        ylabel(class_desc{i + 1});

        set(gca, 'yticklabel', []);

        p = get(gca, 'Position');
        p_diff = p(4) * 0.2;
        p(4) = p(4) + p_diff;
        p(2) = p(2) - ((3 - i) * (p_diff / 3));
        set(gca, 'Position', p);
    end

    save_figure('mood_zoom_response_time_diff_total', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
end
