function test_classification_location_v2(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Aug-2015

    close all;
    
    paths = get_paths(varargin{:});
    
    variant_out = get_argument({'variant_output', 'variantoutput', 'variant_out', 'variantout', 'output_variant', 'outputvariant'}, '', varargin{:});

    if ~isempty(variant_out)
        variant_out = ['_' variant_out];
    end
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    params = get_configuration('features_location_v2', varargin{:});
    
    function [feature_rank, B_lasso] = get_feature_rank(data_full, classification)
        feature_rank = NaN(FEATURES, 1);
        
        B_lasso = lasso(data_full, classification);
        
        features_count = sum(B_lasso ~= 0, 1);
        
        for i = 1:FEATURES
            features_used = find(B_lasso(:, find(features_count == i, 1, 'last')) ~= 0);
            new_features_used = features_used(~ismember(features_used, feature_rank));
            
            if ~isempty(new_features_used)
                feature_rank(find(isnan(feature_rank), 1, 'first') + (0:(length(new_features_used) - 1))) = new_features_used;
            end
        end
        
        missing_features = find(~ismember(1:FEATURES, feature_rank));
        
        if numel(missing_features) > 0
            feature_rank((FEATURES - numel(missing_features) + 1):FEATURES) = missing_features;
        end
    end
    
    valid_qids = ~isnan(classification_properties.QIDS);
    
    data_full = data_raw(:, valid_qids)';
    
    participants = classification_properties.participant(valid_qids);
    participant_variant = classification_properties.variant(valid_qids);
    participant_week_start = classification_properties.week_start(valid_qids);
    participant_class = classification_properties.class(valid_qids);
    participant_qids = classification_properties.QIDS(valid_qids);
    
    TESTS = 7;
    TESTS_SUPPORTED = [1:5 7];
    Q_MIN = 2;
    Q_MAX = 16;
    Q = Q_MIN:1:Q_MAX;
    Q_EXTRA_MAX = 1;
    FEATURES = size(data_raw, 1);
    
    TESTS_TO_PERFORM = get_argument({'run_tests', 'runtests'}, TESTS_SUPPORTED, varargin{:});
    Q_TO_TEST = get_argument({'test_q', 'testq'}, Q, varargin{:});
    Q_EXTRA_TO_TEST = get_argument({'test_q_extra', 'testqextra'}, Q_EXTRA_MAX, varargin{:});
    REPS = get_argument({'cv_reps', 'cvreps', 'reps'}, 10, varargin{:});
    perform_predictions = get_argument({'perform_predictions', 'performpredictions', 'perform_prediction', 'performprediction'}, {}, varargin{:});
    if ~iscell(perform_predictions)
        perform_predictions = {perform_predictions};
    end
    
    CLASSIFICATION_METHOD = upper(get_argument({'classification_method', 'classificationmethod'}, 'LR', varargin{:}));
    
    assert(ismember(CLASSIFICATION_METHOD, ...
        {'LR', 'LR_LASSO', 'LDA', 'NB'}), ...
        sprintf('Invalid classification method: %s', CLASSIFICATION_METHOD));
    
    TRAIN_WITH_LASSO = get_argument({'train_lasso', 'trainlasso'}, strcmp(CLASSIFICATION_METHOD, 'LR_LASSO'), varargin{:});
    
    if strcmp(CLASSIFICATION_METHOD, 'LR') && TRAIN_WITH_LASSO
        warning(sprintf('Using LASSO with logistic regression classification method.\nConsider setting classification method to LR_LASSO.')); %#ok<SPWRN>
    end
        
    assert(~(~TRAIN_WITH_LASSO && strcmp(CLASSIFICATION_METHOD, 'LR_LASSO')), 'If classification method is set to logistic regression with LASSO then ''train_lasso'' option must be true or unspecified.');
    
    LASSO_CV_NUM_LAMBDA = get_argument({'lasso_cv', 'lasso_cv'}, 10, varargin{:});
    
    LDA_OPTS = {'DiscrimType', 'quadratic', 'Prior', 'uniform'};
    
    baseline = zeros(TESTS, numel(Q) + 1);
    
    q_extra = cell(TESTS, 1);
    
    q_valid = false(TESTS, numel(Q) + Q_EXTRA_MAX);
    
    predictions = struct();
    
    for this_prediction = {'full', 'loo', 'loo_eq', 'ind_features', 'pca'}
        if isempty(perform_predictions) || sum(strcmp(perform_predictions, this_prediction)) > 0
            predictions.(this_prediction{:}) = cell(TESTS, numel(Q) + Q_EXTRA_MAX, FEATURES);
        end
    end
    
    feature_rank = NaN(numel(Q) + Q_EXTRA_MAX, FEATURES);
    
    classification_data = struct();
    classification_data.meta = struct();
    classification_data.meta.classes = cell(TESTS, 1);
    classification_data.meta.participant = cell(TESTS, 1);
    classification_data.meta.variant = cell(TESTS, 1);
    classification_data.meta.week_start = cell(TESTS, 1);
    classification_data.meta.participant_idx = cell(TESTS, 1);
    classification_data.meta.participant_class_cohort = cell(TESTS, 1);
    classification_data.meta.participant_class_q = cell(TESTS, 1);
    if isempty(perform_predictions) || sum(strcmp(perform_predictions, 'pca')) > 0
        classification_data.meta.pca_coeff = cell(TESTS, 1);
        classification_data.meta.pca_coeffs = struct();
        classification_data.meta.pca_coeffs.per_test = cell(TESTS, 1);
        classification_data.meta.pca_coeffs.per_participant = cell(TESTS, 1);
        classification_data.meta.pca_coeffs.per_rep = cell(TESTS, 1);
    end
    
    for t = TESTS_TO_PERFORM
        if t == 1
            test_participants_idx = true(size(participant_class));
        elseif (t >= 2) && (t <= 4)
            test_participants_idx = (participant_class == (t - 2));
        elseif t == 5
            test_participants_idx = ismember(participant_class, 0:1);
        elseif t == 7
            test_participants_idx = ismember(participant_class, 1:2);
        end
        
        test_data = data_full(test_participants_idx, :);
        test_participants = participants(test_participants_idx);
        test_variant = participant_variant(test_participants_idx);
        test_week_start = participant_week_start(test_participants_idx);
        test_participant_class = participant_class(test_participants_idx);
        test_participant_qids = participant_qids(test_participants_idx);
        
        test_unique_participants = unique(test_participants);
        test_unique_participant_classes = unique(test_participant_class);
        
        classification_data.meta.classes{t} = test_unique_participant_classes;
        classification_data.meta.participant{t} = test_participants;
        classification_data.meta.variant{t} = test_variant;
        classification_data.meta.week_start{t} = test_week_start;
        classification_data.meta.participant_idx{t} = test_participants_idx;
        classification_data.meta.participant_class_cohort{t} = test_participant_class;
        classification_data.meta.participant_class_q{t} = NaN(numel(Q), sum(test_participants_idx));
        if isempty(perform_predictions) || sum(strcmp(perform_predictions, 'pca')) > 0
            % classification_data.meta.pca_coeffs.per_test{t} = NaN(FEATURES, FEATURES);
            % classification_data.meta.pca_coeffs.per_participant{t} = NaN(numel(Q), FEATURES, numel(test_unique_participants), FEATURES, FEATURES);
            % classification_data.meta.pca_coeffs.per_rep{t} = NaN(numel(Q), FEATURES, numel(test_unique_participants), REPS, FEATURES, FEATURES);
            % classification_data.meta.pca_coeff{t} = NaN(FEATURES, FEATURES);
            classification_data.meta.pca_coeff{t} = NaN(numel(Q), FEATURES, numel(test_unique_participants), FEATURES, FEATURES);
            % classification_data.meta.pca_coeff{t} = NaN(numel(Q), FEATURES, numel(test_unique_participants), REPS, FEATURES, FEATURES);
        end
        
        test_data_normalised = test_data;

        for f = 1:FEATURES
            test_data_normalised(:, f) = test_data_normalised(:, f) - nanmean(test_data_normalised(:, f));
            test_data_normalised(:, f) = test_data_normalised(:, f) / nanstd(test_data_normalised(:, f));
            if sum(isnan(test_data_normalised(:, f))) > 0
                % data is now zero-mean, unit variance so this is acceptable.
                test_data_normalised(isnan(test_data_normalised(:, f)), f) = 0;
            end
        end

        % if isempty(perform_predictions) || sum(strcmp(perform_predictions, this_prediction)) > 0
        %     pca_coeff = pca(test_data_normalised);
        %     classification_data.meta.pca_coeffs.per_test{t} = pca_coeff;
        % 
        %     classification_data.meta.pca_coeff{t} = pca_coeff;
        %     test_data_pca = test_data_normalised * pca_coeff;
        % end
        
        if numel(test_unique_participant_classes) > 1
            q_extra{t} = Q_MAX + 1;
        end
        
        for q = find(ismember([Q, q_extra{t}], [Q_TO_TEST, Q_MAX + Q_EXTRA_TO_TEST]))
            if q <= numel(Q)
                this_q_desc = sprintf('t = %i, Q = %i', t, Q(q));
                
                classes = (test_participant_qids >= Q(q)) + 1;
            elseif q == (numel(Q) + 1)
                this_q_desc = sprintf('t = %i, cohort', t);
                
                classes = zeros(size(test_participant_class));
                
                for this_class_i = 1:numel(test_unique_participant_classes)
                    classes(test_participant_class == test_unique_participant_classes(this_class_i)) = this_class_i;
                end
            end
            
            classification_data.meta.participant_class_q{t}(q, :) = classes;
            
            fprintf('%s', this_q_desc)
            
            total_class_count = sum(bsxfun(@eq, classes, sort(unique(classes))'), 1);

            [min_total_class_count, min_total_class] = min(total_class_count);
            
            baseline(t, q) = max(total_class_count) / sum(total_class_count);

            feature_rank(q, :) = get_feature_rank(test_data, classes);
            
            if numel(total_class_count) > 1 && min_total_class_count >= 5 && numel(unique(test_participants(classes == min_total_class))) >= 2
                fprintf('\n')
                q_valid(t, q) = true;
                for f = 1:FEATURES
                    fprintf('%s, f = %i\n', this_q_desc, f);
                    
                    for this_prediction = {'full', 'loo'}
                        if isempty(perform_predictions) || sum(strcmp(perform_predictions, this_prediction)) > 0
                            predictions.(this_prediction{:}){t, q, f}.qids = test_participant_qids;
                            predictions.(this_prediction{:}){t, q, f}.class = classes;
                            predictions.(this_prediction{:}){t, q, f}.pred = NaN(size(test_participant_qids));
                        end
                    end
                    
                    for this_prediction = {'loo_eq', 'ind_features', 'pca'}
                        if isempty(perform_predictions) || sum(strcmp(perform_predictions, this_prediction)) > 0
                            predictions.(this_prediction{:}){t, q, f}.qids = test_participant_qids;
                            predictions.(this_prediction{:}){t, q, f}.class = classes;
                            predictions.(this_prediction{:}){t, q, f}.pred = NaN(numel(test_participant_qids), REPS);
                        end
                    end
                    
                    if isempty(perform_predictions) || sum(strcmp(perform_predictions, 'full')) > 0
                        if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'}) && TRAIN_WITH_LASSO
                            [B_glm, Fit_glm] = lassoglm(test_data(:, feature_rank(q, 1:f)), classes - 1, 'binomial', 'CV', 10, 'NumLambda', LASSO_CV_NUM_LAMBDA);

                            B = -[Fit_glm.Intercept(Fit_glm.IndexMinDeviance); B_glm(:, Fit_glm.IndexMinDeviance)];
                        else
                            train_opts = {test_data(:, feature_rank(q, 1:f)), classes};

                            if strcmp(CLASSIFICATION_METHOD, 'LR')
                                B = mnrfit(train_opts{:});
                            elseif strcmp(CLASSIFICATION_METHOD, 'NB')
                                Mdl = fitcnb(train_opts{:});
                            elseif strcmp(CLASSIFICATION_METHOD, 'LDA')
                                Mdl = fitcdiscr(train_opts{:}, LDA_OPTS{:});
                            end
                        end

                        if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'})
                            pred_prob = mnrval(B, test_data(:, feature_rank(q, 1:f)));
                            [~, pred] = max(pred_prob, [], 2);
                        elseif ismember(CLASSIFICATION_METHOD, {'NB', 'LDA'})
                            pred = predict(Mdl, test_data(:, feature_rank(q, 1:f)));
                        end

                        predictions.full{t, q, f}.pred = pred;
                    end

                    for p = 1:numel(test_unique_participants)
                        test_idx = ismember(test_participants, test_unique_participants{p});
                        train_idx_full = ~test_idx;

                        train_class_count = [sum(classes(train_idx_full) == 1) sum(classes(train_idx_full) == 2)];

                        [min_train_class_count, min_train_class] = min(train_class_count);

                        if isempty(perform_predictions) || sum(strcmp(perform_predictions, 'loo')) > 0
                            if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'}) && TRAIN_WITH_LASSO
                                [B_glm, Fit_glm] = lassoglm(test_data(train_idx_full, feature_rank(q, 1:f)), classes(train_idx_full) - 1, 'binomial', 'CV', 10, 'NumLambda', LASSO_CV_NUM_LAMBDA);

                                B = -[Fit_glm.Intercept(Fit_glm.IndexMinDeviance); B_glm(:, Fit_glm.IndexMinDeviance)];
                            else
                                train_opts = {test_data(train_idx_full, feature_rank(q, 1:f)), classes(train_idx_full)};

                                if strcmp(CLASSIFICATION_METHOD, 'LR')
                                    B = mnrfit(train_opts{:});
                                elseif strcmp(CLASSIFICATION_METHOD, 'NB')
                                    Mdl = fitcnb(train_opts{:});
                                elseif strcmp(CLASSIFICATION_METHOD, 'LDA')
                                    Mdl = fitcdiscr(train_opts{:}, LDA_OPTS{:});
                                end
                            end

                            if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'})
                                pred_prob = mnrval(B, test_data(test_idx, feature_rank(q, 1:f)));
                                [~, pred] = max(pred_prob, [], 2);
                            elseif ismember(CLASSIFICATION_METHOD, {'NB', 'LDA'})
                                pred = predict(Mdl, test_data(test_idx, feature_rank(q, 1:f)));
                            end

                            predictions.loo{t, q, f}.pred(test_idx) = pred;
                        end
                        
                        if isempty(perform_predictions) || sum(strcmp(perform_predictions, 'pca')) > 0
                            pca_coeff = pca(test_data_normalised(train_idx_full, :));
                            % classification_data.meta.pca_coeffs.per_participant{t}(q, f, p, :, :) = pca_coeff;
                            
                            classification_data.meta.pca_coeff{t}(q, f, p, :, :) = pca_coeff;
                            test_data_pca = test_data_normalised * pca_coeff;
                        end
                        
                        for r = 1:REPS
                            train_idx = train_idx_full & classes == min_train_class;

                            min_class_valid = find(train_idx_full & classes == (~logical(min_train_class - 1) + 1));
                            min_class_valid = min_class_valid(randperm(length(min_class_valid)));
                            train_idx(min_class_valid(1:min_train_class_count)) = true;

                            % if isempty(perform_predictions) || sum(strcmp(perform_predictions, 'pca')) > 0
                            %     pca_coeff = pca(test_data_normalised(train_idx, :));
                            %     classification_data.meta.pca_coeffs.per_rep{t}(q, f, p, r, :, :) = pca_coeff;
                            % 
                            %     classification_data.meta.pca_coeff{t}(q, f, p, r, :, :) = pca_coeff;
                            %     test_data_pca = test_data_normalised * pca_coeff;
                            % end

                            if isempty(perform_predictions) || sum(strcmp(perform_predictions, 'loo_eq')) > 0
                                if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'}) && TRAIN_WITH_LASSO
                                    [B_glm, Fit_glm] = lassoglm(test_data(train_idx, feature_rank(q, 1:f)), classes(train_idx) - 1, 'binomial', 'CV', 10, 'NumLambda', LASSO_CV_NUM_LAMBDA);

                                    B = -[Fit_glm.Intercept(Fit_glm.IndexMinDeviance); B_glm(:, Fit_glm.IndexMinDeviance)];
                                else
                                    train_opts = {test_data(train_idx, feature_rank(q, 1:f)), classes(train_idx)};

                                    if strcmp(CLASSIFICATION_METHOD, 'LR')
                                        B = mnrfit(train_opts{:});
                                    elseif strcmp(CLASSIFICATION_METHOD, 'NB')
                                        Mdl = fitcnb(train_opts{:});
                                    elseif strcmp(CLASSIFICATION_METHOD, 'LDA')
                                        Mdl = fitcdiscr(train_opts{:}, LDA_OPTS{:});
                                    end
                                end

                                if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'})
                                    pred_prob = mnrval(B, test_data(test_idx, feature_rank(q, 1:f)));
                                    [~, pred] = max(pred_prob, [], 2);
                                elseif ismember(CLASSIFICATION_METHOD, {'NB', 'LDA'})
                                    pred = predict(Mdl, test_data(test_idx, feature_rank(q, 1:f)));
                                end

                                predictions.loo_eq{t, q, f}.pred(test_idx, r) = pred;
                            end
                            
                            if isempty(perform_predictions) || sum(strcmp(perform_predictions, 'ind_features')) > 0
                                if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'}) && TRAIN_WITH_LASSO
                                    [B_glm, Fit_glm] = lassoglm(test_data(train_idx, params.feature_order(f)), classes(train_idx) - 1, 'binomial', 'CV', 10, 'NumLambda', LASSO_CV_NUM_LAMBDA);

                                    B = -[Fit_glm.Intercept(Fit_glm.IndexMinDeviance); B_glm(:, Fit_glm.IndexMinDeviance)];
                                else
                                    train_opts = {test_data(train_idx, params.feature_order(f)), classes(train_idx)};

                                    if strcmp(CLASSIFICATION_METHOD, 'LR')
                                        B = mnrfit(train_opts{:});
                                    elseif strcmp(CLASSIFICATION_METHOD, 'NB')
                                        Mdl = fitcnb(train_opts{:});
                                    elseif strcmp(CLASSIFICATION_METHOD, 'LDA')
                                        Mdl = fitcdiscr(train_opts{:}, LDA_OPTS{:});
                                    end
                                end

                                if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'})
                                    pred_prob = mnrval(B, test_data(test_idx, params.feature_order(f)));
                                    [~, pred] = max(pred_prob, [], 2);
                                elseif ismember(CLASSIFICATION_METHOD, {'NB', 'LDA'})
                                    pred = predict(Mdl, test_data(test_idx, params.feature_order(f)));
                                end

                                predictions.ind_features{t, q, f}.pred(test_idx, r) = pred;
                            end
                            
                            if isempty(perform_predictions) || sum(strcmp(perform_predictions, 'pca')) > 0
                                if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'}) && TRAIN_WITH_LASSO
                                    [B_glm, Fit_glm] = lassoglm(test_data_pca(train_idx, 1:f), classes(train_idx) - 1, 'binomial', 'CV', 10, 'NumLambda', LASSO_CV_NUM_LAMBDA);

                                    B = -[Fit_glm.Intercept(Fit_glm.IndexMinDeviance); B_glm(:, Fit_glm.IndexMinDeviance)];
                                else
                                    train_opts = {test_data_pca(train_idx, 1:f), classes(train_idx)};

                                    if strcmp(CLASSIFICATION_METHOD, 'LR')
                                        B = mnrfit(train_opts{:});
                                    elseif strcmp(CLASSIFICATION_METHOD, 'NB')
                                        Mdl = fitcnb(train_opts{:});
                                    elseif strcmp(CLASSIFICATION_METHOD, 'LDA')
                                        Mdl = fitcdiscr(train_opts{:}, LDA_OPTS{:});
                                    end
                                end

                                if ismember(CLASSIFICATION_METHOD, {'LR', 'LR_LASSO'})
                                    pred_prob = mnrval(B, test_data_pca(test_idx, 1:f));
                                    [~, pred] = max(pred_prob, [], 2);
                                elseif ismember(CLASSIFICATION_METHOD, {'NB', 'LDA'})
                                    pred = predict(Mdl, test_data_pca(test_idx, 1:f));
                                end

                                predictions.pca{t, q, f}.pred(test_idx, r) = pred;
                            end
                        end
                    end
                end
            else
                fprintf(' (INVALID)\n')
            end
        end
    end
    
    classification_data.parameters = struct();
    classification_data.parameters.tests = TESTS;
    classification_data.parameters.q = Q;
    classification_data.parameters.features = FEATURES;
    
    classification_data.options = struct();
    classification_data.options.tests_performed = TESTS_TO_PERFORM;
    classification_data.options.q_tested = Q_TO_TEST;
    classification_data.options.q_extra_tested = Q_EXTRA_TO_TEST;
    classification_data.options.reps = REPS;
    
    classification_data.participants = struct();
    classification_data.participants.feature_data = data_full;
    classification_data.participants.class = participant_class;
    classification_data.participants.qids = participant_qids;
    
    classification_data.meta.q_extra = q_extra;
    classification_data.meta.q_valid = q_valid;
    
    classification_data.results = struct();
    classification_data.results.feature_rank = feature_rank;
    classification_data.results.baseline = baseline;
    classification_data.results.predictions = predictions;

    save_data(['classification-location-v2' variant_out '-' datestr(now, 'yyyymmddhhMMss')], classification_data, varargin{:});
end
