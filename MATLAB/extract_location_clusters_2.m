function extract_location_clusters_2(varargin)
% Copyright (c) 2017, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 06-Mar-2017

    paths = get_paths(varargin{:});
    
    variant = get_argument('variant', '', varargin{:});
    combine_variants = get_argument({'combinevariants', 'combine_variants'}, {}, varargin{:});
    
    if isempty(variant) && ~isempty(combine_variants)
        variant = 'combined';

        for i = 1:length(combine_variants)
            this_variant_name = combine_variants{i};

            if isempty(this_variant_name)
                this_variant_name = 'std';
            end

            variant = [variant '_' this_variant_name]; %#ok<AGROW>
        end
    end

    variant_out = '';
    
    if ~isempty(variant)
        variant_out = ['_' variant];
    end

    for file = get_participants(varargin{:})'
        fprintf('Processing %s\n', file.name)

        output_path = [paths.working_path file.name '-'];

        data = load_location_preprocessed(file.name, varargin{:}, 'variant', variant);

        if ~isempty(data) && isfield(data, 'normalised') && isfield(data.normalised, 'filtered') && isfield(data.normalised.filtered, 'segmented') && ~isempty(data.normalised.filtered.segmented.time)
            fprintf('  Clustering data...\n');
            tic;

            location_data = [data.normalised.filtered.segmented.lat data.normalised.filtered.segmented.lon]; % for normalised coordinates, this is in km from home
            location_data = location_data(data.normalised.filtered.segmented.state == 1, :);

            [C_idx, C_final] = cluster_locations_hybrid(location_data, varargin{:});

            data.normalised.filtered.segmented.cluster_idx = zeros(size(data.normalised.filtered.segmented.state));
            data.normalised.filtered.segmented.cluster_valid = zeros(size(data.normalised.filtered.segmented.state));
            data.normalised.filtered.segmented.clusters = C_final;
            data.normalised.filtered.segmented.cluster_idx(data.normalised.filtered.segmented.state == 1) = C_idx;
            data.normalised.filtered.segmented.cluster_valid(data.normalised.filtered.segmented.state == 1) = (C_idx > 0);
            
            save([output_path 'location' variant_out '-preprocessed' ], 'data');

            toc;
        else
            fprintf('  No data loaded\n');
        end
    end
end
