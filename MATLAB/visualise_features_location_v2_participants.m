function visualise_features_location_v2_participants(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 04-Nov-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats';
    
    [~, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(numel(classification_properties.participant) > 0, 'No data loaded.');
    
    valid_qids = ~isnan(classification_properties.QIDS);
    valid_weeks = false(size(valid_qids));
    
    params = get_configuration('features_location_v2', varargin{:});
    
    function prepare_and_save_figure(subdir, filename, varargin)
        save_figure(filename, 'output_sub_dir', [OUTPUT_SUB_DIR '/' subdir], 'FontSize', 50, varargin{:});
    end
    
    study_period_start = get_argument({'studyperiodstart', 'study_period_start'}, [], varargin{:});
    study_period_end = get_argument({'studyperiodend', 'study_period_end'}, [], varargin{:});
    
    hist_max = [0 0];
    
    dist_participants = zeros(3, 3);
    dist_participants_desc = cell(3, 1);
    dist_weeks = zeros(3, 3);
    dist_weeks_desc = cell(2, 1);
    
    for t = 0:2
        if t == 0
            selected_participants_sorted = sort(classification_properties.participant);
            last_selected_participant = selected_participants_sorted(end);
            last_selected_participant = last_selected_participant{:};
            
            opts = {};
            
            if ~isempty(study_period_start)
                opts = [opts, 'left_after', datenum(study_period_start) + 7]; %#ok<AGROW>
            end
            
            if ~isempty(study_period_end)
                opts = [opts, 'entered_before', datenum(study_period_end) - 7, 'min_app_version_date', datenum(study_period_end) - 7]; %#ok<AGROW>
            end
            
            participants_upgraded = get_participants(varargin{:}, 'min_app_version', 1031, opts{:});

            all_data_end = max(classification_properties.week_start);

            test_participants = {};
            test_participant_class = [];
            
            for i = 1:numel(participants_upgraded)
                [~, sort_order] = sort({participants_upgraded(i).name, last_selected_participant});
                
                if sort_order(1) == 1
                    reporting = load_reporting_data(participants_upgraded(i).name, varargin{:});

                    reporting_versions_idx = strcmp([reporting.unsorted.setting], 'version');
                    reporting_versions = cellfun(@str2num, reporting.unsorted.value(reporting_versions_idx));
                    reporting_versions_upgraded = reporting_versions >= 1031;

                    upgrade_idx = find(reporting_versions_upgraded, 1, 'first');

                    reporting_versions_idx = find(reporting_versions_idx);

                    start_day = floor(reporting.unsorted.time(reporting_versions_idx(upgrade_idx)));
                    
                    properties = get_participant_property(participants_upgraded(i).name, [], varargin{:});

                    if ~isempty(properties)
                        start_day = max(start_day, datenum(properties.date_entered));
                        if ~isempty(properties.date_left)
                            end_day = min(datenum(properties.date_left), all_data_end + 6);
                        else
                            end_day = all_data_end + 6;
                        end
                        
                        if start_day < end_day
                            start_wb = (start_day - 1) - weekday(start_day - 1) + 2;
                            end_wb = (end_day - 1) - weekday(end_day - 1) + 2;

                            if ~isnan(properties.date_left)
                                this_data_days = start_day:datenum(properties.date_left);
                            else
                                this_data_days = start_day:floor(now);
                            end

                            this_data_days_shared = NaN(size(this_data_days));

                            share_location = sort(unique(floor(reporting.time(strcmp([reporting.setting], 'setting_share_location') & strcmp([reporting.value], 'true')))));
                            this_data_days_shared(ismember(this_data_days, share_location)) = true;

                            share_location = sort(unique(floor(reporting.time(strcmp([reporting.setting], 'setting_share_location') & strcmp([reporting.value], 'false')))));
                            this_data_days_shared(ismember(this_data_days, share_location)) = false;

                            this_data_days_shared_valid = find(~isnan(this_data_days_shared));
                            this_data_days_shared_missing = this_data_days_shared_valid(diff(this_data_days_shared_valid) > 1);

                            for j = 1:numel(this_data_days_shared_missing)
                                k1 = this_data_days_shared_missing(j);
                                k2 = this_data_days_shared_valid(find(this_data_days_shared_valid > k1, 1, 'first'));
                                if this_data_days_shared(k1) == this_data_days_shared(k2)
                                    this_data_days_shared(k1:k2) = this_data_days_shared(k1);
                                end
                            end

                            if ~isnan(this_data_days_shared(find(this_data_days <= end_day, 1, 'last')))
                                this_data_days_shared(this_data_days > end_day) = false;
                            end

                            if sum(isnan(this_data_days_shared)) > 0
                                access_log = load_access_log_data(participants_upgraded(i).name, varargin{:});

                                this_data_days_access = NaN(size(this_data_days));

                                access_location = unique(access_log.file_date(strncmp(access_log.file_type, 'location', 8)))';

                                this_data_days_access(ismember(this_data_days, access_location)) = true;

                                this_data_days_shared(isnan(this_data_days_shared)) = this_data_days_access(isnan(this_data_days_shared));
                            end

                            if isnan(this_data_days_shared(end))
                                k1 = find(~isnan(this_data_days_shared), 1, 'last');
                                this_data_days_shared((k1 + 1):end) = false;
                            end

                            this_data_days_shared(this_data_days > end_day) = false;
                            
                            if sum(isnan(this_data_days_shared)) > 0
                                this_data_days_shared(isnan(this_data_days_shared)) = false;
                            end

                            this_data_days_shared_weeks_count = histcounts(this_data_days(logical(this_data_days_shared)), (start_wb - 0.5):7:(end_wb + 6.5));

                            this_data_weeks = sum(this_data_days_shared_weeks_count >= 4);

                            if this_data_weeks > 0
                                test_participants = [test_participants; repmat({participants_upgraded(i).name}, this_data_weeks, 1)]; %#ok<AGROW>
                                test_participant_class = [test_participant_class; repmat(properties.classification, this_data_weeks, 1)]; %#ok<AGROW>
                            
                                shared_wb = start_wb:7:end_wb;
                                valid_wb = shared_wb(this_data_days_shared_weeks_count >= 4);

                                pidx = strcmp(classification_properties.participant, participants_upgraded(i).name);

                                valid_weeks(pidx) = ismember(classification_properties.week_start(pidx), valid_wb);
                            end
                        end
                    end
                end
            end
            
            test_type = 'Enrolled';
            test_type_title_participants = 'Upgraded';
            test_type_filename_participants = 'upgraded';
        elseif t == 1
            test_participants = classification_properties.participant(valid_weeks);
            test_participant_class = classification_properties.class(valid_weeks);
            test_type = 'Selected';
            test_type_title_participants = 'with Selected Week(s)';
            test_type_filename_participants = 'selected_weeks';
        elseif t == 2
            test_participants = classification_properties.participant(valid_weeks & valid_qids);
            test_participant_class = classification_properties.class(valid_weeks & valid_qids);
            test_type = 'Valid';
            test_type_title_participants = 'with Valid Week(s)';
            test_type_filename_participants = 'valid_weeks';
        end

        participants_unique = unique(test_participants);
        participants_unique_class = zeros(size(participants_unique));
        participants_unique_weeks = zeros(size(participants_unique));

        for i = 1:numel(participants_unique)
            participants_unique_class(i) = test_participant_class(find(strcmp(test_participants, participants_unique{i}), 1, 'first'));
            participants_unique_weeks(i) = sum(strcmp(test_participants, participants_unique{i}));
        end

        figure
        
        cohort_dist = zeros(1, numel(unique(participants_unique_class)));
        
        for c = unique(participants_unique_class)'
            h = histogram(ones(sum(participants_unique_class == c), 1) * c, -0.5:1:2.5);
            set(h, 'FaceColor', params.clr_cohort(c + 2, :));
            set(h, 'FaceAlpha', 1);
            hold on;
            cohort_dist = cohort_dist + get(h, 'values');
        end
        
        dist_participants(t + 1, :) = cohort_dist;
        
        set(gca, 'xlim', [-0.5, 2.5]);
        y_lim = get(gca, 'ylim');
        hist_max(1) = max(hist_max(1), y_lim(2));
        y_lim(2) = hist_max(1);
        set(gca, 'ylim', y_lim);
        set(gca, 'xtick', 0:2);
        set(gca, 'xticklabel', params.test_description(2:4));

        title(sprintf('Participants %s', test_type_title_participants));
        xlabel('Cohort');
        ylabel('Participants');

        prepare_and_save_figure('participants', sprintf('cohort_distribution_participants_%s', test_type_filename_participants), varargin{:}, 'FontSize', 30);

        cohort_dist_str = '';
        
        for i = 1:3
            cohort_dist_str = sprintf('%s%s: %i; ', cohort_dist_str, params.test_description{i + 1}, cohort_dist(i));
        end
        
        fprintf('Participants %s: %i  (%s)\n', test_type_title_participants, numel(participants_unique), cohort_dist_str(1:end-2));
        
        dist_participants_desc{t+1} = test_type;
        dist_weeks_desc{t+1} = test_type;

        figure

        cohort_dist = zeros(1, numel(unique(test_participant_class)));

        for c = unique(test_participant_class)'
            h = histogram(ones(sum(test_participant_class == c), 1) * c, -0.5:1:2.5);
            set(h, 'FaceColor', params.clr_cohort(c + 2, :));
            set(h, 'FaceAlpha', 1);
            hold on;
            cohort_dist = cohort_dist + get(h, 'values');
        end

        dist_weeks(t+1, :) = cohort_dist;

        set(gca, 'xlim', [-0.5, 2.5]);
        y_lim = get(gca, 'ylim');
        hist_max(2) = max(hist_max(2), y_lim(2));
        y_lim(2) = hist_max(2);
        set(gca, 'ylim', y_lim);
        set(gca, 'xtick', 0:2);
        set(gca, 'xticklabel', params.test_description(2:4));

        title(sprintf('%s Weeks', test_type));
        xlabel('Cohort');
        ylabel(sprintf('%s Weeks', test_type));

        prepare_and_save_figure('participants', sprintf('cohort_distribution_%s_weeks', lower(test_type)), varargin{:}, 'FontSize', 30);

        cohort_dist_str = '';

        for i = 1:3
            cohort_dist_str = sprintf('%s%s: %i; ', cohort_dist_str, params.test_description{i + 1}, cohort_dist(i));
        end

        fprintf('%s weeks: %i  (%s)\n', test_type, numel(test_participant_class), cohort_dist_str(1:end-2));
    end
    
    for t = 1:5
        if t == 1
            test_classes = 0:2;
        elseif t == 5
            test_classes = 0:1;
        else
            test_classes = t - 2;
        end
        
        this_test_description_filename = params.test_description_filename{t};

        figure;

        bar(dist_participants(:, test_classes + 1), 'stacked');
        colormap(params.clr_cohort(test_classes + 2, :));
        set(gca, 'xticklabel', dist_participants_desc);
        set(gca, 'xlim', [0.4 3.6])
        ylabel('Participants');
        title('Participants Available for Analysis');
        legend(params.test_description(test_classes + 2), 'Orientation', 'horizontal');

        prepare_and_save_figure('participants', sprintf('cohort_distribution_participants_overall_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);

        figure;

        bar(dist_weeks(:, test_classes + 1), 'stacked');
        colormap(params.clr_cohort(test_classes + 2, :));
        set(gca, 'xticklabel', dist_weeks_desc);
        set(gca, 'xlim', [0.4 3.6])
        ylabel('Weeks of Data');
        title('Weeks of Data Available for Analysis');
        legend(params.test_description(test_classes + 2), 'Orientation', 'horizontal');

        prepare_and_save_figure('participants', sprintf('cohort_distribution_weeks_overall_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);
    end
end
