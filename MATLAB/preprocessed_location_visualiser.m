function varargout = preprocessed_location_visualiser(varargin)
% PREPROCESSED_LOCATION_VISUALISER MATLAB code for preprocessed_location_visualiser.fig
%      PREPROCESSED_LOCATION_VISUALISER, by itself, creates a new PREPROCESSED_LOCATION_VISUALISER or raises the existing
%      singleton*.
%
%      H = PREPROCESSED_LOCATION_VISUALISER returns the handle to a new PREPROCESSED_LOCATION_VISUALISER or the handle to
%      the existing singleton*.
%
%      PREPROCESSED_LOCATION_VISUALISER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PREPROCESSED_LOCATION_VISUALISER.M with the given input arguments.
%
%      PREPROCESSED_LOCATION_VISUALISER('Property','Value',...) creates a new PREPROCESSED_LOCATION_VISUALISER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before preprocessed_location_visualiser_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to preprocessed_location_visualiser_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help preprocessed_location_visualiser

% Last Modified by GUIDE v2.5 20-Dec-2014 14:16:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @preprocessed_location_visualiser_OpeningFcn, ...
                   'gui_OutputFcn',  @preprocessed_location_visualiser_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before preprocessed_location_visualiser is made visible.
function preprocessed_location_visualiser_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to preprocessed_location_visualiser (see VARARGIN)

% Choose default command line output for preprocessed_location_visualiser
handles.output = hObject;

% UIWAIT makes preprocessed_location_visualiser wait for user response (see UIRESUME)
% uiwait(handles.main_window);

set(handles.lstParticipants, 'String', '');
set(handles.cboWeek, 'String', ' ');
set(handles.cboWeek, 'Value', 1);

set(handles.lstParticipants, 'Enable', 'off');
set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');

guidata(hObject, handles);


function handles_out = load_data(path, handles)
    data = [];
    
    if exist([path 'location-preprocessed.mat'], 'file') == 2
        load([path 'location-preprocessed.mat']);
    end
    
    if ~isempty(data) && isfield(data, 'validated') && ~isempty(data.validated.time)
        original_data = struct;
        original_data.name = 'Original Data';
        original_data.location_data = [data.lat, data.lon];
        original_data.data = data.data;
        original_data.time = data.time;
        
        % Calculate hours spent in each location, limiting to 15 mins max per
        % sample.
        original_data.location_time = [diff(data.time); 0];
        original_data.location_time = min(original_data.location_time, 1/(24 * 4));
        
        time_days = floor(original_data.time(original_data.time >= datenum([2014, 01, 01])));
        original_data.time_days = time_days;
        
        [original_data.locations, ~, original_data.locations_idx] = unique(original_data.location_data, 'rows');

        location_annotations_temp = zeros(size(original_data.locations, 1), 1);

        if exist([path 'location-annotation.mat'], 'file') == 2
            load([path 'location-annotation.mat']);
            [~, dup_location_index] = ismember(locations, original_data.locations, 'rows');
            for i = 1:length(dup_location_index)
                if dup_location_index(i) > 0
                    location_annotations_temp(dup_location_index(i)) = location_annotations(i);
                end
            end
        end
        
        original_data.location_annotations = location_annotations_temp(original_data.locations_idx);
        
        original_data.annotation_colors = lines(4);
        original_data.annotation_colors(1, :) = [0 0 0];
        
        validated_data = struct;
        validated_data.name = 'Pre-processed Data';
        validated_data.location_data = [data.validated.lat, data.validated.lon];
        validated_data.data = data.validated.data;
        validated_data.time = data.validated.time;
        
        % Calculate hours spent in each location, limiting to 15 mins max per
        % sample.
        validated_data.location_time = [diff(data.validated.time); 0];
        validated_data.location_time = min(validated_data.location_time, 1/(24 * 4));
        
        validated_data.time_days = floor(validated_data.time(validated_data.time >= datenum([2014, 01, 01])));
        
        [validated_data.locations, ~, validated_data.locations_idx] = unique(validated_data.location_data, 'rows');
        
        validated_data.location_annotations = data.validated.location_group;
        validated_data.annotation_colors = lines(max(validated_data.location_annotations) + 1);
        validated_data.annotation_colors(1, :) = [0 0 0];
        
        regularised_data = struct;
        regularised_data.name = 'Regularised Data';
        regularised_data.location_data = [data.regularised.lat, data.regularised.lon];
        regularised_data.data = data.regularised.data;
        regularised_data.time = data.regularised.time;
        
        % Calculate hours spent in each location, limiting to 15 mins max per
        % sample.
        regularised_data.location_time = [diff(data.regularised.time); 0];
        regularised_data.location_time = min(regularised_data.location_time, 1/(24 * 4));
        
        regularised_data.time_days = floor(regularised_data.time(regularised_data.time >= datenum([2014, 01, 01])));
        
        [regularised_data.locations, ~, regularised_data.locations_idx] = unique(regularised_data.location_data, 'rows');
        
        regularised_data.location_annotations = data.regularised.location_group;
        regularised_data.annotation_colors = lines(max(regularised_data.location_annotations) + 1);
        regularised_data.annotation_colors(1, :) = [0 0 0];
        
        time_days_unique = time_days(diff(time_days) ~= 0);

        weeks_start = time_days_unique(1);
        weeks_start = weeks_start - weekday(weeks_start) + 2; % Start on the previous Monday
        weeks_end = time_days_unique(end);
        weeks = ((0:floor((weeks_end - weeks_start) / 7)) * 7) + weeks_start;

        string_weeks = cell(1, length(weeks));

        for i = 1:length(weeks)
            string_weeks{i} = ['Week ' num2str(i) ' (Week Beginning ' datestr(weeks(i), 'dd-mmm-yyyy') ')'];
            if sum(ismember(time_days, (0:6) + weeks(i))) > 0
                string_weeks{i} = [string_weeks{i} ' *'];
            end
        end

        set(handles.cboWeek, 'String', string_weeks);

        handles.user_data.data_path = path;
        handles.user_data.original_data = original_data;
        handles.user_data.validated_data = validated_data;
        handles.user_data.regularised_data = regularised_data;
        handles.user_data.time_days = time_days;
        handles.user_data.weeks = weeks;

        scatter_colormap = hot(1100);
        scatter_colormap = flipud(scatter_colormap);
        scatter_colormap = scatter_colormap(100:end, :);
        handles.user_data.scatter_colormap = scatter_colormap;

        handles = plot_week(1, handles);

        ud = handles.user_data; %#ok<NASGU>
    else
        set(handles.cboWeek, 'String', 'No data');
        fprintf('  No data loaded\n');
    end
    
    handles_out = handles;


function handles_out = plot_week(week_no, handles)
    ud = handles.user_data;
    
    handles.user_data.current_week = week_no;

    if week_no == 1
        set(handles.cmdPrevWeek, 'Enable', 'off')
    else
        set(handles.cmdPrevWeek, 'Enable', 'on')
    end
    
    if week_no == length(ud.weeks)
        set(handles.cmdNextWeek, 'Enable', 'off')
    else
        set(handles.cmdNextWeek, 'Enable', 'on')
    end
    
    if isfield(handles.user_data, 'figure_handle_primary') && ...
            ishandle(handles.user_data.figure_handle_primary)
        figure(handles.user_data.figure_handle_primary);
    else
        handles.user_data.figure_handle_primary = figure;
    end
    
    plot_week_data(week_no, handles, ud.original_data);
    
    if isfield(handles.user_data, 'figure_handle_secondary') && ...
            ishandle(handles.user_data.figure_handle_secondary)
        figure(handles.user_data.figure_handle_secondary);
    else
        handles.user_data.figure_handle_secondary = figure;
    end
    
    plot_week_data(week_no, handles, ud.regularised_data);
    
    alldays = ud.weeks(week_no) + (0:6);

    figure(handles.main_window);
    cla;
    
    data = ud.original_data;
    
    week_data_idx = ismember(data.time_days, alldays);
    
    week_locations_idx = data.locations_idx(week_data_idx, :);
    week_location_time = data.location_time(week_data_idx);
    [week_locations_unique] = unique(week_locations_idx);
    week_locations_duration = zeros(size(week_locations_unique, 1), 1);
    
    for location_idx = 1:length(week_locations_unique)
        week_locations_duration(location_idx) = sum(week_location_time(week_locations_idx == week_locations_unique(location_idx)));
    end
    
    colormap(ud.scatter_colormap);
    scatter(data.locations(week_locations_unique, 1), data.locations(week_locations_unique, 2), [], week_locations_duration, 'MarkerFaceColor', 'flat');
    xlabel('Distance from mode (km)')
    ylabel('Distance from mode (km)')
    axis manual;
    colorbar;
    
    set(handles.cboWeek, 'Enable', 'on');

    handles_out = handles;


function plot_week_data(week_no, handles, data)
    ud = handles.user_data;
    
    firstday = ud.weeks(week_no);

    title_str = ['Week Beginning ' datestr(firstday, 'dd-mmm-yyyy')];
    if week_no > 1
        title_str = [title_str ' (day ' num2str(firstday - ud.time_days(1) + 1) ')'];
    end
    title_str = [title_str ' - ' data.name];
    set(handles.cboWeek, 'Value', week_no);

    clr = data.annotation_colors;
    
    for j = 0:6
        thisday = firstday + j;
        
        day_data_idx = (data.time_days == thisday & ~isnan(data.data));
        day_locations_idx = data.locations_idx(day_data_idx, :);
        day_locations_annotation = data.location_annotations(day_data_idx);
        
        subplot(7, 7, (j * 7) + (1:6));
        cla;
        
        stairs(data.time(day_data_idx) - thisday, data.data(day_data_idx), 'LineWidth', 2);
        hold on;
        scatter(data.time(day_data_idx) - thisday, data.data(day_data_idx), 200, clr(day_locations_annotation + 1, :), 'LineWidth', 2, 'Marker', 'x');
        dist_max = max(data.data(day_data_idx));
        dist_min = min(data.data(day_data_idx));
        if isempty(dist_max)
            dist_max = 0;
            dist_min = 0;
        end
        a = [0 1 max((dist_min - 0.5), 0) (dist_max + 0.5)];
        axis(a);
        axis manual;
        if j == 0
            title(title_str, 'FontSize', 30);
        end
        ylabel(datestr(thisday, 'ddd'), 'FontSize', 30);
        set(gca,'XTick', (0:3:24)/24)
        if j == 6
            set(gca,'XTickLabel', datestr((0:3:24)/24, 'hh:00'))
        else
            set(gca,'XTickLabel', [])
        end
        set(gca,'FontSize', 30)
        hold off;
        
        subplot(7, 7, (j * 7) + 7);
        cla;
        
        day_location_time = data.location_time(day_data_idx);
        [day_locations_unique] = unique(day_locations_idx);
        day_locations_duration = zeros(size(day_locations_unique, 1), 1);

        for location_idx = 1:length(day_locations_unique)
            day_locations_duration(location_idx) = sum(day_location_time(day_locations_idx == day_locations_unique(location_idx)));
        end

        colormap(ud.scatter_colormap);
        scatter(data.locations(day_locations_unique, 1), data.locations(day_locations_unique, 2), [], day_locations_duration, 'MarkerFaceColor', 'flat');
        axis manual;
        colorbar;
        caxis([0 1]);
    end

% --- Outputs from this function are returned to the command line.
function varargout = preprocessed_location_visualiser_OutputFcn(hObject, eventdata, handles) %#ok<*INUSL>
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in cmdNextWeek.
function cmdNextWeek_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cmdNextWeek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');
drawnow;
handles = plot_week(handles.user_data.current_week + 1, handles);
guidata(hObject, handles);

% --- Executes on button press in cmdPrevWeek.
function cmdPrevWeek_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cmdPrevWeek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');
drawnow;
handles = plot_week(handles.user_data.current_week - 1, handles);
guidata(hObject, handles);


% --- Executes on selection change in cboWeek.
function cboWeek_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cboWeek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');
drawnow;
handles = plot_week(get(hObject,'Value'), handles);
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function cboWeek_CreateFcn(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to cboWeek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function txtRootPath_Callback(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to txtRootPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtRootPath as text
%        str2double(get(hObject,'String')) returns contents of txtRootPath as a double


% --- Executes during object creation, after setting all properties.
function txtRootPath_CreateFcn(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to txtRootPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtParticipantFilter_Callback(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to txtParticipantFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtParticipantFilter as text
%        str2double(get(hObject,'String')) returns contents of txtParticipantFilter as a double


% --- Executes during object creation, after setting all properties.
function txtParticipantFilter_CreateFcn(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to txtParticipantFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cmdLoadFiles.
function cmdLoadFiles_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cmdLoadFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
root_path = get(handles.txtRootPath, 'String');
filter = get(handles.txtParticipantFilter, 'String');

set(handles.lstParticipants, 'String', '');
set(handles.cboWeek, 'String', ' ');
set(handles.cboWeek, 'Value', 1);

set(handles.lstParticipants, 'Enable', 'off');
set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');

figure(handles.main_window);
cla;
colorbar off;

drawnow;

if length(filter) == 1
    filter = '*';
end

if (isempty(root_path)) || ((root_path(end) ~= '/') && (root_path(end) ~= '\'))
    root_path = [root_path '/'];
end

files = dir([root_path filter]);

participants = cell(1, length(files) + 1);
participants{1} = '';
j = 2;

for file = files'
    if file.name(1) ~= '.' && file.isdir
        participants{j} = file.name;
        j = j + 1;
    end
end

if j > 2
    set(handles.lstParticipants, 'String', participants(1:(j-1)));
    set(handles.lstParticipants, 'Value', 1);
    set(handles.lstParticipants, 'Enable', 'on');

    handles.user_data.root_path = root_path;
else
    handles.user_data.root_path = '';
end

% Update handles structure
guidata(hObject, handles);


function handles_out = select_participant(handles)
    participant_id = get(handles.lstParticipants, 'Value');
    participant_string = get(handles.lstParticipants, 'String');
    participant = participant_string{participant_id};
    
    if ~isempty(participant)
        set(handles.cboWeek, 'String', 'Loading...');
        set(handles.cboWeek, 'Value', 1);
        drawnow;

        path = [handles.user_data.root_path participant '/'];
        
        handles = load_data(path, handles);
    end
    
    handles_out = handles;

% --- Executes on selection change in lstParticipants.
function lstParticipants_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lstParticipants (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.lstParticipants, 'Enable', 'off');
set(handles.cboWeek, 'String', ' ');
set(handles.cboWeek, 'Value', 1);

set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');

figure(handles.main_window);
cla;
colorbar off;

drawnow;

handles = select_participant(handles);
set(handles.lstParticipants, 'Enable', 'on');
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function lstParticipants_CreateFcn(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to lstParticipants (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
