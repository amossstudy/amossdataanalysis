function data_filtered = kalman_filter_location_v2(data_week_start, data_measured, R, Q_weeks, train_idx)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 13-May-2016

    data_filtered = NaN(numel(data_measured), 1);

    P = 1;

    last_i = 0;

    for i = 1:numel(data_measured)
        weeks_diff = inf;
        if (i > 1) && (last_i > 0)
            weeks_diff = (data_week_start(i) - data_week_start(last_i)) / 7;
        end
        if i == 1 || weeks_diff >= 10
            data_filtered(i) = data_measured(i);
        else
            Q = Q_weeks(weeks_diff);

            x = data_filtered(last_i);

            P_ = P + Q;

            K = P_ / (P_ + R);

            x = x + K * (data_measured(i) - x);

            data_filtered(i) = x;

            if train_idx(i)
                P = P_ - K * P_;
            end
        end

        if train_idx(i)
            last_i = i;
        end
    end
end
