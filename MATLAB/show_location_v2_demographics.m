function show_location_v2_demographics(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 10-Aug-2016

    [~, classification_properties] = load_features_location_v2(varargin{:});
    
    params = get_configuration('features_location_v2', varargin{:});
    
    assert(numel(classification_properties.participant) > 0, 'No data loaded.');
    
    valid_qids = ~isnan(classification_properties.QIDS);
    valid_class = ismember(classification_properties.class, 0:1);
    
    test_participants = classification_properties.participant(valid_qids & valid_class);
    test_participant_class = classification_properties.class(valid_qids & valid_class);
    test_qids = classification_properties.QIDS(valid_qids & valid_class);
    qids_11_idx = test_qids >= 11;
    test_type = 'Valid';
    test_type_title_participants = 'with Valid Week(s)';

    cohort_dist = zeros(1, numel(unique(test_participant_class)));
    cohort_dist_str = '';
    
    for c = 0:1
        cohort_dist(c + 1) = sum(test_participant_class == c);
        cohort_dist_str = sprintf('%s%s: %i; ', cohort_dist_str, params.test_description{c + 2}, cohort_dist(c + 1));
    end

    fprintf('%s weeks: %i  (%s)\n', test_type, sum(cohort_dist), cohort_dist_str(1:end-2));

    participants_unique = unique(test_participants);
    participants_unique_class = zeros(size(participants_unique));

    for i = 1:numel(participants_unique)
        participants_unique_class(i) = test_participant_class(find(strcmp(test_participants, participants_unique{i}), 1, 'first'));
    end

    cohort_dist = zeros(1, numel(unique(participants_unique_class)));
    
    for c = 0:1
        cohort_dist(c + 1) = sum(participants_unique_class == c);
    end

    fprintf('Total participants %s: %i\n', test_type_title_participants, sum(cohort_dist));
    
    for c = 0:1
        this_c_participants = unique(test_participants((test_participant_class == c)));
        for q = 0:2
            if q == 0
                q_desc = 'all participants ';
                
                this_q_participants = this_c_participants;
            elseif q == 1
                q_desc = sprintf('without QIDS %s 11', char(8805));
                
                this_q_participants = unique(test_participants((test_participant_class == c) & qids_11_idx));
                
                if numel(this_q_participants) == 0
                    break;
                end
                
                this_c_participants_idx = ismember(this_c_participants, this_q_participants);
                
                this_q_participants = this_c_participants(~this_c_participants_idx);
                
            elseif q == 2
                q_desc = sprintf('with QIDS %s 11   ', char(8805));
                
                this_q_participants = unique(test_participants((test_participant_class == c) & qids_11_idx));
            end
            
            this_dist = numel(this_q_participants);
            this_age_cell = cell(size(this_q_participants));
            this_gender_cell = cell(size(this_q_participants));
            this_weight_cell = cell(size(this_q_participants));
            this_height_cell = cell(size(this_q_participants));
            this_bmi_cell = cell(size(this_q_participants));
            this_employment_status_cell = cell(size(this_q_participants));
            this_valid_weeks = NaN(size(this_q_participants));
            
            for i = 1:numel(this_q_participants)
                props = get_participant_property(this_q_participants{i}, [], varargin{:});
                if isfield(props, 'age')
                    this_age_cell{i} = props.age;
                end
                if isfield(props, 'gender')
                    this_gender_cell{i} = props.gender;
                end
                if isfield(props, 'weight')
                    this_weight_cell{i} = props.weight;
                end
                if isfield(props, 'height')
                    this_height_cell{i} = props.height;
                end
                if isfield(props, 'bmi')
                    this_bmi_cell{i} = props.bmi;
                end
                if isfield(props, 'employment_status')
                    if isempty(props.employment_status)
                        this_employment_status_cell{i} = 0;
                    else
                        this_employment_status_cell{i} = props.employment_status;
                    end
                else
                    this_employment_status_cell{i} = 0;
                end
                
                this_valid_weeks(i) = sum(strcmp(test_participants, this_q_participants{i}));
            end
            
            this_age = [this_age_cell{:}];
            this_gender = [this_gender_cell{:}];
            this_weight = [this_weight_cell{:}];
            this_height = [this_height_cell{:}];
            this_bmi = [this_bmi_cell{:}];
            this_employment_status = [this_employment_status_cell{:}];
            
            str = sprintf('  %s %s : %i ', params.test_description{c + 2}, q_desc, this_dist);
            
            fprintf('%s( ', str);
            fprintf('%i male; %i female )\n', sum(this_gender == 1), sum(this_gender == 2));
            fprintf(repmat(' ', 1, numel(str)));
            fprintf('( age %.0f%s%.0f', mean(this_age), char(177), std(this_age));
            fprintf('; weight %.0f%s%.0f', mean(this_weight), char(177), std(this_weight));
            fprintf('; height %.0f%s%.0f', mean(this_height), char(177), std(this_height));
            fprintf('; BMI %.1f%s%.1f', mean(this_bmi), char(177), std(this_bmi));
            fprintf(' )\n');
            fprintf('%s( ', repmat(' ', 1, numel(str)));
            for j = 1:numel(params.employment_status_id)
                if j > 1
                    fprintf('; ');
                end
                fprintf('%i %s', sum(this_employment_status == params.employment_status_id(j)), params.employment_status_description{j});
            end
            fprintf(' )\n');
            fprintf('%s( ', repmat(' ', 1, numel(str)));
            fprintf('%i valid weeks; %.1f%s%.1f', sum(this_valid_weeks), mean(this_valid_weeks), char(177), std(this_valid_weeks));
            fprintf(' )\n');
        end
    end
end
