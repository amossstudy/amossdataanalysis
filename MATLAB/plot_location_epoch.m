function plot_location_epoch(data, epochs, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 05-May-2015

    time_days = floor(data.time(data.time >= datenum([2014, 01, 01])));
    
    if isfield(data, 'location_group')
        location_annotations = data.location_group;
        clr = lines(max(location_annotations) + 1);
        clr(1, :) = [0 0 0];
    else
        location_annotations = zeros(size(data.data));
        clr = [0 0 0];
    end
    
    first_epoch_start = min([epochs.start]);
    last_epoch_end = max([epochs.end]);
    
    title_str = ['Epochs ' datestr(first_epoch_start, 'dd-mmm-yyyy') ' to ' datestr(last_epoch_end, 'dd-mmm-yyyy')];
    
    epoch_count = size(epochs, 2);
    max_epoch_length = 0;
    
    for epoch = epochs
        if ~isempty(epoch.start) && ~isempty(epoch.end)
            max_epoch_length = max(max_epoch_length, ceil(epoch.end - epoch.start + 1));
        end
    end
    
    episodes_types = get_configuration('episode_types');
    
    for j = 1:epoch_count
        if isempty(epochs(j).start)
            continue;
        end
        
        firstday = epochs(j).start;
        thisepoch = epochs(j).start:epochs(j).end;
        
        week_data_idx = (ismember(time_days, thisepoch) & ~isnan(data.data));
        week_locations_annotation = location_annotations(week_data_idx);
        
        subplot(epoch_count, 1, j);
        cla;
        
        hold on;
        
        if ismember(epochs(j).type, fieldnames(episodes_types))
            patch([0 0 max_epoch_length max_epoch_length], [-0.5 10.5 10.5 -0.5], episodes_types.(epochs(j).type).episode_colour, 'edgecolor', 'none')
        end
        
        for x = 0:max_epoch_length
            if (x < max_epoch_length) && (ismember(weekday(firstday + x), [1 7]))
                patch([0 0 1 1] + x, [-0.5 10.5 10.5 -0.5], [0.9 0.9 0.9], 'edgecolor', 'none', 'facealpha', 0.5)
            end
            if x > 0
                plot([x x], [-0.5 10.5], 'color', [0.5 0.5 0.5]);
            end
        end
        
        for y = [0 5 10]
            plot([0 max_epoch_length], [y y], 'color', [0.5 0.5 0.5]);
        end
        
        for i = 0:(max_epoch_length - 1)
            day_data_idx = week_data_idx & (time_days == (firstday + i));
            stairs(data.time(day_data_idx) - firstday, min(data.data(day_data_idx), 10.5), 'LineWidth', 1, 'Color', lines(1));
        end
        scatter(data.time(week_data_idx) - firstday, min(data.data(week_data_idx), 10.5), 10, clr(week_locations_annotation + 1, :), 'LineWidth', 1, 'Marker', 'x');
        
        if isfield(epochs, 'valid') && ~isempty(epochs(j).valid) && ~epochs(j).valid
            plot([0 max_epoch_length], [-0.5 10.5], 'color', [1 0 0], 'LineWidth', 3);
            plot([0 max_epoch_length], [10.5 -0.5], 'color', [1 0 0], 'LineWidth', 3);
        end
        
        a = [0 max_epoch_length -0.5 10.5];
        axis(a);
        set(gca, 'ylim', [-0.5 10.5]);
        axis manual;
        if j == 1
            title(title_str);
        end
        
        ylabel(datestr(firstday, 'dd-mmm'));
        
        x_ticks = get(gca,'XTick');
        x_ticks = x_ticks(1:end-1);
        set(gca,'XTick', x_ticks + 0.5);
        
        if j < find(~cellfun(@isempty, {epochs.start}), 1, 'last')
            set(gca,'XTickLabel', []);
        else
            set(gca,'XTickLabel', x_ticks + 1);
            xlabel('Day(s) into epoch')
        end
        
        p = get(gca, 'Position');
        p_diff = p(4) * 0.2;
        p(4) = p(4) + p_diff;
        p(2) = p(2) - ((epoch_count - j) * (p_diff / (epoch_count - 1)));
        set(gca, 'Position', p);

        hold off;
    end
    
    p = get(gcf, 'Position');
    p(3) = 1.5 * p(3);
    p(4) = 1.5 * p(4);
    set(gcf, 'Position', p);
    
    p = get(gcf, 'PaperPosition');
    p(3) = 1.5 * p(3);
    p(4) = 1.5 * p(4);
    set(gcf, 'PaperPosition', p);
end
