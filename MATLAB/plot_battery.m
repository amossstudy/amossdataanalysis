function plot_battery(data, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    if isempty(data.time) || (length(data.time) == 1 && data.time == 0)
        data_min = 0;
        data_max = 1;
    else
        data_min = floor(data.time(1));
        data_max = ceil(data.time(end));
    end
    data_days = data_max - data_min;
    
    t_min = get_argument({'tmin', 't_min'}, data_min, varargin{:});
    t_max = get_argument({'tmax', 't_max'}, data_max - 1, varargin{:});
    
    [xticks, xticks_str] = get_xticks(t_min, t_max, varargin{:});
    
    xticks = xticks - data_min + 1;
    
    yticks_time = zeros(5, 1);
    
    for i=0:4
        yticks_time(i + 1) = addtodate(data_min, i * 12, 'hour');
    end
    
    yticks_time(5) = addtodate(yticks_time(5), -1, 'minute');
    
    yticks_str = datestr(yticks_time, 'HH:MM');
    
    t_count_day = 24 * 60 * 60;
    
    if data.epoch.min > 0
        t_count_day = floor(t_count_day / 60);
        t_count_day = floor(t_count_day / data.epoch.min);
    elseif data.epoch.sec > 0
        t_count_day = floor(t_count_day / data.epoch.sec);
    end
    
    yticks = [(t_count_day / 24) (t_count_day / 2) t_count_day (t_count_day * 3 / 2) (t_count_day * 2)];
    
    data_mat = NaN(t_count_day * 2, data_days);
    
    for i=1:data_days
        day_start = ((i - 1) * t_count_day) + 1;
        day_end = min((i + 1) * t_count_day, length(data.data));
        data_mat(1:(day_end - day_start + 1), i) = data.data(day_start:day_end);
    end
    
    data_nan = isnan(data_mat);
    
    % restrict values to between 0 and 1.
    data_mat = min(data_mat, 1);
    data_mat = max(data_mat, 0);
    
    data_mat = (data_mat * 63) + 2;
    
    data_mat(data_nan) = 1;
    
    figure
    image(data_mat);
    set(gca,'YDir','normal')
    set(gca,'XTick',xticks)
    set(gca,'XTickLabel',xticks_str)
    set(gca,'YTick',yticks)
    set(gca,'YTickLabel',yticks_str)
    
    title('Battery Level', 'FontSize', 30);
    
    colormap('summer');
    c = colormap;
    c(1,:) = [1 1 1];
    colormap(c);
    
    hcb = colorbar;
    
    colour_ticks = [0 25 50 75 100];
    
    set(hcb,'YTick',colour_ticks * 62 / 100 + 2);
    set(hcb,'YTickLabel',colour_ticks)
    set(hcb,'YLim',[1.5 64.5])
    ylabel(hcb,'Battery level (%)', 'FontSize', 30)
    
    set(gca, 'FontSize', 30);
    
    p = get(gcf, 'Position');
    p(3) = 3 * p(3);
    set(gcf, 'Position', p);
    
    p = get(gcf, 'PaperPosition');
    p(3) = 3 * p(3);
    set(gcf, 'PaperPosition', p);
    
    a = axis();
    a(1) = t_min - floor(data_min) + 0.5;
    a(2) = t_max - floor(data_min) + 1.5;
    axis(a);
end