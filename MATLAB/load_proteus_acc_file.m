function data = load_proteus_acc_file(file)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 4-Nov-2014

    fid = fopen(file, 'r');
    
    if (fid == -1)
        errordlg(['Could not open file ' file], 'Error', 'modal');
        return;
    end

    % read first line of data to check format
    typestr = fgets(fid);
    
    fclose(fid);
    
    if typestr == -1
        data.steps = [];
        data.mean_x = [];
        data.mean_y = [];
        data.mean_z = [];
        data.magnitude = [];
        data.ref_angle = [];
        data.time = [];
    else
        fid = fopen(file, 'r');

        fields = regexp(typestr, ',', 'split');

        if length(fields) == 6,
            % Read date and time separately from time zone hour and minute.
            tmp = textscan(fid, '%23s%3f%2f,%f,%f,%f,%f,%f', 'delimiter', '\n');
        else
            errordlg(['Invalid activity file ' file], 'Error', 'modal');
            tmp = cell(8,1);
        end
        fclose(fid);

        t = datenum(tmp{1}, 'yyyy-mm-dd HH:MM:SS.FFF');

        %%%%%
        % This adjusts the date as per the time zone based on 
        % https://github.com/maximosipov/actant/blob/master/formats/localtime.m
        % I don't think that this is actually required, but left just in case.
        %zh_num = tmp{2}/(24);
        %zm_num = tmp{3}/(24*60) .* sign(zh_num);
        %
        %t = t - zh_num - zm_num;
        %%%%%

        % Although this is a larage data set, it is almost sorted with only a
        % few exceptions so this doesn't add much computation.
        [t, t_sort_order] = sort(t);

        data.steps = tmp{4}(t_sort_order);
        data.mean_x = tmp{5}(t_sort_order);
        data.mean_y = tmp{6}(t_sort_order);
        data.mean_z = tmp{7}(t_sort_order);
        data.magnitude = sqrt(sum([data.mean_x data.mean_y data.mean_z].^2,2));
        data.ref_angle = tmp{8}(t_sort_order);
        data.time = t;
    end
end