function visualise_data_overview_all_true_colours(varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 28-Dec-2014

    close all;
    
    OUTPUT_SUB_DIR = 'data-quantity';
    
    files = get_participants(varargin{:});
    
    data_dates = datenum([2014, 1, 1]):datenum([2015, 3, 1]);
    
    questionaires = {'ALTMAN', 'EQ_5D', 'GAD_7', 'QIDS'};
    
    for this_questionaire_cell = questionaires
        this_questionaire = this_questionaire_cell{1};
        this_questionaire_str = strrep(this_questionaire, '_', '\_');
        this_questionaire_lc = lower(this_questionaire);
        
        data_matrix = NaN(length(files), length(data_dates));
        data_matrix_class = cell(3, 1);
        for i = 1:3
          data_matrix_class{i} = struct('data', NaN(length(files), length(data_dates)), 'j', 1);
        end

        data_dates_weekly = data_dates - weekday(data_dates) + 2;
        data_matrix_weekly = NaN(length(files), length(data_dates_weekly));

        j = 1;

        for file = files'
            fprintf('Loading %s\n', file.name)

            class = get_participant_property(file.name, 'classification', varargin{:});

            if ~isempty(class)
                data_tc = load_true_colours_preprocessed(file.name);

                if ~isempty(data_tc) && isfield(data_tc, this_questionaire) && isfield(data_tc.(this_questionaire), 'regularised') && isfield(data_tc.(this_questionaire), 'weekly')
                    A = ismember(data_dates, data_tc.(this_questionaire).regularised.time);
                    B = ismember(data_tc.(this_questionaire).regularised.time, data_dates);

                    data_matrix(j, A) = data_tc.(this_questionaire).regularised.data(B);

                    data_matrix_class{class + 1}.data(data_matrix_class{class + 1}.j, A) = data_tc.(this_questionaire).regularised.data(B);

                    for w = data_tc.(this_questionaire).weekly.time
                        data_matrix_weekly(j, ismember(data_dates_weekly, w)) = data_tc.(this_questionaire).weekly.data(data_tc.(this_questionaire).weekly.time == w);
                    end
                end
                
                data_matrix_class{class + 1}.j = data_matrix_class{class + 1}.j + 1;
                j = j + 1;
            end
        end

        t_min = data_dates(1);
        t_max = data_dates(end);

        [xticks, xticks_str] = get_xticks(t_min, t_max, varargin{:});

        data_matrix(isnan(data_matrix)) = -1;
        data_matrix_weekly(isnan(data_matrix)) = -1;

        data_range = 1:(j - 1);
        data_order = data_range;

        if strcmp(this_questionaire, 'ALTMAN') == 1
            q_max = 20;
        elseif strcmp(this_questionaire, 'EQ_5D') == 1
            q_max = 100;
        elseif strcmp(this_questionaire, 'GAD_7') == 1
            q_max = 21;
        elseif strcmp(this_questionaire, 'QIDS') == 1
            q_max = 27;
        end
        
        clr = hot(ceil(q_max * 1.1));
        
        if strcmp(this_questionaire, 'EQ_5D') == 1
            clr(1, :) = 1;
            clr = clr(1:(q_max + 1), :);
        else
            clr = flipud(clr(1:(q_max + 1), :));
            clr(1, :) = 1;
        end

        figure;
        imagesc(data_dates, data_range, data_matrix(data_order, :));
        colormap(clr);
        set(gca, 'xtick', xticks);
        set(gca, 'xticklabel', xticks_str);
        set(gca, 'ytick', []);
        set(gca, 'clim', [-1.5 (q_max + 0.5)]);
        hcb = colorbar;
        set(hcb, 'ylim', [-0.5, (q_max + 0.5)]);
        ylabel(hcb, [this_questionaire_str ' Score']);

        save_figure([this_questionaire_lc '_score_all'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

        figure;
        imagesc(data_dates, data_range, data_matrix_weekly(data_order, :));
        colormap(clr);
        set(gca, 'xtick', xticks);
        set(gca, 'xticklabel', xticks_str);
        set(gca, 'ytick', []);
        set(gca, 'clim', [-1.5 (q_max + 0.5)]);
        hcb = colorbar;
        set(hcb, 'ylim', [-0.5, (q_max + 0.5)]);
        ylabel(hcb, [this_questionaire_str ' Score']);

        save_figure([this_questionaire_lc '_score_weekly_all'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

        hcb_pos = get(hcb, 'Position');

        figure;

        for i = 1:3
            data_range_class = 1:(data_matrix_class{i}.j - 1);
            data_order_class = data_range_class;
            %data_order = data_order(randperm(length(data_order)));

            subplot(3, 6, ((i - 1) * 6) + (1:5));
            imagesc(data_dates, data_range_class, data_matrix_class{i}.data(data_order_class, :));
            colormap(clr);

            set(gca, 'xtick', xticks);
            if i == 3
              set(gca, 'xticklabel', xticks_str);
            else
              set(gca, 'xticklabel', []);
            end
            set(gca, 'ytick', []);
            set(gca, 'clim', [-1.5 (q_max + 0.5)]);
            if i == 1
                ylabel('HC');
            elseif i == 2
                ylabel('BD');
            elseif i == 3
                ylabel('BPD');
            end
            p = get(gca, 'Position');
            p_diff = p(4) * 0.15;
            p(4) = p(4) + p_diff;
            if i == 1
                p(2) = p(2) - p_diff;
            elseif i < 3
                p(2) = p(2) - (p_diff / 2);
            end
            set(gca, 'Position', p);
        end

        subplot(3, 6, 6:6:(3*6));

        hcb = colorbar;
        set(hcb, 'Position', hcb_pos);
        set(gca, 'clim', [-1.5 (q_max + 0.5)]);
        set(hcb, 'ylim', [-0.5, (q_max + 0.5)]);
        ylabel(hcb, [this_questionaire_str ' Score']);
        set(hcb, 'AxisLocation', 'in'); % Ticks and label should be on the right.
        axis off

        save_figure([this_questionaire_lc '_score_class'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

        figure;
        
        d_hist = 1;
        
        if strcmp(this_questionaire, 'EQ_5D') == 1
            d_hist = 5;
        end

        for i = 1:3
            data_range_class = 1:(data_matrix_class{i}.j - 1);
            data_class = data_matrix_class{i}.data(data_range_class, :);

            subplot(3, 1, i);
            histogram(data_class(:), (-d_hist / 2):d_hist:(q_max + (d_hist / 2)));

            if i < 3
              set(gca, 'xticklabel', []);
            end
            set(gca, 'ytick', []);
            set(gca, 'xlim', [(-d_hist / 2), (q_max + (d_hist / 2))]);
            if i == 1
                title([this_questionaire_str ' Scores']);
                ylabel('HC');
            elseif i == 2
                ylabel('BD');
            elseif i == 3
                ylabel('BPD');
                xlabel([this_questionaire_str ' Score']);
            end
            p = get(gca, 'Position');
            p_diff = p(4) * 0.15;
            p(4) = p(4) + p_diff;
            if i == 1
                p(2) = p(2) - p_diff;
            elseif i < 3
                p(2) = p(2) - (p_diff / 2);
            end
            set(gca, 'Position', p);
        end

        save_figure([this_questionaire_lc '_score_hist'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

        figure;

        d_hist = 2;
        
        if strcmp(this_questionaire, 'EQ_5D') == 1
            d_hist = 5;
        end
        
        for i = 1:3
            data_range_class = 1:(data_matrix_class{i}.j - 1);
            data_class = data_matrix_class{i}.data(data_range_class, :);

            subplot(3, 1, i);
            histogram(nanmean(data_class, 2), (-d_hist / 2):d_hist:(ceil(q_max / 2) * 2) + (d_hist / 2));

            if i < 3
              set(gca, 'xticklabel', []);
            end
            set(gca, 'ytick', []);
            set(gca, 'xlim', [(-d_hist / 2), ((ceil(q_max / 2) * 2) + (d_hist / 2))]);
            if i == 1
                title(['Participant ' this_questionaire_str ' Score Mean']);
                ylabel('HC');
            elseif i == 2
                ylabel('BD');
            elseif i == 3
                ylabel('BPD');
                xlabel(['Mean ' this_questionaire_str ' Score']);
            end
            p = get(gca, 'Position');
            p_diff = p(4) * 0.15;
            p(4) = p(4) + p_diff;
            if i == 1
                p(2) = p(2) - p_diff;
            elseif i < 3
                p(2) = p(2) - (p_diff / 2);
            end
            set(gca, 'Position', p);
        end

        save_figure([this_questionaire_lc '_score_mean_hist'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

        figure;

        d_hist = 1;
        max_hist = 10;
        
        if strcmp(this_questionaire, 'EQ_5D') == 1
            d_hist = 2;
            max_hist = 30;
        end
        
        for i = 1:3
            data_range_class = 1:(data_matrix_class{i}.j - 1);
            data_class = data_matrix_class{i}.data(data_range_class, :);

            subplot(3, 1, i);
            histogram(nanstd(data_class, 0, 2), (-d_hist / 2):d_hist:(max_hist + (d_hist / 2)));

            if i < 3
              set(gca, 'xticklabel', []);
            end
            set(gca, 'ytick', []);
            set(gca, 'xlim', [(-d_hist / 2), (max_hist + (d_hist / 2))]);
            if i == 1
                title(['Participant ' this_questionaire_str ' Score Standard Deviation']);
                ylabel('HC');
            elseif i == 2
                ylabel('BD');
            elseif i == 3
                ylabel('BPD');
                xlabel([this_questionaire_str ' Score Standard Deviation']);
            end
            p = get(gca, 'Position');
            p_diff = p(4) * 0.15;
            p(4) = p(4) + p_diff;
            if i == 1
                p(2) = p(2) - p_diff;
            elseif i < 3
                p(2) = p(2) - (p_diff / 2);
            end
            set(gca, 'Position', p);
        end

        save_figure([this_questionaire_lc '_score_sd_hist'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

        figure;

        d_hist = 2;
        max_hist = 14;
        
        if strcmp(this_questionaire, 'EQ_5D') == 1
            d_hist = 5;
            max_hist = 50;
        end
        
        for i = 1:3
            data_range_class = 1:(data_matrix_class{i}.j - 1);
            data_class = data_matrix_class{i}.data(data_range_class, :);

            subplot(3, 1, i);
            histogram(iqr(data_class, 2), (-d_hist / 2):d_hist:(max_hist + (d_hist / 2)));

            if i < 3
              set(gca, 'xticklabel', []);
            end
            set(gca, 'ytick', []);
            set(gca, 'xlim', [(-d_hist / 2), (max_hist + (d_hist / 2))]);
            if i == 1
                title(['Participant ' this_questionaire_str ' Score IQR']);
                ylabel('HC');
            elseif i == 2
                ylabel('BD');
            elseif i == 3
                ylabel('BPD');
                xlabel([this_questionaire_str ' Score IQR']);
            end
            p = get(gca, 'Position');
            p_diff = p(4) * 0.15;
            p(4) = p(4) + p_diff;
            if i == 1
                p(2) = p(2) - p_diff;
            elseif i < 3
                p(2) = p(2) - (p_diff / 2);
            end
            set(gca, 'Position', p);
        end

        save_figure([this_questionaire_lc '_score_iqr_hist'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

        if strcmp(this_questionaire, 'QIDS') == 1
            data_matrix_severity = data_matrix;
            data_matrix_severity(ismember(data_matrix_severity, 0:5)) = 0;
            data_matrix_severity(ismember(data_matrix_severity, 6:10)) = 1;
            data_matrix_severity(ismember(data_matrix_severity, 11:15)) = 2;
            data_matrix_severity(ismember(data_matrix_severity, 16:20)) = 3;
            data_matrix_severity(ismember(data_matrix_severity, 21:27)) = 4;

            data_matrix_severity_weekly = data_matrix_weekly;
            data_matrix_severity_weekly(ismember(data_matrix_severity_weekly, 0:5)) = 0;
            data_matrix_severity_weekly(ismember(data_matrix_severity_weekly, 6:10)) = 1;
            data_matrix_severity_weekly(ismember(data_matrix_severity_weekly, 11:15)) = 2;
            data_matrix_severity_weekly(ismember(data_matrix_severity_weekly, 16:20)) = 3;
            data_matrix_severity_weekly(ismember(data_matrix_severity_weekly, 21:27)) = 4;

            clr = flipud(hot(6));

            figure;
            imagesc(data_dates, data_range, data_matrix_severity(data_order, :));
            colormap(clr);
            set(gca, 'xtick', xticks);
            set(gca, 'xticklabel', xticks_str);
            set(gca, 'ytick', []);
            set(gca, 'clim', [-1.5 4.5]);
            hcb = colorbar;
            set(hcb, 'ylim', [-0.5, 4.5]);
            set(hcb, 'ytick', 0:4);
            ylabel(hcb, [this_questionaire_str ' Severity']);

            save_figure([this_questionaire_lc '_severity_all'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

            figure;
            imagesc(data_dates, data_range, data_matrix_severity_weekly(data_order, :));
            colormap(clr);
            set(gca, 'xtick', xticks);
            set(gca, 'xticklabel', xticks_str);
            set(gca, 'ytick', []);
            set(gca, 'clim', [-1.5 4.5]);
            hcb = colorbar;
            set(hcb, 'ylim', [-0.5, 4.5]);
            set(hcb, 'ytick', 0:4);
            ylabel(hcb, [this_questionaire_str ' Severity']);

            save_figure([this_questionaire_lc '_severity_weekly_all'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        end
    end
end
