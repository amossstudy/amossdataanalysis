function extract_episodes(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 27-Mar-2015

    questionaires = {'ALTMAN', 'QIDS'};
    
    files = get_participants(varargin{:});
    
    for file=files'
        fprintf('Extracting episodes for %s\n', file.name)
        
        data = load_true_colours_data(file.name, varargin{:});

        t_min = inf;
        t_max = 0;

        if ~isempty(data) && isstruct(data) && ~isempty(fields(data))
            for this_questionaire_cell = questionaires
                this_questionaire = strrep(this_questionaire_cell{:}, '-', '_');

                if isfield(data, this_questionaire) && isfield(data.(this_questionaire), 'data')
                    this_questionnaire_time = data.(this_questionaire).time;
                    if ~isempty(this_questionnaire_time)
                        t_min = min(t_min, floor(min(this_questionnaire_time)));
                        t_max = max(t_max, ceil(max(this_questionnaire_time)));
                    end
                end
            end

            episodes = struct();
            episodes.manic = false(t_max - t_min, 1);
            episodes.depressed = false(t_max - t_min, 1);
            episodes.manic_severity = NaN(t_max - t_min, 1);
            episodes.depressed_severity = NaN(t_max - t_min, 1);

            for this_questionaire_cell = questionaires
                this_questionaire = strrep(this_questionaire_cell{:}, '-', '_');

                if strcmp(this_questionaire, 'ALTMAN') == 1
                    episode_name = 'manic';
                    episode_threshold = 6;
                    episode_min_length = 7;
                    severity_thresholds = [0 NaN 6 10];
                elseif strcmp(this_questionaire, 'QIDS') == 1
                    episode_name = 'depressed';
                    episode_threshold = 11;
                    episode_min_length = 14;
                    severity_thresholds = [0 6 11 16];
                end

                severity_name = [episode_name '_severity'];

                if isfield(data, this_questionaire) && isfield(data.(this_questionaire), 'data')
                    data_valid = data.(this_questionaire).data > -1;

                    this_questionnaire_data = data.(this_questionaire).data(data_valid);
                    this_questionnaire_time = data.(this_questionaire).time(data_valid);
                    this_questionnaire_time_diff = [inf; diff(floor(this_questionnaire_time))];

                    i = length(this_questionnaire_data);
                    while i >= 1
                        if this_questionnaire_data(i) < episode_threshold
                            episode_start = ceil(this_questionnaire_time(i)) - min(max(this_questionnaire_time_diff(i) - 1, 0), 6);

                            severity = find(this_questionnaire_data(i) >= severity_thresholds, 1, 'last') - 1;

                            episodes.(severity_name)(max(1, (episode_start - t_min)):(ceil(this_questionnaire_time(i)) - t_min)) = severity;

                            i = i - 1;
                        else
                            episode_end = ceil(this_questionnaire_time(i));
                            episode_start = floor(this_questionnaire_time(i));

                            while this_questionnaire_data(i) >= episode_threshold
                                episode_start = ceil(this_questionnaire_time(i)) - min(max(this_questionnaire_time_diff(i) - 1, 0), 6);

                                severity = find(this_questionnaire_data(i) >= severity_thresholds, 1, 'last') - 1;

                                episodes.(severity_name)(max(1, (episode_start - t_min)):(ceil(this_questionnaire_time(i)) - t_min)) = severity;

                                i = i - 1;

                                if this_questionnaire_time_diff(i + 1) >= 28
                                    break;
                                end
                            end

                            if (episode_end - episode_start + 1) >= episode_min_length
                                episodes.(episode_name)(max(1, (episode_start - t_min)):(episode_end - t_min)) = true;
                            end
                        end
                    end
                end

                episodes.time = (t_min:(t_max - 1))';
            end

            save_data([file.name '-episodes'], episodes, varargin{:});
        else
            fprintf('  No data loaded\n');
        end
    end
end