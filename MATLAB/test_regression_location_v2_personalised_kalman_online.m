function test_regression_location_v2_personalised_kalman_online(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 07-Apr-2016

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/regression/personalised/';
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    data_full = data_raw';
    
    kalman_params = get_kalman_filter_params_location_v2(varargin{:});
    
    FEATURES = size(data_full, 2);
    
    params = get_configuration('features_location_v2', varargin{:});
    
    variant_out = get_argument({'variant_output', 'variantoutput', 'variant_out', 'variantout', 'output_variant', 'outputvariant'}, '', varargin{:});

    if ~isempty(variant_out)
        variant_suffix = ['_' variant_out];
    else
        variant_suffix = '';
    end
    
    use_old_cv = get_argument({'use_old_cv', 'useoldcv'}, false, varargin{:});
    old_cv_variant = get_argument({'old_cv_variant', 'oldcvvariant'}, '', varargin{:});
    
    continue_last = get_argument({'continue_last', 'continuelast'}, false, varargin{:});
    
    valid_qids = ~isnan(classification_properties.QIDS);

    unique_participants = unique(classification_properties.participant(valid_qids));

    if use_old_cv
        regression_data_old = load_regression_data_location_v2_personalised('variant', old_cv_variant);
    end
    
    if continue_last
        regression_data = load_regression_data_location_v2_personalised('variant', variant_out);
        
        if isfield(regression_data.model.full, 'pred') && ~iscell(regression_data.model.full.pred)
            regression_data.model.full = rmfield(regression_data.model.full, 'pred');
        end
    else
        regression_data = struct();
        regression_data.id = cell(numel(unique_participants), 1);
        regression_data.qids = struct();
        regression_data.qids.data = cell(numel(unique_participants), 1);
        regression_data.qids.count = NaN(numel(unique_participants), 1);
        regression_data.qids.min = NaN(numel(unique_participants), 1);
        regression_data.qids.max = NaN(numel(unique_participants), 1);
        regression_data.qids.range = NaN(numel(unique_participants), 1);
        regression_data.qids.mean = NaN(numel(unique_participants), 1);
        regression_data.qids.std = NaN(numel(unique_participants), 1);
        regression_data.qids.med = NaN(numel(unique_participants), 1);
        regression_data.qids.iqr = NaN(numel(unique_participants), 1);
        regression_data.class = NaN(numel(unique_participants), 1);
        regression_data.model = struct();
        regression_data.model.full = struct();
        regression_data.model.full.B = cell(numel(unique_participants), 1);
        regression_data.model.full.FitInfo = cell(numel(unique_participants), 1);
        regression_data.model.full.opt_fitted_model = cell(numel(unique_participants), 1);
        regression_data.model.full.pred = cell(numel(unique_participants), 1);
        regression_data.model.min_deviance_idx = NaN(numel(unique_participants), 1);
        regression_data.model.opt_lambda = NaN(numel(unique_participants), 1);
        regression_data.model.loo = cell(numel(unique_participants), 1);
        regression_data.results = struct();
        regression_data.results.pred = cell(numel(unique_participants), 1);
        regression_data.results.mae = NaN(numel(unique_participants), 1);
        regression_data.results.rmse = NaN(numel(unique_participants), 1);

        for i_p = 1:numel(unique_participants)
            this_participant = unique_participants{i_p};

            regression_data.id{i_p} = this_participant;

            this_participant_qids = classification_properties.QIDS(strcmp(classification_properties.participant, this_participant));

            regression_data.qids.count(i_p) = sum(~isnan(this_participant_qids));
            regression_data.qids.min(i_p) = nanmin(this_participant_qids);
            regression_data.qids.max(i_p) = nanmax(this_participant_qids);
            regression_data.qids.range(i_p) = nanmax(this_participant_qids) - nanmin(this_participant_qids);
            regression_data.qids.mean(i_p) = nanmean(this_participant_qids);
            regression_data.qids.std(i_p) = nanstd(this_participant_qids);
            regression_data.qids.med(i_p) = nanmedian(this_participant_qids);
            regression_data.qids.iqr(i_p) = iqr(this_participant_qids);
        end

        regression_data.data = struct();
        regression_data.data.week_start = cell(numel(unique_participants), 1);
        regression_data.data.data_unnormalised = cell(numel(unique_participants), 1);
        regression_data.data.data_normalised = cell(numel(unique_participants), 1);
        regression_data.data.data_filtered = cell(numel(unique_participants), 1);
    end
    
    [~, std_idx] = sort(regression_data.qids.std, 'descend');
    
    data_normalised = data_full;
    
    full_mean = nanmean(data_normalised, 1);
    full_std = nanstd(data_normalised, 1);
    
    for i = 1:FEATURES
        data_normalised(:, i) = data_normalised(:, i) - nanmean(data_normalised(:, i));
        data_normalised(:, i) = data_normalised(:, i) / nanstd(data_normalised(:, i));
    end
    
    data_filename = ['regression-location-v2-personalised-' datestr(now, 'yyyymmddhhMMss') variant_suffix];
    
    for participant_idx = std_idx'
        fprintf(' Processing participant %i of %i (%s): ', find(std_idx == participant_idx), numel(std_idx), regression_data.id{participant_idx});
        
        if continue_last && ~isnan(regression_data.class(participant_idx))
            fprintf('Skipping.\n');
            continue;
        end
        
        if (regression_data.qids.count(participant_idx) <= 5) || ...
                (regression_data.qids.range(participant_idx) <= 3)
            fprintf('Skipping.\n');
            continue;
        end
        
        participant_class = unique(classification_properties.class(strcmp(classification_properties.participant, regression_data.id{participant_idx})));

        regression_data.class(participant_idx) = participant_class;
        
        tic;
        
        participant_data_idx = find(strcmp(classification_properties.participant, regression_data.id{participant_idx}));
        
        week_start = classification_properties.week_start(participant_data_idx);
        
        [week_start_sorted, week_start_sort_idx] = sort(week_start);
        
        participant_data_idx = participant_data_idx(week_start_sort_idx);
        
        week_start_full = week_start_sorted(1):7:week_start_sorted(end);
        
        data_unnormalised_full = NaN(numel(week_start_full), size(data_normalised, 2));
        data_normalised_full = NaN(numel(week_start_full), size(data_normalised, 2));
        data_filtered_full = NaN(numel(week_start_full), size(data_normalised, 2));
        
        valid_weeks = ismember(week_start_full, week_start_sorted);
        
        qids_full = NaN(numel(week_start_full), 1);
        qids_full(valid_weeks) = classification_properties.QIDS(participant_data_idx);
        
        regression_data.qids.data{participant_idx} = qids_full;
        
        for i = 1:FEATURES
            data_unnormalised_full(valid_weeks, i) = data_full(participant_data_idx, i);
            data_normalised_full(valid_weeks, i) = data_normalised(participant_data_idx, i);
            
            this_data_measured = data_full(participant_data_idx, i);
            this_data_week_start = week_start_sorted(~isnan(this_data_measured));
            this_data_measured = this_data_measured(~isnan(this_data_measured));
            this_data_filtered = kalman_filter_location_v2(this_data_week_start, this_data_measured, kalman_params.R(i), kalman_params.Q(i, :), true(size(this_data_week_start)));
            
            data_filtered_full(valid_weeks, i) = (this_data_filtered - full_mean(i)) / full_std(i);
        end
        
        regression_data.data.week_start{participant_idx} = week_start_full;
        regression_data.data.data_unnormalised{participant_idx} = data_unnormalised_full;
        regression_data.data.data_normalised{participant_idx} = data_normalised_full;
        regression_data.data.data_filtered{participant_idx} = data_filtered_full;
        
        features_to_classify = true(1, size(data_filtered_full, 2));
        
        data_valid = sum(isnan(data_filtered_full(:, features_to_classify)), 2) == 0;
        qids_data_valid = ~isnan(qids_full) & data_valid;
        
        cv_opt_lambda = NaN;
        
        if use_old_cv
            participant_old_idx = find(strcmp(regression_data_old.id, regression_data.id{participant_idx}));
            if ~isempty(participant_old_idx) && ~isnan(regression_data_old.class(participant_old_idx))
                cv_opt_lambda = regression_data_old.model.opt_lambda(participant_old_idx);
                if ~isnan(cv_opt_lambda)
                    regression_data.model.full.B{participant_idx} = regression_data_old.model.full.B{participant_old_idx};
                    regression_data.model.full.FitInfo{participant_idx} = regression_data_old.model.full.FitInfo{participant_old_idx};
                    regression_data.model.min_deviance_idx(participant_idx) = regression_data_old.model.min_deviance_idx(participant_old_idx);
                    regression_data.model.opt_lambda(participant_idx) = regression_data_old.model.opt_lambda(participant_old_idx);
                end
            end
        end
        
        if isnan(cv_opt_lambda)
            [B_full, FitInfo_full] = lassoglm(data_filtered_full(qids_data_valid, features_to_classify), qids_full(qids_data_valid) / 27, 'binomial', 'CV', min(sum(qids_data_valid), 10), 'NumLambda', 10, 'MCReps', 5);

            cv_index_min_deviance = FitInfo_full.IndexMinDeviance;

            if cv_index_min_deviance > (numel(FitInfo_full.Lambda) * 0.9)
                [~, cv_index_min_deviance] = min(FitInfo_full.Deviance(1:floor((numel(FitInfo_full.Lambda) * 0.9))));
            end

            cv_opt_lambda = FitInfo_full.Lambda(cv_index_min_deviance);

            regression_data.model.full.B{participant_idx} = B_full;
            regression_data.model.full.FitInfo{participant_idx} = FitInfo_full;
            regression_data.model.min_deviance_idx(participant_idx) = cv_index_min_deviance;
            regression_data.model.opt_lambda(participant_idx) = cv_opt_lambda;
            
            model_fitted_full = [FitInfo_full.Intercept; B_full];
            model_fitted_full = model_fitted_full(:, cv_index_min_deviance);
        else
            [B, FitInfo] = lassoglm(data_filtered_full(qids_data_valid, features_to_classify), qids_full(qids_data_valid) / 27, 'binomial', 'Lambda', cv_opt_lambda);
            
            model_fitted_full = [FitInfo.Intercept; B];
        end
        
        regression_data.model.full.opt_fitted_model{participant_idx} = model_fitted_full;
        
        pred = NaN(size(week_start_full));
        
        pred(qids_data_valid) = glmval(model_fitted_full, data_filtered_full(qids_data_valid, features_to_classify), 'logit') * 27;
        
        if isfield(regression_data.model.full, 'pred')
            regression_data.model.full.pred{participant_idx} = pred;
        end
        
        figure;
        h = plot_regression_location_v2_personalised(week_start_full, data_filtered_full, qids_full, pred, varargin{:});
        title(h{1}, sprintf('%s (%s)', regression_data.id{participant_idx}, params.test_description{regression_data.class(participant_idx) + 2}));
        save_figure(sprintf('full_fit_%s', regression_data.id{participant_idx}), 'output_sub_dir', OUTPUT_SUB_DIR, 'font_size', 14, varargin{:});
        
        pred = NaN(size(week_start_full));
        
        regression_data.model.loo{participant_idx} = struct();
        regression_data.model.loo{participant_idx}.B = cell(size(qids_data_valid));
        regression_data.model.loo{participant_idx}.FitInfo = cell(size(qids_data_valid));
        regression_data.model.loo{participant_idx}.fitted_model = cell(size(qids_data_valid));
        
        for i = 1:numel(qids_data_valid)
            this_qids_data_valid = qids_data_valid;
            this_qids_data_valid(i:end) = false;
            
            if sum(this_qids_data_valid) >= 2
                this_data_filtered_full = NaN(numel(week_start_full), size(data_normalised, 2));

                for f = 1:FEATURES
                    this_data_measured = data_full(participant_data_idx, f);
                    this_data_week_start = week_start_sorted(~isnan(this_data_measured));
                    this_data_measured = this_data_measured(~isnan(this_data_measured));
                    this_data_train = true(size(this_data_week_start));
                    this_data_train(this_data_week_start >= week_start_full(i)) = false;

                    this_data_filtered = kalman_filter_location_v2(this_data_week_start, this_data_measured, kalman_params.R(f), kalman_params.Q(f, :), this_data_train);

                    this_data_filtered_full(valid_weeks, f) = (this_data_filtered - full_mean(f)) / full_std(f);
                end
                
                [B, FitInfo] = lassoglm(this_data_filtered_full(this_qids_data_valid, features_to_classify), qids_full(this_qids_data_valid) / 27, 'binomial', 'Lambda', cv_opt_lambda);

                model_fitted = [FitInfo.Intercept; B];

                regression_data.model.loo{participant_idx}.B{i} = B;
                regression_data.model.loo{participant_idx}.FitInfo{i} = FitInfo;
                regression_data.model.loo{participant_idx}.fitted_model{i} = model_fitted;

                pred(i) = glmval(model_fitted, this_data_filtered_full(i, features_to_classify), 'logit') * 27;
            end
        end
        
        pred_error = qids_full(qids_data_valid) - pred(qids_data_valid)';
        pred_error = pred_error(~isnan(pred_error));
        
        regression_data.results.pred{participant_idx} = pred;
        regression_data.results.mae(participant_idx) = mean(abs(pred_error));
        regression_data.results.rmse(participant_idx) = sqrt(mean(pred_error .^ 2));
        
        figure
        h = plot_regression_location_v2_personalised(week_start_full, data_filtered_full, qids_full, pred, varargin{:});
        title(h{1}, sprintf('%s (%s)', regression_data.id{participant_idx}, params.test_description{regression_data.class(participant_idx) + 2}));
        save_figure(sprintf('loo_fit_%s', regression_data.id{participant_idx}), 'output_sub_dir', OUTPUT_SUB_DIR, 'font_size', 14, varargin{:});
        
        save_data(data_filename, regression_data, varargin{:});
        
        toc;
    end
    
    save_data(data_filename, regression_data, varargin{:});
end
