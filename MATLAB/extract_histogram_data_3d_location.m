function [hist_data, hist_data_abs] = extract_histogram_data_3d_location(location_time, location_data, x_centers, y_centers)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 15-Jul-2015

    [plot_data_locations, ~, plot_data_locations_idx] = unique(location_data, 'rows');

    hist_data_abs = NaN(numel(y_centers), numel(x_centers));

    dx = x_centers(2) - x_centers(1);
    dy = y_centers(2) - y_centers(1);
    
    x_bin_min = (x_centers - (dx / 2));
    x_bin_max = (x_centers + (dx / 2));
    y_bin_min = (y_centers - (dy / 2));
    y_bin_max = (y_centers + (dy / 2));

    for loc_idx=1:size(plot_data_locations, 1)
        this_location = plot_data_locations(loc_idx, :);

        x_i = ((this_location(2) > x_bin_min) & (this_location(2) <= x_bin_max));
        y_i = ((this_location(1) > y_bin_min) & (this_location(1) <= y_bin_max));

        if isnan(hist_data_abs(y_i, x_i))
            hist_data_abs(y_i, x_i) = 0;
        end

        hist_data_abs(y_i, x_i) = hist_data_abs(y_i, x_i) + sum(location_time(plot_data_locations_idx == loc_idx));
    end

    hist_data = hist_data_abs / nansum(nansum(hist_data_abs));
end
