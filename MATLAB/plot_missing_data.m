function plot_missing_data(x, y, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 29-Apr-2016

    if sum(isnan(y)) > 0
        y_invalid_idx = find(isnan(y))';

        prev_valid_y_idx = y_invalid_idx([2 diff(y_invalid_idx)] > 1) - 1;
        next_valid_y_idx = y_invalid_idx([diff(y_invalid_idx) 2] > 1) + 1;

        if prev_valid_y_idx(1) == 0
            prev_valid_y_idx = prev_valid_y_idx(2:end);
            next_valid_y_idx = next_valid_y_idx(2:end);
        end
        if (numel(next_valid_y_idx) > 0) && (next_valid_y_idx(end) == numel(x) + 1);
            prev_valid_y_idx = prev_valid_y_idx(1:end-1);
            next_valid_y_idx = next_valid_y_idx(1:end-1);
        end

        prev_next_x = reshape([x(prev_valid_y_idx); x(next_valid_y_idx); nan(1, numel(next_valid_y_idx))], 3 * numel(next_valid_y_idx), 1)';
        prev_next_y = reshape([y(prev_valid_y_idx)'; y(next_valid_y_idx)'; nan(1, numel(next_valid_y_idx))], 3 * numel(next_valid_y_idx), 1)';

        plot(prev_next_x, prev_next_y, varargin{:});
    end
end