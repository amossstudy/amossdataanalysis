function [best_result] = test_feature_selection(features, class, description, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 26-Dec-2014

    MAX_TESTS = 6;
    
    k = get_argument({'kfold', 'k-fold'}, 0, varargin{:});
    complete_repetitions = get_argument('repetitions', 1, varargin{:});
    tests = get_argument({'tests', 'runtests', 'run_tests'}, 1:MAX_TESTS, varargin{:});
    
    use_libsvm = false;
    
    try
        msg = evalc('svmtrain');
        
        if (length(msg) > 6) && (strcmp(msg(1:6), 'Usage:'))
            use_libsvm = true;
        end
    catch err %#ok<NASGU>
        % not using libsvm
    end
    
    use_libsvm = get_argument({'uselibsvm', 'use_libsvm'}, use_libsvm, varargin{:});
    
    FEATURES = size(features, 2);
    CLASSES = length(unique(class));
    
    original_class = class;
    
    unique_classes = sort(unique(class));
    class_count = zeros(size(unique_classes));
    
    for i = 1:CLASSES
        class_count(i) = sum(class == unique_classes(i));
    end
    
    resampling_repetitions = 1;
    
    % Allow 20% unbalance between max and min class
    if (min(class_count) / max(class_count)) < 0.8
        resampling_repetitions = ceil(max(class_count) / min(class_count));
    end
    
    repetitions = complete_repetitions * resampling_repetitions;
    
    train_test_types = {'test', 'train'};
    
    classifier_results = struct();
    
    for train_test_cell = train_test_types
        classifier_results.(train_test_cell{:}) = struct();
        classifier_results.(train_test_cell{:}).classification_accuracy = NaN(length(tests), FEATURES, repetitions);
        classifier_results.(train_test_cell{:}).sensitivity = NaN(length(tests), FEATURES, repetitions);
        classifier_results.(train_test_cell{:}).specificity = NaN(length(tests), FEATURES, repetitions);
    end
    
    classifier_test_description = repmat({''}, 1, length(tests));
    
    svm_opts = repmat({{'Standardize', true, 'Solver', 'SMO'}}, length(tests), 1);
    libsvm_opts = repmat({'-q '}, length(tests), 1);

    for i = 1:length(tests)
        switch tests(i)
            case 1
                classifier_test_description{i} = 'SVM (linear)';
                svm_opts{i} = [svm_opts{i}, {'KernelFunction', 'linear'}];
                libsvm_opts{i} = [libsvm_opts{i} '-t 0'];
            case 2
                classifier_test_description{i} = 'SVM (polynomial order 2)';
                svm_opts{i} = [svm_opts{i}, {'KernelFunction', 'polynomial', 'PolynomialOrder', 2}];
                libsvm_opts{i} = [libsvm_opts{i} '-t 1 -d 2'];
            case 3
                classifier_test_description{i} = 'SVM (polynomial order 3)';
                svm_opts{i} = [svm_opts{i}, {'KernelFunction', 'polynomial', 'PolynomialOrder', 3}];
                libsvm_opts{i} = [libsvm_opts{i} '-t 1 -d 3'];
            case 4
                classifier_test_description{i} = 'SVM (polynomial order 4)';
                svm_opts{i} = [svm_opts{i}, {'KernelFunction', 'polynomial', 'PolynomialOrder', 4}];
                libsvm_opts{i} = [libsvm_opts{i} '-t 1 -d 4'];
            case 5
                classifier_test_description{i} = 'SVM (RBF)';
                svm_opts{i} = [svm_opts{i}, {'KernelFunction', 'rbf'}];
                libsvm_opts{i} = [libsvm_opts{i} '-t 2'];
            case 6
                classifier_test_description{i} = 'Logistic regression';
        end
    end
    
    best_result = struct('accuracy', 0);
    
    for r = 1:repetitions
        if repetitions > 1
            fprintf(' Repetition %i of %i (%i%%)\n', r, repetitions, round(((r - 1) / repetitions) * 100));
        end
        if resampling_repetitions > 1
            valid_classes = false(size(original_class));
            for i = 1:CLASSES
                if class_count(i) > min(class_count)
                    this_class_idx = find(original_class == unique_classes(i));
                    this_class_idx = this_class_idx(randperm(length(this_class_idx)));
                    valid_classes(this_class_idx(1:min(class_count))) = true;
                else
                    valid_classes(original_class == unique_classes(i)) = true;
                end
            end
            class = original_class(valid_classes);
        end
        if k == 0
            % LOO CV
            train_spec = ~eye(length(class));
        else
            % k-fold CV
            train_spec = false(length(class), k);
            k_spec = [repmat(1:k, 1, floor(length(class) / k)) 1:rem(length(class), k)];
            k_spec = k_spec(randperm(length(class)));
            for i = 1:k
                train_spec(:, i) = (k_spec ~= i);
            end
        end
        
        for i = 1:length(tests)
            test = tests(i);
            
            fprintf('  Running test %i of %i (%s)\n', i, length(tests), classifier_test_description{i});

            last_str = '';

            for f = 1:FEATURES
                if ~isempty(last_str)
                    fprintf(repmat('\b', 1, length(last_str) - 1));
                end

                last_str = sprintf('    Feature %i of %i (%i%%%%)', f, FEATURES, round(((f - 1) / FEATURES) * 100));
                fprintf(last_str);

                test_features = features(:, 1:f);

                classes = unique(class);
                
                perf = classperf(class, 'Positive', classes(classes > 0), 'Negative', 0);
                perf_train = classperf(class, 'Positive', classes(classes > 0), 'Negative', 0);
                
                classification_results = NaN(length(class), size(train_spec, 2));

                for j = 1:size(train_spec, 2)
                    train_idx = train_spec(:, j);
                    test_idx = ~train_idx;

                    test_features_mean = mean(test_features(train_idx, :));
                    test_features_std = std(test_features(train_idx, :));
                    
                    normalised_features = test_features - repmat(test_features_mean, size(test_features, 1), 1);
                    normalised_features = normalised_features ./ repmat(test_features_std, size(test_features, 1), 1);

                    if ismember(test, 1:5)
                        if use_libsvm
                            svm_model = svmtrain(class(train_idx) + 1, normalised_features(train_idx, :), libsvm_opts{i}); %#ok<NASGU>
                            
                            [~, pred] = evalc('svmpredict(class(test_idx) + 1, normalised_features(test_idx, :), svm_model)');
                            [~, pred_train] = evalc('svmpredict(class(train_idx) + 1, normalised_features(train_idx, :), svm_model)');
                            
                            pred = pred - 1;
                            pred_train = pred_train - 1;
                        else
                            if CLASSES == 2
                                svm_model = fitcsvm(normalised_features(train_idx, :), class(train_idx), ...
                                         svm_opts{i}{:}, 'Prior', 'uniform');
                            else
                                svm_model = fitcecoc(normalised_features(train_idx, :), class(train_idx), ...
                                         'learner', templateSVM(svm_opts{i}{:}), 'Prior', 'uniform');
                            end
                            
                            pred = predict(svm_model, normalised_features(test_idx, :));
                            pred_train = predict(svm_model, normalised_features(train_idx, :));
                        end
                    elseif test == 6
                        B = mnrfit(normalised_features(train_idx, :), class(train_idx) + 1);
                        
                        pred_prob = mnrval(B, normalised_features(test_idx, :));
                        [~, pred] = max(pred_prob, [], 2);
                        pred = pred - 1;
                        
                        pred_train_train = mnrval(B, normalised_features(train_idx, :));
                        [~, pred_train] = max(pred_train_train, [], 2);
                        pred_train = pred_train - 1;
                    end

                    perf = classperf(perf, pred, test_idx);
                    perf_train = classperf(perf_train, pred_train, train_idx);
                    
                    classification_results(test_idx, j) = pred;
                end

                classifier_results.test.classification_accuracy(i, f, r) = perf.CorrectRate;
                classifier_results.test.sensitivity(i, f, r) = perf.Sensitivity;
                classifier_results.test.specificity(i, f, r) = perf.Specificity;
                
                classifier_results.train.classification_accuracy(i, f, r) = perf_train.CorrectRate;
                classifier_results.train.sensitivity(i, f, r) = perf_train.Sensitivity;
                classifier_results.train.specificity(i, f, r) = perf_train.Specificity;
                
                if perf.CorrectRate > best_result.accuracy
                    best_result.accuracy = perf.CorrectRate;
                    best_result.sensitivity = perf.sensitivity;
                    best_result.specificity = perf.specificity;
                    best_result.method = classifier_test_description{i};
                    best_result.features_used = f;
                    best_result.class = class;
                    best_result.classification_results = classification_results;
                    best_result.counting_matrix = perf.CountingMatrix;
                end
            end
            if ~isempty(last_str)
                fprintf(repmat('\b', 1, length(last_str) - 1));
            end
        end
    end
    
    classifier_test_description = repmat(classifier_test_description, 1, length(tests) + length(train_test_types) - 1);
    
    for j = 1:length(train_test_types)
        if j > 1
            train_test_name = strrep(train_test_types{j}, '_', ' ');
            train_test_name(1) = upper(train_test_name(1));
            classifier_test_description{length(tests) + j - 1} = train_test_name;
        end
    end
    
    h_lines = NaN(length(tests) + length(train_test_types) - 1, 1);
    
    description_filename = lower(description);
    description_filename(isspace(description_filename)) = '_';
    description_filename(ismember(description_filename, '=')) = '_';
    
    clr_length = length(tests);
    if clr_length == 1
        clr_length = length(train_test_types);
    end
    clr = lines(clr_length);
    
    for result_type_cell = {'classification_accuracy', 'sensitivity', 'specificity'}
        figure;
        hold on;
        
        result_type_name = strrep(result_type_cell{:}, '_', ' ');
        result_type_name(1) = upper(result_type_name(1));
        result_type_name(find(result_type_name == ' ') + 1) = upper(result_type_name(find(result_type_name == ' ') + 1));
        
        for j = 1:length(train_test_types)
            line_style = '-';
            if j == 2
                line_style = '--';
            end
            
            if repetitions == 1
                for i = 1:length(tests)
                    h_line = plot(1:FEATURES, classifier_results.(train_test_types{j}).(result_type_cell{:})(i, :, :), ['x' line_style], 'color', clr(i, :));
                    
                    if j == 1
                        h_lines(i) = h_line;
                    elseif i == 1
                        h_lines(length(tests) + j - 1) = h_line;
                    end
                end
            else
                for i = 1:length(tests)
                    mean_val = mean(classifier_results.(train_test_types{j}).(result_type_cell{:})(i, :, :), 3);
                    min_val = mean_val - min(classifier_results.(train_test_types{j}).(result_type_cell{:})(i, :, :), [], 3);
                    max_val = max(classifier_results.(train_test_types{j}).(result_type_cell{:})(i, :, :), [], 3) - mean_val;
                    clr_idx = i;
                    if length(tests) == 1
                        clr_idx = j;
                    end
                    h_line = errorbar(1:FEATURES, mean_val, min_val, max_val, ['x' line_style], 'color', clr(clr_idx, :));
                    
                    if j == 1
                        h_lines(i) = h_line;
                    elseif i == 1
                        h_lines(length(tests) + j - 1) = h_line;
                    end
                end
            end
            
            filename_suffix = '';

            if ~strcmp(train_test_types{j}, 'test')
                filename_suffix = ['_' train_test_types{j}];
            end
            
            title(sprintf('%i-Class %s by Features Selected (%s)', CLASSES, result_type_name, description));
            legend(h_lines(1:(length(tests) + j - 1)), classifier_test_description{1:(length(tests) + j - 1)}, 'Location', 'SouthEast')
            set(gca, 'xlim', [0, 20])
            set(gca, 'ylim', [0, 1])
            xlabel('Features Presented to Classifier');
            ylabel(result_type_name);

            save_figure(sprintf('feature_selection_%i_class_%s_%s%s', CLASSES, description_filename, result_type_cell{:}, filename_suffix), varargin{:});
        end
    end
end

