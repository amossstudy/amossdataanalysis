function visualise_features_location_v2_individual_trends_offset(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 18-Nov-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats/individuals/trends/offset';
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    data_full = data_raw';
    
    FEATURES = size(data_full, 2);
    
    valid_qids = ~isnan(classification_properties.QIDS);
    
    params = get_configuration('features_location_v2', varargin{:});
    
    participant_id = classification_properties.participant(valid_qids);
    participant_qids_score = classification_properties.QIDS(valid_qids);
    participant_class = classification_properties.class(valid_qids);
    
    data_normalised = data_full(valid_qids, :);
    
    for i = 1:FEATURES
        data_normalised(:, i) = data_normalised(:, i) - nanmean(data_normalised(:, i));
        data_normalised(:, i) = data_normalised(:, i) / nanstd(data_normalised(:, i));
    end
    
    unique_participants = unique(participant_id);
    
    max_points = 0;
    
    for participant_cell = unique_participants'
        max_points = max(max_points, sum(strcmp(participant_id, participant_cell)));
    end
    
    data_values = NaN(length(unique_participants), max_points, FEATURES + 1);
    data_class = NaN(length(unique_participants), 1);
    data_range = NaN(length(unique_participants), FEATURES + 1);
    data_points = NaN(length(unique_participants), 1);
    
    for p_i = 1:numel(unique_participants)
        participant_data_idx = strcmp(participant_id, unique_participants{p_i});
        
        for i = 1:(FEATURES + 1)
            if i == 1
                all_points = participant_qids_score(participant_data_idx);
            else
                all_points = data_normalised(participant_data_idx, i - 1);
            end
            
            data_values(p_i, 1:numel(all_points), i) = all_points;
            
            data_range(p_i, i) = range(all_points);
        end
        
        data_class(p_i) = participant_class(find(participant_data_idx, 1, 'first'));
        data_points(p_i) = sum(participant_data_idx);
    end
    
    classes_unique = unique(data_class);
    
    first_class_idx = zeros(numel(classes_unique), 1);
    
    for c_i = 1:numel(classes_unique);
        first_class_idx(c_i) = find(data_class == classes_unique(c_i), 1, 'first');
    end
    
    valid_participants_idx = (data_points >= 5) & (data_range(:, 1) >= 5) & ismember(data_class, [0 1]);
    
    regression_model_fun = @(a, b, x) max(0, min(27, a + (b .* x)));
    
    for i = 1:FEATURES
        figure;
        
        participant_corr = NaN(numel(unique_participants), 1);
        
        for p_i = find(valid_participants_idx)'
            participant_corr(p_i) = corr(data_values(p_i, :, 1)', data_values(p_i, :, i + 1)', 'type', 'Spearman', 'rows', 'complete');
        end
        
        histogram(participant_corr, -1:0.2:1);
        
        title([params.feature_name{i} ' Correlation']);
        xlabel('Spearman''s \rho');
        ylabel('Participants');
        
        save_figure(sprintf('individual_feature_qids_corr_%.2i', i), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 20, varargin{:});
        
        for r = 0:2
            figure;
            hold on;

            valid_participants = find(valid_participants_idx);
            valid_corr = participant_corr(valid_participants_idx);
            [~, sort_idx] = sort(abs(valid_corr), 'descend');

            j_max = 10;
            y_offset = j_max;
            j = 1;

            clr = lines(j_max);

            y_max = -inf;
            y_min = inf;
            for p_i = valid_participants(sort_idx(1:j_max))'
                if ~isnan(participant_corr(p_i)) && abs(participant_corr(p_i)) > 0.25

                    this_qids = data_values(p_i, :, 1);
                    this_feature = data_values(p_i, :, i + 1);

                    [~, sort_order] = sort(this_qids);

                    x = this_qids(sort_order);
                    y = this_feature(sort_order);

                    y_plot = y - nanmedian(y) + y_offset;

                    y_valid = ~isnan(y);

                    scatter(x, y_plot, 'x', 'MarkerEdgeColor', clr(j, :));

                    if r == 1
                        p = polyfit(x(y_valid)', y_plot(y_valid)', 1);

                        x_fit = [max([min(x) - 1, 0]) min([max(x) + 1, 27])];

                        plot(x_fit, polyval(p, x_fit), 'Color', clr(j, :), 'LineWidth', 1.5);
                    elseif r == 2
                        p = fit(y_plot(y_valid)', x(y_valid)', regression_model_fun, 'StartPoint', [nanmean(x) 0.5]);

                        y_fit = [min(y_plot) - 1, max(y_plot) + 1];

                        plot(regression_model_fun(p.a, p.b, y_fit), y_fit, 'Color', clr(j, :), 'LineWidth', 1.5);
                    end
                    
                    y_max = max([y_max, max(y_plot)]);
                    y_min = min([y_min, min(y_plot)]);

                    y_offset = y_offset - 2.5;
                    j = j + 1;
                end
            end
            hold off;

            set(gca, 'xlim', [0 27])
            set(gca, 'ylim', [y_min - 1 y_max + 1])
            set(gca, 'ytick', [])
            xlabel('QIDS Score');

            ylabel(params.feature_name{i});

            if r == 0
                filename_suffix = '_raw';
            elseif r == 1
                filename_suffix = '_dep_y';
            elseif r == 2
                filename_suffix = '_dep_x';
            end
            
            save_figure(sprintf('individual_feature_qids_trends_offset_%.2i%s', i, filename_suffix), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 20, varargin{:});
        end
    end
end
