function value = get_fitted_distribution_property(dist, property_name, data, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 23-Feb-2016

    if nargin < 2
        property_name = 'name';
    end
    
    assert(sum(strcmpi(property_name, {'pdf', 'pdf_int', 'name', 'description', 'formatted_parameters', 'line_color', 'line_style'})) > 0, 'Invalid distribution mode "%s"', property_name);
    
    bw = get_argument({'pdf_int_width', 'pdfintwidth'}, 1, varargin{:});
    
    unipdf = @(x, lower, upper) pdf(makedist('Uniform', 'lower', lower, 'upper', upper), x);
    unicdf = @(x, lower, upper) cdf(makedist('Uniform', 'lower', min(lower, upper), 'upper', max(lower, upper)), x);

    if strcmp(dist.type, 'exp') == 1
        if strcmpi(property_name, 'pdf')
            value = exppdf  (data + dist.offset, dist.mu);
        elseif strcmpi(property_name, 'pdf_int')
            value = expcdf  (data + dist.offset + (bw / 2), dist.mu)                - expcdf  (data + dist.offset - (bw / 2), dist.mu);
        elseif strcmpi(property_name, 'name')
            value = 'Exponential';
        elseif strcmpi(property_name, 'description')
            value = sprintf('Exponential: mu=%f', dist.mu);
        elseif strcmpi(property_name, 'formatted_parameters')
            value = ['\mu=' num2str(dist.mu, 2)];
        elseif strcmpi(property_name, 'line_color')
            value = [1 0 0];
        elseif strcmpi(property_name, 'line_style')
            value = '-';
        end
    elseif strcmp(dist.type, 'logn') == 1
        if strcmpi(property_name, 'pdf')
            value = lognpdf (data + dist.offset, dist.mu, dist.sigma);
        elseif strcmpi(property_name, 'pdf_int')
            value = logncdf (data + dist.offset + (bw / 2), dist.mu, dist.sigma)    - logncdf (data + dist.offset - (bw / 2), dist.mu, dist.sigma);
        elseif strcmpi(property_name, 'name')
            value = 'Lognormal';
        elseif strcmpi(property_name, 'description')
            value = sprintf('Lognormal: mu=%f, sigma=%f', dist.mu, dist.sigma);
        elseif strcmpi(property_name, 'formatted_parameters')
            value = ['\mu=' num2str(dist.mu, 2) ', \sigma=' num2str(dist.sigma, 2)];
        elseif strcmpi(property_name, 'line_color')
            value = [0 1 0];
        elseif strcmpi(property_name, 'line_style')
            value = '-';
        end
    elseif strcmp(dist.type, 'poiss') == 1
        if strcmpi(property_name, 'pdf') || strcmpi(property_name, 'pdf_int')
            value = poisspdf(data, dist.lambda);
        elseif strcmpi(property_name, 'name')
            value = 'Poisson';
        elseif strcmpi(property_name, 'description')
            value = sprintf('Poisson: lambda=%f', dist.lambda);
        elseif strcmpi(property_name, 'formatted_parameters')
            value = ['\lambda=' num2str(dist.lambda, 2)];
        elseif strcmpi(property_name, 'line_color')
            value = [0 0 1];
        elseif strcmpi(property_name, 'line_style')
            value = '-';
        end
    elseif strcmp(dist.type, 'uni') == 1
        if strcmpi(property_name, 'pdf')
            value = unipdf  (data + dist.offset, dist.lower, dist.upper);
        elseif strcmpi(property_name, 'pdf_int')
            value = unicdf  (data + dist.offset + (bw / 2), dist.lower, dist.upper) - unicdf  (data + dist.offset - (bw / 2), dist.lower, dist.upper);
        elseif strcmpi(property_name, 'name')
            value = 'Uniform';
        elseif strcmpi(property_name, 'description')
            value = sprintf('Uniform: lower=%f, upper=%f', dist.lower, dist.upper);
        elseif strcmpi(property_name, 'formatted_parameters')
            value = ['a=' num2str(dist.lower, 2) ', b=' num2str(dist.upper, 2)];
        elseif strcmpi(property_name, 'line_color')
            value = [1 1 0];
        elseif strcmpi(property_name, 'line_style')
            value = '-';
        end
    elseif strcmp(dist.type, 'gam') == 1
        if strcmpi(property_name, 'pdf')
            value = gampdf  (data + dist.offset, dist.a, dist.b);
        elseif strcmpi(property_name, 'pdf_int')
            value = gamcdf  (data + dist.offset + (bw / 2), dist.a, dist.b)         - gamcdf  (data + dist.offset - (bw / 2), dist.a, dist.b);
        elseif strcmpi(property_name, 'name')
            value = 'Gamma';
        elseif strcmpi(property_name, 'description')
            value = sprintf('Gamma: alpha=%f, beta=%f', dist.a, dist.b);
        elseif strcmpi(property_name, 'formatted_parameters')
            value = ['\alpha=' num2str(dist.a, 2) ', \beta=' num2str(dist.b, 2)];
        elseif strcmpi(property_name, 'line_color')
            value = [1 0 1];
        elseif strcmpi(property_name, 'line_style')
            value = '-';
        end
    elseif strcmp(dist.type, 'f') == 1
        if strcmpi(property_name, 'pdf')
            value = fpdf    (data + dist.offset, dist.v1, dist.v2);
        elseif strcmpi(property_name, 'pdf_int')
            value = fcdf    (data + dist.offset + (bw / 2), dist.v1, dist.v2)       - fcdf    (data + dist.offset - (bw / 2), dist.v1, dist.v2);
        elseif strcmpi(property_name, 'name')
            value = 'F';
        elseif strcmpi(property_name, 'description')
            value = sprintf('F: V1=%f, V2=%f', dist.v1, dist.v2);
        elseif strcmpi(property_name, 'formatted_parameters')
            value = ['\nu_1=' num2str(dist.v1, 2) ', \nu_2=' num2str(dist.v2, 2)];
        elseif strcmpi(property_name, 'line_color')
            value = [0 1 1];
        elseif strcmpi(property_name, 'line_style')
            value = '-';
        end
    elseif strcmp(dist.type, 'chi2') == 1
        if strcmpi(property_name, 'pdf')
            value = chi2pdf (data + dist.offset, dist.v);
        elseif strcmpi(property_name, 'pdf_int')
            value = chi2cdf (data + dist.offset + (bw / 2), dist.v)                 - chi2cdf (data + dist.offset - (bw / 2), dist.v);
        elseif strcmpi(property_name, 'name')
            value = 'Chi-square';
        elseif strcmpi(property_name, 'description')
            value = sprintf('Chi-square: V=%f', dist.v);
        elseif strcmpi(property_name, 'formatted_parameters')
            value = ['V=' num2str(dist.v, 2)];
        elseif strcmpi(property_name, 'line_color')
            value = [1 0 0];
        elseif strcmpi(property_name, 'line_style')
            value = '--';
        end
    elseif strcmp(dist.type, 'norm') == 1
        if strcmpi(property_name, 'pdf')
            value = normpdf (data + dist.offset, dist.mu, dist.sigma);
        elseif strcmpi(property_name, 'pdf_int')
            value = normcdf (data + dist.offset + (bw / 2), dist.mu, dist.sigma)    - normcdf (data + dist.offset - (bw / 2), dist.mu, dist.sigma);
        elseif strcmpi(property_name, 'name')
            value = 'Normal';
        elseif strcmpi(property_name, 'description')
            value = sprintf('Normal: mu=%f, sigma=%f', dist.mu, dist.sigma);
        elseif strcmpi(property_name, 'formatted_parameters')
            value = ['\mu=' num2str(dist.mu, 2) ', \sigma=' num2str(dist.sigma, 2)];
        elseif strcmpi(property_name, 'line_color')
            value = [0 0 1];
        elseif strcmpi(property_name, 'line_style')
            value = '-';
        end
    end
end
