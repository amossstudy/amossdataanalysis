function visualise_classification_location_v2_data_subsets_compare(compare_variants, varargin)
% Copyright (c) 2017, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 06-Mar-2017

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/classification/data_subsets';
    
    params = get_configuration('features_location_v2', varargin{:});
    
    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    
    show_change = get_argument({'showchange', 'show_change'}, false, varargin{:});
    
    show_horizontal_lines = get_argument({'showhorizontallines', 'show_horizontal_lines'}, false, varargin{:});
    
    show_weeks = get_argument({'showweeks', 'show_weeks'}, false, varargin{:});
    
    variant_desc = get_argument({'variantdesc', 'variant_desc', 'variantdescription', 'variant_description'}, {}, varargin{:});
    
    join_median = get_argument({'joinpoints', 'join_points', 'joinmedian', 'join_median'}, false, varargin{:});
    
    if show_change
        y_lim = get_argument({'ylim', 'y_lim'}, [-0.2 0.2], varargin{:});
    else
        y_lim = get_argument({'ylim', 'y_lim'}, [0.5 1], varargin{:});
    end
    
    assert(iscell(compare_variants) && numel(compare_variants) > 0, 'compare_variants must be a cell array of variants to compare.')
    
    variants = numel(compare_variants);
    
    classification_data = cell(variants, 1);
    
    Q = [];
    TESTS = 0;
    FEATURES = 0;
    
    REPS = 0;
    TESTS_TO_PERFORM = [];
    
    Q_TO_TEST = [];
    Q_EXTRA_TO_TEST = [];
    Q_EXTRA_MAX = 0;
    
    prediction_types = [];
    
    classes = [];
    
    str = repmat({''}, variants, 1);
    
    valid_variants = false(1, variants);
    
    for v_i = 1:variants
        classification_data{v_i} = load_classification_data_location_v2(varargin{:}, 'variant', compare_variants{v_i}, 'warn_not_found', true);
        
        str{v_i} = compare_variants{v_i};
        
        if numel(fieldnames(classification_data{v_i})) > 0 && ...
                isfield(classification_data{v_i}, 'parameters') && ...
                isfield(classification_data{v_i}, 'options') && ...
                isfield(classification_data{v_i}, 'results') && ...
                isfield(classification_data{v_i}.results, 'predictions')

            valid_variants(v_i) = true;

            Q = unique([Q classification_data{v_i}.parameters.q]);
            TESTS = max(TESTS, classification_data{v_i}.parameters.tests);
            FEATURES = max(FEATURES, classification_data{v_i}.parameters.features);

            REPS = max(REPS, classification_data{v_i}.options.reps);
            TESTS_TO_PERFORM = unique([TESTS_TO_PERFORM classification_data{v_i}.options.tests_performed]);

            Q_TO_TEST = unique([Q_TO_TEST classification_data{v_i}.options.q_tested]);
            Q_EXTRA_TO_TEST = unique([Q_EXTRA_TO_TEST classification_data{v_i}.options.q_extra_tested]);

            this_prediction_types = fieldnames(classification_data{v_i}.results.predictions);
            prediction_types = unique([prediction_types; this_prediction_types]);

            Q_EXTRA_MAX = max(Q_EXTRA_MAX, size(classification_data{v_i}.results.predictions.(this_prediction_types{1}), 2) - numel(classification_data{v_i}.parameters.q));
            
            classes = unique([classes classification_data{v_i}.participants.class']);
        end
    end
    
    if isempty(variant_desc)
        variant_desc = str;
    end
    
    Q_MAX = max(Q);
    
    q_all = [Q, Q_MAX + Q_EXTRA_MAX];
    
    data_boxplot_ac = cell(TESTS, numel(q_all));
    data_boxplot_f1 = cell(TESTS, numel(q_all));
    q_valid = false(TESTS, numel(q_all));
    
    test_participants_cohort = cell(TESTS, 1);
    for t = TESTS_TO_PERFORM
        test_participants_cohort{t} = NaN(variants, max(classes) + 1);
    end
    test_participants_q = cell(TESTS, numel(q_all));
    
    data_subsets = numel(params.data_subset_name);
    
    for v_i = find(valid_variants)
        if ismember(fieldnames(classification_data{v_i}.results.predictions), 'ind_features')
            [classification_params, classification_results] = preprocess_classification_data_location_v2(classification_data{v_i});
            for t = classification_params.TESTS_TO_PERFORM
                for c = classes
                    test_participants_cohort{t}(v_i, c + 1) = sum(classification_results.meta.participant_class_cohort{t} == c);
                end
                q_to_display = ismember(q_all, [6, 10, 11, classification_params.Q_MAX + classification_params.Q_EXTRA_TO_TEST]);
                for q = find(q_to_display & classification_results.meta.q_valid(t, :))
                    if ~q_valid(t, q)
                        q_valid(t, q) = true;
                        data_boxplot_ac{t, q} = NaN(FEATURES, (variants * (data_subsets + 2)) + 2);
                        data_boxplot_f1{t, q} = NaN(FEATURES, (variants * (data_subsets + 2)) + 2);
                        test_participants_q{t, q} = NaN(variants, 2);
                    end
                    for f = 1:(FEATURES / data_subsets)
                        for ft = 1:data_subsets
                            this_data_boxplot_data = classification_results.ind_features.accuracy_raw(t, q, f + ((ft - 1) * 10), :);
                            data_boxplot_ac{t, q}(f, ((v_i - 1) * (data_subsets + 2)) + ft + 2) = median(this_data_boxplot_data);
                            this_data_boxplot_data = classification_results.ind_features.f1_raw(t, q, f + ((ft - 1) * 10), :);
                            data_boxplot_f1{t, q}(f, ((v_i - 1) * (data_subsets + 2)) + ft + 2) = median(this_data_boxplot_data);
                        end
                    end
                    for c = 1:2
                        test_participants_q{t, q}(v_i, c) = sum(classification_results.meta.participant_class_q{t}(q, :) == c);
                    end
                end
            end
        end
    end
    
    label_opts = {};
    if latex_labels
        label_opts = {'Interpreter', 'latex'};
    end
    
    v_thresholds = (0:variants) * 7 + 1.5;
    v_steps = reshape([v_thresholds; v_thresholds], numel(v_thresholds) * 2, 1);
    v_steps = v_steps(2:end-1);
    
    for t = TESTS_TO_PERFORM
        q_to_display = ismember(q_all, [6, 10, 11, Q_MAX + Q_EXTRA_TO_TEST]);
        for q = find(q_to_display & q_valid(t, :))
            clr_boxplot = [0 0 0; 0 0 0; params.clr_data_subset];

            for ot = 1:2
                if ot == 1
                    data_boxplot = data_boxplot_ac{t, q};
                elseif ot == 2
                    data_boxplot = data_boxplot_f1{t, q};
                end
                
                if show_change
                    for i = 1:5
                        for j = 2:variants
                            fi = (j - 1) * 7 + i + 2;
                            data_boxplot(:, fi) = data_boxplot(:, fi) - data_boxplot(:, i + 2);
                        end
                    end
                end

                figure;

                boxplot(data_boxplot, 'Colors', clr_boxplot, 'plotstyle', 'compact');

                hold on;

                if show_change
                    v_min = 2;
                else
                    v_min = 1;
                end
                for f = v_min:(variants - 1)
                    plot([8.5 8.5] + (f - 1) * 7, [-1 1], 'Color', [0.5 0.5 0.5])
                end
                
                if show_horizontal_lines
                    if show_change
                        dy = 0.025;
                    else
                        dy = 0.05;
                    end
                    for y = -1:dy:1
                        if y > y_lim(1) && y < y_lim(2)
                            if show_change && y == 0
                                plot([1.5, (variants * 7) + 1.5], [y, y], 'Color', [0.5 0.5 0.5])
                            else
                                plot([1.5, (variants * 7) + 1.5], [y, y], ':', 'Color', [0.5 0.5 0.5])
                            end
                        end
                    end
                end

                hold off;

                h = findall(gca,'Tag','Box');
                legend(h(7:-1:3), params.data_subset_name, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 20, label_opts{:})

                if join_median
                    data_median = nanmedian(data_boxplot, 1);
                    
                    hold on;
                    
                    for ft = data_subsets:-1:1
                        idx = (ft + 2):(data_subsets + 2):((variants * (data_subsets + 2)) + 2);
                        this_median = data_median(idx);
                        valid_idx = ~isnan(this_median);
                        h = plot(idx(valid_idx), this_median(valid_idx), 'Color', clr_boxplot(ft + 2, :));
                        uistack(h, 'bottom');
                    end
                    
                    hold off;
                end
                
                set(gca, 'ylim', y_lim);
                if show_change
                    set(gca, 'xlim', [8.5, (variants * 7) + 1.5]);
                    set(gca, 'xtick', 12:7:(variants * 7));
                    set(gca, 'xticklabel', variant_desc(2:end), 'FontSize', 18);
                else
                    set(gca, 'xlim', [1.5, (variants * 7) + 1.5]);
                    set(gca, 'xtick', 5:7:(variants * 7));
                    set(gca, 'xticklabel', variant_desc, 'FontSize', 18);
                end

                %set(gca, 'XTickLabelRotation', 35)

                if q <= numel(classification_params.Q)
                    if latex_labels
                        this_q_desc = sprintf('QIDS $\\geq$ %i', classification_params.Q(q));
                    else
                        this_q_desc = sprintf('QIDS %s %i', char(8805), classification_params.Q(q));
                    end
                    this_q_filename_suffix_base = sprintf('qids_%.2i', classification_params.Q(q));
                elseif q == (numel(classification_params.Q) + 1)
                    this_q_desc = 'Cohort classification';
                    this_q_filename_suffix_base = 'cohort';
                end

                title(sprintf('Median Classification Results (%s, %s)', params.test_description{t}, this_q_desc), 'FontSize', 30, label_opts{:});

                ylabel_prefix = 'Classification';
                
                if show_change
                    ylabel_prefix = 'Change in classification';
                end
                if ot == 1
                    ylabel(sprintf('%s accuracy', ylabel_prefix), 'FontSize', 20, label_opts{:});
                    this_q_filename_suffix = sprintf('%s_ac', this_q_filename_suffix_base);
                elseif ot == 2
                    if latex_labels
                        ylabel(sprintf('%s F$_1$ Score', ylabel_prefix), 'FontSize', 20, label_opts{:});
                    else
                        ylabel(sprintf('%s F1 Score', ylabel_prefix), 'FontSize', 20, label_opts{:});
                    end
                    this_q_filename_suffix = sprintf('%s_f1', this_q_filename_suffix_base);
                end

                if latex_ticks
                    set(gca, 'TickLabelInterpreter', 'latex')
                end

                p = get(gcf, 'PaperPosition');
                p(3) = 2 * p(3);
                set(gcf, 'PaperPosition', p);

                save_figure(sprintf('data_subsets_compare_%s_ind_features_%s', this_q_filename_suffix, params.test_description_filename{t}), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);
            end
            
            if show_weeks
                for i = 1:3
                    figure

                    str_suffix = '';
                    if i == 2
                        str_suffix = '_pct_max';
                    elseif i == 3
                        str_suffix = '_pct';
                    end
                    
                    for c = 1:2
                        this_test_participants_q = test_participants_q{t, q}(:, c);
                        if i == 2
                            this_test_participants_q = this_test_participants_q ./ max(nansum(test_participants_q{t, q}, 2));
                        elseif i == 3
                            this_test_participants_q = this_test_participants_q ./ nansum(test_participants_q{t, q}, 2);
                        end
                        y = reshape([this_test_participants_q this_test_participants_q]', variants * 2, 1);
                        stairs(v_steps, y, 'Color', params.clr_qids(c, :), 'LineWidth', 2);
                        hold on;
                    end
                    
                    this_y_lim = [0 1.25];
                    
                    if i == 1
                        this_y_lim = this_y_lim * max(nansum(test_participants_q{t, q}, 2));
                        ylabel('Weeks', 'FontSize', 20, label_opts{:});
                    elseif i == 2 || i == 3
                        if latex_labels
                            ylabel('Weeks (\%)', 'FontSize', 20, label_opts{:});
                        else
                            ylabel('Weeks (%)', 'FontSize', 20, label_opts{:});
                        end
                    end
                    
                    for f = 1:(variants - 1)
                        plot([8.5 8.5] + (f - 1) * 7, this_y_lim, 'Color', [0.5 0.5 0.5])
                    end
                    hold off;
                    
                    set(gca, 'ylim', this_y_lim);
                    set(gca, 'xlim', [1.5, (variants * 7) + 1.5]);
                    set(gca, 'xtick', 5:7:(variants * 7));
                    set(gca, 'xticklabel', variant_desc, 'FontSize', 18);

                    if latex_labels
                        legend_str = {['QIDS $<$ ' num2str(params.qids_threshold)], ['QIDS $\geq$ ' num2str(params.qids_threshold)]};
                    else
                        legend_str = {['QIDS < ' num2str(params.qids_threshold)], ['QIDS ' char(8805) ' ' num2str(params.qids_threshold)]};
                    end
                    legend(legend_str, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 20, label_opts{:})

                    if latex_ticks
                        set(gca, 'TickLabelInterpreter', 'latex')
                    end

                    p = get(gcf, 'PaperPosition');
                    p(3) = 2 * p(3);
                    p(4) = p(4) / 2;
                    set(gcf, 'PaperPosition', p);

                    save_figure(sprintf('data_subsets_compare_%s_participants_%s%s', this_q_filename_suffix_base, params.test_description_filename{t}, str_suffix), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);
                end
            end
        end
        
        if show_weeks
            for i = 1:3
                figure

                str_suffix = '';
                text_format = '%i';
                if i == 2
                    str_suffix = '_pct_max';
                    text_format = '%.2f';
                elseif i == 3
                    str_suffix = '_pct';
                    text_format = '%.2f';
                end
                
                legend_str = {};

                this_y_lim = [0 1];

                if i == 1
                    this_y_lim = this_y_lim * max(nansum(test_participants_cohort{t}, 2));
                end
                
                for c = find(nansum(test_participants_cohort{t}, 1) > 0)
                    legend_str = [legend_str params.test_description(c + 1)]; %#ok<AGROW>
                    this_test_participants_cohort = test_participants_cohort{t}(:, c);
                    if i == 2
                        this_test_participants_cohort = this_test_participants_cohort ./ max(nansum(test_participants_cohort{t}, 2));
                    elseif i == 3
                        this_test_participants_cohort = this_test_participants_cohort ./ nansum(test_participants_cohort{t}, 2);
                    end
                    y = reshape([this_test_participants_cohort this_test_participants_cohort]', variants * 2, 1);
                    stairs(v_steps, y, 'Color', params.clr_cohort(c + 1, :), 'LineWidth', 2);
                    hold on;
                    
                    for v_i = 1:variants
                        if ~isnan(this_test_participants_cohort(v_i))
                            text(mean(v_steps([((v_i * 2) - 1) (v_i * 2)])), this_test_participants_cohort(v_i) - (this_y_lim(2) * 0.01), sprintf(text_format, this_test_participants_cohort(v_i)), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', 'FontSize', 18, label_opts{:});
                        end
                    end
                end
                
                if i == 1
                    ylabel('Weeks', 'FontSize', 20, label_opts{:});
                elseif i == 2 || i == 3
                    if latex_labels
                        ylabel('Weeks (\%)', 'FontSize', 20, label_opts{:});
                    else
                        ylabel('Weeks (%)', 'FontSize', 20, label_opts{:});
                    end
                end
                
                for f = 1:(variants - 1)
                    plot([8.5 8.5] + (f - 1) * 7, this_y_lim, 'Color', [0.5 0.5 0.5])
                end
                hold off;
                
                set(gca, 'xlim', [1.5, (variants * 7) + 1.5]);
                set(gca, 'ylim', this_y_lim);
                set(gca, 'xtick', 5:7:(variants * 7));
                set(gca, 'xticklabel', variant_desc, 'FontSize', 18);

                legend(legend_str, 'Location', 'South', 'Orientation', 'horizontal', 'FontSize', 20, label_opts{:})

                if latex_ticks
                    set(gca, 'TickLabelInterpreter', 'latex')
                end

                p = get(gcf, 'PaperPosition');
                p(3) = 2 * p(3);
                p(4) = p(4) / 2;
                set(gcf, 'PaperPosition', p);

                save_figure(sprintf('data_subsets_compare_participants_%s%s', params.test_description_filename{t}, str_suffix), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);
            end
        end
    end
end
