function varargout = location_annotator(varargin)
% LOCATION_ANNOTATOR MATLAB code for location_annotator.fig
%      LOCATION_ANNOTATOR, by itself, creates a new LOCATION_ANNOTATOR or raises the existing
%      singleton*.
%
%      H = LOCATION_ANNOTATOR returns the handle to a new LOCATION_ANNOTATOR or the handle to
%      the existing singleton*.
%
%      LOCATION_ANNOTATOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOCATION_ANNOTATOR.M with the given input arguments.
%
%      LOCATION_ANNOTATOR('Property','Value',...) creates a new LOCATION_ANNOTATOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before location_annotator_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to location_annotator_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help location_annotator

% Last Modified by GUIDE v2.5 24-Mar-2016 15:43:45

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @location_annotator_OpeningFcn, ...
                   'gui_OutputFcn',  @location_annotator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before location_annotator is made visible.
function location_annotator_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to location_annotator (see VARARGIN)

% Choose default command line output for location_annotator
handles.output = hObject;

% UIWAIT makes location_annotator wait for user response (see UIRESUME)
% uiwait(handles.main_window);

set(handles.lblDataStandard, 'Value', 0);
set(handles.lblDataGMS, 'Value', 0);
set(handles.lblDataCombined, 'Value', 0);
set(handles.lblDataAll, 'Value', 0);

saved_variant = get_system_setting(mfilename, 'variant');

if isempty(saved_variant)
    set(handles.lblDataStandard, 'Value', 1);
    handles.user_data.variant = '';
else
    if strcmp(saved_variant, 'gms')
        set(handles.lblDataGMS, 'Value', 1);
    elseif strcmp(saved_variant, 'combined_std_gms')
        set(handles.lblDataCombined, 'Value', 1);
    elseif strcmp(saved_variant, 'all')
        set(handles.lblDataAll, 'Value', 1);
    end
    handles.user_data.variant = saved_variant;
end

saved_after_upgrade = get_system_setting(mfilename, 'after_upgrade');

if isempty(saved_after_upgrade) || ~saved_after_upgrade
    set(handles.chkAfterUpgrade, 'Value', 0);
    handles.user_data.after_upgrade = false;
else
    set(handles.chkAfterUpgrade, 'Value', 1);
    handles.user_data.after_upgrade = true;
end

saved_participant_filter = get_system_setting(mfilename, 'participant_filter');

if ~isempty(saved_participant_filter)
    set(handles.txtParticipantFilter, 'String', saved_participant_filter);
end

show_3d = get_system_setting(mfilename, 'show_coordinates_3d');
set(handles.chkShow3D, 'Value', ~isempty(show_3d) && show_3d);

set(handles.lblDisplayStandard, 'Value', 0);
set(handles.lblDisplayOriginal, 'Value', 0);
set(handles.lblDisplayValidated, 'Value', 0);
set(handles.lblDisplayRegularised, 'Value', 0);
set(handles.lblDisplayNormalised, 'Value', 0);
set(handles.lblDisplayNormalisedDifferentiated, 'Value', 0);
set(handles.lblDisplayNormalisedFiltered, 'Value', 0);
set(handles.lblDisplayNormalisedFilteredDifferentiated, 'Value', 0);
set(handles.lblDisplayNormalisedSegmented, 'Value', 0);
set(handles.lblDisplayNormalisedFilteredSegmented, 'Value', 0);

handles.user_data.rounded_accuracy = 0.1; %km;

saved_display_type = get_system_setting(mfilename, 'display_type');

if isempty(saved_display_type) && ~strcmp(saved_display_type, '')
    set(handles.lblDisplayNormalised, 'Value', 1);
    handles.user_data.display_type = 'normalised';
elseif isempty(saved_display_type)
    set(handles.lblDisplayStandard, 'Value', 1);
    handles.user_data.display_type = '';
else
    if strcmp(saved_display_type, 'original')
        set(handles.lblDisplayOriginal, 'Value', 1);
    elseif strcmp(saved_display_type, 'validated')
        set(handles.lblDisplayValidated, 'Value', 1);
    elseif strcmp(saved_display_type, 'regularised')
        set(handles.lblDisplayRegularised, 'Value', 1);
    elseif strcmp(saved_display_type, 'normalised')
        set(handles.lblDisplayNormalised, 'Value', 1);
    elseif strcmp(saved_display_type, 'differentiated') || strcmp(saved_display_type, 'normalised_differentiated')
        set(handles.lblDisplayNormalisedDifferentiated, 'Value', 1);
    elseif strcmp(saved_display_type, 'normalised_filtered')
        set(handles.lblDisplayNormalisedFiltered, 'Value', 1);
    elseif strcmp(saved_display_type, 'normalised_filtered_differentiated')
        set(handles.lblDisplayNormalisedFilteredDifferentiated, 'Value', 1);
    elseif strcmp(saved_display_type, 'normalised_segmented')
        set(handles.lblDisplayNormalisedSegmented, 'Value', 1);
    elseif strcmp(saved_display_type, 'normalised_filtered_segmented')
        set(handles.lblDisplayNormalisedFilteredSegmented, 'Value', 1);
    end
    handles.user_data.display_type = saved_display_type;
end

show_stationary_only = get_system_setting(mfilename, 'show_stationary_only');
set(handles.chkStationaryOnly, 'Value', ~isempty(show_stationary_only) && show_stationary_only);

set(handles.lstParticipants, 'String', '');
set(handles.cboWeek, 'String', ' ');
set(handles.cboWeek, 'Value', 1);
set(handles.chkValidWeek, 'Value', 0);
set(handles.chkDataStandardValid, 'Value', 0);
set(handles.chkDataGMSValid, 'Value', 0);
set(handles.chkDataCombinedValid, 'Value', 0);
set(handles.lstLocations, 'String', '');

set(handles.lstParticipants, 'Enable', 'off');
set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');
set(handles.chkValidWeek, 'Enable', 'off');
set(handles.chkDataStandardValid, 'Enable', 'off');
set(handles.chkDataGMSValid, 'Enable', 'off');
set(handles.chkDataCombinedValid, 'Enable', 'off');
set(handles.lstLocations, 'Enable', 'off');
set(handles.cmdAddLocations, 'Enable', 'off');
set(handles.lblLabelNone, 'Value', 0);
set(handles.lblLabelHome, 'Value', 0);
set(handles.lblLabelWork, 'Value', 0);
set(handles.lblLabelPlay, 'Value', 0);
set(handles.lblLabelNone, 'Enable', 'off');
set(handles.lblLabelHome, 'Enable', 'off');
set(handles.lblLabelWork, 'Enable', 'off');
set(handles.lblLabelPlay, 'Enable', 'off');
set(handles.cboDay, 'String', ' ');
set(handles.cboDay, 'Value', 1);
set(handles.cboDay, 'Enable', 'off');

handles.user_data.participant = '';

save_eps = get_system_setting(mfilename, 'save_eps');
set(handles.chkSaveEPS, 'Value', ~isempty(save_eps) && save_eps);

save_fig = get_system_setting(mfilename, 'save_fig');
set(handles.chkSaveFIG, 'Value', ~isempty(save_fig) && save_fig);

save_png = get_system_setting(mfilename, 'save_png');
set(handles.chkSavePNG, 'Value', ~isempty(save_png) && save_png);

set(handles.optSaveAllParticipants, 'Value', false);
set(handles.optSaveAllWeeks, 'Value', false);
set(handles.optSaveThisWeek, 'Value', true);

saved_filename_suffix = get_system_setting(mfilename, 'filename_suffix');

if ~isempty(saved_filename_suffix)
    set(handles.txtFilenameSuffix, 'String', saved_filename_suffix);
end

guidata(hObject, handles);

function handles_out = load_data(participant, handles)
    paths = get_paths();
    
    output_path_base = [paths.working_path participant '-location'];
    
    output_path = output_path_base;
    
    if ~isempty(handles.user_data.variant)
        output_path = [output_path '_' handles.user_data.variant];
    end
    
    t_min_hard = datenum([2014, 01, 01]);
    t_min = t_min_hard;
    weeks = [];
    
    if handles.user_data.after_upgrade
        reporting = load_reporting_data(participant);

        reporting_versions_idx = strcmp([reporting.setting], 'version');
        reporting_versions = cellfun(@str2num, reporting.value(reporting_versions_idx));
        reporting_versions_upgraded = reporting_versions >= 1031;
        
        if sum(reporting_versions_upgraded) > 0
            upgrade_idx = find(diff([0; reporting_versions_upgraded]), 1, 'last');

            reporting_versions_idx = find(reporting_versions_idx);

            upgrade_time = reporting.time(reporting_versions_idx(upgrade_idx));
            
            t_min = floor(upgrade_time) + 1;
        else
            t_min = inf;
        end
    end
    
    this_data = [];
    
    is_valid = false;
    
    if t_min ~= inf
        if strcmp(handles.user_data.variant, 'all')
            this_data = struct();
            data_variants = struct();
            
            for this_variant_cell = {'', 'gms', 'combined_std_gms'}
                this_variant_struct_name = this_variant_cell{:};
                if isempty(this_variant_struct_name)
                    this_variant_struct_name = 'std';
                end
                
                data = load_location_preprocessed(participant, 'variant', this_variant_cell{:});

                data_valid_normalised = isstruct(data) && isfield(data, 'normalised') && isfield(data.normalised, 'lat') && isfield(data.normalised, 'lon');
                data_valid_normalised = data_valid_normalised && isfield(data.normalised, 'time') && ~isempty(data.normalised.time) && (sum(data.normalised.time >= t_min) > 0);
                data_valid_filtered   = isstruct(data) && isfield(data, 'normalised') && isfield(data.normalised, 'filtered') && isfield(data.normalised.filtered, 'lat') && isfield(data.normalised.filtered, 'lon');
                data_valid_filtered   = data_valid_filtered && isfield(data.normalised.filtered, 'time') && ~isempty(data.normalised.filtered.time) && (sum(data.normalised.filtered.time >= t_min) > 0);
                data_valid_segmented  = isstruct(data) && isfield(data, 'normalised') && isfield(data.normalised, 'filtered') && isfield(data.normalised.filtered, 'segmented') && isfield(data.normalised.filtered.segmented, 'lat') && isfield(data.normalised.filtered.segmented, 'lon');
                data_valid_segmented  = data_valid_segmented && isfield(data.normalised.filtered.segmented, 'time') && ~isempty(data.normalised.filtered.segmented.time) && (sum(data.normalised.filtered.segmented.time >= t_min) > 0);
                
                is_valid = is_valid || data_valid_normalised || data_valid_filtered || data_valid_segmented;
                
                if isstruct(data) && isfield(data, 'normalised')
                    data.normalised.is_valid = data_valid_normalised;
                    if isfield(data.normalised, 'filtered')
                        data.normalised.filtered.is_valid = data_valid_filtered;
                        if isfield(data.normalised.filtered, 'segmented')
                            data.normalised.filtered.segmented.is_valid = data_valid_segmented;
                        end
                    end
                end
                if data_valid_normalised
                    data.normalised.time_days = floor(data.normalised.time);
                end
                if data_valid_filtered
                    data.normalised.filtered.time_days = floor(data.normalised.filtered.time);
                end
                if data_valid_segmented
                    data.normalised.filtered.segmented.time_days = floor(data.normalised.filtered.segmented.time);
                    
                    location_data = [data.normalised.filtered.segmented.lat, data.normalised.filtered.segmented.lon];
                    
                    % round data to accuracy of handles.user_data.rounded_accuracy.
                    location_data_rounded = round(location_data / handles.user_data.rounded_accuracy) * handles.user_data.rounded_accuracy;

                    [locations_raw_unique, ~, locations_raw_unique_idx] = unique(location_data, 'rows');
                    locations_raw_unique_rounded = round(locations_raw_unique / handles.user_data.rounded_accuracy) * handles.user_data.rounded_accuracy;
                    [locations_rounded_unique, ~, locations_rounded_unique_idx] = unique(location_data_rounded, 'rows');

                    [~, locations_raw_unique_rounded_idx] = ismember(locations_raw_unique_rounded, locations_rounded_unique, 'rows');

                    data.normalised.filtered.segmented.location_data = location_data;
                    data.normalised.filtered.segmented.locations_raw = locations_raw_unique;
                    data.normalised.filtered.segmented.locations_raw_idx = locations_raw_unique_idx;
                    data.normalised.filtered.segmented.locations_rounded = locations_rounded_unique;
                    data.normalised.filtered.segmented.locations_rounded_idx = locations_rounded_unique_idx;
                    data.normalised.filtered.segmented.locations_raw_rounded_idx = locations_raw_unique_rounded_idx;
                    
                    if isfield(data.normalised.filtered.segmented, 'clusters')
                        data.normalised.filtered.segmented.cluster_clr = [0 0 0; parula(size(data.normalised.filtered.segmented.clusters, 1))];
                    end
                end
                
                data_variants.(this_variant_struct_name) = data;
                
                if isempty(this_variant_cell{:})
                    data_variants.(this_variant_struct_name).data_path = output_path_base;
                else
                    data_variants.(this_variant_struct_name).data_path = [output_path_base '_' this_variant_cell{:}];
                end
                
                if data_valid_segmented
                    this_data = data.normalised.filtered.segmented;
                end
            end
            this_data.variants = data_variants;
            subtype = '';
        else
            data = load_location_preprocessed(participant, 'variant', handles.user_data.variant);

            subtype = '';

            if isempty(handles.user_data.display_type)
                this_data = data;

                is_valid = (isfield(this_data, 'lat') && isfield(this_data, 'lon'));
            elseif regexp(handles.user_data.display_type, '.*differentiated$')
                parts = regexp(handles.user_data.display_type, '_', 'split');

                if numel(parts) == 1
                    parts{1} = 'normalised';
                else
                    parts = parts(1:end-1);
                end

                if numel(parts) == 1 && isfield(data, parts{1})
                    this_data = data.(parts{1});
                elseif numel(parts) == 2 && isfield(data, parts{1}) && isfield(data.(parts{1}), parts{2})
                    this_data = data.(parts{1}).(parts{2});
                end

                is_valid = isstruct(this_data) && (isfield(this_data, 'lat') && isfield(this_data, 'lon') && isfield(this_data, 'differentiated'));

                subtype = 'differentiated';
            elseif sum(handles.user_data.display_type == '_') > 0
                parts = regexp(handles.user_data.display_type, '_', 'split');

                if numel(parts) == 2 && isfield(data, parts{1}) && isfield(data.(parts{1}), parts{2})
                    this_data = data.(parts{1}).(parts{2});
                    subtype = parts{2};
                elseif numel(parts) == 3 && isfield(data, parts{1}) && isfield(data.(parts{1}), parts{2})  && isfield(data.(parts{1}).(parts{2}), parts{3})
                    this_data = data.(parts{1}).(parts{2}).(parts{3});
                    subtype = parts{3};
                end

                is_valid = isstruct(this_data) && (isfield(this_data, 'lat') && isfield(this_data, 'lon'));
            elseif isfield(data, handles.user_data.display_type)
                this_data = data.(handles.user_data.display_type);
                if strcmp(handles.user_data.display_type, 'original')
                    this_data.time = data.time;
                end

                is_valid = isstruct(this_data) && (isfield(this_data, 'lat') && isfield(this_data, 'lon'));
            end
            
            is_valid = is_valid && ~isempty(this_data) && isfield(this_data, 'time') && ~isempty(this_data.time) && (sum(this_data.time >= t_min) > 0);
        end
    end
    
    if (t_min ~= inf) && is_valid
        time_min_data = floor(min(data.time(data.time >= t_min_hard)));
        
        location_data = [this_data.lat, this_data.lon];

        if strcmp(subtype, 'differentiated')
            this_data.data = this_data.differentiated.data;
        else
            % data is the distance from the mode.
            this_data.data = sqrt(sum(location_data.^2,2));
        end

        % Calculate hours spent in each location, limiting to 15 mins max per
        % sample.
        location_time = [diff(this_data.time); 0];
        location_time = min(location_time, 1/(24 * 4));

        time_days = floor(this_data.time);
        time_days_valid = time_days(time_days >= t_min);
        time_days_unique = time_days_valid(diff([time_days_valid; time_days_valid(end) + 1]) ~= 0);

        weeks_start = time_days_unique(1);
        weeks_start = (weeks_start - 1) - weekday(weeks_start - 1) + 2; % Start on the previous Monday
        weeks_end = time_days_unique(end);
        weeks = ((0:floor((weeks_end - weeks_start) / 7)) * 7) + weeks_start;

        % round data to accuracy of handles.user_data.rounded_accuracy.
        location_data_rounded = round(location_data / handles.user_data.rounded_accuracy) * handles.user_data.rounded_accuracy;

        [locations_raw_unique, ~, locations_raw_unique_idx] = unique(location_data, 'rows');
        locations_raw_unique_rounded = round(locations_raw_unique / handles.user_data.rounded_accuracy) * handles.user_data.rounded_accuracy;
        [locations_rounded_unique, ~, locations_rounded_unique_idx] = unique(location_data_rounded, 'rows');
        
        [~, locations_raw_unique_rounded_idx] = ismember(locations_raw_unique_rounded, locations_rounded_unique, 'rows');
        
        location_annotations_new = zeros(size(locations_rounded_unique, 1), 1);
        
        variant_annotations = struct();
        
        for this_variant_cell = {'', 'gms', 'combined_std_gms'}
            this_variant_struct_name = this_variant_cell{:};
            if isempty(this_variant_struct_name)
                this_variant_struct_name = 'std';
            end

            this_variant_annotations = struct();
            this_variant_week_annotations = struct();

            this_variant_week_annotations.start = weeks;
            this_variant_week_annotations.valid = false(size(weeks));

            loaded_annotations = load_location_annotations(participant, 'variant', this_variant_cell{:});

            if numel(fieldnames(loaded_annotations)) > 0
                if strcmp(this_variant_cell{:}, handles.user_data.variant)
                    if isfield(loaded_annotations, 'locations') && ~isempty(loaded_annotations.locations)
                        [~, dup_location_index] = ismember(loaded_annotations.locations, locations_rounded_unique, 'rows');
                    elseif isfield(loaded_annotations, 'locations_rounded') && ~isempty(loaded_annotations.locations_rounded)
                        [~, dup_location_index] = ismember(loaded_annotations.locations_rounded, locations_rounded_unique, 'rows');
                    else
                        dup_location_index = [];
                    end
                    for i = 1:length(dup_location_index)
                        if dup_location_index(i) > 0
                            location_annotations_new(dup_location_index(i)) = loaded_annotations.location_annotations(i);
                        end
                    end
                elseif strcmp(handles.user_data.variant, 'all')
                    for fieldname_cell = fieldnames(loaded_annotations)'
                        if ~strcmp(fieldname_cell{:}, 'week_annotations')
                            this_variant_annotations.(fieldname_cell{:}) = loaded_annotations.(fieldname_cell{:});
                        end
                    end
                end
                if isfield(loaded_annotations, 'week_annotations')
                    for w = find(ismember(loaded_annotations.week_annotations.start, this_variant_week_annotations.start))
                        this_week = loaded_annotations.week_annotations.start(w);
                        this_variant_week_annotations.valid(this_variant_week_annotations.start == this_week) = loaded_annotations.week_annotations.valid(w);
                    end

                    missing_weeks = ~ismember(loaded_annotations.week_annotations.start, this_variant_week_annotations.start);

                    if sum(missing_weeks) > 0
                        this_variant_week_annotations.start = [this_variant_week_annotations.start loaded_annotations.week_annotations.start(missing_weeks)];
                        this_variant_week_annotations.valid = [this_variant_week_annotations.valid loaded_annotations.week_annotations.valid(missing_weeks)];

                        [this_variant_week_annotations.start, weeks_sort_order] = sort(this_variant_week_annotations.start);
                        this_variant_week_annotations.valid = this_variant_week_annotations.valid(weeks_sort_order);
                    end
                end
            end
            
            this_variant_annotations.week_annotations = this_variant_week_annotations;
            if strcmp(handles.user_data.variant, 'all')
                this_variant_annotations.data_path = data_variants.(this_variant_struct_name).data_path;
            end

            for required_field_cell = {'locations_rounded', 'location_annotations'}
                if ~isfield(this_variant_annotations, required_field_cell{:})
                    this_variant_annotations.(required_field_cell{:}) = [];
                end
            end
            
            variant_annotations.(this_variant_struct_name) = this_variant_annotations;

            if strcmp(this_variant_cell{:}, handles.user_data.variant)
                week_annotations = this_variant_week_annotations;
            elseif strcmp(handles.user_data.variant, 'all')
                week_annotations = struct();
            end
        end

        annotation_colors = lines(4);
        annotation_colors(1, :) = [0 0 0];

        weeks_string_base = cell(1, length(weeks));
        weeks_string = cell(1, length(weeks));
        weeks_valid_data = false(1, length(weeks));

        for i = 1:length(weeks)
            weeks_string_base{i} = ['Week ' num2str(i) ' (Week Beginning ' datestr(weeks(i), 'dd-mmm-yyyy') ')'];
            if sum(ismember(time_days_valid, (0:6) + weeks(i))) > 0
                weeks_valid_data(i) = true;
                weeks_string_base{i} = [weeks_string_base{i} ' *'];
            end
            weeks_string{i} = weeks_string_base{i};
            if ~strcmp(handles.user_data.variant, 'all') && ...
                    (sum(week_annotations.start == weeks(i)) > 0) && ...
                    (week_annotations.valid(week_annotations.start == weeks(i)))
                weeks_string{i} = [weeks_string{i} ' ' char(10003)];
            else
                for this_variant_cell = {'std', 'gms', 'combined_std_gms'}
                    if (sum(variant_annotations.(this_variant_cell{:}).week_annotations.start == weeks(i)) > 0) && ...
                            (variant_annotations.(this_variant_cell{:}).week_annotations.valid(variant_annotations.(this_variant_cell{:}).week_annotations.start == weeks(i)))
                        weeks_string{i} = [weeks_string{i} ' ' char(9702)];
                        break;
                    end
                end
            end
        end

        set(handles.cboWeek, 'String', weeks_string);

        handles.user_data.subtype = subtype;
        handles.user_data.data_path = output_path;
        handles.user_data.location_data = location_data;
        handles.user_data.location_time = location_time;
        handles.user_data.locations_raw = locations_raw_unique;
        handles.user_data.locations_raw_idx = locations_raw_unique_idx;
        handles.user_data.locations_rounded = locations_rounded_unique;
        handles.user_data.locations_rounded_idx = locations_rounded_unique_idx;
        handles.user_data.locations_raw_rounded_idx = locations_raw_unique_rounded_idx;
        handles.user_data.location_annotations = location_annotations_new;
        handles.user_data.annotation_colors = annotation_colors;
        if isfield(handles.user_data, 'clusters') && ~isfield(this_data, 'clusters')
            handles.user_data = rmfield(handles.user_data, 'clusters');
        end
        if isfield(handles.user_data, 'cluster_idx') && ~isfield(this_data, 'cluster_idx')
            handles.user_data = rmfield(handles.user_data, 'cluster_idx');
        end
        if isfield(handles.user_data, 'cluster_clr')
            handles.user_data = rmfield(handles.user_data, 'cluster_clr');
        end
        for fieldname_cell = fieldnames(this_data)'
            handles.user_data.(fieldname_cell{:}) = this_data.(fieldname_cell{:});
        end
        if isfield(handles.user_data, 'clusters')
            handles.user_data.cluster_clr = [0 0 0; parula(size(handles.user_data.clusters, 1))];
        end
        handles.user_data.time_days = time_days;
        handles.user_data.time_min = t_min;
        handles.user_data.time_min_data = time_min_data;
        handles.user_data.weeks = weeks;
        handles.user_data.weeks_string_base = weeks_string_base;
        handles.user_data.weeks_string = weeks_string;
        handles.user_data.weeks_valid_data = weeks_valid_data;
        handles.user_data.week_annotations = week_annotations;
        handles.user_data.variant_annotations = variant_annotations;

        scatter_colormap = hot(1100);
        scatter_colormap = flipud(scatter_colormap);
        scatter_colormap = scatter_colormap(100:end, :);
        handles.user_data.scatter_colormap = scatter_colormap;

        ud = handles.user_data; %#ok<NASGU>

        save([output_path '-annotation-data.mat'], ...
            '-struct', 'ud', 'data', 'time', 'location_data', ...
            'location_time', 'locations_rounded', 'locations_rounded_idx', ...
            '-v7.3')
    elseif t_min == inf
        set(handles.cboWeek, 'String', 'Not upgraded');
    else
        set(handles.cboWeek, 'String', 'No data');
    end
    
    handles.user_data.weeks = weeks;
    
    handles_out = handles;


function handles_out = update_valid_checkboxes(handles)
    firstday = handles.user_data.weeks(handles.user_data.current_week);
    
    if ~strcmp(handles.user_data.variant, 'all')
        this_week_annotation = (handles.user_data.week_annotations.start == firstday);
        if handles.user_data.week_annotations.valid(this_week_annotation)
            set(handles.chkValidWeek, 'Value', 1);
        else
            set(handles.chkValidWeek, 'Value', 0);
        end
    end
    
    set(handles.chkDataStandardValid, 'Value', 0);
    set(handles.chkDataGMSValid, 'Value', 0);
    set(handles.chkDataCombinedValid, 'Value', 0);
    
    this_week_annotation = (handles.user_data.variant_annotations.std.week_annotations.start == firstday);
    if handles.user_data.variant_annotations.std.week_annotations.valid(this_week_annotation)
        set(handles.chkDataStandardValid, 'Value', 1);
    end
    
    this_week_annotation = (handles.user_data.variant_annotations.gms.week_annotations.start == firstday);
    if handles.user_data.variant_annotations.gms.week_annotations.valid(this_week_annotation)
        set(handles.chkDataGMSValid, 'Value', 1);
    end
    
    this_week_annotation = (handles.user_data.variant_annotations.combined_std_gms.week_annotations.start == firstday);
    if handles.user_data.variant_annotations.combined_std_gms.week_annotations.valid(this_week_annotation)
        set(handles.chkDataCombinedValid, 'Value', 1);
    end
    
    handles_out = handles;


function handles_out = plot_week(week_no, handles)
    
    handles.user_data.current_week = week_no;
    
    handles = update_valid_checkboxes(handles);
    
    firstday = handles.user_data.weeks(week_no);
    
    day_string = cell(8, 1);
    day_string{1} = 'Show all days';
    for j = 0:6
        day_string{j + 2} = sprintf('Show only %s', datestr(firstday + j, 'dddd (dd-mmm-yyyy)'));
    end
    
    set(handles.cboDay, 'String', day_string);
    set(handles.cboDay, 'Value', 1);
    
    handles_out = draw_data(handles, 0:6);


function handles_out = draw_data(handles, display_days)

    ud = handles.user_data;
    
    week_no = ud.current_week;
    
    if isfield(handles.user_data, 'figure_handle') && ...
            ishandle(handles.user_data.figure_handle)
        figure(handles.user_data.figure_handle);
    else
        handles.user_data.figure_handle = figure;
        set(handles.user_data.figure_handle, 'Name', 'Distance')
    end
    
    if week_no == 1
        set(handles.cmdPrevWeek, 'Enable', 'off')
    else
        set(handles.cmdPrevWeek, 'Enable', 'on')
    end
    
    if week_no == length(ud.weeks)
        set(handles.cmdNextWeek, 'Enable', 'off')
    else
        set(handles.cmdNextWeek, 'Enable', 'on')
    end
    
    firstday = ud.weeks(week_no);
    
    this_day = firstday - ud.time_min_data + 1;
    
    if numel(display_days) > 1
        title_str = ['Week Beginning ' datestr(firstday, 'dd-mmm-yyyy')];
    else
        title_str = datestr(firstday + display_days, 'dddd dd-mmm-yyyy');
        this_day = this_day + display_days;
    end

    if this_day > 0
        % valid_days = ud.time_days(ud.time_days >= ud.time_min);
        % this_day_in_selection = num2str(firstday - valid_days(1) + 1);
        title_str = [title_str ' (day ' num2str(this_day) ')'];
    end
    
    set(handles.cboWeek, 'Value', week_no);

    if isfield(ud, 'cluster_clr')
        clr = ud.cluster_clr;
    else
        clr = ud.annotation_colors;
    end
    
    alldays = firstday + display_days;

    state_clr = [ 0.7  1    0.7 ;
                  0.7  0.7  1   ;
                  1    0.7  0.7 ];
    
    if ~strcmp(ud.variant, 'all')
        for j = display_days
            thisday = firstday + j;

            day_data_idx = (ud.time_days == thisday) & (thisday >= ud.time_min);
            day_data_time_rel = ud.time(day_data_idx) - thisday;
            day_locations_idx = ud.locations_rounded_idx(day_data_idx, :);
            if isfield(ud, 'cluster_idx')
                day_locations_annotation = ud.cluster_idx(day_data_idx);
            else
                day_locations_annotation = ud.location_annotations(day_locations_idx);
            end

            if numel(display_days) > 1
                subplot(numel(display_days), 7, ((j - min(display_days)) * numel(display_days)) + (1:6));
            else
                subplot(1, 1, 1);
            end
            cla;

            stairs(day_data_time_rel, ud.data(day_data_idx), 'LineWidth', 2);
            hold on;

            scatter(day_data_time_rel, ud.data(day_data_idx), 200, clr(day_locations_annotation + 1, :), 'LineWidth', 2, 'Marker', 'x');
            dist_max = max(ud.data(day_data_idx));
            dist_min = min(ud.data(day_data_idx));
            if isempty(dist_max)
                dist_max = 0;
                dist_min = 0;
            end
            a = [0 1 max(dist_min - 0.5, 0) max(dist_max + 0.5, 1)];
            axis(a);
            axis manual;
            if j == 0
                title(title_str, 'FontSize', 30);
            end
            ylabel(datestr(thisday, 'ddd'), 'FontSize', 30);
            set(gca,'XTick', (0:3:24)/24)
            if j == 6
                set(gca,'XTickLabel', datestr((0:3:24)/24, 'hh:00'))
            else
                set(gca,'XTickLabel', [])
            end
            set(gca,'FontSize', 30)

            if strcmp(ud.subtype, 'segmented') && sum(day_data_idx) > 0
                y_lim = get(gca, 'ylim');

                location_time = min([1/(24 * 60); diff(ud.time(day_data_idx))], 1/(24 * 12));

                h = zeros(1, 3);

                time_points = [(day_data_time_rel - location_time) day_data_time_rel day_data_time_rel];
                time_points_lin = reshape(time_points', numel(time_points), 1);
                time_points_cont = abs(time_points(2:end, 1) - time_points(1:end-1, 3)) < (1/(24 * 60 * 6));

                for this_state = 1:3
                    this_state_idx = ud.state(day_data_idx) == this_state;

                    state_points = zeros(size(time_points));

                    state_points(:, 1) = this_state_idx;
                    state_points(:, 2) = this_state_idx;

                    state_points(time_points_cont, 3) = NaN;

                    state_points_lin = reshape(state_points', numel(state_points), 1);

                    [a, b] = stairs(time_points_lin(~isnan(state_points_lin)), state_points_lin(~isnan(state_points_lin)));

                    h(this_state) = patch([0; a(1); a; 1], double([0; 0; b; 0]) * y_lim(2), state_clr(this_state, :));
                end

                uistack(h, 'bottom')
            end

            hold off;

            if numel(display_days) > 1
                subplot(numel(display_days), 7, ((j - min(display_days)) * numel(display_days)) + 7);
                cla;

                day_location_time = ud.location_time(day_data_idx);
                [day_locations_unique] = unique(day_locations_idx);
                day_locations_duration = zeros(size(day_locations_unique, 1), 1);

                for location_idx = 1:length(day_locations_unique)
                    day_locations_duration(location_idx) = sum(day_location_time(day_locations_idx == day_locations_unique(location_idx)));
                end

                colormap(ud.scatter_colormap);
                scatter(ud.locations_rounded(day_locations_unique, 1), ud.locations_rounded(day_locations_unique, 2), [], day_locations_duration, 'MarkerFaceColor', 'flat');
                axis manual;
                colorbar;
                caxis([0 1]);
            end
        end
    else
        variants = {'std', 'gms', 'combined_std_gms'};
        for j = 1:9
            subplot(9, 1, j);
            cla;
            
            this_variant = variants{floor((j - 1) / 3) + 1};
            
            this_variant_data = ud.variants.(this_variant);
            
            if rem((j - 1), 3) == 0
                if isfield(this_variant_data, 'normalised')
                    this_data = this_variant_data.normalised;
                else
                    this_data = struct('is_valid', false);
                end
            elseif rem((j - 1), 3) == 1
                if isfield(this_variant_data, 'normalised') && isfield(this_variant_data.normalised, 'filtered')
                    this_data = this_variant_data.normalised.filtered;
                else
                    this_data = struct('is_valid', false);
                end
            elseif rem((j - 1), 3) == 2
                if isfield(this_variant_data, 'normalised') && isfield(this_variant_data.normalised, 'filtered') && isfield(this_variant_data.normalised.filtered, 'segmented')
                    this_data = this_variant_data.normalised.filtered.segmented;
                else
                    this_data = struct('is_valid', false);
                end
            end
            
            if this_data.is_valid
                day_data_idx = ismember(this_data.time_days, alldays) & (this_data.time_days >= ud.time_min);
                day_data_time_rel = this_data.time(day_data_idx) - alldays(1);

                if isfield(this_data, 'cluster_idx')
                    day_locations_annotation = this_data.cluster_idx(day_data_idx);
                    clr = this_data.cluster_clr;
                else
                    day_locations_annotation = 0;
                    clr = ud.annotation_colors;
                end

                stairs(day_data_time_rel, this_data.data(day_data_idx), 'LineWidth', 2);
                hold on;

                scatter(day_data_time_rel, this_data.data(day_data_idx), 50, clr(day_locations_annotation + 1, :), 'LineWidth', 2, 'Marker', 'x');

                dist_max = max(this_data.data(day_data_idx));
                dist_min = min(this_data.data(day_data_idx));

                if (rem((j - 1), 3) == 2) && (sum(day_data_idx) > 0)
                    y_lim = a(3:4);

                    location_time = min([1/(24 * 60); diff(this_data.time(day_data_idx))], 1/(24 * 12));

                    h = zeros(1, 3);

                    time_points = [(day_data_time_rel - location_time) day_data_time_rel day_data_time_rel];
                    time_points_lin = reshape(time_points', numel(time_points), 1);
                    time_points_cont = abs(time_points(2:end, 1) - time_points(1:end-1, 3)) < (1/(24 * 60 * 6));

                    for this_state = 1:3
                        this_state_idx = this_data.state(day_data_idx) == this_state;

                        state_points = zeros(size(time_points));

                        state_points(:, 1) = this_state_idx;
                        state_points(:, 2) = this_state_idx;

                        state_points(time_points_cont, 3) = NaN;

                        state_points_lin = reshape(state_points', numel(state_points), 1);

                        [a, b] = stairs(time_points_lin(~isnan(state_points_lin)), state_points_lin(~isnan(state_points_lin)));

                        h(this_state) = patch([0; a(1); a; 1], double([0; 0; b; 0]) * y_lim(2), state_clr(this_state, :));
                    end

                    uistack(h, 'bottom')
                end
            else
                dist_min = [];
                dist_max = [];
            end
            
            if isempty(dist_max)
                dist_max = 0;
                dist_min = 0;
            end
            a = [0 numel(alldays) max(dist_min - 0.5, 0) max(dist_max + 0.5, 1)];
            axis(a);
            axis manual;
            
            if j == 1
                title(title_str, 'FontSize', 30);
            end
            
            if numel(alldays) > 1
                day_sep = numel(alldays) - 1;
                h_days = stairs(reshape([(1:day_sep)' (1:day_sep)' NaN(day_sep, 1)]', day_sep * 3, 1), reshape([repmat(max(dist_min - 0.5, 0), day_sep, 1) repmat(max(dist_max + 0.5, 1), day_sep, 1) NaN(day_sep, 1)]', day_sep * 3, 1), 'color', [0 0 0]);
                uistack(h_days, 'bottom')
                
                set(gca,'XTick', 0:0.5:numel(alldays))
                if j == 9
                    str = cellstr(datestr([alldays(1), reshape([alldays; alldays], 1, numel(alldays) * 2)], 'ddd'));
                    str(1:2:((numel(alldays) * 2) + 1)) = repmat({''}, 1, numel(alldays) + 1)';
                    set(gca,'XTickLabel', str)
                    xlabel('');
                else
                    set(gca,'XTickLabel', [])
                end
            else
                set(gca,'XTick', (0:3:24)/24)
                if j == 9
                    set(gca,'XTickLabel', datestr((0:3:24)/24, 'hh:00'))
                else
                    set(gca,'XTickLabel', [])
                end
            end
            if j == 2
                ylabel(['Standard' char(10) 'filt']);
            elseif j == 5
                ylabel(['GMS' char(10) 'filt']);
            elseif j == 8
                ylabel(['Combined' char(10) 'filt']);
            elseif rem((j - 1), 3) == 0
                ylabel('norm');
            elseif rem((j - 1), 3) == 2
                ylabel('seg');
            end
            set(gca,'FontSize', 20)
        end
    end

    figure(handles.main_window);
    cla;
    
    week_data_idx = ismember(ud.time_days, alldays) & (ud.time_days >= ud.time_min);
    
    week_location_time = ud.location_time(week_data_idx);
    
    week_locations_idx = ud.locations_raw_idx(week_data_idx, :);
    week_locations_unique = unique(week_locations_idx);
    week_locations_duration = zeros(size(week_locations_unique, 1), 1);
    week_locations_raw_rounded_ids = ud.locations_raw_rounded_idx(week_locations_unique);
    if strcmp(ud.subtype, 'segmented')
        week_locations_state = ud.state(week_data_idx);
    end
    if isfield(ud, 'cluster_idx')
        week_locations_annotation = ud.cluster_idx(week_data_idx);
    else
        week_locations_annotation = ud.location_annotations(ud.locations_rounded_idx(week_data_idx, :));
    end
    
    week_locations_rounded_idx = ud.locations_rounded_idx(week_data_idx, :);
    week_locations_rounded_unique = unique(week_locations_rounded_idx);
    week_locations_rounded_duration = zeros(size(week_locations_rounded_unique, 1), 1);
    
    for location_idx = 1:length(week_locations_unique)
        location_rounded_idx = ud.locations_raw_rounded_idx(week_locations_unique(location_idx));
        week_locations_duration(location_idx) = sum(week_location_time(week_locations_rounded_idx == location_rounded_idx));
    end
    
    for location_idx = 1:length(week_locations_rounded_unique)
        week_locations_rounded_duration(location_idx) = sum(week_location_time(week_locations_rounded_idx == week_locations_rounded_unique(location_idx)));
    end

    if get(handles.chkShow3D, 'Value')
        if numel(week_locations_unique) > 0
            grid_x = linspace(floor(min(ud.locations_raw(week_locations_unique, 1))), ceil(max(ud.locations_raw(week_locations_unique, 1))), 50);
            grid_y = linspace(floor(min(ud.locations_raw(week_locations_unique, 2))), ceil(max(ud.locations_raw(week_locations_unique, 2))), 50);
        else
            grid_x = 0:1;
            grid_y = 0:1;
        end
        grid_z = NaN(numel(grid_x), numel(grid_y));
        grid_c = NaN(numel(grid_x), numel(grid_y));
        
        for location_idx = 1:length(week_locations_unique)
            location = ud.locations_raw(week_locations_unique(location_idx), :);
            [~, x_i] = min(abs(grid_x - location(1)));
            [~, y_i] = min(abs(grid_y - location(2)));
            
            valid_idx = true(size(week_location_time));
            
            if strcmp(ud.subtype, 'segmented') && get(handles.chkStationaryOnly, 'Value')
                valid_idx = (week_locations_state == 1);
            end
            
            this_location_time = sum(week_location_time(valid_idx & (week_locations_idx == week_locations_unique(location_idx))));
            this_location_annotations = week_locations_annotation(valid_idx & (week_locations_idx == week_locations_unique(location_idx)));
            
            if ~isempty(this_location_time) && (this_location_time > 0)
                if isnan(grid_z(x_i, y_i))
                    grid_z(x_i, y_i) = 0;
                end
                grid_z(x_i, y_i) = grid_z(x_i, y_i) + this_location_time;
                if strcmp(ud.subtype, 'segmented') && get(handles.chkStationaryOnly, 'Value')
                    grid_c(x_i, y_i) = mode(this_location_annotations);
                end
            end
        end
        if strcmp(ud.subtype, 'segmented') && get(handles.chkStationaryOnly, 'Value')
            colormap(clr);
            plot_histogram_3d(grid_x, grid_y, grid_z', grid_c');
            set(gca, 'clim', [0 size(clr, 1)]);
        else
            colormap(ud.scatter_colormap);
            plot_histogram_3d(grid_x, grid_y, grid_z');
        end
        set(gca, 'YDir', 'normal');
        axis auto;
        xlabel({'Distance from' 'home east (km)'}, 'HorizontalAlignment', 'center')
        ylabel({'Distance from' 'home north (km)'}, 'HorizontalAlignment', 'center')
    else
        colormap(ud.scatter_colormap);
        if strcmp(ud.subtype, 'segmented') && get(handles.chkStationaryOnly, 'Value')
            valid_week_locations_unique = unique(week_locations_idx(week_locations_state == 1));
        else
            valid_week_locations_unique = week_locations_unique;
        end
        scatter(ud.locations_raw(valid_week_locations_unique, 1), ud.locations_raw(valid_week_locations_unique, 2), [], week_locations_duration(ismember(valid_week_locations_unique, week_locations_unique)), 'MarkerFaceColor', 'flat');
        if isfield(ud, 'cluster_idx') && isfield(ud, 'clusters')
            hold on;
            week_cluster_idx = ud.cluster_idx(week_data_idx);
            week_cluster_idx = unique(week_cluster_idx(week_cluster_idx > 0));
            week_clusters = ud.clusters(week_cluster_idx, :);
            scatter(week_clusters(:, 1), week_clusters(:, 2), 200, clr(week_cluster_idx + 1, :), 'LineWidth', 2, 'Marker', '+');
            hold off;
        end
        xlabel('Distance from home east (km)')
        ylabel('Distance from home north (km)')
        if ~isempty(week_locations_rounded_unique)
            a = axis;
            a(1) = min(a(1), min(ud.locations_rounded(week_locations_rounded_unique, 1)) - (handles.user_data.rounded_accuracy / 2));
            a(2) = max(a(2), max(ud.locations_rounded(week_locations_rounded_unique, 1)) + (handles.user_data.rounded_accuracy / 2));
            a(3) = min(a(3), min(ud.locations_rounded(week_locations_rounded_unique, 2)) - (handles.user_data.rounded_accuracy / 2));
            a(4) = max(a(4), max(ud.locations_rounded(week_locations_rounded_unique, 2)) + (handles.user_data.rounded_accuracy / 2));
            axis(a);
        end
        axis manual;
        colorbar;
    end
    
    [~, week_locations_rounded_sorted_idx] = sort(week_locations_rounded_duration, 1, 'descend');
    
    set(handles.lstLocations, 'String', '');
    string_locations_base = cell(1, length(week_locations_rounded_duration));
    string_locations = cell(1, length(week_locations_rounded_duration));
    list_location_index = zeros(1, length(week_locations_rounded_duration));
    
    i_max = length(week_locations_rounded_duration);
    
    for i = 1:size(week_locations_rounded_unique, 1)
        string_locations_base{i} = ['Location ' num2str(i)];
        if (week_locations_rounded_duration(week_locations_rounded_sorted_idx(i)) < 1/24)
            if i_max > i
                i_max = i - 1;
            end
        else
            switch ud.location_annotations(week_locations_rounded_unique(week_locations_rounded_sorted_idx(i)))
                case 1
                    string_locations{i} = [string_locations_base{i} ' (Home)'];
                case 2
                    string_locations{i} = [string_locations_base{i} ' (Work)'];
                case 3
                    string_locations{i} = [string_locations_base{i} ' (Play)'];
                otherwise
                    string_locations{i} = string_locations_base{i};
            end
            list_location_index(i) = week_locations_rounded_unique(week_locations_rounded_sorted_idx(i));
        end
    end
    
    handles.user_data.week_location_strings_base = string_locations_base;
    
    set(handles.lstLocations, 'String', string_locations(1:i_max));
    
    handles.user_data.week_locations_list_index = list_location_index;
    handles.user_data.week_locations_index = week_locations_rounded_sorted_idx;
    handles.user_data.week_locations = week_locations_rounded_unique;
    handles.user_data.week_locations_raw = week_locations_unique;
    handles.user_data.week_locations_raw_rounded_idx = week_locations_raw_rounded_ids;
    
    handles.user_data.display_days = display_days;
    
    set(handles.lstLocations, 'Value', []);
    handles = select_location(handles);
    
    set(handles.cboWeek, 'Enable', 'on');
    
    if ~strcmp(ud.variant, 'all')
        if i_max > 0
            set(handles.lstLocations, 'Enable', 'on');
        end
        
        set(handles.chkValidWeek, 'Enable', 'on');
        set(handles.cmdAddLocations, 'Enable', 'on');
    else
        set(handles.chkDataStandardValid, 'Enable', 'on');
        set(handles.chkDataGMSValid, 'Enable', 'on');
        set(handles.chkDataCombinedValid, 'Enable', 'on');
    end
    set(handles.cboDay, 'Enable', 'on');

    handles_out = handles;


% --- Outputs from this function are returned to the command line.
function varargout = location_annotator_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in cmdNextWeek.
function cmdNextWeek_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cmdNextWeek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');
set(handles.chkValidWeek, 'Value', 0);
set(handles.chkValidWeek, 'Enable', 'off');
set(handles.chkDataStandardValid, 'Value', 0);
set(handles.chkDataGMSValid, 'Value', 0);
set(handles.chkDataCombinedValid, 'Value', 0);
set(handles.chkDataStandardValid, 'Enable', 'off');
set(handles.chkDataGMSValid, 'Enable', 'off');
set(handles.chkDataCombinedValid, 'Enable', 'off');
set(handles.lstLocations, 'Enable', 'off');
set(handles.lblLabelNone, 'Enable', 'off');
set(handles.lblLabelHome, 'Enable', 'off');
set(handles.lblLabelWork, 'Enable', 'off');
set(handles.lblLabelPlay, 'Enable', 'off');
set(handles.cboDay, 'Enable', 'off');
drawnow;

next_valid_week_idx = find(handles.user_data.weeks_valid_data & (1:numel(handles.user_data.weeks_valid_data)) > handles.user_data.current_week, 1, 'first');
if isempty(next_valid_week_idx)
    next_valid_week_idx = handles.user_data.current_week + 1;
end
handles = plot_week(next_valid_week_idx, handles);
guidata(hObject, handles);

% --- Executes on button press in cmdPrevWeek.
function cmdPrevWeek_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cmdPrevWeek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');
set(handles.chkValidWeek, 'Value', 0);
set(handles.chkValidWeek, 'Enable', 'off');
set(handles.chkDataStandardValid, 'Value', 0);
set(handles.chkDataGMSValid, 'Value', 0);
set(handles.chkDataCombinedValid, 'Value', 0);
set(handles.chkDataStandardValid, 'Enable', 'off');
set(handles.chkDataGMSValid, 'Enable', 'off');
set(handles.chkDataCombinedValid, 'Enable', 'off');
set(handles.lstLocations, 'Enable', 'off');
set(handles.lblLabelNone, 'Enable', 'off');
set(handles.lblLabelHome, 'Enable', 'off');
set(handles.lblLabelWork, 'Enable', 'off');
set(handles.lblLabelPlay, 'Enable', 'off');
set(handles.cboDay, 'Enable', 'off');
drawnow;
prev_valid_week_idx = find(handles.user_data.weeks_valid_data & (1:numel(handles.user_data.weeks_valid_data)) < handles.user_data.current_week, 1, 'last');
if isempty(prev_valid_week_idx)
    prev_valid_week_idx = handles.user_data.current_week - 1;
end
handles = plot_week(prev_valid_week_idx, handles);
guidata(hObject, handles);


function handles_out = select_location(handles)
    location_value = get(handles.lstLocations, 'Value');

    if isfield(handles.user_data, 'current_location_series')
        current_location_series = handles.user_data.current_location_series;
        for i = 1:length(current_location_series)
            if current_location_series(i) ~= 0
                if ishandle(current_location_series(i))
                    delete(current_location_series(i));
                end
            end
        end
    end

    if ~isempty(location_value)
        current_location_series = zeros(1, (8 * 2) * length(location_value));

        annotations = zeros(size(location_value));
        
        for i = 1:length(location_value)
            i_offset = (8 * 2) * (i - 1);
            
            location_idx = handles.user_data.week_locations_list_index(location_value(i));
            location = handles.user_data.locations_rounded(location_idx, :);
            annotations(i) = handles.user_data.location_annotations(location_idx);

            figure(handles.user_data.figure_handle);

            firstday = handles.user_data.weeks(handles.user_data.current_week);

            display_days = handles.user_data.display_days;
            
            for j = display_days;
                thisday = firstday + j;

                day_data_idx = (handles.user_data.time_days == thisday) & (thisday >= handles.user_data.time_min);
                day_data = handles.user_data.data(day_data_idx);
                day_time = handles.user_data.time(day_data_idx);
                day_location_idx = handles.user_data.locations_rounded_idx(day_data_idx);

                if sum(day_location_idx == location_idx) > 0
                    if numel(display_days) > 1
                        subplot(numel(display_days), 7, ((j - min(display_days)) * numel(display_days)) + (1:6));
                    end
                    hold on;
                    current_location_series(i_offset + (j * 2) + 1) = scatter(day_time(day_location_idx == location_idx) - thisday, day_data(day_location_idx == location_idx), 300, [0 0 1], 'LineWidth', 1, 'Marker', 'o');
                    hold off;

                    if numel(display_days) > 1
                        subplot(numel(display_days), 7, ((j - min(display_days)) * numel(display_days)) + 7);
                        hold on;
                        current_location_series(i_offset + (j * 2) + 2) = scatter(location(1), location(2), 200, [0 0 1], 'LineWidth', 3, 'Marker', 'x');
                        hold off;
                    end
                end
            end

            figure(handles.main_window);
            hold on;
            x_coords = (handles.user_data.rounded_accuracy * [-0.5 0.5 0.5 -0.5]) + location(1);
            y_coords = (handles.user_data.rounded_accuracy * [-0.5 -0.5 0.5 0.5]) + location(2);
            current_location_series(i_offset + (7 * 2) + 1) = patch(x_coords, y_coords, [0, 0, 1], 'FaceAlpha', 0.1);
            current_location_series(i_offset + (7 * 2) + 2) = scatter(location(1), location(2), 200, [0 0 1], 'LineWidth', 3, 'Marker', 'x');
            hold off;
            handles.user_data.current_location_series = current_location_series;
        end
        
        if range(annotations) == 0
            switch annotations(1)
                case 0
                    set(handles.lblLabelNone, 'Value', 1);
                case 1
                    set(handles.lblLabelHome, 'Value', 1);
                case 2
                    set(handles.lblLabelWork, 'Value', 1);
                case 3
                    set(handles.lblLabelPlay, 'Value', 1);
            end
        else
            set(handles.lblLabelNone, 'Value', 0);
            set(handles.lblLabelHome, 'Value', 0);
            set(handles.lblLabelWork, 'Value', 0);
            set(handles.lblLabelPlay, 'Value', 0);

        end
        
        set(handles.lblLabelNone, 'Enable', 'on');
        set(handles.lblLabelHome, 'Enable', 'on');
        set(handles.lblLabelWork, 'Enable', 'on');
        set(handles.lblLabelPlay, 'Enable', 'on');
    else
        set(handles.lblLabelNone, 'Value', 0);
        set(handles.lblLabelHome, 'Value', 0);
        set(handles.lblLabelWork, 'Value', 0);
        set(handles.lblLabelPlay, 'Value', 0);
        set(handles.lblLabelNone, 'Enable', 'off');
        set(handles.lblLabelHome, 'Enable', 'off');
        set(handles.lblLabelWork, 'Enable', 'off');
        set(handles.lblLabelPlay, 'Enable', 'off');
    end
    
    handles_out = handles;

% --- Executes on selection change in lstLocations.
function lstLocations_Callback(hObject, eventdata, handles) %#ok<*INUSL,DEFNU>
% hObject    handle to lstLocations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = select_location(handles);
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function lstLocations_CreateFcn(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to lstLocations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in cboWeek.
function cboWeek_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cboWeek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');
set(handles.chkValidWeek, 'Value', 0);
set(handles.chkValidWeek, 'Enable', 'off');
set(handles.chkDataStandardValid, 'Value', 0);
set(handles.chkDataGMSValid, 'Value', 0);
set(handles.chkDataCombinedValid, 'Value', 0);
set(handles.chkDataStandardValid, 'Enable', 'off');
set(handles.chkDataGMSValid, 'Enable', 'off');
set(handles.chkDataCombinedValid, 'Enable', 'off');
set(handles.lstLocations, 'Enable', 'off');
set(handles.lblLabelNone, 'Enable', 'off');
set(handles.lblLabelHome, 'Enable', 'off');
set(handles.lblLabelWork, 'Enable', 'off');
set(handles.lblLabelPlay, 'Enable', 'off');
set(handles.cboDay, 'Enable', 'off');
drawnow;
handles = plot_week(get(hObject,'Value'), handles);
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function cboWeek_CreateFcn(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to cboWeek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function handles_out = annotate_current_location(location_annotation, handles)
    location_value = get(handles.lstLocations,'Value');
    
    for i = 1:length(location_value)
        location_idx = handles.user_data.week_locations_list_index(location_value(i));
        handles.user_data.location_annotations(location_idx) = location_annotation;

        figure(handles.user_data.figure_handle);

        firstday = handles.user_data.weeks(handles.user_data.current_week);

        clr = handles.user_data.annotation_colors;
        
        display_days = handles.user_data.display_days;

        for j = display_days
            thisday = firstday + j;

            day_data_idx = (handles.user_data.time_days == thisday);
            day_data = handles.user_data.data(day_data_idx);
            day_time = handles.user_data.time(day_data_idx);
            day_location_idx = handles.user_data.locations_rounded_idx(day_data_idx);

            if sum(day_location_idx == location_idx) > 0
                if numel(display_days) > 1
                    subplot(numel(display_days), 7, ((j - min(display_days)) * numel(display_days)) + (1:6));
                end
                hold on;
                % This isn't ideal since we're just overlaying each time, but
                % it will be cleared on reload so meh.
                scatter(day_time(day_location_idx == location_idx) - thisday, day_data(day_location_idx == location_idx), 200, clr(location_annotation + 1, :), 'LineWidth', 2, 'Marker', 'x');
                hold off;
            end
        end

        string_locations = get(handles.lstLocations, 'String');

        string_locations{location_value(i)} = handles.user_data.week_location_strings_base{location_value(i)};

        switch location_annotation
            case 1
                string_locations{location_value(i)} = [string_locations{location_value(i)} ' (Home)'];
            case 2
                string_locations{location_value(i)} = [string_locations{location_value(i)} ' (Work)'];
            case 3
                string_locations{location_value(i)} = [string_locations{location_value(i)} ' (Play)'];
        end

        set(handles.lstLocations, 'String', string_locations);
    end
    
    save_annotaions(handles.user_data);
    
    handles_out = handles;


function save_annotaions(ud)
    save([ud.data_path '-annotation.mat'], ...
        '-struct', 'ud', 'locations_rounded', 'location_annotations', ...
        'week_annotations', '-v7.3')


% --- Executes on button press in lblLabelNone.
function lblLabelNone_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblLabelNone (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = annotate_current_location(0, handles);
guidata(hObject, handles);


% --- Executes on button press in lblLabelHome.
function lblLabelHome_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblLabelHome (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = annotate_current_location(1, handles);
guidata(hObject, handles);


% --- Executes on button press in lblLabelWork.
function lblLabelWork_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblLabelWork (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = annotate_current_location(2, handles);
guidata(hObject, handles);


% --- Executes on button press in lblLabelPlay.
function lblLabelPlay_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblLabelPlay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = annotate_current_location(3, handles);
guidata(hObject, handles);


% --- Executes on button press in cmdAddLocations.
function cmdAddLocations_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cmdAddLocations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.lstLocations, 'Enable', 'off');
set(handles.cmdAddLocations, 'Enable', 'off');
set(handles.lblLabelNone, 'Enable', 'off');
set(handles.lblLabelHome, 'Enable', 'off');
set(handles.lblLabelWork, 'Enable', 'off');
set(handles.lblLabelPlay, 'Enable', 'off');

rect = getrect(handles.main_window);

j = find(handles.user_data.week_locations_list_index == 0, 1, 'first');
%    handles.user_data.week_locations_index = week_locations_sorted_idx;
%handles.user_data.locations_rounded(handles.user_data.week_locations, :)

string_locations = get(handles.lstLocations, 'String');

locations_selected = zeros(size(handles.user_data.week_locations));
k = 1;

for i=1:length(handles.user_data.week_locations_raw)
    if sum(locations_selected == handles.user_data.week_locations_raw_rounded_idx(i)) == 0
        location = handles.user_data.locations_raw(handles.user_data.week_locations_raw(i), :);
        if (location(1) >= rect(1)) && (location(1) <= (rect(1) + rect(3))) && ...
                (location(2) >= rect(2)) && (location(2) <= (rect(2) + rect(4)))

            locations_selected(k) = handles.user_data.week_locations_raw_rounded_idx(i);
            k = k + 1;
        end
    end
end

[~, locations_selected] = ismember(locations_selected, handles.user_data.week_locations);

items_to_select = zeros(k-1, 1);

if k > 1
    for i=1:(k-1)
        location = locations_selected(i);
        if ~ismember(handles.user_data.week_locations_list_index, handles.user_data.week_locations(location))
            handles.user_data.week_locations_list_index(j) = handles.user_data.week_locations(location);

            string_locations{j} = handles.user_data.week_location_strings_base{j};

            switch handles.user_data.location_annotations(handles.user_data.week_locations(location))
                case 1
                    string_locations{j} = [string_locations{j} ' (Home)'];
                case 2
                    string_locations{j} = [string_locations{j} ' (Work)'];
                case 3
                    string_locations{j} = [string_locations{j} ' (Play)'];
            end

            items_to_select(i) = j;
            
            j = j + 1;
        else
            items_to_select(i) = find(handles.user_data.week_locations_list_index == handles.user_data.week_locations(location), 1, 'first');
        end
    end
end

set(handles.lstLocations, 'String', string_locations);

set(handles.lstLocations, 'Value', items_to_select);

handles = select_location(handles);

set(handles.lstLocations, 'Enable', 'on');
set(handles.cmdAddLocations, 'Enable', 'on');

guidata(hObject, handles);



function txtParticipantFilter_Callback(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to txtParticipantFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtParticipantFilter as text
%        str2double(get(hObject,'String')) returns contents of txtParticipantFilter as a double


% --- Executes during object creation, after setting all properties.
function txtParticipantFilter_CreateFcn(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to txtParticipantFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cmdLoadFiles.
function cmdLoadFiles_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cmdLoadFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
filter = get(handles.txtParticipantFilter, 'String');

set_system_setting(mfilename, 'participant_filter', filter);

set(handles.lstParticipants, 'String', '');
set(handles.cboWeek, 'String', ' ');
set(handles.cboWeek, 'Value', 1);
set(handles.chkValidWeek, 'Value', 0);
set(handles.chkDataStandardValid, 'Value', 0);
set(handles.chkDataGMSValid, 'Value', 0);
set(handles.chkDataCombinedValid, 'Value', 0);
set(handles.lstLocations, 'String', '');

set(handles.lstParticipants, 'Enable', 'off');
set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');
set(handles.chkValidWeek, 'Enable', 'off');
set(handles.chkDataStandardValid, 'Enable', 'off');
set(handles.chkDataGMSValid, 'Enable', 'off');
set(handles.chkDataCombinedValid, 'Enable', 'off');
set(handles.lstLocations, 'Enable', 'off');
set(handles.cmdAddLocations, 'Enable', 'off');
set(handles.lblLabelNone, 'Value', 0);
set(handles.lblLabelHome, 'Value', 0);
set(handles.lblLabelWork, 'Value', 0);
set(handles.lblLabelPlay, 'Value', 0);
set(handles.lblLabelNone, 'Enable', 'off');
set(handles.lblLabelHome, 'Enable', 'off');
set(handles.lblLabelWork, 'Enable', 'off');
set(handles.lblLabelPlay, 'Enable', 'off');
set(handles.cboDay, 'String', ' ');
set(handles.cboDay, 'Value', 1);
set(handles.cboDay, 'Enable', 'off');

handles.user_data.participant = '';

figure(handles.main_window);
cla;
colorbar off;

drawnow;

if length(filter) == 1
    filter = '*';
end

files = get_participants('participant_filter', filter);

participants = cell(1, length(files) + 1);
participants{1} = '';
j = 2;

for file = files'
    participants{j} = file.name;
    j = j + 1;
end

if j > 2
    set(handles.lstParticipants, 'String', participants(1:(j-1)));
    set(handles.lstParticipants, 'Value', 1);
    set(handles.lstParticipants, 'Enable', 'on');
end

% Update handles structure
guidata(hObject, handles);


function handles_out = select_participant(handles, save_week)
    set(handles.lstParticipants, 'Enable', 'off');
    
    set(handles.lblDataStandard, 'Enable', 'off');
    set(handles.lblDataGMS, 'Enable', 'off');
    set(handles.lblDataCombined, 'Enable', 'off');
    set(handles.lblDataAll, 'Enable', 'off');
    set(handles.chkAfterUpgrade, 'Enable', 'off');
    
    set(handles.cboWeek, 'String', ' ');
    set(handles.cboWeek, 'Value', 1);
    set(handles.chkValidWeek, 'Value', 0);
    set(handles.chkDataStandardValid, 'Value', 0);
    set(handles.chkDataGMSValid, 'Value', 0);
    set(handles.chkDataCombinedValid, 'Value', 0);
    set(handles.lstLocations, 'String', '');

    set(handles.cmdNextWeek, 'Enable', 'off');
    set(handles.cmdPrevWeek, 'Enable', 'off');
    set(handles.cboWeek, 'Enable', 'off');
    set(handles.chkValidWeek, 'Enable', 'off');
    set(handles.chkDataStandardValid, 'Enable', 'off');
    set(handles.chkDataGMSValid, 'Enable', 'off');
    set(handles.chkDataCombinedValid, 'Enable', 'off');
    set(handles.lstLocations, 'Enable', 'off');
    set(handles.cmdAddLocations, 'Enable', 'off');
    set(handles.lblLabelNone, 'Value', 0);
    set(handles.lblLabelHome, 'Value', 0);
    set(handles.lblLabelWork, 'Value', 0);
    set(handles.lblLabelPlay, 'Value', 0);
    set(handles.lblLabelNone, 'Enable', 'off');
    set(handles.lblLabelHome, 'Enable', 'off');
    set(handles.lblLabelWork, 'Enable', 'off');
    set(handles.lblLabelPlay, 'Enable', 'off');
    
    set(handles.cboDay, 'String', ' ');
    set(handles.cboDay, 'Value', 1);
    set(handles.cboDay, 'Enable', 'off');

    figure(handles.main_window);
    cla;
    colorbar off;

    drawnow;

    participant_id = get(handles.lstParticipants, 'Value');
    participant_string = get(handles.lstParticipants, 'String');
    participant = participant_string{participant_id};
    
    week_min_start = -inf;
    
    if save_week && ~isempty(handles.user_data.weeks)
        week_min_start = handles.user_data.weeks(handles.user_data.current_week);
    end
    
    if ~isempty(participant)
        set(handles.cboWeek, 'String', 'Loading...');
        set(handles.cboWeek, 'Value', 1);
        drawnow;

        handles = load_data(participant, handles);
        
        if ~isempty(handles.user_data.weeks)
            if ~isinf(week_min_start)
                week_idx = find(handles.user_data.weeks >= week_min_start, 1, 'first');
            else
                week_idx = find(handles.user_data.weeks_valid_data, 1, 'first');
            end
            
            if ~isempty(week_idx)
                handles = plot_week(week_idx, handles);
            else
                handles = plot_week(1, handles);
            end
        end
    end
    
    handles.user_data.participant = participant;
    
    set(handles.lstParticipants, 'Enable', 'on');
    
    set(handles.lblDataStandard, 'Enable', 'on');
    set(handles.lblDataGMS, 'Enable', 'on');
    set(handles.lblDataCombined, 'Enable', 'on');
    set(handles.lblDataAll, 'Enable', 'on');
    set(handles.chkAfterUpgrade, 'Enable', 'on');
    
    handles_out = handles;

    
% --- Executes on selection change in lstParticipants.
function lstParticipants_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lstParticipants (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

participant_id = get(handles.lstParticipants, 'Value');
participant_string = get(handles.lstParticipants, 'String');
participant = participant_string{participant_id};

if isfield(handles, 'user_data') && isfield(handles.user_data, 'participant') && strcmp(handles.user_data.participant, participant)
    handles = select_participant(handles, true);
else
    handles = select_participant(handles, false);
end

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function lstParticipants_CreateFcn(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to lstParticipants (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chkAfterUpgrade.
function chkAfterUpgrade_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to chkAfterUpgrade (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.after_upgrade = logical(get(hObject, 'Value'));
set_system_setting(mfilename, 'after_upgrade', handles.user_data.after_upgrade);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDataStandard.
function lblDataStandard_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDataStandard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.variant = '';
set_system_setting(mfilename, 'variant', handles.user_data.variant);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDataGMS.
function lblDataGMS_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDataGMS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.variant = 'gms';
set_system_setting(mfilename, 'variant', handles.user_data.variant);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDataCombined.
function lblDataCombined_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDataCombined (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.variant = 'combined_std_gms';
set_system_setting(mfilename, 'variant', handles.user_data.variant);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDisplayStandard.
function lblDisplayStandard_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDisplayStandard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.display_type = '';
set_system_setting(mfilename, 'display_type', handles.user_data.display_type);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDisplayOriginal.
function lblDisplayOriginal_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDisplayOriginal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.display_type = 'original';
set_system_setting(mfilename, 'display_type', handles.user_data.display_type);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDisplayValidated.
function lblDisplayValidated_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDisplayValidated (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.display_type = 'validated';
set_system_setting(mfilename, 'display_type', handles.user_data.display_type);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDisplayRegularised.
function lblDisplayRegularised_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDisplayRegularised (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.display_type = 'regularised';
set_system_setting(mfilename, 'display_type', handles.user_data.display_type);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDisplayNormalised.
function lblDisplayNormalised_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDisplayNormalised (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.display_type = 'normalised';
set_system_setting(mfilename, 'display_type', handles.user_data.display_type);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDisplayNormalisedDifferentiated.
function lblDisplayNormalisedDifferentiated_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDisplayNormalisedDifferentiated (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.display_type = 'normalised_differentiated';
set_system_setting(mfilename, 'display_type', handles.user_data.display_type);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDisplayNormalisedFiltered.
function lblDisplayNormalisedFiltered_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDisplayNormalisedFiltered (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.display_type = 'normalised_filtered';
set_system_setting(mfilename, 'display_type', handles.user_data.display_type);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDisplayNormalisedFilteredDifferentiated.
function lblDisplayNormalisedFilteredDifferentiated_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDisplayNormalisedFilteredDifferentiated (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.display_type = 'normalised_filtered_differentiated';
set_system_setting(mfilename, 'display_type', handles.user_data.display_type);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDisplayNormalisedSegmented.
function lblDisplayNormalisedSegmented_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDisplayNormalisedSegmented (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.display_type = 'normalised_segmented';
set_system_setting(mfilename, 'display_type', handles.user_data.display_type);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDisplayNormalisedFilteredSegmented.
function lblDisplayNormalisedFilteredSegmented_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDisplayNormalisedFilteredSegmented (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.user_data.display_type = 'normalised_filtered_segmented';
set_system_setting(mfilename, 'display_type', handles.user_data.display_type);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in chkValidWeek.
function chkValidWeek_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to chkValidWeek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

this_week_start = handles.user_data.weeks(handles.user_data.current_week);
this_week_annotation = (handles.user_data.week_annotations.start == this_week_start);

handles.user_data.week_annotations.valid(this_week_annotation) = logical(get(hObject, 'Value'));

this_variant = handles.user_data.variant;
if isempty(this_variant)
    this_variant = 'std';
end

this_variant_week_idx = (handles.user_data.variant_annotations.(this_variant).week_annotations.start == this_week_start);

handles.user_data.variant_annotations.(this_variant).week_annotations.valid(this_variant_week_idx) = logical(get(hObject, 'Value'));

if strcmp(this_variant, 'std')
    set(handles.chkDataStandardValid, 'Value', logical(get(hObject, 'Value')));
elseif strcmp(this_variant, 'gms')
    set(handles.chkDataGMSValid, 'Value', logical(get(hObject, 'Value')));
elseif strcmp(this_variant, 'combined_std_gms')
    set(handles.chkDataCombinedValid, 'Value', logical(get(hObject, 'Value')));
end

weeks_string = handles.user_data.weeks_string;
weeks_string{handles.user_data.current_week} = handles.user_data.weeks_string_base{handles.user_data.current_week};

if logical(get(hObject, 'Value'))
    weeks_string{handles.user_data.current_week} = [weeks_string{handles.user_data.current_week} ' ' char(10003)];
else
    for this_variant_cell = {'std', 'gms', 'combined_std_gms'}
        this_variant_week_idx = (handles.user_data.variant_annotations.(this_variant_cell{:}).week_annotations.start == this_week_start);
        
        if (sum(this_variant_week_idx) > 0) && (handles.user_data.variant_annotations.(this_variant_cell{:}).week_annotations.valid(this_variant_week_idx))
            weeks_string{handles.user_data.current_week} = [weeks_string{handles.user_data.current_week} ' ' char(9702)];
            break;
        end
    end
end

set(handles.cboWeek, 'String', weeks_string);

handles.user_data.weeks_string = weeks_string;

save_annotaions(handles.user_data);

% Update handles structure
guidata(hObject, handles);


% --- Executes on selection change in cboDay.
function cboDay_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cboDay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');
set(handles.chkValidWeek, 'Value', 0);
set(handles.chkValidWeek, 'Enable', 'off');
set(handles.chkDataStandardValid, 'Value', 0);
set(handles.chkDataGMSValid, 'Value', 0);
set(handles.chkDataCombinedValid, 'Value', 0);
set(handles.chkDataStandardValid, 'Enable', 'off');
set(handles.chkDataGMSValid, 'Enable', 'off');
set(handles.chkDataCombinedValid, 'Enable', 'off');
set(handles.lstLocations, 'Enable', 'off');
set(handles.lblLabelNone, 'Enable', 'off');
set(handles.lblLabelHome, 'Enable', 'off');
set(handles.lblLabelWork, 'Enable', 'off');
set(handles.lblLabelPlay, 'Enable', 'off');
set(handles.cboDay, 'Enable', 'off');
drawnow;

display_days = get(hObject,'Value');

if display_days == 1
    display_days = 0:6;
else
    display_days = display_days - 2;
end

handles = update_valid_checkboxes(handles);

handles = draw_data(handles, display_days);

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function cboDay_CreateFcn(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to cboDay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function save_current_figure(handles, type, varargin)
this_date = handles.user_data.weeks(handles.user_data.current_week);

if numel(handles.user_data.display_days) == 1
    this_date = this_date + handles.user_data.display_days;
end

date_str = datestr(this_date, 'yyyy-mm-dd');

if numel(handles.user_data.display_days) > 1
    date_str = ['wb_' date_str];
end

display_type_str = handles.user_data.display_type;

if numel(display_type_str) > 0
    display_type_str = ['_' display_type_str];
end

variant_str = handles.user_data.variant;

if numel(variant_str) > 0
    variant_str = ['_' variant_str];
end

filename = sprintf('%s_%s_%s%s%s', handles.user_data.participant, type, date_str, variant_str, display_type_str);

options = {};
options = [{'save_eps', get(handles.chkSaveEPS, 'Value')} options];
options = [{'save_fig', get(handles.chkSaveFIG, 'Value')} options];
options = [{'save_png', get(handles.chkSavePNG, 'Value')} options];

filename_suffix = get(handles.txtFilenameSuffix, 'String');

set_system_setting(mfilename, 'filename_suffix', filename_suffix);

if ~isempty(filename_suffix)
    options = [{'filename_suffix', filename_suffix} options];
end

save_figure(filename, 'output_sub_dir', 'location/annotator', options{:}, varargin{:});


function save_main_figure(handles, varargin)
fig = figure;
h = axes;
p = get(gca, 'position');
delete(h);
copyobj(handles.axes1, fig);
set(gca, 'units', 'normalized');
set(gca, 'position', p);
colormap(gca, colormap(handles.axes1))

type_str = 'coordinates';

if get(handles.chkShow3D, 'Value')
    type_str = [type_str '_3d'];
end

save_current_figure(handles, type_str, varargin{:});

close(fig)


function save_detail_figure(handles, varargin)
if isfield(handles.user_data, 'figure_handle') && ...
        ishandle(handles.user_data.figure_handle)
    figure(handles.user_data.figure_handle);

    p = get(gcf, 'PaperPosition');
    p2 = p;
    if numel(handles.user_data.display_days) == 1
        p2(4) = p2(4) / 2;
    else
        p2(3) = p2(3) * 2;
        p2(4) = p2(4) * 2;
    end
    set(gcf, 'PaperPosition', p2);

    save_current_figure(handles, 'distance', varargin{:});

    set(gcf, 'PaperPosition', p);
else
    warning('Distance figure closed.');
end


function save_figures(handles, save_main, save_detail)
if save_detail
    save_detail_figure(handles);
end
if save_main
    save_main_figure(handles);
end


function handles_out = save_figures_loop(handles, save_main, save_detail)

if get(handles.optSaveThisWeek, 'Value')
    save_figures(handles, save_main, save_detail);
elseif get(handles.optSaveAllWeeks, 'Value')
    for w = 1:numel(handles.user_data.weeks)
        handles = plot_week(w, handles);
        save_figures(handles, save_main, save_detail);
    end
elseif get(handles.optSaveAllParticipants, 'Value')
    participants = get(handles.lstParticipants, 'String');

    for p = 2:numel(participants)
        set(handles.lstParticipants, 'Value', p);
        handles = select_participant(handles, false);
        for w = 1:numel(handles.user_data.weeks)
            handles = plot_week(w, handles);
            save_figures(handles, save_main, save_detail);
        end
    end
end

handles_out = handles;


% --- Executes on button press in cmdSaveDetailFigure.
function cmdSaveDetailFigure_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cmdSaveDetailFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = save_figures_loop(handles, false, true);
guidata(hObject, handles);


% --- Executes on button press in cmdSaveMainFigure.
function cmdSaveMainFigure_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cmdSaveDetailFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = save_figures_loop(handles, true, false);
guidata(hObject, handles);


% --- Executes on button press in cmdSaveBothFigures.
function cmdSaveBothFigures_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to cmdSaveBothFigures (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = save_figures_loop(handles, true, true);
guidata(hObject, handles);


% --- Executes on button press in chkSavePNG.
function chkSavePNG_Callback(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to chkSavePNG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set_system_setting(mfilename, 'save_png', get(hObject,'Value'));


% --- Executes on button press in chkSaveFIG.
function chkSaveFIG_Callback(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to chkSaveFIG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set_system_setting(mfilename, 'save_fig', get(hObject,'Value'));


% --- Executes on button press in chkSaveEPS.
function chkSaveEPS_Callback(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to chkSaveEPS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set_system_setting(mfilename, 'save_eps', get(hObject,'Value'));


% --- Executes on button press in optSaveAllParticipants.
function optSaveAllParticipants_Callback(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to optSaveAllParticipants (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set_system_setting(mfilename, 'save_all_participants', get(hObject,'Value'));
set_system_setting(mfilename, 'save_all_weeks', false);
set_system_setting(mfilename, 'save_this_week', false);


% --- Executes on button press in optSaveAllWeeks.
function optSaveAllWeeks_Callback(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to optSaveAllWeeks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set_system_setting(mfilename, 'save_all_participants', false);
set_system_setting(mfilename, 'save_all_weeks', get(hObject,'Value'));
set_system_setting(mfilename, 'save_this_week', false);


% --- Executes on button press in optSaveThisWeek.
function optSaveThisWeek_Callback(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to optSaveThisWeek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set_system_setting(mfilename, 'save_all_participants', false);
set_system_setting(mfilename, 'save_all_weeks', false);
set_system_setting(mfilename, 'save_this_week', get(hObject,'Value'));


function txtFilenameSuffix_Callback(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to txtFilenameSuffix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtFilenameSuffix as text
%        str2double(get(hObject,'String')) returns contents of txtFilenameSuffix as a double


% --- Executes during object creation, after setting all properties.
function txtFilenameSuffix_CreateFcn(hObject, eventdata, handles) %#ok<INUSD,DEFNU>
% hObject    handle to txtFilenameSuffix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chkShow3D.
function chkShow3D_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to chkShow3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set_system_setting(mfilename, 'show_coordinates_3d', get(hObject,'Value'));

set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');
set(handles.chkValidWeek, 'Value', 0);
set(handles.chkValidWeek, 'Enable', 'off');
set(handles.chkDataStandardValid, 'Value', 0);
set(handles.chkDataGMSValid, 'Value', 0);
set(handles.chkDataCombinedValid, 'Value', 0);
set(handles.chkDataStandardValid, 'Enable', 'off');
set(handles.chkDataGMSValid, 'Enable', 'off');
set(handles.chkDataCombinedValid, 'Enable', 'off');
set(handles.lstLocations, 'Enable', 'off');
set(handles.lblLabelNone, 'Enable', 'off');
set(handles.lblLabelHome, 'Enable', 'off');
set(handles.lblLabelWork, 'Enable', 'off');
set(handles.lblLabelPlay, 'Enable', 'off');
set(handles.cboDay, 'Enable', 'off');
drawnow;
handles = plot_week(handles.user_data.current_week, handles);
guidata(hObject, handles);


% --- Executes on button press in chkStationaryOnly.
function chkStationaryOnly_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to chkStationaryOnly (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set_system_setting(mfilename, 'show_stationary_only', get(hObject,'Value'));

set(handles.cmdNextWeek, 'Enable', 'off');
set(handles.cmdPrevWeek, 'Enable', 'off');
set(handles.cboWeek, 'Enable', 'off');
set(handles.chkValidWeek, 'Value', 0);
set(handles.chkValidWeek, 'Enable', 'off');
set(handles.chkDataStandardValid, 'Value', 0);
set(handles.chkDataGMSValid, 'Value', 0);
set(handles.chkDataCombinedValid, 'Value', 0);
set(handles.chkDataStandardValid, 'Enable', 'off');
set(handles.chkDataGMSValid, 'Enable', 'off');
set(handles.chkDataCombinedValid, 'Enable', 'off');
set(handles.lstLocations, 'Enable', 'off');
set(handles.lblLabelNone, 'Enable', 'off');
set(handles.lblLabelHome, 'Enable', 'off');
set(handles.lblLabelWork, 'Enable', 'off');
set(handles.lblLabelPlay, 'Enable', 'off');
set(handles.cboDay, 'Enable', 'off');
drawnow;
handles = plot_week(handles.user_data.current_week, handles);
guidata(hObject, handles);


% --- Executes on button press in chkDataStandardValid.
function chkDataStandardValid_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to chkDataStandardValid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

this_week_start = handles.user_data.weeks(handles.user_data.current_week);

this_variant = 'std';

this_variant_week_idx = (handles.user_data.variant_annotations.(this_variant).week_annotations.start == this_week_start);

handles.user_data.variant_annotations.(this_variant).week_annotations.valid(this_variant_week_idx) = logical(get(hObject, 'Value'));

weeks_string = handles.user_data.weeks_string;
weeks_string{handles.user_data.current_week} = handles.user_data.weeks_string_base{handles.user_data.current_week};

for this_variant_cell = {'std', 'gms', 'combined_std_gms'}
    this_variant_week_idx = (handles.user_data.variant_annotations.(this_variant_cell{:}).week_annotations.start == this_week_start);

    if (sum(this_variant_week_idx) > 0) && (handles.user_data.variant_annotations.(this_variant_cell{:}).week_annotations.valid(this_variant_week_idx))
        weeks_string{handles.user_data.current_week} = [weeks_string{handles.user_data.current_week} ' ' char(9702)];
        break;
    end
end

set(handles.cboWeek, 'String', weeks_string);

handles.user_data.weeks_string = weeks_string;

save_annotaions(handles.user_data.variant_annotations.(this_variant));

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in chkDataGMSValid.
function chkDataGMSValid_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to chkDataGMSValid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

this_week_start = handles.user_data.weeks(handles.user_data.current_week);

this_variant = 'gms';

this_variant_week_idx = (handles.user_data.variant_annotations.(this_variant).week_annotations.start == this_week_start);

handles.user_data.variant_annotations.(this_variant).week_annotations.valid(this_variant_week_idx) = logical(get(hObject, 'Value'));

weeks_string = handles.user_data.weeks_string;
weeks_string{handles.user_data.current_week} = handles.user_data.weeks_string_base{handles.user_data.current_week};

for this_variant_cell = {'std', 'gms', 'combined_std_gms'}
    this_variant_week_idx = (handles.user_data.variant_annotations.(this_variant_cell{:}).week_annotations.start == this_week_start);

    if (sum(this_variant_week_idx) > 0) && (handles.user_data.variant_annotations.(this_variant_cell{:}).week_annotations.valid(this_variant_week_idx))
        weeks_string{handles.user_data.current_week} = [weeks_string{handles.user_data.current_week} ' ' char(9702)];
        break;
    end
end

set(handles.cboWeek, 'String', weeks_string);

handles.user_data.weeks_string = weeks_string;

save_annotaions(handles.user_data.variant_annotations.(this_variant));

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in chkDataCombinedValid.
function chkDataCombinedValid_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to chkDataCombinedValid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

this_week_start = handles.user_data.weeks(handles.user_data.current_week);

this_variant = 'combined_std_gms';

this_variant_week_idx = (handles.user_data.variant_annotations.(this_variant).week_annotations.start == this_week_start);

handles.user_data.variant_annotations.(this_variant).week_annotations.valid(this_variant_week_idx) = logical(get(hObject, 'Value'));

weeks_string = handles.user_data.weeks_string;
weeks_string{handles.user_data.current_week} = handles.user_data.weeks_string_base{handles.user_data.current_week};

for this_variant_cell = {'std', 'gms', 'combined_std_gms'}
    this_variant_week_idx = (handles.user_data.variant_annotations.(this_variant_cell{:}).week_annotations.start == this_week_start);

    if (sum(this_variant_week_idx) > 0) && (handles.user_data.variant_annotations.(this_variant_cell{:}).week_annotations.valid(this_variant_week_idx))
        weeks_string{handles.user_data.current_week} = [weeks_string{handles.user_data.current_week} ' ' char(9702)];
        break;
    end
end

set(handles.cboWeek, 'String', weeks_string);

handles.user_data.weeks_string = weeks_string;

save_annotaions(handles.user_data.variant_annotations.(this_variant));

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in lblDataAll.
function lblDataAll_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to lblDataAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.user_data.variant = 'all';
set_system_setting(mfilename, 'variant', handles.user_data.variant);

if ~isempty(handles.user_data.participant)
    handles = select_participant(handles, true);
end

% Update handles structure
guidata(hObject, handles);
