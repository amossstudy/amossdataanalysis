function plot_histogram_3d(x, y, z, c)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 12-Feb-2015

    if nargin < 4
        c = z;
    end
    
    clim = [min(min(c)) max(max(c))];
    
    dx = x(2) - x(1);
    dy = y(2) - y(1);
    
    h = bar3(z, 1);
    
    for i = 1:size(z,2)
        % Remove NaN bars
        zdata = get(h(i),'zdata');
        k = 1;
        for j = 0:6:(6*size(z,1)-6)
            if isnan(z(k,i))
                zdata(j+1:j+6,:) = NaN;
            end
            k = k+1;
        end
        set(h(i),'zdata',zdata);
        
        % Set colour of each bar to the height
        cdata = get(h(i),'cdata');
        k = 1;
        for j = 0:6:(6*size(z,1)-6)
            if ~isnan(z(k,i))
                cdata(j+1:j+6,:) = c(k,i);
            end
            k = k+1;
        end
        set(h(i),'cdata',cdata);
        set(h(i),'LineWidth', 0.01);
        
        % Correct x-coordinates of bars
        xdata = get(h(i),'xdata');
        xdata_new = zeros(size(xdata));
        xdata_new(:, [1 2]) = x(i) - (dx / 2);
        xdata_new(:, [3 4]) = x(i) + (dx / 2);
        xdata_new(isnan(xdata)) = NaN;
        set(h(i),'xdata',xdata_new);
        
        % Correct y-coordinates of bars
        ydata = get(h(i),'ydata');
        ydata_new = zeros(size(ydata));
        for j = 0:6:(size(ydata, 1) - 6)
            ydata_new([(j+1) (j+2)], :) = y((j / 6) + 1) - (dy / 2);
            ydata_new([(j+3) (j+4)], :) = y((j / 6) + 1) + (dy / 2);
            ydata_new([(j+5) (j+6)], :) = y((j / 6) + 1) - (dy / 2);
        end
        ydata_new(isnan(ydata)) = NaN;
        set(h(i),'ydata',ydata_new);
    end
    
    % Correct axes for new x- and y-coordinates of bars
    set(gca, 'xlim', [(min(x) - (dx / 2)) (max(x) + (dx / 2))]);
    set(gca, 'ylim', [(min(y) - (dy / 2)) (max(y) + (dy / 2))]);
    
    set(gca, 'XTickMode', 'auto')
    set(gca, 'YTickMode', 'auto')
    
    if (clim(1) ~= clim(2)) && ~isnan(clim(1)) && ~isnan(clim(2))
        set(gca, 'clim', clim);
    end
end
