function fit_distributions_true_colours(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 01-Mar-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-quantity';
    
    generate_figures = get_argument({'generatefigures', 'generate_figures'}, true, varargin{:});
    
    files = get_participants(varargin{:});
    
    data_dates = datenum([2014, 1, 1]):datenum([2015, 3, 1]);
    
    questionaires = {'ALTMAN', 'EQ_5D', 'GAD_7', 'QIDS'};
    
    class_desc = {'HC', 'BD', 'BPD'};
    
    data = struct;
    
    for this_questionaire_cell = questionaires
        this_questionaire = this_questionaire_cell{1};
        this_questionaire_str = strrep(this_questionaire, '_', '\_');
        this_questionaire_lc = lower(this_questionaire);
        
        data.(this_questionaire) = struct;
        
        data_matrix_class = cell(3, 1);
        for i = 1:3
          data_matrix_class{i} = struct('data', NaN(length(files), length(data_dates)), 'j', 1);
        end

        for file = files'
            fprintf('Loading %s\n', file.name)

            class = get_participant_property(file.name, 'classification', varargin{:});

            if ~isempty(class)
                data_tc = load_true_colours_preprocessed(file.name);

                if ~isempty(data_tc) && isfield(data_tc, this_questionaire) && isfield(data_tc.(this_questionaire), 'regularised') && isfield(data_tc.(this_questionaire), 'weekly')
                    A = ismember(data_dates, data_tc.(this_questionaire).regularised.time);
                    B = ismember(data_tc.(this_questionaire).regularised.time, data_dates);

                    data_matrix_class{class + 1}.data(data_matrix_class{class + 1}.j, A) = data_tc.(this_questionaire).regularised.data(B);
                end
                
                data_matrix_class{class + 1}.j = data_matrix_class{class + 1}.j + 1;
            end
        end

        fprintf(' Processing %s\n', this_questionaire);
        
        if strcmp(this_questionaire, 'ALTMAN') == 1
            q_max = 20;
        elseif strcmp(this_questionaire, 'EQ_5D') == 1
            q_max = 100;
        elseif strcmp(this_questionaire, 'GAD_7') == 1
            q_max = 21;
        elseif strcmp(this_questionaire, 'QIDS') == 1
            q_max = 27;
        end
        
        if generate_figures == true
            figure;
        end
        
        d_hist = 1;
        
        if strcmp(this_questionaire, 'EQ_5D') == 1
            d_hist = 5;
        end
        
        hist_edges = (-d_hist / 2):d_hist:(q_max + (d_hist / 2));

        data.(this_questionaire).raw = struct;
        
        for i = 1:3
            fprintf('  Calculating %s score distribution for %s:', this_questionaire, class_desc{i});
            
            data_range_class = 1:(data_matrix_class{i}.j - 1);
            data_class = data_matrix_class{i}.data(data_range_class, :);

            if generate_figures == true
                subplot(3, 1, i);
                histogram(data_class(:), hist_edges);

                hold on;
            end
            
            data.(this_questionaire).raw.(class_desc{i}) = fit_distributions(data_class, hist_edges, 'offset', d_hist / 2);
            
            if generate_figures == true
                if strcmp(this_questionaire, 'EQ_5D') == 1
                    h_legend = legend;
                    set(h_legend, 'Location', 'NorthWest');
                end

                if i < 3
                  set(gca, 'xticklabel', []);
                end
                set(gca, 'ytick', []);
                set(gca, 'xlim', hist_edges([1 end]));
                if i == 1
                    title([this_questionaire_str ' Scores']);
                elseif i == 3
                    xlabel([this_questionaire_str ' Score']);
                end
                ylabel(class_desc{i});
                p = get(gca, 'Position');
                p_diff = p(4) * 0.15;
                p(4) = p(4) + p_diff;
                if i == 1
                    p(2) = p(2) - p_diff;
                elseif i < 3
                    p(2) = p(2) - (p_diff / 2);
                end
                set(gca, 'Position', p);
            end
        end

        if generate_figures == true
            save_figure([this_questionaire_lc '_score_dist'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

            figure;
        end
        
        d_hist = 2;
        
        if strcmp(this_questionaire, 'EQ_5D') == 1
            d_hist = 5;
        end
        
        hist_edges = 0:d_hist:(ceil(q_max / 2) * 2);
        
        data.(this_questionaire).mean = struct;
        
        for i = 1:3
            fprintf('  Calculating participant mean %s score distribution for %s:', this_questionaire, class_desc{i});
            
            data_range_class = 1:(data_matrix_class{i}.j - 1);
            data_class = data_matrix_class{i}.data(data_range_class, :);

            if generate_figures == true
                subplot(3, 1, i);
                histogram(nanmean(data_class, 2), hist_edges);

                hold on;
            end
            
            data.(this_questionaire).mean.(class_desc{i}) = fit_distributions(nanmean(data_class, 2), hist_edges);
            
            if generate_figures == true
                if strcmp(this_questionaire, 'EQ_5D') == 1
                    h_legend = legend;
                    set(h_legend, 'Location', 'NorthWest');
                end

                if i < 3
                  set(gca, 'xticklabel', []);
                end
                set(gca, 'ytick', []);
                set(gca, 'xlim', hist_edges([1 end]));
                if i == 1
                    title(['Participant ' this_questionaire_str ' Score Mean']);
                elseif i == 3
                    xlabel(['Mean ' this_questionaire_str ' Score']);
                end
                ylabel(class_desc{i});
                p = get(gca, 'Position');
                p_diff = p(4) * 0.15;
                p(4) = p(4) + p_diff;
                if i == 1
                    p(2) = p(2) - p_diff;
                elseif i < 3
                    p(2) = p(2) - (p_diff / 2);
                end
                set(gca, 'Position', p);
            end
        end

        if generate_figures == true
            save_figure([this_questionaire_lc '_score_mean_dist'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

            figure;
        end
        
        d_hist = 1;
        max_hist = 10;
        
        if strcmp(this_questionaire, 'EQ_5D') == 1
            d_hist = 2;
            max_hist = 30;
        end
        
        hist_edges = 0:d_hist:max_hist;
        
        data.(this_questionaire).std = struct;
        
        for i = 1:3
            fprintf('  Calculating participant %s score standard deviation distribution for %s:', this_questionaire, class_desc{i});
            
            data_range_class = 1:(data_matrix_class{i}.j - 1);
            data_class = data_matrix_class{i}.data(data_range_class, :);

            if generate_figures == true
                subplot(3, 1, i);
                histogram(nanstd(data_class, 0, 2), hist_edges);

                hold on;
            end
            
            data.(this_questionaire).std.(class_desc{i}) = fit_distributions(nanstd(data_class, 0, 2), hist_edges);
            
            if generate_figures == true
                if i < 3
                  set(gca, 'xticklabel', []);
                end
                set(gca, 'ytick', []);
                set(gca, 'xlim', hist_edges([1 end]));
                if i == 1
                    title(['Participant ' this_questionaire_str ' Score Standard Deviation']);
                elseif i == 3
                    xlabel([this_questionaire_str ' Score Standard Deviation']);
                end
                ylabel(class_desc{i});
                p = get(gca, 'Position');
                p_diff = p(4) * 0.15;
                p(4) = p(4) + p_diff;
                if i == 1
                    p(2) = p(2) - p_diff;
                elseif i < 3
                    p(2) = p(2) - (p_diff / 2);
                end
                set(gca, 'Position', p);
            end
        end

        if generate_figures == true
            save_figure([this_questionaire_lc '_score_sd_dist'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

            figure;
        end
        
        d_hist = 2;
        max_hist = 14;
        
        if strcmp(this_questionaire, 'EQ_5D') == 1
            d_hist = 5;
            max_hist = 50;
        end
        
        hist_edges = 0:d_hist:max_hist;
        
        data.(this_questionaire).iqr = struct;
        
        for i = 1:3
            fprintf('  Calculating participant %s score IQR for %s:', this_questionaire, class_desc{i});
            
            data_range_class = 1:(data_matrix_class{i}.j - 1);
            data_class = data_matrix_class{i}.data(data_range_class, :);

            if generate_figures == true
                subplot(3, 1, i);
                histogram(iqr(data_class, 2), hist_edges);

                hold on;
            end
            
            data.(this_questionaire).iqr.(class_desc{i}) = fit_distributions(iqr(data_class, 2), hist_edges);
            
            if generate_figures == true
                if i < 3
                  set(gca, 'xticklabel', []);
                end
                set(gca, 'ytick', []);
                set(gca, 'xlim', hist_edges([1 end]));
                if i == 1
                    title(['Participant ' this_questionaire_str ' Score IQR']);
                elseif i == 3
                    xlabel([this_questionaire_str ' Score IQR']);
                end
                ylabel(class_desc{i});
                p = get(gca, 'Position');
                p_diff = p(4) * 0.15;
                p(4) = p(4) + p_diff;
                if i == 1
                    p(2) = p(2) - p_diff;
                elseif i < 3
                    p(2) = p(2) - (p_diff / 2);
                end
                set(gca, 'Position', p);
            end
        end

        if generate_figures == true
            save_figure([this_questionaire_lc '_score_iqr_dist'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        end
    end
    
    save_data('true-colours-distributions', data, varargin{:});
end
