function [data] = load_true_colours_combined(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 29-Feb-2016

    paths = get_paths(varargin{:});
    
    true_colours_data_type = get_argument({'true_colours_data_type', 'truecoloursdatatype'}, 'raw', varargin{:});
    
    data = struct;
    
    if strcmpi(true_colours_data_type, 'raw')
        working_filename = 'combined-true-colours.mat';
    elseif strcmpi(true_colours_data_type, 'resampled')
        working_filename = 'combined-true-colours-resampled.mat';
    else
        error('Invalid True Colours data type: %s', true_colours_data_type);
    end
    
    if (paths.prefer_working) && (exist([paths.working_path working_filename], 'file') == 2)
        if ~isempty(paths.temp_path) && (exist(paths.temp_path, 'dir') == 7) && ...
                exist([paths.temp_path working_filename], 'file') == 2
            fprintf('  Loading %s (from temp): ', working_filename);
            tic; load([paths.temp_path working_filename]); toc;
        else
            fprintf('  Loading %s: ', working_filename);
            tic; load([paths.working_path working_filename]); toc;
        end
    else
        questionnaires = fieldnames(get_configuration('questionaires'))';

        data = struct();
        for this_questionnaire_cell = questionnaires
            data.(this_questionnaire_cell{:}) = struct();
            data.(this_questionnaire_cell{:}).participant = {};
        end

        for file = get_participants(varargin{:}, 'prefer_working', 0)'
            if strcmpi(true_colours_data_type, 'raw')
                tc_data = load_true_colours_data(file.name, varargin{:});
            elseif strcmpi(true_colours_data_type, 'resampled')
                tc_data = load_true_colours_resampled(file.name, varargin{:});
            end

            for this_questionnaire_cell = questionnaires
                this_q = this_questionnaire_cell{:};
                if isfield(tc_data, this_q)
                    for this_fieldname_cell = fieldnames(tc_data.(this_q))'
                        this_f = this_fieldname_cell{:};
                        if ~ismember(this_f, fieldnames(data.(this_q)))
                            data.(this_q).(this_f) = tc_data.(this_q).(this_f);
                        else
                            data.(this_q).(this_f) = [data.(this_q).(this_f); tc_data.(this_q).(this_f)];
                        end
                    end

                    data.(this_q).participant = [data.(this_q).participant; repmat({file.name}, numel(tc_data.(this_q).time), 1)];
                end
            end
        end
    end
end
