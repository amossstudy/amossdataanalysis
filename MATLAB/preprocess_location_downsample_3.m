function preprocess_location_downsample_2(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 22-Oct-2015

    variant = get_argument('variant', '', varargin{:});
    combine_variants = get_argument({'combinevariants', 'combine_variants'}, {}, varargin{:});
    
    if isempty(variant) && ~isempty(combine_variants)
        variant = 'combined';

        for i = 1:length(combine_variants)
            this_variant_name = combine_variants{i};

            if isempty(this_variant_name)
                this_variant_name = 'std';
            end

            variant = [variant '_' this_variant_name]; %#ok<AGROW>
        end
    end

    variant_out = '';
    
    if ~isempty(variant)
        variant_out = ['_' variant];
    end

    for file = get_participants(varargin{:})'
        fprintf('Processing %s', file.name)

        data = load_location_preprocessed(file.name, varargin{:}, 'variant', variant);

        if ~isempty(data) && isfield(data, 'normalised') && isfield(data.normalised, 'filtered') && ~isempty(data.normalised.filtered.time)
            fprintf('\n  Downsampling: ');
            
            location_data = [data.normalised.filtered.lat data.normalised.filtered.lon]; % for normalised coordinates, this is in km from home
            
            t_min = floor(min(data.normalised.filtered.time));
            t_max = floor(max(data.normalised.filtered.time)) + 1;
            
            t_start = 1;
            t_end = 1;
            
            t_range = t_min:(1 / 24):t_max;
            
            ds_time = NaN(numel(t_range) * (60 + 12), 1);
            ds_location_data = NaN(numel(t_range) * (60 + 12), 2);
            
            ds_i = 1;
            
            last_status_text = '';
            
            ds_window_5min = (1 / (24 * 12));
            
            for i = 2:length(t_range)
                if (i == 2) || (rem(i, 500) == 0) || (i == length(t_range))
                    if ~isempty(last_status_text)
                        fprintf(repmat('\b', 1, length(last_status_text) - 1))
                    end
                    last_status_text = sprintf('%.2f%%%%', (i / length(t_range)) * 100);
                    fprintf(last_status_text);
                end
                
                ds_time_new_5min = (t_range(i - 1):ds_window_5min:(t_range(i) - ds_window_5min)) + (ds_window_5min / 2);
                ds_location_data_new_5min = NaN(length(ds_time_new_5min), 2);
                
                while (t_start < numel(data.normalised.filtered.time)) && ...
                        (data.normalised.filtered.time(t_start) < t_range(i - 1));
                    t_start = t_start + 1;
                end
                
                if data.normalised.filtered.time(t_start) < t_range(i)
                    t_end = max(t_end, t_start);

                    while ((t_end + 1) < numel(data.normalised.filtered.time)) && ...
                            (data.normalised.filtered.time(t_end + 1) < t_range(i));
                        t_end = t_end + 1;
                    end
                    
                    this_time = data.normalised.filtered.time(t_start:t_end);
                    this_location_data = location_data(t_start:t_end, :);
                    
                    if (std(this_location_data(:, 1)) < 0.01) && ...
                            (std(this_location_data(:, 2)) < 0.01)
                        for j = 1:numel(ds_time_new_5min)
                            if sum((this_time >= (ds_time_new_5min(j) - (ds_window_5min / 2))) & ...
                                    (this_time < (ds_time_new_5min(j) + (ds_window_5min / 2)))) > 0
                                ds_location_data_new_5min(j, :) = mean(this_location_data, 1);
                            end
                        end
                    else
                        for j = 1:numel(ds_time_new_5min)
                            valid_range = (this_time >= (ds_time_new_5min(j) - (ds_window_5min / 2))) & (this_time < (ds_time_new_5min(j) + (ds_window_5min / 2)));
                            
                            if sum(valid_range) > 0
                                range_location_data = this_location_data(valid_range, :);
                                
                                ds_location_data_new_5min(j, :) = median(range_location_data, 1);
                            end
                        end
                    end
                end
                
                ds_time(ds_i:(ds_i + numel(ds_time_new_5min) - 1)) = ds_time_new_5min;
                ds_location_data(ds_i:(ds_i + numel(ds_time_new_5min) - 1), :) = ds_location_data_new_5min;
                
                ds_i = ds_i + numel(ds_time_new_5min);
                t_start = t_end;
            end
            
            if ~isempty(last_status_text)
                fprintf(repmat('\b', 1, length(last_status_text) - 1))
            end
            fprintf('done');
            
            ds_data = struct();
            ds_data.time = ds_time;
            ds_data.data = sqrt(sum(ds_location_data .^ 2, 2));
            ds_data.lat = ds_location_data(:, 1);
            ds_data.lon = ds_location_data(:, 2);
            ds_data.filled_state = zeros(size(ds_time));
            
            location_dxydt = calculate_location_differential(ds_data);
            idx = (location_dxydt.data > 1000);
            
            while (sum(idx) > 0)
                ds_data.data(idx) = NaN;
                ds_data.lat(idx) = NaN;
                ds_data.lon(idx) = NaN;
                
                location_dxydt = calculate_location_differential(ds_data);
                idx = (location_dxydt.data > 1000);
            end
            
            fprintf('\n  Filling missing data: ');
            
            invalid_idx = find(isnan(ds_data.data));
            invalid_idx_start = invalid_idx([0; diff(invalid_idx)] > 1);
            invalid_idx_end = invalid_idx([diff(invalid_idx); 0] > 1);
            
            for i = 1:(numel(invalid_idx_start) - 1)
                i_start = invalid_idx_start(i);
                i_end = invalid_idx_end(i + 1);
                
                if (i_start > 1) && (i_end < ds_i)
                    t_start = ds_data.time(i_start);
                    t_end = ds_data.time(i_end);
                    
                    loc_start = ds_location_data(i_start - 1, :);
                    loc_end = ds_location_data(i_end + 1, :);

                    loc_diff = sqrt(sum((loc_start - loc_end) .^ 2, 2));

                    loc_fill_stationary = {};
                    i_fill_stationary = [];

                    loc_fill_transitioning = {};
                    i_fill_transitioning = [];

% %                     if t_start >= datenum([2016 01 25])
% %                         a = 1;
% %                     end

                    if ((loc_diff < 1) || ((sqrt(sum(loc_start .^ 2)) > 0.75) && (sqrt(sum(loc_end .^ 2)) > 0.75))) && ...
                            (((t_end - t_start) <= (1 / 4)) || ((t_start >= (21 / 24)) && ((t_end - t_start) <= (1 / 2)))) || ...
                            ((sqrt(sum(loc_start .^ 2)) < 0.25) && (sqrt(sum(loc_end .^ 2)) < 0.25) && (t_start >= (15 / 24)) && ((t_end - t_start) <= (3 / 4)))
                        
                        loc_fill_stationary = {mean([loc_start; loc_end], 1)};

                        min_transition_time = (sqrt(sum((loc_start - loc_fill_stationary{1}) .^ 2, 2)) / (80 * 24));

                        t_start_min = t_start + min_transition_time;
                        t_end_max = t_end - min_transition_time;

                        if t_start_min < t_end_max
                            i_start_min = find(ds_data.time <= t_start_min, 1, 'last');
                            i_end_max = find(ds_data.time >= t_end_max, 1, 'first');

                            if (i_start_min > i_start)
                                i_fill_transitioning = [i_start i_start_min - 1];
                                loc_fill_transitioning = {[loc_start; loc_fill_stationary{1}]};

                                i_start = i_start_min;
                            end

                            if (i_end_max < i_end)
                                i_fill_transitioning = [i_fill_transitioning; [i_end_max + 1 i_end]]; %#ok<AGROW>
                                loc_fill_transitioning = [loc_fill_transitioning, {[loc_fill_stationary{1}; loc_end]}]; %#ok<AGROW>

                                i_end = i_end_max;
                            end
                            
                            i_fill_stationary = [i_start i_end];
                        else
                            i_fill_stationary = [];
                            loc_fill_stationary = {};
                            i_fill_transitioning = [i_start i_end];
                            loc_fill_transitioning = {[loc_start; loc_end]};
                        end
                    elseif (sqrt(sum(loc_start .^ 2)) > 0.75) && (sqrt(sum(loc_end .^ 2)) < 0.75) && ...
                            (((t_end - t_start) <= (1 / 4)) || ((t_start >= (20 / 24)) && ((t_end - t_start) <= (3 / 4))))
                        i_fill_stationary = [i_start i_end];
                        loc_fill_stationary = {loc_start};


                        min_transition_time = (sqrt(sum(loc_fill_stationary{1} .^ 2, 2)) / (80 * 24));

                        [t_end_max, end_max_type] = min([t_start + (1 / 12), t_end - min_transition_time]);
                        i_end_max = find(ds_data.time >= t_end_max, 1, 'first');

                        if isempty(i_end_max)
                            i_end_max = i_start;
                        end

                        if (i_end_max < i_end) && (i_end_max > i_start)
                            i_fill_stationary = [i_start i_end_max];

                            if end_max_type == 1
                                t_remaining = t_end - ds_data.time(i_end_max - 1) - min_transition_time;
                                
                                if t_remaining > (0.5 / 24)
                                    i_start_end = find(ds_data.time >= (t_end - min(t_remaining, 1 / 12)), 1, 'first');
                                    
                                    i_fill_stationary = [i_fill_stationary; [i_start_end i_end]]; %#ok<AGROW>
                                    loc_fill_stationary = [loc_fill_stationary, {loc_end}]; %#ok<AGROW>
                                    
                                    i_end = i_start_end - 1;
                                end
                                
                                i_transition_mid = floor(mean([i_end_max + 1 i_end]));
                                t_transition_mid = ds_data.time(i_transition_mid);

                                t_transition_start = t_transition_mid - (min_transition_time / 2);
                                t_transition_end = t_transition_mid + (min_transition_time / 2);

                                i_transition_start = max(i_end_max + 1, find(ds_data.time <= t_transition_start, 1, 'last'));
                                i_transition_end = min(i_end, find(ds_data.time >= t_transition_end, 1, 'first'));

                                i_fill_transitioning = [i_transition_start i_transition_end];
                                loc_fill_transitioning = {[loc_start; loc_end]};
                            elseif end_max_type == 2
                                i_fill_transitioning = [i_end_max + 1 i_end];
                                loc_fill_transitioning = {[loc_start; loc_end]};
                            end
                        elseif (i_end_max <= i_start)
                            i_fill_stationary = [];
                            loc_fill_stationary = {};
                            i_fill_transitioning = [i_start i_end];
                            loc_fill_transitioning = {[loc_start; loc_end]};
                        end
                    elseif (sqrt(sum(loc_start .^ 2)) < 0.75) && (sqrt(sum(loc_end .^ 2)) > 0.75) && ...
                            (((t_end - t_start) <= (1 / 4)) || ((t_start >= (20 / 24)) && ((t_end - t_start) <= (3 / 4))))
                        i_fill_stationary = [i_start i_end];
                        loc_fill_stationary = {loc_end};

                        min_transition_time = (sqrt(sum(loc_fill_stationary{1} .^ 2, 2)) / (80 * 24));

                        [t_start_min, start_min_type] = max([t_end - (1 / 12), t_start + min_transition_time]);
                        i_start_min = find(ds_data.time <= t_start_min, 1, 'last');

                        if isempty(i_start_min)
                            i_start_min = i_end;
                        end

                        if (i_start_min > i_start) && (i_start_min < i_end)
                            i_fill_stationary = [i_start_min i_end];
                            if start_min_type == 1
                                t_remaining = ds_data.time(i_start_min - 1) - t_start - min_transition_time;
                                
                                if t_remaining > (0.5 / 24)
                                    i_end_start = find(ds_data.time <= (t_start + min(t_remaining, 1 / 12)), 1, 'last');
                                    
                                    i_fill_stationary = [i_fill_stationary; [i_start i_end_start]]; %#ok<AGROW>
                                    loc_fill_stationary = [loc_fill_stationary, {loc_start}]; %#ok<AGROW>
                                    
                                    i_start = i_end_start + 1;
                                end
                                
                                i_transition_mid = floor(mean([i_start i_start_min - 1]));
                                t_transition_mid = ds_data.time(i_transition_mid);

                                t_transition_start = t_transition_mid - (min_transition_time / 2);
                                t_transition_end = t_transition_mid + (min_transition_time / 2);

                                i_transition_start = max(i_start, find(ds_data.time <= t_transition_start, 1, 'last'));
                                i_transition_end = min(i_start_min - 1, find(ds_data.time >= t_transition_end, 1, 'first'));

                                i_fill_transitioning = [i_transition_start i_transition_end];
                                loc_fill_transitioning = {[loc_start; loc_end]};
                            elseif start_min_type == 2
                                i_fill_transitioning = [i_start i_start_min - 1];
                                loc_fill_transitioning = {[loc_start; loc_end]};
                            end
                        elseif (i_start_min >= i_end)
                            i_fill_stationary = [];
                            loc_fill_stationary = {};
                            i_fill_transitioning = [i_start i_end];
                            loc_fill_transitioning = {[loc_start; loc_end]};
                        end
                    end

                    for j = 1:size(i_fill_stationary, 1)
                        this_i_start = i_fill_stationary(j, 1) - 1;
                        this_i_end = i_fill_stationary(j, 2) + 1;
                        
                        ds_data.data(this_i_start:this_i_end) = sqrt(sum(loc_fill_stationary{j} .^ 2, 2));
                        ds_data.lat(this_i_start:this_i_end) = loc_fill_stationary{j}(1);
                        ds_data.lon(this_i_start:this_i_end) = loc_fill_stationary{j}(2);
                        ds_data.filled_state(this_i_start:this_i_end) = 1;
                    end

                    for j = 1:size(i_fill_transitioning, 1)
                        this_i_start = i_fill_transitioning(j, 1) - 1;
                        this_i_end = i_fill_transitioning(j, 2) + 1;

                        this_loc_start = loc_fill_transitioning{j}(1, :);
                        this_loc_end = loc_fill_transitioning{j}(2, :);

                        this_fill = NaN(this_i_end - this_i_start + 1, 2);

                        this_fill(:, 1) = linspace(this_loc_start(1), this_loc_end(1), this_i_end - this_i_start + 1);
                        this_fill(:, 2) = linspace(this_loc_start(2), this_loc_end(2), this_i_end - this_i_start + 1);

                        ds_data.data(this_i_start:this_i_end) = sqrt(sum(this_fill .^ 2, 2));
                        ds_data.lat(this_i_start:this_i_end) = this_fill(:, 1);
                        ds_data.lon(this_i_start:this_i_end) = this_fill(:, 2);
                        ds_data.filled_state(this_i_start:this_i_end) = 2;
                    end
                end
            end
            
            is_valid = ~isnan(ds_data.data);
            
            data.normalised.downsampled = struct();
            data.normalised.downsampled.time = ds_data.time(is_valid);
            data.normalised.downsampled.data = ds_data.data(is_valid);
            data.normalised.downsampled.lat = ds_data.lat(is_valid);
            data.normalised.downsampled.lon = ds_data.lon(is_valid);
            data.normalised.downsampled.filled_state = ds_data.filled_state(is_valid);
            
            data.normalised.downsampled.differentiated = calculate_location_differential(data.normalised.downsampled);
            
            fprintf('done');
            
            fprintf('\n  Completed: ');
            toc;
            
            save_data([file.name '-location' variant_out '-preprocessed'], data, varargin{:});
        else
            fprintf('\n  No data loaded\n');
        end
    end
end