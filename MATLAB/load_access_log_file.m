function data = load_access_log_file(file)
% Copyright (c) 2017, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 19-Feb-2017

    fid = fopen(file, 'r');
    
    if (fid == -1)
        errordlg(['Could not open file ' file], 'Error', 'modal');
        return;
    end

    % read first line of data to check format
    typestr = fgets(fid);
    
    fclose(fid);
    
    data = struct();
    data.unsorted = struct();
    
    if typestr == -1
        data.time = [];
        data.event = {};
        data.filename = {};
        data.file_type = {};
        data.file_date_str = {};
        data.file_date = [];
        data.status = {};
        data.unsorted.time = [];
        data.unsorted.event = {};
        data.unsorted.filename = {};
        data.unsorted.file_type = {};
        data.unsorted.file_date_str = {};
        data.unsorted.file_date = [];
        data.unsorted.status = {};
    else
        fid = fopen(file, 'r');

        fields = regexp(typestr, ',', 'split');

        if length(fields) == 2 || length(fields) == 4,
            tmp = textscan(fid, '%19s,%[^,\n]%[^\n]', 'delimiter', '');
        else
            errordlg(['Invalid reporting file ' file], 'Error', 'modal');
            tmp = cell(3,1);
        end
        fclose(fid);

        t = datenum(tmp{1}, 'yyyy-mm-dd HH:MM:SS');
        
        idx = strncmp(tmp{3}, ',', 1);
        
        tmp{3}(idx) = cellfun(@(x) x(2:end), tmp{3}(idx), 'UniformOutput', false);
        filename = tmp{3};
        idx = ~cellfun(@isempty, strfind(filename, ','));
        status = repmat({''}, size(tmp{3}));
        status(idx) = cellfun(@(x) x((find(ismember(x, ','), 1, 'first') + 1):end), filename(idx), 'UniformOutput', false);
        filename(idx) = cellfun(@(x) x(1:(find(ismember(x, ','), 1, 'first') - 1)), filename(idx), 'UniformOutput', false);
        
        date_start = regexp(filename, '\d\d\d\d-\d\d-\d\d');
        idx = ~cellfun(@isempty, date_start);
        
        file_type = repmat({''}, size(tmp{3}));
        file_date_str = repmat({''}, size(tmp{3}));
        file_date = NaN(size(tmp{3}));
        
        file_type(idx) = cellfun(@(x) x(1:(regexp(x, '\d\d\d\d-\d\d-\d\d') - 2)), filename(idx), 'UniformOutput', false);
        file_date_str(idx) = cellfun(@(x) x(regexp(x, '\d\d\d\d-\d\d-\d\d'):(regexp(x, '\d\d\d\d-\d\d-\d\d') + 9)), filename(idx), 'UniformOutput', false);
        file_date(idx) = datenum(file_date_str(idx), 'yyyy-mm-dd');
        
        data.unsorted.time = t;
        data.unsorted.event = tmp{2};
        data.unsorted.filename = filename;
        data.unsorted.file_type = file_type;
        data.unsorted.file_date_str = file_date_str;
        data.unsorted.file_date = file_date;
        data.unsorted.status = status;
        
        % Although this is a larage data set, it is almost sorted with only a
        % few exceptions so this doesn't add much computation.
        [t, t_sort_order] = sort(t);

        data.time = t;
        data.event = tmp{2}(t_sort_order);
        data.filename = filename(t_sort_order);
        data.file_type = file_type(t_sort_order);
        data.file_date_str = file_date_str(t_sort_order);
        data.file_date = file_date(t_sort_order);
        data.status = status(t_sort_order);
    end
end