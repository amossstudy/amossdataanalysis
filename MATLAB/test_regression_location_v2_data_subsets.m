function [predictions] = test_regression_location_v2_data_subsets(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 25-Nov-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/regression/data_subsets';

    output_variable = get_argument({'output_variable', 'outputvariable'}, 'qids', varargin{:});
    
    assert(sum(strcmpi(output_variable, {'qids', 'ipde'})) == 1, sprintf('Unknown output variable type: %s', output_variable));
    
    output_variable = lower(output_variable);
    
    OUTPUT_SUB_DIR = [OUTPUT_SUB_DIR '/' output_variable];
    
    params = get_configuration('features_location_v2', varargin{:});
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    FEATURES = size(data_raw, 1);
    
    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    
    label_opts = {};
    if latex_labels
        label_opts = {'Interpreter', 'latex'};
    end
    
    scatter_symbols = {'o', 'x', '+', '*', 's', 'd', '^', 'v', '<', '>'};
    
    regression_type_description = {...
        'none', 'feat-dep-fit-linear', '%s-dep-fit-linear', ...                               1, 2, 3,
        '%s-dep-fit-linear-bounded', ...                                                      4,
        '%s-dep-fit-linear-bounded-lar', '%s-dep-fit-linear-bounded-bisquare', ...          5, 6
        '%s-dep-fit-logit-varmax', ...                                                        7,
        '%s-dep-fit-logit-varmax-lar', '%s-dep-fit-logit-varmax-bisquare', ...              8, 9,
        '%s-dep-fit-polynomial-bounded', ...                                                  10,
        '%s-dep-fit-polynomial-bounded-lar', '%s-dep-fit-polynomial-bounded-bisquare', ...  11, 12,
        '%s-dep-fitglm-linear-logit' ...                                                      13,
        '%s-dep-fit-logit', ...                                                               14,
        '%s-dep-fitglm-polynomial-logit' ...                                                  15,
        '%s-dep-fitglm-linear' ...                                                            16,
        '%s-dep-fitglm-polynomial' ...                                                        17
        };
    
    for i = 1:numel(regression_type_description)
        regression_type_description{i} = sprintf(regression_type_description{i}, output_variable);
    end
    
    regression_type_legend_name = {'', '', 'Linear', 'Bounded Linear', ...
        'Bounded Linear (LAR)', 'Bounded Linear (Bisquare)', ...
        'Logistic Variable', 'Logistic Variable (LAR)', 'Logistic Variable (Bisquare)', ...
        'Bounded Quadratic', 'Bounded Quadratic (LAR)', 'Bounded Quadratic (Bisquare)', ...
        'Linear Logistic GLM', 'Logistic', 'Quadratic Logistic GLM', ...
        'Linear', 'Quadratic'};
    
    STD_ID = [3:12 14];
    GLM_ID = [13 15 16 17];
    
    TESTS = 5;
    
    if nargout >= 1
        predictions = cell(TESTS, 1);
    end
    
    % Specify the regression tests to run. Suitable options are
    %   Standard    : [1 16 4 13 15] (Linear, bounded linear, logistic &
    %                                 quadratic)
    %   Rubust demo : [1 4 5 6]      (Bounded Linear, Bounded Linear (LAR) &
    %                                 Bounded Linear (Bisquare))
    R_TEST = get_argument({'run_regressions', 'runregressions'}, 1:numel(regression_type_description), varargin{:});
    TESTS_TO_PERFORM = get_argument({'run_tests', 'runtests'}, 1:TESTS, varargin{:});
    RESULTS_TO_SHOW = {'nrmsd', 'nmae', 'rmsd', 'mae', 'rss', 'p_value'};

    remove_outliers = get_argument({'remove_outliers', 'removeoutliers'}, false, varargin{:});
    
    %features_to_show_display_idx = 1:FEATURES;
    %features_to_show_display_idx = get_argument({'features_to_show', 'featurestoshow', 'features_to_show_display_idx', 'featurestoshowdisplayidx'}, features_to_show_display_idx, varargin{:});
    
    features_to_show = params.feature_order(params.feature_display_order(1:FEATURES));
    %features_to_show = params.feature_order(11);
    
    if strcmp(output_variable, 'qids')
        valid_output = ~isnan(classification_properties.QIDS);
        participant_output = classification_properties.QIDS(valid_output);
    elseif strcmp(output_variable, 'ipde')
        valid_output = ~isnan([classification_properties.IPDE.dimensional]');
        participant_output = [classification_properties.IPDE(valid_output).dimensional]';
    end
    
    data_full = data_raw(features_to_show, valid_output)';
    
    participants = classification_properties.participant(valid_output);
    participant_week_start = classification_properties.week_start(valid_output);
    participant_class = classification_properties.class(valid_output);
    
    for t = TESTS_TO_PERFORM
        if t == 1
            test_classes = 0:2; %#ok<NASGU>
            
            test_participants_idx = true(size(participant_class));
        elseif t == 5
            test_classes = 0:1;
            
            test_participants_idx = ismember(participant_class, test_classes);
        else
            test_classes = t - 2;
            
            test_participants_idx = (participant_class == test_classes);
        end
        
        test_participants = participants(test_participants_idx);
        test_participant_output = participant_output(test_participants_idx);
        
        if nargout >= 1
            predictions{t} = struct();
            predictions{t}.participant = test_participants;
            predictions{t}.week_start = participant_week_start(test_participants_idx);
            predictions{t}.y = test_participant_output;
            predictions{t}.r_test = R_TEST;
            predictions{t}.features_to_show = features_to_show;
            predictions{t}.y_pred = cell(numel(R_TEST), 1);
        end
        
        this_params.test_description = params.test_description{t};
        this_params.test_description_filename = params.test_description_filename{t};

        test_data = data_full(test_participants_idx, :);

        if remove_outliers
            for i = 1:numel(features_to_show)
                non_nan = ~isnan(test_data(:, i));

                this_feature_data = test_data(non_nan, i);

                this_feature_data_mean = mean(this_feature_data);
                this_feature_data_std = std(this_feature_data);

                outliers = false(size(this_feature_data));

                outliers(this_feature_data > (this_feature_data_mean + (5 * this_feature_data_std))) = true;
                outliers(this_feature_data < (this_feature_data_mean - (5 * this_feature_data_std))) = true;

                if sum(outliers) > 0
                    outliers_idx = false(size(non_nan));
                    outliers_idx(non_nan) = outliers;

                    test_data(outliers_idx, i) = NaN;
                end
            end
        end

        result_figure = struct();

        combined_error_rate = NaN(numel(features_to_show), numel(R_TEST), numel(RESULTS_TO_SHOW));

        for r_i = 1:numel(R_TEST)
            if nargout >= 1
                predictions{t}.y_pred{r_i} = NaN(numel(test_participants), numel(features_to_show));
            end
            
            r = R_TEST(r_i);
            
            regression_model_coef_n = 0;

            if ismember(r, 2:3)
                regression_model_fun = @(a, b, x) a + (b * x);
                regression_model_fun_inv = @(a, b, x) max(0, min(27, (x - a) / b));
                regression_model_coef_n = 2;
            elseif ismember(r, 4:6)
                regression_model_fun = @(a, b, x) max(0, min(27, a + (b .* x)));
                regression_model_coef_n = 2;
            elseif ismember(r, 14)
                regression_model_fun = @(a, b, x) 27./(1 + exp(-(a + (b * x))));
                regression_model_coef_n = 2;
            elseif ismember(r, 7:9)
                regression_model_fun = @(a, b, c, x) min(c, 27)./(1 + exp(-(a + (b * x))));
                regression_model_coef_n = 3;
            elseif ismember(r, 10:12)
                regression_model_fun = @(a, b, c, x) max(0, min(27, a + (b .* x) + (c .* (x.^2))));
                regression_model_coef_n = 3;
            end

            if ismember(r, [2:6 14])
                regression_model_opts = {regression_model_fun, 'StartPoint', [0.5 0.5]};
            elseif ismember(r, 7:9)
                regression_model_opts = {regression_model_fun, 'StartPoint', [0.5 0.5 27]};
            elseif ismember(r, 10:12)
                regression_model_opts = {regression_model_fun, 'StartPoint', [0.5 0.5 0.5]};
            end

            if ismember(r, [5 8 11])
                regression_model_opts = [regression_model_opts, {'Robust', 'LAR'}]; %#ok<AGROW>
            elseif ismember(r, [6 9 12])
                regression_model_opts = [regression_model_opts, {'Robust', 'Bisquare'}]; %#ok<AGROW>
            end

            if r == 13
                glmopts = {'linear', 'Distribution', 'binomial', 'Link', 'logit'};
            elseif r == 15
                glmopts = {'purequadratic', 'Distribution', 'binomial', 'Link', 'logit'};
            elseif r == 16
                glmopts = {'linear'};
            elseif r == 17
                glmopts = {'purequadratic'};
            end

            output_pred = NaN(numel(features_to_show), numel(test_participants));
            output_pred_result = struct();

            for result_name = RESULTS_TO_SHOW
                output_pred_result.(result_name{:}) = NaN(numel(features_to_show), 1);
            end
            
            if r_i == 1
                str_xticklabel = params.feature_short(params.feature_order(params.feature_display_order(1:params.features)));

                for result_name = RESULTS_TO_SHOW
                    if strcmp(result_name, RESULTS_TO_SHOW{1})
                        legend_h = cell(5, 1);
                        legend_str = cell(5, 1);
                        
                        f2 = figure;

                        hold on;
                        for i = 1:5
                            legend_h{i} = plot(NaN, NaN, 's', 'Color', params.clr_data_subset(i, :), 'MarkerFaceColor', params.clr_data_subset(i, :), 'MarkerSize', 10);
                            legend_str{i} = params.data_subset_name{i};
                        end
                        hold off;
                        h_legend = legend([legend_h{:}], legend_str, 'FontSize', 20, label_opts{:});

                        set(h_legend, 'Units', 'pixels');
                        set(f2, 'Units', 'pixels');
                        for orientation = {'vertical', 'horizontal'}
                            set(h_legend, 'orientation', orientation{:});
                            drawnow();
                            p_legend = get(h_legend, 'Position');
                            p_legend(1:2) = [2 2];
                            set(h_legend, 'Position', p_legend);
                            p_figure = get(f2, 'Position');
                            p_figure(3:4) = p_legend(3:4) + [2 2];
                            set(f2, 'Position', p_figure);
                            set(f2, 'PaperPositionMode', 'auto')
                            save_figure(sprintf('regression_data_subsets_%s_legend_subset_type_%s', this_params.test_description_filename, orientation{:}), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);
                        end
                        
                        legend_h = cell(numel(R_TEST), 1);
                        legend_str = cell(numel(R_TEST), 1);
                        
                        f2 = figure;
                        
                        hold on;
                        for r_j = 1:numel(R_TEST)
                          
                            if R_TEST(r_j) == 1
                                legend_h{r_j} = plot(NaN, NaN, 'Color', lines(1), 'LineWidth', 2);
                                legend_str{r_j} = 'Baseline';
                            else
                                legend_h{r_j} = plot(NaN, NaN, scatter_symbols{mod(r_j - 1, numel(scatter_symbols))}, 'LineWidth', 2, 'MarkerEdgeColor', params.clr_data_subset(1, :), 'MarkerSize', 10);
                                legend_str{r_j} = regression_type_legend_name{R_TEST(r_j)};
                            end
                        end
                        hold off;
                        h_legend = legend([legend_h{:}], legend_str, 'FontSize', 20, label_opts{:});

                        set(h_legend, 'Units', 'pixels');
                        set(f2, 'Units', 'pixels');
                        for orientation = {'vertical', 'horizontal'}
                            set(h_legend, 'orientation', orientation{:});
                            drawnow();
                            p_legend = get(h_legend, 'Position');
                            p_legend(1:2) = [2 2];
                            set(h_legend, 'Position', p_legend);
                            p_figure = get(f2, 'Position');
                            p_figure(3:4) = p_legend(3:4) + [2 2];
                            set(f2, 'Position', p_figure);
                            set(f2, 'PaperPositionMode', 'auto')
                            save_figure(sprintf('regression_data_subsets_%s_legend_regression_type_%s', this_params.test_description_filename, orientation{:}), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);
                        end
                    end
                    
                    result_figure.(result_name{:}) = struct();
                    result_figure.(result_name{:}).handle = figure;
                    result_figure.(result_name{:}).ax_main = axes;
                    result_figure.(result_name{:}).filename = sprintf('regression_data_subsets_%s_%s', this_params.test_description_filename, strrep(result_name{:}, '_', '-'));
                    result_figure.(result_name{:}).v_div = cell(0);

                    title('Regression Error Rate (Individual)', 'FontSize', 30, label_opts{:});

                    set(gca, 'xlim', [1.5, (numel(params.feature_name) + 21.5)]);
                    set(gca, 'xtick', 5:7:(numel(params.feature_name) + 22), 'FontSize', 18);

                    if latex_ticks
                        set(gca, 'TickLabelInterpreter', 'latex')
                    end

                    p = get(gcf, 'PaperPosition');
                    p(3) = p(3) * 2;

                    if sum(strcmp(result_name, {'rmsd', 'nrmsd'})) > 0
                        if strcmp(output_variable, 'qids') && strcmp(result_name, 'rmsd')
                            set(gca, 'ylim', [4.6, 6.4]);
                        elseif strcmp(output_variable, 'qids') && strcmp(result_name, 'nrmsd')
                            set(gca, 'ylim', [0.18, 0.32]);
                        end
                        set(gca, 'xticklabel', ' ');
                        xlabel(' ', 'FontSize', 20, label_opts{:});
                        p(4) = p(4) * 0.5;
                    elseif sum(strcmp(result_name, {'mae', 'nmae', 'rss', 'p_value'})) > 0
                        if strcmp(output_variable, 'qids') && strcmp(result_name, 'mae')
                            set(gca, 'ylim', [3.6, 5.4]);
                        elseif strcmp(output_variable, 'qids') && strcmp(result_name, 'nmae')
                            set(gca, 'ylim', [0.13, 0.27]);
                        elseif strcmp(result_name, 'p_value')
                            set(gca, 'ylim', [0, 1.1]);
                        end
                        set(gca, 'xticklabel', str_xticklabel, 'FontSize', 18);
                        xlabel('Feature Presented to Model', 'FontSize', 20, label_opts{:});
                        p(4) = p(4) * 0.5;
                    end

                    set(gcf, 'PaperPosition', p);

                    ylabel(upper(strrep(result_name{:}, '_', '-')), 'FontSize', 20, label_opts{:});

                    save_figure(result_figure.(result_name{:}).filename, 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', []);

                    hold on;
                end
            end

            for i = 1:numel(features_to_show)
                non_nan = ~isnan(test_data(:, i));
                
                for p = unique(test_participants)'
                    test_idx = ismember(test_participants, p);

                    if sum(test_idx & non_nan) == 0
                        continue;
                    end

                    train_idx_full = ~test_idx & non_nan;
                    test_idx = test_idx & non_nan;

                    if r == 1
                        output_pred(i, test_idx) = mean(test_participant_output(train_idx_full));
                    elseif r == 2
                        model_fitted = fit(test_participant_output(train_idx_full), test_data(train_idx_full, i), regression_model_opts{:});
                        output_pred(i, test_idx) = regression_model_fun_inv(model_fitted.a, model_fitted.b, test_data(test_idx, i));
                    elseif ismember(r, STD_ID)
                        model_fitted = fit(test_data(train_idx_full, i), test_participant_output(train_idx_full), regression_model_opts{:});
                        if regression_model_coef_n == 2
                            output_pred(i, test_idx) = regression_model_fun(model_fitted.a, model_fitted.b, test_data(test_idx, i));
                        elseif regression_model_coef_n == 3
                            output_pred(i, test_idx) = regression_model_fun(model_fitted.a, model_fitted.b, model_fitted.c, test_data(test_idx, i));
                        end
                    elseif ismember(r, GLM_ID)
                        model_fitted = fitglm(test_data(train_idx_full, i), test_participant_output(train_idx_full) / 27, glmopts{:});
                        output_pred(i, test_idx) = predict(model_fitted, test_data(test_idx, i)) * 27;
                    end
                end

                if nargout >= 1
                    predictions{t}.y_pred{r_i}(:, i) = output_pred(i, :);
                end
                
                non_nan = ~isnan(output_pred(i, :));

                for result_i = 1:numel(RESULTS_TO_SHOW)
                    result_name = RESULTS_TO_SHOW{result_i};
                    
                    this_result = NaN;
                    
                    if sum(strcmp(result_name, {'rmsd', 'nrmsd'})) > 0
                        this_result = rms(output_pred(i, non_nan)' - test_participant_output(non_nan));
                    elseif sum(strcmp(result_name, {'mae', 'nmae'})) > 0
                        this_result = mean(abs(output_pred(i, non_nan)' - test_participant_output(non_nan)));
                    elseif strcmp(result_name, 'rss')
                        this_result = sum((output_pred(i, non_nan)' - test_participant_output(non_nan)) .^ 2);
                    elseif strcmp(result_name, 'p_value') && (r ~= 1) && isfield(output_pred_result, 'rss') && (sum(R_TEST == 1) > 0)
                        baseline_rss = combined_error_rate(i, R_TEST == 1, strcmp(RESULTS_TO_SHOW, 'rss'));
                        this_rss = output_pred_result.rss(i);
                        
                        baseline_df = 1;
                        if ismember(r, GLM_ID)
                            this_df = size(model_fitted.Coefficients, 1);
                        else
                            this_df = regression_model_coef_n;
                        end
                        n = sum(non_nan);
                        
                        this_f_stat = ((baseline_rss - this_rss) / (this_df - baseline_df)) / (this_rss / (n - this_df));
                        this_result = 1 - fcdf(this_f_stat, this_df - baseline_df, n - this_df);
                    end
                    
                    if sum(strcmp(result_name, {'nrmsd', 'nmae'})) > 0
                        this_result = this_result / max(test_participant_output);
                    end
                    
                    output_pred_result.(result_name)(i) = this_result;
                    
                    combined_error_rate(i, R_TEST == r, result_i) = this_result;
                end
            end

            for result_name = RESULTS_TO_SHOW
                this_figure = result_figure.(result_name{:});
                figure(this_figure.handle);

                hold on;
                
                if r == 1
                    data_x = NaN(numel(params.feature_name) + 22, 1);
                    
                    for i = 1:5
                        f_idx = (features_to_show > ((i - 1) * params.features)) & (features_to_show <= (i * params.features));
                        if sum(f_idx) > 0
                            scatter_feature_x = (find(f_idx) - (((i - 1) * 10) + 1)) * 7 + i + 1;

                            data_x(scatter_feature_x) = output_pred_result.(result_name{:})(f_idx);
                        end
                    end
                    stairs((1:(numel(params.feature_name) + 22)) + 0.5, data_x, 'Color', lines(1), 'LineWidth', 2);
                else
                    for i = 1:5
                        f_idx = (features_to_show > ((i - 1) * params.features)) & (features_to_show <= (i * params.features));
                        if sum(f_idx) > 0
                            scatter_feature_x = (find(f_idx) - (((i - 1) * 10) + 1)) * 7 + i + 2;

                            scatter(scatter_feature_x, output_pred_result.(result_name{:})(f_idx), 100, scatter_symbols{mod(r_i - 1, numel(scatter_symbols))}, 'LineWidth', 2, 'MarkerEdgeColor', params.clr_data_subset(i, :));
                        end
                    end
                end
                
                if isempty(result_figure.(result_name{:}).v_div)
                    result_figure.(result_name{:}).v_div = cell(9, 1);
                else
                    delete([result_figure.(result_name{:}).v_div{:}]);
                end
                
                y_lim = get(gca, 'ylim');
                
                for f = 1:9
                    result_figure.(result_name{:}).v_div{f} = plot([8.5 8.5] + (f - 1) * 7, y_lim, 'Color', [0.5 0.5 0.5]);
                end
                
                hold off;
                
                save_figure(this_figure.filename, 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:}, 'FontSize', [], 'figure_handle', this_figure.handle);
            end
        end
        
        regression_results_filename_base = sprintf('features_%s_data_subsets', output_variable');

        regression_results_filename = sprintf('%s_%s', regression_results_filename_base, this_params.test_description_filename);

        save_text(regression_results_filename, [{'output_sub_dir', OUTPUT_SUB_DIR}, varargin, {'append', false}], '%s\n', datestr(now));

        save_text_params = {regression_results_filename, [{'output_sub_dir', OUTPUT_SUB_DIR}, varargin, {'append', true}]};

        line_width = ((numel(RESULTS_TO_SHOW) + 1) * 10);

        for i = 1:numel(features_to_show)
            save_fprintf(save_text_params{:}, '%s\n', repmat('=', line_width, 1));
            save_fprintf(save_text_params{:}, '%s%s%s\n', repmat(' ', floor((line_width - numel(params.feature_name{features_to_show(i)})) / 2), 1), params.feature_name{features_to_show(i)}, repmat(' ', ceil((line_width - numel(params.feature_name{features_to_show(i)})) / 2), 1));
            save_fprintf(save_text_params{:}, '%s\n', repmat('-', line_width, 1));
            this_line = '  Test   ';
            output_format = '   %.2i    ';
            for result_name = RESULTS_TO_SHOW
                this_result_name = upper(strrep(result_name{:}, '_', '-'));
                while length(this_result_name) < 10
                    this_result_name = [this_result_name ' ']; %#ok<AGROW>
                end
                this_line = [this_line this_result_name]; %#ok<AGROW>
                output_format = [output_format '%.6f  ']; %#ok<AGROW>
            end
            save_fprintf(save_text_params{:}, [this_line ' \n']);
            save_fprintf(save_text_params{:}, [output_format ' \n'], [R_TEST' reshape(combined_error_rate(i, :, :), numel(R_TEST), numel(RESULTS_TO_SHOW))]')
            save_fprintf(save_text_params{:}, '%s\n', repmat('=', line_width, 1));
        end
        
        regression_results_filename = sprintf('%s_by_base_feature_%s', regression_results_filename_base, this_params.test_description_filename);

        save_text(regression_results_filename, [{'output_sub_dir', OUTPUT_SUB_DIR}, varargin, {'append', false}], '%s\n', datestr(now));

        save_text_params = {regression_results_filename, [{'output_sub_dir', OUTPUT_SUB_DIR}, varargin, {'append', true}]};

        line_width = ((numel(RESULTS_TO_SHOW) + 2) * 10);

        features_to_show_data_subset = floor((features_to_show - 1) / params.features);
        
        for r_i = 1:numel(R_TEST)
            save_fprintf(save_text_params{:}, '%s\n', repmat('=', line_width, 1));
            save_fprintf(save_text_params{:}, '%s%s%s\n', repmat(' ', floor((line_width - numel(regression_type_legend_name{R_TEST(r_i)})) / 2), 1), regression_type_legend_name{R_TEST(r_i)}, repmat(' ', ceil((line_width - numel(regression_type_legend_name{R_TEST(r_i)})) / 2), 1));
            save_fprintf(save_text_params{:}, '%s\n', repmat('=', line_width, 1));
            base_features = sort(unique(mod(features_to_show - 1, params.features) + 1));
            for i = base_features
                save_fprintf(save_text_params{:}, '%s%s%s\n', repmat(' ', floor((line_width - numel(params.feature_name{i})) / 2), 1), params.feature_name{i}, repmat(' ', ceil((line_width - numel(params.feature_name{i})) / 2), 1));
                save_fprintf(save_text_params{:}, '%s\n', repmat('-', line_width, 1));
                this_line = ' Data Subset       ';
                output_format = ' %-17s ';
                for result_name = RESULTS_TO_SHOW
                    this_result_name = upper(strrep(result_name{:}, '_', '-'));
                    while length(this_result_name) < 10
                        this_result_name = [this_result_name ' ']; %#ok<AGROW>
                    end
                    this_line = [this_line this_result_name]; %#ok<AGROW>
                    output_format = [output_format '%.6f  ']; %#ok<AGROW>
                end
                save_fprintf(save_text_params{:}, [this_line ' \n']);
                
                for j = find(mod(features_to_show - 1, params.features) == (i - 1))
                    save_fprintf(save_text_params{:}, [output_format ' \n'], params.data_subset_name{features_to_show_data_subset(j) + 1}, combined_error_rate(j, R_TEST == R_TEST(r_i), :))
                end
                if i ~= base_features(end)
                    save_fprintf(save_text_params{:}, '%s\n', repmat('-', line_width, 1));
                end
            end
            save_fprintf(save_text_params{:}, '%s\n', repmat('=', line_width, 1));
        end
    end
end
