function act = merge_proteus_acc(acc1, acc2, sort_data)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 4-Nov-2014

    if nargin < 3
        sort_data = 1;
    end
    
    act.time = [acc1.time; acc2.time];
    act.steps = [acc1.steps; acc2.steps];
    act.mean_x = [acc1.mean_x; acc2.mean_x];
    act.mean_y = [acc1.mean_y; acc2.mean_y];
    act.mean_z = [acc1.mean_z; acc2.mean_z];
    act.magnitude = [acc1.magnitude; acc2.magnitude];
    act.ref_angle = [acc1.ref_angle; acc2.ref_angle];
    
    if (sort_data)
        [act.time, t_sort_order] = sort(act.time);
        act.steps = act.steps(t_sort_order);
        act.mean_x = act.mean_x(t_sort_order);
        act.mean_y = act.mean_y(t_sort_order);
        act.mean_z = act.mean_z(t_sort_order);
        act.magnitude = act.magnitude(t_sort_order);
        act.ref_angle = act.ref_angle(t_sort_order);
    end
end
