function value = preprocess_data_get_property(file_type, participant, parameter, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 04-Dec-2014

    assert(ismember(file_type, {'location'}), 'Invalid file type.');
    
    datastruct = ['preprocess_data_' file_type];
    
    paths = get_paths(varargin{:});
    
    datapath_working = [paths.working_path datastruct '.mat'];
    
    mfilepath = fileparts(mfilename('fullpath'));
    
    datapath = [mfilepath '/' datastruct '.mat'];
    
    value = [];
    
    if (exist(datapath, 'file') == 2) || (exist(datapath_working, 'file') == 2)
        if (exist(datapath_working, 'file') == 2)
            load(datapath_working);
        else
            load(datapath);
        end
        eval(['preprocess_data = ' datastruct ';']);
        
        if nargin >= 2
            if isfield(preprocess_data, ['p' participant])
                if nargin < 3 || isempty(parameter)
                    value = fieldnames(preprocess_data.(['p' participant]));
                else
                    if isfield(preprocess_data.(['p' participant]), parameter)
                        value = preprocess_data.(['p' participant]).(parameter);
                    end
                end
            end
        else
            value = fieldnames(preprocess_data);
            for i = 1:numel(value)
                value{i} = value{i}(2:end);
            end
        end
    end
end
