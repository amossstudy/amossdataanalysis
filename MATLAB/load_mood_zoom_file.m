function data = load_mood_zoom_file(file)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    fid = fopen(file, 'r');
    
    if (fid == -1)
        errordlg(['Could not open file ' file], 'Error', 'modal');
        return;
    end

    % read first line of data to check format
    typestr = fgets(fid);
    
    fclose(fid);
    
    if typestr == -1
        data = struct();

        data.data = [];
        data.time = [];
    else
        fid = fopen(file, 'r');

        fields = regexp(typestr, ',', 'split');

        if rem(length(fields) - 1, 2) == 0
            % Read date and time separately from time zone hour and minute.
            regexstr = ['%23s%3f%2f' repmat(',%f', 1, (length(fields) - 1) / 2) repmat(',%[^,\n]', 1, (length(fields) - 1) / 2) '%[^\n]'];
            tmp = textscan(fid, regexstr, 'delimiter', '');
        end
        
        fclose(fid);

        if (rem(length(fields) - 1, 2) == 0) && ...
                (size(tmp{1}, 1) == size(tmp{length(tmp)}, 1)) && ...
                (sum(~strcmp(tmp{length(tmp)}, '')) == 0)
            
            data = load_mood_zoom_records(tmp(1:(length(tmp) - 1)));
        else
            % There are probably rows with different numbers of records.
            % Load file one row at a time.
            
            fid = fopen(file, 'r');
            
            data = struct();
            data.time = [];
            
            while ~feof(fid)
                typestr = fgets(fid);

                fields = regexp(typestr, ',', 'split');
                
                if rem(length(fields) - 1, 2) == 0
                    % Read date and time separately from time zone hour and minute.
                    regexstr = ['%23s%3f%2f' repmat(',%f', 1, (length(fields) - 1) / 2) repmat(',%[^,]', 1, (length(fields) - 1) / 2)];
                    tmp = textscan(strtrim(typestr), regexstr, 'delimiter', '');
                    
                    data_new = load_mood_zoom_records(tmp);
                    
                    if size(data_new.time, 1) > 0
                        data = merge_mood_zoom(data, data_new);
                    end
                end
            end
            
            fclose(fid);
        end
    end
end
