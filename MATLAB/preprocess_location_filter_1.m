function preprocess_location_filter_1(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 16-Jul-2015

    variant = get_argument('variant', '', varargin{:});
    combine_variants = get_argument({'combinevariants', 'combine_variants'}, {}, varargin{:});
    
    if isempty(variant) && ~isempty(combine_variants)
        variant = 'combined';

        for i = 1:length(combine_variants)
            this_variant_name = combine_variants{i};

            if isempty(this_variant_name)
                this_variant_name = 'std';
            end

            variant = [variant '_' this_variant_name]; %#ok<AGROW>
        end
    end

    variant_out = '';
    
    if ~isempty(variant)
        variant_out = ['_' variant];
    end

    for file = get_participants(varargin{:})'
        fprintf('Processing %s\n', file.name)

        data = load_location_preprocessed(file.name, varargin{:}, 'variant', variant);

        if ~isempty(data) && isfield(data, 'normalised') && ~isempty(data.normalised.time) && ...
                 isfield(data.normalised, 'differentiated')
            
            fprintf('  Filtering data: ');
            tic;

            data.normalised.filtered = struct();
            data.normalised.filtered.time = data.normalised.time;
            data.normalised.filtered.data = data.normalised.data;
            data.normalised.filtered.lat = data.normalised.lat;
            data.normalised.filtered.lon = data.normalised.lon;
            data.normalised.filtered.differentiated = data.normalised.differentiated;
            
            location_data = [data.normalised.filtered.lat data.normalised.filtered.lon]; % for normalised coordinates, this is in km from home
            
            invalid_diff = find(data.normalised.filtered.differentiated.data > 50);
            
            if ~isempty(invalid_diff)
                is_valid = true(size(data.normalised.filtered.time));

                this_diff_i = invalid_diff(1);

                while this_diff_i < length(is_valid)
                    if is_valid(this_diff_i - 1)
                        last_valid = this_diff_i - 1;
                    end
                    
                    this_diff_lat = data.normalised.filtered.lat(this_diff_i);
                    this_diff_lon = data.normalised.filtered.lon(this_diff_i);

                    j = this_diff_i + 1;

                    while ((j <= length(is_valid)) && ...
                            (data.normalised.filtered.lat(j) == this_diff_lat) && ...
                            (data.normalised.filtered.lon(j) == this_diff_lon))
                        j = j + 1;
                    end

                    if j <= length(is_valid)
                        is_valid(this_diff_i:(j - 1)) = false;
                        
                        dist_to_last_valid = sqrt(sum((location_data(j, :) - location_data(last_valid, :)).^2));
                        
                        dt_to_last_valid = sum(data.normalised.filtered.differentiated.dt((last_valid + 1):j));
                        data.normalised.filtered.differentiated.dt(j) = dt_to_last_valid;
                        
                        data.normalised.filtered.differentiated.data(j) = dist_to_last_valid ./ dt_to_last_valid;
                        
                        if data.normalised.filtered.differentiated.data(j) > 50
                            this_diff_i = j;
                        else
                            this_diff_i = invalid_diff(find(invalid_diff > j, 1, 'first'));
                        end
                    else
                        is_valid(this_diff_i:(j - 1)) = false;
                        break;
                    end
                end
                
                data.normalised.filtered.time = data.normalised.filtered.time(is_valid);
                data.normalised.filtered.data = data.normalised.filtered.data(is_valid);
                data.normalised.filtered.lat = data.normalised.filtered.lat(is_valid);
                data.normalised.filtered.lon = data.normalised.filtered.lon(is_valid);
                data.normalised.filtered.differentiated.data = data.normalised.filtered.differentiated.data(is_valid);
                data.normalised.filtered.differentiated.dt = data.normalised.filtered.differentiated.dt(is_valid);
            end

            toc;
            
            save_data([file.name '-location' variant_out '-preprocessed'], data, varargin{:});
        else
            fprintf('  No data loaded\n');
        end
    end
end