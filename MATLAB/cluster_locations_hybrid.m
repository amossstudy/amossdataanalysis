function [location_clusters, cluster_centers] = cluster_locations_hybrid(location_data, varargin)
% Copyright (c) 2018, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 21-August-2018

    show_figures = get_argument('show_figures', false, varargin{:});
    
    T = get_argument('t', 100, varargin{:});
    
    cluster_std = get_argument({'cluster_std', 'clusterstd'}, 0.150, varargin{:});
    cluster_sample_std = get_argument({'cluster_sample_std', 'clustersamplestd'}, 0.010, varargin{:});

    cluster_var = diag([1 1] * (cluster_std ^ 2));
    
    cluster_sample_var = diag([1 1] * (cluster_sample_std ^ 2));

    alpha = get_argument('alpha', 0.0001);

    location_data_remaining_idx = true(size(location_data, 1), 1);
    location_data_valid_idx = false(size(location_data, 1), 1);
    location_data_remaining = location_data;

    radius = 0.100;

    C = NaN(size(location_data));
    C_i = 0;
    C_idx = zeros(size(location_data, 1), 1);

    while true
        location_data_rounded = round(location_data_remaining .* 1) ./ 1;

        [~, ~, location_data_rounded_unique_idx] = unique(location_data_rounded, 'rows');

        i_max = mode(location_data_rounded_unique_idx);

        grid_location_data_rounded = round(location_data_remaining(location_data_rounded_unique_idx == i_max, :) .* 10) ./ 10;

        [location_data_rounded_unique, ~, location_data_rounded_unique_idx] = unique(grid_location_data_rounded, 'rows');

        this_c = location_data_rounded_unique(mode(location_data_rounded_unique_idx), :);

        for i = 1:10

            idx_valid = ~isnan(location_data_remaining(:, 1)) & ~isnan(location_data_remaining(:, 2)) & ...
                    location_data_remaining(:, 1) <= (this_c(1) + radius) & ...
                    location_data_remaining(:, 1) >= (this_c(1) - radius) & ...
                    location_data_remaining(:, 2) <= (this_c(2) + radius) & ...
                    location_data_remaining(:, 2) >= (this_c(2) - radius);

            idx_valid(idx_valid) = sqrt(sum((location_data_remaining(idx_valid, :) - repmat(this_c, sum(idx_valid), 1)) .^ 2, 2)) <= radius;

            this_c = mean(location_data_remaining(idx_valid, :), 1);
        end
        if sum(idx_valid) > 5
            location_data_valid_idx(location_data_remaining_idx) = idx_valid;
            C_i = C_i + 1;
            C(C_i, :) = this_c;
            this_idx = find(location_data_remaining_idx);
            C_idx(this_idx(idx_valid)) = C_i;
        end
        location_data_remaining_idx(location_data_remaining_idx) = ~idx_valid;
        location_data_remaining(idx_valid, :) = [];

        if numel(location_data_remaining) == 0
            break
        end
    end

    N = sum(location_data_valid_idx);

    burn_in = T * 0.2;

    C = C(1:C_i, :);
    size_C = size(C, 1);

    C_all = NaN(size_C, 2, T + 1);

    C_all(:, :, 1) = C;

    if show_figures
        figure;
    end

    for t = 1:T
        C_idx_counts = histcounts(C_idx, 1:(size_C + 1));

        if t == 1
            fprintf('   start: %i clusters\n', sum(C_idx_counts > 0));
        end

        fprintf('   t = %i: ', t);

        if show_figures
            cla
            hold on;
            C_valid = (C_idx_counts > 0);
            for j = find(C_valid)
                scatter(location_data(C_idx == j, 1), location_data(C_idx == j, 2));
            end
            scatter(C(C_valid, 1), C(C_valid, 2), 200, '+', 'markeredgecolor', 'k');
            hold off;
            axis([-1, 1, -1, 1])
            pause(1)
        end

        p_C = -inf(size(location_data, 1), size_C);
        for j = 1:size_C
            p_C(:, j) = log(mvnpdf(location_data, C(j, :), cluster_var));
        end

        i_rnd = rand(size(location_data, 1), 1);

        for i = find(location_data_valid_idx)'
            C_idx_counts(C_idx(i)) = C_idx_counts(C_idx(i)) - 1;

            p_idx = p_C(i, :);

            j = C_idx_counts > 0;
            p_idx(j) = p_idx(j) + log(C_idx_counts(j) / (N - 1 + alpha));
            p_idx(~j) = p_idx(~j) + log((alpha / sum(~j)) * (1 / (N - 1 + alpha)));

            p_idx = exp(p_idx - max(p_idx));
            p_idx = p_idx / sum(p_idx);

            new_idx = sum(i_rnd(i) > cumsum(p_idx)) + 1;

            C_idx(i) = new_idx;
            C_idx_counts(new_idx) = C_idx_counts(new_idx) + 1;
        end

        for i = unique(C_idx(C_idx > 0))'
            if C_idx_counts(i) == 1
                C(i, :) = location_data(C_idx == i, :);
            else
                c_mean = mean(location_data(C_idx == i, :), 1);
                %c_cov = cov(location_data(C_idx == i, :), 1);

                C(i, :) = mvnrnd(c_mean, cluster_sample_var, 1);
            end
        end

        if t > 5
            C_dist = inf(size_C, size_C);
            C_valid = (C_idx_counts > 0);
            for i = find(C_valid)
                C_dist(i, C_valid) = sqrt(sum((C(C_valid, :) - repmat(C(i,:), sum(C_valid), 1)) .^ 2, 2));
            end
            C_dist(logical(eye(size_C))) = inf;

            min_dist = min(min(C_dist));
            [min_dist_i, min_dist_j] = ind2sub(size(C_dist), find(C_dist == min_dist));

            r = randi(numel(min_dist_i));

            min_dist_i = min_dist_i(r);
            min_dist_j = min_dist_j(r);

            p_merge = normcdf(-min_dist, 0, sqrt(cluster_sample_var(1))) * 2;

            if rand <= p_merge
                if C_idx_counts(min_dist_j) > C_idx_counts(min_dist_i)
                    keep_i = min_dist_j;
                    merge_i = min_dist_i;
                else
                    keep_i = min_dist_i;
                    merge_i = min_dist_j;
                end

                C_idx(C_idx == merge_i) = keep_i;
                C_idx_counts(keep_i) = C_idx_counts(keep_i) + C_idx_counts(merge_i);
                C_idx_counts(merge_i) = 0;
            end
        end

        C_valid = (C_idx_counts > 0);
        C_all(C_valid, :, t + 1) = C(C_valid, :);

        fprintf('%i clusters\n', sum(C_idx_counts > 0));
    end

    C_valid = (C_idx_counts > 0);

    C_final = NaN(size(C));

    for i = find(C_valid)
        C_final(i, :) = nanmean(C_all(i, :, (burn_in + 1):T), 3);
    end

    C_final = C_final(C_valid, :);

    C_dist = inf(size(location_data, 1), size(C_final, 1));
    for j = 1:size(C_final, 1)
        C_dist(:, j) = sqrt(sum((location_data - repmat(C_final(j, :), size(location_data, 1), 1)) .^ 2, 2));
    end

    C_idx = zeros(size(location_data, 1), 1);

    for i = find(location_data_valid_idx)'
        [~, C_idx(i)] = min(C_dist(i, :));
    end

    if show_figures
        figure

        hold on;
        for j = 1:size(C_final, 1)
            scatter(location_data(C_idx == j, 1), location_data(C_idx == j, 2));
        end
        scatter(C_final(:, 1), C_final(:, 2), 200, '+', 'markeredgecolor', 'k');
        hold off;
        axis([-1, 1, -1, 1])
    end

    location_clusters = C_idx;
    cluster_centers = C_final;
end
