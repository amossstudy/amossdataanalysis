function visualise_features_location_v2_pca(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 04-Nov-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats';
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    data_full = data_raw';
    
    FEATURES = size(data_full, 2);
    
    valid_qids = ~isnan(classification_properties.QIDS);
    
    params = get_configuration('features_location_v2', varargin{:});
    
    participant_qids_score = classification_properties.QIDS(valid_qids);
    participant_qids = (participant_qids_score >= params.qids_threshold);
    participant_class = classification_properties.class(valid_qids);
    
    function prepare_and_save_figure(subdir, filename, varargin)
        save_figure(filename, 'output_sub_dir', [OUTPUT_SUB_DIR '/' subdir], 'FontSize', 50, varargin{:});
    end
    
    for t = 1:5
        if t == 1
            test_classes = 0:2;
            
            test_participants_idx = true(size(participant_class));
            
            test_valid_qids = valid_qids;
            test_participant_qids = participant_qids;
        elseif t == 5
            test_classes = 0:1;
            
            test_participants_idx = ismember(participant_class, test_classes);
            
            test_valid_qids = valid_qids & ismember(classification_properties.class, test_classes);
            test_participant_qids = participant_qids(test_participants_idx);
        else
            test_classes = t - 2;
            
            test_participants_idx = (participant_class == test_classes);
            
            test_valid_qids = valid_qids & (classification_properties.class == test_classes);
            test_participant_qids = participant_qids(test_participants_idx);
        end
        
        this_test_description = params.test_description{t};
        this_test_description_filename = params.test_description_filename{t};

        data_normalised = data_full(test_valid_qids, :);

        for i = 1:FEATURES
            data_normalised(:, i) = data_normalised(:, i) - nanmean(data_normalised(:, i));
            data_normalised(:, i) = data_normalised(:, i) / nanstd(data_normalised(:, i));
            if sum(isnan(data_normalised(:, i))) > 0
                % data is now zero-mean, unit variance so this is acceptable.
                data_normalised(isnan(data_normalised(:, i)), i) = 0;
            end
        end

        [~, pca_score] = pca(data_normalised);
        
        figure;
        ax1 = axes;
        h = cell(2, 1);
        h_valid = false(size(h));
        legend_str = {['QIDS < ' num2str(params.qids_threshold)], ['QIDS ' char(8805) ' ' num2str(params.qids_threshold)]};
        if sum(~test_participant_qids) > 0
            h{1} = scatter(pca_score(~test_participant_qids, 1), pca_score(~test_participant_qids, 2), 100, params.clr_qids(1, :), '+', 'LineWidth', 1);
            h_valid(1) = true;
            hold on;
        end
        if sum(test_participant_qids) > 0
            h{2} = scatter(pca_score(test_participant_qids, 1), pca_score(test_participant_qids, 2), 100, params.clr_qids(2, :), '+', 'LineWidth', 1);
            h_valid(2) = true;
        end
        hold off;
        axis([-7 7 -7 7]);
        legend([h{h_valid}], legend_str(h_valid), 'Location', 'North', 'Orientation', 'horizontal');
        xlabel('1st Principal Component');
        ylabel('2nd Principal Component');
        title(sprintf('Principal Component Analysis (%s)', this_test_description));
        
        prepare_and_save_figure('feature-reduction/pca', sprintf('features_qids_pca_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);
        
        if (sum(test_participant_qids) > 0) && (sum(test_participant_qids) < numel(test_participant_qids))
            B = mnrfit(pca_score(:, 1:2), test_participant_qids + 1);

            boundary_pc1 = -7:7;

            boundary_pc2_logit = (-B(1) - (B(2) * boundary_pc1) - log((1 / 0.5) - 1)) ./ B(3);

            ax2 = copyobj(gca, gcf);
            linkaxes([ax1, ax2], 'xy');
            axis('manual')
            delete(get(ax2,'Children'));
            h_lr = plot(boundary_pc1, boundary_pc2_logit, 'LineWidth', 1.5, 'Color', params.clr_qids_boundary, 'Parent', ax2);
            axis([-7 7 -7 7]);
            legend(h_lr, {'Logistic Regression (p = 0.5)'}, 'Location', 'South', 'Orientation', 'horizontal', 'Color', 'white');

            set(ax2, 'Color', 'none', 'Box', 'off')
            set(ax1, 'ActivePositionProperty', 'outerposition')
            set(ax2, 'ActivePositionProperty', 'outerposition')
            set(ax2, 'OuterPosition', get(ax1, 'outerposition'))

            % Set to non breaking space to allocate space (required when
            % saving.)
            xlabel(ax2, char(160))
            ylabel(ax2, char(160))
            title(ax2, char(160))
        end
        
        prepare_and_save_figure('feature-reduction/pca', sprintf('features_qids_pca_lr_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);
        
        if (sum(test_participant_qids) > 0) && (sum(test_participant_qids) < numel(test_participant_qids))
            delete(ax2);
            
            proportion = 1 - (sum(test_participant_qids) / numel(test_participant_qids));
            
            boundary_pc2_logit = (-B(1) - (B(2) * boundary_pc1) - log((1 / (proportion)) - 1)) ./ B(3);
            
            ax2 = copyobj(gca, gcf);
            linkaxes([ax1, ax2], 'xy');
            axis('manual')
            delete(get(ax2,'Children'));
            h_lr = plot(boundary_pc1, boundary_pc2_logit, 'LineWidth', 1.5, 'Color', params.clr_qids_boundary, 'Parent', ax2);
            axis([-7 7 -7 7]);
            legend(h_lr, {sprintf('Logistic Regression (p = %.2f)', proportion)}, 'Location', 'South', 'Orientation', 'horizontal', 'Color', 'white');
            
            set(ax2, 'Color', 'none', 'Box', 'off')
            set(ax1, 'ActivePositionProperty', 'outerposition')
            set(ax2, 'ActivePositionProperty', 'outerposition')
            set(ax2, 'OuterPosition', get(ax1, 'outerposition'))

            % Set to non breaking space to allocate space (required when
            % saving.)
            xlabel(ax2, char(160))
            ylabel(ax2, char(160))
            title(ax2, char(160))
        end
        
        prepare_and_save_figure('feature-reduction/pca', sprintf('features_qids_pca_lr_unbiased_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);
        
        if (sum(test_participant_qids) > 0) && (sum(test_participant_qids) < numel(test_participant_qids))
            delete(ax2);
            
            cls = fitcdiscr(pca_score(:, 1:2), test_participant_qids + 1, 'Prior', 'uniform');
            
            K = cls.Coeffs(1, 2).Const;
            L = cls.Coeffs(1, 2).Linear;
            
            fn = @(x1,x2) K + L(1)*x1 + L(2)*x2;
            
            ax2 = copyobj(gca, gcf);
            linkaxes([ax1, ax2], 'xy');
            axis('manual')
            delete(get(ax2,'Children'));
            h_lda = ezplot(ax2, fn,[-7 7 -7 7]);
            h_lda.Color = params.clr_qids_boundary;
            h_lda.LineWidth = 1.5;
            axis([-7 7 -7 7]);
            legend(h_lda, {'LDA (linear)'}, 'Location', 'South', 'Orientation', 'horizontal', 'Color', 'white');

            set(ax2, 'Color', 'none', 'Box', 'off')
            set(ax1, 'ActivePositionProperty', 'outerposition')
            set(ax2, 'ActivePositionProperty', 'outerposition')
            set(ax2, 'OuterPosition', get(ax1, 'outerposition'))

            % Set to non breaking space to allocate space (required when
            % saving.)
            xlabel(ax2, char(160))
            ylabel(ax2, char(160))
            title(ax2, char(160))
        end
        
        prepare_and_save_figure('feature-reduction/pca', sprintf('features_qids_pca_lda_linear_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);
        
        if (sum(test_participant_qids) > 0) && (sum(test_participant_qids) < numel(test_participant_qids))
            delete(ax2);
            
            cls = fitcdiscr(pca_score(:, 1:2), test_participant_qids + 1, 'DiscrimType', 'quadratic', 'Prior', 'uniform');
            
            K = cls.Coeffs(1, 2).Const;
            L = cls.Coeffs(1, 2).Linear;
            Q = cls.Coeffs(1, 2).Quadratic;
            
            fn = @(x1,x2) K + L(1)*x1 + L(2)*x2 + Q(1,1)*x1.^2 + ...
                (Q(1,2)+Q(2,1))*x1.*x2 + Q(2,2)*x2.^2;
            
            ax2 = copyobj(gca, gcf);
            linkaxes([ax1, ax2], 'xy');
            axis('manual')
            delete(get(ax2,'Children'));
            h_lda = ezplot(ax2, fn, [-7 7 -7 7]);
            h_lda.Color = params.clr_qids_boundary;
            h_lda.LineWidth = 1.5;
            axis([-7 7 -7 7]);
            legend(h_lda, {'LDA (quadratic)'}, 'Location', 'South', 'Orientation', 'horizontal', 'Color', 'white');

            set(ax2, 'Color', 'none', 'Box', 'off')
            set(ax1, 'ActivePositionProperty', 'outerposition')
            set(ax2, 'ActivePositionProperty', 'outerposition')
            set(ax2, 'OuterPosition', get(ax1, 'outerposition'))

            % Set to non breaking space to allocate space (required when
            % saving.)
            xlabel(ax2, char(160))
            ylabel(ax2, char(160))
            title(ax2, char(160))
        end
        
        prepare_and_save_figure('feature-reduction/pca', sprintf('features_qids_pca_lda_quadratic_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);
        
        if (sum(test_participant_qids) > 0) && (sum(test_participant_qids) < numel(test_participant_qids))
            delete(ax2);
            
            Mdl = fitcnb(pca_score(:, 1:2), test_participant_qids + 1, 'Prior', 'uniform');
            
            Params = cell2mat(Mdl.DistributionParameters);
            
            Mu = Params(2*(1:2)-1,1:2); % Extract the means
            Sigma = zeros(2,2,2);
            
            ax2 = copyobj(gca, gcf);
            linkaxes([ax1, ax2], 'xy');
            axis('manual')
            delete(get(ax2,'Children'));

            dxy = 0.1;
            x = (-7 + (dxy / 2)):dxy:(7 - (dxy / 2));
            
            for j = 1:2
                Sigma(:,:,j) = diag(Params(2*j,:) .^ 2); % Extract the covariance matrix
            end

            U1 = Mu(1, :)';
            U2 = Mu(2, :)';
            S1 = Sigma(:, :, 1);
            S2 = Sigma(:, :, 2);
            Prior1 = Mdl.Prior(1);
            Prior2 = Mdl.Prior(2);

            S1inv = inv(S1);
            S2inv = inv(S2);

            U1x = U1(1);
            U1y = U1(2);
            U2x = U2(1);
            U2y = U2(2);
            S1x = S1inv(1, 1);
            S1y = S1inv(2, 2);
            S2x = S2inv(1, 1);
            S2y = S2inv(2, 2);

            a = log((sqrt(det(S2)) * Prior1)/(sqrt(det(S1)) * Prior2));

            y_sqrt = sqrt(((S1x-S2x)*(x + ((2*S2x*U2x-2*S1x*U1x)/(2*(S1x-S2x)))).^2 - ...
                (((2*S2x*U2x-2*S1x*U1x)^2)/(4*(S1x-S2x))) - ...
                (((2*S2y*U2y-2*S1y*U1y)^2)/(4*(S1y-S2y))) + ...
                S1x*U1x^2 - S2x*U2x^2 + S1y*U1y^2 - S2y*U2y^2 - (2*a)) / (-(S1y-S2y)));

            y1 = -((2*S2y*U2y-2*S1y*U1y)/(2*(S1y-S2y))) + y_sqrt;
            y2 = -((2*S2y*U2y-2*S1y*U1y)/(2*(S1y-S2y))) - y_sqrt;

            x0_sqrt = sqrt(((2*a) - S1x*U1x^2 + S2x*U2x^2 - S1y*U1y^2 + S2y*U2y^2 + ...
                (((2*S2y*U2y-2*S1y*U1y)^2)/(4*(S1y-S2y))) + ...
                (((2*S2x*U2x-2*S1x*U1x)^2)/(4*(S1x-S2x)))) / (S1x-S2x));

            x0_min = (-(2*S2x*U2x-2*S1x*U1x)/(2*(S1x-S2x))) - x0_sqrt;
            x0_max = (-(2*S2x*U2x-2*S1x*U1x)/(2*(S1x-S2x))) + x0_sqrt;

            y0 = -((2*S2y*U2y-2*S1y*U1y)/(2*(S1y-S2y)));

            valid_y1 = (real(y1) == y1);
            valid_y2 = (real(y2) == y2);

            if ((S1x-S2x)/(S1y-S2y)) < 0
                if sum(~valid_y1) > 0 && sum(~valid_y2) > 0
                    x_plot = [];
                    y_plot = [];
                    if valid_y1(1) && valid_y2(1)
                        this_valid = 1:(find(~valid_y1, 1, 'first')-1);
                        x_plot = [x(this_valid)  x0_min fliplr(x(this_valid))];
                        y_plot = [y1(this_valid) y0     fliplr(y2(this_valid))];
                    end
                    if valid_y1(end) && valid_y2(end)
                        this_valid = (find(~valid_y1, 1, 'last')+1):numel(valid_y1);
                        x_plot = [x_plot NaN fliplr(x(this_valid))  x0_max x(this_valid)]; %#ok<AGROW>
                        y_plot = [y_plot NaN fliplr(y1(this_valid)) y0     y2(this_valid)]; %#ok<AGROW>
                    end
                else
                    x_plot = [x(valid_y1)  NaN x(valid_y2) ];
                    y_plot = [y1(valid_y1) NaN y2(valid_y2)];
                end
            else
                x_plot = [x0_min x(valid_y1)  x0_max fliplr(x(valid_y2))  x0_min];
                y_plot = [y0     y1(valid_y1) y0     fliplr(y2(valid_y2)) y0];
            end

            h_nb = plot(x_plot, y_plot, 'LineWidth', 1.5, 'Color', params.clr_qids_boundary, 'Parent', ax2);

            axis([-7 7 -7 7]);
            legend(h_nb, {'Naive Bayes'}, 'Location', 'South', 'Orientation', 'horizontal', 'Color', 'white');

            set(ax2, 'Color', 'none', 'Box', 'off')
            set(ax1, 'ActivePositionProperty', 'outerposition')
            set(ax2, 'ActivePositionProperty', 'outerposition')
            set(ax2, 'OuterPosition', get(ax1, 'outerposition'))

            % Set to non breaking space to allocate space (required when
            % saving.)
            xlabel(ax2, char(160))
            ylabel(ax2, char(160))
            title(ax2, char(160))
        end
        
        prepare_and_save_figure('feature-reduction/pca', sprintf('features_qids_pca_nb_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);
        
        if numel(test_classes) > 1
            figure;
            ax1 = axes;
            for c = test_classes
                scatter(pca_score(participant_class(test_participants_idx) == c, 1), pca_score(participant_class(test_participants_idx) == c, 2), 100, params.clr_cohort(c + 2, :), '+', 'LineWidth', 1);
                hold on;
            end
            hold off;
            axis([-7 7 -7 7]);
            legend(params.test_description(test_classes + 2), 'Location', 'North', 'Orientation', 'horizontal');
            xlabel('1st Principal Component');
            ylabel('2nd Principal Component');
            title(sprintf('Principal Component Analysis (%s)', this_test_description));

            prepare_and_save_figure('feature-reduction/pca', sprintf('features_pca_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);

            if numel(test_classes) == 2
                clr_boundary = params.clr_cohort(5, :);
                if (sum(participant_class(test_participants_idx) == test_classes(1)) > 0) && ...
                        (sum(participant_class(test_participants_idx) == test_classes(2)) )
                    B = mnrfit(pca_score(:, 1:2), (participant_class(test_participants_idx) == test_classes(1)) + 1);

                    boundary_pc1 = -7:7;

                    boundary_pc2_logit = (-B(1) - (B(2) * boundary_pc1) - log((1 / 0.5) - 1)) ./ B(3);

                    ax2 = copyobj(gca, gcf);
                    linkaxes([ax1, ax2], 'xy');
                    axis('manual')
                    delete(get(ax2,'Children'));
                    h_lr = plot(boundary_pc1, boundary_pc2_logit, 'LineWidth', 1.5, 'Color', clr_boundary, 'Parent', ax2);
                    axis([-7 7 -7 7]);
                    legend(h_lr, {'Logistic Regression (p = 0.5)'}, 'Location', 'South', 'Orientation', 'horizontal', 'Color', 'white');

                    set(ax2, 'Color', 'none', 'Box', 'off')
                    set(ax1, 'ActivePositionProperty', 'outerposition')
                    set(ax2, 'ActivePositionProperty', 'outerposition')
                    set(ax2, 'OuterPosition', get(ax1, 'outerposition'))

                    % Set to non breaking space to allocate space (required
                    % when saving.)
                    xlabel(ax2, char(160))
                    ylabel(ax2, char(160))
                    title(ax2, char(160))
                end
                
                prepare_and_save_figure('feature-reduction/pca', sprintf('features_pca_lr_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);

                if (sum(participant_class(test_participants_idx) == test_classes(1)) > 0) && ...
                        (sum(participant_class(test_participants_idx) == test_classes(2)) )
                    delete(ax2);

                    proportion = 1 - (sum(participant_class(test_participants_idx) == test_classes(1)) / numel(participant_class(test_participants_idx)));

                    boundary_pc2_logit = (-B(1) - (B(2) * boundary_pc1) - log((1 / (proportion)) - 1)) ./ B(3);

                    ax2 = copyobj(gca, gcf);
                    linkaxes([ax1, ax2], 'xy');
                    axis('manual')
                    delete(get(ax2,'Children'));
                    h_lr = plot(boundary_pc1, boundary_pc2_logit, 'LineWidth', 1.5, 'Color', clr_boundary, 'Parent', ax2);
                    axis([-7 7 -7 7]);
                    legend(h_lr, {sprintf('Logistic Regression (p = %.2f)', proportion)}, 'Location', 'South', 'Orientation', 'horizontal', 'Color', 'white');

                    set(ax2, 'Color', 'none', 'Box', 'off')
                    set(ax1, 'ActivePositionProperty', 'outerposition')
                    set(ax2, 'ActivePositionProperty', 'outerposition')
                    set(ax2, 'OuterPosition', get(ax1, 'outerposition'))

                    % Set to non breaking space to allocate space (required when
                    % saving.)
                    xlabel(ax2, char(160))
                    ylabel(ax2, char(160))
                    title(ax2, char(160))
                end

                prepare_and_save_figure('feature-reduction/pca', sprintf('features_pca_lr_unbiased_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);
        
                
                if (sum(participant_class(test_participants_idx) == test_classes(1)) > 0) && ...
                        (sum(participant_class(test_participants_idx) == test_classes(2)) )
                    delete(ax2);

                    cls = fitcdiscr(pca_score(:, 1:2), (participant_class(test_participants_idx) == test_classes(1)) + 1, 'Prior', 'uniform');

                    K = cls.Coeffs(1, 2).Const;
                    L = cls.Coeffs(1, 2).Linear;

                    fn = @(x1,x2) K + L(1)*x1 + L(2)*x2;

                    ax2 = copyobj(gca, gcf);
                    linkaxes([ax1, ax2], 'xy');
                    axis('manual')
                    delete(get(ax2,'Children'));
                    h_lda = ezplot(ax2, fn,[-7 7 -7 7]);
                    h_lda.Color = clr_boundary;
                    h_lda.LineWidth = 1.5;
                    axis([-7 7 -7 7]);
                    legend(h_lda, {'LDA (linear)'}, 'Location', 'South', 'Orientation', 'horizontal', 'Color', 'white');

                    set(ax2, 'Color', 'none', 'Box', 'off')
                    set(ax1, 'ActivePositionProperty', 'outerposition')
                    set(ax2, 'ActivePositionProperty', 'outerposition')
                    set(ax2, 'OuterPosition', get(ax1, 'outerposition'))

                    % Set to non breaking space to allocate space (required when
                    % saving.)
                    xlabel(ax2, char(160))
                    ylabel(ax2, char(160))
                    title(ax2, char(160))
                end

                prepare_and_save_figure('feature-reduction/pca', sprintf('features_pca_lda_linear_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);

                if (sum(participant_class(test_participants_idx) == test_classes(1)) > 0) && ...
                        (sum(participant_class(test_participants_idx) == test_classes(2)) )
                    delete(ax2);

                    cls = fitcdiscr(pca_score(:, 1:2), (participant_class(test_participants_idx) == test_classes(1)) + 1, 'DiscrimType', 'quadratic', 'Prior', 'uniform');

                    K = cls.Coeffs(1, 2).Const;
                    L = cls.Coeffs(1, 2).Linear;
                    Q = cls.Coeffs(1, 2).Quadratic;

                    fn = @(x1,x2) K + L(1)*x1 + L(2)*x2 + Q(1,1)*x1.^2 + ...
                        (Q(1,2)+Q(2,1))*x1.*x2 + Q(2,2)*x2.^2;

                    ax2 = copyobj(gca, gcf);
                    linkaxes([ax1, ax2], 'xy');
                    axis('manual')
                    delete(get(ax2,'Children'));
                    h_lda = ezplot(ax2, fn, [-7 7 -7 7]);
                    h_lda.Color = clr_boundary;
                    h_lda.LineWidth = 1.5;
                    axis([-7 7 -7 7]);
                    legend(h_lda, {'LDA (quadratic)'}, 'Location', 'South', 'Orientation', 'horizontal', 'Color', 'white');

                    set(ax2, 'Color', 'none', 'Box', 'off')
                    set(ax1, 'ActivePositionProperty', 'outerposition')
                    set(ax2, 'ActivePositionProperty', 'outerposition')
                    set(ax2, 'OuterPosition', get(ax1, 'outerposition'))

                    % Set to non breaking space to allocate space (required when
                    % saving.)
                    xlabel(ax2, char(160))
                    ylabel(ax2, char(160))
                    title(ax2, char(160))
                end

                prepare_and_save_figure('feature-reduction/pca', sprintf('features_pca_lda_quadratic_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);

                if (sum(participant_class(test_participants_idx) == test_classes(1)) > 0) && ...
                        (sum(participant_class(test_participants_idx) == test_classes(2)) )
                    delete(ax2);

                    Mdl = fitcnb(pca_score(:, 1:2), (participant_class(test_participants_idx) == test_classes(1)) + 1, 'Prior', 'uniform');

                    Params = cell2mat(Mdl.DistributionParameters);

                    Mu = Params(2*(1:2)-1,1:2); % Extract the means
                    Sigma = zeros(2,2,2);

                    ax2 = copyobj(gca, gcf);
                    linkaxes([ax1, ax2], 'xy');
                    axis('manual')
                    delete(get(ax2,'Children'));

                    dxy = 0.1;
                    x = (-7 + (dxy / 2)):dxy:(7 - (dxy / 2));

                    for j = 1:2
                        Sigma(:,:,j) = diag(Params(2*j,:) .^ 2); % Extract the covariance matrix
                    end

                    U1 = Mu(1, :)';
                    U2 = Mu(2, :)';
                    S1 = Sigma(:, :, 1);
                    S2 = Sigma(:, :, 2);
                    Prior1 = Mdl.Prior(1);
                    Prior2 = Mdl.Prior(2);

                    S1inv = inv(S1);
                    S2inv = inv(S2);

                    U1x = U1(1);
                    U1y = U1(2);
                    U2x = U2(1);
                    U2y = U2(2);
                    S1x = S1inv(1, 1);
                    S1y = S1inv(2, 2);
                    S2x = S2inv(1, 1);
                    S2y = S2inv(2, 2);

                    a = log((sqrt(det(S2)) * Prior1)/(sqrt(det(S1)) * Prior2));

                    y_sqrt = sqrt(((S1x-S2x)*(x + ((2*S2x*U2x-2*S1x*U1x)/(2*(S1x-S2x)))).^2 - ...
                        (((2*S2x*U2x-2*S1x*U1x)^2)/(4*(S1x-S2x))) - ...
                        (((2*S2y*U2y-2*S1y*U1y)^2)/(4*(S1y-S2y))) + ...
                        S1x*U1x^2 - S2x*U2x^2 + S1y*U1y^2 - S2y*U2y^2 - (2*a)) / (-(S1y-S2y)));

                    y1 = -((2*S2y*U2y-2*S1y*U1y)/(2*(S1y-S2y))) + y_sqrt;
                    y2 = -((2*S2y*U2y-2*S1y*U1y)/(2*(S1y-S2y))) - y_sqrt;

                    x0_sqrt = sqrt(((2*a) - S1x*U1x^2 + S2x*U2x^2 - S1y*U1y^2 + S2y*U2y^2 + ...
                        (((2*S2y*U2y-2*S1y*U1y)^2)/(4*(S1y-S2y))) + ...
                        (((2*S2x*U2x-2*S1x*U1x)^2)/(4*(S1x-S2x)))) / (S1x-S2x));

                    x0_min = (-(2*S2x*U2x-2*S1x*U1x)/(2*(S1x-S2x))) - x0_sqrt;
                    x0_max = (-(2*S2x*U2x-2*S1x*U1x)/(2*(S1x-S2x))) + x0_sqrt;

                    y0 = -((2*S2y*U2y-2*S1y*U1y)/(2*(S1y-S2y)));

                    valid_y1 = (real(y1) == y1);
                    valid_y2 = (real(y2) == y2);

                    if ((S1x-S2x)/(S1y-S2y)) < 0
                        if sum(~valid_y1) > 0 && sum(~valid_y2) > 0
                            x_plot = [];
                            y_plot = [];
                            if valid_y1(1) && valid_y2(1)
                                this_valid = 1:(find(~valid_y1, 1, 'first')-1);
                                x_plot = [x(this_valid)  x0_min fliplr(x(this_valid))];
                                y_plot = [y1(this_valid) y0     fliplr(y2(this_valid))];
                            end
                            if valid_y1(end) && valid_y2(end)
                                this_valid = (find(~valid_y1, 1, 'last')+1):numel(valid_y1);
                                x_plot = [x_plot NaN fliplr(x(this_valid))  x0_max x(this_valid)]; %#ok<AGROW>
                                y_plot = [y_plot NaN fliplr(y1(this_valid)) y0     y2(this_valid)]; %#ok<AGROW>
                            end
                        else
                            x_plot = [x(valid_y1)  NaN x(valid_y2) ];
                            y_plot = [y1(valid_y1) NaN y2(valid_y2)];
                        end
                    else
                        x_plot = [x0_min x(valid_y1)  x0_max fliplr(x(valid_y2))  x0_min];
                        y_plot = [y0     y1(valid_y1) y0     fliplr(y2(valid_y2)) y0];
                    end

                    h_nb = plot(x_plot, y_plot, 'LineWidth', 1.5, 'Color', clr_boundary, 'Parent', ax2);

                    axis([-7 7 -7 7]);
                    legend(h_nb, {'Naive Bayes'}, 'Location', 'South', 'Orientation', 'horizontal', 'Color', 'white');

                    set(ax2, 'Color', 'none', 'Box', 'off')
                    set(ax1, 'ActivePositionProperty', 'outerposition')
                    set(ax2, 'ActivePositionProperty', 'outerposition')
                    set(ax2, 'OuterPosition', get(ax1, 'outerposition'))

                    % Set to non breaking space to allocate space (required when
                    % saving.)
                    xlabel(ax2, char(160))
                    ylabel(ax2, char(160))
                    title(ax2, char(160))
                end

                prepare_and_save_figure('feature-reduction/pca', sprintf('features_pca_nb_%s', this_test_description_filename), varargin{:}, 'FontSize', 30);
            end
        end
    end
end
