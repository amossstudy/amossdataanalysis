function [predictions] = test_regression_location_v2(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Sept-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/regression';
    OUTPUT_SUB_DIR_MODELS = 'models';
    
    output_variable = get_argument({'output_variable', 'outputvariable'}, 'qids', varargin{:});
    
    assert(sum(strcmpi(output_variable, {'qids', 'ipde'})) == 1, sprintf('Unknown output variable type: %s', output_variable));
    
    output_variable = lower(output_variable);
    
    OUTPUT_SUB_DIR = [OUTPUT_SUB_DIR '/' output_variable];
    
    params = get_configuration('features_location_v2', varargin{:});
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    FEATURES = size(data_raw, 1);
    
    scatter_symbols = {'x', '+', '*'};
    
    regression_type_description = {...
        'none', 'feat-dep-fit-linear', '%s-dep-fit-linear', ...                               1 2 3
        '%s-dep-fit-linear-bounded', ...                                                      4
        '%s-dep-fit-linear-bounded-lar', '%s-dep-fit-linear-bounded-bisquare', ...            5 6
        '%s-dep-fit-logit-varmax', ...                                                        7
        '%s-dep-fit-logit-varmax-lar', '%s-dep-fit-logit-varmax-bisquare', ...                8 9
        '%s-dep-fit-polynomial-bounded', ...                                                  10
        '%s-dep-fit-polynomial-bounded-lar', '%s-dep-fit-polynomial-bounded-bisquare', ...    11 12
        '%s-dep-fitglm-linear-logit' ...                                                      13
        '%s-dep-fit-logit', ...                                                               14
        '%s-dep-fitglm-polynomial-logit' ...                                                  15
        '%s-dep-fitglm-linear' ...                                                            16
        '%s-dep-fitglm-polynomial' ...                                                        17
        '%s-dep-threshold' ...                                                                18
        '%s-dep-baseline' ...                                                                 19
        '%s-dep-classification-lr' ...                                                        20
        };
    
    for i = 1:numel(regression_type_description)
        regression_type_description{i} = sprintf(regression_type_description{i}, output_variable);
    end
    
    regression_type_description_combined = 'combined';
    
    regression_type_legend_name = {'', '', 'Linear', 'Bounded Linear', ...
        'Bounded Linear (LAR)', 'Bounded Linear (Bisquare)', ...
        'Logistic Variable', 'Logistic Variable (LAR)', 'Logistic Variable (Bisquare)', ...
        'Bounded Quadratic', 'Bounded Quadratic (LAR)', 'Bounded Quadratic (Bisquare)', ...
        'Linear Logistic GLM', 'Logistic', 'Quadratic Logistic GLM', ...
        'Linear', 'Quadratic', sprintf('QIDS = %i', params.qids_threshold), ...
        'Baseline', 'Logistic Regression Threshold'};
    
    STD_ID = [3:12 14];
    GLM_ID = [13 15 16 17];
    LASSO_ID = [13 16];
    
    TESTS = 6;
    
    if nargout >= 1
        predictions = cell(TESTS, 1);
    end
    
    % Specify the regression tests to run. Suitable options are
    %   Standard    : [1 16 4 13 15] (Linear, bounded linear, logistic &
    %                                 quadratic)
    %   Rubust demo : [1 4 5 6]      (Bounded Linear, Bounded Linear (LAR) &
    %                                 Bounded Linear (Bisquare))
    R_TEST = get_argument({'run_regressions', 'runregressions'}, 1:numel(regression_type_description), varargin{:});
    TESTS_TO_PERFORM = get_argument({'run_tests', 'runtests'}, 1:TESTS, varargin{:});
    RESULTS_TO_SHOW = {'nrmsd', 'nmae', 'rmsd', 'mae', 'rss', 'p_value'};
    
    R_TEST_CLR = [0 0 0; parula(numel(R_TEST))];
    R_TEST_CLR = get_argument({'regression_colour', 'regressioncolour', 'regression_color', 'regressioncolor'}, R_TEST_CLR, varargin{:});
    assert(size(R_TEST_CLR, 2) == 3, 'Regression colour matrix must have exactly three columns.');
    assert(size(R_TEST_CLR, 1) >= numel(R_TEST), 'Regression colour matrix must have at least the same number of rows as the number of regression tests being run.');
    
    custom_test_desc = get_argument({'regression_description', 'regressiondescription'}, {}, varargin{:});
    assert(numel(custom_test_desc) == 0 || numel(custom_test_desc) >= numel(R_TEST), 'Regression description matrix must have at least the same number of rows as the number of regression tests being run.');
    
    R_TEST_DESC = regression_type_legend_name(R_TEST);
    
    if numel(custom_test_desc) > 0
        for i = 1:numel(R_TEST)
            if ischar(custom_test_desc{i})
                R_TEST_DESC{i} = custom_test_desc{i};
            end
        end
    end
    
    PCA_OPTIONS = get_argument({'display_pca', 'displaypca'}, false, varargin{:});
    COMBINED_OUTPUT = get_argument({'display_combined', 'displaycombined'}, true, varargin{:});
    % The following options are only used if display_combined is true.
    CUMULATIVE_OUTPUT = get_argument({'display_cumulative', 'displaycumulative'}, false, varargin{:});
    % The following option is only used if display_combined and display_cumulative are true.
    % Cannot be used in cunjunction with display_cumulative_wrapper.
    CUMULATIVE_LASSO = get_argument({'display_cumulative_lasso', 'displaycumulativelasso'}, false, varargin{:});
    % The following option is only used if display_combined and display_cumulative are true.
    % Cannot be used in cunjunction with display_cumulative_wrapper.
    CUMULATIVE_LASSO_PER_PARTICIPANT = get_argument({'display_cumulative_lasso_per_participant', 'displaycumulativelassoperparticipant'}, false, varargin{:});
    % The following option is only used if display_combined and display_cumulative are true.
    % Cannot be used in cunjunction with display_cumulative_lasso.
    CUMULATIVE_WRAPPER = get_argument({'display_cumulative_wrapper', 'displaycumulativewrapper'}, false, varargin{:});
    
    assert(~((sum(CUMULATIVE_LASSO == true) > 0) && (sum(CUMULATIVE_WRAPPER == true) > 0)), 'Can only specify display_cumulative_lasso or display_cumulative_wrapper')
    assert(~((sum(CUMULATIVE_LASSO_PER_PARTICIPANT == true) > 0) && (sum(CUMULATIVE_WRAPPER == true) > 0)), 'Can only specify display_cumulative_lasso_per_participant or display_cumulative_wrapper')
    
    remove_outliers = get_argument({'remove_outliers', 'removeoutliers'}, false, varargin{:});
    
    cumulative_max_features = get_argument({'cumulative_max_features', 'cumulativemaxfeatures'}, Inf, varargin{:});
    
    save_figures = get_argument({'savefigure', 'save_figure', 'savefigures', 'save_figures'}, true, varargin{:});
    
    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    times_labels = get_argument({'timeslabels', 'times_labels'}, false, varargin{:});
    times_ticks = get_argument({'timesticks', 'times_ticks'}, times_labels, varargin{:});
    
    show_legend_only = get_argument({'showlegendonly', 'show_legend_only', 'savelegendonly', 'save_legend_only'}, true, varargin{:});
    show_with_legend = get_argument({'showwithlegend', 'show_with_legend', 'savewithlegend', 'save_with_legend'}, true, varargin{:});
    show_without_legend = get_argument({'showwithoutlegend', 'show_without_legend', 'savewithoutlegend', 'save_without_legend'}, true, varargin{:});
    show_partial_combined = get_argument({'showpartialcombined', 'show_partial_combined', 'savepartialcombined', 'save_partial_combined'}, true, varargin{:});
    run_model_cv = get_argument({'runmodelcv', 'run_model_cv'}, true, varargin{:});
    
    show_x_label = get_argument({'showxlabel', 'show_xlabel', 'show_x_label'}, false, varargin{:});
    xlim_value = get_argument({'xlim', 'x_lim'}, [], varargin{:});
    
    label_opts = {};
    if latex_labels
        label_opts = {'Interpreter', 'latex'};
    elseif times_labels
        label_opts = {'FontName', 'Times New Roman', 'Interpreter', 'none'};
    end
    
    tick_label_opts = {};
    if latex_ticks
        tick_label_opts = {'TickLabelInterpreter', 'latex'};
    elseif times_ticks
        tick_label_opts = {'FontName', 'Times New Roman'};
    end
    
    if show_legend_only
        RESULTS_TO_SHOW = [RESULTS_TO_SHOW, {'legend'}];
    end
    
    features_to_show_display_idx = 1:FEATURES;
    features_to_show_display_idx = get_argument({'features_to_show', 'featurestoshow', 'features_to_show_display_idx', 'featurestoshowdisplayidx'}, features_to_show_display_idx, varargin{:});
    
    features_to_show = params.feature_order(params.feature_display_order(features_to_show_display_idx));
    
    highlight_participants = get_argument({'highlight_participants', 'highlightparticipants'}, [], varargin{:});
    highlight_marker_size = get_argument({'highlight_marker_size', 'highlightmarkersize', 'highlight_markersize'}, 70, varargin{:});
    
    function [feature_rank, B_lasso] = get_feature_rank(data_full, classification)
        feature_rank = NaN(numel(features_to_show), 1);
        
        B_lasso = lasso(data_full, classification);
        
        features_count = sum(B_lasso ~= 0, 1);
        
        for f = 1:numel(features_to_show)
            features_used = find(B_lasso(:, find(features_count == f, 1, 'last')) ~= 0);
            new_features_used = features_used(~ismember(features_used, feature_rank));
            
            if ~isempty(new_features_used)
                feature_rank(find(isnan(feature_rank), 1, 'first') + (0:(length(new_features_used) - 1))) = new_features_used;
            end
        end
        
        missing_features = find(~ismember(1:numel(features_to_show), feature_rank));
        
        if numel(missing_features) > 0
            feature_rank((numel(features_to_show) - numel(missing_features) + 1):numel(features_to_show)) = missing_features;
        end
    end
    
    function prepare_and_save_figure(subdir, filename, varargin)
        save_figure(filename, 'output_sub_dir', [OUTPUT_SUB_DIR '/' subdir], varargin{:});
    end
  
    if strcmp(output_variable, 'qids')
        valid_output = ~isnan(classification_properties.QIDS);
        participant_output = classification_properties.QIDS(valid_output);
        ylabel_str = 'QIDS Score';
        ylim_value = [0 28];
    elseif strcmp(output_variable, 'ipde')
        valid_output = ~isnan([classification_properties.IPDE.dimensional]');
        participant_output = [classification_properties.IPDE(valid_output).dimensional]';
        ylabel_str = 'IPDE Dimensional Score';
        ylim_value = [0 25];
    end
    
    data_full = data_raw(features_to_show, valid_output)';
    
    participants = classification_properties.participant(valid_output);
    participant_week_start = classification_properties.week_start(valid_output);
    participant_class = classification_properties.class(valid_output);
    
    pred_mrmsd = cell(TESTS, 1);
    
    for t = TESTS_TO_PERFORM
        if t == 1
            train_classes = 0:2;
        elseif t == 5
            train_classes = 0:1;
        elseif t == 6
            train_classes = 1;
        else
            train_classes = t - 2;
        end
        
        if t == 6
            test_classes = 0:1;
        else
            test_classes = train_classes;
        end
        
        train_participants_idx = ismember(participant_class, train_classes);
        test_participants_idx = ismember(participant_class, test_classes);
        
        test_participants = participants(test_participants_idx);
        test_participants_unique = unique(test_participants);
        test_participant_class = participant_class(test_participants_idx);
        test_participant_output = participant_output(test_participants_idx);
        
        if nargout >= 1
            predictions{t} = struct();
            predictions{t}.participant = test_participants;
            predictions{t}.week_start = participant_week_start(test_participants_idx);
            predictions{t}.y = test_participant_output;
            predictions{t}.r_test = R_TEST;
            predictions{t}.features_to_show = features_to_show;
            predictions{t}.y_pred_fs = cell(numel(R_TEST), 1);
            predictions{t}.y_pred = cell(numel(R_TEST), 1);
            predictions{t}.y_pred_features = cell(numel(R_TEST), 1);
        end
        
        test_train_idx = ismember(test_participant_class, train_classes);
        
        this_params.test_description = params.test_description{t};
        this_params.test_description_filename = params.test_description_filename{t};

        for do_pca = PCA_OPTIONS
            train_data = data_full(train_participants_idx, :);
            test_data = data_full(test_participants_idx, :);
            
            if do_pca
                train_data_normalised = train_data;
                test_data_normalised = test_data;

                for i = 1:numel(features_to_show)
                    train_data_normalised(:, i) = train_data_normalised(:, i) - nanmean(train_data(:, i));
                    train_data_normalised(:, i) = train_data_normalised(:, i) / nanstd(train_data(:, i));
                    
                    test_data_normalised(:, i) = test_data_normalised(:, i) - nanmean(train_data(:, i));
                    test_data_normalised(:, i) = test_data_normalised(:, i) / nanstd(train_data(:, i));
                end

                pca_coeff = pca(train_data_normalised);
                
                test_data = test_data_normalised * pca_coeff;
            elseif remove_outliers
                for i = 1:numel(features_to_show)
                    train_non_nan = ~isnan(train_data(:, i));
                    non_nan = ~isnan(test_data(:, i));

                    train_feature_data = train_data(train_non_nan, i);
                    plot_feature_data = test_data(non_nan, i);

                    train_feature_data_mean = mean(train_feature_data);
                    train_feature_data_std = std(train_feature_data);

                    outliers = false(size(plot_feature_data));

                    outliers(plot_feature_data > (train_feature_data_mean + (5 * train_feature_data_std))) = true;
                    outliers(plot_feature_data < (train_feature_data_mean - (5 * train_feature_data_std))) = true;
                    
                    if sum(outliers) > 0
                        outliers_idx = false(size(non_nan));
                        outliers_idx(non_nan) = outliers;

                        test_data(outliers_idx, i) = NaN;
                    end
                end
            end
            
            if COMBINED_OUTPUT && sum(CUMULATIVE_OUTPUT == true) > 0
                feature_rank = get_feature_rank(test_data(test_train_idx, :), test_participant_output(test_train_idx));
            end
            
            pred_mrmsd{t} = NaN(numel(features_to_show), ((numel(regression_type_description) - 1) * ((2 * numel(test_classes)) + 1)));

            rho = NaN(numel(features_to_show), 1);
            
            result_figure = struct();

            r_i = 1;

            combined_figure_handle = cell(numel(features_to_show), 1);
            combined_figure_legend = cell(numel(features_to_show), numel(R_TEST));
            
            combined_error_rate = NaN(numel(features_to_show), numel(R_TEST), numel(RESULTS_TO_SHOW));
            
            for r_j = 1:numel(R_TEST)
                r = R_TEST(r_j);
                
                if ~COMBINED_OUTPUT
                    this_regression_type_name = regression_type_description{r};
                else
                    this_regression_type_name = regression_type_description_combined;
                end

                regression_model_coef_n = 0;

                if ismember(r, 2:3)
                    regression_model_fun = @(a, b, x) a + (b * x);
                    regression_model_fun_inv = @(a, b, x) max(0, min(27, (x - a) / b));
                    regression_model_coef_n = 2;
                elseif ismember(r, 4:6)
                    regression_model_fun = @(a, b, x) max(0, min(27, a + (b .* x)));
                    regression_model_coef_n = 2;
                elseif ismember(r, 14)
                    regression_model_fun = @(a, b, x) 27./(1 + exp(-(a + (b * x))));
                    regression_model_coef_n = 2;
                elseif ismember(r, 7:9)
                    regression_model_fun = @(a, b, c, x) min(c, 27)./(1 + exp(-(a + (b * x))));
                    regression_model_coef_n = 3;
                elseif ismember(r, 10:12)
                    regression_model_fun = @(a, b, c, x) max(0, min(27, a + (b .* x) + (c .* (x.^2))));
                    regression_model_coef_n = 3;
                end
                
                if ismember(r, [2:6 14])
                    regression_model_opts = {regression_model_fun, 'StartPoint', [0.5 0.5]};
                elseif ismember(r, 7:9)
                    regression_model_opts = {regression_model_fun, 'StartPoint', [0.5 0.5 27]};
                elseif ismember(r, 10:12)
                    regression_model_opts = {regression_model_fun, 'StartPoint', [1 1 1]};
                end
                
                if ismember(r, [5 8 11])
                    regression_model_opts = [regression_model_opts, {'Robust', 'LAR'}]; %#ok<AGROW>
                elseif ismember(r, [6 9 12])
                    regression_model_opts = [regression_model_opts, {'Robust', 'Bisquare'}]; %#ok<AGROW>
                end
                
                if r == 13
                    glmopts_dist = 'binomial';
                    glmopts_link = 'logit';
                    glmopts = {'linear', 'Distribution', glmopts_dist, 'Link', glmopts_link};
                elseif r == 15
                    glmopts = {'purequadratic', 'Distribution', 'binomial', 'Link', 'logit'};
                elseif r == 16
                    glmopts_dist = 'normal';
                    glmopts_link = 'identity';
                    glmopts = {'linear'};
                elseif r == 17
                    glmopts = {'purequadratic'};
                end

                show_classes = (numel(test_classes) > 1) && ~COMBINED_OUTPUT;

                if r == 1
                    if show_classes || COMBINED_OUTPUT
                        h_scatter = cell(numel(features_to_show), numel(test_classes));
                        h_scatter_text = cell(numel(features_to_show), numel(test_classes));
                    else
                        h_scatter = cell(numel(features_to_show), 1);
                        h_scatter_text = cell(numel(features_to_show), 1);
                    end
                end
                
                if ~COMBINED_OUTPUT || (sum(CUMULATIVE_OUTPUT == false) > 0)
                    features_to_show_individually = 1:numel(features_to_show);
                else
                    features_to_show_individually = [];
                end
                
                for i = features_to_show_individually
                    if do_pca
                        this_feature_name = sprintf('Principal Component %i', i);
                        this_feature_name_file = sprintf('PC%.2i', i);
                    else
                        this_feature_name = params.feature_name{features_to_show(i)};
                        this_feature_name_file = sprintf('%.2i_%s', features_to_show(i), strrep(lower(this_feature_name), ' ', '_'));
                    end
                    
                    if COMBINED_OUTPUT
                        this_feature_suffix = '_combined';
                    elseif show_classes
                        this_feature_suffix = '_classes';
                    else
                        this_feature_suffix = '';
                    end
                    
                    non_nan = ~isnan(test_data(:, i));
                    
                    if isnan(rho(i))
                        rho(i) = corr(test_data(non_nan, i), test_participant_output(non_nan));
                    end
                    
                    if r > 1
                        regression_model = cell(numel(test_classes) + 1, 1);
                        regression_model_coef = NaN(numel(test_classes) + 1, regression_model_coef_n);
                        regression_model_rmse = NaN(numel(test_classes) + 1, 1);
                        for ci = 0:numel(test_classes)
                            if ci == 0
                                class_idx = test_train_idx;
                            else
                                class_idx = (test_participant_class == test_classes(ci));
                            end
                            if r == 2
                                model_fitted = fit(test_participant_output(class_idx & non_nan), test_data(class_idx & non_nan, i), regression_model_opts{:});
                            elseif ismember(r, STD_ID)
                                model_fitted = fit(test_data(class_idx & non_nan, i), test_participant_output(class_idx & non_nan), regression_model_opts{:});
                            elseif ismember(r, GLM_ID)
                                model_fitted = fitglm(test_data(class_idx & non_nan, i), test_participant_output(class_idx & non_nan) / 27, glmopts{:});
                            elseif r == 18
                                model_fitted = params.qids_threshold;
                            elseif r == 19
                                model_fitted = mean(test_participant_output(class_idx & non_nan));
                            elseif r == 20
                                feature_data = test_data(class_idx & non_nan, i);
                                output_data = test_participant_output(class_idx & non_nan);
                                output_class = (output_data >= params.qids_threshold) + 1;
                                
                                n_class = histcounts(output_class, 0.5:2.5);
                                
                                [~, idx_classes_sorted] = sort(n_class);
                                
                                feature_data_all = nan(n_class(idx_classes_sorted(2)) * 2, 1);
                                output_class_all = nan(n_class(idx_classes_sorted(2)) * 2, 1);
                                
                                feature_data_all(1:sum(n_class)) = feature_data;
                                output_class_all(1:sum(n_class)) = output_class;
                                
                                idx_class_min = find(output_class == idx_classes_sorted(1));
                                idx_class_slected = repmat(1:n_class(idx_classes_sorted(1)), 1, floor(n_class(idx_classes_sorted(2)) / n_class(idx_classes_sorted(1))));
                                %idx_class_slected = [idx_class_slected randperm(n_class(idx_classes_sorted(1)))]; %#ok<AGROW>
                                idx_class_slected = idx_class_slected(1:abs(diff(n_class)));
                                
                                feature_data_all((sum(n_class) + 1):(n_class(idx_classes_sorted(2)) * 2)) = feature_data(idx_class_min(idx_class_slected));
                                output_class_all((sum(n_class) + 1):(n_class(idx_classes_sorted(2)) * 2)) = output_class(idx_class_min(idx_class_slected));
                                
                                B = mnrfit(feature_data_all, output_class_all);
                                model_fitted = -B(1) / B(2);
                            end
                            regression_model{ci + 1} = model_fitted;
                            if regression_model_coef_n >= 1
                                regression_model_coef(ci + 1, 1) = model_fitted.a;
                            end
                            if regression_model_coef_n >= 2
                                regression_model_coef(ci + 1, 2) = model_fitted.b;
                            end
                            if regression_model_coef_n >= 3
                                regression_model_coef(ci + 1, 3) = model_fitted.c;
                            end
                            if r == 2
                                regression_model_rmse(ci + 1) = rms(regression_model_fun(model_fitted.a, model_fitted.b, test_participant_output(class_idx & non_nan)) - test_data(class_idx & non_nan, i));
                            elseif regression_model_coef_n == 2
                                regression_model_rmse(ci + 1) = rms(regression_model_fun(model_fitted.a, model_fitted.b, test_data(class_idx & non_nan, i)) - test_participant_output(class_idx & non_nan));
                            elseif regression_model_coef_n == 3
                                regression_model_rmse(ci + 1) = rms(regression_model_fun(model_fitted.a, model_fitted.b, model_fitted.c, test_data(class_idx & non_nan, i)) - test_participant_output(class_idx & non_nan));
                            elseif ismember(r, GLM_ID)
                                regression_model_rmse(ci + 1) = rms(predict(model_fitted, test_data(class_idx & non_nan, i)) * 27 - test_participant_output(class_idx & non_nan));
                            end
                        end
                    end
                    
                    if ~COMBINED_OUTPUT || r == 1
                        f1 = figure;
                        
                        combined_figure_handle{i} = f1;

                        h_title = axes;
                        set(h_title, 'color', 'none');
                        set(h_title, 'xtick', []);
                        set(h_title, 'ytick', []);
                        set(h_title, 'xcolor', 'none');
                        set(h_title, 'ycolor', 'none');
                        
                        title(h_title, sprintf('%s (%s)', this_feature_name, this_params.test_description), label_opts{:});

                        axes;
                        
                        if show_classes || COMBINED_OUTPUT
                            for ci = 1:numel(test_classes)
                                class_idx = (test_participant_class == test_classes(ci));

                                if isempty(highlight_participants)
                                    this_clr = params.clr_cohort(test_classes(ci) + 2, :);
                                else
                                    this_clr = ones(1, 3) * 0.75;
                                end
                                
                                h_scatter{i, ci} = scatter(test_data(class_idx, i), test_participant_output(class_idx), 70, this_clr, 'filled', params.symbol_cohort{test_classes(ci) + 2});
                                h_scatter_text{i, ci} = params.test_description{test_classes(ci) + 2};
                                
                                if ci == 1
                                    hold on;
                                end
                            end
                            for ci = 1:numel(test_classes)
                                class_idx = (test_participant_class == test_classes(ci));
                                if ~isempty(highlight_participants)
                                    for p_i = 1:numel(highlight_participants)
                                        p = highlight_participants{p_i};
                                        p_idx = strcmp(test_participants, p);
                                        if sum(class_idx & p_idx) > 0
                                            scatter(test_data(class_idx & p_idx, i), test_participant_output(class_idx & p_idx), highlight_marker_size, 'filled', params.symbol_cohort{test_classes(ci) + 2});
                                        end
                                    end
                                end
                            end
                        else
                            if isempty(highlight_participants)
                                this_clr = params.clr_cohort(t, :);
                            else
                                this_clr = ones(1, 3) * 0.75;
                            end
                            
                            h_scatter{i} = scatter(test_data(:, i), test_participant_output, 70, this_clr, 'filled');
                            h_scatter_text{i} = this_params.test_description;
                        end
                        
                        axis(axis);
                        ylabel(ylabel_str, label_opts{:});
                        
                        if show_x_label
                            xlabel(this_feature_name, label_opts{:});
                        end
                        
                        pos = get(gca, 'OuterPosition');

                        title(gca, char(160));
                        set(gca, 'ylim', ylim_value);
                        
                        if ~COMBINED_OUTPUT
                            set(gca, 'OuterPosition', pos .* [1 1 1 0.89]);
                        end
                    else
                        f1 = figure(combined_figure_handle{i});
                    end
                    
                    if numel(xlim_value) == 2
                        set(gca, 'xlim', xlim_value);
                    end
                    
                    limits = axis;

                    trend_handles = cell(numel(test_classes) + 3, 1);
                    pred_y = cell(numel(test_classes) + 1, 2);
                    meas_y = cell(numel(test_classes) + 1, 1);

                    regression_x = linspace(limits(1), limits(2), 100);

                    nrmsd_ind_text = '';
                    nrmsd_comb_text = '';

                    if r > 1
                        hold on;
                        if show_classes
                            for ci = 1:numel(test_classes)
                                class_idx = (test_participant_class == test_classes(ci));
                                if r == 2
                                    regression_y = 0:27;
                                    regression_x = regression_model_fun(regression_model_coef(ci + 1, 1), regression_model_coef(ci + 1, 2), regression_y);
                                    pred_y{ci, 1} = regression_model_fun_inv(regression_model_coef(ci + 1, 1), regression_model_coef(ci + 1, 2), test_data(class_idx & non_nan, i));
                                    pred_y{ci, 2} = regression_model_fun_inv(regression_model_coef(ci + 1, 1), regression_model_coef(ci + 1, 2), test_data(non_nan, i));
                                elseif regression_model_coef_n == 2
                                    regression_y = regression_model_fun(regression_model_coef(ci + 1, 1), regression_model_coef(ci + 1, 2), regression_x);
                                    pred_y{ci, 1} = regression_model_fun(regression_model_coef(ci + 1, 1), regression_model_coef(ci + 1, 2), test_data(class_idx & non_nan, i));
                                    pred_y{ci, 2} = regression_model_fun(regression_model_coef(ci + 1, 1), regression_model_coef(ci + 1, 2), test_data(non_nan, i));
                                elseif regression_model_coef_n == 3
                                    regression_y = regression_model_fun(regression_model_coef(ci + 1, 1), regression_model_coef(ci + 1, 2), regression_model_coef(ci + 1, 3), regression_x);
                                    pred_y{ci, 1} = regression_model_fun(regression_model_coef(ci + 1, 1), regression_model_coef(ci + 1, 2), regression_model_coef(ci + 1, 3), test_data(class_idx & non_nan, i));
                                    pred_y{ci, 2} = regression_model_fun(regression_model_coef(ci + 1, 1), regression_model_coef(ci + 1, 2), regression_model_coef(ci + 1, 3), test_data(non_nan, i));
                                elseif ismember(r, GLM_ID)
                                    regression_y = predict(regression_model{ci + 1}, regression_x') * 27;
                                    pred_y{ci, 1} = predict(regression_model{ci + 1}, test_data(class_idx & non_nan, i)) * 27;
                                    pred_y{ci, 2} = predict(regression_model{ci + 1}, test_data(non_nan, i)) * 27;
                                end
                                meas_y{ci} = test_participant_output(class_idx & non_nan);
                                if r == 18
                                    trend_handles{ci} = plot(regression_x([1, end]), [1 1] * regression_model{ci + 1}, '--', 'Color', [1 1 1] * 0.5, 'LineWidth', 1.5);
                                elseif r == 19
                                    trend_handles{ci} = plot(regression_x([1, end]), [1 1] * regression_model{ci + 1}, 'Color', lines(1), 'LineWidth', 1.5);
                                elseif r == 20
                                    trend_handles{ci} = plot([1, 1] * regression_model{ci + 1}, ylim_value, '--', 'Color', params.clr_cohort(test_classes(ci) + 2, :), 'LineWidth', 1.5);
                                else
                                    trend_handles{ci} = plot(regression_x, regression_y, 'Color', params.clr_cohort(test_classes(ci) + 2, :), 'LineWidth', 1);
                                end
                             end
                        end
                        if ~COMBINED_OUTPUT
                            trend_clr = params.clr_cohort(t, :);
                        else
                            trend_clr = R_TEST_CLR(R_TEST == r, :);
                        end
                        if r == 2
                            regression_y = 0:27;
                            regression_x = regression_model_fun(regression_model_coef(1, 1), regression_model_coef(1, 2), regression_y);
                            pred_y{numel(test_classes) + 1, 1} = regression_model_fun_inv(regression_model_coef(1, 1), regression_model_coef(1, 2), test_data(test_train_idx & non_nan, i));
                        elseif regression_model_coef_n == 2
                            regression_y = regression_model_fun(regression_model_coef(1, 1), regression_model_coef(1, 2), regression_x);
                            pred_y{numel(test_classes) + 1, 1} = regression_model_fun(regression_model_coef(1, 1), regression_model_coef(1, 2), test_data(test_train_idx & non_nan, i));
                        elseif regression_model_coef_n == 3
                            regression_y = regression_model_fun(regression_model_coef(1, 1), regression_model_coef(1, 2), regression_model_coef(1, 3), regression_x);
                            pred_y{numel(test_classes) + 1, 1} = regression_model_fun(regression_model_coef(1, 1), regression_model_coef(1, 2), regression_model_coef(1, 3), test_data(test_train_idx & non_nan, i));
                        elseif ismember(r, GLM_ID)
                            regression_y = predict(regression_model{1}, regression_x') * 27;
                            pred_y{numel(test_classes) + 1, 1} = predict(regression_model{1}, test_data(test_train_idx & non_nan, i)) * 27;
                        end
                        meas_y{numel(test_classes) + 1} = test_participant_output(test_train_idx & non_nan);

                        if r == 18
                            trend_handles{numel(test_classes) + 1} = plot(regression_x([1, end]), [1 1] * regression_model{ci + 1}, '--', 'Color', [1 1 1] * 0.5, 'LineWidth', 2);
                        elseif r == 19
                            trend_handles{numel(test_classes) + 1} = plot(regression_x([1, end]), [1 1] * regression_model{ci + 1}, 'Color', lines(1), 'LineWidth', 2);
                        elseif r == 20
                            trend_handles{numel(test_classes) + 1} = plot([1, 1] * regression_model{ci + 1}, ylim_value, '--', 'Color', trend_clr, 'LineWidth', 2);
                        else
                            trend_handles{numel(test_classes) + 1} = plot(regression_x, regression_y, 'Color', trend_clr, 'LineWidth', 2);
                        end
                        
                        if ~COMBINED_OUTPUT
                            if r == 2
                                trend_handles{numel(test_classes) + 2} = plot(regression_x + regression_model_rmse(1), regression_y, '--', 'Color', trend_clr, 'LineWidth', 1);
                                trend_handles{numel(test_classes) + 3} = plot(regression_x - regression_model_rmse(1), regression_y, '--', 'Color', trend_clr, 'LineWidth', 1);
                            elseif ~ismember(r, 18:20)
                                trend_handles{numel(test_classes) + 2} = plot(regression_x, regression_y + regression_model_rmse(1), '--', 'Color', trend_clr, 'LineWidth', 1);
                                trend_handles{numel(test_classes) + 3} = plot(regression_x, regression_y - regression_model_rmse(1), '--', 'Color', trend_clr, 'LineWidth', 1);
                            end
                        else
                            combined_figure_legend{i, R_TEST == r} = trend_handles{numel(test_classes) + 1};
                            
                            if ~ismember(r, 18:20)
                                for result_i = 1:numel(RESULTS_TO_SHOW)
                                    result_name = RESULTS_TO_SHOW{result_i};
                                    
                                    this_result = NaN;
                                    
                                    if sum(strcmp(result_name, {'rmsd', 'nrmsd'})) > 0
                                        this_result = rms(pred_y{numel(test_classes) + 1, 1} - meas_y{numel(test_classes) + 1});
                                    elseif sum(strcmp(result_name, {'mae', 'nmae'})) > 0
                                        this_result = mean(abs(pred_y{numel(test_classes) + 1, 1} - meas_y{numel(test_classes) + 1}));
                                    elseif strcmp(result_name, 'rss')
                                        this_result = sum((pred_y{numel(test_classes) + 1, 1} - meas_y{numel(test_classes) + 1}) .^ 2);
                                    end
                                    
                                    if sum(strcmp(result_name, {'nrmsd', 'nmae'})) > 0
                                        this_result = this_result / max(meas_y{numel(test_classes) + 1});
                                    end
                                    
                                    combined_error_rate(i, R_TEST == r, result_i) = this_result;
                                end
                            end
                        end
                        hold off;

                        if ismember(r, 18:20)
                            ci_range = [];
                        elseif show_classes
                            ci_range = 1:(numel(test_classes) + 1);
                        else
                            ci_range = numel(test_classes) + 1;
                        end

                        for ci = ci_range
                            rms_offset = (r - 2) * ((2 * numel(test_classes)) + 1);
                            pred_mrmsd{t}(i, rms_offset + ci) = rms(pred_y{ci, 1} - meas_y{ci}) / max(meas_y{ci});

                            if ci <= numel(test_classes)
                                if ~isempty(nrmsd_ind_text)
                                    nrmsd_ind_text = sprintf('%s, ', nrmsd_ind_text);
                                end

                                nrmsd_ind_text = sprintf('%sOnly %s: %.3f', nrmsd_ind_text, params.test_description{test_classes(ci) + 2}, pred_mrmsd{t}(i, rms_offset + ci));

                                pred_mrmsd{t}(i, rms_offset + numel(test_classes) + 1 + ci) = rms(pred_y{ci, 2} - meas_y{numel(test_classes) + 1}) / max(meas_y{numel(test_classes) + 1});

                                if ~isempty(nrmsd_comb_text)
                                    nrmsd_comb_text = sprintf('%s, ', nrmsd_comb_text);
                                end

                                nrmsd_comb_text = sprintf('%s%s: %.3f', nrmsd_comb_text, params.test_description{test_classes(ci) + 2}, pred_mrmsd{t}(i, rms_offset + numel(test_classes) + 1 + ci));
                            else
                                if show_classes
                                    nrmsd_comb_text = sprintf('%.3f (%s)', pred_mrmsd{t}(i, rms_offset + ci), nrmsd_comb_text);
                                else
                                    nrmsd_comb_text = sprintf('%.3f', pred_mrmsd{t}(i, rms_offset + ci));
                                end
                            end
                        end
                    elseif COMBINED_OUTPUT
                        for result_i = 1:numel(RESULTS_TO_SHOW)
                            result_name = RESULTS_TO_SHOW{result_i};
                            
                            this_result = NaN;
                            
                            if sum(strcmp(result_name, {'rmsd', 'nrmsd'})) > 0
                                this_result = rms(mean(test_participant_output(test_train_idx & non_nan)) - test_participant_output(test_train_idx & non_nan));
                            elseif sum(strcmp(result_name, {'mae', 'nmae'})) > 0
                                this_result = mean(abs(mean(test_participant_output(test_train_idx & non_nan)) - test_participant_output(test_train_idx & non_nan)));
                            elseif strcmp(result_name, 'rss')
                                this_result = sum((mean(test_participant_output(test_train_idx & non_nan)) - test_participant_output(test_train_idx & non_nan)) .^ 2);
                            end
                            
                            if sum(strcmp(result_name, {'nrmsd', 'nmae'})) > 0
                                this_result = this_result / max(test_participant_output(test_train_idx & non_nan));
                            end
                            
                            combined_error_rate(i, R_TEST == r, result_i) = this_result;
                        end
                    end

                    if show_classes
                        c_max = numel(test_classes) + 1;
                    else
                        c_max = 0;
                    end
                    for ci = -1:c_max
                        valid_legend_entries = false((2 * numel(test_classes)) + 1, 1);
                        if show_classes
                            valid_legend_entries(1:numel(test_classes)) = true;
                        else
                            valid_legend_entries(1) = true;
                        end
                        if ci >= 0
                            valid_legend_entries((2 * numel(test_classes)) + 1) = true;
                        end

                        for cj = 1:numel(test_classes)
                            if ci > numel(test_classes)
                                valid_legend_entries(numel(test_classes) + cj) = true;
                                if r > 1
                                    set(trend_handles{cj}, 'visible', 'on');
                                end
                            elseif r > 1
                                set(trend_handles{cj}, 'visible', 'off');
                            end
                        end

                        if r > 1
                            for cj = 1:3
                                if ci == -1
                                    set(trend_handles{numel(test_classes) + cj}, 'visible', 'off');
                                else
                                    set(trend_handles{numel(test_classes) + cj}, 'visible', 'on');
                                end
                            end
                        end

                        if (ci >= 1) && (ci <= numel(test_classes))
                            valid_legend_entries(numel(test_classes) + ci) = true;
                            if r > 1
                                set(trend_handles{ci}, 'visible', 'on');
                            end
                        end

                        if COMBINED_OUTPUT
                            trend_description = sprintf('_%.2i', R_TEST(1:find(R_TEST == r)));
                        elseif ci == -1
                            trend_description = '';
                        elseif ci == 0
                            trend_description = '_model';
                        elseif ci <= numel(test_classes)
                            trend_description = sprintf('_model_%s', params.test_description_filename{test_classes(ci) + 2});
                        else
                            trend_description = '_model_all';
                        end

                        if (r > 1 && ci >= 0) || (r == 1 && ci == -1)
                            if ci == 0 && ~COMBINED_OUTPUT
                                if show_classes
                                    annotation('textbox', [0 0.825 1 0.1], 'String', {sprintf('NRMSD: %s\n(%s)', nrmsd_comb_text, nrmsd_ind_text)}, 'FontSize', 20, 'HorizontalAlignment', 'center', 'LineStyle', 'none', label_opts{:});
                                else
                                    annotation('textbox', [0 0.825 1 0.1], 'String', {sprintf('NRMSD: %s', nrmsd_comb_text)}, 'FontSize', 20, 'HorizontalAlignment', 'center', 'LineStyle', 'none', label_opts{:});
                                end
                            end

                            legend off;
                            
                            if numel(tick_label_opts) > 0
                                set(gca, tick_label_opts{:})
                            end
                            
                            if ~COMBINED_OUTPUT || show_partial_combined || (r_j == numel(R_TEST))
                                prepare_and_save_figure(sprintf('%s/%s', OUTPUT_SUB_DIR_MODELS, this_regression_type_name), sprintf('features_%s_%s%s_%s%s', output_variable, this_feature_name_file, this_feature_suffix, this_params.test_description_filename, trend_description), varargin{:}, 'FontSize', 30, 'figure_handle', f1, 'save_figure', show_without_legend);
                            end
                            
%                             xlabel(this_feature_name, label_opts{:})
%                             h_legend = legend([h_scatter{i, :}], h_scatter_text{i, :}, 'Location', 'East', label_opts{:});
%                             hold on;
%                             h_threshold = plot([limits(1), limits(2)], [10 10], 'k--', 'LineWidth', 1);
%                             uistack(h_threshold, 'bottom');
%                             hold off;
%                             prepare_and_save_figure(sprintf('%s/%s', OUTPUT_SUB_DIR_MODELS, this_regression_type_name), sprintf('features_%s_%s%s_%s%s_small', output_variable, this_feature_name_file, this_feature_suffix, this_params.test_description_filename, trend_description), varargin{:}, 'FontSize', 16, 'figure_handle', f1);
%                             xlabel('', label_opts{:})
%                             delete(h_legend);
%                             delete(h_threshold);
%                             prepare_and_save_figure(sprintf('%s/%s', OUTPUT_SUB_DIR_MODELS, this_regression_type_name), sprintf('features_%s_%s%s_%s%s', output_variable, this_feature_name_file, this_feature_suffix, this_params.test_description_filename, trend_description), varargin{:}, 'FontSize', 30, 'figure_handle', f1, 'save_figure', false);
                            
                            if COMBINED_OUTPUT && show_with_legend
                                combined_valid_legend = ~cellfun(@isempty, combined_figure_legend(i, :));
                                
                                if sum(combined_valid_legend) > 0
                                    limit_centre = (limits(1) + limits(2)) / 2;
                                    above_center = sum(test_data(non_nan, i) >= limit_centre) / sum(non_nan);
                                    if rho(i) <= 0 && above_center >= 0.5
                                        legend([combined_figure_legend{i, combined_valid_legend}], R_TEST_DESC(combined_valid_legend), 'Location', 'SouthWest', 'FontSize', 18, label_opts{:});
                                    elseif rho(i) <= 0 && above_center < 0.5
                                        legend([combined_figure_legend{i, combined_valid_legend}], R_TEST_DESC(combined_valid_legend), 'Location', 'NorthEast', 'FontSize', 18, label_opts{:});
                                    elseif rho(i) > 0 && above_center >= 0.5
                                        legend([combined_figure_legend{i, combined_valid_legend}], R_TEST_DESC(combined_valid_legend), 'Location', 'NorthWest', 'FontSize', 18, label_opts{:});
                                    elseif rho(i) > 0 && above_center < 0.5
                                        legend([combined_figure_legend{i, combined_valid_legend}], R_TEST_DESC(combined_valid_legend), 'Location', 'SouthEast', 'FontSize', 18, label_opts{:});
                                    end
                                    
                                    if numel(tick_label_opts) > 0
                                        set(gca, tick_label_opts{:})
                                    end
                                    
                                    if ~COMBINED_OUTPUT || show_partial_combined || (r_j == numel(R_TEST))
                                        prepare_and_save_figure(sprintf('%s/%s', OUTPUT_SUB_DIR_MODELS, this_regression_type_name), sprintf('features_%s_%s%s_%s%s_legend', output_variable, this_feature_name_file, this_feature_suffix, this_params.test_description_filename, trend_description), varargin{:}, 'FontSize', [], 'figure_handle', f1);
                                    end
                                end
                            end
                        end

                        if r == 1 && i == 1 && ~do_pca && ~COMBINED_OUTPUT && show_legend_only && (show_partial_combined || (r_j == numel(R_TEST)))
                            f2 = figure;

                            legend_handles = cell((2 * numel(test_classes)) + 1, 1);
                            legend_strings = cell((2 * numel(test_classes)) + 1, 1);

                            hold on;

                            if show_classes
                                for cj = 1:numel(test_classes)
                                    if isempty(highlight_participants)
                                        this_clr = params.clr_cohort(test_classes(cj) + 2, :);
                                    else
                                        this_clr = ones(1, 3) * 0.75;
                                    end
                                    legend_handles{cj} = plot([1, 1], params.symbol_cohort{test_classes(cj) + 2}, 'Color', this_clr, 'MarkerFaceColor', this_clr, 'LineWidth', 1.5, 'MarkerSize', 12, 'Visible', 'off');
                                    legend_strings{cj} = sprintf('%s Data', params.test_description{test_classes(cj) + 2});
                                end
                            else
                                if isempty(highlight_participants)
                                    this_clr = params.clr_cohort(t, :);
                                else
                                    this_clr = ones(1, 3) * 0.75;
                                end
                                legend_handles{1} = plot([1, 1], 'o', 'Color', this_clr, 'MarkerFaceColor', this_clr, 'LineWidth', 1.5, 'MarkerSize', 12, 'Visible', 'off');
                                legend_strings{1} = sprintf('%s Data', this_params.test_description);
                            end

                            if show_classes
                                for cj = 1:numel(test_classes)
                                    legend_handles{numel(test_classes) + cj} = plot([0 0], [1 1], 'Color', params.clr_cohort(test_classes(cj) + 2, :), 'LineWidth', 1, 'Visible', 'off');
                                    legend_strings{numel(test_classes) + cj} = sprintf('%s Model', params.test_description{test_classes(cj) + 2});
                                end
                            end

                            legend_handles{(2 * numel(test_classes)) + 1} = plot([1 1], [2 2], 'Color', params.clr_cohort(t, :), 'LineWidth', 2, 'Visible', 'off');
                            legend_handles{(2 * numel(test_classes)) + 2} = plot([2 2], [1 1], '--', 'Color', params.clr_cohort(t, :), 'LineWidth', 1);
                            if t == 1
                                legend_strings{(2 * numel(test_classes)) + 1} = 'Fused Model';
                                legend_strings{(2 * numel(test_classes)) + 2} = 'Fused RMSE';
                            else
                                legend_strings{(2 * numel(test_classes)) + 1} = sprintf('%s Model', this_params.test_description);
                                legend_strings{(2 * numel(test_classes)) + 2} = sprintf('%s RMSE', this_params.test_description);
                            end
                            hold off;

                            h_legend = legend([legend_handles{valid_legend_entries}], legend_strings(valid_legend_entries), 'FontSize', 20, label_opts{:});

                            axis off

                            set(h_legend, 'Units', 'pixels');
                            set(f2, 'Units', 'pixels');
                            for orientation = {'vertical', 'horizontal'}
                                set(h_legend, 'orientation', orientation{:});
                                drawnow();
                                p_legend = get(h_legend, 'Position');
                                p_legend(1:2) = [2 2];
                                set(h_legend, 'Position', p_legend);
                                p_figure = get(f2, 'Position');
                                p_figure(3:4) = p_legend(3:4) + [2 2];
                                set(f2, 'Position', p_figure);
                                set(f2, 'PaperPositionMode', 'auto')
                                prepare_and_save_figure(sprintf('%s/%s', OUTPUT_SUB_DIR_MODELS, this_regression_type_name), sprintf('features_%s_00_legend%s_%s%s_%s', output_variable, this_feature_suffix, this_params.test_description_filename, trend_description, orientation{:}), varargin{:}, 'FontSize', 20);
                            end
                        elseif i == 1 && ~do_pca && COMBINED_OUTPUT && show_legend_only && (show_partial_combined || (r_j == numel(R_TEST)))
                            f2 = figure;

                            legend_handles = cell(numel(test_classes) + find(R_TEST == r) - 1, 1);
                            legend_strings = cell(numel(test_classes) + find(R_TEST == r) - 1, 1);

                            hold on;

                            for cj = 1:numel(test_classes)
                                if isempty(highlight_participants)
                                    this_clr = params.clr_cohort(test_classes(cj) + 2, :);
                                else
                                    this_clr = ones(1, 3) * 0.75;
                                end
                                legend_handles{cj} = plot([1, 1], params.symbol_cohort{test_classes(cj) + 2}, 'Color', this_clr, 'MarkerFaceColor', this_clr, 'LineWidth', 1.5, 'MarkerSize', 12, 'Visible', 'off');
                                legend_strings{cj} = sprintf('%s Data', params.test_description{test_classes(cj) + 2});
                            end

                            for rj = 2:find(R_TEST == r)
                                trend_clr = R_TEST_CLR(rj, :);
                                if R_TEST(rj) == 18
                                    legend_handles{numel(test_classes) + rj - 1} = plot([1, 1], '--', 'Color', [1 1 1] * 0.5, 'LineWidth', 2, 'Visible', 'off');
                                elseif R_TEST(rj) == 19
                                    legend_handles{numel(test_classes) + rj - 1} = plot([1, 1], 'Color', lines(1), 'LineWidth', 2, 'Visible', 'off');
                                elseif R_TEST(rj) == 20
                                    legend_handles{numel(test_classes) + rj - 1} = plot([1, 1], '--', 'Color', trend_clr, 'LineWidth', 2, 'Visible', 'off');
                                else
                                    legend_handles{numel(test_classes) + rj - 1} = plot([1, 1], 'Color', trend_clr, 'LineWidth', 2, 'Visible', 'off');
                                end
                                legend_strings{numel(test_classes) + rj - 1} = R_TEST_DESC{rj};
                            end

                            hold off;

                            h_legend = legend([legend_handles{:}], legend_strings, 'FontSize', 20, label_opts{:});

                            axis off

                            set(h_legend, 'Units', 'pixels');
                            set(f2, 'Units', 'pixels');
                            for orientation = {'vertical', 'horizontal'}
                                set(h_legend, 'orientation', orientation{:});
                                drawnow();
                                p_legend = get(h_legend, 'Position');
                                p_legend(1:2) = [2 2];
                                set(h_legend, 'Position', p_legend);
                                p_figure = get(f2, 'Position');
                                p_figure(3:4) = p_legend(3:4) + [2 2];
                                set(f2, 'Position', p_figure);
                                set(f2, 'PaperPositionMode', 'auto')
                                prepare_and_save_figure(sprintf('%s/%s', OUTPUT_SUB_DIR_MODELS, this_regression_type_name), sprintf('features_%s_00_legend%s_%s%s_%s', output_variable, this_feature_suffix, this_params.test_description_filename, trend_description, orientation{:}), varargin{:}, 'FontSize', 20);
                            end
                            
                            close(f2);
                        end
                    end
                    
                    if ~COMBINED_OUTPUT && save_figures
                        close(f1);
                    end
                end

                if numel(features_to_show_individually) > 0
                    figure;

                    imagesc(pred_mrmsd{t});
                end

                if r > 1 && COMBINED_OUTPUT && ~ismember(r, 18:20) && run_model_cv
                    if nargout >= 1
                        predictions{t}.y_pred{r_i + 1} = NaN(numel(test_participants), numel(features_to_show));
                    end

                    output_pred = NaN(numel(features_to_show), numel(test_participants));
                    output_pred_result = struct();
                    
                    fprintf('Running model %i (%s)\n', r_i, regression_type_legend_name{r})

                    for cumulative = CUMULATIVE_OUTPUT
                        if cumulative
                            cumulative_type_opts = false(1, 4);
                            for i = 1:numel(CUMULATIVE_LASSO)
                                if CUMULATIVE_LASSO(i) == true
                                    cumulative_type_opts(2) = true;
                                elseif numel(CUMULATIVE_LASSO) > 1
                                    cumulative_type_opts(1) = true;
                                end
                            end
                            for i = 1:numel(CUMULATIVE_LASSO_PER_PARTICIPANT)
                                if CUMULATIVE_LASSO_PER_PARTICIPANT(i) == true
                                    cumulative_type_opts(4) = true;
                                elseif numel(CUMULATIVE_LASSO_PER_PARTICIPANT) > 1
                                    cumulative_type_opts(1) = true;
                                end
                            end
                            for i = 1:numel(CUMULATIVE_WRAPPER)
                                if CUMULATIVE_WRAPPER(i) == true
                                    cumulative_type_opts(3) = true;
                                elseif numel(CUMULATIVE_WRAPPER) > 1
                                    cumulative_type_opts(1) = true;
                                end
                            end
                            if (numel(CUMULATIVE_WRAPPER) == 1) && (CUMULATIVE_WRAPPER == false) && ...
                                    (numel(CUMULATIVE_LASSO) == 1) && (CUMULATIVE_LASSO == false) && ...
                                    (numel(CUMULATIVE_LASSO_PER_PARTICIPANT) == 1) && (CUMULATIVE_LASSO_PER_PARTICIPANT == false)
                                cumulative_type_opts(1) = true;
                            end
                        else
                            cumulative_type_opts = 1;
                        end
                        for cumulative_type = find(cumulative_type_opts)
                            use_lasso = false;
                            use_lasso_per_participant = false;
                            use_wrapper = false;
                            if cumulative_type == 2
                                use_lasso = true;
                            elseif cumulative_type == 4
                                use_lasso_per_participant = true;
                            elseif cumulative_type == 3
                                use_wrapper = true;
                            end
                              
                            max_features_to_show = numel(features_to_show);
                            if cumulative
                                fprintf(' Processing cumulative features')
                                if use_lasso
                                    fprintf(' with LASSO')
                                elseif use_lasso_per_participant
                                    fprintf(' with LASSO per participant')
                                elseif use_wrapper
                                    fprintf(' with wrapper')
                                end
                                if do_pca
                                    fprintf(' (PCA)')
                                end
                                fprintf('\n')
                                max_features_to_show = min(numel(features_to_show), cumulative_max_features);
                                
                                if use_wrapper && (nargout >= 1)
                                    predictions{t}.y_pred{r_i + 1} = NaN(numel(test_participants), max_features_to_show);
                                    predictions{t}.y_pred_fs{r_i + 1} = NaN(numel(test_participants), numel(features_to_show), max_features_to_show);
                                    predictions{t}.y_pred_features{r_i + 1} = NaN(1, max_features_to_show);
                                end
                            end
                            cumulative_str = '';
                            if do_pca
                                cumulative_str = '_pca';
                            end
                            if cumulative
                                cumulative_str = [cumulative_str '_cumulative']; %#ok<AGROW>
                            end
                            if use_lasso
                                cumulative_str = [cumulative_str '_lasso']; %#ok<AGROW>
                            elseif use_lasso_per_participant
                                cumulative_str = [cumulative_str '_lasso_per_participant']; %#ok<AGROW>
                            elseif use_wrapper
                                cumulative_str = [cumulative_str '_wrapper']; %#ok<AGROW>
                            end
                            for result_name = RESULTS_TO_SHOW
                                output_pred_result.([result_name{:} cumulative_str]) = NaN(numel(features_to_show), 1);
                            end
                            if r_i == 1
                                str_xticklabel = cell(numel(features_to_show), 1);

                                for i = 1:numel(features_to_show)
                                    if do_pca
                                        str_xticklabel{i} = sprintf('PC%i', i);
                                    elseif use_wrapper || use_lasso_per_participant
                                        str_xticklabel{i} = num2str(i);
                                    elseif cumulative
                                        str_xticklabel{i} = params.feature_short{features_to_show(feature_rank(i))};
                                    else
                                        str_xticklabel{i} = params.feature_short{features_to_show(i)};
                                    end
                                end

                                non_nan = ones(size(test_participants));
                                
                                train_mean = NaN(numel(test_participant_output), 1);

                                for p = test_participants_unique'
                                    test_idx = ismember(test_participants, p);

                                    if sum(test_idx & non_nan & test_train_idx) == 0
                                        continue;
                                    end

                                    train_idx_full = ~test_idx & non_nan & test_train_idx;
                                    test_idx = test_idx & non_nan & test_train_idx;

                                    train_mean(test_idx) = mean(test_participant_output(train_idx_full));
                                end
                                
                                baseline = struct;
                                
                                for result_name = RESULTS_TO_SHOW
                                    this_result = NaN;
                                    
                                    if sum(strcmp(result_name, {'rmsd', 'nrmsd'})) > 0
                                        this_result = rms(train_mean(test_train_idx & non_nan) - test_participant_output(test_train_idx & non_nan));
                                    elseif sum(strcmp(result_name, {'mae', 'nmae'})) > 0
                                        this_result = mean(abs(train_mean(test_train_idx & non_nan) - test_participant_output(test_train_idx & non_nan)));
                                    elseif strcmp(result_name, 'rss')
                                        this_result = sum((train_mean(test_train_idx & non_nan) - test_participant_output(test_train_idx & non_nan)) .^ 2);
                                    end
                                    
                                    if sum(strcmp(result_name, {'nrmsd', 'nmae'})) > 0
                                        this_result = this_result / max(test_participant_output(test_train_idx & non_nan));
                                    end
                                    
                                    baseline.(result_name{:}) = this_result;
                                end
                                
                                for result_name = RESULTS_TO_SHOW
                                    result_figure.([result_name{:} cumulative_str]) = struct();
                                    result_figure.([result_name{:} cumulative_str]).handle = figure;
                                    result_figure.([result_name{:} cumulative_str]).filename = sprintf('regression_result%s_%s_%s', cumulative_str, this_params.test_description_filename, strrep(result_name{:}, '_', '-'));
                                    result_figure.([result_name{:} cumulative_str]).legend_str = {'Baseline'};
                                    
                                    this_figure = result_figure.([result_name{:} cumulative_str]);
                                    
                                    axes;
                                    
                                    plot([0.5 (numel(features_to_show) + 0.5)], [0 0] + baseline.(result_name{:}), 'LineWidth', 1.5);
                                    
                                    if ~strcmp(result_name, 'legend')
                                        title_details = '';

                                        if do_pca
                                            title_details = 'PCA, ';
                                            feature_type = 'Principal Component';
                                        else
                                            feature_type = 'Feature';
                                        end

                                        if cumulative
                                            if use_lasso
                                                title_str = sprintf('Regression Error Rate (%sCumulative, Regularised using LASSO)', title_details);
                                            elseif use_lasso_per_participant
                                                title_str = sprintf('Regression Error Rate (%sCumulative, Regularised using LASSO per participant)', title_details);
                                            elseif use_wrapper
                                                title_str = sprintf('Regression Error Rate (%sCumulative, Wrapper Feature Selection)', title_details);
                                            else
                                                title_str = sprintf('Regression Error Rate (%sCumulative)', title_details);
                                            end
                                            if use_lasso_per_participant
                                                xlabel_str = sprintf('%ss Included in Model', feature_type);
                                            else
                                                xlabel_str = sprintf('%ss Cumulatively Presented to Model', feature_type);
                                            end
                                        else
                                            title_str = sprintf('Regression Error Rate (%sIndividual)', title_details);
                                            xlabel_str = sprintf('%s Presented to Model', feature_type);
                                        end

                                        title(title_str, label_opts{:});

                                        set(gca, 'xlim', [0.5 (max_features_to_show + 0.5)]);
                                        set(gca, 'xtick', 1:max_features_to_show);

                                        if numel(tick_label_opts) > 0
                                            set(gca, tick_label_opts{:})
                                        end

                                        p = get(gcf, 'PaperPosition');
                                        p(3) = p(3) * 2.2;

                                        if sum(strcmp(result_name, {'rmsd', 'nrmsd'})) > 0
                                            if strcmp(output_variable, 'qids') && strcmp(result_name, 'rmsd')
                                                set(gca, 'ylim', [4.25, 6.25]);
                                            elseif strcmp(output_variable, 'qids') && strcmp(result_name, 'nrmsd')
                                                set(gca, 'ylim', [0.18, 0.32]);
                                            end
                                            set(gca, 'xticklabel', ' ');
                                            xlabel(' ', label_opts{:});
                                            p(4) = p(4) * 0.8;
                                        elseif sum(strcmp(result_name, {'mae', 'nmae', 'rss', 'p_value'})) > 0
                                            if strcmp(output_variable, 'qids') && strcmp(result_name, 'mae')
                                                set(gca, 'ylim', [3.25, 5.25]);
                                            elseif strcmp(output_variable, 'qids') && strcmp(result_name, 'nmae')
                                                set(gca, 'ylim', [0.13, 0.27]);
                                            elseif strcmp(result_name, 'p_value')
                                                set(gca, 'ylim', [0, 1.1]);
                                            end
                                            set(gca, 'xticklabel', str_xticklabel(1:max_features_to_show));
                                            xlabel(xlabel_str, label_opts{:});
                                            p(4) = p(4) * 0.8;
                                        end

                                        set(gcf, 'PaperPosition', p);

                                        ylabel(upper(strrep(result_name{:}, '_', '-')), label_opts{:});

                                        prepare_and_save_figure('', this_figure.filename, varargin{:}, 'FontSize', 30);
                                    else
                                        h_legend = legend(this_figure.legend_str, 'FontSize', 20, label_opts{:});

                                        axis off

                                        set(h_legend, 'Units', 'pixels');
                                        set(this_figure.handle, 'Units', 'pixels');
                                        for orientation = {'vertical', 'horizontal'}
                                            set(h_legend, 'orientation', orientation{:});
                                            drawnow();
                                            p_legend = get(h_legend, 'Position');
                                            p_legend(1:2) = [2 2];
                                            set(h_legend, 'Position', p_legend);
                                            p_figure = get(this_figure.handle, 'Position');
                                            p_figure(3:4) = p_legend(3:4) + [2 2];
                                            set(this_figure.handle, 'Position', p_figure);
                                            set(this_figure.handle, 'PaperPositionMode', 'auto')
                                            prepare_and_save_figure('', sprintf('%s_%s', this_figure.filename, orientation{:}), varargin{:}, 'FontSize', 20);
                                        end
                                    end
                                    hold on;
                                end
                            end

                            if (cumulative && ~ismember(r, GLM_ID)) || ((use_lasso || use_lasso_per_participant) && ~ismember(r, LASSO_ID) && ~ismember(r, GLM_ID))
                                continue;
                            elseif ((use_lasso || use_lasso_per_participant) && ~ismember(r, LASSO_ID) && ismember(r, GLM_ID))
                                is_valid = false;
                            else
                                is_valid = true;
                                
                                feature_rank_wrapper = NaN(numel(features_to_show), 1);
                                
                                if use_lasso_per_participant && ismember(r, LASSO_ID)
                                    non_nan = (sum(isnan(test_data), 2) == 0);
                                    
                                    participant_lasso = cell(size(test_participants_unique));
                                    
                                    for p_i = 1:numel(test_participants_unique)
                                        p = test_participants_unique(p_i);
                                        
                                        test_idx = ismember(test_participants, p);
                                        train_idx_full = ~test_idx & non_nan & test_train_idx;
                                        
                                        [B_lasso, FI_lasso] = lassoglm(test_data(train_idx_full, :), test_participant_output(train_idx_full) / 27, glmopts_dist, 'CV', 10);
                                        
                                        model_fitted = [FI_lasso.Intercept; B_lasso];

                                        participant_lasso{p_i} = struct();
                                        participant_lasso{p_i}.model = model_fitted;
                                        participant_lasso{p_i}.FI = FI_lasso;
                                    end
                                end
                                
                                for i = 1:max_features_to_show
                                    if cumulative
                                        fprintf('  Processing feature %i of %i', i, max_features_to_show)
                                        if use_wrapper
                                            remaining_feature_idx = isnan(feature_rank_wrapper);
                                            
                                            wrapper_selected_features_idx = ~remaining_feature_idx;
                                            
                                            wrapper_selection_results = Inf(size(feature_rank_wrapper));
                                            
                                            for this_feature = find(remaining_feature_idx')
                                                wrapper_selected_features_new_idx = wrapper_selected_features_idx;
                                                wrapper_selected_features_new_idx(this_feature) = true;
                                                
                                                test_pred = NaN(1, numel(test_participants));
                                                
                                                non_nan = (sum(isnan(test_data(:, wrapper_selected_features_new_idx)), 2) == 0);

                                                for p = test_participants_unique'
                                                    test_idx = ismember(test_participants, p);

                                                    if sum(test_idx & non_nan & test_train_idx) == 0
                                                        continue;
                                                    end

                                                    train_idx_full = ~test_idx & non_nan & test_train_idx;
                                                    test_idx = test_idx & non_nan & test_train_idx;
                                                    
                                                    if r == 2
                                                        model_fitted = fit(test_participant_output(train_idx_full), test_data(train_idx_full, wrapper_selected_features_new_idx), regression_model_opts{:});
                                                        test_pred(test_idx) = regression_model_fun_inv(model_fitted.a, model_fitted.b, test_data(test_idx, wrapper_selected_features_new_idx));
                                                    elseif ismember(r, STD_ID)
                                                        model_fitted = fit(test_data(train_idx_full, wrapper_selected_features_new_idx), test_participant_output(train_idx_full), regression_model_opts{:});
                                                        if regression_model_coef_n == 2
                                                            test_pred(test_idx) = regression_model_fun(model_fitted.a, model_fitted.b, test_data(test_idx, wrapper_selected_features_new_idx));
                                                        elseif regression_model_coef_n == 3
                                                            test_pred(test_idx) = regression_model_fun(model_fitted.a, model_fitted.b, model_fitted.c, test_data(test_idx, wrapper_selected_features_new_idx));
                                                        end
                                                    elseif ismember(r, GLM_ID)
                                                        model_fitted = fitglm(test_data(train_idx_full, wrapper_selected_features_new_idx), test_participant_output(train_idx_full) / 27, glmopts{:});
                                                        test_pred(test_idx) = predict(model_fitted, test_data(test_idx, wrapper_selected_features_new_idx)) * 27;
                                                    end
                                                end
                                                
                                                if nargout >= 1
                                                    predictions{t}.y_pred_fs{r_i + 1}(:, this_feature, i) = test_pred;
                                                end
                                                
                                                output_non_nan = ~isnan(test_pred);
                                                
                                                wrapper_selection_results(this_feature) = mean(abs(test_pred(output_non_nan)' - test_participant_output(output_non_nan)));
                                            end
                                            
                                            [~, selected_feature] = min(wrapper_selection_results);
                                            
                                            feature_rank_wrapper(selected_feature) = sum(wrapper_selected_features_idx) + 1;
                                            
                                            str_xticklabel{i} = params.feature_short{features_to_show(selected_feature)};
                                            
                                            if nargout >= 1
                                                predictions{t}.y_pred_features{r_i + 1}(:, i) = features_to_show(selected_feature);
                                            end
                                        end
                                        
                                        fprintf(' (%s)\n', str_xticklabel{i})
                                    end
                                    
                                    if cumulative && do_pca
                                        selected_features_idx = 1:i;
                                    elseif cumulative && use_wrapper
                                        selected_features_idx = ~isnan(feature_rank_wrapper);
                                    elseif use_lasso_per_participant && ismember(r, LASSO_ID)
                                        selected_features_idx = ones(size(test_data, 2), 1);
                                    elseif cumulative
                                        selected_features_idx = feature_rank(1:i);
                                    else
                                        selected_features_idx = i;
                                    end

                                    non_nan = (sum(isnan(test_data(:, selected_features_idx)), 2) == 0);
                                    
                                    for p_i = 1:numel(test_participants_unique)
                                        p = test_participants_unique(p_i);
                                        
                                        test_idx = ismember(test_participants, p);
                                        
                                        if sum(test_idx & non_nan & test_train_idx) == 0
                                            continue;
                                        end
                                        
                                        train_idx_full = ~test_idx & non_nan & test_train_idx;
                                        test_idx = test_idx & non_nan & test_train_idx;

                                        if r == 2
                                            model_fitted = fit(test_participant_output(train_idx_full), test_data(train_idx_full, selected_features_idx), regression_model_opts{:});
                                            output_pred(i, test_idx) = regression_model_fun_inv(model_fitted.a, model_fitted.b, test_data(test_idx, selected_features_idx));
                                        elseif ismember(r, STD_ID)
                                            model_fitted = fit(test_data(train_idx_full, selected_features_idx), test_participant_output(train_idx_full), regression_model_opts{:});
                                            if regression_model_coef_n == 2
                                                output_pred(i, test_idx) = regression_model_fun(model_fitted.a, model_fitted.b, test_data(test_idx, selected_features_idx));
                                            elseif regression_model_coef_n == 3
                                                output_pred(i, test_idx) = regression_model_fun(model_fitted.a, model_fitted.b, model_fitted.c, test_data(test_idx, selected_features_idx));
                                            end
                                        elseif use_lasso && ismember(r, LASSO_ID)
                                            if (i == 1)
                                                model_fitted = glmfit(test_data(train_idx_full, selected_features_idx), test_participant_output(train_idx_full) / 27, glmopts_dist);
                                            else
                                                [B_lasso, FI_lasso] = lassoglm(test_data(train_idx_full, selected_features_idx), test_participant_output(train_idx_full) / 27, glmopts_dist, 'CV', 10);
                                                model_fitted = [FI_lasso.Intercept; B_lasso];

                                                model_fitted = model_fitted(:, FI_lasso.IndexMinDeviance);
                                            end
                                            output_pred(i, test_idx) = glmval(model_fitted, test_data(test_idx, selected_features_idx), glmopts_link) * 27;
                                        elseif use_lasso_per_participant && ismember(r, LASSO_ID)
                                            model_fitted = participant_lasso{p_i}.model;
                                            FI_lasso = participant_lasso{p_i}.FI;
                                            
                                            model_fitted_features = sum(model_fitted(2:end, :) ~= 0, 1);
                                            
                                            if sum(model_fitted_features == i) > 0
                                                this_i = i;
                                            elseif sum(model_fitted_features < i) > 0
                                                this_i = max(model_fitted_features(model_fitted_features < i));
                                            elseif sum(model_fitted_features > i) > 0
                                                this_i = min(model_fitted_features(model_fitted_features > i));
                                            else
                                                continue; % This should never happen.
                                            end
                                            
                                            model_fitted_features_i = find(model_fitted_features == this_i);
                                            
                                            [~, model_fitted_features_i_min] = min(FI_lasso.Deviance(model_fitted_features_i));
                                            
                                            model_fitted_features_i_min_idx = model_fitted_features_i(model_fitted_features_i_min);
                                            
                                            model_fitted = model_fitted(:, model_fitted_features_i_min_idx);
                                            
                                            output_pred(i, test_idx) = glmval(model_fitted, test_data(test_idx, :), glmopts_link) * 27;
                                        elseif ismember(r, GLM_ID)
                                            model_fitted = fitglm(test_data(train_idx_full, selected_features_idx), test_participant_output(train_idx_full) / 27, glmopts{:});
                                            output_pred(i, test_idx) = predict(model_fitted, test_data(test_idx, selected_features_idx)) * 27;
                                        end
                                    end

                                    if nargout >= 1
                                        predictions{t}.y_pred{r_i + 1}(:, i) = output_pred(i, :);
                                    end

                                    output_non_nan = ~isnan(output_pred(i, :));

                                    for result_name = RESULTS_TO_SHOW
                                        this_result = NaN;
                                        
                                        if sum(strcmp(result_name, {'rmsd', 'nrmsd'})) > 0
                                            this_result = rms(output_pred(i, output_non_nan)' - test_participant_output(output_non_nan));
                                        elseif sum(strcmp(result_name, {'mae', 'nmae'})) > 0
                                            this_result = mean(abs(output_pred(i, output_non_nan)' - test_participant_output(output_non_nan)));
                                        elseif strcmp(result_name, 'rss')
                                            this_result = sum((output_pred(i, output_non_nan)' - test_participant_output(output_non_nan)) .^ 2);
                                        elseif strcmp(result_name, 'p_value')
                                            baseline_rss = baseline.rss;
                                            this_rss = output_pred_result.(['rss' cumulative_str])(i);
                                            
                                            baseline_df = 1;
                                            if use_lasso && ismember(r, LASSO_ID)
                                                this_df = size(model_fitted, 1);
                                            elseif use_lasso_per_participant && ismember(r, LASSO_ID)
                                                this_df = i + 1;
                                            elseif ismember(r, GLM_ID)
                                                this_df = size(model_fitted.Coefficients, 1);
                                            else
                                                this_df = regression_model_coef_n;
                                            end
                                            n = sum(non_nan & test_train_idx);
                                            
                                            this_f_stat = ((baseline_rss - this_rss) / (this_df - baseline_df)) / (this_rss / (n - this_df));
                                            this_result = 1 - fcdf(this_f_stat, this_df - baseline_df, n - this_df);
                                        end
                                        
                                        if sum(strcmp(result_name, {'nrmsd', 'nmae'})) > 0
                                            this_result = this_result / max(test_participant_output(test_train_idx));
                                        end
                                        
                                        output_pred_result.([result_name{:} cumulative_str])(i) = this_result;
                                    end
                                end
                            end

                            for result_name = RESULTS_TO_SHOW
                                this_figure = result_figure.([result_name{:} cumulative_str]);
                                figure(this_figure.handle);
                                
                                hold on;

                                this_legend_str = this_figure.legend_str;
                                this_legend_str = [this_legend_str R_TEST_DESC(r_j)]; %#ok<AGROW>
                                
                                if cumulative
                                    if is_valid
                                        h = plot(1:numel(features_to_show), output_pred_result.([result_name{:} cumulative_str]), [scatter_symbols{floor((r_i - 1) / 7) + 1} '-'], 'MarkerSize', 15, 'LineWidth', 2);
                                    end
                                    
                                    if use_lasso
                                        if is_valid
                                            clr_opts = {'Color', get(h, 'Color')};
                                        else
                                            clr_opts = {};
                                        end
                                        
                                        if isfield(output_pred_result, [result_name{:} strrep(cumulative_str, '_lasso', '')])
                                            plot(1:numel(features_to_show), output_pred_result.([result_name{:} strrep(cumulative_str, '_lasso', '')]), [scatter_symbols{floor((r_i - 1) / 7) + 1} ':'], clr_opts{:}, 'MarkerSize', 15, 'LineWidth', 2);
                                            
                                            if is_valid
                                                this_legend_str{end} = [this_legend_str{end} ' (LASSO)'];
                                                this_legend_str = [this_legend_str R_TEST_DESC(r_j)]; %#ok<AGROW>
                                            end
                                        end
                                    elseif use_lasso_per_participant
                                        if is_valid
                                            clr_opts = {'Color', get(h, 'Color')};
                                        else
                                            clr_opts = {};
                                        end
                                        
                                        if isfield(output_pred_result, [result_name{:} strrep(cumulative_str, '_lasso_per_participant', '')])
                                            plot(1:numel(features_to_show), output_pred_result.([result_name{:} strrep(cumulative_str, '_lasso_per_participant', '')]), [scatter_symbols{floor((r_i - 1) / 7) + 1} ':'], clr_opts{:}, 'MarkerSize', 15, 'LineWidth', 2);
                                            
                                            if is_valid
                                                this_legend_str{end} = [this_legend_str{end} ' (LASSO)'];
                                                this_legend_str = [this_legend_str R_TEST_DESC(r_j)]; %#ok<AGROW>
                                            end
                                        end
                                    end
                                else
                                    if sum(R_TEST > 1) > 1
                                        x_offset = (((find(R_TEST == r) - 2) / (sum(R_TEST > 1) - 1)) * 0.4) - 0.2;
                                    else
                                        x_offset = 0;
                                    end

                                    scatter((1:numel(features_to_show)) + x_offset, output_pred_result.([result_name{:} cumulative_str]), 200, scatter_symbols{floor((r_i - 1) / 7) + 1}, 'LineWidth', 2);
                                end
                                
                                result_figure.([result_name{:} cumulative_str]).legend_str = this_legend_str;
                                
                                if ~strcmp(result_name, 'legend')
                                    prepare_and_save_figure('', this_figure.filename, varargin{:}, 'FontSize', [], 'figure_handle', this_figure.handle);
                                else
                                    h_legend = legend(this_legend_str, 'FontSize', 20, label_opts{:});

                                    axis off

                                    set(h_legend, 'Units', 'pixels');
                                    set(this_figure.handle, 'Units', 'pixels');
                                    for orientation = {'vertical', 'horizontal'}
                                        set(h_legend, 'orientation', orientation{:});
                                        drawnow();
                                        p_legend = get(h_legend, 'Position');
                                        p_legend(1:2) = [2 2];
                                        set(h_legend, 'Position', p_legend);
                                        p_figure = get(this_figure.handle, 'Position');
                                        p_figure(3:4) = p_legend(3:4) + [2 2];
                                        set(this_figure.handle, 'Position', p_figure);
                                        set(this_figure.handle, 'PaperPositionMode', 'auto')
                                        prepare_and_save_figure('', sprintf('%s_%s', this_figure.filename, orientation{:}), varargin{:}, 'FontSize', 20);
                                    end
                                end
                            end
                            
                            if cumulative && is_valid
                                regression_results_filename = sprintf('regression_result%s_%s', cumulative_str, this_params.test_description_filename);
                                regression_results_sub_dir = OUTPUT_SUB_DIR;

                                if r_i == 1
                                    save_text(regression_results_filename, [{'output_sub_dir', regression_results_sub_dir}, varargin, {'append', false}], '%s\n', datestr(now));
                                end
                                
                                save_text_params = {regression_results_filename, [{'output_sub_dir', regression_results_sub_dir}, varargin, {'append', true}]};

                                valid_results_idx = ~strcmp(RESULTS_TO_SHOW, 'legend');
                                
                                line_width = ((sum(valid_results_idx) + 2) * 10) + 5;

                                save_fprintf(save_text_params{:}, '%s\n', repmat('=', line_width, 1));
                                save_fprintf(save_text_params{:}, '%s%s\n', repmat(' ', floor((line_width - numel(regression_type_legend_name{r})) / 2), 1), regression_type_legend_name{r});
                                save_fprintf(save_text_params{:}, '%s\n', repmat('-', line_width, 1));
                                this_line = '  No  Selected Feature  ';
                                output_format = ' %3i  %-16s  ';
                                for result_name = RESULTS_TO_SHOW(valid_results_idx)
                                    this_result_name = upper(strrep(result_name{:}, '_', '-'));
                                    while length(this_result_name) < 10
                                        this_result_name = [this_result_name ' ']; %#ok<AGROW>
                                    end
                                    this_line = [this_line this_result_name]; %#ok<AGROW>
                                    output_format = [output_format '%.6f  ']; %#ok<AGROW>
                                end
                                save_fprintf(save_text_params{:}, [this_line ' \n']);
                                
                                for i = 0:max_features_to_show
                                    this_feature_results = NaN(1, numel(RESULTS_TO_SHOW));
                                    this_feature_name = '';
                                    for result_i = 1:numel(RESULTS_TO_SHOW)
                                        if i == 0
                                            this_feature_results(result_i) = baseline.(RESULTS_TO_SHOW{result_i});
                                            this_feature_name = 'BASELINE';
                                        else
                                            this_feature_results(result_i) = output_pred_result.([RESULTS_TO_SHOW{result_i} cumulative_str])(i);
                                            this_feature_name = str_xticklabel{i};
                                        end
                                    end
                                    this_feature_results = this_feature_results(valid_results_idx);
                                    save_fprintf(save_text_params{:}, [output_format ' \n'], i, this_feature_name, this_feature_results)
                                end
                                save_fprintf(save_text_params{:}, '%s\n', repmat('=', line_width, 1));
                            end
                        end
                    end
                    r_i = r_i + 1;
                end
            end

            if COMBINED_OUTPUT && (sum(CUMULATIVE_OUTPUT == false) > 0)
                regression_results_filename_base = sprintf('features_%s_results', output_variable');
                
                if do_pca
                    regression_results_filename_base = [regression_results_filename_base '_pca']; %#ok<AGROW>
                end
                
                regression_results_filename = sprintf('%s_%s', regression_results_filename_base, this_params.test_description_filename);
                regression_results_sub_dir = sprintf('%s/%s/%s', OUTPUT_SUB_DIR, OUTPUT_SUB_DIR_MODELS, regression_type_description_combined);

                save_text(regression_results_filename, [{'output_sub_dir', regression_results_sub_dir}, varargin, {'append', false}], '%s\n', datestr(now));

                save_text_params = {regression_results_filename, [{'output_sub_dir', regression_results_sub_dir}, varargin, {'append', true}]};

                line_width = ((numel(RESULTS_TO_SHOW) + 1) * 10);

                for i = 1:numel(features_to_show)
                    save_fprintf(save_text_params{:}, '%s\n', repmat('=', line_width, 1));
                    save_fprintf(save_text_params{:}, '%s%s\n', repmat(' ', floor((line_width - numel(params.feature_name{features_to_show(i)})) / 2), 1), params.feature_name{features_to_show(i)});
                    save_fprintf(save_text_params{:}, '%s\n', repmat('-', line_width, 1));
                    this_line = '  Test   ';
                    output_format = '   %.2i    ';
                    for result_name = RESULTS_TO_SHOW
                        this_result_name = upper(strrep(result_name{:}, '_', '-'));
                        while length(this_result_name) < 10
                            this_result_name = [this_result_name ' ']; %#ok<AGROW>
                        end
                        this_line = [this_line this_result_name]; %#ok<AGROW>
                        output_format = [output_format '%.6f  ']; %#ok<AGROW>
                    end
                    save_fprintf(save_text_params{:}, [this_line ' \n']);
                    save_fprintf(save_text_params{:}, [output_format ' \n'], [R_TEST' reshape(combined_error_rate(i, :, :), numel(R_TEST), numel(RESULTS_TO_SHOW))]')
                    save_fprintf(save_text_params{:}, '%s\n', repmat('=', line_width, 1));
                end
            end
        end
    end
end
