function plot_location_weekly(data, week_beginning, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 30-Jan-2015

    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    
    label_opts = {};
    if latex_labels
        label_opts = {'Interpreter', 'latex'};
    end

    rows = get_argument('rows', 7, varargin{:});
    annotate_home = get_argument({'annotate_home', 'annotatehome'}, false, varargin{:});
    annotate_locations = get_argument({'annotate_locations', 'annotatelocations'}, false, varargin{:});
    show_state = get_argument({'show_state', 'showstate'}, false, varargin{:});
    
    activity_data = get_argument({'activity_data', 'activitydata'}, [], varargin{:});
    battery_data = get_argument({'battery_data', 'batterydata'}, [], varargin{:});
    
    first_day = get_argument({'first_day', 'firstday', 'start_at', 'startat'}, week_beginning, varargin{:});
    
    time_days = floor(data.time(data.time >= datenum([2014, 01, 01])));
    
    week_data_idx = (ismember(time_days, week_beginning + ((week_beginning - first_day):6)) & ~isnan(data.data));
    
    if annotate_locations && isfield(data, 'clusters') && isfield(data, 'cluster_idx')
        week_cluster_idx = unique(data.cluster_idx(week_data_idx));
        week_cluster_idx = week_cluster_idx(week_cluster_idx ~= 0);
        clr = zeros(max(week_cluster_idx) + 1, 3);
        clr(week_cluster_idx + 1, :) = parula(numel(week_cluster_idx));
        if annotate_home
            [~, home_cluster] = min(sqrt(sum(data.clusters .^ 2, 2)));
            clr_tmp = lines(2);
            clr(home_cluster + 1, :) = clr_tmp(2, :);
        end
        location_annotations = data.cluster_idx;
    elseif annotate_home && isfield(data, 'clusters') && isfield(data, 'cluster_idx')
        [~, home_cluster] = min(sqrt(sum(data.clusters .^ 2, 2)));
        location_annotations = double(data.cluster_idx == home_cluster);
        clr = lines(2);
        clr(1, :) = [0 0 0];
    elseif isfield(data, 'location_group')
        location_annotations = data.location_group;
        clr = lines(max(location_annotations) + 1);
        clr(1, :) = [0 0 0];
    else
        location_annotations = zeros(size(data.data));
        clr = [0 0 0];
    end
    
    battery_clr = lines(7);
    battery_clr = battery_clr(7, :);
    
    title_str = ['Week Beginning ' datestr(week_beginning, 'dd-mmm-yyyy')];
    
    for j = 0:6
        thisday = week_beginning + j;
        
        day_data_idx = (time_days >= first_day) & (time_days == thisday) & ~isnan(data.data);
        day_data_time_rel = data.time(day_data_idx) - thisday;
        day_locations_annotation = location_annotations(day_data_idx);
        
        r_max = 1.05; % ratio from top tick to top of graph
        if isstruct(activity_data)
            r_min = -0.25; % ratio from bottom tick to bottom of graph
        else
            r_min = -0.05; % ratio from bottom tick to bottom of graph
        end
        
        max_y = 99 * r_max;
        
        data_clipped = min(data.data(day_data_idx), max_y);
        
        if ~isempty(data_clipped)
            y_max_tick = max(min(ceil(max(data_clipped) / 10) * 10, 99), 10);
            y_mid_tick = max(min(ceil(max(data_clipped) / 10) * 10, 100), 10) / 2;
        else
            y_max_tick = 10;
            y_mid_tick = 5;
        end
        
        subplot(rows, 7, (j * 7) + (1:7));
        cla;
        
        hold on;
        plot([0 1], [0 0], 'color', [0.5 0.5 0.5]);
        plot([0 1], [y_mid_tick y_mid_tick], 'color', [0.5 0.5 0.5]);
        plot([0 1], [y_max_tick y_max_tick], 'color', [0.5 0.5 0.5]);
        
        stairs(day_data_time_rel, data_clipped, 'LineWidth', 2, 'Color', lines(1));
        scatter(day_data_time_rel, data_clipped, 50, clr(day_locations_annotation + 1, :), 'LineWidth', 2, 'Marker', 'x');
        %dist_max = max(data.data(day_data_idx));
        %dist_min = min(data.data(day_data_idx));
        %if isempty(dist_max)
        %    dist_max = 0;
        %    dist_min = 0;
        %end
        %a = [0 1 max((dist_min - 0.5), 0) (dist_max + 0.5)];
        
        y_lim = [y_max_tick * r_min y_max_tick * r_max];
        
        a = [0 1 y_lim];
        axis(a);
        axis manual;
        
        if show_state && isfield(data, 'state') && ~isempty(data_clipped)
            state_clr = [ 0.7  1    0.7  ;
                          0.7  0.7  1    ;
                          1    0.7  0.7  ;
                          0.85 1    0.85 ];
            
            location_time = min([1/(24 * 60); diff(data.time(day_data_idx))], 1/(24 * 12));
            
            h = zeros(1, 3);

            time_points = [(day_data_time_rel - location_time) day_data_time_rel day_data_time_rel];
            time_points_lin = reshape(time_points', numel(time_points), 1);
            time_points_cont = [abs(time_points(2:end, 1) - time_points(1:end-1, 3)) < (1/(24 * 60 * 6)); true];
            
            for this_state = 1:4
                this_state_idx = data.state(day_data_idx) == this_state;
                
                if (this_state == 1) && isfield(data, 'cluster_valid')
                    this_state_idx = this_state_idx & logical(data.cluster_valid(day_data_idx));
                elseif (this_state == 4) && isfield(data, 'cluster_valid')
                    this_state_idx = ~logical(data.cluster_valid(day_data_idx));
                end
                
                state_points = zeros(size(time_points));

                state_points(:, 1) = this_state_idx;
                state_points(:, 2) = this_state_idx;

                state_points(time_points_cont, 3) = NaN;

                state_points_lin = reshape(state_points', numel(state_points), 1);

                [a, b] = stairs(time_points_lin(~isnan(state_points_lin)), state_points_lin(~isnan(state_points_lin)));

                if day_data_time_rel(1) < (1/(24 * 12))
                    a_1 = 0;
                    b_1 = b(1);
                else
                    a_1 = a(1);
                    b_1 = 0;
                end
                if day_data_time_rel(end) > 1 - (1/(24 * 12))
                    a_end = 1;
                    b_end = b(end);
                else
                    a_end = a(end);
                    b_end = 0;
                end
                
                h(this_state) = patch([0; a_1; a; a_end; 1], double([0; b_1; b; b_end; 0]) * (y_lim(2) - y_lim(1)) + y_lim(1), state_clr(this_state, :));
            end
            
            uistack(h, 'bottom')
        end
        
        if isstruct(activity_data)
            activity_idx = (activity_data.time >= first_day) & (activity_data.time >= thisday) & (activity_data.time < (thisday + 1)) & ~isnan(activity_data.data);

            this_activity_time = activity_data.time(activity_idx) - thisday;
            this_activity_data = sqrt(activity_data.x(activity_idx) .^ 2 + activity_data.y(activity_idx) .^ 2 + activity_data.z(activity_idx) .^ 2);

            this_activity_time_ds = 0:(1/(24 * 60)):1;
            this_activity_data_ds = NaN(size(this_activity_time_ds));

            for i = 1:numel(this_activity_time_ds) - 1
                activity_ds_idx = this_activity_time >= this_activity_time_ds(i) & this_activity_time < this_activity_time_ds(i + 1);
                if sum(activity_ds_idx) > 0
                     this_activity_data_ds(i) = max(this_activity_data(activity_ds_idx));
                end
            end
            
            this_activity_data_ds(isnan(this_activity_data_ds)) = 0;
            
            this_activity_data_ds = this_activity_data_ds - 9;
            
            this_activity_data_ds = this_activity_data_ds / 100;
            
            this_activity_data_ds = this_activity_data_ds * y_max_tick;
            
            this_activity_data_ds = this_activity_data_ds + (y_max_tick * r_min);

            stairs(this_activity_time_ds, this_activity_data_ds, 'linewidth', 1)
        end
        
        ylabel(datestr(thisday, 'ddd'), 'FontSize', 30, label_opts{:});
        set(gca,'XTick', (0:3:24)/24)
        if j == 6
            set(gca,'XTickLabel', datestr((0:3:24)/24, 'hh:00'))
        else
            set(gca,'XTickLabel', [])
        end
        set(gca,'YTick', [0 y_mid_tick y_max_tick])
        set(gca,'FontSize', 30)
        
        if latex_ticks
            set(gca, 'TickLabelInterpreter', 'latex')
        end
        
        hold off;
        
        if isstruct(battery_data)
            a1 = gca;
            a2 = axes('position', get(gca, 'position'));

            battery_idx = (battery_data.time >= first_day) & (battery_data.time >= thisday) & (battery_data.time < (thisday + 1)) & ~isnan(battery_data.data);

            this_battery_time = battery_data.time(battery_idx) - thisday;
            this_battery_data = battery_data.data(battery_idx);

            this_battery_time_ds = (-1/(24 * 12)):(1/(24 * 6)):(1 + (1/(24 * 12)));
            this_battery_data_ds = NaN(size(this_battery_time_ds));

            for i = 1:(numel(this_battery_time_ds) - 1)
                battery_ds_idx = this_battery_time >= this_battery_time_ds(i) & this_battery_time < this_battery_time_ds(i + 1);
                if sum(battery_ds_idx) > 0
                     this_battery_data_ds(i) = mean(this_battery_data(battery_ds_idx));
                end
            end
            this_battery_data_ds(end) = this_battery_data_ds(end-1);
            
            this_battery_time_ds = reshape(repmat(this_battery_time_ds, 2, 1), numel(this_battery_time_ds) * 2, 1)';
            this_battery_data_ds = reshape(repmat(this_battery_data_ds, 2, 1), numel(this_battery_data_ds) * 2, 1)';
            
            for i = fliplr(find(isnan(this_battery_data_ds)))
                if i < numel(this_battery_data_ds) && ~isnan(this_battery_data_ds(i + 1))
                    this_battery_data_ds(i + 1) = NaN;
                elseif i > 1 && ~isnan(this_battery_data_ds(i - 1))
                    this_battery_data_ds(i) = this_battery_data_ds(i - 1);
                end
            end
            
            this_battery_data_ds(1) = NaN;
            this_battery_data_ds(end) = NaN;
            
            this_battery_data_ds(isnan(this_battery_data_ds)) = r_min - 0.01;
            
            %plot(a2, this_battery_time, this_battery_data, '.', 'color', battery_clr)
            %hold on;
            patch(this_battery_time_ds, this_battery_data_ds, 'flat', 'facecolor', battery_clr, 'facealpha', 0.1, 'edgecolor', battery_clr, 'edgealpha', 0.5)
            %hold off;
            
            uistack(a2, 'down')
            
            set(a2, 'YAxisLocation', 'Right');
            set(a1, 'color', 'none');
            set(a2, 'XTick', []);
            set(a2, 'XLim', [0, 1]);
            set(a2, 'YLim', [r_min, r_max]);
            set(a2, 'YTick', [0, 1]);
            set(a2, 'YTickLabel', {'0', '100'});
            set(a2, 'YColor', battery_clr);
            set(a2, 'FontSize', 30)
            
            if latex_ticks
                set(gca, 'TickLabelInterpreter', 'latex')
            end
            
            if j == 0
                title(a2, title_str, 'FontSize', 30, label_opts{:});
            end
        else
            if j == 0
                title(title_str, 'FontSize', 30, label_opts{:});
            end
        end
    end
    
    p = get(gcf, 'Position');
    p(3) = 2 * p(3);
    p(4) = 2 * p(4);
    set(gcf, 'Position', p);
    
    p = get(gcf, 'PaperPosition');
    p(3) = 2 * p(3);
    p(4) = 2 * p(4);
    set(gcf, 'PaperPosition', p);
end
