function visualise_data_stats_true_colours_questions(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 10-Mar-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-stats';
    
    files = get_participants(varargin{:});
    
    data_dates = [2014, 1, 1; 2015, 3, 1];
    
    questionaires = {'ALTMAN', 'EQ-5D', 'GAD-7', 'QIDS'};

    for this_questionaire_cell = questionaires
        this_questionaire_str = this_questionaire_cell{1};
        this_questionaire = strrep(this_questionaire_str, '-', '_');
        this_questionaire_lc = lower(this_questionaire);
        
        data_class = cell(3, 1);
        for i = 1:3
          data_class{i} = struct('data', [], 'j', 1);
        end

        for file = files'
            fprintf('Loading %s\n', file.name)

            class = get_participant_property(file.name, 'classification', varargin{:});

            if ~isempty(class)
                data_tc = load_true_colours_data(file.name);

                if ~isempty(data_tc) && isfield(data_tc, this_questionaire) && isfield(data_tc.(this_questionaire), 'questions')
                    this_questionnaire_data = data_tc.(this_questionaire).questions;
                    
                    if ~isempty(this_questionnaire_data)
                        if isempty(data_class{class + 1}.data)
                            data_class{class + 1}.data = NaN(length(files), (datenum(data_dates(2,:)) - datenum(data_dates(1,:))) * 2, size(this_questionnaire_data, 2));
                        end
                        
                        data_range = 1:size(this_questionnaire_data, 1);

                        data_class{class + 1}.data(data_class{class + 1}.j, data_range, :) = this_questionnaire_data;

                        data_class{class + 1}.j = data_class{class + 1}.j + 1;
                    end
                end
            end
        end

        if strcmp(this_questionaire, 'ALTMAN') == 1
            q_max = 4;
        elseif strcmp(this_questionaire, 'EQ_5D') == 1
            q_max = 3;
        elseif strcmp(this_questionaire, 'GAD_7') == 1
            q_max = 3;
        elseif strcmp(this_questionaire, 'QIDS') == 1
            q_max = 3;
        end
        
        d_hist = 1;
        
        question_min = 1;
        
        while true
            figure;

            for i = 1:3
                j_range_class = 1:(data_class{i}.j - 1);
                questions = size(data_class{i}.data, 3);
                question_max = min(question_min + 7, questions);
                questions_figure = question_max - question_min + 1;
                for q = question_min:question_max
                    class_data = data_class{i}.data(j_range_class, :, q);

                    this_data = class_data(:);
                    hist_edges = (-d_hist / 2):d_hist:((ceil(q_max / 2) * 2) + (d_hist / 2));

                    subplot(3, questions_figure, ((i - 1) * questions_figure) + q - question_min + 1);

                    histogram(this_data, hist_edges);

                    hold on;
                    axis manual;

                    fit_distributions(this_data, hist_edges, varargin{:}, 'show_legend', false, 'offset', d_hist / 2);

                    if i < 3
                      set(gca, 'xticklabel', []);
                    end
                    set(gca, 'ytick', []);
                    set(gca, 'xlim', hist_edges([1 end]));
                    if i == 1
                        title([this_questionaire_str ' Q' num2str(q)]);
                    elseif i == 3
                        xlabel('Value');
                    end
                    if q == question_min
                        if i == 1
                            ylabel('HC');
                        elseif i == 2
                            ylabel('BD');
                        elseif i == 3
                            ylabel('BPD');
                        end
                    end
                    p = get(gca, 'Position');
                    p_diff = p(4) * 0.2;
                    p(4) = p(4) + p_diff;
                    if i == 1
                        p(2) = p(2) - p_diff;
                    elseif i < 3
                        p(2) = p(2) - (p_diff / 2);
                    end
                    p_diff = p(3) * 0.15;
                    p(3) = p(3) + p_diff;
                    if q == questions_figure
                        p(1) = p(1) - p_diff;
                    elseif q > question_min
                        p(1) = p(1) - ((q - question_min) * (p_diff / (questions_figure - 1)));
                    end
                    set(gca, 'Position', p);
                end
            end

            p = get(gcf, 'Position');
            p(3) = (p(3) / 5) * (questions_figure + 1);
            set(gcf, 'Position', p);

            p = get(gcf, 'PaperPosition');
            p(3) = (p(3) / 5) * (questions_figure + 1);
            set(gcf, 'PaperPosition', p);

            file_suffix = '';
            
            if (q < questions) || (question_min > 1)
                file_suffix = ['_' num2str(((question_min - 1) / 8) + 1)];
            end
            
            save_figure(['true_colours_questions_' this_questionaire_lc '_hist' file_suffix], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
            
            if q < questions
                question_min = q + 1;
            else
                break;
            end
        end
        
        d_hist = 0.5;
        
        question_min = 1;
        
        while true
            figure;

            for i = 1:3
                j_range_class = 1:(data_class{i}.j - 1);
                questions = size(data_class{i}.data, 3);
                question_max = min(question_min + 7, questions);
                questions_figure = question_max - question_min + 1;
                for q = question_min:question_max
                    class_data = data_class{i}.data(j_range_class, :, q);

                    this_data = nanmean(class_data, 2);
                    hist_edges = 0:d_hist:(ceil(q_max / 2) * 2);

                    subplot(3, questions_figure, ((i - 1) * questions_figure) + q - question_min + 1);

                    histogram(this_data, hist_edges);

                    hold on;
                    axis manual;

                    fit_distributions(this_data, hist_edges, varargin{:}, 'show_legend', false);

                    if i < 3
                      set(gca, 'xticklabel', []);
                    end
                    set(gca, 'ytick', []);
                    set(gca, 'xlim', hist_edges([1 end]));
                    if i == 1
                        title([this_questionaire_str ' Q' num2str(q)]);
                    elseif i == 3
                        xlabel('Mean');
                    end
                    if q == question_min
                        if i == 1
                            ylabel('HC');
                        elseif i == 2
                            ylabel('BD');
                        elseif i == 3
                            ylabel('BPD');
                        end
                    end
                    p = get(gca, 'Position');
                    p_diff = p(4) * 0.2;
                    p(4) = p(4) + p_diff;
                    if i == 1
                        p(2) = p(2) - p_diff;
                    elseif i < 3
                        p(2) = p(2) - (p_diff / 2);
                    end
                    p_diff = p(3) * 0.15;
                    p(3) = p(3) + p_diff;
                    if q == questions_figure
                        p(1) = p(1) - p_diff;
                    elseif q > question_min
                        p(1) = p(1) - ((q - question_min) * (p_diff / (questions_figure - 1)));
                    end
                    set(gca, 'Position', p);
                end
            end

            p = get(gcf, 'Position');
            p(3) = (p(3) / 5) * (questions_figure + 1);
            set(gcf, 'Position', p);

            p = get(gcf, 'PaperPosition');
            p(3) = (p(3) / 5) * (questions_figure + 1);
            set(gcf, 'PaperPosition', p);

            file_suffix = '';
            
            if (q < questions) || (question_min > 1)
                file_suffix = ['_' num2str(((question_min - 1) / 8) + 1)];
            end
            
            save_figure(['true_colours_questions_' this_questionaire_lc '_mean_hist' file_suffix], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
            
            if q < questions
                question_min = q + 1;
            else
                break;
            end
        end
        
        d_hist = 0.5;
        
        question_min = 1;
        
        while true
            figure;

            for i = 1:3
                j_range_class = 1:(data_class{i}.j - 1);
                questions = size(data_class{i}.data, 3);
                question_max = min(question_min + 7, questions);
                questions_figure = question_max - question_min + 1;
                for q = question_min:question_max
                    class_data = data_class{i}.data(j_range_class, :, q);

                    this_data = nanstd(class_data, 0, 2);
                    hist_edges = 0:d_hist:(ceil(q_max / 2) * 2);

                    subplot(3, questions_figure, ((i - 1) * questions_figure) + q - question_min + 1);

                    histogram(this_data, hist_edges);

                    hold on;
                    axis manual;

                    fit_distributions(this_data, hist_edges, varargin{:}, 'show_legend', false);

                    if i < 3
                      set(gca, 'xticklabel', []);
                    end
                    set(gca, 'ytick', []);
                    set(gca, 'xlim', hist_edges([1 end]));
                    if i == 1
                        title([this_questionaire_str ' Q' num2str(q)]);
                    elseif i == 3
                        xlabel('Std');
                    end
                    if q == question_min
                        if i == 1
                            ylabel('HC');
                        elseif i == 2
                            ylabel('BD');
                        elseif i == 3
                            ylabel('BPD');
                        end
                    end
                    p = get(gca, 'Position');
                    p_diff = p(4) * 0.2;
                    p(4) = p(4) + p_diff;
                    if i == 1
                        p(2) = p(2) - p_diff;
                    elseif i < 3
                        p(2) = p(2) - (p_diff / 2);
                    end
                    p_diff = p(3) * 0.15;
                    p(3) = p(3) + p_diff;
                    if q == questions_figure
                        p(1) = p(1) - p_diff;
                    elseif q > question_min
                        p(1) = p(1) - ((q - question_min) * (p_diff / (questions_figure - 1)));
                    end
                    set(gca, 'Position', p);
                end
            end

            p = get(gcf, 'Position');
            p(3) = (p(3) / 5) * (questions_figure + 1);
            set(gcf, 'Position', p);

            p = get(gcf, 'PaperPosition');
            p(3) = (p(3) / 5) * (questions_figure + 1);
            set(gcf, 'PaperPosition', p);

            file_suffix = '';
            
            if (q < questions) || (question_min > 1)
                file_suffix = ['_' num2str(((question_min - 1) / 8) + 1)];
            end
            
            save_figure(['true_colours_questions_' this_questionaire_lc '_std_hist' file_suffix], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
            
            if q < questions
                question_min = q + 1;
            else
                break;
            end
        end
        
        d_hist = 1;
        
        question_min = 1;
        
        while true
            figure;

            for i = 1:3
                j_range_class = 1:(data_class{i}.j - 1);
                questions = size(data_class{i}.data, 3);
                question_max = min(question_min + 7, questions);
                questions_figure = question_max - question_min + 1;
                for q = question_min:question_max
                    class_data = data_class{i}.data(j_range_class, :, q);

                    this_data = iqr(class_data, 2);
                    hist_edges = 0:d_hist:(ceil(q_max / 2) * 2);

                    subplot(3, questions_figure, ((i - 1) * questions_figure) + q - question_min + 1);

                    histogram(this_data, hist_edges);

                    hold on;
                    axis manual;

                    fit_distributions(this_data, hist_edges, varargin{:}, 'show_legend', false);

                    if i < 3
                      set(gca, 'xticklabel', []);
                    end
                    set(gca, 'ytick', []);
                    set(gca, 'xlim', hist_edges([1 end]));
                    if i == 1
                        title([this_questionaire_str ' Q' num2str(q)]);
                    elseif i == 3
                        xlabel('IQR');
                    end
                    if q == question_min
                        if i == 1
                            ylabel('HC');
                        elseif i == 2
                            ylabel('BD');
                        elseif i == 3
                            ylabel('BPD');
                        end
                    end
                    p = get(gca, 'Position');
                    p_diff = p(4) * 0.2;
                    p(4) = p(4) + p_diff;
                    if i == 1
                        p(2) = p(2) - p_diff;
                    elseif i < 3
                        p(2) = p(2) - (p_diff / 2);
                    end
                    p_diff = p(3) * 0.15;
                    p(3) = p(3) + p_diff;
                    if q == questions_figure
                        p(1) = p(1) - p_diff;
                    elseif q > question_min
                        p(1) = p(1) - ((q - question_min) * (p_diff / (questions_figure - 1)));
                    end
                    set(gca, 'Position', p);
                end
            end

            p = get(gcf, 'Position');
            p(3) = (p(3) / 5) * (questions_figure + 1);
            set(gcf, 'Position', p);

            p = get(gcf, 'PaperPosition');
            p(3) = (p(3) / 5) * (questions_figure + 1);
            set(gcf, 'PaperPosition', p);

            file_suffix = '';
            
            if (q < questions) || (question_min > 1)
                file_suffix = ['_' num2str(((question_min - 1) / 8) + 1)];
            end
            
            save_figure(['true_colours_questions_' this_questionaire_lc '_iqr_hist' file_suffix], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
            
            if q < questions
                question_min = q + 1;
            else
                break;
            end
        end
    end
end
