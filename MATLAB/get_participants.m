function participants = get_participants(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 27-Jan-2015

    paths = get_paths(varargin{:});
    
    participant_filter = get_argument({'dirfilter', 'dir_filter', 'participantfilter', 'participant_filter'}, '*', varargin{:});
    start_after = get_argument({'startafter', 'start_after'}, [], varargin{:});
    finish_before = get_argument({'finishbefore', 'finish_before'}, [], varargin{:});
    only_load = get_argument({'onlyload', 'only_load', 'loadonly', 'load_only'}, {}, varargin{:});
    classification_filter = get_argument({'classificationfilter', 'classification_filter', 'classfilter', 'class_filter'}, [], varargin{:});
    entered_before = get_argument({'enteredbefore', 'entered_before', 'joinedbefore', 'joined_before'}, [], varargin{:});
    entered_after = get_argument({'enteredafter', 'entered_after', 'joinedafter', 'joined_after'}, [], varargin{:});
    left_before = get_argument({'leftbefore', 'left_before'}, [], varargin{:});
    left_after = get_argument({'leftafter', 'left_after'}, [], varargin{:});
    phone_filter = get_argument({'phonefilter', 'phone_filter'}, [], varargin{:});
    android_version_filter = get_argument({'androidfilter', 'android_filter'}, [], varargin{:});
    min_app_version = get_argument({'appversion', 'app_version', 'minappversion', 'min_app_version'}, [], varargin{:});
    min_app_version_date = get_argument({'appversiondate', 'app_version_date', 'minappversiondate', 'min_app_version_date'}, [], varargin{:});
    
    mood_zoom_event = get_argument({'moodzoomevent', 'mood_zoom_event'}, '', varargin{:});
    
    if ~isempty(phone_filter) || ~isempty(android_version_filter)
        phone_details = load_phone_details_data();
    end
    
    participant_list = cell(0);
    
    if isempty(paths.app_name) && paths.prefer_working
        root_path = paths.working_path;
        
        files = dir([root_path participant_filter]);
        
        participant_list = cell(size(files));
        
        j = 1;
        
        for file = files'
            name_parts = regexp(file.name, '-', 'split');
            if ~file.isdir
                participant_list{j} = name_parts{1};
                j = j + 1;
            end
        end
        
        participant_list = unique(participant_list(1:(j-1)));
    end
    
    if numel(participant_list) == 0
        root_path = paths.processed_data_path;
        
        if strcmp(paths.app_name, 'mood-zoom')
            if ~isempty(mood_zoom_event)
                root_paths = {[root_path mood_zoom_event '/']};
            else
                root_paths = {};

                for this_event = dir(root_path)'
                    if (exist([root_path this_event.name], 'dir') == 7) && (this_event.name(1) ~= '.')
                        root_paths = [root_paths, {[root_path this_event.name '/']}]; %#ok<AGROW>
                    end
                end
            end
        else
            root_paths = {root_path};
        end
        
        participant_list = {};
        
        for this_root_path_cell = root_paths
            this_root_path = this_root_path_cell{:};
            this_sub_dir = strrep(this_root_path, root_path, '');
            if (exist([this_root_path participant_filter], 'dir') == 7)
                parent = [this_root_path participant_filter];
                participant = '';
                while isempty(participant)
                    [parent, participant] = fileparts(parent);
                end
                this_participant_list = {participant};
                this_participant_valid = true;
            else
                files = dir([this_root_path participant_filter]);

                this_participant_list = cell(size(files));
                this_participant_valid = false(size(files));

                for i=1:length(files)
                    if files(i).name(1) ~= '.'
                        this_participant_valid(i) = true;
                        this_participant_list{i} = files(i).name;
                    end
                end
            end
            
            participant_list = [participant_list; strcat(this_sub_dir, this_participant_list(this_participant_valid))]; %#ok<AGROW>
        end
        
        participant_list = unique(participant_list);
    end
    
    participant_list = sort(participant_list);
    participant_valid = false(size(participant_list));
    
    for i=1:length(participant_list)
        participant = participant_list{i};
        if ~isempty(start_after) || (~isempty(only_load) && sum(strcmp(participant, only_load)) == 0) || (participant(1) == '.')
            if ~isempty(start_after) && strcmp(participant, start_after)
                start_after = [];
            end
        else
            if ~isempty(finish_before)
                [~, sort_order] = sort({finish_before, participant});
                if (sort_order(1) == 1)
                    break;
                end
            end
            if ~isempty(only_load) && sum(strcmp(participant, only_load)) > 0
                participant_valid(i) = true;
            elseif isempty(only_load)
                participant_valid(i) = true;
            end
        end
    end
    
    participant_list = participant_list(participant_valid);
    
    if isempty(paths.app_name) && ~isempty(classification_filter) || ~isempty(entered_before) || ~isempty(entered_after) || ~isempty(left_before) || ~isempty(left_after)
        participant_list_new = cell(size(participant_list));
        j = 0;
        for participant = participant_list'
            participant_valid = true;
            
            if ~isempty(classification_filter)
                class = get_participant_property(participant{:}, 'classification', varargin{:});
                participant_valid = ~isempty(class) && ismember(class, classification_filter);
            end
            
            if ~isempty(entered_before) && participant_valid
                date_entered = get_participant_property(participant{:}, 'date_entered', varargin{:});
                participant_valid = ~isempty(date_entered) && datenum(date_entered) <= datenum(entered_before);
            end
            
            if ~isempty(entered_after) && participant_valid
                date_entered = get_participant_property(participant{:}, 'date_entered', varargin{:});
                participant_valid = ~isempty(date_entered) && datenum(date_entered) >= datenum(entered_after);
            end
            
            if ~isempty(left_before) && participant_valid
                date_left = get_participant_property(participant{:}, 'date_left', varargin{:});
                participant_valid = ~isempty(date_left) && datenum(date_left) <= datenum(left_before);
            end
            
            if ~isempty(left_after) && participant_valid
                date_left = get_participant_property(participant{:}, 'date_left', varargin{:});
                participant_valid = isempty(date_left) || datenum(date_left) >= datenum(left_after);
            end
            
            if participant_valid
                j = j + 1;
                participant_list_new{j} = participant{:};
            end
        end
        
        if j > 0
            participant_list = participant_list_new(1:j);
        else
            participant_list = cell(0);
        end
    end
    
    if ~isempty(phone_filter) || ~isempty(android_version_filter) || ~isempty(min_app_version)
        participant_list_new = cell(size(participant_list));
        j = 0;
        for participant = participant_list'
            participant_valid = true;
            
            if ~isempty(phone_filter) && participant_valid
                participant_phones = phone_details.phone(strcmp(phone_details.participant, participant{:}));
                participant_valid = (sum(strcmp(participant_phones, phone_filter)) > 0);
            end
            if ~isempty(android_version_filter) && participant_valid
                participant_android_versions = phone_details.version(strcmp(phone_details.participant, participant{:}));
                participant_valid = (sum(strcmp(participant_android_versions, android_version_filter)) > 0);
            end
            if ~isempty(min_app_version) && participant_valid
                reporting = load_reporting_data(participant{:}, varargin{:});

                if ~isempty(reporting) && isstruct(reporting)
                    reporting_versions_idx = strcmp([reporting.unsorted.setting], 'version');
                    reporting_versions = cellfun(@str2num, reporting.unsorted.value(reporting_versions_idx));
                    reporting_versions_upgraded_idx = reporting_versions >= min_app_version;

                    participant_valid = (sum(reporting_versions_upgraded_idx) > 0);

                    if participant_valid && ~isempty(min_app_version_date)
                        reporting_versions_time = reporting.unsorted.time(reporting_versions_idx);
                        participant_valid = reporting_versions_time(find(reporting_versions_upgraded_idx, 1, 'first')) <= datenum(min_app_version_date);
                    end
                else
                    participant_valid = false;
                end
            end
            
            if participant_valid
                j = j + 1;
                participant_list_new{j} = participant{:};
            end
        end
        
        if j > 0
            participant_list = participant_list_new(1:j);
        else
            participant_list = cell(0);
        end
    end
    
    participants = repmat(struct('name', ''), size(participant_list));
    
    for i=1:length(participant_list)
        participants(i).name = participant_list{i};
    end
end
