function visualise_features_location_v2_individual_trends(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 12-Oct-2014

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats/individuals/trends';
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    data_full = data_raw';
    
    FEATURES = size(data_full, 2);
    
    valid_qids = ~isnan(classification_properties.QIDS);
    
    params = get_configuration('features_location_v2', varargin{:});
    
    participant_id = classification_properties.participant(valid_qids);
    participant_qids_score = classification_properties.QIDS(valid_qids);
    participant_class = classification_properties.class(valid_qids);
    
    data_normalised = data_full(valid_qids, :);
    
    for i = 1:FEATURES
        data_normalised(:, i) = data_normalised(:, i) - nanmean(data_normalised(:, i));
        data_normalised(:, i) = data_normalised(:, i) / nanstd(data_normalised(:, i));
    end
    
    unique_participants = unique(participant_id);
    
    max_points = 0;
    
    for participant_cell = unique_participants'
        max_points = max(max_points, sum(strcmp(participant_id, participant_cell)));
    end
    
    data_values = NaN(length(unique_participants), max_points, FEATURES + 1);
    data_class = NaN(length(unique_participants), 1);
    data_range = NaN(length(unique_participants), FEATURES + 1);
    data_points = NaN(length(unique_participants), 1);
    
    for p_i = 1:numel(unique_participants)
        participant_data_idx = strcmp(participant_id, unique_participants{p_i});
        
        for i = 1:(FEATURES + 1)
            if i == 1
                all_points = participant_qids_score(participant_data_idx);
            else
                all_points = data_normalised(participant_data_idx, i - 1);
            end
            
            data_values(p_i, 1:numel(all_points), i) = all_points;
            
            data_range(p_i, i) = range(all_points);
        end
        
        data_class(p_i) = participant_class(find(participant_data_idx, 1, 'first'));
        data_points(p_i) = sum(participant_data_idx);
    end
    
    classes_unique = unique(data_class);
    
    first_class_idx = zeros(numel(classes_unique), 1);
    
    for c_i = 1:numel(classes_unique);
        first_class_idx(c_i) = find(data_class == classes_unique(c_i), 1, 'first');
    end
    
    valid_participants_idx = (data_points >= 3) & (data_range(:, 1) >= 5) & ismember(data_class, [0 1]);
    
    clr_corr = flipud(hot(100));
    
    for i = 1:FEATURES
        figure;
        
        participant_corr = NaN(numel(unique_participants), 1);
        
        for p_i = find(valid_participants_idx)'
            participant_corr(p_i) = corr(data_values(p_i, :, 1)', data_values(p_i, :, i + 1)', 'type', 'Spearman', 'rows', 'complete');
        end
        
        histogram(participant_corr, -1:0.2:1);
        
        title([params.feature_name{i} ' Correlation']);
        xlabel('Spearman''s \rho');
        ylabel('Participants');
        
        save_figure(sprintf('individual_feature_qids_corr_%.2i', i), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 20, varargin{:});
        
        figure;
        hold on;
        for p_i = find(valid_participants_idx)'
            if ~isnan(participant_corr(p_i))
                this_qids = data_values(p_i, :, 1);
                this_feature = data_values(p_i, :, i + 1);
                
                [~, sort_order] = sort(this_qids);
                
                plot(this_qids(sort_order), this_feature(sort_order), 'Color', clr_corr(round(abs(participant_corr(p_i) * 99)) + 1, :), 'LineWidth', 1.5);
            end
        end
        hold off;
        
        set(gca, 'xlim', [0 27])
        xlabel('QIDS Score');
        
        ylabel(params.feature_name{i});
        
        hcb = colorbar();
        colormap([flipud(clr_corr); clr_corr]);
        set(gca, 'clim', [-1 1]);
        ylabel(hcb, 'Spearman''s \rho');
        
        save_figure(sprintf('individual_feature_qids_trends_%.2i', i), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 20, varargin{:});
    end
end
