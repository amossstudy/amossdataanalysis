function data_interleaved = interleave_epoch(data, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    if isempty(data.time)
        t_min = 0;
        t_max = 0;
    else
        t_min = floor(data.time(1));
        t_max = ceil(data.time(end));
    end
    
    t_min = get_argument({'tmin', 't_min'}, t_min, varargin{:});
    t_max = get_argument({'tmax', 't_max'}, t_max, varargin{:});
    
    if ~isempty(data.time)
        if t_min > floor(data.time(1))
            t_vec = datevec(data.time(1));
            t_vec(3:6) = [1 0 0 0];
            t_min = floor(datenum(t_vec));
        end
        if t_max < ceil(data.time(end))
            t_vec = datevec(data.time(end));
            t_vec(3:6) = [1 0 0 0];
            t_max = floor(addtodate(datenum(t_vec), 1, 'month'));
        end
    end
    
    t_diff = t_max - t_min;
    
    % assume downsampled to 1 second resolution
    t_count = (t_diff) * 24 * 60 * 60;
    
    if data.epoch.min > 0
        t_count = floor(t_count / 60);
        t_count = t_count / data.epoch.min;
    elseif data.epoch.sec > 0
        t_count = t_count / data.epoch.sec;
    end
    
    t_count = t_count + 1;
    
    t_full = linspace(t_min, t_max, floor(t_count))';
    
    t = data.time;
    
    data_temp = NaN(length(t_full), size(data.data, 2), size(data.data, 3));
    
    j = 1;
    
    for i=1:size(t_full, 1)
        j_min = j;
        
        if j <= size(t, 1)
            
            assert(t(j) >= t_full(i))

            while (t(j) == t_full(i))
                j = j + 1;
                if j > size(t, 1)
                    break;
                end
            end
        end
        
        j_max = j - 1;
        
        if j_max >= j_min
            data_temp(i,:,:) = mean(data.data(j_min:j_max, :, :), 1);
        end
    end
    
    data_interleaved.time = t_full;
    data_interleaved.data = data_temp;
    data_interleaved.epoch.min = data.epoch.min;
    data_interleaved.epoch.sec = data.epoch.sec;
end