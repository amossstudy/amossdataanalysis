function test_classification_location(varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Dec-2014

    close all;
    
    OUTPUT_SUB_DIR = 'location';
    
    location_version = get_argument({'featureversion', 'feature_version'}, 1, varargin{:});
    
    if location_version == 1
        [data_raw, class_3c] = load_features_location(varargin{:});
    elseif location_version == 2
        [data_raw, classification_properties] = load_features_location_v2(varargin{:});
        class_3c = classification_properties.class;
        
        OUTPUT_SUB_DIR = [OUTPUT_SUB_DIR '/v2'];
    end
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    data_full = data_raw';
    
    class_2c = ismember(class_3c, [1, 2]);
    
    TEST_RF = 0;
    TESTS = 20;
    
    svm_test_results_2c = NaN(TESTS, 5);
    svm_test_results_3c = NaN(TESTS, 5);
    
    if TEST_RF
        rf_test_results = NaN(TESTS, 5);
    end
    
    cp_train_svm = classperf(class_2c);
    
    data_features_2c = data_full(:, 1:min(size(data_full, 2), 14));
    data_features_3c = data_full(:, 1:min(size(data_full, 2), 14));
    
    svm_model = fitcsvm(data_features_2c, class_2c, ...
             'Standardize', true, 'Solver', 'SMO', ...
             'KernelFunction', 'polynomial', 'PolynomialOrder', 4, 'Prior', 'uniform');
    
    pred_train = predict(svm_model, data_features_2c);
    
    cp_train_svm = classperf(cp_train_svm, pred_train);
    
    svm_test_results_2c(1, 1) = cp_train_svm.CorrectRate;
    
    cp_train_svm = classperf(class_3c);
    
    svm_model = fitcecoc(data_features_3c, class_3c, ...
             'learner', templateSVM('Standardize', true, 'Solver', 'SMO', ...
             'KernelFunction', 'polynomial', 'PolynomialOrder', 4));
    
    pred_train = predict(svm_model, data_features_3c);
    
    cp_train_svm = classperf(cp_train_svm, pred_train);
    
    svm_test_results_3c(1, 1) = cp_train_svm.CorrectRate;
    
    cp_svm_2c_total = classperf(class_2c);
    cp_svm_3c_total = classperf(class_3c);
    
    for i = 1:TESTS
        k = 2; % k-fold cross validation

        partitions_2c = cvpartition(class_2c, 'Kfold', k);
        partitions_3c = cvpartition(class_3c, 'Kfold', k);

        cp_svm_2c = classperf(class_2c);
        cp_svm_3c = classperf(class_3c);
        
        if TEST_RF
            cp_rf = classperf(class_2c); %#ok<*UNRCH>
        end
        
        for j = 1:k
            test_idx_2c = partitions_2c.test(j);
            train_idx_2c = partitions_2c.training(j);
            
            svm_model = fitcsvm(data_features_2c(train_idx_2c, :), class_2c(train_idx_2c), ...
                     'Standardize', true, 'Solver', 'SMO', ...
                     'KernelFunction', 'polynomial', 'PolynomialOrder', 4, 'Prior', 'uniform');

            pred_svm = predict(svm_model, data_features_2c(test_idx_2c, :));

            cp_svm_2c = classperf(cp_svm_2c, pred_svm, test_idx_2c);
            
            if TEST_RF
                rf_model = TreeBagger(50, data_features_2c(train_idx_2c, :), class_2c(train_idx_2c), 'Method', 'classification');

                pred_rf = str2double(rf_model.predict(data_features_2c(test_idx_2c, :)));

                cp_rf = classperf(cp_rf, pred_rf, test_idx_2c);
            end
            
            test_idx_3c = partitions_3c.test(j);
            train_idx_3c = partitions_3c.training(j);
            
            svm_model = fitcecoc(data_features_3c(train_idx_3c, :), class_3c(train_idx_3c), ...
                     'learner', templateSVM('Standardize', true, 'Solver', 'SMO', ...
                     'KernelFunction', 'polynomial', 'PolynomialOrder', 4));

            pred_svm = predict(svm_model, data_features_3c(test_idx_3c, :));

            cp_svm_3c = classperf(cp_svm_3c, pred_svm, test_idx_3c);
        end

        svm_test_results_2c(i, 2) = cp_svm_2c.CorrectRate;
        cp_svm_2c = classperf(class_2c);
        
        svm_test_results_3c(i, 2) = cp_svm_3c.CorrectRate;
        cp_svm_3c = classperf(class_3c);
        
        if TEST_RF
            rf_test_results(i, 2) = cp_rf.CorrectRate;
            cp_rf = classperf(class_2c);
        end
        
        k = 5; % k-fold cross validation

        partitions_2c = cvpartition(class_2c, 'Kfold', k);
        partitions_3c = cvpartition(class_3c, 'Kfold', k);
        
        for j = 1:k
            test_idx_2c = partitions_2c.test(j);
            train_idx_2c = partitions_2c.training(j);

            svm_model = fitcsvm(data_features_2c(train_idx_2c, :), class_2c(train_idx_2c), ...
                     'Standardize', true, 'Solver', 'SMO', ...
                     'KernelFunction', 'polynomial', 'PolynomialOrder', 4, 'Prior', 'uniform');

            pred_svm = predict(svm_model, data_features_2c(test_idx_2c, :));

            cp_svm_2c = classperf(cp_svm_2c, pred_svm, test_idx_2c);
            
            if TEST_RF
                rf_model = TreeBagger(50, data_features_2c(train_idx_2c, :), class_2c(train_idx_2c), 'Method', 'classification');

                pred_rf = str2double(rf_model.predict(data_features_2c(test_idx_2c, :)));

                cp_rf = classperf(cp_rf, pred_rf, test_idx_2c);
            end
            
            test_idx_3c = partitions_3c.test(j);
            train_idx_3c = partitions_3c.training(j);
            
            svm_model = fitcecoc(data_features_3c(train_idx_3c, :), class_3c(train_idx_3c), ...
                     'learner', templateSVM('Standardize', true, 'Solver', 'SMO', ...
                     'KernelFunction', 'polynomial', 'PolynomialOrder', 4));

            pred_svm = predict(svm_model, data_features_3c(test_idx_3c, :));

            cp_svm_3c = classperf(cp_svm_3c, pred_svm, test_idx_3c);
        end

        svm_test_results_2c(i, 3) = cp_svm_2c.CorrectRate;
        cp_svm_2c = classperf(class_2c);
        
        svm_test_results_3c(i, 3) = cp_svm_3c.CorrectRate;
        cp_svm_3c = classperf(class_3c);
        
        if TEST_RF
            rf_test_results(i, 3) = cp_rf.CorrectRate;
            cp_rf = classperf(class_2c);
        end
        
        k = 10; % k-fold cross validation

        partitions_2c = cvpartition(class_2c, 'Kfold', k);
        partitions_3c = cvpartition(class_3c, 'Kfold', k);
        
        for j = 1:k
            test_idx_2c = partitions_2c.test(j);
            train_idx_2c = partitions_2c.training(j);

            svm_model = fitcsvm(data_features_2c(train_idx_2c, :), class_2c(train_idx_2c), ...
                     'Standardize', true, 'Solver', 'SMO', ...
                     'KernelFunction', 'polynomial', 'PolynomialOrder', 4, 'Prior', 'uniform');

            pred_svm = predict(svm_model, data_features_2c(test_idx_2c, :));

            cp_svm_2c = classperf(cp_svm_2c, pred_svm, test_idx_2c);
            cp_svm_2c_total = classperf(cp_svm_2c_total, pred_svm, test_idx_2c);
            
            if TEST_RF
                rf_model = TreeBagger(50, data_features_2c(train_idx_2c, :), class_2c(train_idx_2c), 'Method', 'classification');

                pred_rf = str2double(rf_model.predict(data_features_2c(test_idx_2c, :)));

                cp_rf = classperf(cp_rf, pred_rf, test_idx_2c);
            end
            
            test_idx_3c = partitions_3c.test(j);
            train_idx_3c = partitions_3c.training(j);
            
            svm_model = fitcecoc(data_features_3c(train_idx_3c, :), class_3c(train_idx_3c), ...
                     'learner', templateSVM('Standardize', true, 'Solver', 'SMO', ...
                     'KernelFunction', 'polynomial', 'PolynomialOrder', 4));

            pred_svm = predict(svm_model, data_features_3c(test_idx_3c, :));

            cp_svm_3c = classperf(cp_svm_3c, pred_svm, test_idx_3c);
            cp_svm_3c_total = classperf(cp_svm_3c_total, pred_svm, test_idx_3c);
        end

        svm_test_results_2c(i, 4) = cp_svm_2c.CorrectRate;
        svm_test_results_3c(i, 4) = cp_svm_3c.CorrectRate;
        
        if TEST_RF
            rf_test_results(i, 4) = cp_rf.CorrectRate;
        end
    end
    
    cp_svm_2c = classperf(class_2c);
    partitions_2c = cvpartition(class_2c, 'LeaveOut');

    for j = 1:length(class_2c)
        test_idx_2c = partitions_2c.test(j);
        train_idx_2c = partitions_2c.training(j);

        svm_model = fitcsvm(data_features_2c(train_idx_2c, :), class_2c(train_idx_2c), ...
                 'Standardize', true, 'Solver', 'SMO', ...
                 'KernelFunction', 'polynomial', 'PolynomialOrder', 4, 'Prior', 'uniform');

        pred_svm = predict(svm_model, data_features_2c(test_idx_2c, :));

        cp_svm_2c = classperf(cp_svm_2c, pred_svm, test_idx_2c);
        cp_svm_2c_total = classperf(cp_svm_2c_total, pred_svm, test_idx_2c);

        if TEST_RF
            rf_model = TreeBagger(50, data_features_2c(train_idx_2c, :), class_2c(train_idx_2c), 'Method', 'classification');

            pred_rf = str2double(rf_model.predict(data_features_2c(test_idx_2c, :)));

            cp_rf = classperf(cp_rf, pred_rf, test_idx_2c);
        end
    end

    svm_test_results_2c(i, 5) = cp_svm_2c.CorrectRate;

    if TEST_RF
        rf_test_results(i, 5) = cp_rf.CorrectRate;
    end
    
    cp_svm_3c = classperf(class_3c);
    partitions_3c = cvpartition(class_3c, 'LeaveOut');

    for j = 1:length(class_3c)
        test_idx_3c = partitions_3c.test(j);
        train_idx_3c = partitions_3c.training(j);

        svm_model = fitcecoc(data_features_3c(train_idx_3c, :), class_3c(train_idx_3c), ...
                 'learner', templateSVM('Standardize', true, 'Solver', 'SMO', ...
                 'KernelFunction', 'polynomial', 'PolynomialOrder', 4));

        pred_svm = predict(svm_model, data_features_3c(test_idx_3c, :));

        cp_svm_3c = classperf(cp_svm_3c, pred_svm, test_idx_3c);
        cp_svm_3c_total = classperf(cp_svm_3c_total, pred_svm, test_idx_3c);
    end
    
    svm_test_results_3c(i, 5) = cp_svm_3c.CorrectRate;
    
    figure;
    boxplot(svm_test_results_2c);
    title('SVM 2-Class Classification Accuracy');
    set(gca, 'xticklabel', {'Train'; '2-Fold CV'; '5-Fold CV'; '10-Fold CV'; 'LOO CV'})
    set(gca, 'ylim', [0.4, 1])
    save_figure('classification_2_class', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure;
    boxplot(svm_test_results_3c);
    title('SVM 3-Class Classification Accuracy');
    set(gca, 'xticklabel', {'Train'; '2-Fold CV'; '5-Fold CV'; '10-Fold CV'; 'LOO CV'})
    set(gca, 'ylim', [0.4, 1])
    save_figure('classification_3_class', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    if TEST_RF
        figure;
        boxplot(rf_test_results);
        title('RF Classification Accuracy');
        set(gca, 'xticklabel', {'Train (N/A)'; '2-Fold CV'; '5-Fold CV'; '10-Fold CV'; 'LOO CV'})
        set(gca, 'ylim', [0.4, 1])
    end
    
    cp_svm_2c.CountingMatrix
    cp_svm_3c.CountingMatrix

    cp_svm_2c_total.CountingMatrix
    cp_svm_3c_total.CountingMatrix
end

