function cluster_locations_smc(participant, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 26-Oct-2014

    close all;
    
    OUTPUT_SUB_DIR = 'data-overview/weekly';
    
    data = load_location_data(participant, varargin{:});
    
    %%
    if ~isempty(data)
        %%
        fprintf('  Anonymizing data : ');
        tic;
        location_data = [data.lat, data.lon];
        
        if sum(size(location_data)) > 0
            loc_home = mode(location_data, 1);
        
            location_data(:,1) = location_data(:,1) - loc_home(1);
            location_data(:,2) = location_data(:,2) - loc_home(2);
        
            % In Oxford (51�45'07"N 1�15'28"W):
            %  Length Of A Degree Of Latitude In Meters: 111248.23835493479
            %  Length Of A Degree Of Longitude In Meters: 70197.65060613726
            % From http://www.csgnetwork.com/degreelenllavcalc.html
            
            location_data(:,1) = location_data(:,1) * 111.25; %km
            location_data(:,2) = location_data(:,2) * 70.2; %km
            
            scatter(location_data(:,1), location_data(:,2));
            
            %figure;
            %t= 0:pi/10:2*pi;
            %for i=1:size(location_data, 1)
            %    pb=patch(((0.1 * sin(t))+ location_data(i, 1)),((0.1 * cos(t))+location_data(i, 2)),'b','edgecolor','none');
            %    alpha(pb,.1);
            %end
        end
        
        data.data = sqrt(sum(location_data.^2,2));
        toc;
        %%
        
        time_days = floor(data.time);
        time_days_unique = time_days(diff(time_days) ~= 0);
        
        weeks_start = time_days_unique(1);
        weeks_start = weeks_start - weekday(weeks_start) + 2; % Start on the previous Monday
        weeks_end = time_days_unique(end);
        
        weeks = ((0:floor((weeks_end - weeks_start) / 7)) * 7) + weeks_start;
        
        %%
        %close all
        
        PARTICLES = 50;
        
        p_locations = zeros(PARTICLES, 1000, 2);
        p_locations_new = zeros(PARTICLES, 1000, 2);
        p_location_count = ones(1, PARTICLES);
        p_location_count_new = zeros(1, PARTICLES);
        
        clr = hsv(1000);
        
        clr = clr(randperm(size(clr, 1)),:);
        clr(1, :) = [0 0 0];
        clr(2, :) = [1 0 0];
        
        for w = 1:length(weeks)
            firstday = weeks(w);
            alldays = firstday + (0:6);
            
            figure;
            
            for j = 0:6
                thisday = firstday + j;
                day_i = (time_days == thisday);
                day_t = find(day_i);
                min_t = min(day_t);
                
                % state 1 = travelling; 2 = in a location; 
                initial_state = 2;
                transition = [0.6 0.4; 0.1 0.9];
                p_data = zeros(PARTICLES, sum(day_i), 2);
                p_data_new = zeros(PARTICLES, sum(day_i), 2);
                p_log_lik = zeros(1, PARTICLES);
                p_log_lik_new = zeros(1, PARTICLES);
                p_location_travel = zeros(1, 2);
                
                p_rand = rand(PARTICLES, sum(day_i));
                
                for t = 1:max(day_t - min_t + 1)
                    p_weights = ones(1, PARTICLES);
                    for p = 1:PARTICLES
                        if (t == 1)
                            p_data(p, t, 1) = initial_state;
                        else
                            if (sqrt(sum((location_data(day_t(t - 1), :) - location_data(day_t(t), :)) .^ 2)) >= 2)
                                p_data(p, t, 1) = 1;
                            else
                                p_data(p, t, 1) = sum((p_rand(p, t) >= cumsum(transition(p_data(p, t - 1, 1), :)))) + 1;
                                p_weights(p) = p_weights(p) * transition(p_data(p, t - 1, 1), p_data(p, t, 1));
                            end
                        end
                        
                        if (t == 1) || ((p_data(p, t, 1) == 2) && (p_data(p, t - 1, 1) == 1))
                            b_cont = ones(1, 2);
                            while b_cont(1) == 1
                                p_data(p, t, 2) = sum((rand(1) >= cumsum(ones(1, p_location_count(p) + 1) / (p_location_count(p) + 1)))) + 1;
                                if (p_data(p, t, 2) > p_location_count(p))
                                    while b_cont(2) < 100
                                        p_locations(p, p_location_count(p) + 1, 1) = location_data(day_t(t), 1) + (0.5 * randn(1));
                                        p_locations(p, p_location_count(p) + 1, 2) = location_data(day_t(t), 2) + (0.5 * randn(1));
                                        if ((min(abs(p_locations(p, 1:(p_location_count(p)), 1) - p_locations(p, p_location_count(p) + 1, 1))) > 0.5) && ...
                                                (min(abs(p_locations(p, 1:(p_location_count(p)), 2) - p_locations(p, p_location_count(p) + 1, 2))) > 0.5))
                                            b_cont(1) = 0;
                                            b_cont(2) = 100;
                                            p_location_count(p) = p_location_count(p) + 1;
                                        else
                                            b_cont(2) = b_cont(2) + 1;
                                        end
                                    end
                                    %p_weights(p) = p_weights(p) * 0.8; % / p_location_count(p); % Penalise creation of new locations
                                else
                                    b_cont(1) = 0;
                                end
                            end
                        elseif (p_data(p, t, 1) == 1)
                            p_data(p, t, 2) = 0;
                            p_location_travel(1) = location_data(day_t(t - 1), 1);% + (0.5 * p_randn(p, t, 1));
                            p_location_travel(2) = location_data(day_t(t - 1), 2);% + (0.5 * p_randn(p, t, 2));
                        elseif (p_data(p, t, 1) == 2)
                            p_data(p, t, 2) = p_data(p, t - 1, 2);
                        end
                        
                        if (p_data(p, t, 1) == 2)
                            p_weights(p) = p_weights(p) * normpdf(location_data(day_t(t), 1), p_locations(p, p_data(p, t, 2), 1), 0.5);
                            p_weights(p) = p_weights(p) * normpdf(location_data(day_t(t), 2), p_locations(p, p_data(p, t, 2), 2), 0.5);
                        elseif (p_data(p, t, 1) == 1)
                            p_weights(p) = p_weights(p) * (1 - normpdf(location_data(day_t(t), 1), p_location_travel(1), 0.5));
                            p_weights(p) = p_weights(p) * (1 - normpdf(location_data(day_t(t), 2), p_location_travel(2), 0.5));
                        end
                    end
                    
                    p_log_lik = p_log_lik + log(p_weights);
                    
                    p_weights = p_weights / sum(p_weights);
                    
                    % Always retain particle with max log likelihood
                    %[~, p_max_log_lik_i] = max(p_log_lik);
                    %p_data_new(1, :, :) = p_data(p_max_log_lik_i, :, :);
                    %p_log_lik_new(1) = p_log_lik(p_max_log_lik_i);
                    
                    for p = 1:PARTICLES
                        p_sample = sum((rand(1) >= cumsum(p_weights))) + 1;
                        
                        p_data_new(p, :, :) = p_data(p_sample, :, :);
                        p_locations_new(p, :, :) = p_locations(p_sample, :, :);
                        p_location_count_new(p) = p_location_count(p_sample);
                        p_log_lik_new(p) = p_log_lik(p_sample);
                    end
                    
                    p_data = p_data_new;
                    p_locations = p_locations_new;
                    p_location_count = p_location_count_new;
                    p_log_lik = p_log_lik_new;
                end
                
                [~, p_max_log_lik_i] = max(p_log_lik);
                
                % clr = lines(max(p_data(p_max_log_lik_i, :, 2)) + 1);
                
%                 figure;
%                 imagesc(data.time(day_i) - thisday, 1:PARTICLES, p_data(:, :, 1));
% 
%                 figure;
%                 stairs(data.time(day_i) - thisday, data.data(day_i), 'LineWidth', 2);
%                 hold on,
%                 scatter(data.time(day_i) - thisday, data.data(day_i), 200, clr(p_data(p_max_log_lik_i, :, 1), :), 'LineWidth', 2, 'Marker', 'x');

                
                dist_max = max(data.data(ismember(time_days, alldays)));
                
                if isempty(dist_max)
                    dist_max = 1;
                end
                
                %figure
                a = [0 1 0 dist_max];
                subplot(7, 7, (j * 7) + (1:6));
                %[num, name] = weekday(thisday, 'short')
                stairs(data.time(day_i) - thisday, data.data(day_i), 'LineWidth', 2);
                hold on;
                scatter(data.time(day_i) - thisday, data.data(day_i), 200, clr(p_data(p_max_log_lik_i, :, 2) + 1, :), 'LineWidth', 2, 'Marker', 'x');
                axis(a);
                if j == 0
                    title(['Week Beginning ' datestr(firstday, 'dd-mmm-yyyy')], 'FontSize', 30);
                end
                ylabel(datestr(thisday, 'ddd'), 'FontSize', 30);
                set(gca,'XTick', (0:3:24)/24)
                if j == 6
                    set(gca,'XTickLabel', datestr((0:3:24)/24, 'hh:00'))
                else
                    set(gca,'XTickLabel', [])
                end
                set(gca,'FontSize', 30)
                
                subplot(7, 7, (j * 7) + 7);
                scatter(location_data(day_i, 1), location_data(day_i, 2), 100 * (logical(p_data(p_max_log_lik_i, :, 1) ~= 1) + 1), clr(p_data(p_max_log_lik_i, :, 2) + 1, :), 'LineWidth', 2, 'Marker', 'x');
            end
            
            p = get(gcf, 'Position');
            p(3) = 3 * p(3);
            p(4) = 2 * p(4);
            set(gcf, 'Position', p);

            p = get(gcf, 'PaperPosition');
            p(3) = 3 * p(3);
            p(4) = 2 * p(4);
            set(gcf, 'PaperPosition', p);
            
            fprintf('  Saving : ');
            tic;
            wb_str = datestr(firstday, 'yy-mm-dd');
            save_figure([participant '-location-wb-' wb_str], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
            toc;
            
            close;
        end
        %%
        
        if ~isempty(data.data)
            figure
            stairs(data.time, data.data);
        end
        
        %%
        % In case there is overlap between different data files. Usually
        % this would not be required.
        fprintf('  Downsampling by second : ');
        tic; location_ds = downsample_second(data); toc;
        %%
        fprintf('  Downsampling by epoch : ');
        tic; location_ds2 = downsample_epoch(location_ds, 20, 0); toc;

        fprintf('  Interleaving : ');
        tic; location_ds3 = interleave_epoch(location_ds2, varargin{:}); toc;

        %%
        fprintf('  Plotting : ');
        tic; plot_location(location_ds3, varargin{:}); toc;
        %%
        fprintf('  Saving : ');
        tic;
        save_figure([participant '-location'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        save_data([participant '-location'], location_ds3, 'root_path_type', 'output_path', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
        %%
        fprintf('  Downsampling (simple) : ');
        tic; location_ds_simple = downsample_epoch(location_ds, 30, 0); toc;
        fprintf('  Interleaving (simple) : ');
        tic; location_ds_simple3 = interleave_epoch(location_ds_simple); toc;
        
        fprintf('  Plotting (simple) : ');
        tic; plot_location_simple(location_ds_simple3, varargin{:}); toc;
        %%
        fprintf('  Saving (simple) : ');
        tic;
        save_figure([participant '-location-simple'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        save_data([participant '-location-simple'], location_ds_simple3, 'root_path_type', 'output_path', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
    else
        fprintf('  No data loaded\n');
    end
end