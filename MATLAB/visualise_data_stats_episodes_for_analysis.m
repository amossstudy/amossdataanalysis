function visualise_data_stats_episodes_for_analysis(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 30-Apr-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-stats';
    
    files = get_participants('classification_filter', 1, varargin{:});
    
    episodes_types = get_configuration('episode_types');
    
    episode_lengths = struct();
    episode_count_raw = struct();
    episode_count_selected = struct();
    
    for episode_cell = fieldnames(episodes_types)'
        episode_lengths.(episode_cell{:}) = NaN(length(files), 1000);
        episode_count_raw.(episode_cell{:}) = zeros(length(files), 1);
        episode_count_selected.(episode_cell{:}) = zeros(length(files), 1);
    end
    
    for i=1:length(files)
        file = files(i);

        fprintf('Loading %s\n', file.name)

        class = get_participant_property(file.name, 'classification', varargin{:});

        if ~isempty(class)
            episodes = load_episodes_for_analysis(file.name);

            if ~isempty(episodes)
                for episode_cell = fieldnames(episodes_types)'
                    if ~isfield(episodes, episode_cell{:})
                        continue;
                    end
                    
                    episode_diff = diff(episodes.(episode_cell{:}));
                    episode_start = episodes.time([0; episode_diff] > 0);
                    episode_end = episodes.time([episode_diff; 0] < 0);
                    if (episodes.(episode_cell{:})(1) == true)
                        episode_start = [episodes.time(1); episode_start]; %#ok<AGROW>
                    end
                    if (episodes.(episode_cell{:})(end) == true)
                        episode_end = [episode_end; episodes.time(end)]; %#ok<AGROW>
                    end
                    episode_lengths.(episode_cell{:})(i, 1:length(episode_start)) = (episode_end - episode_start + 1);
                    episode_count_raw.(episode_cell{:})(i) = length(episode_start);
                end
                for episode = episodes.epochs
                    episode_count_selected.(episode.type)(i) = episode_count_selected.(episode.type)(i) + 1;
                end
            end
        end
    end

    for episode_cell = fieldnames(episodes_types)'
        episode = episodes_types.(episode_cell{:});
        figure;
        histogram(episode_lengths.(episode_cell{:})(:) / 7, 0.5:1:60);
        title(['Episode Length (' episode.display_name ')']);
        xlabel(['Episode Length (' episode.display_name ')']);
        set(gca, 'ylim', [0, 6]);

        save_figure(['episode-for-analysis-length-' episode_cell{:}], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    end
    
    fprintf('Episodes extracted:\n')
    
    total_episodes = 0;
    total = 0;
    
    for episode_cell = fieldnames(episodes_types)'
        episode = episodes_types.(episode_cell{:});
        episode_count = nansum(episode_count_raw.(episode_cell{:}));
        fprintf('  %s: %i\n', episode.display_name, episode_count);
        total = total + episode_count;
        if ~strcmp(episode_cell{:}, 'euthymic')
            total_episodes = total_episodes + episode_count;
        end
    end
    
    fprintf('  Total episodes: %i\n', total_episodes);
    fprintf('  Total: %i\n', total);
    
    fprintf('Epochs selected for analysis:\n')
    
    total_episodes = 0;
    total = 0;
    
    for episode_cell = fieldnames(episodes_types)'
        episode = episodes_types.(episode_cell{:});
        episode_count = sum(episode_count_selected.(episode_cell{:}));
        fprintf('  %s: %i\n', episode.display_name, episode_count);
        total = total + episode_count;
        if ~strcmp(episode_cell{:}, 'euthymic')
            total_episodes = total_episodes + episode_count;
        end
    end
    
    fprintf('  Total episodes: %i\n', total_episodes);
    fprintf('  Total: %i\n', total);
end
