function plot_location_multi_weekly(data, period_beginning, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 30-Jan-2015

    show_battery = get_argument({'show_battery', 'showbattery'}, false, varargin{:});
    if show_battery
        battery_data = get_argument({'battery_data', 'batterydata'}, [], varargin{:});
        power_on_time = get_argument({'power_on_time', 'powerontime'}, [], varargin{:});
        power_off_time = get_argument({'power_off_time', 'powerofftime'}, [], varargin{:});
    end
    upgrade_time = get_argument({'upgrade_time', 'upgradetime'}, [], varargin{:});
    
    time_days = floor(data.time(data.time >= datenum([2014, 01, 01])));
    
    [~, ~, locations_idx] = unique([data.lat, data.lon], 'rows');
    
    if isfield(data, 'location_group')
        location_annotations = data.location_group;
        clr = lines(max(location_annotations) + 1);
        clr(1, :) = [0 0 0];
    else
        location_annotations = zeros(size(data.data));
        clr = [0 0 0];
    end
    
    title_str = ['Period Beginning ' datestr(period_beginning, 'dd-mmm-yyyy')];
    
    for j = 0:7
        firstday = period_beginning + (j * 7);
        thisweek = firstday + (0:6);
        
        if isempty(ismember(time_days, thisweek)) || isempty(isnan(data.data))
            week_data_idx = [];
        else
            week_data_idx = (ismember(time_days, thisweek) & ~isnan(data.data));
        end
        week_locations_idx = locations_idx(week_data_idx, :);
        week_locations_annotation = location_annotations(week_data_idx);
        
        subplot(8, 7, (j * 7) + (1:7));
        cla;
        
        hold on;
        if show_battery && ~isempty(battery_data)
            battery_valid = (battery_data.time >= firstday) & (battery_data.time < (firstday + 7));
            if sum(battery_valid) > 2
                battery_time_valid = battery_data.time(battery_valid);
                battery_data_valid = battery_data.data(battery_valid);
                dt = 1/(24 * 4);
                battery_time_resampled = [(firstday - dt) firstday (firstday + (dt / 2)):dt:(firstday + 7 - (dt / 2)) (firstday + 7) (firstday + 7 + dt)];
                battery_data_resampled = zeros(size(battery_time_resampled));
                for i = 2:(length(battery_time_resampled) - 1)
                    resampling_points = (battery_time_valid >= (battery_time_resampled(i) - (dt / 2))) & (battery_time_valid < (battery_time_resampled(i) + (dt / 2)));
                    if sum(resampling_points) > 0
                        battery_data_resampled(i) = mean(battery_data_valid(resampling_points));
                    end
                end
                [xx, yy] = stairs(battery_time_resampled - firstday, battery_data_resampled * 10);
                patch(xx, yy, [0.75 1 1], 'LineStyle', 'none');
            end
        end
        if show_battery && ~isempty(power_off_time)
            power_off_time_valid = (power_off_time >= firstday) & (power_off_time < (firstday + 7));
            if sum(power_off_time_valid) > 0
                for t = find(power_off_time_valid)'
                    plot([power_off_time(t) power_off_time(t)] - firstday, [-0.5 10.5], 'color', [0.5 0 0]);
                end
            end
        end
        if show_battery && ~isempty(power_on_time)
            power_on_time_valid = (power_on_time >= firstday) & (power_on_time < (firstday + 7));
            if sum(power_on_time_valid) > 0
                for t = find(power_on_time_valid)'
                    plot([power_on_time(t) power_on_time(t)] - firstday, [-0.5 10.5], 'color', [0 0.5 0]);
                end
            end
        end
        if ~isempty(upgrade_time)
            upgrade_time_valid = (upgrade_time >= firstday) & (upgrade_time < (firstday + 7));
            if sum(upgrade_time_valid) > 0
                for t = find(upgrade_time_valid)'
                    plot([upgrade_time(t) upgrade_time(t)] - firstday, [-0.5 10.5], 'color', [0.5 0.2 0.5], 'LineWidth', 4);
                end
            end
        end
        plot([0 7], [0 0], 'color', [0.5 0.5 0.5]);
        plot([0 7], [5 5], 'color', [0.5 0.5 0.5]);
        plot([0 7], [10 10], 'color', [0.5 0.5 0.5]);
        stairs(data.time(week_data_idx) - firstday, min(data.data(week_data_idx), 10.5), 'LineWidth', 2, 'Color', lines(1));
        scatter(data.time(week_data_idx) - firstday, min(data.data(week_data_idx), 10.5), 10, clr(week_locations_annotation + 1, :), 'LineWidth', 1, 'Marker', 'x');
        %dist_max = max(data.data(week_data_idx));
        %dist_min = min(data.data(week_data_idx));
        %if isempty(dist_max)
        %    dist_max = 0;
        %    dist_min = 0;
        %end
        %a = [0 7 max((dist_min - 0.5), 0) (dist_max + 0.5)];
        a = [0 7 -0.5 10.5];
        axis(a);
        axis manual;
        if j == 0
            title(title_str, 'FontSize', 30);
        end
        if (j == 0) || (day(firstday) < day(firstday - 7))
            ylabel(datestr(firstday, 'dd-mmm'), 'FontSize', 24);
        else
            ylabel(datestr(firstday, 'dd-mmm'), 'FontSize', 24);
        end
        set(gca,'XTick', 0:7)
        if j == 7
            set(gca,'XTickLabel', datestr(thisweek, 'ddd'))
        else
            set(gca,'XTickLabel', [])
        end
        set(gca,'FontSize', 30)
        hold off;
    end
    
    p = get(gcf, 'Position');
    p(3) = 3 * p(3);
    p(4) = 3 * p(4);
    set(gcf, 'Position', p);
    
    p = get(gcf, 'PaperPosition');
    p(3) = 3 * p(3);
    p(4) = 3 * p(4);
    set(gcf, 'PaperPosition', p);
end
