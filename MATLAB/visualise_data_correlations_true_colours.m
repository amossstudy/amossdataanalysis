function visualise_data_correlations_true_colours(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Mar-2015

    close all;

    OUTPUT_SUB_DIR = 'data-correlations';

    files = get_participants(varargin{:});

    data_dates = [2014, 1, 1; 2015, 3, 1];

    questionaires = {'ALTMAN', 'GAD-7', 'QIDS'};

    data_class = cell(3, 1);
    for i = 1:3
        data_class{i} = struct();
        for j = 1:numel(questionaires);
            q1_str = questionaires{j};
            q1 = strrep(q1_str, '-', '_');
            for k = j:numel(questionaires);
                if j == k
                    data_class{i}.(q1) = struct('data', [], 'j', 1);
                else
                    q2_str = questionaires{k};
                    q2 = strrep(q2_str, '-', '_');

                    data_class{i}.([q1 '_' q2]) = struct('data', [], 'j', 1);
                end
            end
        end
    end

    for file = files'
        fprintf('Loading %s\n', file.name)

        class = get_participant_property(file.name, 'classification', varargin{:});

        if ~isempty(class)
            data_tc = load_true_colours_preprocessed(file.name);

            if ~isempty(data_tc)
                for j = 1:numel(questionaires);
                    q1_str = questionaires{j};
                    q1 = strrep(q1_str, '-', '_');

                    if isfield(data_tc, q1) && isfield(data_tc.(q1), 'questions')
                        q1_data = data_tc.(q1).questions;
                        q1_dates = floor(data_tc.(q1).time);
                        q1_dates_unique = sort(unique(q1_dates));

                        for k = j:numel(questionaires);
                            if j == k
                                if isempty(data_class{class + 1}.(q1).data) && ~isempty(q1_data)
                                    data_class{class + 1}.(q1).data = NaN(length(files) * (datenum(data_dates(2,:)) - datenum(data_dates(1,:))) * 2, size(q1_data, 2));
                                end

                                this_j = data_class{class + 1}.(q1).j;

                                data_class{class + 1}.(q1).data(this_j:(this_j + size(q1_data, 1) - 1), :) = q1_data;

                                data_class{class + 1}.(q1).j = this_j + size(q1_data, 1);
                            else
                                q2_str = questionaires{k};
                                q2 = strrep(q2_str, '-', '_');

                                if isfield(data_tc, q2) && isfield(data_tc.(q2), 'questions')
                                    q2_data = data_tc.(q2).questions;
                                    q2_dates = floor(data_tc.(q2).time);
                                    q2_dates_unique = sort(unique(q2_dates));

                                    if isempty(data_class{class + 1}.([q1 '_' q2]).data) && ~isempty(q1_data) && ~isempty(q2_data)
                                        data_class{class + 1}.([q1 '_' q2]).data = NaN(length(files) * (datenum(data_dates(2,:)) - datenum(data_dates(1,:))) * 2, size(q1_data, 2) + size(q2_data, 2));
                                    end

                                    dates_intersection = q2_dates_unique(ismember(q2_dates_unique, q1_dates_unique));

                                    dates_intersection_q1_idx = ismember(q1_dates, dates_intersection);
                                    dates_intersection_q2_idx = ismember(q2_dates, dates_intersection);

                                    dates_intersection_q1_data = q1_data(dates_intersection_q1_idx & (diff([q1_dates; (q1_dates(end) + 1)]) > 0), :);
                                    dates_intersection_q2_data = q2_data(dates_intersection_q2_idx & (diff([q2_dates; (q2_dates(end) + 1)]) > 0), :);

                                    assert(size(dates_intersection_q1_data, 1) == size(dates_intersection_q2_data, 1));

                                    this_j = data_class{class + 1}.([q1 '_' q2]).j;

                                    data_class{class + 1}.([q1 '_' q2]).data(this_j:(this_j + size(dates_intersection_q1_data, 1) - 1), :) = [dates_intersection_q1_data dates_intersection_q2_data];

                                    data_class{class + 1}.([q1 '_' q2]).j = this_j + size(dates_intersection_q1_data, 1);
                                end
                            end
                        end
                    end
                end
            end
        end
    end

    group_labels = {'Combined', 'HC', 'BD', 'BPD'};

    data_combined = struct();

    for i = 1:3
        for j = 1:numel(questionaires);
            q1_str = questionaires{j};
            q1 = strrep(q1_str, '-', '_');
            for k = j:numel(questionaires);
                q2_str = questionaires{k};
                q2 = strrep(q2_str, '-', '_');
                if j == k
                    if ~isfield(data_combined, q1)
                        data_combined.(q1) = struct('data', NaN(0, size(data_class{i}.(q1).data, 2)), 'total', 0);
                    end
                    data_combined.(q1).total = data_combined.(q1).total + data_class{i}.(q1).j - 1;
                else
                    if ~isfield(data_combined, [q1 '_' q2])
                        data_combined.([q1 '_' q2]) = struct('data', NaN(0, size(data_class{i}.([q1 '_' q2]).data, 2)), 'total', 0);
                    end
                    data_combined.([q1 '_' q2]).total = data_combined.([q1 '_' q2]).total + data_class{i}.([q1 '_' q2]).j - 1;
                end
            end
        end
    end

    for q = fieldnames(data_combined)'
        data_combined.(q{1}).data = NaN(data_combined.(q{1}).total, size(data_combined.(q{1}).data, 2));

        j = 1;

        for i = 1:3
            data_combined.(q{1}).data(j:(j + data_class{i}.(q{1}).j - 2), :) = data_class{i}.(q{1}).data(1:(data_class{i}.(q{1}).j - 1), :);
            j = j + data_class{i}.(q{1}).j - 1;
        end
    end

    for i = 0:3
        for j = 1:numel(questionaires);
            q1_str = questionaires{j};
            q1 = strrep(q1_str, '-', '_');
            
            if i == 0
                q1_data = data_combined.(q1).data;
            else
                q1_data = data_class{i}.(q1).data;
            end
            q1_questions = size(q1_data, 2);
            q1_ticks = cell(q1_questions, 1);
            for q = 1:q1_questions
                q1_ticks{q} = sprintf('%i', q);
            end
            
            for k = j:numel(questionaires);
                if j == k
                    corr_str = q1_str;
                    corr_ticks = q1_ticks;
                    
                    if i == 0
                        data_matrix = data_combined.(q1).data;
                        data_j = data_combined.(q1).total + 1;
                    else
                        data_matrix = data_class{i}.(q1).data;
                        data_j = data_class{i}.(q1).j;
                    end
                else
                    q2_str = questionaires{k};
                    q2 = strrep(q2_str, '-', '_');

                    if i == 0
                        q2_data = data_combined.(q2).data;
                    else
                        q2_data = data_class{i}.(q2).data;
                    end
                    q2_questions = size(q2_data, 2);
                    q2_ticks = cell(q2_questions, 1);
                    for q = 1:q2_questions
                        q2_ticks{q} = sprintf('%i', q);
                    end

                    corr_str = [q1_str '/' q2_str];
                    corr_ticks = [q1_ticks; q2_ticks];
                    
                    if i == 0
                        data_matrix = data_combined.([q1 '_' q2]).data;
                        data_j = data_combined.([q1 '_' q2]).total + 1;
                    else
                        data_matrix = data_class{i}.([q1 '_' q2]).data;
                        data_j = data_class{i}.([q1 '_' q2]).j;
                    end
                end
        
                data_matrix = data_matrix(1:(data_j - 1), :);
                data_matrix = data_matrix(sum(isnan(data_matrix), 2) == 0, :);

                corr_matrix = corr(data_matrix, 'Type', 'Spearman');

                corr_matrix(logical(eye(length(corr_matrix)))) = zeros(length(corr_matrix), 1);

                figure;

                imagesc(corr_matrix);

%                 for i_x = 1:length(corr_matrix)
%                     for i_y = 1:length(corr_matrix)
%                         if i_x ~= i_y
%                             text(i_x, i_y, num2str(round(corr_matrix(i_x, i_y) * 10000) / 10000, 2), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FontSize', 16)
%                         end
%                     end
%                 end

                if j ~= k
                    hold on;
                    plot([q1_questions q1_questions] + 0.5, [0.5, (length(corr_matrix) + 0.5)], 'k');
                    plot([0.5, (length(corr_matrix) + 0.5)], [q1_questions q1_questions] + 0.5, 'k');
                    hold off;
                end

                set(gca, 'ytick', 1:length(corr_matrix));
                set(gca, 'xtick', 1:length(corr_matrix));
                set(gca, 'yticklabel', corr_ticks);
                set(gca, 'xticklabel', corr_ticks);

                xlabel([corr_str ' Question']);
                ylabel([corr_str ' Question']);

                clr = [spring(64); flipud(autumn(64))];
                clr_val = linspace(1, -1, size(clr, 1));

                t = tinv(1 - (0.01 / 2), size(data_matrix, 1) - 2);
                r_sig = t / sqrt(size(data_matrix, 1) - 2 + (t ^ 2));

                clr(abs(clr_val) < r_sig, :) = repmat(clr(size(clr, 1) / 2, :), sum(abs(clr_val) < r_sig), 1);

                set(gca, 'clim', [-1, 1])

                colormap(clr);

                colorbar;

                title([corr_str ' Correlations (' group_labels{i + 1} ')']);

                save_figure(['true_colours_correlations_' lower(strrep(corr_str, '/', '_')) '_' group_labels{i + 1}], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
            end
        end
    end
end
