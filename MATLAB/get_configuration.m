function value = get_configuration(configuration_name, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 05-May-2015

    value = [];
    
    
    switch lower(configuration_name)
        case 'cohorts'
            clr = lines(7);
            
            value = struct();
            
            value.cohort_name = {'HC', 'BD', 'BPD'};
            value.cohort_clr = clr([2 4 5], :);
        case 'episode_types'
            clr = lines(7);
            
            value = struct();
            value.euthymic  = struct('id', 0, 'display_name', 'Euthymic',  'display_name_short', 'Euth.', 'line_colour', clr(3, :), 'episode_colour', min(1, clr(3, :) + 0.6), 'epoch_colour', min(1, clr(3, :) + 0.2));
            value.depressed = struct('id', 1, 'display_name', 'Depressed', 'display_name_short', 'Dep.',  'line_colour', clr(7, :), 'episode_colour', min(1, clr(7, :) + 0.6), 'epoch_colour', min(1, clr(7, :) + 0.2));
            value.manic     = struct('id', 2, 'display_name', 'Manic',     'display_name_short', 'Manic', 'line_colour', clr(4, :), 'episode_colour', min(1, clr(4, :) + 0.5), 'epoch_colour', min(1, clr(4, :) + 0.2));
            value.mixed     = struct('id', 3, 'display_name', 'Mixed',     'display_name_short', 'Mixed', 'line_colour', clr(1, :), 'episode_colour', min(1, clr(1, :) + 0.6), 'epoch_colour', min(1, clr(1, :) + 0.2));
        case 'questionaires'
            clr = lines(7);
            
            value = struct();
            value.ALTMAN = struct('id', 0, 'display_name', 'ALTMAN', 'line_colour', clr(4, :), 'min', 0, 'max', 20);
            value.GAD_7  = struct('id', 1, 'display_name', 'GAD-7',  'line_colour', clr(1, :), 'min', 0, 'max', 21);
            value.QIDS   = struct('id', 2, 'display_name', 'QIDS',   'line_colour', clr(7, :), 'min', 0, 'max', 27);
            value.EQ_5D  = struct('id', 3, 'display_name', 'EQ-5D',  'line_colour', clr(2, :), 'min', 0, 'max', 100);
        case {'features_location', 'features_location_v1'}
            value = struct();
            
            value.feature_title = {'Mean % of day recorded > 5km from home', ...
                                   'Mean % of day recorded > 5km from home', ...
                                   'Mean % of day recorded > 20km from home', ...
                                   'Mean % of day recorded > 20km from home', ...
                                   'Variance of % of day recorded > 5km from home', ...
                                   'Variance of % of day recorded > 5km from home', ...
                                   'Mean number of locations visited', ...
                                   'Mean number of locations visited', ...
                                   'Variance of locations visited', ...
                                   'Variance of locations visited', ...
                                   'Mean % of days stay home', ...
                                   'Mean % of days stay home', ...
                                   'Variance of % of days stay home', ...
                                   'Variance of % of days stay home', ...
                                   'Variance of time leaving home', ...
                                   'Variance of time leaving home', ...
                                   'MSE', 'MSE', 'MSE', ...
                                   'DFA', 'DFA', 'DFA'};
            
            value.feature_title_full = value.feature_title;
            value.feature_title_full(17:19) = {'Multi Scale Entropy Analysis'};
            value.feature_title_full(20:22) = {'Detrended Fluctuation Analysis'};
            
            value.feature_subset = {'Monday - Friday', ...
                                    'at weekends', ...
                                    'Monday - Friday', ...
                                    'at weekends', ...
                                    'Monday - Friday', ...
                                    'at weekends', ...
                                    'Monday - Friday', ...
                                    'at weekends', ...
                                    'Monday - Friday', ...
                                    'at weekends', ...
                                    'Monday - Friday', ...
                                    'at weekends', ...
                                    'Monday - Friday', ...
                                    'at weekends', ...
                                    'Monday - Friday', ...
                                    'at weekends', ...
                                    'area under trend', ...
                                    'variance of noise from trend', ...
                                    'last non-zero scale', ...
                                    'feature 1', ...
                                    'feature 2', ...
                                    'feature 3'};
            
            value.feature_filename = {'mean_5km', 'mean_5km', ...
                                   'mean_20km', 'mean_20km', ...
                                   'var_5km', 'var_5km', ...
                                   'mean_locations', 'mean_locations', ...
                                   'var_locations', 'var_locations', ...
                                   'mean_stay_home', 'mean_stay_home', ...
                                   'var_stay_home', 'var_stay_home', ...
                                   'var_leave_home', 'var_leave_home', ...
                                   'mse', 'mse', 'mse', ...
                                   'dfa', 'dfa', 'dfa'};
            
            value.feature_subset_filename = {'weekdays', 'weekends', ...
                                    'weekdays', 'weekends', ...
                                    'weekdays', 'weekends', ...
                                    'weekdays', 'weekends', ...
                                    'weekdays', 'weekends', ...
                                    'weekdays', 'weekends', ...
                                    'weekdays', 'weekends', ...
                                    'weekdays', 'weekends', ...
                                    'area_under_trend', 'variance_from_trend', 'max_scale', ...
                                    'f1', 'f2', 'f3'};
            
            value.feature_short = {'MEAN_WD5K', 'MEAN_WE5K', ...      1, 2
                                   'MEAN_WD20K', 'MEAN_WE20K', ...    3, 4
                                   'VAR_WD5K', 'VAR_WE5K', ...        5, 6
                                   'MEAN_WDLOCS', 'MEAN_WELOCS', ...  7, 8
                                   'VAR_WDLOCS', 'VAR_WELOCS', ...    9, 10
                                   'MEAN_WDSH', 'MEAN_WESH', ...      11, 12
                                   'VAR_WDSH', 'VAR_WESH', ...        13, 14
                                   'VAR_WDLH', 'VAR_WELH', ...        15, 16
                                   'MSE_AREA', 'MSE_TRENDVAR', 'MSE_MAXSCALE', ... 17, 18, 19
                                   'DFA_TREND1', 'DFA_TREND2', 'DFA_TRENDVAR'};  % 20, 21, 22
            
            value.cohort_name = {'HC', 'BP', 'BPD'};
        case 'features_location_v2'
            clr = lines(7);
            
            value = struct();
            
            value.features = 10;
            
            value.asrm_threshold = 6;
            value.asrm_threshold_multi = [6 10];
            
            value.gad_7_threshold = 10;
            
            value.qids_threshold = 11;
            value.qids_threshold_multi = [6 11];
            
            feature_name_base = {'Location Variance', 'Number of Clusters', 'Entropy', ...
                                 'Normalized Entropy', 'Home Stay', 'Diurnal Movement', ...
                                 'Transition Time', 'Total Distance', 'Diurnal Movement Normalised', ...
                                 'Diurnal Movement Distance'};
            
            feature_short_base = {'LV', 'NC', 'ENT', 'NENT', 'HS', 'DM', 'TT', 'TD', 'DMN', 'DMD'};
            feature_order_base = [3 4 1 5 7 8 6 2 9 10];
            feature_display_order_base = [1 2 3 4 5 6 8 7 9 10];
            
            value.test_description = {'All', 'HC', 'BD', 'BPD', 'HC/BD', 'BD with HC', 'BD/BPD'};
            value.test_description_filename = lower(value.test_description);
            value.test_description_filename = strrep(value.test_description_filename, '/', '_');
            value.test_description_filename = strrep(value.test_description_filename, ' ', '_');
            
            value.employment_status_id = 0:7;
            value.employment_status_description = {'Unknown', 'F/T Employed', 'P/T Employed', ...
                                                   'Unemployed', 'Student', 'Student & P/T', ...
                                                   'Voluntary Work', 'Retired'};
            
            value.clr_cohort = clr([1 2 4 5 7 6 3], :);
            value.clr_qids = clr(1:2, :);
            value.clr_qids_boundary = clr(3, :);
            value.clr_data_subset = clr([2 4 5 1 3], :);
            
            value.symbol_cohort = {'v', 's', '^', 'd', 'o', 'v', '<'};
            
            value.feature_base = {};
            value.feature_base_short = {};
            value.feature_data_subset = {};
            value.feature_data_subset_short = {};
            value.feature_name = {};
            value.feature_short = {};
            value.feature_order = [];
            value.feature_display_order = [];
            value.data_subset_name = {'Base', 'Weekday', 'Weekend', 'Median', 'Optimised'};
            value.data_subset_name_short = {'', 'WD', 'WE', 'MEDIAN', 'OPTIMISED'};
            
            for i = 0:4
                value.feature_order = [value.feature_order (feature_order_base + (i * value.features))];
                value.feature_display_order = [value.feature_display_order (feature_display_order_base + (i * value.features))];
                
                feature_name = feature_name_base;
                feature_short = feature_short_base;
                
                value.feature_base = [value.feature_name feature_name];
                value.feature_base_short = [value.feature_base_short feature_short];
                value.feature_data_subset = [value.feature_data_subset repmat(value.data_subset_name(i + 1), 1, numel(feature_name))];
                value.feature_data_subset_short = [value.feature_data_subset_short repmat(value.data_subset_name_short(i + 1), 1, numel(feature_name))];
                
                switch i
                    case 1
                        feature_name = strcat(feature_name, ' Weekday');
                        feature_short = strcat(feature_short, ' WD');
                    case 2
                        feature_name = strcat(feature_name, ' Weekend');
                        feature_short = strcat(feature_short, ' WE');
                    case 3
                        feature_name = strcat(feature_name, ' Median');
                        feature_short = strcat(feature_short, ' MEDIAN');
                    case 4
                        feature_name = strcat(feature_name, ' Optimised');
                        feature_short = strcat(feature_short, ' OPTIMISED');
                end
                
                value.feature_name = [value.feature_name feature_name];
                value.feature_short = [value.feature_short feature_short];
            end
            
            value.cohort_classification = struct;
            value.cohort_classification.hc_vs_bd_bpd = struct();
            value.cohort_classification.hc_vs_bd_bpd.classes = {0, [1 2]};
            value.cohort_classification.hc_vs_bd_bpd.names = {'HC', 'BD / BPD'};
            value.cohort_classification.hc_vs_bd_bpd.clr_idx = [2, 7];
            value.cohort_classification.hc_vs_bd_vs_bpd = struct();
            value.cohort_classification.hc_vs_bd_vs_bpd.classes = {0, 1, 2};
            value.cohort_classification.hc_vs_bd_vs_bpd.names = {'HC', 'BD', 'BPD'};
            value.cohort_classification.hc_vs_bd_vs_bpd.clr_idx = [2, 3, 4];
            value.cohort_classification.hc_vs_bd = struct();
            value.cohort_classification.hc_vs_bd.classes = {0, 1};
            value.cohort_classification.hc_vs_bd.names = {'HC', 'BD'};
            value.cohort_classification.hc_vs_bd.clr_idx = [2, 3];
    end
end
