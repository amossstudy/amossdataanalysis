function extract_features_location_dfa(participant, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 27-Dec-2014

    FEATURES = 3;
    
    features = load_location_features(participant, varargin{:});
    
    features.dfa = NaN(FEATURES, 1);
    
    dfa = load_location_dfa(participant);
    
    if isstruct(dfa)
        epoch_count = 0;
        
        if isfield(features, 'episodes') && isfield(features.episodes, 'start')
            epoch_count = size(features.episodes.start, 2);
        end
        
        features.episodes.dfa = NaN(FEATURES, epoch_count);
        features.episodes.dfa_is_valid = false(epoch_count, 1);
        
        for j = 0:epoch_count
            this_dfa_range = [];
            this_dfa_result = [];
            
            if (j == 0) && isfield(dfa, 'range') && ~isempty(dfa.range) && isfield(dfa, 'result') && ~isempty(dfa.result)
                this_dfa_range = dfa.range;
                this_dfa_result = dfa.result;
            elseif (j > 0) && isfield(dfa, 'episodes') && isfield(dfa.episodes, 'result') && ~isempty(dfa.episodes.result)
                for k = 1:epoch_count
                    if (features.episodes.start(j) == dfa.episodes.start(k)) && (features.episodes.end(j) == dfa.episodes.end(k))
                        this_dfa_range = dfa.episodes.range;
                        this_dfa_result = dfa.episodes.result(:, k);
                        break;
                    end
                end
            end
            
            if isempty(this_dfa_result) || isempty(this_dfa_range)
                continue;
            end
            
            features_dfa = NaN(FEATURES, 1);
            
            i_1 = (this_dfa_range >= 10) & (this_dfa_range < 500);
            i_2 = (this_dfa_range >= 500) & (this_dfa_range < 3500);

            polycoef_1 = polyfit(log(this_dfa_range(i_1)), log(this_dfa_result(i_1)), 1);
            polycoef_2 = polyfit(log(this_dfa_range(i_2)), log(this_dfa_result(i_2)), 1);

            features_dfa(1) = polycoef_1(1);
            features_dfa(2) = polycoef_2(1);

            dfa_trend = [polyval(polycoef_1, log(this_dfa_range(i_1))); polyval(polycoef_2, log(this_dfa_range(i_2)))];
            dfa_trend_diff = log(this_dfa_result(i_1 | i_2)) - dfa_trend;

            features_dfa(3) = var(dfa_trend_diff);
            
            if (j == 0)
                features.dfa = features_dfa;
                features.dfa_is_valid = true;
            else
                features.episodes.dfa(:, j) = features_dfa;
                features.episodes.dfa_is_valid(j) = true;
            end
        end
    else
        fprintf('  No data loaded\n');
    end
    
    save_data([participant '-location-features'], features, varargin{:});
end
