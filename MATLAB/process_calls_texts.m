function data_processed = process_calls_texts(data)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    t_vec = floor(datevec(data.time));
    t_vec(:,5) = 0;
    t_vec(:,6) = 0;
    t = datenum(t_vec);
    
    t_vec_uniq = unique(t_vec, 'rows');
    t_uniq = datenum(t_vec_uniq);
    
    id_unique = unique(data.id);
    direction_lookup = ['I'; 'O'];
    
    data_temp = zeros(size(t_vec_uniq, 1), length(id_unique), 2);
    
    j = 1;
    
    for i=1:size(t_uniq, 1)
        assert(t(j) == t_uniq(i,:))
        
        while (t(j) == t_uniq(i,:))
            i_id = strmatch(data.id(j), id_unique, 'exact');
            i_dir = find(direction_lookup == data.direction(j));
            data_temp(i, i_id, i_dir) = ...
                data_temp(i, i_id, i_dir) + data.length(i);
            
            j = j + 1;
            if j > size(t, 1)
                break;
            end
        end
    end
    
    data_processed.time = t_uniq;
    data_processed.data = data_temp;
    data_processed.epoch.min = 60;
    data_processed.epoch.sec = 0;
end