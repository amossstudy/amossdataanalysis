function [mean_by_time, mean_by_level, mean_pseudo] = calculate_battery_discharge_means(battery_discharges, discharge_cycles_idx)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 15-Jun-2015

    if nargin < 2
        discharge_cycles_idx = true(size(battery_discharges.start.time));
    end

    mean_by_time = struct();
    mean_by_time.level = 1:-0.01:0;
    mean_by_time.time = NaN(length(discharge_cycles_idx), length(mean_by_time.level));

    discharge_cycle_max_length = 0;
    for i = find(discharge_cycles_idx)'
        discharge_cycle_max_length = max(discharge_cycle_max_length, battery_discharges.cycles{i}.time(end));
    end

    mean_by_level = struct();
    mean_by_level.time_diff = 0.05;
    mean_by_level.time = 0:mean_by_level.time_diff:discharge_cycle_max_length;
    mean_by_level.level = NaN(length(discharge_cycles_idx), length(mean_by_level.time));
    mean_by_level.level(:, 1) = 1;

    for i = find(discharge_cycles_idx)'
        for j = 1:length(mean_by_time.level);
            idx = find(battery_discharges.cycles{i}.data == mean_by_time.level(j), 1, 'first');
            if ~isempty(idx)
                mean_by_time.time(i, j) = battery_discharges.cycles{i}.time(idx);
            end
        end
        for j = 2:length(mean_by_level.time)
            this_time = mean_by_level.time(j);
            this_time_min = this_time - (mean_by_level.time_diff / 2);
            this_time_max = this_time + (mean_by_level.time_diff / 2);
            this_time_valid_range = (battery_discharges.cycles{i}.time >= this_time_min) & (battery_discharges.cycles{i}.time < this_time_max);
            if sum(this_time_valid_range) > 0
                mean_by_level.level(i, j) = mean(battery_discharges.cycles{i}.data(this_time_valid_range));
            elseif sum(battery_discharges.cycles{i}.time >= this_time_min) == 0
                break;
            end
        end
    end

    mean_by_time.mean_time = nanmean(mean_by_time.time);
    mean_by_level.mean_level = nanmean(mean_by_level.level);

    mean_by_time.valid = struct();
    mean_by_level.valid = struct();

    mean_by_time.valid.level = mean_by_time.level(~isnan(mean_by_time.mean_time));
    mean_by_time.valid.mean_time = mean_by_time.mean_time(~isnan(mean_by_time.mean_time));

    mean_by_level.valid.time = mean_by_level.time(~isnan(mean_by_level.mean_level));
    mean_by_level.valid.mean_level = mean_by_level.mean_level(~isnan(mean_by_level.mean_level));

    mean_pseudo = struct();
    mean_pseudo.crossover = [];
    for i = 2:(length(mean_by_level.valid.time) - 1);
        xi1 = mean_by_level.valid.time(i - 1);
        xi2 = mean_by_level.valid.time(i);
        yi1 = mean_by_level.valid.mean_level(i - 1);
        yi2 = mean_by_level.valid.mean_level(i);
        ai = (yi2 - yi1) / (xi2 - xi1);
        bi = yi1 - (ai * xi1);

        for j = 2:(length(mean_by_time.valid.level) - 2)
            xj1 = mean_by_time.valid.mean_time(j - 1);
            xj2 = mean_by_time.valid.mean_time(j);

            if (min(xj1, xj2) < xi2) && (max(xj1, xj2) > xi1)
                yj1 = mean_by_time.valid.level(j - 1);
                yj2 = mean_by_time.valid.level(j);
                aj = (yj2 - yj1) / (xj2 - xj1);
                bj = yj1 - (aj * xj1);

                x_intercept = (bj - bi) / (ai - aj);

                if (x_intercept >= xi1) && (x_intercept < xi2) && ...
                        (x_intercept >= min(xj1, xj2)) && (x_intercept < max(xj1, xj2)) && ...
                        (x_intercept > 0) && ((ai > aj) || (aj >= 0))
                    y_intercept = (aj * x_intercept) + bj;

                    mean_pseudo.crossover = [x_intercept y_intercept];

                    if x_intercept > (2 / 24)
                        break;
                    end
                end
            end
        end
        if ~isempty(mean_pseudo.crossover) && (mean_pseudo.crossover(1) > (2 / 24))
            break;
        end
    end
    if ~isempty(mean_pseudo.crossover)
        mean_pseudo.a = (mean_pseudo.crossover(2) - 1) / (mean_pseudo.crossover(1) - 0);
        mean_pseudo.b = 1;
    else
        mean_pseudo.a = [];
        mean_pseudo.b = [];
    end
end
