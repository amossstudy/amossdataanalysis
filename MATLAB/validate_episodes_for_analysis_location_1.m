function validate_episodes_for_analysis_location_1(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 06-May-2015

% Criteria:
%   * Maximum 4 / 14 days missing
%   * Maximum 2 / 4 weekend days missing
%   * Must be recorded > 500m from home in each epoch

    files = get_participants(varargin{:});
    
    valid_epochs = NaN(length(files), 100);
    total_epochs = NaN(length(files), 100);
    
    episodes_types = get_configuration('episode_types');
    
    j = 1;
    
    for file = files'
        fprintf('Validating episodes for analysis for %s\n', file.name)
        
        episodes_for_analysis = load_episodes_for_analysis(file.name);
        
        if ~isempty(episodes_for_analysis) && isstruct(episodes_for_analysis) && ...
                isfield(episodes_for_analysis, 'epochs') && size(episodes_for_analysis.epochs, 2) > 0
            
            for i = 1:size(episodes_for_analysis.epochs, 2)
                episodes_for_analysis.epochs(i).valid = false;
                total_epochs(j, i) = episodes_types.(episodes_for_analysis.epochs(i).type).id;
            end
            
            data = load_location_preprocessed(file.name, varargin{:});

            if ~isempty(data) && isstruct(data) && ~isempty(fields(data))
                data_days = floor(data.validated.time);

                for i = 1:size(episodes_for_analysis.epochs, 2)
                    epoch_valid_points = (data.validated.time >= episodes_for_analysis.epochs(i).start) & (data.validated.time < (episodes_for_analysis.epochs(i).end + 1)) & ~isnan(data.validated.data);
                    
                    if sum(epoch_valid_points) > 0
                        epoch_range = episodes_for_analysis.epochs(i).start:episodes_for_analysis.epochs(i).end;
                        
                        if max(data.validated.data(epoch_valid_points)) > 0.5 % Exclude epoch if never recorded > 500m from home
                            valid_days = 0;
                            total_days = episodes_for_analysis.epochs(i).end - episodes_for_analysis.epochs(i).start + 1;

                            valid_weekend = 0;
                            total_weekend = sum(ismember(weekday(epoch_range), [1 7]));

                            for day = epoch_range
                                if sum((data_days == day) & epoch_valid_points) > 0
                                    valid_days = valid_days + 1;
                                    if ismember(weekday(day), [1 7])
                                        valid_weekend = valid_weekend + 1;
                                    end
                                end
                            end
                            if (valid_days / total_days) >= (10 / 14) && ...
                                    (valid_weekend / total_weekend) >= (2 / 4)
                                episodes_for_analysis.epochs(i).valid = true;
                                valid_epochs(j, i) = episodes_types.(episodes_for_analysis.epochs(i).type).id;
                            end
                        end
                    end
                end
            end

            save_data([file.name '-episodes-analysis'], episodes_for_analysis, varargin{:});
        else
            fprintf('  No data loaded\n');
        end
        
        j = j + 1;
    end
    
    total_epochs_per_participant = sum(~isnan(total_epochs), 2);
    total_epochs_per_participant = total_epochs_per_participant(total_epochs_per_participant > 0);
    
    fprintf('\nTotal participants: %i\n', size(total_epochs, 1));
    fprintf('Total participants with epochs: %i\n', size(total_epochs_per_participant, 1));
    fprintf('Total epochs: %i\n', sum(sum(~isnan(total_epochs))));
    fprintf('Total epochs per participant with epochs: %f (SD %f)\n', mean(total_epochs_per_participant), std(total_epochs_per_participant));
    fprintf('Total epoch types:\n');
    
    total_count = 0;
    total_episode_count = 0;
    
    for episode_cell = fieldnames(episodes_types)'
        episode = episodes_types.(episode_cell{:});
        episode_count = sum(sum(total_epochs == episode.id));
        fprintf('  %s: %i\n', episode.display_name, episode_count);
        total_count = total_count + episode_count;
        if ~strcmp(episode_cell{:}, 'euthymic')
            total_episode_count = total_episode_count + episode_count;
        end
    end
    
    fprintf('  Total episodes: %i\n', total_episode_count);
    fprintf('  Total: %i\n', total_count);
    
    valid_epochs_per_participant = sum(~isnan(valid_epochs), 2);
    valid_epochs_per_participant = valid_epochs_per_participant(valid_epochs_per_participant > 0);
    
    fprintf('\nValid participants with epochs: %i\n', size(valid_epochs_per_participant, 1));
    fprintf('Valid epochs: %i\n', sum(sum(~isnan(valid_epochs))));
    fprintf('Valid epochs per participant with epochs: %f (SD %f)\n', mean(valid_epochs_per_participant), std(valid_epochs_per_participant));
    fprintf('Valid epoch types:\n');
    
    total_count = 0;
    total_episode_count = 0;
    
    for episode_cell = fieldnames(episodes_types)'
        episode = episodes_types.(episode_cell{:});
        episode_count = sum(sum(valid_epochs == episode.id));
        fprintf('  %s: %i\n', episode.display_name, episode_count);
        total_count = total_count + episode_count;
        if ~strcmp(episode_cell{:}, 'euthymic')
            total_episode_count = total_episode_count + episode_count;
        end
    end
    
    fprintf('  Total episodes: %i\n', total_episode_count);
    fprintf('  Total: %i\n', total_count);
end