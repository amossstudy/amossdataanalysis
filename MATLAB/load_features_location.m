function [classification_data, classification_class] = load_features_location(varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 23-Dec-2014

    classification_data = [];
    classification_class = [];
    
    for file = get_participants(varargin{:})'
        features = load_location_features(file.name, varargin{:});

        class = get_participant_property(file.name, 'classification', varargin{:});

        if ~isempty(fieldnames(features)) && isfield(features, 'is_valid') && features.is_valid == 1 && ~isempty(features.class) && ~isempty(class)
            this_data = features.basic;
            classification_class = [classification_class; features.class]; %#ok<AGROW>

            if isfield(features, 'mse_is_valid') && features.mse_is_valid == 1
                this_data = [this_data; features.mse]; %#ok<AGROW>
            else
                this_data = [this_data; NaN(3, 1)]; %#ok<AGROW>
            end

            if isfield(features, 'dfa_is_valid') && features.dfa_is_valid == 1
                this_data = [this_data; features.dfa]; %#ok<AGROW>
            else
                this_data = [this_data; NaN(3, 1)]; %#ok<AGROW>
            end

            classification_data = [classification_data this_data]; %#ok<AGROW>
        end
    end
end
