function visualise_features_location_v2_raw(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 04-Nov-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats';
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    data_full = data_raw';
    
    TESTS = 6;
    FEATURES = size(data_full, 2);
    
    TESTS_TO_PERFORM = get_argument({'run_tests', 'runtests'}, 1:TESTS, varargin{:});
    
    valid_qids = ~isnan(classification_properties.QIDS);
    
    params = get_configuration('features_location_v2', varargin{:});
    
    save_figures = get_argument({'savefigure', 'save_figure', 'savefigures', 'save_figures'}, true, varargin{:});
    
    function prepare_and_save_figure(subdir, filename, varargin)
        save_figure(filename, 'output_sub_dir', [OUTPUT_SUB_DIR '/' subdir], 'FontSize', 50, varargin{:});
    end

    features_to_show_display_idx = 1:FEATURES;
    features_to_show_display_idx = get_argument({'features_to_show', 'featurestoshow', 'features_to_show_display_idx', 'featurestoshowdisplayidx'}, features_to_show_display_idx, varargin{:});
    
    features_to_show = params.feature_order(params.feature_display_order(features_to_show_display_idx));
    
    for t = TESTS_TO_PERFORM
        if t == 1
            test_classes = 0:2;
            
            test_valid_qids = valid_qids;
        elseif t == 5 ||  t == 6
            test_classes = 0:1;
            
            test_valid_qids = valid_qids & ismember(classification_properties.class, test_classes);
        else
            test_classes = t - 2;
            
            test_valid_qids = valid_qids & (classification_properties.class == test_classes);
        end
        
        this_test_description = params.test_description{t};
        this_test_description_filename = params.test_description_filename{t};
        
        regression_model_fun = @(a, b, x) a + (b * x);
        
        for i = features_to_show
            this_feature_name = params.feature_name{i};
            this_feature_name_file = strrep(lower(this_feature_name), ' ', '_');
            
            fprintf('  Processing %s, feature %.2i (%s)\n', this_test_description, i, params.feature_name{i});
            
            this_feature_valid_qids = test_valid_qids;
            non_nan = ~isnan(data_full(:, i));
            
            this_feature_data = data_full(this_feature_valid_qids & non_nan, i);
            
            this_feature_data_mean = mean(this_feature_data);
            this_feature_data_std = std(this_feature_data);
            
            outliers = false(size(this_feature_data));
            
            outliers(this_feature_data > (this_feature_data_mean + (5 * this_feature_data_std))) = true;
            outliers(this_feature_data < (this_feature_data_mean - (5 * this_feature_data_std))) = true;
            
            if sum(outliers) > 0
                fprintf('    Removing %i outliers\n', sum(outliers));

                this_feature_valid_qids(this_feature_valid_qids & non_nan) = ~outliers;
            end
            
            first_loop = true;
            show_classes_opts = false;
            if t == 6
                show_classes_opts = true;
            elseif numel(test_classes) > 1
                show_classes_opts = [false, true];
            end
            for show_classes = show_classes_opts
                this_feature_suffix = '';
                if show_classes
                    this_feature_suffix = '_classes';
                end
                regression_model_coef = NaN(numel(test_classes) + 1, 2);
                regression_model_rmse = NaN(numel(test_classes) + 1, 1);
                for ci = 0:numel(test_classes)
                    class_idx = this_feature_valid_qids;
                    if ci > 0
                        class_idx = class_idx & (classification_properties.class == test_classes(ci));
                    end
                    model_fitted = fit(data_full(class_idx & non_nan, i), classification_properties.QIDS(class_idx & non_nan), regression_model_fun, 'StartPoint', [0.5 0.5]);
                    regression_model_coef(ci + 1, 1) = model_fitted.a;
                    regression_model_coef(ci + 1, 2) = model_fitted.b;
                    regression_model_rmse(ci + 1) = rms(regression_model_fun(model_fitted.a, model_fitted.b, data_full(class_idx & non_nan, i)) - classification_properties.QIDS(class_idx & non_nan));
                end
                data_norm = data_full(this_feature_valid_qids & non_nan, i);
                data_norm = data_norm - mean(data_norm);
                data_norm = data_norm / std(data_norm);
                out_norm = classification_properties.QIDS(this_feature_valid_qids & non_nan);
                out_norm = out_norm - mean(out_norm);
                out_norm = out_norm / std(out_norm);
                [rho, p] = corr(data_norm, out_norm);
                limits = [];
                
                for show_dist = [false true]
                    f1 = figure;
                    if show_dist
                        h_title = subplot(4, 1, 1);

                        histogram_edges = linspace(limits(1), limits(2), 21);
                        
                        h = histogram(data_full(this_feature_valid_qids, i), histogram_edges);
                        
                        set(h, 'FaceColor', params.clr_cohort(t, :));
                        set(h, 'FaceAlpha', 1);

                        if show_classes && numel(test_classes) > 1
                            set(h, 'FaceColor', params.clr_cohort(test_classes(1) + 2, :));
                            hold on;
                            for ci = 2:numel(test_classes)
                                class_idx = this_feature_valid_qids & ismember(classification_properties.class, test_classes(ci:end));
                                
                                h = histogram(data_full(class_idx, i), histogram_edges);
                                
                                set(h, 'FaceColor', params.clr_cohort(test_classes(ci) + 2, :));
                                set(h, 'FaceAlpha', 1);
                            end
                            
                            h = histogram(data_full(this_feature_valid_qids, i), histogram_edges, 'DisplayStyle', 'stairs', 'EdgeColor', params.clr_cohort(t, :));
                        end
                        
                        set(h_title, 'ylim', [0 max(get(h, 'Values')) * 1.4]);
                        
                        set(h_title, 'xticklabel', []);
                        set(h_title, 'ytick', []);

                        this_feature_suffix = [this_feature_suffix '_dist']; %#ok<AGROW>#

                        box off;
                        subplot(4, 1, 2:4);
                    else
                        h_title = axes;
                        set(h_title, 'color', 'none');
                        set(h_title, 'xtick', []);
                        set(h_title, 'ytick', []);
                        set(h_title, 'xcolor', 'none');
                        set(h_title, 'ycolor', 'none');
                        axes;
                    end
                    
                    legend_handles = cell((2 * numel(test_classes)) + 2, 1);
                    legend_strings = cell((2 * numel(test_classes)) + 2, 1);

                    if show_classes
                        for ci = 1:numel(test_classes)
                            class_idx = this_feature_valid_qids & (classification_properties.class == test_classes(ci));
                            
                            legend_handles{ci} = scatter(data_full(class_idx, i), classification_properties.QIDS(class_idx), [], params.clr_cohort(test_classes(ci) + 2, :), params.symbol_cohort{test_classes(ci) + 2}, 'filled');
                            legend_strings{ci} = sprintf('%s Data', params.test_description{test_classes(ci) + 2});
                            
                            if ci == 1
                                hold on;
                            end
                        end
                    else
                        legend_handles{1} = scatter(data_full(this_feature_valid_qids, i), classification_properties.QIDS(this_feature_valid_qids), [], params.clr_cohort(t, :), params.symbol_cohort{t}, 'filled');
                        legend_strings{1} = sprintf('%s Data', this_test_description);
                    end
                    axis(axis);
                    limits = axis;
                    
                    hold on;
                    h_qids = plot(limits(1:2), [1 1] * params.qids_threshold, '--', 'Color', [1 1 1] * 0.5, 'LineWidth', 1.5);
                    uistack(h_qids, 'bottom');
                    legend_handles{numel(test_classes) + 1} = h_qids;
                    legend_strings{numel(test_classes) + 1} = sprintf('QIDS = %i', params.qids_threshold);
                    hold off;
                    
                    title(h_title, sprintf('%s (%s)', this_feature_name, this_test_description));
                    ylabel('QIDS Score');

                    pos = get(gca, 'OuterPosition');
                    
                    if show_dist
                        set(gca, 'ylim', [0 24]);
                        set(h_title, 'xlim', get(gca, 'xlim'));
                        set(gca, 'OuterPosition', pos .* [1 1 1 1.05]);
                        set(gca, 'ActivePositionProperty', 'position');
                    else
                        title(gca, char(160));
                        set(gca, 'ylim', [0 28]);
                        set(gca, 'OuterPosition', pos .* [1 1 1 0.98]);
                    end
                    
                    trend_handles = cell(numel(test_classes) + 3, 1);
                    
                    regression_x = linspace(limits(1), limits(2), 100);
                    
                    hold on;
                    if show_classes
                        for ci = 1:numel(test_classes)
                            if t == 6 && ci == 2
                                this_model_idx = 1;
                                this_model_clr = params.clr_cohort(t, :);
                                this_model_description = params.test_description{5};
                            else
                                this_model_idx = ci + 1;
                                this_model_clr = params.clr_cohort(test_classes(ci) + 2, :);
                                this_model_description = params.test_description{test_classes(ci) + 2};
                            end
                            regression_y = regression_model_fun(regression_model_coef(this_model_idx, 1), regression_model_coef(this_model_idx, 2), regression_x);
                            trend_handles{ci} = plot(regression_x, regression_y, 'Color', this_model_clr, 'LineWidth', 1);
                            legend_handles{numel(test_classes) + ci + 1} = trend_handles{ci};
                            legend_strings{numel(test_classes) + ci + 1} = sprintf('%s Model', this_model_description);
                        end
                    end
                    if t == 6
                        this_model_idx = 3;
                        this_model_class = 3;
                    else
                        this_model_idx = 1;
                        this_model_class = t;
                    end
                    trend_clr = params.clr_cohort(this_model_class, :);
                    regression_y = regression_model_fun(regression_model_coef(this_model_idx, 1), regression_model_coef(this_model_idx, 2), regression_x);
                    trend_handles{numel(test_classes) + 1} = plot(regression_x, regression_y, 'Color', trend_clr, 'LineWidth', 2);
                    trend_handles{numel(test_classes) + 2} = plot(regression_x, regression_y + regression_model_rmse(this_model_idx), '--', 'Color', trend_clr, 'LineWidth', 1);
                    trend_handles{numel(test_classes) + 3} = plot(regression_x, regression_y - regression_model_rmse(this_model_idx), '--', 'Color', trend_clr, 'LineWidth', 1);
                    legend_handles{(2 * numel(test_classes)) + 2} = trend_handles{numel(test_classes) + 1};
                    if t == 1
                        legend_strings{(2 * numel(test_classes)) + 2} = 'Fused Model';
                    else
                        legend_strings{(2 * numel(test_classes)) + 2} = sprintf('%s Model', params.test_description{this_model_class});
                    end
                    hold off;
                    
                    if ~show_classes
                        c_max = 0;
                    else
                        c_max = numel(test_classes) + 1;
                    end
                    for ci = -1:c_max
                        valid_legend_entries = false(size(legend_handles));
                        if show_classes
                            valid_legend_entries(1:(numel(test_classes) + 1)) = true;
                        else
                            valid_legend_entries(1) = true;
                            valid_legend_entries(numel(test_classes) + 1) = true;
                        end
                        if ci >= 0
                            valid_legend_entries((2 * numel(test_classes)) + 2) = true;
                        end
                        
                        for cj = 1:numel(test_classes)
                            if ci > numel(test_classes)
                                valid_legend_entries(numel(test_classes) + cj + 1) = true;
                                set(trend_handles{cj}, 'visible', 'on');
                            else
                                set(trend_handles{cj}, 'visible', 'off');
                            end
                        end

                        for cj = 1:3
                            if ci == -1
                                set(trend_handles{numel(test_classes) + cj}, 'visible', 'off');
                            else
                                set(trend_handles{numel(test_classes) + cj}, 'visible', 'on');
                            end
                        end
                        
                        if (ci >= 1) && (ci <= numel(test_classes))
                            valid_legend_entries(numel(test_classes) + ci + 1) = true;
                            set(trend_handles{ci}, 'visible', 'on');
                        end

                        if ci == -1
                            trend_description = '';
                        elseif ci == 0
                            trend_description = '_trend';
                        elseif ci <= numel(test_classes)
                            if t == 6 && ci == 2
                                this_trend_idx = 5;
                            else
                                this_trend_idx = test_classes(ci) + 2;
                            end
                            trend_description = sprintf('_trend_%s', params.test_description_filename{this_trend_idx});
                        else
                            trend_description = '_trend_all';
                        end

                        if ci == 0
                            if p < 0.001
                                p_text = 'p < 0.001';
                            else
                                p_text = sprintf('p = %.3f', p);
                            end
                            
                            % annotation('textbox', [0 0.825 1 0.1], 'String', {sprintf('r = %.2f, %s', rho, p_text)}, 'FontSize', 18, 'HorizontalAlignment', 'center', 'LineStyle', 'none');
                            
                            if first_loop
                                fprintf('    %s = %.2f, %s\n', char(961), rho, p_text);
                                first_loop = false;
                            end
                        end
                        
                        prepare_and_save_figure('features-raw', sprintf('features_qids_%.2i_%s%s_%s%s', i, this_feature_name_file, this_feature_suffix, this_test_description_filename, trend_description), varargin{:}, 'FontSize', 30, 'figure_handle', f1);
%                         prepare_and_save_figure('features-raw', sprintf('features_qids_%.2i_%s%s_%s%s_small', i, this_feature_name_file, this_feature_suffix, this_test_description_filename, trend_description), varargin{:}, 'FontSize', 16, 'figure_handle', f1);
%                         prepare_and_save_figure('features-raw', sprintf('features_qids_%.2i_%s%s_%s%s', i, this_feature_name_file, this_feature_suffix, this_test_description_filename, trend_description), varargin{:}, 'FontSize', 30, 'figure_handle', f1, 'save_figure', false);
                        
                        limit_centre = (limits(1) + limits(2)) / 2;
                        above_center = sum(data_full(this_feature_valid_qids, i) >= limit_centre) / sum(this_feature_valid_qids);
                        if rho <= 0 && above_center >= 0.5
                            h_legend = legend([legend_handles{valid_legend_entries}], legend_strings(valid_legend_entries), 'FontSize', 18, 'Location', 'SouthWest');
                        elseif rho <= 0 && above_center < 0.5
                            h_legend = legend([legend_handles{valid_legend_entries}], legend_strings(valid_legend_entries), 'FontSize', 18, 'Location', 'NorthEast');
                        elseif rho > 0 && above_center >= 0.5
                            h_legend = legend([legend_handles{valid_legend_entries}], legend_strings(valid_legend_entries), 'FontSize', 18, 'Location', 'NorthWest');
                        elseif rho > 0 && above_center < 0.5
                            h_legend = legend([legend_handles{valid_legend_entries}], legend_strings(valid_legend_entries), 'FontSize', 18, 'Location', 'SouthEast');
                        end

                        prepare_and_save_figure('features-raw', sprintf('features_qids_%.2i_%s%s_%s%s_with_legend', i, this_feature_name_file, this_feature_suffix, this_test_description_filename, trend_description), varargin{:}, 'FontSize', [], 'figure_handle', f1);

                        delete(h_legend);
                    
                        if i == features_to_show(1) && ~show_dist
                            f2 = figure;

                            copyobj(get(f1, 'children'), f2);

                            for obj = get(f2, 'children')'
                                if strcmp(get(obj, 'Type'), 'axes')
                                    set(obj, 'visible', 'off');
                                    for child = get(obj, 'children')'
                                        set(child, 'visible', 'off');
                                    end
                                end
                            end
                            hold on;
                            legend_handles_new = legend_handles;
                            if show_classes
                                for cj = 1:numel(test_classes)
                                    legend_handles_new{cj} = plot([1, 1], params.symbol_cohort{test_classes(cj) + 2}, 'Color', params.clr_cohort(test_classes(cj) + 2, :), 'MarkerFaceColor', params.clr_cohort(test_classes(cj) + 2, :), 'LineWidth', 1.5, 'MarkerSize', 12, 'Visible', 'off');
                                end
                            else
                                legend_handles_new{1} = plot([1, 1], params.symbol_cohort{t}, 'Color', params.clr_cohort(t, :), 'MarkerFaceColor', params.clr_cohort(t, :), 'LineWidth', 1.5, 'MarkerSize', 12, 'Visible', 'off');
                            end
                            h_legend = legend([legend_handles_new{valid_legend_entries}], legend_strings(valid_legend_entries), 'FontSize', 20);

                            set(h_legend, 'Units', 'pixels');
                            set(f2, 'Units', 'pixels');
                            for orientation = {'vertical', 'horizontal'}
                                set(h_legend, 'orientation', orientation{:});
                                drawnow();
                                p_legend = get(h_legend, 'Position');
                                p_legend(1:2) = [2 2];
                                set(h_legend, 'Position', p_legend);
                                p_figure = get(f2, 'Position');
                                p_figure(3:4) = p_legend(3:4) + [2 2];
                                set(f2, 'Position', p_figure);
                                set(f2, 'PaperPositionMode', 'auto')
                                prepare_and_save_figure('features-raw', sprintf('features_qids_00_legend%s_%s%s_%s', this_feature_suffix, this_test_description_filename, trend_description, orientation{:}), varargin{:}, 'FontSize', 20);
                            end
                            
                            %close(f2);
                            
                            figure(f1);
                        end
                    end
                    if save_figures
                        close(f1);
                    end
                end
            end
        end
    end
end
