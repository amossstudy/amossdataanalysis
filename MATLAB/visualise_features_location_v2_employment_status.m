function visualise_features_location_v2_employment_status(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 16-Nov-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats';
    
    [~, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(numel(classification_properties.participant) > 0, 'No data loaded.');
    
    valid_qids = ~isnan(classification_properties.QIDS);
    
    params = get_configuration('features_location_v2', varargin{:});
    
    hist_bins = [params.employment_status_id - 0.5, max(params.employment_status_id) + 0.5];
    
    function prepare_and_save_figure(subdir, filename, varargin)
        save_figure(filename, 'output_sub_dir', [OUTPUT_SUB_DIR '/' subdir], 'FontSize', 50, varargin{:});
    end

    hist_type_data = repmat(struct(), 2, 1);
    hist_type_data(1).filename_suffix = '';
    hist_type_data(2).filename_suffix = '_participants';
    hist_type_data(1).ylabel = 'Valid Weeks';
    hist_type_data(2).ylabel = 'Participants';
    
    hist_max = zeros(2, 1);
    
    for t = 1:5
        if t == 1
            test_valid_participants = valid_qids;
        elseif t == 5
            test_classes = 0:1;
            
            test_valid_participants = valid_qids & ismember(classification_properties.class, test_classes);
        else
            test_classes = t - 2;
            
            test_valid_participants = valid_qids & (classification_properties.class == test_classes);
        end
        
        this_test_description = params.test_description{t};
        this_test_description_filename = params.test_description_filename{t};

        test_participants = classification_properties.participant(test_valid_participants);
        test_participants_unique = unique(test_participants);
        test_participants_qids = classification_properties.QIDS(test_valid_participants);
        test_participants_employment_status = classification_properties.employment_status(test_valid_participants);
        test_participants_unique_employment_status = NaN(size(test_participants_unique));
        
        for p_i = 1:numel(test_participants_unique)
            this_participant_employment_status = unique(test_participants_employment_status(strcmp(test_participants, test_participants_unique{p_i})));
            
            assert(numel(this_participant_employment_status) == 1, 'Invalid participant employment status.');
            
            test_participants_unique_employment_status(p_i) = this_participant_employment_status;
        end
        
        hist_data = cell(2, 1);
        hist_data{1} = test_participants_employment_status;
        hist_data{2} = test_participants_unique_employment_status;
        
        for hist_type = 1:2
            figure
            
            h = histogram(hist_data{hist_type}, hist_bins);

            set(h, 'FaceColor', params.clr_cohort(t, :));
            set(h, 'FaceAlpha', 1);

            set(gca, 'xlim', hist_bins([1, end]));
            set(gca, 'xtick', params.employment_status_id);
            set(gca, 'xticklabel', params.employment_status_description, 'FontSize', 18);
            set(gca, 'XTickLabelRotation', 35);
            y_lim = get(gca, 'ylim');
            hist_max(hist_type) = max(hist_max(hist_type), y_lim(2));
            y_lim(2) = hist_max(hist_type);
            set(gca, 'ylim', y_lim);

            title(sprintf('Employment Status Distribution (%s)', this_test_description), 'FontSize', 28);
            ylabel(hist_type_data(hist_type).ylabel, 'FontSize', 30);

            prepare_and_save_figure('employment_status', sprintf('employment_status_distribution%s_%s', hist_type_data(hist_type).filename_suffix, this_test_description_filename), varargin{:}, 'FontSize', []);
            
            if hist_type == 1 && sum(test_participants_qids >= params.qids_threshold) > 0
                figure
                
                h = histogram(hist_data{hist_type}, hist_bins);
                set(h, 'FaceColor', params.clr_cohort(t, :));
                set(h, 'FaceAlpha', 1);

                hold on;
                
                histogram(hist_data{hist_type}(test_participants_qids >= params.qids_threshold), hist_bins, 'FaceColor', [0 0 0], 'FaceAlpha', 0.4);

                hold off;
                
                legend_str = {['QIDS < ' num2str(params.qids_threshold)], ['QIDS ' char(8805) ' ' num2str(params.qids_threshold)]};
                legend(legend_str, 'FontSize', 20);
                
                set(gca, 'xlim', hist_bins([1, end]));
                set(gca, 'xtick', params.employment_status_id);
                set(gca, 'xticklabel', params.employment_status_description, 'FontSize', 18);
                set(gca, 'XTickLabelRotation', 35);
                y_lim = get(gca, 'ylim');
                hist_max(hist_type) = max(hist_max(hist_type), y_lim(2));
                y_lim(2) = hist_max(hist_type);
                set(gca, 'ylim', y_lim);

                title(sprintf('Employment Status Distribution (%s)', this_test_description), 'FontSize', 28);
                ylabel(hist_type_data(hist_type).ylabel, 'FontSize', 30);

                prepare_and_save_figure('employment_status', sprintf('employment_status_distribution_qids_%i_%s', params.qids_threshold, this_test_description_filename), varargin{:}, 'FontSize', []);
            end
        end
    end

    hist_max = zeros(2, 1);
    
    for j = 2:-1:1
        for hist_type = 1:2
            figure
            
            legend_strings = cell(j + 2, 1);
            filename_string = '';

            for i = 0:j
                iter_valid_participants = valid_qids & ismember(classification_properties.class, i:j);
                
                iter_participants = classification_properties.participant(iter_valid_participants);
                iter_participants_employment_status = classification_properties.employment_status(iter_valid_participants);

                if hist_type == 1
                    hist_data = iter_participants_employment_status;
                else
                    iter_participants_unique = unique(iter_participants);
                    iter_participants_unique_employment_status = NaN(size(iter_participants_unique));

                    for p_i = 1:numel(iter_participants_unique)
                        this_participant_employment_status = unique(iter_participants_employment_status(strcmp(iter_participants, iter_participants_unique{p_i})));

                        assert(numel(this_participant_employment_status) == 1, 'Invalid participant employment status.');

                        iter_participants_unique_employment_status(p_i) = this_participant_employment_status;
                    end
                    
                    hist_data = iter_participants_unique_employment_status;
                end
                
                h = histogram(hist_data, hist_bins);
                
                if i == 0
                    total_data = hist_data;
                end
                
                set(h, 'FaceColor', params.clr_cohort(i + 2, :));
                set(h, 'FaceAlpha', 1);

                legend_strings{i + 1} = params.test_description{i + 2};

                filename_string = sprintf('%s_%s', filename_string, params.test_description_filename{i + 2});

                if i == 0
                    hold on;
                end
            end

            if j == 2
                clr_total = params.clr_cohort(1, :);
            else
                clr_total = params.clr_cohort(5, :);
            end
            
            histogram(total_data, hist_bins, 'DisplayStyle', 'stairs', 'EdgeColor', clr_total);
            legend_strings{j + 2} = 'Total';
            hold off;

            legend(legend_strings, 'FontSize', 20);

            set(gca, 'xlim', hist_bins([1, end]));
            set(gca, 'xtick', params.employment_status_id);
            set(gca, 'xticklabel', params.employment_status_description, 'FontSize', 18);
            set(gca, 'xticklabelrotation', 35);
            y_lim = get(gca, 'ylim');
            hist_max(hist_type) = max(hist_max(hist_type), y_lim(2));
            y_lim(2) = hist_max(hist_type);
            set(gca, 'ylim', y_lim);

            title('Employment Status Distribution Stacked', 'FontSize', 28);
            ylabel(hist_type_data(hist_type).ylabel, 'FontSize', 30);

            prepare_and_save_figure('employment_status', sprintf('employment_status_distribution_stacked%s%s', hist_type_data(hist_type).filename_suffix, filename_string), varargin{:}, 'FontSize', []);
        end
    end
end
