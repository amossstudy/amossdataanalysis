function visualise_features_location_v2_cohort_qids_distributions(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 04-Mar-2016

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats/feature-distributions';
    
    save_figures = get_argument({'savefigure', 'save_figure', 'savefigures', 'save_figures'}, true, varargin{:});
    
    params = get_configuration('features_location_v2');
    
    [classification_data, classification_properties] = load_features_location_v2_distributions(varargin{:});
    
    qids_lt_11_idx = classification_properties.QIDS < 11;
    qids_ge_11_idx = classification_properties.QIDS >= 11;
    
    boxplot_labels = cell(6, 1);
    
    for c = 1:3
        boxplot_labels{(c - 1) * 2 + 1} = [params.test_description{c + 1} ' (QIDS < 11)'];
        boxplot_labels{(c - 1) * 2 + 2} = [params.test_description{c + 1} ' (QIDS ' char(8805) ' 11)'];
    end
    
    for f = 1:size(classification_data.raw, 1)
        data_raw = NaN(6, size(classification_data.raw, 2));
        data_boxplot = NaN(6, size(classification_data.raw, 2));

        for c = 1:3
            cohort_idx = classification_properties.class == (c - 1);
            
            data_all = classification_data.raw(f, :);
            data_all_normalised = (data_all - mean(data_all)) / std(data_all);
            data_qids_lt_11 = data_all(cohort_idx & qids_lt_11_idx);
            data_qids_ge_11 = data_all(cohort_idx & qids_ge_11_idx);
            
            data_boxplot((c - 1) * 2 + 1, 1:sum(cohort_idx & qids_lt_11_idx)) = data_all_normalised(cohort_idx & qids_lt_11_idx);
            data_boxplot((c - 1) * 2 + 2, 1:sum(cohort_idx & qids_ge_11_idx)) = data_all_normalised(cohort_idx & qids_ge_11_idx);
            
            data_raw((c - 1) * 2 + 1, 1:numel(data_qids_lt_11)) = data_qids_lt_11;
            data_raw((c - 1) * 2 + 2, 1:numel(data_qids_ge_11)) = data_qids_ge_11;
        end
        
        this_feature_name = params.feature_name{f};
        this_feature_name_file = strrep(lower(this_feature_name), ' ', '_');

        for t = 1:4
            figure;

            boxplot_valid_idx = sum(~isnan(data_boxplot), 2) > 0;

            if t == 1
                subplot_rows = sum(boxplot_valid_idx);
            elseif t == 2
                subplot_rows = 3;
            elseif t == 3 || t == 4
                subplot_rows = 2;
            end

            subplot(subplot_rows, 4, (0:(subplot_rows - 1)) * 4 + 1);

            clr_valid = lines(sum(boxplot_valid_idx));
            clr_all = zeros(numel(boxplot_valid_idx), 3);
            clr_all(boxplot_valid_idx, :) = clr_valid;

            boxplot(data_boxplot(boxplot_valid_idx, :)', 'Colors', clr_valid);

            set(gca, 'xticklabel', boxplot_labels(boxplot_valid_idx));
            set(gca, 'XTickLabelRotation', 40);
            ylabel('Feature value (normalised)', 'FontSize', 18);

            p = get(gca, 'Position');
            p_diff = p(3) * 1.5;
            p(1) = p(1) - p_diff;
            p(3) = p(3) + p_diff;
            set(gca, 'Position', p);

            a_min = floor(min(min(data_raw)));
            a_max = ceil(max(max(data_raw)));
            edges = linspace(a_min, a_max, 50);
            pdf_x = linspace(a_min, a_max, 200);

            this_test_description_filename = '';

            if t == 1
                this_test_description_filename = 'histograms';

                j = 1;
                for i = find(boxplot_valid_idx)'
                    subplot(subplot_rows, 4, (j - 1) * 4 + (2:4));
                    histogram(data_raw(i, :), edges, 'Normalization', 'pdf', 'FaceColor', clr_all(i, :))
                    if j == 1
                        title(params.feature_name{f}, 'FontSize', 18);
                    end
                    set(gca, 'ytick', []);
                    if j < subplot_rows
                        set(gca, 'xticklabel', []);
                    else
                        set(gca, 'FontSize', 18);
                    end
                    hold on;
                    c = floor((i + 1) / 2);
                    if mod(i, 2) == 1
                        pdf_y = get_fitted_distribution_property(classification_properties.dist_qids_lt_11{f, c}, 'pdf', pdf_x);
                    elseif mod(i, 2) == 0
                        pdf_y = get_fitted_distribution_property(classification_properties.dist_qids_ge_11{f, c}, 'pdf', pdf_x);
                    end
                    plot(pdf_x, pdf_y, 'Color', clr_all(i, :), 'LineWidth', 2);
                    hold off;
                    set(gca, 'xlim', [a_min a_max]);
                    j = j + 1;
                end
            elseif t == 2
                this_test_description_filename = 'qids_11_by_cohort';

                for i = 1:3
                    subplot(subplot_rows, 4, (i - 1) * 4 + (2:4));

                    data_y_classes = zeros(numel(pdf_x), 2);

                    if boxplot_valid_idx((i - 1) * 2 + 1)
                        data_y_classes(:, 1) = get_fitted_distribution_property(classification_properties.dist_qids_lt_11{f, i}, 'pdf', pdf_x);
                    end
                    if boxplot_valid_idx((i - 1) * 2 + 2)
                        data_y_classes(:, 2) = get_fitted_distribution_property(classification_properties.dist_qids_ge_11{f, i}, 'pdf', pdf_x);
                    end
                    
                    h_area = area(pdf_x, data_y_classes ./ repmat(sum(data_y_classes, 2), 1, 2));

                    for j = 1:2
                        set(h_area(j), 'FaceColor', clr_all(((i - 1) * 2) + j, :))
                    end

                    set(gca, 'Layer', 'top');

                    if i == 1
                        title(params.feature_name{f}, 'FontSize', 18);
                    end

                    y_ticks = 0:0.25:1;
                    y_tick_str = strtrim(cellstr(num2str(y_ticks')));
                    y_tick_str([2, 4]) = {[], []};

                    set(gca, 'ytick', y_ticks);
                    set(gca, 'yticklabel', y_tick_str);
                    set(gca, 'ygrid', 'on')
                    set(gca, 'YAxisLocation', 'right')
                    set(gca, 'FontSize', 18);

                    if i < subplot_rows
                        set(gca, 'xticklabel', []);
                    end

                    set(gca, 'xlim', [a_min a_max]);
                    set(gca, 'ylim', [0 1]);
                end
            elseif t == 3 || t == 4
                this_test_description_filename = 'cohort_by_qids_11';
                if t == 4
                    this_test_description_filename = [this_test_description_filename, '_no_hc']; %#ok<AGROW>
                end

                for i = 1:2
                    subplot(subplot_rows, 4, (i - 1) * 4 + (2:4));

                    data_y_classes = zeros(numel(pdf_x), 3);

                    if t == 3
                        j_min = 1;
                    elseif t == 4
                        % Don't include HCs.
                        j_min = 2;
                    end

                    for j = j_min:3
                        if boxplot_valid_idx((j - 1) * 2 + i)
                            if i == 1
                                data_y_classes(:, j) = get_fitted_distribution_property(classification_properties.dist_qids_lt_11{f, j}, 'pdf', pdf_x);
                            elseif i == 2
                                data_y_classes(:, j) = get_fitted_distribution_property(classification_properties.dist_qids_ge_11{f, j}, 'pdf', pdf_x);
                            end
                        end
                    end


                    h_area = area(pdf_x, data_y_classes ./ repmat(sum(data_y_classes, 2), 1, 3));

                    for j = 1:3
                        set(h_area(j), 'FaceColor', clr_all(((j - 1) * 2) + i, :))
                    end

                    set(gca, 'Layer', 'top');

                    if i == 1
                        title(params.feature_name{f}, 'FontSize', 18);
                    end

                    y_ticks = 0:0.25:1;
                    y_tick_str = strtrim(cellstr(num2str(y_ticks')));
                    y_tick_str([2, 4]) = {[], []};

                    set(gca, 'ytick', y_ticks);
                    set(gca, 'yticklabel', y_tick_str);
                    set(gca, 'ygrid', 'on')
                    set(gca, 'YAxisLocation', 'right')
                    set(gca, 'FontSize', 18);

                    if i < subplot_rows
                        set(gca, 'xticklabel', []);
                    end

                    set(gca, 'xlim', [a_min a_max]);
                    set(gca, 'ylim', [0 1]);
                end
            end

            save_figure(sprintf('feature_distributions_%.2i_%s_%s', f, this_feature_name_file, this_test_description_filename), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:})
        end

        if save_figures
            close all;
        end
    end
end
