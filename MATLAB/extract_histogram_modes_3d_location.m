function [modes, mode_center, mode_mass] = extract_histogram_modes_3d_location(hist_data)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 15-Jul-2015

    modes = NaN(size(hist_data));
    mode_center = zeros(numel(~isnan(hist_data)), 2);
    mode_mass = zeros(numel(~isnan(hist_data)), 1);
    mode_count = 0;

    while nansum(hist_data(isnan(modes))) > 0
        this_max = max(max(hist_data(isnan(modes))));
        this_max_list = find(hist_data == this_max);
        for this_max_ind = this_max_list'
            [this_max_x, this_max_y] = ind2sub(size(hist_data), this_max_ind);

            max_surr_mode = 0;

            for i = 2:2:8
                [diff_x, diff_y] = ind2sub([3 3], i);
                diff_x = diff_x - 2;
                diff_y = diff_y - 2;

                if ((this_max_x + diff_x) > 0) && ((this_max_y + diff_y) > 0) && ...
                        ((this_max_x + diff_x) < size(hist_data, 1)) && ((this_max_y + diff_y) < size(hist_data, 2))

                    this_surr_mode = modes(this_max_x + diff_x, this_max_y + diff_y);

                    if ~isnan(this_surr_mode) && ((max_surr_mode == 0) || (mode_mass(this_surr_mode) > mode_mass(max_surr_mode)))
                        max_surr_mode = this_surr_mode;
                    end
                end
            end

            if (max_surr_mode > 0)
                modes(this_max_x, this_max_y) = max_surr_mode;
                mode_mass(max_surr_mode) = mode_mass(max_surr_mode) + hist_data(this_max_x, this_max_y);
            else
                modes(this_max_x, this_max_y) = mode_count + 1;
                mode_mass(mode_count + 1) = hist_data(this_max_x, this_max_y);
                mode_center(mode_count + 1, :) = [this_max_x, this_max_y];
                mode_count = mode_count + 1;
            end
        end
    end

    mode_center = mode_center(1:mode_count, :);
    mode_mass = mode_mass(1:mode_count);
end
