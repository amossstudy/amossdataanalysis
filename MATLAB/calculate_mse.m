function mse = calculate_mse(data, m, r, tau_range)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 21-Dec-2014

% References:
% * http://www.physionet.org/physiotools/mse/
% * Costa, M., Goldberger, A., & Peng, C.-K. (2002). Multiscale Entropy
%   Analysis of Complex Physiologic Time Series. Physical Review Letters,
%   89(6), 68102. doi:10.1103/PhysRevLett.89.068102
%   Retrieved from http://dx.doi.org/10.1103/PhysRevLett.89.068102
% * Costa, M., Goldberger, A., & Peng, C.-K. (2005). Multiscale entropy
%   analysis of biological signals. Physical Review E, 71(2), 21906.
%   Retrieved from http://dx.doi.org/10.1103/PhysRevE.71.021906

    N = length(data);
    
    mse = zeros(length(tau_range), 1);
    
    for tau = tau_range
        data_cg = coarse_graining(tau);
        mse(tau) = samp_en(data_cg, tau);
    end
    
    function out = coarse_graining(tau)
        out = zeros(floor(N/tau), 1);
        for j = 1:floor((N/tau))
            out(j) = sum(data((((j-1)*tau)+1):(j*tau))) / tau;
        end
    end

    function out = samp_en(data_cg, tau)
        A = 0;
        B = 0;
        i_max = (floor(N/tau) - m);
        
        for i = 1:i_max
            for j = 1:i_max
                if i ~= j
                    matching = m + 1;
                    for k=0:m
                        if abs(data_cg(i+k) - data_cg(j+k)) > r
                            matching = k;
                            break;
                        end
                    end
                    
                    if matching == (m + 1)
                        A = A + 1;
                        B = B + 1;
                    elseif matching == m
                        B = B + 1;
                    end
                end
            end
        end
        
        out = -log(A/B);
    end
end
