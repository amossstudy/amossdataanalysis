function extract_features_location(participant, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 06-Dec-2014

    FEATURES = 16;
    WEEKLY_FEATURES = 28;
    
    features = load_location_features(participant, varargin{:});
    
    features.basic = NaN(FEATURES, 1);
    features.is_valid = 0;
    
    features.class = get_participant_property(participant, 'classification', varargin{:});
    
    data = load_location_preprocessed(participant);
    
    episodes_for_analysis = load_episodes_for_analysis(participant);
    
    episodes_types = get_configuration('episode_types');
    
    if ~isempty(data) && isfield(data, 'validated') && sum(~isnan(data.validated.data) > 0)
        epoch_count = 0;
        valid_epoch_count = 0;
        if ~isempty(episodes_for_analysis) && isfield(episodes_for_analysis, 'epochs')
            epoch_count = size(episodes_for_analysis.epochs, 2);
            for epoch = episodes_for_analysis.epochs
                if epoch.valid
                    valid_epoch_count = valid_epoch_count + 1;
                end
            end
        end
        
        features.episodes = struct;
        features.episodes.basic = NaN(FEATURES, valid_epoch_count);
        features.episodes.is_valid = false(valid_epoch_count, 1);
        features.episodes.class = zeros(1, valid_epoch_count) - 1;
        features.episodes.start = NaN(1, valid_epoch_count);
        features.episodes.end = NaN(1, valid_epoch_count);
        
        ep_invalid = 0;
        
        for ep_id = 0:epoch_count
            if ep_id == 0
                valid_days = true(size(data.validated.time));
            else
                if ~episodes_for_analysis.epochs(ep_id).valid
                    ep_invalid = ep_invalid + 1;
                    continue;
                end
                valid_days = (data.validated.time >= episodes_for_analysis.epochs(ep_id).start) & (data.validated.time < episodes_for_analysis.epochs(ep_id).end + 1) & ~isnan(data.validated.data);
            end
            
            data_days = floor(data.validated.time);
            day_of_week = weekday(data.validated.time);
            data_time_of_day = rem(data.validated.time, 1);
            data_morning = (data_time_of_day >= (3/24) & data_time_of_day <= (6/24));
            data_morning_and_day = (data_time_of_day >= (3/24));
            data_days_unique = unique(data_days(valid_days));
            day_of_week_unique = weekday(data_days_unique);

            if ep_id == 0
                weeks_start = data_days_unique(1);
                % Start on the previous Monday when processing whole data
                % set. If processing an epoch, then treat the first day in
                % the epoch as the first day of the week, regardless of the
                % day.
                weeks_start = weeks_start - weekday(weeks_start) + 2;
                weeks_end = data_days_unique(end);
            else
                weeks_start = episodes_for_analysis.epochs(ep_id).start;
                weeks_end = episodes_for_analysis.epochs(ep_id).end;
            end
            
            weeks = ((0:floor((weeks_end - weeks_start) / 7)) * 7) + weeks_start;

            if ep_id == 0
                features.weekly = struct;
                features.weekly.time = weeks;
                features.weekly.basic = NaN(WEEKLY_FEATURES, length(weeks));
                features.weekly.is_valid = 1;
            end

            features_basic = NaN(FEATURES, 1);

            location_time = [diff(data.validated.time); 0];
            location_time = min(location_time, 1/(24 * 4));

            features_basic(1) = sum(location_time(valid_days & data.validated.data > 5 & ~ismember(day_of_week, [7 1]))) / sum(~ismember(day_of_week_unique, [7 1]));
            features_basic(2) = sum(location_time(valid_days & data.validated.data > 5 & ismember(day_of_week, [7 1]))) / sum(ismember(day_of_week_unique, [7 1]));

            features_basic(3) = sum(location_time(valid_days & data.validated.data > 20 & ~ismember(day_of_week, [7 1]))) / sum(~ismember(day_of_week_unique, [7 1]));
            features_basic(4) = sum(location_time(valid_days & data.validated.data > 20 & ismember(day_of_week, [7 1]))) / sum(ismember(day_of_week_unique, [7 1]));

            if sum(~ismember(day_of_week_unique, [7 1])) == 0
                features_basic([1, 3]) = 0;
            end

            if sum(ismember(day_of_week_unique, [7 1])) == 0
                features_basic([2, 4]) = 0;
            end

            week_var = NaN(1, length(weeks));
            weekend_var = NaN(1, length(weeks));
            week_mean = NaN(1, length(weeks));
            weekend_mean = NaN(1, length(weeks));
            week_locations = NaN(1, length(weeks));
            weekend_locations = NaN(1, length(weeks));
            week_valid_days = zeros(1, length(weeks));
            weekend_valid_days = zeros(1, length(weeks));
            week_stay_home = zeros(1, length(weeks));
            weekend_stay_home = zeros(1, length(weeks));
            week_time_leave_home = NaN(1, length(weeks) * 7);
            weekend_time_leave_home = NaN(1, length(weeks) * 7);
            week_time_leave_home_mean = zeros(1, length(weeks));
            weekend_time_leave_home_mean = zeros(1, length(weeks));
            week_time_leave_home_var = zeros(1, length(weeks));
            weekend_time_leave_home_var = zeros(1, length(weeks));

            for j = 1:length(weeks)
                week_durations = NaN(1, 7);
                weekend_durations = NaN(1, 7);

                WEEKDAYS = find(ismember(weekday(weeks(j) + (0:6)), 2:6)) - 1;
                WEEKENDS = find(ismember(weekday(weeks(j) + (0:6)), [1 7])) - 1;

                if ep_id == 0
                    if sum(ismember(data_days, weeks(j) + WEEKDAYS)) > 0
                        features.weekly.basic(1, j) = sum(location_time(data.validated.data > 5 & ismember(data_days, weeks(j) + WEEKDAYS))) / sum(ismember(data_days, weeks(j) + WEEKDAYS));
                        features.weekly.basic(3, j) = sum(location_time(data.validated.data > 20 & ismember(data_days, weeks(j) + WEEKDAYS))) / sum(ismember(data_days, weeks(j) + WEEKDAYS));
                    end

                    if sum(ismember(data_days, weeks(j) + WEEKENDS)) > 0
                        features.weekly.basic(2, j) = sum(location_time(data.validated.data > 5 & ismember(data_days, weeks(j) + WEEKENDS))) / sum(ismember(data_days, weeks(j) + WEEKENDS));
                        features.weekly.basic(4, j) = sum(location_time(data.validated.data > 20 & ismember(data_days, weeks(j) + WEEKENDS))) / sum(ismember(data_days, weeks(j) + WEEKENDS));
                    end
                end

                for k = WEEKDAYS;
                    current_day = (data_days == (weeks(j) + k)) & valid_days;
                    week_durations(k + 1) = sum(location_time(data.validated.data > 5 & current_day));
                    if sum(current_day) > 0
                        week_valid_days = week_valid_days + 1;
                        if length(unique(data.validated.location_group(current_day))) == 1
                            week_stay_home(j) = week_stay_home(j) + 1;
                        else
                            % Do we have data at some point between 3AM and 6AM?
                            if (sum(current_day & data_morning)) > 0
                                % Was the participant at home at some point between 3AM and 6AM?
                                morning_location = data.validated.location_group(current_day & data_morning);
                                if (sum(morning_location == 1)) > 0
                                    morning_and_day_locations = data.validated.location_group(current_day & data_morning_and_day);
                                    morning_and_day_times = data_time_of_day(current_day & data_morning_and_day);

                                    first_home_idx = find(morning_and_day_locations == 1, 1, 'first');

                                    morning_and_day_locations = morning_and_day_locations(morning_and_day_times >= morning_and_day_times(first_home_idx));
                                    morning_and_day_times = morning_and_day_times(morning_and_day_times >= morning_and_day_times(first_home_idx));

                                    first_away_idx = find(morning_and_day_locations ~= 1, 1, 'first');

                                    if ~isempty(first_away_idx)
                                        week_time_leave_home(((j - 1) * 7) + 1 + k) = morning_and_day_times(first_away_idx);
                                    end
                                end
                            end
                        end
                    end
                end

                for k = WEEKENDS;
                    current_day = (data_days == (weeks(j) + k)) & valid_days;
                    weekend_durations(k + 1) = sum(location_time(data.validated.data > 5 & current_day));
                    if sum(current_day) > 0
                        weekend_valid_days = weekend_valid_days + 1;
                        if length(unique(data.validated.location_group(current_day))) == 1
                            weekend_stay_home(j) = weekend_stay_home(j) + 1;
                        else
                            % Do we have data at some point between 3AM and 6AM?
                            if (sum(current_day & data_morning)) > 0
                                % Was the participant at home at some point between 3AM and 6AM?
                                morning_location = data.validated.location_group(current_day & data_morning);
                                if (sum(morning_location == 1)) > 0
                                    morning_and_day_locations = data.validated.location_group(current_day & data_morning_and_day);
                                    morning_and_day_times = data_time_of_day(current_day & data_morning_and_day);

                                    first_home_idx = find(morning_and_day_locations == 1, 1, 'first');

                                    morning_and_day_locations = morning_and_day_locations(morning_and_day_times >= morning_and_day_times(first_home_idx));
                                    morning_and_day_times = morning_and_day_times(morning_and_day_times >= morning_and_day_times(first_home_idx));

                                    first_away_idx = find(morning_and_day_locations ~= 1, 1, 'first');

                                    if ~isempty(first_away_idx)
                                        weekend_time_leave_home(((j - 1) * 7) + 1 + k) = morning_and_day_times(first_away_idx);
                                    end
                                end
                            end
                        end
                    end
                end

                week_durations = week_durations(WEEKDAYS + 1);
                weekend_durations = weekend_durations(WEEKENDS + 1);
                
                week_var(j) = var(week_durations(~isnan(week_durations)));
                weekend_var(j) = var(weekend_durations(~isnan(weekend_durations)));

                week_mean(j) = mean(week_durations(~isnan(week_durations)));
                weekend_mean(j) = mean(weekend_durations(~isnan(weekend_durations)));

                % Exclude travelling (location_group = 0)
                week_locations(j) = sum(unique(data.validated.location_group(ismember(data_days, weeks(j) + WEEKDAYS))) > 0);
                weekend_locations(j) = sum(unique(data.validated.location_group(ismember(data_days, weeks(j) + WEEKENDS))) > 0);

                temp_week_time_leave_home = week_time_leave_home(((j - 1) * 7) + 1 + WEEKDAYS);
                temp_weekend_time_leave_home = weekend_time_leave_home(((j - 1) * 7) + 1 + WEEKENDS);
                temp_week_time_leave_home = temp_week_time_leave_home(~isnan(temp_week_time_leave_home));
                temp_weekend_time_leave_home = temp_weekend_time_leave_home(~isnan(temp_weekend_time_leave_home));

                if numel(temp_week_time_leave_home) == 0
                    temp_week_time_leave_home = NaN;
                end

                if numel(temp_weekend_time_leave_home) == 0
                    temp_weekend_time_leave_home = NaN;
                end

                week_time_leave_home_mean(j) = mean(temp_week_time_leave_home);
                weekend_time_leave_home_mean(j) = mean(temp_weekend_time_leave_home);

                week_time_leave_home_var(j) = var(temp_week_time_leave_home);
                weekend_time_leave_home_var(j) = var(temp_weekend_time_leave_home);
            end

            features_basic(5) = var(week_mean(~isnan(week_mean)));
            features_basic(6) = var(weekend_mean(~isnan(weekend_mean)));

            features_basic(7) = mean(week_locations(~isnan(week_locations) & (week_locations > 0)));
            features_basic(8) = mean(weekend_locations(~isnan(weekend_locations) & (weekend_locations > 0)));

            features_basic(9) = var(week_locations(~isnan(week_locations) & (week_locations > 0)));
            features_basic(10) = var(weekend_locations(~isnan(weekend_locations) & (weekend_locations > 0)));

            features_basic(11) = mean(week_stay_home(week_valid_days > 0) ./ week_valid_days(week_valid_days > 0));
            features_basic(12) = mean(weekend_stay_home(weekend_valid_days > 0) ./ weekend_valid_days(weekend_valid_days > 0));

            features_basic(13) = var(week_stay_home(week_valid_days > 0) ./ week_valid_days(week_valid_days > 0));
            features_basic(14) = var(weekend_stay_home(weekend_valid_days > 0) ./ weekend_valid_days(weekend_valid_days > 0));

            features_basic(15) = var(week_time_leave_home(~isnan(week_time_leave_home)));
            features_basic(16) = var(weekend_time_leave_home(~isnan(weekend_time_leave_home)));

            if ep_id == 0
                features.basic(:) = features_basic;
                features.is_valid = true;

                features.weekly.basic(5, :) = week_var;
                features.weekly.basic(6, :) = weekend_var;

                features.weekly.basic(7, :) = week_mean;
                features.weekly.basic(8, :) = weekend_mean;

                features.weekly.basic(9, :) = week_locations;
                features.weekly.basic(10, :) = weekend_locations;

                features.weekly.basic(11, :) = week_stay_home;
                features.weekly.basic(12, :) = weekend_stay_home;

                features.weekly.basic(13, :) = week_time_leave_home_mean;
                features.weekly.basic(14, :) = weekend_time_leave_home_mean;

                features.weekly.basic(15, :) = week_time_leave_home_var;
                features.weekly.basic(16, :) = weekend_time_leave_home_var;

                features.weekly.basic(17, :) = features.weekly.basic(5, :) / mean(features.weekly.basic(5, :));
                features.weekly.basic(18, :) = features.weekly.basic(6, :) / mean(features.weekly.basic(6, :));
                features.weekly.basic(19, :) = features.weekly.basic(7, :) / mean(features.weekly.basic(7, :));
                features.weekly.basic(20, :) = features.weekly.basic(8, :) / mean(features.weekly.basic(8, :));
                features.weekly.basic(21, :) = features.weekly.basic(9, :) / mean(features.weekly.basic(9, :));
                features.weekly.basic(22, :) = features.weekly.basic(10, :) / mean(features.weekly.basic(10, :));
                features.weekly.basic(23, :) = features.weekly.basic(11, :) / mean(features.weekly.basic(11, :));
                features.weekly.basic(24, :) = features.weekly.basic(12, :) / mean(features.weekly.basic(12, :));
                features.weekly.basic(25, :) = features.weekly.basic(13, :) / mean(features.weekly.basic(13, :));
                features.weekly.basic(26, :) = features.weekly.basic(14, :) / mean(features.weekly.basic(14, :));
                features.weekly.basic(27, :) = features.weekly.basic(15, :) / mean(features.weekly.basic(15, :));
                features.weekly.basic(28, :) = features.weekly.basic(16, :) / mean(features.weekly.basic(16, :));

                features.weekly.basic(isinf(features.weekly.basic)) = 0;

                tc_data = load_true_colours_preprocessed(participant, varargin{:});

                if ~isempty(tc_data)
                    features.weekly.class = struct;
                    features.weekly.class.qids = NaN(1, length(weeks));

                    location_data_dates_weekly = floor(data.regularised.time) - weekday(data.regularised.time) + 2;

                    if isfield(tc_data, 'QIDS')
                        for j = 1:length(weeks)
                            if sum(tc_data.QIDS.weekly.time == weeks(j)) > 0
                                if sum(~isnan(data.regularised.data(location_data_dates_weekly == weeks(j)))) >= (5 * 144)
                                    features.weekly.class.qids(j) = tc_data.QIDS.weekly.data(tc_data.QIDS.weekly.time == weeks(j));
                                end
                            end
                        end
                    end

                    features.weekly.class.qids_severity = features.weekly.class.qids;

                    features.weekly.class.qids_severity(ismember(features.weekly.class.qids_severity, 0:5)) = 0;
                    features.weekly.class.qids_severity(ismember(features.weekly.class.qids_severity, 6:10)) = 1;
                    features.weekly.class.qids_severity(ismember(features.weekly.class.qids_severity, 11:15)) = 2;
                    features.weekly.class.qids_severity(ismember(features.weekly.class.qids_severity, 16:20)) = 3;
                    features.weekly.class.qids_severity(ismember(features.weekly.class.qids_severity, 21:27)) = 4;
                end
            else
                features.episodes.basic(:, ep_id - ep_invalid) = features_basic;
                features.episodes.is_valid(ep_id - ep_invalid) = true;
                if isfield(episodes_types, episodes_for_analysis.epochs(ep_id).type)
                    features.episodes.class(ep_id - ep_invalid) = episodes_types.(episodes_for_analysis.epochs(ep_id).type).id;
                else
                    features.episodes.class(ep_id - ep_invalid) = NaN;
                end
                features.episodes.start(ep_id - ep_invalid) = episodes_for_analysis.epochs(ep_id).start;
                features.episodes.end(ep_id - ep_invalid) = episodes_for_analysis.epochs(ep_id).end;
            end
        end
    else
        fprintf('  No data loaded\n');
    end
    
    save_data([participant '-location-features'], features, varargin{:});
end
