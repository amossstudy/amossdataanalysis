function plot_calls_texts_by_id(time, data, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    if isempty(time)
        t_min = 0;
        t_max = 0;
    else
        t_min = floor(time(1));
        t_max = ceil(time(end));
    end
    
    t_min = get_argument({'tmin', 't_min'}, t_min, varargin{:});
    t_max = get_argument({'tmax', 't_max'}, t_max, varargin{:});
    title_str = get_argument('title', [], varargin{:});
    
    [xticks, xticks_str] = get_xticks(t_min, t_max, varargin{:});
    
    figure('Renderer', 'Painters');
    % i = time index
    % j = caller id
    % v = total time in epoch
    [i,j,v] = find(data);
    if isempty(j) ~= 1
      c = hsv(max(j));
      %while length(c) < max(j)
      %    c = [c; c];
      %end
      if isempty(c) ~= 1
          c = c(randperm(size(c, 1)),:);
      end

      colormap(c);
    end
    
    scatter(time(i), j, min(v / 2, 3600) / 2, j, 'filled');
    
    set(gca,'XTick',xticks)
    set(gca,'XTickLabel',xticks_str)
    set(gca,'YTick',[])
    
    if ~isempty(title_str)
      title(title_str, 'FontSize', 30);
    end
    
    ylabel('Sender / Recipient ID', 'FontSize', 30);
    
    set(gca, 'FontSize', 30);
    
    p = get(gca, 'pos');
    % When changing width we also need to change bottom and height.
    p(2) = 0.1798;
    p(3) = 0.7450;
    p(4) = 0.7081;
    set(gca, 'pos', p);
    
    p = get(gcf, 'Position');
    p(3) = 3 * p(3);
    set(gcf, 'Position', p);
    
    p = get(gcf, 'PaperPosition');
    p(3) = 3 * p(3);
    set(gcf, 'PaperPosition', p);
    
    a = axis();
    a(1) = t_min - 0.5;
    a(2) = t_max + 0.5;
    a(3) = -4;
    a(4) = max([max(j) 0]) + 5;
    axis(a);
end