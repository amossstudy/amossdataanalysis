function preprocess_location(varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 04-Dec-2014

    variant = get_argument('variant', '', varargin{:});
    combine_variants = get_argument({'combinevariants', 'combine_variants'}, {}, varargin{:});
    
    if ~isempty(variant)
        variant = ['_' variant];
    end
    
    for file = get_participants(varargin{:})'
        fprintf('Preprocessing %s\n', file.name)

        if ~isempty(combine_variants)
            data_in = struct();
            data_in.valid = false;
            data_in.variant_data = struct();
            data_in.variant_names = cell(length(combine_variants), 1);

            combination_description = '_combined';

            total_data = 0;

            for i = 1:length(combine_variants)
                this_variant = combine_variants{i};
                this_variant_name = this_variant;

                if isempty(this_variant)
                    this_variant_name = 'std';
                end

                combination_description = [combination_description '_' this_variant_name]; %#ok<AGROW>

                data_in.variant_data.(this_variant_name) = load_location_data(file.name, varargin{:}, 'variant', this_variant);

                if ~isempty(data_in.variant_data.(this_variant_name)) && ~isempty(data_in.variant_data.(this_variant_name).time)
                    data_in.valid = true;
                    data_in.variant_data.(this_variant_name).valid = true;
                    total_data = total_data + length(data_in.variant_data.(this_variant_name).time);
                else
                    if ~isstruct(data_in.variant_data.(this_variant_name))
                        data_in.variant_data.(this_variant_name) = struct();
                    end
                    data_in.variant_data.(this_variant_name).valid = false;
                end
                data_in.variant_data.(this_variant_name).index = i;

                data_in.variant_names{i} = this_variant_name;
            end

            if isempty(variant)
                variant = combination_description;
            end

            if data_in.valid == true
                data_in.time = NaN(total_data, 1);
                data_in.lat = NaN(total_data, 1);
                data_in.lon = NaN(total_data, 1);
                data_in.variant_index = NaN(total_data, 1);

                i = 1;
                for this_variant_cell = data_in.variant_names'
                    this_variant_name = this_variant_cell{:};
                    if data_in.variant_data.(this_variant_name).valid && ~isempty(data_in.variant_data.(this_variant_name).time)
                        i_end = i + length(data_in.variant_data.(this_variant_name).time) - 1;
                        data_in.time(i:i_end) = data_in.variant_data.(this_variant_name).time;
                        data_in.lat(i:i_end) = data_in.variant_data.(this_variant_name).lat;
                        data_in.lon(i:i_end) = data_in.variant_data.(this_variant_name).lon;

                        data_in.variant_index(i:i_end) = data_in.variant_data.(this_variant_name).index;

                        i = i_end + 1;
                    end
                end

                [data_in.time, t_sort_order] = sort(data_in.time);
                data_in.lat = data_in.lat(t_sort_order);
                data_in.lon = data_in.lon(t_sort_order);
                data_in.variant_index = data_in.variant_index(t_sort_order);
                data_in.variants = length(fieldnames(data_in.variant_data));
            end
        else
            data_in = load_location_data(file.name, varargin{:});
            data_in.valid = (~isempty(data_in) && ~isempty(data_in.time));
            data_in.variants = 0;
        end

        data = [];

        if data_in.valid
            data = struct();
            data.time = data_in.time;

            location_data = [data_in.lat, data_in.lon];

            if sum(size(location_data)) > 0
                global_mode = mode(location_data, 1);

                location_data(:,1) = location_data(:,1) - global_mode(1);
                location_data(:,2) = location_data(:,2) - global_mode(2);

                % In Oxford (51�45'07"N 1�15'28"W):
                %  Length Of A Degree Of Latitude In Meters: 111248.23835493479
                %  Length Of A Degree Of Longitude In Meters: 70197.65060613726
                % From http://www.csgnetwork.com/degreelenllavcalc.html

                location_data(:,1) = location_data(:,1) * 111.25; %km
                location_data(:,2) = location_data(:,2) * 70.2; %km

                % round to the nearest 0.1km
                location_data_rounded = round(location_data * 10) / 10;
            end

            data.original.lat = data_in.lat;
            data.original.lon = data_in.lon;

            data.lat = location_data_rounded(:,1);
            data.lon = location_data_rounded(:,2);

            data.data = sqrt(sum(location_data_rounded.^2,2));

            valid_days = (data.time >= datenum([2014, 01, 01]));

            data_valid = struct;
            data_valid.lat = location_data(valid_days, 1);
            data_valid.lon = location_data(valid_days, 2);
            data_valid.time = data.time(valid_days);

            location_data_valid = [data_valid.lat, data_valid.lon];

            data_days = floor(data_valid.time);
            data_time_of_day = rem(data_valid.time, 1);
            data_days_unique = unique(data_days);

            reset_days = sort(datenum(preprocess_data_get_property('location', file.name, 'reset', varargin{:})));
            ignore_days = sort(datenum(preprocess_data_get_property('location', file.name, 'ignore', varargin{:})));

            if sum(reset_days < 600000) > 0
                reset_days(reset_days < 600000) = reset_days(reset_days < 600000) + data_days_unique(1) - 1;
            end
            if sum(ignore_days < 600000) > 0
                ignore_days(ignore_days < 600000) = ignore_days(ignore_days < 600000) + data_days_unique(1) - 1;
            end

            reporting = load_reporting_data(file.name, varargin{:});

            if ~isempty(reporting) && isstruct(reporting)
                reporting_versions_idx = strcmp([reporting.setting], 'version');
                reporting_versions = cellfun(@str2num, reporting.value(reporting_versions_idx));
                reporting_versions_upgraded = reporting_versions >= 1031;

                if sum(reporting_versions_upgraded) > 0
                    upgrade_idx = find(diff([0; reporting_versions_upgraded]), 1, 'last');

                    reporting_versions_idx = find(reporting_versions_idx);

                    upgrade_time = reporting.time(reporting_versions_idx(upgrade_idx));

                    reset_days = [reset_days; floor(upgrade_time)]; %#ok<AGROW>
                end
            end
            
            reset_days = sort(reset_days);
            
            if data_in.variants > 0
                data_window_days = repmat(data_days_unique, data_in.variants, 1);
                data_variant_index = data_in.variant_index(valid_days);
            else
                data_window_days = data_days_unique;
            end

            day_night_mode = NaN(length(data_window_days), 2);

            for d = 1:length(data_days_unique)
                day = data_days_unique(d);

                data_window = data_days == day ...
                    & data_time_of_day >= (2 / 24) & data_time_of_day <= (7 / 14);

                if sum(data_window) > 0
                    if data_in.variants > 0
                        for v = 1:data_in.variants
                            data_window_variant = data_window & data_variant_index == v;

                            day_night_mode(d + ((v - 1) * length(data_days_unique)), :) = mode(location_data_rounded(data_window_variant, :), 1);
                        end
                    else
                        day_night_mode(d, :) = mode(location_data_rounded(data_window, :), 1);
                    end
                end
            end

            preprocess_windows = struct();
            
            preprocess_windows.window_min = zeros(length(reset_days) + 1, 1);
            preprocess_windows.window_max = zeros(length(reset_days) + 1, 1);
            
            preprocess_windows.window_min(1) = min(data_days_unique);
            preprocess_windows.window_max(end) = max(data_days_unique);
            
            for i = 1:length(reset_days)
                preprocess_windows.window_max(i) = reset_days(i) - 1;
                preprocess_windows.window_min(i + 1) = reset_days(i) + 1;
            end
            
            day_window_idx = NaN(size(data_days_unique));
            
            data_valid_times = true(size(data_days));

            for i = 1:length(preprocess_windows.window_min)
                day_window_idx((data_days_unique >= preprocess_windows.window_min(i)) & ...
                    data_days_unique <= preprocess_windows.window_max(i)) = i;
            end
            
            window_size = 7 * 4;

            for d = 1:length(data_days_unique)
                if isnan(day_window_idx(d))
                    data_valid_times(data_days == data_days_unique(d)) = false;
                else
                    data_window = (day_window_idx == day_window_idx(d));

                    window_night_modes = day_night_mode(data_window, :);

                    window_night_modes_valid = (sum(isnan(window_night_modes), 2) == 0);

                    if sum(window_night_modes_valid) == 0
                        data_valid_times(data_days == data_days_unique(d)) = false;
                    else
                        day_index = data_days == data_days_unique(d);
                        local_mode = mode(window_night_modes(window_night_modes_valid, :), 1);
                        location_data_valid(day_index, 1) = location_data_valid(day_index, 1) - local_mode(1);
                        location_data_valid(day_index, 2) = location_data_valid(day_index, 2) - local_mode(2);
                    end
                end
            end

            data.normalised = struct();
            data.normalised.time = data_valid.time(data_valid_times);
            data.normalised.data = sqrt(sum(location_data_valid(data_valid_times, :).^2,2));
            data.normalised.lat = location_data_valid(data_valid_times, 1);
            data.normalised.lon = location_data_valid(data_valid_times, 2);
            
            data.normalised.differentiated = calculate_location_differential(data.normalised);

            if data_in.variants > 0
                data.normalised.data_variant_index = data_variant_index(data_valid_times);
            end

            % round to the nearest 0.5km
            data_valid.lat = round(data_valid.lat * 2) / 2;
            data_valid.lon = round(data_valid.lon * 2) / 2;

            location_time = [diff(data_valid.time); 0];

            additional_data = struct;
            additional_data.lat = [];
            additional_data.lon = [];
            additional_data.time = [];

            % Fill gaps larger than 30 mins and less than 10 hours
            for t = find(location_time > (1/48) & location_time < 10/24)'
                if (abs(data_valid.lat(t) - data_valid.lat(t + 1)) <= 3) && ...
                        (abs(data_valid.lon(t) - data_valid.lon(t + 1)) <= 3)
                    t_new = data_valid.time(t) + 15 / (24 * 60);
                    while t_new < data_valid.time(t + 1)
                        additional_data.time = [additional_data.time; t_new];
                        additional_data.lat = [additional_data.lat; data_valid.lat(t)];
                        additional_data.lon = [additional_data.lon; data_valid.lon(t)];
                        t_new = t_new + 15 / (24 * 60);
                    end
                end
            end

            data_filled = merge_location(data_valid, additional_data);

            fprintf('  Pre-processing data: ');
            tic;

            data_days = floor(data_filled.time);
            data_time_of_day = rem(data_filled.time, 1);
            data_days_unique = unique(data_days);

            location_time = [diff(data_filled.time); 0];
            location_time = min(location_time, 1/(24 * 4));

            location_data_filled = [data_filled.lat, data_filled.lon];
            location_data_validated = NaN(size(location_data_filled));

            data_filled.data = sqrt(sum(location_data_filled.^2,2));

            data_days_unique(ismember(data_days_unique, reset_days)) = 0;
            data_days_unique(ismember(data_days_unique, ignore_days)) = 0;

            valid_days = ~ismember(data_days, reset_days) & ~ismember(data_days, ignore_days);

            locations_unique = [];
            locations_unique_time = [];

            for day = data_days_unique'
                if day == 0
                    continue;
                end

                d_min = max([max(reset_days(reset_days < day)) (day - floor(window_size / 2))]);
                d_max = min([min(reset_days(reset_days > day)) (day + floor(window_size / 2))]);

                data_window = data_filled.time >= d_min & data_filled.time <= d_max ...
                    & data_time_of_day >= (2 / 24) & data_time_of_day <= (7 / 14) ...
                    & valid_days;

                day_idx = (data_days == day);
                % Remove days where we can't calculate mode or where we have
                % less than 12 hours of data.
                if sum(data_window) > 0 && sum(location_time(day_idx)) > 12 / 24
                    local_mode = mode(location_data_filled(data_window, :), 1);

                    location_data_validated(day_idx, 1) = location_data_filled(day_idx, 1) - local_mode(1);
                    location_data_validated(day_idx, 2) = location_data_filled(day_idx, 2) - local_mode(2);

                    day_locations = location_data_validated(day_idx, :);
                    day_location_time = location_time(day_idx);
                    [day_locations_unique, ~, day_locations_idx] = unique(day_locations, 'rows');

                    for location_idx = 1:size(day_locations_unique, 1)
                        day_location_duration = sum(day_location_time(day_locations_idx == location_idx));

                        if day_location_duration >= 1/24
                            if ~isempty(locations_unique)
                                locations_unique_idx = ismember(locations_unique, day_locations_unique(location_idx, :), 'rows');
                            else
                                locations_unique_idx = 0;
                            end
                            if sum(locations_unique_idx) > 0
                                locations_unique_time(locations_unique_idx) = locations_unique_time(locations_unique_idx) + day_location_duration; %#ok<AGROW>
                            else
                                locations_unique = [locations_unique; day_locations_unique(location_idx, :)]; %#ok<AGROW>
                                locations_unique_time = [locations_unique_time; day_location_duration]; %#ok<AGROW>
                            end
                        end
                    end
                end
            end

            data.validated.time = data_filled.time;
            data.validated.data = sqrt(sum(location_data_validated.^2,2));
            data.validated.lat = location_data_validated(:, 1);
            data.validated.lon = location_data_validated(:, 2);

            toc;

            fprintf('  Generating location groups: ');
            tic;

            data.validated.location_group = zeros(size(data.validated.time));
            data.location_centers = [];

            location_groups = zeros(size(locations_unique_time));

            g = 1;

            while sum(location_groups == 0) > 0
                if g > 1
                    p_all = locations_unique_time .* (location_groups == 0);
                    p_all = p_all / sum(p_all);
                    location_i = sum(cumsum(p_all) <= rand(1)) + 1;
                else
                    location_i = find(ismember(locations_unique, [0, 0], 'rows') == 1);
                end

                location = locations_unique(location_i, :);

                location_range = (locations_unique(:, 1) >= location(1) - 2.2) & (locations_unique(:, 1) <= location(1) + 2.2) & ...
                    (locations_unique(:, 2) >= location(2) - 2.2) & (locations_unique(:, 2) <= location(2) + 2.2) & ...
                    (location_groups == 0);

                locations_in_range = locations_unique(location_range, :);

                location_groups(location_range) = g;

                data.validated.location_group(ismember(location_data_validated, locations_in_range, 'rows')) = g;

                data.location_centers = [data.location_centers; location];

                g = g + 1;
            end
            
            toc;
        else
            fprintf('  No data loaded\n');
        end

        save_data([file.name '-location' variant '-preprocessed'], data, varargin{:});
    end
end