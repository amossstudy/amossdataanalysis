function data = load_blood_pressure_data(participant, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 10-May-2016

    paths = get_paths(varargin{:});
    
    data = [];
    
    working_filename = [participant '-blood-pressure.mat'];
    
    if (paths.prefer_working) && (exist([paths.working_path working_filename], 'file') == 2)
        if ~isempty(paths.temp_path) && (exist(paths.temp_path, 'dir') == 7) && ...
                exist([paths.temp_path working_filename], 'file') == 2
            fprintf('  Loading %s (from temp): ', working_filename);
            tic; data_loaded = load([paths.temp_path working_filename]); toc;
        else
            fprintf('  Loading %s: ', working_filename);
            tic; data_loaded = load([paths.working_path working_filename]); toc;
        end
        data = data_loaded.data;
    else
        first_load = true;
        
        input_path = [paths.processed_data_path participant '/'];
        
        for file = dir([input_path 'blood-pressure*.csv'])'
            if strcmpi(file.name, 'blood-pressure.csv') || ~isempty(regexpi(file.name, 'blood-pressure-[0-9][0-9][0-9][0-9]-[0-9][0-9].csv'))
                fprintf('  Loading %s: ', file.name);
                tic; data_new = load_blood_pressure_file([input_path file.name]); toc;
                if first_load
                    data = data_new;
                    first_load = false;
                else
                    data = merge_blood_pressure(data, data_new);
                end
            end
        end
    end
end
