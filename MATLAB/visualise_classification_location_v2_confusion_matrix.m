function visualise_classification_location_v2_confusion_matrix(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Aug-2016

    close all;
    
    params = get_configuration('features_location_v2', varargin{:});
    
    classification_data = load_classification_data_location_v2(varargin{:});
    
    [classification_params, classification_results, ~, preds] = preprocess_classification_data_location_v2(classification_data);
    
    prediction_types_with_reps = {'loo_eq', 'ind_features', 'pca'};
    prediction_types_fs = {'loo_fs', 'cv_5f_fs', 'cv_10f_fs', 'time_fs'};
    
    for type_cell = fieldnames(classification_results)'
        if strcmp(type_cell, 'meta')
            continue;
        end
        
        fprintf('Test: %s\n', type_cell{:});
        
        for t = classification_params.TESTS_TO_PERFORM

            if ismember(type_cell, [prediction_types_with_reps prediction_types_fs])
                q_all = [classification_params.Q, classification_params.Q_MAX + classification_params.Q_EXTRA_MAX];
                q_to_display = ismember(q_all, [6, 10, 11, classification_params.Q_MAX + classification_params.Q_EXTRA_TO_TEST]);
                for q = find(q_to_display & classification_results.meta.q_valid(t, :))
                    fprintf(' Q %s %i\n', char(8805), classification_params.Q(q));
                    
                    str = cell(classification_params.FEATURES, 1);
                    
                    for f = 1:min(classification_params.FEATURES, size(classification_data.results.feature_rank, 2))
                        if strcmp(type_cell, 'ind_features')
                            str{f} = params.feature_short{params.feature_order(f)};
                        elseif strcmp(type_cell, 'pca')
                            str{f} = sprintf('PC%i', f);
                        else
                            str{f} = params.feature_short{classification_data.results.feature_rank(q, f)};
                        end
                        
                        fprintf('  Feature %i (%s)\n', f, str{f});
                        classes_unique = sort(unique(preds.(type_cell{:}){t, q, f}.classes));
                        class_count = NaN(numel(classes_unique));
                        s_len = numel(num2str(numel(preds.(type_cell{:}){t, q, f}.pred)));
                        
                        for c1 = 1:numel(classes_unique)
                            c_true = classes_unique(c1);
                            c_idx = preds.(type_cell{:}){t, q, f}.classes == c_true;
                            c_pred = preds.(type_cell{:}){t, q, f}.pred(c_idx, :);
                            
                            if c_true == 1
                                fprintf('   Q < %i\t:', classification_params.Q(q));
                            elseif c_true == 2
                                fprintf('   Q %s %i\t:', char(8805), classification_params.Q(q));
                            end

                            for c2 = 1:numel(classes_unique)
                                class_count(c1, c2) = sum(sum(c_pred == classes_unique(c2)));
                                s = num2str(class_count(c1, c2));
                                fprintf('  %s%s', repmat(' ', 1, s_len - numel(s)), s);
                            end
                            
                            s = num2str(sum(class_count(c1, :)));
                            fprintf(' |  %s%s  |', repmat(' ', 1, s_len - numel(s)), s);
                            
                            for c2 = 1:numel(classes_unique)
                                fprintf('  %.3f', class_count(c1, c2) / sum(class_count(c1, :)));
                            end
                            
                            fprintf('\n');
                        end
                    end
                end
            end
        end
    end
end
