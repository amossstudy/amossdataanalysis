function params = get_kalman_filter_params_location_v2(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 13-May-2016

    kalman_window_backwards = get_argument({'kalman_window_backwards', 'kalmanwindowbackwards'}, 3, varargin{:});
    kalman_window_forwards = get_argument({'kalman_window_forwards', 'kalmanwindowforwards'}, 3, varargin{:});
    
    [data_no_window_raw, prop_no_window] = load_features_location_v2(varargin{:}, 'average_data_backwards', 0, 'average_data_forwards', 0, 'kalman_filter', false);
    [data_window_raw, ~]                 = load_features_location_v2(varargin{:}, 'average_data_backwards', kalman_window_backwards, 'average_data_forwards', kalman_window_forwards, 'kalman_filter', false);

    params = struct();
    
    FEATURES = size(data_no_window_raw, 1);
    
    diff_weeks_range = 1:20;
    
    params.R = NaN(FEATURES, 1);
    params.Q = NaN(FEATURES, numel(diff_weeks_range));
    
    for f_i = 1:FEATURES
        this_f_data_diff = data_no_window_raw(f_i, :) - data_window_raw(f_i, :);

        params.R(f_i) = nanvar(this_f_data_diff);
        
        for w = 1:numel(diff_weeks_range)
            diff_weeks = diff_weeks_range(w);
            
            this_week_sequential_diff = NaN(size(data_no_window_raw, 2), 1);
            
            for i = (diff_weeks + 1):size(data_no_window_raw, 2)
                if strcmp(prop_no_window.participant{i}, prop_no_window.participant{i - diff_weeks})
                    if (prop_no_window.week_start(i) - prop_no_window.week_start(i - diff_weeks) == (7 * diff_weeks))
                        this_week_sequential_diff(i) = data_window_raw(f_i, i) - data_window_raw(f_i, i - diff_weeks);
                    end
                end
            end
            params.Q(f_i, w) = nanvar(this_week_sequential_diff(:));
        end
    end
end
