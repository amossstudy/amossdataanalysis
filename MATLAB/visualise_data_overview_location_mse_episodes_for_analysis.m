function visualise_data_overview_location_mse_episodes_for_analysis(participant, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 14-May-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location-episode-epochs/mse';
    EPOCHS_PER_GRAPH = 10;
    
    mse = load_location_mse(participant, varargin{:});
    
    if isstruct(mse) && isfield(mse, 'episodes') && isfield(mse.episodes, 'start') && ~isempty(mse.episodes.start)
        
        [~, epochs_sorted_idx] = sort([mse.episodes.start]);
        
        for i = 1:EPOCHS_PER_GRAPH:length(epochs_sorted_idx)
            close all;
            
            epochs_beginning = mse.episodes.start(epochs_sorted_idx(i));
            
            i_max = min(i + EPOCHS_PER_GRAPH - 1, length(epochs_sorted_idx));
            
            epochs_to_plot = zeros(1, EPOCHS_PER_GRAPH);
            epochs_to_plot(1:(i_max - i + 1)) = epochs_sorted_idx(i:i_max);
            
            fprintf(['  Plotting epochs beginning ' datestr(epochs_beginning, 'dd-mmm-yyyy') ': ']);
            tic;
            plot_location_mse_epoch(mse.episodes, epochs_to_plot, varargin{:})
            toc;
            
            fprintf('  Saving: ');
            tic;
            save_figure([participant '-' datestr(epochs_beginning, 'yyyy-mm-dd')], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
            toc;
         end
    else
        fprintf('  No data loaded\n');
    end
end