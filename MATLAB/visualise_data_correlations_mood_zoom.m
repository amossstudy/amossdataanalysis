function visualise_data_correlations_mood_zoom(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Mar-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-correlations';
    
    files = get_participants(varargin{:});
    
    data_dates = [2014, 1, 1; 2015, 3, 1];
    
    data_class = cell(3, 1);
    for i = 1:3
      data_class{i} = struct('j', 1, 'combined', struct('j', 1));
    end
    
    data_moods = struct;

    for file = files'
        fprintf('Loading %s\n', file.name)

        class = get_participant_property(file.name, 'classification', varargin{:});

        if ~isempty(class)
            data_mz = load_mood_zoom_data(file.name);

            if ~isempty(data_mz)
                combined_j = data_class{class + 1}.combined.j;
                for mood = fieldnames(data_mz)'
                    if strcmp(mood, 'time') == 0
                        data_moods.(mood{:}) = true;
                        if ~isfield(data_class{class + 1}, mood)
                            data_class{class + 1}.(mood{:}) = NaN(length(files), (datenum(data_dates(2,:)) - datenum(data_dates(1,:))) * 10);
                        end
                        if ~isfield(data_class{class + 1}.combined, mood)
                            data_class{class + 1}.combined.(mood{:}) = NaN(length(files) * (datenum(data_dates(2,:)) - datenum(data_dates(1,:))) * 10, 1);
                        end
                        
                        this_mood_data = data_mz.(mood{:});

                        if ~isempty(this_mood_data)
                            data_range = 1:length(this_mood_data);

                            data_class{class + 1}.(mood{:})(data_class{class + 1}.j, data_range) = this_mood_data;
                            
                            data_class{class + 1}.combined.(mood{:})(combined_j:(combined_j + length(this_mood_data) - 1)) = this_mood_data;
                            
                            data_class{class + 1}.combined.j = max(data_class{class + 1}.combined.j, combined_j + length(this_mood_data));
                        end
                    end
                end
                data_class{class + 1}.j = data_class{class + 1}.j + 1;
            end
        end
    end

    group_labels = {'Combined', 'HC', 'BD', 'BPD'};
    
    data_moods_list = fieldnames(data_moods);
    moods = numel(data_moods_list);
    
    data_moods_str = data_moods_list;
    
    for i = 1:moods
        mood_str = data_moods_str{i};
        mood_str(1) = upper(mood_str(1));
        data_moods_str{i} = mood_str;
    end
    
    data_total = 0;
    
    for i = 1:3
        data_total = data_total + data_class{i}.combined.j - 1;
    end
    
    data_matrix_all = NaN(data_total, moods);
    data_matrix_all_j = 1;
    
    data_matrix_class = cell(3, 1);
    
    for i = 1:3
        data_matrix_class{i} = NaN(data_class{i}.combined.j - 1, moods);
        
        for m = 1:moods
            mood = data_moods_list{m};
            data_matrix_class{i}(:, m) = data_class{i}.combined.(mood)(1:(data_class{i}.combined.j - 1), :);
            
            data_matrix_all(data_matrix_all_j:(data_matrix_all_j + data_class{i}.combined.j - 2), m) = data_class{i}.combined.(mood)(1:(data_class{i}.combined.j - 1), :);
        end
        data_matrix_all_j = data_matrix_all_j + data_class{i}.combined.j - 1;
    end
    
    for i = 0:3
        if i == 0
            data_matrix = data_matrix_all;
        else
            data_matrix = data_matrix_class{i};
        end
        
        corr_matrix = corr(data_matrix, 'Type', 'Spearman');
        
        corr_matrix(logical(eye(length(corr_matrix)))) = zeros(length(corr_matrix), 1);
        
        figure;
        
        imagesc(corr_matrix);
        
        for j = 1:moods
            for k = 1:moods
                if j ~= k
                    text(j, k, num2str(round(corr_matrix(k, j) * 10000)/10000, 2), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FontSize',16)
                end
            end
        end
        
        set(gca, 'yticklabel', data_moods_str);
        set(gca, 'xticklabel', data_moods_str);
        
        clr = [spring(64); flipud(autumn(64))];
        
        set(gca, 'clim', [-1, 1])
        
        colormap(clr);
        
        colorbar;
        
        title(['Mood Zoom Correlations (' group_labels{i + 1} ')']);
        
        save_figure(['mood_zoom_correlations_' group_labels{i + 1}], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    end
end
