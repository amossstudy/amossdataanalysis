function visualise_features_location_v2_individual_range(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 12-Oct-2014

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats/individuals/range';
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    data_full = data_raw';
    
    FEATURES = size(data_full, 2);
    
    valid_qids = ~isnan(classification_properties.QIDS);
    
    params = get_configuration('features_location_v2', varargin{:});
    
    participant_id = classification_properties.participant(valid_qids);
    participant_qids_score = classification_properties.QIDS(valid_qids);
    participant_class = classification_properties.class(valid_qids);
    
    data_normalised = data_full(valid_qids, :);
    
    for i = 1:FEATURES
        data_normalised(:, i) = data_normalised(:, i) - nanmean(data_normalised(:, i));
        data_normalised(:, i) = data_normalised(:, i) / nanstd(data_normalised(:, i));
    end
    
    unique_participants = unique(participant_id);
    
    max_points = 0;
    
    for participant_cell = unique_participants'
        max_points = max(max_points, sum(strcmp(participant_id, participant_cell)));
    end
    
    data_values = NaN(length(unique_participants), max_points, FEATURES + 1);
    data_class = NaN(length(unique_participants), 1);
    data_range = NaN(length(unique_participants), FEATURES + 1);
    data_points = NaN(length(unique_participants), 1);
    
    for p_i = 1:numel(unique_participants)
        participant_data_idx = strcmp(participant_id, unique_participants{p_i});
        
        for i = 1:(FEATURES + 1)
            if i == 1
                all_points = participant_qids_score(participant_data_idx);
            else
                all_points = data_normalised(participant_data_idx, i - 1);
            end
            
            data_values(p_i, 1:numel(all_points), i) = all_points;
            
            data_range(p_i, i) = range(all_points);
        end
        
        data_class(p_i) = participant_class(find(participant_data_idx, 1, 'first'));
        data_points(p_i) = sum(participant_data_idx);
    end
    
    classes_unique = unique(data_class);
    
    first_class_idx = zeros(numel(classes_unique), 1);
    
    for c_i = 1:numel(classes_unique);
        first_class_idx(c_i) = find(data_class == classes_unique(c_i), 1, 'first');
    end
    
    figure;
    
    boxplot(data_values(:, :, 1)', 'Colors', params.clr_cohort(data_class + 2, :), 'plotstyle', 'compact');
    
    h = flipud(findall(gca,'Tag','Box'));
    
    legend(h(first_class_idx), params.test_description(classes_unique + 2), 'Location', 'North', 'Orientation', 'horizontal')
    
    set(gca, 'xtick', 5:5:numel(unique_participants));
    set(gca, 'xticklabel', 5:5:numel(unique_participants));
    set(gca, 'ylim', [0 30])
    
    xlabel('Participant');
    ylabel('QIDS Score');
    
    save_figure('qids_range_unsorted', 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 20, varargin{:});
    
    [~, sort_idx] = sortrows([data_class data_range(:, 1)]);
    
    for i = 1:(FEATURES + 1)
        figure;
        
        boxplot(data_values(sort_idx, :, i)', 'Colors', params.clr_cohort(data_class(sort_idx) + 2, :), 'plotstyle', 'compact');
        
        h = flipud(findall(gca,'Tag','Box'));
        
        legend(h(ismember(sort_idx, first_class_idx)), params.test_description(classes_unique + 2), 'Location', 'North', 'Orientation', 'horizontal')
        
        set(gca, 'xtick', 5:5:numel(unique_participants));
        set(gca, 'xticklabel', 5:5:numel(unique_participants));
        
        if i == 1
            set(gca, 'ylim', [0 30])
            ylabel('QIDS Score');
            
            filename_prefix = 'qids_range';
            filename_suffix = '';
        else
            set(gca, 'ylim', [-4 4])
            ylabel(params.feature_name{i - 1});
            
            filename_prefix = 'feature_range_qids';
            filename_suffix = sprintf('_feature_%.2i', i - 1);
        end
        
        xlabel('Participant');
        
        save_figure(sprintf('%s_sorted%s', filename_prefix, filename_suffix), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 20, varargin{:});
    end
    
    valid_participants_idx = (data_points >= 5);
    
    for i = 1:FEATURES
        figure;
        
        h = cell(numel(classes_unique), 1);
        for c_i = 1:numel(classes_unique);
            this_valid_participants_idx = valid_participants_idx & (data_class == classes_unique(c_i));
            h{c_i} = scatter(data_range(this_valid_participants_idx, 1), data_range(this_valid_participants_idx, i + 1), [], params.clr_cohort(data_class(this_valid_participants_idx) + 2, :), params.symbol_cohort{classes_unique(c_i) + 2}, 'filled');
            if c_i == 1
                hold on;
            end
        end
        hold off;
        
        legend([h{:}], params.test_description(classes_unique + 2), 'Location', 'NorthEast')
        
        xlabel('QIDS Score Range');
        
        set(gca, 'ylim', [0 5])
        
        ylabel([params.feature_name{i} ' Range']);
        
        save_figure(sprintf('range_correlation_qids_feature_%.2i', i), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 20, varargin{:});
        
        fprintf('%s Correlation: %.4f\n', params.feature_short{i}, corr(data_range(valid_participants_idx, 1), data_range(valid_participants_idx, i + 1), 'type', 'Spearman'));
    end
end
