function save_data(filename, data, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 18-Mar-2016

    if isempty(inputname(2))
        warning(sprintf('save_data must be called with a variable containing the data.\nData has not been saved.')) %#ok<SPWRN>
    else
        if get_argument({'save_data', 'savedata'}, true, varargin{:})
            path_type = get_argument({'path_type', 'pathtype', 'root_path_type', 'rootpathtype'}, 'working_path', varargin{:});
            split_struct = get_argument({'split_struct', 'splitstruct'}, false, varargin{:});
            mat_file_version = get_argument({'mat_file_version', 'matfileversion'}, '-v7.3', varargin{:});

            paths = get_paths(varargin{:});

            output_path = paths.(path_type);

            if split_struct
                data_to_save = data; %#ok<NASGU>
            else
                data_to_save = struct();
                data_to_save.(inputname(2)) = data; %#ok<STRNU>
            end

            if exist(fileparts([output_path filename]), 'dir') ~= 7
                mkdir(fileparts([output_path filename]));
            end

            save([output_path filename], '-struct', 'data_to_save', mat_file_version);
        end
    end
end
