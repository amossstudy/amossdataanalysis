function visualise_data_overview_location(participant, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    close all;
    
    OUTPUT_SUB_DIR = 'data-overview';
    
    data = load_location_data(participant, varargin{:});
    
    %%
    if ~isempty(data)
        %%
        fprintf('  Anonymizing data : ');
        tic;
        location_data = [data.lat, data.lon];
        
        if sum(size(location_data)) > 0
            loc_home = mode(location_data, 1);
        
            location_data(:,1) = location_data(:,1) - loc_home(1);
            location_data(:,2) = location_data(:,2) - loc_home(2);
        
            scatter(location_data(:,1), location_data(:,2));
        end
        
        data.data = sqrt(sum(location_data.^2,2));
        toc;
        %%
        
        if ~isempty(data.data)
            figure
            stairs(data.time, data.data);
        end
        
        %%
        % In case there is overlap between different data files. Usually
        % this would not be required.
        fprintf('  Downsampling by second : ');
        tic; location_ds = downsample_second(data); toc;
        %%
        fprintf('  Downsampling by epoch : ');
        tic; location_ds2 = downsample_epoch(location_ds, 20, 0); toc;

        fprintf('  Interleaving : ');
        tic; location_ds3 = interleave_epoch(location_ds2, varargin{:}); toc;

        %%
        fprintf('  Plotting : ');
        tic; plot_location(location_ds3, varargin{:}); toc;
        %%
        fprintf('  Saving : ');
        tic;
        save_figure([participant '-location'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        save_data([participant '-location'], location_ds3, 'root_path_type', 'output_path', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
        %%
        fprintf('  Downsampling (simple) : ');
        tic; location_ds_simple = downsample_epoch(location_ds, 30, 0); toc;
        fprintf('  Interleaving (simple) : ');
        tic; location_ds_simple3 = interleave_epoch(location_ds_simple); toc;
        
        fprintf('  Plotting (simple) : ');
        tic; plot_location_simple(location_ds_simple3, varargin{:}); toc;
        %%
        fprintf('  Saving (simple) : ');
        tic;
        save_figure([participant '-location-simple'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        save_data([participant '-location-simple'], location_ds_simple3, 'root_path_type', 'output_path', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
    else
        fprintf('  No data loaded\n');
    end
end