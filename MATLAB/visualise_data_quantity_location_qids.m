function visualise_data_quantity_location_qids(varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 28-Dec-2014

    close all;
    
    OUTPUT_SUB_DIR = 'data-quantity';
    
    files = get_participants(varargin{:});
    
    participant_class = NaN(length(files), 1);
    
    data_dates = datenum([2014, 1, 1]):datenum([2014, 12, 1]);
    
    data_dates_weekly = data_dates - weekday(data_dates) + 2;
    
    tc_data_matrix = NaN(length(files), length(data_dates));
    location_data_matrix = NaN(length(files), length(data_dates));
    
    location_data_days = zeros(length(files), 1);
    location_data_available_range = zeros(length(files), 1);
    location_data_available_valid_days = zeros(length(files), 1);
    location_data_recorded_raw = zeros(length(files), 1);
    location_data_recorded_raw_75 = zeros(length(files), 1);
    location_data_recorded_validated_75 = zeros(length(files), 1);
    
    j = 1;
    
    for file = files'
        fprintf('Loading %s\n', file.name)

        class = get_participant_property(file.name, 'classification', varargin{:});

        if ~isempty(class)
            data_tc = load_true_colours_preprocessed(file.name);
            
            if ~isempty(data_tc) && isfield(data_tc, 'QIDS') && isfield(data_tc.QIDS, 'weekly') && isfield(data_tc.QIDS.weekly, 'time')
                participant_class(j) = class;
                
                for w = data_tc.QIDS.weekly.time
                    tc_data_matrix(j, ismember(data_dates_weekly, w)) = data_tc.QIDS.weekly.data(data_tc.QIDS.weekly.time == w);
                end
                
                data_location = load_location_preprocessed(file.name);
                
                if ~isempty(data_location) && ~isempty(data_location.time) && isfield(data_location, 'regularised')
                
                    location_dates_raw = floor(data_location.time);
                    location_dates_unique_raw = unique(location_dates_raw);

                    % Calculate the total number of days in the data set
                    % for the participant.
                    location_data_min = min(location_dates_raw(location_dates_raw >= data_dates(1)));
                    location_data_max = max(location_dates_raw(location_dates_raw <= data_dates(end)));
                    location_data_range = location_data_min:location_data_max;
                    
                    % Count the number of days within the range that have
                    % some data.
                    location_data_available_range(j) = location_data_max - location_data_min + 1;
                    location_data_available_valid_days(j) = sum(ismember(location_dates_unique_raw, location_data_range));
                    
                    % Get total amount of data recorded.
                    location_data_valid_raw = ismember(location_dates_raw, location_data_range);
                    location_dates_valid_raw = location_dates_raw(location_data_valid_raw);
                    location_time_raw = [diff(data_location.time(location_data_valid_raw)); 0];
                    location_time_raw = min(location_time_raw, 1/(24 * 4)); % Limit to 15 mins
                    location_data_recorded_raw(j) = sum(location_time_raw);
                    
                    % Get days with >75% recorded data.
                    data_daily_recorded_raw = zeros(length(location_dates_unique_raw),1);
                    for d = 1:length(location_dates_unique_raw)
                        data_daily_recorded_raw(d) = sum(location_time_raw(location_dates_valid_raw == location_dates_unique_raw(d)));
                    end
                    location_data_recorded_raw_75(j) = sum(data_daily_recorded_raw > 0.75);
                    
                    location_dates_validated = floor(data_location.validated.time);
                    location_dates_unique_validated = unique(location_dates_validated);
                    
                    % Get total amount of data recorded (validated).
                    location_data_valid_validated = ismember(location_dates_validated, location_data_range);
                    location_dates_valid_validated = location_dates_validated(location_data_valid_validated);
                    location_time_validated = [diff(data_location.validated.time(location_data_valid_validated)); 0];
                    location_time_validated = min(location_time_validated, 1/(24 * 4)); % Limit to 15 mins
                    
                    % Get days with >75% recorded data (validated).
                    data_daily_recorded_validated = zeros(length(location_dates_unique_validated),1);
                    for d = 1:length(location_dates_unique_validated)
                        data_daily_recorded_validated(d) = sum(location_time_validated(location_dates_valid_validated == location_dates_unique_validated(d)));
                    end
                    location_data_recorded_validated_75(j) = sum(data_daily_recorded_validated > 0.75);
                    
                    location_data_dates = floor(data_location.regularised.time);
                    location_data_dates_weekly = floor(data_location.regularised.time) - weekday(data_location.regularised.time) + 2;
                    
                    for w = unique(location_data_dates_weekly)'
                        location_data_matrix(j, ismember(data_dates_weekly, w)) = sum(~isnan(data_location.regularised.data(location_data_dates_weekly == w)));
                        for d = 0:6
                            if sum(~isnan(data_location.regularised.data(location_data_dates == (w + d)))) > 20
                                location_data_days(j) = location_data_days(j) + 1;
                            end
                        end
                    end
                end
                
                j = j + 1;
            end
        end
    end
    
    t_min = data_dates(1);
    t_max = data_dates(end);
    
    [xticks, xticks_str] = get_xticks(t_min, t_max, varargin{:});
    
    tc_data_matrix(isnan(tc_data_matrix)) = -1;
    location_data_matrix(isnan(location_data_matrix)) = -1;
    
    data_matrix_severity = tc_data_matrix;
    data_matrix_severity(ismember(data_matrix_severity, 0:5)) = 0;
    data_matrix_severity(ismember(data_matrix_severity, 6:10)) = 1;
    data_matrix_severity(ismember(data_matrix_severity, 11:15)) = 2;
    data_matrix_severity(ismember(data_matrix_severity, 16:20)) = 3;
    data_matrix_severity(ismember(data_matrix_severity, 21:27)) = 4;
    
    data_range = 1:(j - 1);
    data_order = data_range;
    %data_order = data_order(randperm(length(data_order)));
    
    clr = lines(10);
    clr_light = min(clr .* 1.75, 1);
    
    figure;
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_available_range(data_order); 0], 'Color', clr(1, :), 'LineWidth', 2);
    set(gca, 'xlim', [(data_range(1) - 0.5) (data_range(end) + 0.5)]);
    title('Total days in range');
    a = axis;
    
    save_figure('location_total', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure;
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_available_range(data_order); 0], 'Color', clr_light(1, :), 'LineWidth', 0.5);
    hold on;
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_available_valid_days(data_order); 0], 'Color', clr(2, :), 'LineWidth', 2);
    axis(a)
    title('Days with data');
    
    save_figure('location_days_with_data', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure;
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_available_range(data_order); 0], 'Color', clr_light(1, :), 'LineWidth', 0.5);
    hold on;
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_available_valid_days(data_order); 0], 'Color', clr_light(2, :), 'LineWidth', 0.5);
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_recorded_raw(data_order); 0], 'Color', clr(3, :), 'LineWidth', 2);
    axis(a)
    title('Recorded data');
    
    save_figure('location_recorded_data', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure;
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_available_range(data_order); 0], 'Color', clr_light(1, :), 'LineWidth', 0.5);
    hold on;
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_available_valid_days(data_order); 0], 'Color', clr_light(2, :), 'LineWidth', 0.5);
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_recorded_raw(data_order); 0], 'Color', clr_light(3, :), 'LineWidth', 0.5);
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_recorded_raw_75(data_order); 0], 'Color', clr(4, :), 'LineWidth', 2);
    axis(a)
    title('Recorded data over 75%');
    
    save_figure('location_recorded_data_75', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure;
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_available_range(data_order); 0], 'Color', clr_light(1, :), 'LineWidth', 0.5);
    hold on;
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_available_valid_days(data_order); 0], 'Color', clr_light(2, :), 'LineWidth', 0.5);
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_recorded_raw(data_order); 0], 'Color', clr_light(3, :), 'LineWidth', 0.5);
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_recorded_raw_75(data_order); 0], 'Color', clr_light(4, :), 'LineWidth', 0.5);
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_recorded_validated_75(data_order); 0], 'Color', clr(5, :), 'LineWidth', 2);
    axis(a)
    title('Preprocessed data over 75%');
    
    save_figure('location_preprocessed_data_75', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure;
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; sort(location_data_recorded_validated_75(data_order), 1, 'descend'); 0], 'Color', clr(5, :), 'LineWidth', 2);
    axis(a)
    title('Preprocessed data over 75%');
    
    save_figure('location_preprocessed_data_75_sorted', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure
    histogram(location_data_recorded_validated_75(data_range), 0:28:300)
    xlabel('Days of preprocessed data over 75%');
    title('Preprocessed data over 75% (total)');
    
    save_figure('location_preprocessed_data_75_hist_all', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure
    histogram(location_data_recorded_validated_75(data_range(participant_class == 0)), 0:28:300)
    xlabel('Days of preprocessed data over 75%');
    title('Preprocessed data over 75% (HC)');
    
    save_figure('location_preprocessed_data_75_hist_hc', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure
    histogram(location_data_recorded_validated_75(data_range(participant_class == 1)), 0:28:300)
    xlabel('Days of preprocessed data over 75%');
    title('Preprocessed data over 75% (BD)');
    
    save_figure('location_preprocessed_data_75_hist_bd', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure
    histogram(location_data_recorded_validated_75(data_range(participant_class == 2)), 0:28:300)
    xlabel('Days of preprocessed data over 75%');
    title('Preprocessed data over 75% (BPD)');
    
    save_figure('location_preprocessed_data_75_hist_bpd', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    clr = flipud(hot(6));
    
    valid_locations = data_matrix_severity;
    valid_locations(location_data_matrix < (144 * 5)) = -1;
    
    figure;
    imagesc(data_dates, data_range, valid_locations(data_order, :));
    colormap(clr);
    set(gca, 'xtick', xticks);
    set(gca, 'xticklabel', xticks_str);
    set(gca, 'ytick', []);
    set(gca, 'clim', [-1.5 4.5]);
    hcb = colorbar;
    set(hcb, 'ylim', [-0.5, 4.5]);
    set(hcb, 'ytick', 0:4);
    ylabel(hcb, 'QIDS Severity');
    set(gca, 'FontSize', 30);
    
    save_figure('valid_location_qids_severity_all', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    clr = flipud(gray((144 * 7) + 1));
    
    figure;
    imagesc(data_dates, data_range, location_data_matrix(data_order, :));
    colormap(clr);
    set(gca, 'xtick', xticks);
    set(gca, 'xticklabel', xticks_str);
    set(gca, 'ytick', []);
    set(gca, 'clim', [-1.5 ((144 * 7) + 0.5)]);
    hcb = colorbar;
    set(hcb, 'ylim', [-0.5, ((144 * 7) + 0.5)]);
    set(hcb, 'ytick', 0:144:(144 * 7));
    set(hcb, 'xticklabel', 0:7);
    ylabel(hcb, 'Preprocessed Data Available (days)');
    set(gca, 'FontSize', 30);
    
    save_figure('location_all', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure;
    stairs([0 (data_range - 0.5) (data_range(end) + 0.5)], [0; location_data_days(data_order); 0]);
    set(gca, 'xlim', [(data_range(1) - 0.5) (data_range(end) + 0.5)]);
    
    save_figure('location_simple_all', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
end
