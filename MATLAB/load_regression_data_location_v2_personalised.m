function [regression_data] = load_regression_data_location_v2_personalised(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 07-Oct-2015

    paths = get_paths(varargin{:});
    
    variant = get_argument({'variant'}, '', varargin{:});

    filename_base = get_argument({'filename_base', 'filenamebase'}, 'regression-location-v2-personalised', varargin{:});
    variant_desc = '';
    variant_ext = '';
    
    filename_filter = [filename_base '*'];
    if ~isempty(variant)
        variant_ext = ['_' variant];
        filename_filter = [filename_filter variant_ext];
        variant_desc = sprintf(' with variant ''%s''', variant);
    end
    
    max_file = [];
    max_file_date_time = 0;
    
    for file = dir([paths.working_path filename_filter '.mat'])'
        if ~isempty(regexpi(file.name, [filename_base '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' variant_ext '.mat']))
            [file_date_time_string, ~] = regexp(file.name, [filename_base '-([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])' variant_ext '.mat'],'tokens','match');

            file_date_time = datenum(file_date_time_string{1}, 'yyyymmddHHMMSS');

            if file_date_time > max_file_date_time
                max_file = file.name;
                max_file_date_time = file_date_time;
            end
        end
    end
    
    assert(~isempty(max_file), 'No classification data found%s.', variant_desc);
    
    fprintf('Loading classification data%s from %s: ', variant_desc, datestr(max_file_date_time, 'dd/mm/yyyy HH:MM:SS'));

    regression_data = struct();

    data = load([paths.working_path max_file]);
    
    if isfield(data, 'regression_data')
        regression_data = data.regression_data;
    elseif isfield(data, 'participant_stats')
        regression_data = data.participant_stats;
    end
    
    fprintf('done\n');
    
    assert(numel(fieldnames(regression_data)) > 0, 'No data loaded.');
end
