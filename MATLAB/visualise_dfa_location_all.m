function visualise_dfa_location_all(varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 23-Dec-2014

    close all;
    
    paths = get_paths(varargin{:});
    
    root_path = paths.processed_data_path;
    
    figure;
    
    for file = get_participants(varargin{:})'
        fprintf('Adding trace for %s: ', file.name)
        if exist([root_path file.name '/location-dfa.mat'], 'file') == 2
            tic;
            
            dfa = [];
            dfa_range = [];
            
            load([root_path file.name '/location-dfa.mat']);
            
            % for backward compatibility
            if ~isstruct(dfa) && ~isempty(dfa) && ~isempty(dfa_range)
                dfa = struct('range', dfa_range, 'result', dfa);
            end
            
            loglog(dfa.range, dfa.result);
            
            hold on;
            
            toc;
        else
            fprintf('No data loaded\n')
        end
    end
    
    set(gca, 'FontSize', 16);
    
    ylabel('Average Fluctuation in Box')
    xlabel('DFA Box Size (samples)')
end
