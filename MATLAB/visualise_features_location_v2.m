function visualise_features_location_v2(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Aug-2014

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats';
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    data_full = data_raw';
    
    FEATURES = size(data_full, 2);
    
    valid_qids = ~isnan(classification_properties.QIDS);
    
    params = get_configuration('features_location_v2', varargin{:});
    
    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    
    label_opts = {};
    if latex_labels
        label_opts = {'Interpreter', 'latex'};
    end
    
    participant_qids_score = classification_properties.QIDS(valid_qids);
    participant_qids = (participant_qids_score >= params.qids_threshold);
    participant_class = classification_properties.class(valid_qids);
    
    function prepare_and_save_figure(subdir, filename, varargin)
        save_figure(filename, 'output_sub_dir', [OUTPUT_SUB_DIR '/' subdir], 'FontSize', 50, varargin{:});
    end

    hide_depressed_hc = get_argument({'hidedepressedhc', 'hide_depressed_hc'}, false, varargin{:});
    combine_hc = get_argument({'combinehc', 'combine_hc'}, false, varargin{:});
    
    features_to_show_display_idx = 1:FEATURES;
    features_to_show_display_idx = get_argument({'features_to_show', 'featurestoshow', 'features_to_show_display_idx', 'featurestoshowdisplayidx'}, features_to_show_display_idx, varargin{:});
    
    features_to_show = params.feature_order(params.feature_display_order(features_to_show_display_idx));
    
    show_correlations = get_argument({'showcorrelations', 'show_correlations', 'savecorrelations', 'save_correlations'}, true, varargin{:});
    show_cohort = get_argument({'showcohort', 'show_cohort', 'savecohort', 'save_cohort'}, true, varargin{:});
    show_cohort_qids = get_argument({'showcohortqids', 'show_cohort_qids', 'savecohortqids', 'save_cohort_qids'}, true, varargin{:});
    show_cohort_qids_multi = get_argument({'showcohortqidsmulti', 'show_cohort_qids_multi', 'savecohortqidsmulti', 'save_cohort_qids_multi'}, show_cohort_qids, varargin{:});
    show_qids = get_argument({'showqids', 'show_qids', 'saveqids', 'save_qids'}, true, varargin{:});
    use_cohort_clr = get_argument({'usecohortclr', 'use_cohort_clr'}, true, varargin{:});
    
    for classification_type = fieldnames(params.cohort_classification)'
        this_classification = params.cohort_classification.(classification_type{:});
        this_classification_title = strjoin(this_classification.names, ' vs. ');
        
        valid_idx = ismember(classification_properties.class, [this_classification.classes{:}]);
        
        data_normalised = data_full(valid_idx, features_to_show);

        for i = 1:numel(features_to_show)
            data_normalised(:, i) = data_normalised(:, i) - nanmean(data_normalised(:, i));
            data_normalised(:, i) = data_normalised(:, i) / nanstd(data_normalised(:, i));
            if sum(isnan(data_normalised(:, i))) > 0
                % data is now zero-mean, unit variance so this is acceptable.
                data_normalised(isnan(data_normalised(:, i)), i) = 0;
            end
        end

        if show_cohort
            data_boxplot = NaN(size(data_normalised, 1), (numel(features_to_show) * (numel(this_classification.classes) + 1)) + 1);
            data_boxplot_valid = NaN(size(data_normalised, 1), (numel(features_to_show) * (numel(this_classification.classes) + 1)) + 1);

            for i = 1:numel(features_to_show)
                this_data = data_normalised(:, i);
                this_data_valid = data_normalised(valid_qids(valid_idx), i);

                for ci = 1:numel(this_classification.classes)
                    this_class_idx = ismember(classification_properties.class(valid_idx), this_classification.classes{ci});

                    data_boxplot(1:sum(this_class_idx), (i - 1) * (numel(this_classification.classes) + 1) + ci + 1) = this_data(this_class_idx);

                    this_class_idx = this_class_idx(valid_qids(valid_idx));

                    data_boxplot_valid(1:sum(this_class_idx), (i - 1) * (numel(this_classification.classes) + 1) + ci + 1) = this_data_valid(this_class_idx);
                end
            end

            figure;

            boxplot(data_boxplot, 'Colors', [0 0 0; params.clr_cohort(this_classification.clr_idx, :)]);

            h = findall(gca,'Tag','Box');
            legend(h((numel(this_classification.classes) + 1):-1:2), this_classification.names, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 20, label_opts{:})

            set(gca, 'ylim', [-4, 5.5]);
            set(gca, 'xtick', ((numel(this_classification.classes) / 2) + 1.5):(numel(this_classification.classes) + 1):(numel(features_to_show) * (numel(this_classification.classes) + 1)));

            set(gca, 'xticklabel', params.feature_short(features_to_show), 'FontSize', 18);

            set(gca, 'XTickLabelRotation', 20)

            title(sprintf('Feature Statistics (%s)', this_classification_title), 'FontSize', 30, label_opts{:});
            %xlabel('Feature', 'FontSize', 20, label_opts{:});
            ylabel('Feature value (normalised)', 'FontSize', 20, label_opts{:});

            if latex_ticks
                set(gca, 'TickLabelInterpreter', 'latex')
            end

            p = get(gcf, 'PaperPosition');
            p(3) = 2 * p(3);
            set(gcf, 'PaperPosition', p);

            prepare_and_save_figure('', sprintf('features_cohort_%s_feature_statistics', classification_type{:}), varargin{:}, 'FontSize', []);

            figure;

            boxplot(data_boxplot_valid, 'Colors', [0 0 0; params.clr_cohort(this_classification.clr_idx, :)]);

            h = findall(gca,'Tag','Box');
            legend(h((numel(this_classification.classes) + 1):-1:2), this_classification.names, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 20, label_opts{:})

            set(gca, 'ylim', [-4, 5.5]);
            set(gca, 'xtick', ((numel(this_classification.classes) / 2) + 1.5):(numel(this_classification.classes) + 1):(numel(features_to_show) * (numel(this_classification.classes) + 1)));

            set(gca, 'xticklabel', params.feature_short(features_to_show), 'FontSize', 18);

            set(gca, 'XTickLabelRotation', 20)

            title(sprintf('Feature Statistics (%s)', this_classification_title), 'FontSize', 30, label_opts{:});
            %xlabel('Feature', 'FontSize', 20, label_opts{:});
            ylabel('Feature value (normalised)', 'FontSize', 20, label_opts{:});

            if latex_ticks
                set(gca, 'TickLabelInterpreter', 'latex')
            end

            p = get(gcf, 'PaperPosition');
            p(3) = 2 * p(3);
            set(gcf, 'PaperPosition', p);

            prepare_and_save_figure('', sprintf('features_cohort_%s_valid_feature_statistics', classification_type{:}), varargin{:}, 'FontSize', []);
        end
        
        if show_cohort_qids
            cohort_qids = false(numel(this_classification.classes), 2);

            for ci = 1:numel(this_classification.classes)
                if combine_hc && sum(this_classification.classes{ci} == 0) == numel(this_classification.classes{ci})
                    this_cohort_qids = 0;
                else
                    this_cohort_qids = participant_qids(valid_idx(valid_qids)) & ismember(participant_class(valid_idx(valid_qids)), this_classification.classes{ci});
                end
                
                cohort_qids(ci, 1) = sum(~this_cohort_qids) > 0;
                if ~hide_depressed_hc || sum(this_classification.classes{ci} == 0) ~= numel(this_classification.classes{ci})
                    cohort_qids(ci, 2) = sum(this_cohort_qids) > 0;
                end
            end

            data_boxplot = NaN(size(data_normalised, 1), (numel(features_to_show) * (sum(sum(cohort_qids)) + 1)) + 1);

            legend_str = cell(sum(sum(cohort_qids)), 1);

            for i = 1:numel(features_to_show)
                this_data = data_normalised(valid_qids(valid_idx), i);

                j = 1;

                for ci = 1:numel(this_classification.classes)
                    this_class_idx = ismember(participant_class(valid_idx(valid_qids)), this_classification.classes{ci});

                    for q = find(cohort_qids(ci, :))
                        if q == 1
                            if combine_hc && sum(this_classification.classes{ci} == 0) == numel(this_classification.classes{ci})
                                this_class_qids_idx = this_class_idx;
                                if i == 1
                                    this_legend_suffix = '';
                                end
                            else
                                this_class_qids_idx = this_class_idx & ~participant_qids(valid_idx(valid_qids));
                                if i == 1
                                    if latex_labels
                                        this_legend_suffix = ['QIDS $<$ ' num2str(params.qids_threshold)];
                                    else
                                        this_legend_suffix = ['QIDS < ' num2str(params.qids_threshold)];
                                    end
                                end
                            end
                        elseif q == 2
                            this_class_qids_idx = this_class_idx & participant_qids(valid_idx(valid_qids));
                            if i == 1
                                if latex_labels
                                    this_legend_suffix = ['QIDS $\geq$ ' num2str(params.qids_threshold)];
                                else
                                    this_legend_suffix = ['QIDS ' char(8805) ' ' num2str(params.qids_threshold)];
                                end
                            end
                        end
                        data_boxplot(1:sum(this_class_qids_idx), (i - 1) * (sum(sum(cohort_qids)) + 1) + j + 1) = this_data(this_class_qids_idx);

                        if i == 1
                            if numel(this_legend_suffix) > 0
                                legend_str{j} = [this_classification.names{ci} ' (' this_legend_suffix ')'];
                            else
                                legend_str{j} = this_classification.names{ci};
                            end
                        end

                        j = j + 1;
                    end
                end
            end

            figure;

            if use_cohort_clr
                clr_cohort_qids = zeros(sum(sum(cohort_qids)), 3);
                j = 1;

                for ci = 1:numel(this_classification.classes)
                    for q = find(cohort_qids(ci, :))
                        this_clr = params.clr_cohort(this_classification.clr_idx(ci), :);
                        this_clr = this_clr ./ (1 + (((q - 1) * 2) / 2));
                        clr_cohort_qids(j, :) = this_clr;
                        j = j + 1;
                    end
                end
            else
                clr_cohort_qids = lines(sum(sum(cohort_qids)));
            end

            boxplot(data_boxplot, 'Colors', [0 0 0; clr_cohort_qids]);

            h = findall(gca,'Tag','Box');
            legend(h((sum(sum(cohort_qids)) + 1):-1:2), legend_str, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 20, label_opts{:})

            set(gca, 'ylim', [-4, 5.5]);
            set(gca, 'xtick', ((sum(sum(cohort_qids)) / 2) + 1.5):(sum(sum(cohort_qids)) + 1):(numel(features_to_show) * (sum(sum(cohort_qids)) + 1)));

            set(gca, 'xticklabel', params.feature_short(features_to_show), 'FontSize', 18);

            set(gca, 'XTickLabelRotation', 20)

            title(sprintf('Feature Statistics (%s)', this_classification_title), 'FontSize', 30, label_opts{:});
            %xlabel('Feature', 'FontSize', 20, label_opts{:});
            ylabel('Feature value (normalised)', 'FontSize', 20, label_opts{:});

            if latex_ticks
                set(gca, 'TickLabelInterpreter', 'latex')
            end

            p = get(gcf, 'PaperPosition');
            p(3) = 2 * p(3);
            set(gcf, 'PaperPosition', p);

            if sum(sum(cohort_qids)) <= 3 && numel([legend_str{:}]) < 50
                legend(h((sum(sum(cohort_qids)) + 1):-1:2), legend_str, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 20, label_opts{:})
            elseif sum(sum(cohort_qids)) <= 3
                legend(h((sum(sum(cohort_qids)) + 1):-1:2), legend_str, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 20, label_opts{:})
            elseif sum(sum(cohort_qids)) > 5 || numel([legend_str{:}]) >= 80
                legend(h((sum(sum(cohort_qids)) + 1):-1:2), legend_str, 'Location', 'EastOutside', 'Orientation', 'vertical', 'FontSize', 18, label_opts{:})
            else
                legend(h((sum(sum(cohort_qids)) + 1):-1:2), legend_str, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 17.5, label_opts{:})
            end
            prepare_and_save_figure('', sprintf('features_cohort_qids_%.2i_%s_feature_statistics', params.qids_threshold, classification_type{:}), varargin{:}, 'FontSize', []);
        end
        
        if show_cohort_qids_multi
            cohort_qids = false(numel(this_classification.classes), numel(params.qids_threshold_multi) + 1);
            cohort_qids(:, 1) = true;

            for ci = 1:numel(this_classification.classes)
                if ~combine_hc || sum(this_classification.classes{ci} == 0) ~= numel(this_classification.classes{ci})
                    this_cohort_qids_score = participant_qids_score(valid_idx(valid_qids));
                    this_cohort_qids_score = this_cohort_qids_score(ismember(participant_class(valid_idx(valid_qids)), this_classification.classes{ci}));

                    for j = 1:numel(params.qids_threshold_multi)
                        if sum(this_cohort_qids_score >= params.qids_threshold_multi(j)) > 0
                            if ~hide_depressed_hc || j == 1 || sum(this_classification.classes{ci} == 0) ~= numel(this_classification.classes{ci})
                                cohort_qids(ci, j + 1) = true;
                            end
                        end
                    end
                end
            end

            data_boxplot = NaN(size(data_normalised, 1), (numel(features_to_show) * (sum(sum(cohort_qids)) + 1)) + 1);

            legend_str = cell(sum(sum(cohort_qids)), 1);

            for i = 1:numel(features_to_show)
                this_data = data_normalised(valid_qids(valid_idx), i);

                j = 1;

                for ci = 1:numel(this_classification.classes)
                    this_class_idx = ismember(participant_class(valid_idx(valid_qids)), this_classification.classes{ci});
                    this_cohort_qids_score = participant_qids_score(valid_idx(valid_qids));

                    for q = find(cohort_qids(ci, :))
                        if q == 1
                            if combine_hc && sum(this_classification.classes{ci} == 0) == numel(this_classification.classes{ci})
                                this_class_qids_idx = this_class_idx;
                                if i == 1
                                    this_legend_suffix = '';
                                end
                            else
                                this_class_qids_idx = this_class_idx & this_cohort_qids_score < params.qids_threshold_multi(1);
                                if i == 1
                                    if latex_labels
                                        this_legend_suffix = ['QIDS $<$ ' num2str(params.qids_threshold_multi(q))];
                                    else
                                        this_legend_suffix = ['QIDS < ' num2str(params.qids_threshold_multi(q))];
                                    end
                                end
                            end
                        elseif q == numel(params.qids_threshold_multi) + 1;
                            this_class_qids_idx = this_class_idx & this_cohort_qids_score >= params.qids_threshold_multi(end);
                            if i == 1
                                if latex_labels
                                    this_legend_suffix = [num2str(params.qids_threshold_multi(q - 1)) ' $\leq$ QIDS'];
                                else
                                    this_legend_suffix = [num2str(params.qids_threshold_multi(q - 1)) ' ' char(8804) ' QIDS'];
                                end
                            end
                        else
                            this_class_qids_idx = this_class_idx & this_cohort_qids_score >= params.qids_threshold_multi(q - 1) & this_cohort_qids_score < params.qids_threshold_multi(q);
                            if i == 1
                                if latex_labels
                                    this_legend_suffix = [num2str(params.qids_threshold_multi(q - 1)) ' $\leq$ QIDS $<$ ' num2str(params.qids_threshold_multi(q))];
                                else
                                    this_legend_suffix = [num2str(params.qids_threshold_multi(q - 1)) ' ' char(8804) ' QIDS < ' num2str(params.qids_threshold_multi(q))];
                                end
                            end
                        end
                        data_boxplot(1:sum(this_class_qids_idx), (i - 1) * (sum(sum(cohort_qids)) + 1) + j + 1) = this_data(this_class_qids_idx);

                        if i == 1
                            if numel(this_legend_suffix) > 0
                                legend_str{j} = [this_classification.names{ci} ' (' this_legend_suffix ')'];
                            else
                                legend_str{j} = this_classification.names{ci};
                            end
                        end

                        j = j + 1;
                    end
                end
            end

            figure;

            if use_cohort_clr
                clr_cohort_qids = zeros(sum(sum(cohort_qids)), 3);
                j = 1;

                for ci = 1:numel(this_classification.classes)
                    for q = find(cohort_qids(ci, :))
                        this_clr = params.clr_cohort(this_classification.clr_idx(ci), :);
                        this_clr = this_clr ./ (1 + ((q - 1) / 2));
                        clr_cohort_qids(j, :) = this_clr;
                        j = j + 1;
                    end
                end
            else
                clr_cohort_qids = lines(sum(sum(cohort_qids)));
            end

            boxplot(data_boxplot, 'Colors', [0 0 0; clr_cohort_qids]);

            h = findall(gca,'Tag','Box');

            set(gca, 'ylim', [-4, 5.5]);
            set(gca, 'xtick', ((sum(sum(cohort_qids)) / 2) + 1.5):(sum(sum(cohort_qids)) + 1):(numel(features_to_show) * (sum(sum(cohort_qids)) + 1)));

            set(gca, 'xticklabel', params.feature_short(features_to_show), 'FontSize', 18);

            set(gca, 'XTickLabelRotation', 20)

            title(sprintf('Feature Statistics (%s)', this_classification_title), 'FontSize', 30, label_opts{:});
            %xlabel('Feature', 'FontSize', 20, label_opts{:});
            ylabel('Feature value (normalised)', 'FontSize', 20, label_opts{:});

            if latex_ticks
                set(gca, 'TickLabelInterpreter', 'latex')
            end

            p = get(gcf, 'PaperPosition');
            p(3) = 2 * p(3);
            set(gcf, 'PaperPosition', p);

            if sum(sum(cohort_qids)) > 5 || numel([legend_str{:}]) >= 80
                legend(h((sum(sum(cohort_qids)) + 1):-1:2), legend_str, 'Location', 'EastOutside', 'Orientation', 'vertical', 'FontSize', 18, label_opts{:})
            else
                legend(h((sum(sum(cohort_qids)) + 1):-1:2), legend_str, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 17.5, label_opts{:})
            end
            prepare_and_save_figure('', sprintf('features_cohort_qids%s_%s_feature_statistics', sprintf('_%.2i', params.qids_threshold_multi), classification_type{:}), varargin{:}, 'FontSize', []);
        end
    end
    
    for t = 1:5
        if t == 1
            test_valid_qids = valid_qids;
            test_participant_qids = participant_qids;
        elseif t == 5
            test_classes = 0:1;
            
            test_participants_idx = ismember(participant_class, test_classes);
            
            test_valid_qids = valid_qids & ismember(classification_properties.class, test_classes);
            test_participant_qids = participant_qids(test_participants_idx);
        else
            test_classes = t - 2;
            
            test_participants_idx = (participant_class == test_classes);
            
            test_valid_qids = valid_qids & (classification_properties.class == test_classes);
            test_participant_qids = participant_qids(test_participants_idx);
        end
        
        this_test_description = params.test_description{t};
        this_test_description_filename = params.test_description_filename{t};

        data_correlation = data_full(test_valid_qids, features_to_show);

        for i = 1:numel(features_to_show)
            data_correlation(:, i) = data_correlation(:, i) - nanmean(data_correlation(:, i));
            data_correlation(:, i) = data_correlation(:, i) / nanstd(data_correlation(:, i));
            if sum(isnan(data_correlation(:, i))) > 0
                % data is now zero-mean, unit variance so this is acceptable.
                data_correlation(isnan(data_correlation(:, i)), i) = 0;
            end
        end

        if show_correlations
            [r, p] = corrcoef(data_correlation);

            figure

            image(ones([(size(r) - 1) 3]));

            box off;

            for i = 1:numel(features_to_show)
                for j = (i + 1):numel(features_to_show)
                    rectangle('position', [(i - 0.5) (j - 1.5) 1 1]);
                    if p(i, j) < 0.001
                        sig_str = '***';
                    elseif p(i, j) < 0.01
                        sig_str = '**';
                    elseif p(i, j) < 0.05
                        sig_str = '*';
                    else
                        sig_str = '';
                    end
                    text(i, j - 1, sprintf('%0.2f%s', r(i, j), sig_str), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FontSize', 13, label_opts{:});
                end
            end

            set(gca, 'xtick', 1:(numel(features_to_show) - 1));
            set(gca, 'xticklabel', params.feature_short(features_to_show(1:end-1)), 'FontSize', 18);
            set(gca, 'XTickLabelRotation', 30)
            set(gca, 'ytick', 1:(numel(features_to_show) - 1));
            set(gca, 'yticklabel', params.feature_short(features_to_show(2:end)), 'FontSize', 18);

            title(sprintf('Feature Correlations (%s)', this_test_description), 'FontSize', 30, label_opts{:});
            %xlabel('Feature', 'FontSize', 20, label_opts{:});
            %ylabel('Feature', 'FontSize', 20, label_opts{:});

            if latex_ticks
                set(gca, 'TickLabelInterpreter', 'latex')
            end

            prepare_and_save_figure('', sprintf('features_qids_correlations_%s', this_test_description_filename), varargin{:}, 'FontSize', []);
        end
        
        if show_qids
            data_boxplot = NaN(size(data_correlation, 1), numel(features_to_show) * 3 + 1);

            for i = 1:numel(features_to_show)
                this_data = data_correlation(:, i);

                data_boxplot(1:sum(~test_participant_qids), (i - 1) * 3 + 2) = this_data(~test_participant_qids);
                data_boxplot(1:sum(test_participant_qids), (i - 1) * 3 + 3) = this_data(test_participant_qids);
            end

            figure;

            boxplot(data_boxplot, 'Colors', [0 0 0; params.clr_qids]);

            h = findall(gca,'Tag','Box');
            if latex_labels
                legend_str = {['QIDS $<$ ' num2str(params.qids_threshold)], ['QIDS $\geq$ ' num2str(params.qids_threshold)]};
            else
                legend_str = {['QIDS < ' num2str(params.qids_threshold)], ['QIDS ' char(8805) ' ' num2str(params.qids_threshold)]};
            end
            legend(h([3, 2]), legend_str, 'Location', 'North', 'Orientation', 'horizontal', 'FontSize', 20, label_opts{:})

            set(gca, 'ylim', [-4, 5.5]);
            set(gca, 'xtick', 2.5:3:(numel(features_to_show) * 3));

            set(gca, 'xticklabel', params.feature_short(features_to_show), 'FontSize', 18);

            set(gca, 'XTickLabelRotation', 20)

            title(sprintf('Feature Statistics (%s)', this_test_description), 'FontSize', 30, label_opts{:});
            %xlabel('Feature', 'FontSize', 20, label_opts{:});
            ylabel('Feature value (normalised)', 'FontSize', 20, label_opts{:});

            if latex_ticks
                set(gca, 'TickLabelInterpreter', 'latex')
            end

            p = get(gcf, 'PaperPosition');
            p(3) = 2 * p(3);
            set(gcf, 'PaperPosition', p);

            prepare_and_save_figure('', sprintf('features_qids_%.2i_feature_statistics_%s', params.qids_threshold, this_test_description_filename), varargin{:}, 'FontSize', []);
        end
    end
end
