function visualise_classification_location_v2_errors(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 07-Oct-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/classification/errors';
    
    classification_data = load_classification_data_location_v2(varargin{:});
    
    [params, results] = preprocess_classification_data_location_v2(classification_data);
    
    function [qids_range, qids_result, qids_error, qids_correct] = get_error_profile(result, q_diff)
        if nargin < 2
            q_diff = 2;
        end
        
        qids_range = [-0.5:q_diff:24.5 27.5];
        qids_result = zeros(length(qids_range), 2);
        qids_error = NaN(size(qids_range));
        qids_correct = NaN(size(result.pred));
        
        class1 = result.class == 1;
        class2 = result.class == 2;
        
        for q_i = (numel(qids_range) - 1):-1:1
            this_qids = result.qids >= qids_range(q_i) & result.qids < qids_range(q_i + 1);
            
            total_correct = 0;
            total_trials = 0;
            
            for rep = 1:size(result.pred, 2)
                correct1 = result.pred(this_qids & class1, rep) == result.class(this_qids & class1);
                correct2 = result.pred(this_qids & class2, rep) == result.class(this_qids & class2);
                
                total_correct = total_correct + sum(correct1) + sum(correct2);
                total_trials = total_trials + numel(correct1) + numel(correct2);
                
                qids_result(q_i, 1) = qids_result(q_i, 1) + sum(correct1) + sum(~correct2);
                qids_result(q_i, 2) = qids_result(q_i, 2) + sum(correct2) + sum(~correct1);
                
                qids_correct(this_qids & class1, rep) = correct1;
                qids_correct(this_qids & class2, rep) = correct2;
            end
            
            if total_trials > 0
                qids_error(q_i) = 1 - (total_correct / total_trials);
            end
        end
    end
    
    test_description = {'All', 'HC', 'BD', 'BPD', 'HC/BD'};
    
    clr_cohort = lines(7);
    clr_cohort = clr_cohort([1 2 4 5 7 3 6], :);
    
    for t = params.TESTS_TO_PERFORM
        q_all = [params.Q, params.Q_MAX + params.Q_EXTRA_TO_TEST];
        q_to_display = ismember(q_all, [6, 10, 11, params.Q_MAX + params.Q_EXTRA_TO_TEST]);
        for q = find(q_to_display & results.meta.q_valid(t, :))
            if q <= numel(params.Q)
                this_q_desc = sprintf('QIDS %s %i', char(8805), params.Q(q));
                this_q_desc_short = this_q_desc;
                this_q_filename_suffix = sprintf('qids_%.2i', params.Q(q));
            elseif q == (numel(params.Q) + 1)
                this_q_desc = 'Cohort classification';
                this_q_desc_short = 'Cohort';
                this_q_filename_suffix = 'cohort';
            end
            
            text_filename = sprintf('classification_results_%s_%s', this_q_filename_suffix, strrep(lower(test_description{t}), '/', '_'));
            
            save_text(text_filename, [{'output_sub_dir', OUTPUT_SUB_DIR}, varargin, {'append', false}], '%s\n', datestr(now));
            
            save_text_params = {text_filename, [{'output_sub_dir', OUTPUT_SUB_DIR}, varargin, {'append', true}]};
            
            for type_cell = fieldnames(results)'
                if sum(ismember(type_cell, {'loo_eq', 'ind_features', 'pca'}) > 0)
                    median_ac = reshape(nanmedian(results.(type_cell{:}).accuracy_raw(t, q, :, :), 4), params.FEATURES, 1);
                    
                    save_fprintf(save_text_params{:}, '====================================\n');
                    save_fprintf(save_text_params{:}, '%s, %s, %s\n', test_description{t}, this_q_desc, type_cell{:});
                    save_fprintf(save_text_params{:}, '------------------------------------\n');
                    [max_ac, max_ac_f] = max(median_ac);
                    save_fprintf(save_text_params{:}, ' Max accuracy at %i features: %.4f\n', max_ac_f, max_ac);
                    save_fprintf(save_text_params{:}, '------------------------------------\n');
                    this_result_predictions = classification_data.results.predictions.(type_cell{:}){t, q, max_ac_f};
                    
                    [qids_range, qids_result, qids_error] = get_error_profile(this_result_predictions, 2);
                    
                    class_reps = repmat(this_result_predictions.class, 1, size(this_result_predictions.pred, 2));
                    
                    unique_classes = sort(unique(this_result_predictions.class))';
                    
                    save_fprintf(save_text_params{:}, '   True |   Predicted\n        | %s \n  %s\n', sprintf('   %i   ', unique_classes - 1), repmat('-', 1, 9 + (numel(unique_classes) * 7)))
                    
                    counts = NaN(numel(unique_classes));
                    
                    for c_true = unique_classes
                        str_text = sprintf(' %i%s| ', c_true - 1, repmat(' ', 1, 4 - length(num2str(c_true))));
                        c_total = sum(sum((class_reps == c_true)));
                        for c_pred = unique_classes
                            c_num = sum(sum((this_result_predictions.pred == c_pred) & (class_reps == c_true)));
                            counts(unique_classes == c_true, unique_classes == c_pred) = c_num;
                            str_text = sprintf('%s %.3f ', str_text, c_num / c_total);
                        end
                        save_fprintf(save_text_params{:}, '   %s \n', str_text);
                    end
                    
                    save_fprintf(save_text_params{:}, '------------------------------------\n');
                    
                    save_fprintf(save_text_params{:}, '  Accuracy:    %.4f\n', sum(diag(counts)) / sum(sum(counts)));
                    
                    if numel(unique_classes) == 2
                        save_fprintf(save_text_params{:}, '  Sensitivity: %.4f\n', counts(2, 2) / sum(counts(2, :)));
                        save_fprintf(save_text_params{:}, '  Specificity: %.4f\n', counts(1, 1) / sum(counts(1, :)));
                    end
                    
                    save_fprintf(save_text_params{:}, '------------------------------------\n');
                    
                    for err_q = (numel(qids_range) - 1):-1:1
                        this_qids_error = qids_error(err_q);
                        if isnan(this_qids_error)
                            this_qids_error = 0;
                        end
                        save_fprintf(save_text_params{:}, '%.1f - %.1f :  %.4i, %.4i (%.3f)\n', qids_range(err_q), qids_range(err_q + 1), qids_result(err_q, 1), qids_result(err_q, 2), this_qids_error);
                    end
                    
                    save_fprintf(save_text_params{:}, '====================================\n');
                    
                    [qids_range, ~, qids_error, qids_correct] = get_error_profile(this_result_predictions, 1);
                    
                    figure;
                    
                    qids_error(isnan(qids_error)) = -1;
                    
                    stairs(qids_range, qids_error, 'LineWidth', 1.5);
                    
                    title(sprintf('%s (%s, %s)', results.(type_cell{:}).title_short, test_description{t}, this_q_desc_short))
                    
                    set(gca, 'xlim', qids_range([1 end]));
                    set(gca, 'ylim', [0 1]);
                    
                    xlabel('QIDS Score');
                    ylabel('Error Rate');
                    
                    save_figure(sprintf('error_profile_%s_%s_%s', this_q_filename_suffix, type_cell{:}, strrep(lower(test_description{t}), '/', '_')), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 20, varargin{:});
                    
                    qids_all_reps = repmat(this_result_predictions.qids, 1, size(this_result_predictions.pred, 2));
                    
                    figure;
                    
                    histogram(qids_all_reps, -0.5:1:27.5, 'FaceColor', clr_cohort(t, :), 'FaceAlpha', 1);
                    hold on;
                    histogram(qids_all_reps(~qids_correct), -0.5:1:27.5, 'FaceColor', [0 0 0], 'FaceAlpha', 0.4);
                    hold off;
                    
                    legend(test_description{t}, 'Errors');
                    
                    title(sprintf('%s (%s, %s)', results.(type_cell{:}).title_short, test_description{t}, this_q_desc_short))
                    
                    set(gca, 'xlim', [-0.5, 27.5]);
                    set(gca, 'FontSize', 20);
                    set(gca, 'yticklabel', get(gca, 'ytick') / params.REPS);
                    
                    xlabel('QIDS Score');
                    ylabel('Valid Weeks');
                    
                    save_figure(sprintf('error_profile_dist_%s_%s_%s', this_q_filename_suffix, type_cell{:}, strrep(lower(test_description{t}), '/', '_')), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 20, varargin{:});
                    
                    if numel(classification_data.meta.classes{t}) > 1
                        figure;
                        
                        h = cell(numel(classification_data.meta.classes{t}) + 1, 1);
                        legend_strings = cell(size(h));
                        
                        for c_i = 1:numel(classification_data.meta.classes{t})
                            classes = classification_data.meta.classes{t}(c_i:end);
                            
                            h{c_i} = histogram(qids_all_reps(ismember(results.meta.participant_class_cohort{t}, classes), :), -0.5:1:27.5, 'FaceColor', clr_cohort(classes(1) + 2, :), 'FaceAlpha', 1);
                            legend_strings{c_i} = test_description{classes(1) + 2};
                            
                            if c_i == 1
                                hold on;
                            end
                            
                            error_idx = (results.meta.participant_class_cohort{t} == classes(1));
                            error_idx = repmat(error_idx, 1, size(qids_correct, 2)) & ~qids_correct;
                            error_idx = error_idx | repmat(ismember(results.meta.participant_class_cohort{t}, classes(2:end)), 1, size(qids_correct, 2));
                            
                            h{end} = histogram(qids_all_reps(error_idx), -0.5:1:27.5, 'FaceColor', [0 0 0], 'FaceAlpha', 0.4);
                            legend_strings{end} = 'Errors';
                        end
                        
                        legend([h{:}], legend_strings);
                        
                        title(sprintf('%s (%s, %s)', results.(type_cell{:}).title_short, test_description{t}, this_q_desc_short))
                        
                        set(gca, 'xlim', [-0.5, 27.5]);
                        set(gca, 'FontSize', 20);
                        set(gca, 'yticklabel', get(gca, 'ytick') / params.REPS);
                        
                        xlabel('QIDS Score');
                        ylabel('Valid Weeks');
                        
                        save_figure(sprintf('error_profile_dist_%s_%s_%s_cohorts', this_q_filename_suffix, type_cell{:}, strrep(lower(test_description{t}), '/', '_')), 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', 20, varargin{:});
                        
                        hold off;
                    end
                end
            end
        end
    end
end
