function visualise_data_overview_location_histogram(participant, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Feb-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location-histogram';
    
    WEEKS_IN_PERIOD = 4;
    
    xlim = get_argument('xlim', [-40 40], varargin{:});
    ylim = get_argument('ylim', [-40 40], varargin{:});
    dx = get_argument('dx', 0.5, varargin{:});
    dy = get_argument('dy', 0.5, varargin{:});
    
    data = load_location_preprocessed(participant, varargin{:});
    
    data = data.normalised;
    
    if ~isempty(data) && ~isempty(data.time)
        valid_data = data.time >= datenum([2014, 01, 01]);
        time_days = floor(data.time(valid_data));
        time_time = data.time(valid_data) - time_days;

        locations = [data.lat, data.lon];

        location_time = [diff(data.time); 0];
        location_time = min(location_time, 1/(24 * 4));

        % Centers
        x = xlim(1):dx:xlim(2);
        y = ylim(1):dy:ylim(2);

        WEEKDAYS = 2:6;
        WEEKENDS = [1 7];

        period_beginning = 0;
        
        fprintf(' Entire dataset\n');
        
        %participant_all_idx = true(size(time_days));
        z_data = extract_histogram_data_3d_location(location_time, locations, x, y);
        [data_modes, data_mode_center, data_mode_mass] = extract_histogram_modes_3d_location(z_data);
        plot_and_save_data(z_data, 'All');
        plot_and_save_modes(z_data, data_modes, 'All');
        
        close all;
        
        period_beginning = min(time_days) - weekday(min(time_days)) + 2;

        while period_beginning < max(time_days)
            period_ending = period_beginning + (WEEKS_IN_PERIOD * 7) - 1;
            
            fprintf([' Period ' datestr(period_beginning, 'yyyy-mm-dd') ' - ' datestr(period_ending, 'yyyy-mm-dd') '\n']);
            
            period_days = period_beginning + (0:((7 * WEEKS_IN_PERIOD) - 1));

            period_data_idx = (ismember(time_days, period_days) & ~isnan(data.data(valid_data)));

            period_all_idx = (period_data_idx);
            z_data = extract_histogram_data_3d_location(location_time(period_all_idx), locations(period_all_idx, :), x, y);
            [data_modes, data_mode_center, data_mode_mass] = extract_histogram_modes_3d_location(z_data);
            plot_and_save_data(z_data, 'All');
            plot_and_save_modes(z_data, data_modes, 'All');

            period_night_idx = (period_data_idx & ismember(weekday(time_days), WEEKDAYS) & (time_time >= (2/24)) & (time_time <= (6/24)));
            z_data = extract_histogram_data_3d_location(location_time(period_night_idx), locations(period_night_idx, :), x, y);
            [data_modes, data_mode_center, data_mode_mass] = extract_histogram_modes_3d_location(z_data);
            plot_and_save_data(z_data, 'Night');
            plot_and_save_modes(z_data, data_modes, 'Night');

            period_weekday_idx = (period_data_idx & ismember(weekday(time_days), WEEKDAYS));
            z_data = extract_histogram_data_3d_location(location_time(period_weekday_idx), locations(period_weekday_idx, :), x, y);
            [data_modes, data_mode_center, data_mode_mass] = extract_histogram_modes_3d_location(z_data);
            plot_and_save_data(z_data, 'Weekday');
            plot_and_save_modes(z_data, data_modes, 'Weekday');

            period_weekend_idx = (period_data_idx & ismember(weekday(time_days), WEEKENDS));
            z_data = extract_histogram_data_3d_location(location_time(period_weekend_idx), locations(period_weekend_idx, :), x, y);
            [data_modes, data_mode_center, data_mode_mass] = extract_histogram_modes_3d_location(z_data);
            plot_and_save_data(z_data, 'Weekend');
            plot_and_save_modes(z_data, data_modes, 'Weekend');

            period_weekday_working_idx = (period_data_idx & ismember(weekday(time_days), WEEKDAYS) & (time_time >= (10/24)) & (time_time <= (16/24)));
            z_data = extract_histogram_data_3d_location(location_time(period_weekday_working_idx), locations(period_weekday_working_idx, :), x, y);
            [data_modes, data_mode_center, data_mode_mass] = extract_histogram_modes_3d_location(z_data);
            plot_and_save_data(z_data, 'Weekday Working Time');
            plot_and_save_modes(z_data, data_modes, 'Weekday Working Time');
            
            close all;
            
            period_beginning = period_beginning + (WEEKS_IN_PERIOD * 7);
        end
    end
    
    function [title_period] = get_title_period()
        if period_beginning > 0
            title_period = [datestr(period_beginning, 'dd/mm/yyyy') ' - ' datestr(period_ending, 'dd/mm/yyyy') ' '];
        else
            title_period = 'Dataset ';
        end
    end
    
    function [filename_period] = get_filename_period()
        if period_beginning > 0
            filename_period = datestr(period_beginning, 'yyyy-mm-dd');
        else
            filename_period = '0000-00-00';
        end
    end
    
    function plot_and_save_data(z, data_description)
        fprintf(['  Plotting ' lower(data_description) ' locations histogram: ']);
        tic;
        plot_data(z);
        title([get_title_period data_description ' Locations']);
        toc;

        fprintf(['  Saving ' lower(data_description) ' locations histogram: ']);
        tic;
        filename_description = strrep(lower(data_description), ' ', '-');
        save_figure([participant '-' get_filename_period '-' filename_description], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
    end

    function plot_and_save_modes(data_z, data_modes, data_description)
        fprintf(['  Plotting ' lower(data_description) ' location modes histogram: ']);
        tic;
        plot_modes(data_z, data_modes);
        title([get_title_period data_description ' Location Modes']);
        toc;

        fprintf(['  Saving ' lower(data_description) ' location modes histogram: ']);
        tic;
        filename_description = strrep(lower(data_description), ' ', '-');
        save_figure([participant '-' get_filename_period '-' filename_description '-modes'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
    end

    function plot_data(z)
        figure

        plot_histogram_3d(x, y, z)
        
        set(gca, 'clim', [0 1]);
        
        xlabel('Distance (km)', 'FontSize', 16);
        ylabel('Distance (km)', 'FontSize', 16);
        zlabel('Time recorded in location (%)', 'FontSize', 16);
        
        thiscolormap = colormap(autumn);
        colormap(flipud(thiscolormap));
        
        hcb = colorbar;
        
        ylabel(hcb, 'Time recorded in location (%)', 'FontSize', 16);
    end

    function plot_modes(data_z, data_modes)
        figure

        plot_histogram_3d(x, y, data_z, data_modes)

        xlabel('Distance (km)', 'FontSize', 16);
        ylabel('Distance (km)', 'FontSize', 16);
        zlabel('Time recorded in location (%)', 'FontSize', 16);
        
        colormap(lines(max(max(data_modes))))
        colorbar off;
    end
end
