function preprocess_location_downsample_1(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 22-Oct-2015

    variant = get_argument('variant', '', varargin{:});
    combine_variants = get_argument({'combinevariants', 'combine_variants'}, {}, varargin{:});
    
    if isempty(variant) && ~isempty(combine_variants)
        variant = 'combined';

        for i = 1:length(combine_variants)
            this_variant_name = combine_variants{i};

            if isempty(this_variant_name)
                this_variant_name = 'std';
            end

            variant = [variant '_' this_variant_name]; %#ok<AGROW>
        end
    end

    variant_out = '';
    
    if ~isempty(variant)
        variant_out = ['_' variant];
    end

    for file = get_participants(varargin{:})'
        fprintf('Processing %s\n', file.name)

        data = load_location_preprocessed(file.name, varargin{:}, 'variant', variant);

        if ~isempty(data) && isfield(data, 'normalised') && isfield(data.normalised, 'filtered') && ~isempty(data.normalised.filtered.time)
            fprintf('\n    Downsampling: ');
            
            location_data = [data.normalised.filtered.lat data.normalised.filtered.lon]; % for normalised coordinates, this is in km from home
            
            t_min = floor(min(data.normalised.filtered.time));
            t_max = floor(max(data.normalised.filtered.time)) + 1;
            
            t_start = 1;
            t_end = 1;
            
            t_range = t_min:(1 / 24):t_max;
            
            ds_time = NaN(numel(t_range) * (60 + 12), 1);
            ds_location_data = NaN(numel(t_range) * (60 + 12), 2);
            
            ds_i = 1;
            
            last_status_text = '';
            
            ds_window_5min = (1 / (24 * 12));
            ds_window_1min = (1 / (24 * 60));
            
            for i = 2:length(t_range)
                if (i == 2) || (rem(i, 500) == 0) || (i == length(t_range))
                    if ~isempty(last_status_text)
                        fprintf(repmat('\b', 1, length(last_status_text) - 1))
                    end
                    last_status_text = sprintf('%.2f%%%%', (i / length(t_range)) * 100);
                    fprintf(last_status_text);
                end
                
                ds_time_new_5min = (t_range(i - 1):ds_window_5min:(t_range(i) - ds_window_5min)) + (ds_window_5min / 2);
                ds_time_new_1min = (t_range(i - 1):ds_window_1min:(t_range(i) - ds_window_1min)) + (ds_window_1min / 2);
                ds_location_data_new_5min = NaN(length(ds_time_new_5min), 2);
                ds_location_data_new_1min = NaN(length(ds_time_new_1min), 2);
                
                while (t_start < numel(data.normalised.filtered.time)) && ...
                        (data.normalised.filtered.time(t_start) < t_range(i - 1));
                    t_start = t_start + 1;
                end
                
                if data.normalised.filtered.time(t_start) < t_range(i)
                    t_end = max(t_end, t_start);

                    while ((t_end + 1) < numel(data.normalised.filtered.time)) && ...
                            (data.normalised.filtered.time(t_end + 1) < t_range(i));
                        t_end = t_end + 1;
                    end
                    
                    this_time = data.normalised.filtered.time(t_start:t_end);
                    this_location_data = location_data(t_start:t_end, :);
                    
                    if (std(this_location_data(:, 1)) < 0.01) && ...
                            (std(this_location_data(:, 2)) < 0.01)
                        
                        for j = 1:numel(ds_time_new_5min)
                            if sum((this_time >= (ds_time_new_5min(j) - (ds_window_5min / 2))) & (this_time < (ds_time_new_5min(j) + (ds_window_5min / 2)))) > 0
                                ds_location_data_new_5min(j, :) = mean(this_location_data, 1);
                            end
                        end
                    else
                        for j = 1:numel(ds_time_new_5min)
                            valid_range = (this_time >= (ds_time_new_5min(j) - (ds_window_5min / 2))) & (this_time < (ds_time_new_5min(j) + (ds_window_5min / 2)));
                            
                            if sum(valid_range) > 0
                                range_location_data = this_location_data(valid_range, :);
                                
                                if (std(range_location_data(:, 1)) < 0.01) && ...
                                        (std(range_location_data(:, 2)) < 0.01)
                                    
                                    ds_location_data_new_5min(j, :) = mean(range_location_data, 1);
                                else
                                    for k = 1:numel(ds_time_new_1min)
                                        valid_range = (this_time >= (ds_time_new_1min(k) - (ds_window_1min / 2))) & (this_time < (ds_time_new_1min(k) + (ds_window_1min / 2)));
                                        
                                        if sum(valid_range) > 0
                                            range_location_data = this_location_data(valid_range, :);
                                            
                                            ds_location_data_new_1min(k, :) = mean(range_location_data, 1);
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
                
                ds_time_new_temp = [ds_time_new_5min, ds_time_new_1min];
                ds_location_data_new_temp = [ds_location_data_new_5min; ds_location_data_new_1min];
                
                [ds_time_new_temp, ds_temp_sort_order] = sort(ds_time_new_temp);
                ds_location_data_new_temp = ds_location_data_new_temp(ds_temp_sort_order, :);
                
                ds_time(ds_i:(ds_i + numel(ds_time_new_temp) - 1)) = ds_time_new_temp;
                ds_location_data(ds_i:(ds_i + numel(ds_time_new_temp) - 1), :) = ds_location_data_new_temp;
                
                ds_i = ds_i + numel(ds_time_new_temp);
                t_start = t_end;
            end
            
            ds_data = struct();
            ds_data.time = ds_time;
            ds_data.data = sqrt(sum(ds_location_data .^ 2, 2));
            ds_data.lat = ds_location_data(:, 1);
            ds_data.lon = ds_location_data(:, 2);
            
            is_valid = ~isnan(ds_data.data);
            
            data.normalised.downsampled = struct();
            data.normalised.downsampled.time = ds_data.time(is_valid);
            data.normalised.downsampled.data = ds_data.data(is_valid);
            data.normalised.downsampled.lat = ds_data.lat(is_valid);
            data.normalised.downsampled.lon = ds_data.lon(is_valid);
            
            data.normalised.downsampled.differentiated = calculate_location_differential(data.normalised.downsampled);
            
            if ~isempty(last_status_text)
                fprintf(repmat('\b', 1, length(last_status_text) - 1))
            end
            fprintf('done');
            
            fprintf('\n    Completed: ');
            toc;
            
            save_data([file.name '-location' variant_out '-preprocessed'], data, varargin{:});
        else
            fprintf('  No data loaded\n');
        end
    end
end