function preprocess_location_filter_2(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 27-Jul-2015

    variant = get_argument('variant', '', varargin{:});
    combine_variants = get_argument({'combinevariants', 'combine_variants'}, {}, varargin{:});
    
    if isempty(variant) && ~isempty(combine_variants)
        variant = 'combined';

        for i = 1:length(combine_variants)
            this_variant_name = combine_variants{i};

            if isempty(this_variant_name)
                this_variant_name = 'std';
            end

            variant = [variant '_' this_variant_name]; %#ok<AGROW>
        end
    end

    variant_out = '';
    
    if ~isempty(variant)
        variant_out = ['_' variant];
    end

    for file = get_participants(varargin{:})'
        fprintf('Processing %s\n', file.name)

        data = load_location_preprocessed(file.name, varargin{:}, 'variant', variant);

        if ~isempty(data) && isfield(data, 'normalised') && ~isempty(data.normalised.time)
            
            fprintf('  Filtering data: ');
            tic;

            data.normalised.filtered = struct();
            data.normalised.filtered.time = data.normalised.time;
            data.normalised.filtered.data = data.normalised.data;
            data.normalised.filtered.lat = data.normalised.lat;
            data.normalised.filtered.lon = data.normalised.lon;
            
            for i = 1:20
                location_data = [data.normalised.filtered.lat data.normalised.filtered.lon]; % for normalised coordinates, this is in km from home
                location_data_rounded = round(location_data * 100) / 100;
                
                data_differentiated = calculate_location_differential(data.normalised.filtered);
                
                invalid_diff = find(data_differentiated.data > 10000);

                if ~isempty(invalid_diff)
                    invalid_source = location_data_rounded(invalid_diff - 1, :);
                    invalid_dest = location_data_rounded(invalid_diff, :);

                    [sources, ~, sources_idx] = unique(invalid_source, 'rows');
                    [dests, ~, dests_idx] = unique(invalid_dest, 'rows');

                    [sources_sorted, sources_sorted_idx] = sort(histc(sources_idx, sort(unique(sources_idx))), 'descend');
                    [dests_sorted, dests_sorted_idx] = sort(histc(dests_idx, sort(unique(dests_idx))), 'descend');

                    is_valid = true(size(data.normalised.filtered.time));

                    if sources_sorted(1) > dests_sorted(1)
                        locations_at_source_mode = ismember(location_data_rounded, sources(sources_sorted_idx(1), :), 'rows');
                        is_valid(locations_at_source_mode) = false;
                    else
                        locations_at_dest_mode = ismember(location_data_rounded, dests(dests_sorted_idx(1), :), 'rows');
                        is_valid(locations_at_dest_mode) = false;
                    end

                    data.normalised.filtered.time = data.normalised.filtered.time(is_valid);
                    data.normalised.filtered.data = data.normalised.filtered.data(is_valid);
                    data.normalised.filtered.lat = data.normalised.filtered.lat(is_valid);
                    data.normalised.filtered.lon = data.normalised.filtered.lon(is_valid);
                else
                    break;
                end
                
                data.normalised.filtered.differentiated = calculate_location_differential(data.normalised.filtered);
            end
            
            toc;
            
            save_data([file.name '-location' variant_out '-preprocessed'], data, varargin{:});
        else
            fprintf('  No data loaded\n');
        end
    end
end