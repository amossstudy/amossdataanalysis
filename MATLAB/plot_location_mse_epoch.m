function plot_location_mse_epoch(mse_epochs, mse_epochs_idx, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 14-May-2015

    first_epoch_start = min([mse_epochs.start]);
    last_epoch_end = max([mse_epochs.end]);
    
    title_str = ['Epochs ' datestr(first_epoch_start, 'dd-mmm-yyyy') ' to ' datestr(last_epoch_end, 'dd-mmm-yyyy')];
    
    epoch_count = length(mse_epochs_idx);
    
    h = NaN(size(mse_epochs_idx));
    
    for j = 1:epoch_count
        if mse_epochs_idx(j) <= 0
            continue;
        end
        
        firstday = mse_epochs.start(mse_epochs_idx(j));
        
        h(j) = subplot(epoch_count, 1, j);
        
        plot(mse_epochs.scales, mse_epochs.result(:, :, mse_epochs_idx(j)));
        
        set(gca, 'xlim', [0 mse_epochs.scales(end)]);
        axis manual;
        
        if j == 1
            title(title_str);
        end
        
        ylabel(datestr(firstday, 'dd-mmm'));
        
        if j == 1
            legend(num2str(mse_epochs.m_range', 'm = %-d'), 'Location', 'NorthEast');
        end
        
        if j < find(mse_epochs_idx > 0, 1, 'last')
            set(gca,'XTickLabel', []);
        else
            xlabel('MSE Scale')
        end
        
        p = get(gca, 'Position');
        p_diff = p(4) * 0.2;
        p(4) = p(4) + p_diff;
        p(2) = p(2) - ((epoch_count - j) * (p_diff / (epoch_count - 1)));
        set(gca, 'Position', p);
    end
    
    % Bring legend to top of stack.
    uistack(h(1), 'top')
    
    p = get(gcf, 'Position');
    p(3) = 1.5 * p(3);
    p(4) = 1.5 * p(4);
    set(gcf, 'Position', p);
    
    p = get(gcf, 'PaperPosition');
    p(3) = 1.5 * p(3);
    p(4) = 1.5 * p(4);
    set(gcf, 'PaperPosition', p);
end
