function visualise_features_location_v2_lasso(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 04-Nov-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats';
    
    [data_raw, classification_properties] = load_features_location_v2(varargin{:});
    
    assert(size(data_raw, 2) > 0, 'No data loaded.');
    
    data_full = data_raw';
    
    FEATURES = size(data_full, 2);
    
    valid_qids = ~isnan(classification_properties.QIDS);
    
    params = get_configuration('features_location_v2', varargin{:});
    
    function feature_rank = get_feature_rank(data_full, classification)
        feature_rank = NaN(FEATURES, 1);
        
        B = lasso(data_full, classification);
        
        features_count = sum(B ~= 0, 1);
        
        for f = 1:FEATURES
            features_used = find(B(:, find(features_count == f, 1, 'last')) ~= 0);
            new_features_used = features_used(~ismember(features_used, feature_rank));
            
            if ~isempty(new_features_used)
                feature_rank(f - length(new_features_used) + 1:f) = new_features_used;
            end
        end
        
        missing_features = find(~ismember(1:FEATURES, feature_rank));
        
        if numel(missing_features) > 0
            feature_rank((FEATURES - numel(missing_features) + 1):FEATURES) = missing_features;
        end
    end
    
    function prepare_and_save_figure(subdir, filename, varargin)
        save_figure(filename, 'output_sub_dir', [OUTPUT_SUB_DIR '/' subdir], 'FontSize', 50, varargin{:});
    end

    for classification_type = fieldnames(params.cohort_classification)'
        this_classification = params.cohort_classification.(classification_type{:});
        this_classification_title = strjoin(this_classification.names, ' vs. ');
        
        valid_idx = ismember(classification_properties.class, [this_classification.classes{:}]);
        
        this_class_labels = NaN(size(classification_properties.class));
        
        for ci = 1:numel(this_classification.classes)
            this_class_labels(ismember(classification_properties.class, this_classification.classes{ci})) = ci;
        end
        
        feature_rank = get_feature_rank(data_full(valid_idx, :), this_class_labels(valid_idx));
        
        fprintf('%s LASSO features:', this_classification_title);
        fprintf(' %s', params.feature_short{feature_rank});
        fprintf('\n');
        
        for feature_comb = combnk(1:3, 2)'
            figure
            hold on;
            for ci = 1:numel(this_classification.classes)
                this_class_idx = ismember(classification_properties.class, this_classification.classes{ci});
                
                scatter(data_full(this_class_idx, feature_rank(feature_comb(1))), data_full(this_class_idx, feature_rank(feature_comb(2))), 20, params.clr_cohort(this_classification.clr_idx(ci), :));
            end
            hold off;
            title(sprintf('LASSO Features %i & %i', feature_comb));
            xlabel(params.feature_name{feature_rank(feature_comb(1))});
            ylabel(params.feature_name{feature_rank(feature_comb(2))});
            legend(this_classification.names, 'Location', 'South', 'Orientation', 'horizontal');
            prepare_and_save_figure('feature-selection/lasso', sprintf('features_cohort_%s_lasso_f%i_f%i', classification_type{:}, feature_comb), varargin{:}, 'FontSize', 30);
        end

        figure
        for ci = 1:numel(this_classification.classes)
            this_class_idx = ismember(classification_properties.class, this_classification.classes{ci});
            
            scatter3(data_full(this_class_idx, feature_rank(1)), data_full(this_class_idx, feature_rank(2)), data_full(this_class_idx, feature_rank(3)), 20, params.clr_cohort(this_classification.clr_idx(ci), :));
            if ci == 1
                hold on;
            end
        end
        hold off;
        title('LASSO Features 1, 2 & 3');
        xlabel(params.feature_name{feature_rank(1)});
        ylabel(params.feature_name{feature_rank(2)});
        zlabel(params.feature_name{feature_rank(3)});
        legend(this_classification.names, 'Location', 'North', 'Orientation', 'horizontal');
        prepare_and_save_figure('feature-selection/lasso', sprintf('features_cohort_%s_lasso_f1_f2_f3', classification_type{:}), varargin{:}, 'FontSize', 30);
    end
    
    for t = 1:5
        if t == 1
            test_valid_qids = valid_qids;
        elseif t == 5
            test_classes = 0:1;
            
            test_valid_qids = valid_qids & ismember(classification_properties.class, test_classes);
        else
            test_classes = t - 2;
            
            test_valid_qids = valid_qids & (classification_properties.class == test_classes);
        end
        
        this_test_description = params.test_description{t};
        this_test_description_filename = params.test_description_filename{t};

        feature_rank = get_feature_rank(data_full(test_valid_qids, :), classification_properties.QIDS(test_valid_qids));

        fprintf('QIDS LASSO features (%s):', this_test_description);
        fprintf(' %s', params.feature_short{feature_rank});
        fprintf('\n');
        
        for feature_comb = combnk(1:3, 2)'
            figure
            scatter(data_full(test_valid_qids, feature_rank(feature_comb(1))), data_full(test_valid_qids, feature_rank(feature_comb(2))), [], classification_properties.QIDS(test_valid_qids));
            title(sprintf('LASSO Features %i & %i (%s)', feature_comb, this_test_description));
            xlabel(params.feature_name{feature_rank(feature_comb(1))});
            ylabel(params.feature_name{feature_rank(feature_comb(2))});
            colormap(flipud(autumn));
            h = colorbar;
            ylabel(h, 'QIDS Score');
            prepare_and_save_figure('feature-selection/lasso', sprintf('features_qids_lasso_f%i_f%i_%s', feature_comb, this_test_description_filename), varargin{:}, 'FontSize', 16);
            
            if numel(unique(classification_properties.QIDS(test_valid_qids) >= params.qids_threshold)) > 1
                figure
                h = cell(2, 1);
                this_valid = test_valid_qids & (classification_properties.QIDS < params.qids_threshold);
                h{1} = scatter(data_full(this_valid, feature_rank(feature_comb(1))), data_full(this_valid, feature_rank(feature_comb(2))), 50, classification_properties.QIDS(this_valid), 'filled', 'o');
                hold on
                this_valid = test_valid_qids & (classification_properties.QIDS >= params.qids_threshold);
                h{2} = scatter(data_full(this_valid, feature_rank(feature_comb(1))), data_full(this_valid, feature_rank(feature_comb(2))), 150, classification_properties.QIDS(this_valid), 'filled', 'p');

                axis(axis);
                limits = axis;

                B = mnrfit(data_full(test_valid_qids, [feature_rank(feature_comb(1)), feature_rank(feature_comb(2))]), (classification_properties.QIDS(test_valid_qids) >= params.qids_threshold) + 1);
                boundary_y = (-B(1) - (B(2) * limits(1:2)) - log((1 / (1 - (sum(this_valid) / sum(test_valid_qids)))) - 1)) ./ B(3);
                plot(limits(1:2), boundary_y, 'LineWidth', 1.5, 'Color', params.clr_cohort(3, :));
                hold off;

                legend([h{:}], {['QIDS < ' num2str(params.qids_threshold)], ['QIDS ' char(8805) ' ' num2str(params.qids_threshold)]}, 'Location', 'SouthEast');
                title(sprintf('LASSO Features %i & %i (%s)', feature_comb, this_test_description));
                xlabel(params.feature_name{feature_rank(feature_comb(1))});
                ylabel(params.feature_name{feature_rank(feature_comb(2))});
                colormap(flipud(copper));
                h = colorbar;
                ylabel(h, 'QIDS Score');
                prepare_and_save_figure('feature-selection/lasso', sprintf('features_qids_lasso_f%i_f%i_threshold_%s', feature_comb, this_test_description_filename), varargin{:}, 'FontSize', 16, 'save_figure', true);
            end
        end
    end
end
