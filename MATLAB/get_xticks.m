function [xticks, xticks_str] = get_xticks(t_min, t_max, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 18-Mar-2015

    custom_format = get_argument({'custom_format', 'customformat'}, [], varargin{:});
    interval_multiplier = get_argument({'interval_multiplier', 'intervalmultiplier'}, 1, varargin{:});

    t_days = t_max - t_min;

    xticks = [];

    t_current = t_min;

    dateformat = {'dd-mmm'};

    while t_current <= t_max
        if (t_current <= t_max)
            xticks = [xticks t_current]; %#ok<AGROW>
        end
        if t_days <= 4
            t_current = addtodate(t_current, 12 * interval_multiplier, 'hour');
            dateformat = {'ddd HH:MM', 'ddd HH:MM'};
        elseif t_days <= 7
            t_current = addtodate(t_current, 1 * interval_multiplier, 'day');
        elseif t_days <= 14
            t_current = addtodate(t_current, 2 * interval_multiplier, 'day');
        elseif t_days <= 31
            t_current = addtodate(t_current, 7 * interval_multiplier, 'day');
        elseif t_days <= (31 * 6)
            t_current = addtodate(t_current, 1 * interval_multiplier, 'month');
        elseif t_days <= (356)
            t_current = addtodate(t_current, 2 * interval_multiplier, 'month');
        elseif t_days <= (4 * 356)
            t_current = addtodate(t_current, 6 * interval_multiplier, 'month');
            dateformat = {'mmm yyyy'};
        else
            t_current = addtodate(t_current, 1 * interval_multiplier, 'year');
            dateformat = {'mmm yyyy'};
        end
    end
    
    if ~isempty(custom_format)
        dateformat = {custom_format};
    end

    xticks_str = cell(size(xticks));
    for i = 1:length(xticks)
        xticks_str{i} = datestr(xticks(i), dateformat{mod(i - 1, length(dateformat)) + 1});
    end
end
