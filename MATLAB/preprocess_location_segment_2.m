function preprocess_location_segment_2(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 23-Jul-2015

    paths = get_paths(varargin{:});
    
    variant = get_argument('variant', '', varargin{:});
    combine_variants = get_argument({'combinevariants', 'combine_variants'}, {}, varargin{:});
    
    if isempty(variant) && ~isempty(combine_variants)
        variant = 'combined';

        for i = 1:length(combine_variants)
            this_variant_name = combine_variants{i};

            if isempty(this_variant_name)
                this_variant_name = 'std';
            end

            variant = [variant '_' this_variant_name]; %#ok<AGROW>
        end
    end

    variant_out = '';
    
    if ~isempty(variant)
        variant_out = ['_' variant];
    end

    for file = get_participants(varargin{:})'
        fprintf('Processing %s\n', file.name)

        output_path = [paths.working_path file.name '-'];

        reporting = load_reporting_data(file.name, varargin{:});

        reporting_versions_idx = strcmp([reporting.setting], 'version');
        reporting_versions = cellfun(@str2num, reporting.value(reporting_versions_idx));
        reporting_versions_upgraded = reporting_versions >= 1031;
        
        if sum(reporting_versions_upgraded) > 0
            upgrade_idx = find(diff([0; reporting_versions_upgraded]), 1, 'last');

            reporting_versions_idx = find(reporting_versions_idx);

            upgrade_time = reporting.time(reporting_versions_idx(upgrade_idx));
            
            t_min = floor(upgrade_time) + 1;
        else
            fprintf('  Not upgraded\n');
            
            continue;
        end
        
        data = load_location_preprocessed(file.name, varargin{:}, 'variant', variant);

        if ~isempty(data) && isfield(data, 'normalised') && ...
                ((isfield(data.normalised, 'filtered') && ~isempty(data.normalised.filtered.time)) || ...
                (isfield(data.normalised, 'downsampled') && ~isempty(data.normalised.downsampled.time)))
            fprintf('  Segmenting data: ');
            tic;

            segmented = struct();
            
            if (isfield(data.normalised, 'downsampled') && ~isempty(data.normalised.downsampled.time))
                source_data = 'downsampled';
            elseif (isfield(data.normalised, 'filtered') && ~isempty(data.normalised.filtered.time))
                source_data = 'filtered';
            end
            
            valid_time = data.normalised.(source_data).time >= t_min;
            valid_time = valid_time & [true; (diff(data.normalised.(source_data).time) > 0)];
            
            segmented.time = data.normalised.(source_data).time(valid_time);
            segmented.data = data.normalised.(source_data).data(valid_time);
            segmented.lat = data.normalised.(source_data).lat(valid_time);
            segmented.lon = data.normalised.(source_data).lon(valid_time);
            segmented.state = zeros(size(segmented.time));
            
            if sum(valid_time) > 0
                location_dxydt = calculate_location_differential(segmented);
                location_dxydt_avg = zeros(size(location_dxydt.data));
                location_dxydt_avg_back = zeros(size(location_dxydt.data));
                location_dxydt_avg_fwd = zeros(size(location_dxydt.data));

                t_min = 1;
                t_max = 1;

                window_size = 1 / (24 * 6);

                for t = 1:length(location_dxydt.data)
                    while segmented.time(t_min) < (segmented.time(t) - (window_size / 2))
                        t_min = t_min + 1;
                    end
                    while (t_max + 1) < numel(segmented.time) && segmented.time(t_max + 1) < (segmented.time(t) + (window_size / 2))
                        t_max = t_max + 1;
                    end

                    location_dxydt_avg(t) = mean(location_dxydt.data(t_min:t_max));
                    location_dxydt_avg_back(t) = mean(location_dxydt.data(t_min:t));
                    location_dxydt_avg_fwd(t) = mean(location_dxydt.data(t:t_max));
                end

                % States: 1 = Static location; 2 = Travelling
                segmented.state = double((location_dxydt_avg > 1.5) & (location_dxydt_avg_back > 1.5) & (location_dxydt_avg_fwd > 1.5) & (location_dxydt.dxy > 0.4)) + 1;
                
                state_transitions = find(abs([0; diff(segmented.state)]) == 1);
                short_transitions = state_transitions([false; diff(state_transitions) == 1]) - 1;
                short_transitions_to_static = short_transitions(segmented.state(short_transitions) == 1);
                segmented.state(short_transitions_to_static) = 2;
            end
            
            data.normalised.filtered.segmented = segmented;
            
            save([output_path 'location' variant_out '-preprocessed' ], 'data');

            toc;
        else
            fprintf('  No data loaded\n');
        end
    end
end
