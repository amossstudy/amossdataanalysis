function save_text(filename, options, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 23-Sep-2015

    paths = get_paths(options{:});
    
    output_path = paths.output_path;
    
    filename_suffix = get_argument({'filenamesuffix', 'filename_suffix', 'filesuffix', 'file_suffix'}, '', options{:});
    filename_extension = get_argument({'filenameextension', 'filename_extension', 'fileextension', 'file_extension'}, 'txt', options{:});
    save_figure = get_argument({'savefigure', 'save_figure', 'savefigures', 'save_figures'}, true, options{:});
    save_txt = get_argument({'savetxt', 'save_txt', 'savetext', 'save_text'}, true, options{:});
    force_save_txt = get_argument({'forcesavetxt', 'force_save_txt', 'forcesavetext', 'force_save_text'}, false, options{:});
    append = get_argument({'append', 'appendoutput', 'append_output', 'appendtxt', 'append_txt', 'appendtext', 'append_text'}, true, options{:});
    
    if ~isempty(filename_extension) && filename_extension(1) == '.'
        filename_extension = filename_extension(2:end);
    end
    
    if (save_figure && save_txt) || force_save_txt
        if exist(output_path, 'dir') ~= 7
            mkdir(output_path);
        end

        if append
            file_mode = 'at';
        else
            file_mode = 'wt';
        end
        fid = fopen([output_path filename filename_suffix '.' filename_extension], file_mode, 'n', 'UTF-8');
        fprintf(fid, varargin{:});
        fclose(fid);
    end
end
