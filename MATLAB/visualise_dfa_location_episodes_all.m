function visualise_dfa_location_episodes_all(varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 23-Dec-2014

    close all;
    
    figure;
    
    for file = get_participants(varargin{:})'
        fprintf('Adding trace(s) for %s: ', file.name)
        tic;

        dfa = load_location_dfa(file.name);

        if isstruct(dfa)
            if isstruct(dfa) && isfield(dfa, 'range') && ~isempty(dfa.range) && isfield(dfa, 'result') && ~isempty(dfa.result)
                loglog(dfa.range, dfa.result);

                hold on;
            end
            
            if isfield(dfa, 'episodes') && ...
                    isfield(dfa.episodes, 'range') && ~isempty(dfa.episodes.range) && ...
                    isfield(dfa.episodes, 'result') && ~isempty(dfa.episodes.result)
                for i = 1:size(dfa.episodes.result, 2)
                    loglog(dfa.episodes.range, dfa.episodes.result(:,i));
                end
            end
            
            toc;
        else
            fprintf('No data loaded\n')
        end
    end
    
    set(gca, 'FontSize', 16);
    
    ylabel('Average Fluctuation in Box')
    xlabel('DFA Box Size (samples)')
end
