function visualise_data_overview_activity(participant, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    close all;
    
    OUTPUT_SUB_DIR = 'data-overview';
    
    data_ds = load_activity_data(participant, varargin{:});
    
    %%
    if ~isempty(data_ds)
        %%
        % In case there is overlap between different data files. Usually
        % this would not be required.
        fprintf('  Downsampling by second : ');
        tic; activity_ds = downsample_second(data_ds); toc;
        %%
        fprintf('  Downsampling by epoch : ');
        tic; activity_ds2 = downsample_epoch(activity_ds, 10, 0); toc;

        fprintf('  Interleaving : ');
        tic; activity_ds3 = interleave_epoch(activity_ds2, varargin{:}); toc;
        %%
        fprintf('  Plotting : ');
        tic; plot_activity(activity_ds3, varargin{:}); toc;
        %%
        fprintf('  Saving : ');
        tic;
        save_figure([participant '-activity'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        save_data([participant '-activity'], activity_ds3, 'root_path_type', 'output_path', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
        %%
        fprintf('  Downsampling (simple) : ');
        tic; activity_ds_simple = downsample_epoch(activity_ds, 30, 0); toc;
        fprintf('  Interleaving (simple) : ');
        tic; activity_ds_simple3 = interleave_epoch(activity_ds_simple); toc;
        
        fprintf('  Plotting (simple) : ');
        tic; plot_activity_simple(activity_ds_simple3, varargin{:}); toc;
        %%
        fprintf('  Saving (simple) : ');
        tic;
        save_figure([participant '-activity-simple'], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
        save_data([participant '-activity-simple'], activity_ds_simple3, 'root_path_type', 'output_path', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        toc;
    else
        fprintf('  No data loaded\n');
    end
end