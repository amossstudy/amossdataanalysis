function extract_features_location_mse(participant, varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 06-Dec-2014

    FEATURES = 3;
    
    features = load_location_features(participant, varargin{:});
    
    features.mse = NaN(FEATURES, 1);
    
    mse = load_location_mse(participant, varargin{:});
    
    FILTER_LENGTH = 51;
    
    if isstruct(mse)
        epoch_count = 0;
        
        if isfield(features, 'episodes') && isfield(features.episodes, 'start')
            epoch_count = size(features.episodes.start, 2);
        end
        
        features.episodes.mse = NaN(FEATURES, epoch_count);
        features.episodes.mse_is_valid = false(epoch_count, 1);
        
        i = 1;
        
        for j = 0:epoch_count
            this_mse = [];
            
            if (j == 0) && isfield(mse, 'result') && ~isempty(mse.result)
                this_mse = mse.result(:, i);
            elseif (j > 0) && isfield(mse, 'episodes') && isfield(mse.episodes, 'result') && ~isempty(mse.episodes.result)
                for k = 1:epoch_count
                    if (features.episodes.start(j) == mse.episodes.start(k)) && (features.episodes.end(j) == mse.episodes.end(k))
                        this_mse = mse.episodes.result(:, i, k);
                        break;
                    end
                end
            end
            
            if isempty(this_mse)
                continue;
            end
            
            features_mse = NaN(FEATURES, 1);
            
            this_mse(isnan(this_mse)) = 0; %#ok<AGROW>
            this_mse(isinf(this_mse)) = 0; %#ok<AGROW>
            
            data_filtered = conv(this_mse, repmat(1/FILTER_LENGTH, FILTER_LENGTH, 1), 'valid');
            data_filtered(isnan(data_filtered)) = 0;
            
            features_mse(1) = sum(data_filtered);
            features_mse(2) = var(this_mse - [zeros(floor(FILTER_LENGTH / 2), 1); data_filtered; zeros(floor(FILTER_LENGTH / 2), 1)]);
            if sum(this_mse) == 0
                features_mse(3) = 0;
            else
                features_mse(3) = find(this_mse > 0, 1, 'last');
            end
            
            if (j == 0)
                features.mse = features_mse;
                features.mse_is_valid = true;
            else
                features.episodes.mse(:, j) = features_mse;
                features.episodes.mse_is_valid(j) = true;
            end
        end
    else
        fprintf('  No data loaded\n');
    end
    
    save_data([participant '-location-features'], features, varargin{:});
end
