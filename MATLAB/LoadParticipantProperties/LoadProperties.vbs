' AMoSS Participant Properties Loader - A script to load participant
' properties for use with MATLAB scripts.
'
' Copyright (c) 2015, Nick Palmius (University of Oxford)
' All rights reserved.
'
' Redistribution and use in source and binary forms, with or without
' modification, are permitted provided that the following conditions are
' met:
'
' 1. Redistributions of source code must retain the above copyright
'    notice, this list of conditions and the following disclaimer.
'
' 2. Redistributions in binary form must reproduce the above copyright
'    notice, this list of conditions and the following disclaimer in the
'    documentation and/or other materials provided with the distribution.
'
' 3. Neither the name of the University of Oxford nor the names of its
'    contributors may be used to endorse or promote products derived
'    from this software without specific prior written permission.
'
' THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
' "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
' LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
' A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
' HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
' SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
' LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
' DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
' THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
' (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
' OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
' Contact: npalmius@googlemail.com
' Originally written by Nick Palmius, 27-Feb-2015

Option Explicit

Dim wsh, fso, xl, txt

Set wsh = CreateObject("WScript.Shell")
Set fso = CreateObject("Scripting.FileSystemObject")

Dim strOutDir, strDefaultDir, strSettingsFile, strOutFile, strFilename

strOutDir = wsh.CurrentDirectory

If not right(strOutDir, 1) = "\" Then
  strOutDir = strOutDir & "\"
End If

strSettingsFile = strOutDir & ".LoadProperties"
strOutFile = strOutDir & "participant_properties.m"

strDefaultDir = strOutDir

If fso.FileExists(strSettingsFile) Then
  Set txt = fso.OpenTextFile(strSettingsFile, 1, False)
  Dim strLastFile
  If Not txt.AtEndOfStream Then
    strLastFile = txt.ReadLine
    strLastFile = fso.GetParentFolderName(strLastFile)
    If fso.FolderExists(strLastFile) Then
      strDefaultDir = strLastFile
    End If
  End If
  Call txt.Close
End If

Const xlDown = -4121
Const xlToRight = -4161

Set xl = CreateObject("Excel.Application")

Call xl.ExecuteExcel4Macro("DIRECTORY(""" + strDefaultDir + """)")

strFilename = xl.GetOpenFilename("Excel Files (*.xls*),*.xls*", 0, "Please choose the AMoSS Participant Info file (V2)")

If not strFilename = False Then
  Dim wb, ws, i, f
  Dim participant, classification
  Dim strLinePrefix
  Dim strColumnParticipant, strColumnClassification
  Dim strColumnAge, strColumnGender, strColumnWeight, strColumnHeight, strColumnBMI
  Dim strColumnBDType
  Dim strColumnDateEntered, strColumnDateLeft
  Dim strColumnBISAttentional, strColumnBISMotor, strColumnBISNonplanning, strColumnBISTotal
  Dim strColumnIPDE
  Dim strColumnEmploymentStatus
  Dim iDemographicValue
  Dim iBDType
  Dim dateCurrent, strDateVec
  Dim iBISValue, iIPDEValue
  Dim iEmploymentStatusValue
  Dim iBlankColumns
  
  If fso.FileExists(strSettingsFile) Then
    Call fso.DeleteFile(strSettingsFile)
  End If
  Set txt = fso.OpenTextFile(strSettingsFile, 2, True)
  Call txt.WriteLine(strFilename)
  Call txt.Close
  Set f = fso.GetFile(strSettingsFile)
  f.Attributes = f.Attributes Or 2 ' Add 2 = Hidden file attribute
  
  Set txt = fso.OpenTextFile(strOutFile, 2, True)
  
  Set wb = xl.Workbooks.Open(strFilename)
  Set ws = wb.Sheets(1)
  
  strColumnAge = ""
  strColumnGender = ""
  strColumnWeight = ""
  strColumnHeight = ""
  strColumnBMI = ""
  strColumnBDType = ""
  strColumnDateEntered = ""
  strColumnDateLeft = ""
  strColumnBISAttentional = ""
  strColumnBISMotor = ""
  strColumnBISNonplanning = ""
  strColumnBISTotal = ""
  strColumnIPDE = ""
  strColumnEmploymentStatus = ""
  
  iBlankColumns = 0
  
  For i = 1 To (ws.Rows(1).Columns.Count)
    Dim strColumn
    strColumn = split(ws.Cells(1, i).address(True, False), "$")(0)
    select case lcase(ws.Cells(1, i).value)
    case "participant_number"
      strColumnParticipant = strColumn
    case "code"
      strColumnClassification = strColumn
    case "age"
      strColumnAge = strColumn
    case "gender"
      strColumnGender = strColumn
    case "weight_kg"
      strColumnWeight = strColumn
    case "height_cm"
      strColumnHeight = strColumn
    case "bmi"
      strColumnBMI = strColumn
    case "bdi/bdii"
      strColumnBDType = strColumn
    case "date_entered_study"
      strColumnDateEntered = strColumn
    case "date_left_study"
      strColumnDateLeft = strColumn
    case "bis_attentional score"
      strColumnBISAttentional = strColumn
    case "bis_motor score"
      strColumnBISMotor = strColumn
    case "bis_nonplanning score"
      strColumnBISNonplanning = strColumn
    case "bis11_total score"
      strColumnBISTotal = strColumn
    case "ipde dimensional score"
      strColumnIPDE = strColumn
    case "emplyment_status"
      strColumnEmploymentStatus = strColumn
    case "employment_status"
      strColumnEmploymentStatus = strColumn
    end select
    
    If len(ws.Cells(1, i).value) = 0 Then
      iBlankColumns = iBlankColumns + 1
      
      If iBlankColumns > 5 Then
        Exit For
      End If
    Else
      iBlankColumns = 0
    End If
  Next
  
  For i = 2 To (ws.Range(strColumnParticipant & "2", ws.Range(strColumnParticipant & "2").End(xlDown)).Rows.Count + 1)
    participant = ws.Range(strColumnParticipant & CStr(i)).value
    classification = ws.Range(strColumnClassification & CStr(i)).value
    If IsNumeric(classification) Then
      If CLng(classification) >= 1 And CLng(classification) <= 3 Then
        strLinePrefix = "set_participant_property('" & participant & "'"
        Call txt.Write(strLinePrefix & ", 'classification', " & CStr(CLng(classification) Mod 3))
        strLinePrefix = ", ..." & vbCrLf & Space(len(strLinePrefix) + 2)
        If Len(strColumnAge) > 0 Then
          iDemographicValue = ws.Range(strColumnAge & CStr(i)).value
          If len(iDemographicValue) > 0 And IsNumeric(iDemographicValue) Then
            Call txt.Write(strLinePrefix & "'age', " & iDemographicValue)
          Else
            Call txt.Write(strLinePrefix & "'age', []")
          End If
        End If
        If Len(strColumnGender) > 0 Then
          iDemographicValue = ws.Range(strColumnGender & CStr(i)).value
          If len(iDemographicValue) > 0 And IsNumeric(iDemographicValue) Then
            Call txt.Write(strLinePrefix & "'gender', " & iDemographicValue)
          Else
            Call txt.Write(strLinePrefix & "'gender', []")
          End If
        End If
        If Len(strColumnWeight) > 0 Then
          iDemographicValue = ws.Range(strColumnWeight & CStr(i)).value
          If len(iDemographicValue) > 0 And IsNumeric(iDemographicValue) Then
            Call txt.Write(strLinePrefix & "'weight', " & iDemographicValue)
          Else
            Call txt.Write(strLinePrefix & "'weight', []")
          End If
        End If
        If Len(strColumnHeight) > 0 Then
          iDemographicValue = ws.Range(strColumnHeight & CStr(i)).value
          If len(iDemographicValue) > 0 And IsNumeric(iDemographicValue) Then
            Call txt.Write(strLinePrefix & "'height', " & iDemographicValue)
          Else
            Call txt.Write(strLinePrefix & "'height', []")
          End If
        End If
        If Len(strColumnBMI) > 0 Then
          iDemographicValue = ws.Range(strColumnBMI & CStr(i)).value
          If len(iDemographicValue) > 0 And IsNumeric(iDemographicValue) Then
            Call txt.Write(strLinePrefix & "'bmi', " & iDemographicValue)
          Else
            Call txt.Write(strLinePrefix & "'bmi', []")
          End If
        End If
        If Len(strColumnBDType) > 0 Then
          iBDType = ws.Range(strColumnBDType & CStr(i)).value
          If len(iBDType) > 0 And IsNumeric(iBDType) And (CInt(iBDType) > 0 Or CLng(classification) = 1) Then
            Call txt.Write(strLinePrefix & "'bd_type', " & iBDType)
          Else
            Call txt.Write(strLinePrefix & "'bd_type', []")
          End If
        End If
        If Len(strColumnDateEntered) > 0 Then
          dateCurrent = ws.Range(strColumnDateEntered & CStr(i)).value
          If len(dateCurrent) > 0 And IsDate(dateCurrent) Then
            strDateVec = "[" & Year(dateCurrent) & ", " & Month(dateCurrent) & ", " & Day(dateCurrent) & "]"
            Call txt.Write(strLinePrefix & "'date_entered', " & strDateVec)
          Else
            Call txt.Write(strLinePrefix & "'date_entered', []")
          End If
        End If
        If Len(strColumnDateLeft) > 0 Then
          dateCurrent = ws.Range(strColumnDateLeft & CStr(i)).value
          If len(dateCurrent) > 0 And IsDate(dateCurrent) Then
            strDateVec = "[" & Year(dateCurrent) & ", " & Month(dateCurrent) & ", " & Day(dateCurrent) & "]"
            Call txt.Write(strLinePrefix & "'date_left', " & strDateVec)
          Else
            Call txt.Write(strLinePrefix & "'date_left', []")
          End If
        End If
        If Len(strColumnBISAttentional) > 0 Then
          iBISValue = ws.Range(strColumnBISAttentional & CStr(i)).value
          If len(iBISValue) > 0 And IsNumeric(iBISValue) Then
            Call txt.Write(strLinePrefix & "'bis_attentional', " & iBISValue)
          Else
            Call txt.Write(strLinePrefix & "'bis_attentional', []")
          End If
        End If
        If Len(strColumnBISMotor) > 0 Then
          iBISValue = ws.Range(strColumnBISMotor & CStr(i)).value
          If len(iBISValue) > 0 And IsNumeric(iBISValue) Then
            Call txt.Write(strLinePrefix & "'bis_motor', " & iBISValue)
          Else
            Call txt.Write(strLinePrefix & "'bis_motor', []")
          End If
        End If
        If Len(strColumnBISNonplanning) > 0 Then
          iBISValue = ws.Range(strColumnBISNonplanning & CStr(i)).value
          If len(iBISValue) > 0 And IsNumeric(iBISValue) Then
            Call txt.Write(strLinePrefix & "'bis_nonplanning', " & iBISValue)
          Else
            Call txt.Write(strLinePrefix & "'bis_nonplanning', []")
          End If
        End If
        If Len(strColumnBISTotal) > 0 Then
          iBISValue = ws.Range(strColumnBISTotal & CStr(i)).value
          If len(iBISValue) > 0 And IsNumeric(iBISValue) Then
            Call txt.Write(strLinePrefix & "'bis_total', " & iBISValue)
          Else
            Call txt.Write(strLinePrefix & "'bis_total', []")
          End If
        End If
        If Len(strColumnIPDE) > 0 Then
          iIPDEValue = ws.Range(strColumnIPDE & CStr(i)).value
          If len(iIPDEValue) > 0 And IsNumeric(iIPDEValue) Then
            Call txt.Write(strLinePrefix & "'ipde_dimensional', " & iIPDEValue)
          Else
            Call txt.Write(strLinePrefix & "'ipde_dimensional', []")
          End If
        End If
        If Len(strColumnEmploymentStatus) > 0 Then
          iEmploymentStatusValue = ws.Range(strColumnEmploymentStatus & CStr(i)).value
          If len(iEmploymentStatusValue) > 0 And IsNumeric(iEmploymentStatusValue) Then
            Call txt.Write(strLinePrefix & "'employment_status', " & iEmploymentStatusValue)
          Else
            Call txt.Write(strLinePrefix & "'employment_status', []")
          End If
        End If
        Call txt.Write(")" & vbCrLf)
      Else
        Call txt.WriteLine("clear_participant_properties('" & participant & "');")
      End If
    Else
      Call txt.WriteLine("clear_participant_properties('" & participant & "');")
    End If
  Next
  
  Call wb.close
  Call xl.quit
  
  Call txt.Close
  
  Call wscript.echo("Done!")
Else
  Call wscript.echo("Nothing to do!")
End If