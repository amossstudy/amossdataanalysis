function data = load_true_colours_file(file)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 28-Dec-2014

    fid = fopen(file, 'r');
    
    if (fid == -1)
        errordlg(['Could not open file ' file], 'Error', 'modal');
        return;
    end

    thisline = fgetl(fid);
    
    data = struct;
    data_index = struct;
    
    if ischar(thisline)
        % Skip header
        thisline = fgetl(fid);
        
        while ischar(thisline)
            fields = regexp(thisline, ',', 'split');
            
            field_name = fields{2};
            field_name(isspace(field_name)) = '_';
            field_name(ismember(field_name, '-')) = '_';
            
            if length(fields) == 5
                if ~isfield(data, field_name)
                    data.(field_name) = struct;
                    data.(field_name).time = zeros(0);
                    data.(field_name).message = cell(0);
                    if strcmpi(field_name, 'ALTMAN') == 1
                        data.(field_name).questions = NaN(0, 5);
                    elseif strcmpi(field_name, 'EQ_5D') == 1
                        data.(field_name).questions = NaN(0, 5);
                    elseif strcmpi(field_name, 'GAD_7') == 1
                        data.(field_name).questions = NaN(0, 7);
                    elseif strcmpi(field_name, 'QIDS') == 1
                        data.(field_name).questions = NaN(0, 16);
                    end
                    data.(field_name).data = zeros(0);
                    data_index.(field_name) = 1;
                end
                if ~isempty(regexpi(fields{3}, '[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]'))
                    this_index = data_index.(field_name);
                    data.(field_name).time(this_index, 1) = datenum(fields{3}, 'dd/mm/yyyy HH:MM:SS');
                    msg = fields{4};
                    data.(field_name).message{this_index, 1} = msg;
                    msg = strrep(msg, ' ', '');
                    if strcmpi(field_name, 'ALTMAN') == 1
                        data.(field_name).questions(this_index, :) = NaN(1, 5);
                        if length(msg) == 6
                            for i = 2:6
                                if ~isempty(str2double(msg(i)))
                                    data.(field_name).questions(this_index, i - 1) = str2double(msg(i));
                                end
                            end
                        end
                    elseif strcmpi(field_name, 'EQ_5D') == 1
                        msg = fields{4};
                        data.(field_name).questions(this_index, :) = NaN(1, 5);
                        if (length(msg) > 6) && (msg(7) == ' ') && (sum(ismember(msg(1:6), ' ')) == 0)
                            for i = 2:6
                                if ~isempty(str2double(msg(i)))
                                    data.(field_name).questions(this_index, i - 1) = str2double(msg(i));
                                end
                            end
                        end
                    elseif strcmpi(field_name, 'GAD_7') == 1
                        data.(field_name).questions(this_index, :) = NaN(1, 7);
                        if length(msg) == 8
                            for i = 2:8
                                if ~isempty(str2double(msg(i)))
                                    data.(field_name).questions(this_index, i - 1) = str2double(msg(i));
                                end
                            end
                        end
                    elseif strcmpi(field_name, 'QIDS') == 1
                        data.(field_name).questions(this_index, :) = NaN(1, 16);
                        if length(msg) == 17
                            for i = 2:17
                                if ~isempty(str2double(msg(i)))
                                    data.(field_name).questions(this_index, i - 1) = str2double(msg(i));
                                end
                            end
                        end
                    end
                    data.(field_name).data(this_index, 1) = str2double(fields{5});

                    data_index.(field_name) = this_index + 1;
                end
            end
            thisline = fgetl(fid);
        end
    end
    
    for field_name=fieldnames(data)'
        [data.(field_name{1}).time, sort_idx] = sort(data.(field_name{1}).time);
        data.(field_name{1}).message = data.(field_name{1}).message(sort_idx);
        data.(field_name{1}).data = data.(field_name{1}).data(sort_idx);
    end
    
    fclose(fid);
end