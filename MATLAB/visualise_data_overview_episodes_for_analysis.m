function visualise_data_overview_episodes_for_analysis(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 08-May-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-overview\episode-epochs';
    
    results = get_argument('results', [], varargin{:});
    
    files = get_participants(varargin{:});
    
    data_dates = datenum([2014, 1, 1]):ceil(now);
    
    min_date = data_dates(end);
    max_date = data_dates(1);
    
    epochs_matrix = zeros(length(files), length(data_dates));
    valid_epochs_matrix = zeros(length(files), length(data_dates));
    
    episodes_types = get_configuration('episode_types');
    
    episode_max = 0;
    
    for episode_type = fieldnames(episodes_types)'
        episode_max = max(episode_max, episodes_types.(episode_type{:}).id + 1);
    end
    
    episode_name = cell(1, episode_max);
    clr = zeros(episode_max + 3, 3);
    
    clr(1, :) = [0.5 0.5 0.5];
    clr(2, :) = [0.85 0.85 0.85];
    clr(3, :) = [1 1 1];
    
    for episode_type = fieldnames(episodes_types)'
        clr(4 + episodes_types.(episode_type{:}).id, :) = episodes_types.(episode_type{:}).episode_colour;
        episode_name{episodes_types.(episode_type{:}).id + 1} = episodes_types.(episode_type{:}).display_name_short;
    end
    
    j = 1;
    
    participants = cell(length(files), 1);
    participant_start = NaN(length(files), 1);
    
    for file = files'
        fprintf('Loading %s\n', file.name)

        date_entered = get_participant_property(file.name, 'date_entered', varargin{:});
        date_left = get_participant_property(file.name, 'date_left', varargin{:});

        if ~isempty(date_entered)
            epochs_matrix(j, data_dates < datenum(date_entered)) = -1;
            valid_epochs_matrix(j, data_dates < datenum(date_entered)) = -1;
            
            participant_start(j) = datenum(date_entered);
            
            min_date = min(min_date, datenum(date_entered));
        end

        if ~isempty(date_left)
            epochs_matrix(j, data_dates > datenum(date_left)) = -1;
            valid_epochs_matrix(j, data_dates > datenum(date_left)) = -1;
            
            max_date = max(max_date, datenum(date_left));
        end

        episodes_for_analysis = load_episodes_for_analysis(file.name, varargin{:});
        
        if ~isempty(episodes_for_analysis) && isfield(episodes_for_analysis, 'epochs')
            for epoch = episodes_for_analysis.epochs
                epoch_type_id = -2;
                
                if isfield(episodes_types, epoch.type)
                    epoch_type_id = episodes_types.(epoch.type).id + 1;
                end
                
                epochs_matrix(j, data_dates >= epoch.start & data_dates <= epoch.end) = epoch_type_id;
                
                max_date = max(max_date, epoch.end);
                
                if ~isfield(epoch, 'valid') || epoch.valid
                    valid_epochs_matrix(j, data_dates >= epoch.start & data_dates <= epoch.end) = epoch_type_id;
                end
            end
        end
        
        participants{j} = file.name;
        
        j = j + 1;
    end
    
    max_date_vec = datevec(addtodate(max_date, 1, 'month'));
    max_date_vec = max_date_vec(1:3);
    max_date_vec(3) = 1;
    max_date = datenum(max_date_vec);
    
    min_date_vec = datevec(min_date);
    min_date_vec = min_date_vec(1:3);
    min_date_vec(3) = 1;
    min_date = datenum(min_date_vec);
    
    valid_dates = (data_dates <= max_date) & (data_dates >= min_date);
    
    figure;
    imagesc(data_dates(valid_dates), 1:(j - 1), epochs_matrix(1:(j - 1), valid_dates));
    
    [xticks, xticks_str] = get_xticks(min_date, max_date, varargin{:});
    
    y_ticks = 1:2:(j - 1);
    
    colormap(clr);
    set(gca, 'xtick', xticks);
    set(gca, 'xticklabel', xticks_str);
    set(gca, 'ytick', y_ticks);
    set(gca, 'yticklabel', participants(y_ticks));
    set(gca, 'clim', [-2.5 (episode_max + 0.5)]);
    hcb = colorbar;
    set(hcb, 'ylim', [0.5, (episode_max + 0.5)]);
    set(hcb, 'ytick', 1:episode_max);
    set(hcb, 'xticklabel', episode_name);
    
    hold on;
    
    for i = 1:(j - 1)
        plot([min_date (participant_start(i) - 1)], [i i], ':', 'color', [0.6 0.6 0.6], 'linewidth', 1);
    end
    
    hold off;
    
    save_figure('epochs_all', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    figure;
    imagesc(data_dates(valid_dates), 1:(j - 1), valid_epochs_matrix(1:(j - 1), valid_dates));
    
    [xticks, xticks_str] = get_xticks(min_date, max_date, varargin{:});
    
    colormap(clr);
    set(gca, 'xtick', xticks);
    set(gca, 'xticklabel', xticks_str);
    set(gca, 'ytick', y_ticks);
    set(gca, 'yticklabel', participants(y_ticks));
    set(gca, 'clim', [-2.5 (episode_max + 0.5)]);
    hcb = colorbar;
    set(hcb, 'ylim', [0.5, (episode_max + 0.5)]);
    set(hcb, 'ytick', 1:episode_max);
    set(hcb, 'xticklabel', episode_name);
    
    hold on;
    
    for i = 1:(j - 1)
        plot([min_date (participant_start(i) - 1)], [i i], ':', 'color', [0.6 0.6 0.6], 'linewidth', 1);
    end
    
    hold off;
    
    save_figure('epochs_valid', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    
    if ~isempty(results)
        hold on;
        
        classification_result = results.classification_results;
        classification_result = classification_result(~isnan(classification_result));
        
        for i = 1:size(results.class, 1)
            if classification_result(i) ~= results.class(i)
                j = find(strcmp(participants, results.properties(i).participant));
                plot([results.properties(i).start results.properties(i).end], [(j + 0.5) (j - 0.5)], 'color', [1 0 0]);
                plot([results.properties(i).start results.properties(i).end], [(j - 0.5) (j + 0.5)], 'color', [1 0 0]);
            end
        end
        
        hold off;
        
        save_figure('epochs_valid_classification', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    end
end
