function act_ds = downsample_epoch(act, epoch_min, epoch_sec)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    t_vec = datevec(act.time);
    
    if epoch_min > 0
        epoch_sec = 0;
        t_vec(:,6) = 0;
        t_vec(:,5) = floor(t_vec(:,5) / epoch_min) * epoch_min;
    elseif epoch_sec > 0
        t_vec(:,6) = floor(t_vec(:,6) / epoch_sec) * epoch_sec;
    end
    
    t = datenum(t_vec);
    
    t_vec_uniq = unique(t_vec, 'rows');
    t_uniq = datenum(t_vec_uniq);
    
    data = zeros(size(t_vec_uniq, 1), 1);
    
    j = 1;
    
    for i=1:size(t_uniq, 1)
        j_min = j;
        
        assert(t(j) == t_uniq(i,:))
        
        while (t(j) == t_uniq(i,:))
            j = j + 1;
            if j > size(t, 1)
                break;
            end
        end
        j_max = j - 1;
        
        data(i) = mean(act.data(j_min:j_max));
    end
    
    act_ds.time = t_uniq;
    act_ds.data = data;
    act_ds.epoch.min = epoch_min;
    act_ds.epoch.sec = epoch_sec;
end