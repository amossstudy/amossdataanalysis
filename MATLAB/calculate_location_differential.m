function [data_differentiated] = calculate_location_differential(data_in)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 16-Jul-2015

    data_differentiated = struct();
    
    if ~isempty(data_in) && isfield(data_in, 'time') && ~isempty(data_in.time)
        location_time_diff = [(1 / (24 * 4)); diff(data_in.time)];
        location_data = [data_in.lat data_in.lon]; % for normalised coordinates, this is in km from home

        location_data_diff = [0 0; (location_data(2:end, :) - location_data(1:end-1, :))];

        % Process any duplicate times
        offset = 1;
        while sum(location_time_diff == 0) > 0
            offset = offset + 1;

            location_zero = (location_time_diff == 0);
            
            while find(location_zero, 1, 'first') <= offset
                location_time_diff(find(location_zero, 1, 'first')) = location_time_diff(1);
                location_data_diff(find(location_zero, 1, 'first'), :) = [0 0];
                location_zero(find(location_zero, 1, 'first')) = false;
            end

            temp_location_time_diff = (data_in.time(location_zero) - data_in.time(find(location_zero) - offset));
            temp_location_data_diff = (location_data(location_zero, :) - location_data(find(location_zero) - offset, :));

            location_time_diff(location_zero) = temp_location_time_diff;
            location_data_diff(location_zero, :) = temp_location_data_diff;
        end
        
        location_time_diff(location_time_diff > (1 / (24 * 4))) = NaN; % time in location in days
        location_time_diff = location_time_diff * 24; % time in location in hours

        location_data_dxy = sqrt(sum(location_data_diff.^2,2)); % Distance travelled in km

        location_data_diff = location_data_dxy ./ location_time_diff; % Speed in km/h

        location_data_diff_cleaned = location_data_diff;
        
        invalid_diff = find(location_data_diff_cleaned > 200);
        
        i1 = 1;
        i2 = 1;
        for i = invalid_diff'
            t = data_in.time(i);
            while (i1 <= numel(data_in.time)) && (data_in.time(i1) < t - (1 / (24 * 6)))
                i1 = i1 + 1;
            end
            i2 = max(i, i2);
            while (i2 <= numel(data_in.time)) && (data_in.time(i2) < t + (1 / (24 * 6)))
                i2 = i2 + 1;
            end
            i2 = i2 - 1;
            
            locations_range = location_data(i1:i2, :);
            
            locations_median = median(locations_range, 1);
            
            locations_prob = mvnpdf(location_data(i, :), locations_median, [0.1, 0; 0, 0.1] .^ 2) / mvnpdf(locations_median, locations_median, [0.1, 0; 0, 0.1] .^ 2);
            
            if locations_prob >= 0.5
                location_data_diff_cleaned(i) = 0;
            end
        end
        
        data_differentiated.data = location_data_diff_cleaned;
        data_differentiated.raw = location_data_diff;
        data_differentiated.dt = location_time_diff;
        data_differentiated.dxy = location_data_dxy;
    end
end
