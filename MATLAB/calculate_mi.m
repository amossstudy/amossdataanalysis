function mi = calculate_mi(feature_data, class)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 29-Dec-2014

% References:
% * https://en.wikipedia.org/wiki/Mutual_information

    class_valid = class(~isnan(feature_data));
    feature_data_valid = feature_data(~isnan(feature_data));

    N = length(class_valid);
    CLASSES = unique(class_valid);
    NCLASSES = length(CLASSES);
    
    % References:
    % * https://en.wikipedia.org/wiki/Freedman%E2%80%93Diaconis_rule
    % * http://www.fmrib.ox.ac.uk/analysis/techrep/tr00mj2/tr00mj2/node24.html
    % * Freedman, D., & Diaconis, P. (1981). On the histogram as a density
    %   estimator: L2 theory. Zeitschrift F�r Wahrscheinlichkeitstheorie
    %   Und Verwandte Gebiete, 57(4), 453�476.
    %   Retrieved from http://dx.doi.org/doi:10.1007/BF01025868
    % * Izenman, A. J. (1991). Recent Developments in Nonparametric
    %   Density Estimation. Journal of the American Statistical
    %   Association, 86(413), 205�224.
    %   Retrieved from http://dx.doi.org/10.1080/01621459.1991.10475021
    bin_size = 2 * iqr(feature_data_valid) * (N ^ (-1/3));
    
    NBINS = round(range(feature_data_valid) / bin_size);
    
    joint_pdf = zeros(NBINS, NCLASSES);
    
    class_pdf = zeros(NCLASSES, 1);
    
    [feature_pdf, bin_centers] = hist(feature_data_valid, NBINS);
    
    df = bin_centers(2) - bin_centers(1);
    
    feature_pdf = feature_pdf / (df * sum(feature_pdf));
    
    for c = 1:NCLASSES
        class_pdf(c) = sum(class_valid == CLASSES(c)) / N;
        
        joint_pdf(:, c) = hist(feature_data_valid(class_valid == CLASSES(c)), bin_centers);
        joint_pdf(:, c) = joint_pdf(:, c) / (df * sum(joint_pdf(:, c)));
        joint_pdf(:, c) = joint_pdf(:, c) * class_pdf(c);
    end
    
    joint_pdf = joint_pdf / (df * sum(sum(joint_pdf)));
    
    mi = 0;
    
    for y = 1:NCLASSES
        for x = 1:NBINS
            if joint_pdf(x, y) > 0 && feature_pdf(x) > 0 && class_pdf(y) > 0
                mi = mi + (joint_pdf(x, y) * df * log2(joint_pdf(x, y) / (feature_pdf(x) * class_pdf(y))));
            end
        end
    end
end
