function mood_mood = merge_mood_zoom(mood_mood1, mood_mood2, sort_data)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 17-Oct-2014

    if nargin < 3
        sort_data = 1;
    end
    
    mood_mood = struct();
    
    mood_mood.time = [mood_mood1.time; mood_mood2.time];
    if (sort_data)
        [mood_mood.time, t_sort_order] = sort(mood_mood.time);
    end
    
    fields1 = fieldnames(mood_mood1);
    fields2 = fieldnames(mood_mood2);
    
    for i = 1:numel(fields1)
        if sum(strcmp(fields1{i}, fields2)) == 0
            mood_mood.(fields1{i}) = [mood_mood1.(fields1{i}); NaN(size(mood_mood2.time))];
        else
            mood_mood.(fields1{i}) = [mood_mood1.(fields1{i}); mood_mood2.(fields1{i})];
        end
        if (sort_data)
            mood_mood.(fields1{i}) = mood_mood.(fields1{i})(t_sort_order);
        end
    end
    
    for i = 1:numel(fields2)
        if sum(strcmp(fields2{i}, fields1)) == 0
            mood_mood.(fields2{i}) = [NaN(size(mood_mood1.time)); mood_mood2.(fields2{i})];
            if (sort_data)
                mood_mood.(fields2{i}) = mood_mood.(fields2{i})(t_sort_order);
            end
        end
    end
end
