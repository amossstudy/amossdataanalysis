function [classification_data, classification_properties] = load_features_location_v2_distributions(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 04-Mar-2016

    do_loo = get_argument({'do_loo', 'doloo', 'loo'}, false, varargin{:});
    
    cohort_config = get_configuration('cohorts');
    questionnaire_config = get_configuration('questionaires');
    questionnaires = fieldnames(questionnaire_config)';
    
    classification_data = struct();
    
    [classification_data.raw, classification_properties] = load_features_location_v2(varargin{:});
    
    classification_data.p_cohort = NaN([size(classification_data.raw), 3]);
    classification_data.p_qids_lt_11 = NaN([size(classification_data.raw), 3]);
    classification_data.p_qids_ge_11 = NaN([size(classification_data.raw), 3]);
    
    classification_properties.dist_cohort = cell(size(classification_data.raw, 1), 3);
    classification_properties.dist_qids_lt_11 = cell(size(classification_data.raw, 1), 3);
    classification_properties.dist_qids_ge_11 = cell(size(classification_data.raw, 1), 3);
    
    classification_properties.unique_participant = unique(classification_properties.participant);
    
    for this_questionnaire_cell = questionnaires
        if isfield(classification_properties, this_questionnaire_cell{:})
            classification_data.(['p_' this_questionnaire_cell{:}]) = NaN(3, size(classification_data.raw, 2));
            classification_properties.(['dist_' this_questionnaire_cell{:}]) = cell(1, 3);
            
            if do_loo
                classification_data.(['p_' this_questionnaire_cell{:} '_loo']) = NaN(3, size(classification_data.raw, 2));
                classification_properties.(['dist_' this_questionnaire_cell{:} '_loo']) = cell(size(classification_properties.unique_participant, 1), 3);
            end
        end
    end
    
    tc_combined = load_true_colours_combined('true_colours_data_type', 'resampled');
    
    tc_combined_participants = {};
    
    for this_questionnaire_cell = fieldnames(tc_combined)'
        if isfield(tc_combined.(this_questionnaire_cell{:}), 'participant')
            tc_combined_participants = [tc_combined_participants; tc_combined.(this_questionnaire_cell{:}).participant]; %#ok<AGROW>
        end
    end
    
    tc_combined_participants = unique(tc_combined_participants);
    tc_combined_participants_class = NaN(size(tc_combined_participants));
    
    for this_participant_cell = tc_combined_participants'
        this_participant_class = get_participant_property(this_participant_cell{:}, 'classification', varargin{:});
        if ~isempty(this_participant_class)
            tc_combined_participants_class(strcmp(tc_combined_participants, this_participant_cell)) = this_participant_class;
        end
    end

    fprintf('Calculating cohort distributions for questionnaires.\n');
    
    for q_i = 1:numel(questionnaires)
        this_questionnaire = questionnaires{q_i};
        
        d_hist = 1;
        if strcmp(this_questionnaire, 'EQ_5D') == 1
            d_hist = 5;
        end
        
        bin_edges = (questionnaire_config.(this_questionnaire).min - (d_hist / 2)):d_hist:(questionnaire_config.(this_questionnaire).max + (d_hist / 2));
        
        test_q_value = classification_properties.(this_questionnaire);

        for c = 1:3
            fprintf(' %s questionnaire (%s):', questionnaire_config.(this_questionnaire).display_name, cohort_config.cohort_name{c});
            
            class_train_participants_idx = (tc_combined_participants_class == (c - 1));
            train_participants = tc_combined_participants(class_train_participants_idx);
            train_idx = ismember(tc_combined.(this_questionnaire).participant, train_participants);
            
            train_q_value = tc_combined.(this_questionnaire).data(train_idx);
            train_q_value = train_q_value(~isnan(train_q_value));

            dist_class = fit_distributions(train_q_value, bin_edges, varargin{:}, 'generate_figures', false, 'offset', d_hist / 2);
            classification_properties.(['dist_' this_questionnaire]){c} = dist_class;

            test_q_p = get_fitted_distribution_property(dist_class, 'pdf', test_q_value);
            classification_data.(['p_' this_questionnaire])(c, :) = test_q_p;
        end
    end
    
    if do_loo
        fprintf('Calculating LOO cohort distributions for questionnaires.\n');
        
        for this_participant_cell = classification_properties.unique_participant'
            this_participant_idx = strcmp(classification_properties.unique_participant, this_participant_cell{:});
            
            for q_i = 1:numel(questionnaires)
                this_questionnaire = questionnaires{q_i};

                train_participants_idx = ~strcmp(tc_combined_participants, this_participant_cell{:});

                d_hist = 1;
                if strcmp(this_questionnaire, 'EQ_5D') == 1
                    d_hist = 5;
                end
                
                bin_edges = (questionnaire_config.(this_questionnaire).min - (d_hist / 2)):d_hist:(questionnaire_config.(this_questionnaire).max + (d_hist / 2));

                test_idx = strcmp(classification_properties.participant, this_participant_cell{:});
                test_q_value = classification_properties.(this_questionnaire)(test_idx);
                
                for c = 1:3
                    fprintf(' %s %s (%s):', this_participant_cell{:}, questionnaire_config.(this_questionnaire).display_name, cohort_config.cohort_name{c});
                    
                    class_train_participants_idx = train_participants_idx & (tc_combined_participants_class == (c - 1));
                    train_participants = tc_combined_participants(class_train_participants_idx);
                    train_idx = ismember(tc_combined.(this_questionnaire).participant, train_participants);

                    train_q_value = tc_combined.(this_questionnaire).data(train_idx);
                    train_q_value = train_q_value(~isnan(train_q_value));

                    dist_class = fit_distributions(train_q_value, bin_edges, varargin{:}, 'generate_figures', false, 'offset', d_hist / 2);
                    classification_properties.(['dist_' this_questionnaire '_loo']){this_participant_idx, c} = dist_class;
                    
                    test_q_p = get_fitted_distribution_property(dist_class, 'pdf', test_q_value);
                    classification_data.(['p_' this_questionnaire '_loo'])(c, test_idx) = test_q_p;
                end
            end
        end
    end
    
    fprintf('Calculating feature distributions for QIDS questionnaire.\n');
    
    qids_lt_11_idx = classification_properties.QIDS < 11;
    qids_ge_11_idx = classification_properties.QIDS >= 11;
    
    for f = 1:size(classification_data.raw, 1)
        for c = 1:3            
            cohort_idx = classification_properties.class == (c - 1);
            
            data_all = classification_data.raw(f, :);
            data_cohort = data_all(cohort_idx);
            data_qids_lt_11 = data_all(cohort_idx & qids_lt_11_idx);
            data_qids_ge_11 = data_all(cohort_idx & qids_ge_11_idx);
            
            fprintf(' Feature %i (%s) cohort:', f, cohort_config.cohort_name{c});
            dist_cohort = fit_distributions(data_cohort, [], varargin{:}, 'generate_figures', false);
            classification_data.p_cohort(f, :, c) = get_fitted_distribution_property(dist_cohort, 'pdf', data_all);
            classification_properties.dist_cohort{f, c} = dist_cohort;
            
            fprintf(' Feature %i (%s) QIDS < 11:', f, cohort_config.cohort_name{c});
            dist_qids_lt_11 = fit_distributions(data_qids_lt_11, [], varargin{:}, 'generate_figures', false);
            classification_data.p_qids_lt_11(f, :, c) = get_fitted_distribution_property(dist_qids_lt_11, 'pdf', data_all);
            classification_properties.dist_qids_lt_11{f, c} = dist_qids_lt_11;
            
            fprintf(' Feature %i (%s) QIDS %s 11:', f, cohort_config.cohort_name{c}, char(8805));
            if numel(data_qids_ge_11) > 0
                dist_qids_ge_11 = fit_distributions(data_qids_ge_11, [], varargin{:}, 'generate_figures', false);
                classification_data.p_qids_ge_11(f, :, c) = get_fitted_distribution_property(dist_qids_ge_11, 'pdf', data_all);
                classification_properties.dist_qids_ge_11{f, c} = dist_qids_ge_11;
            else
                fprintf(' N/A\n');
            end
        end
    end
end
