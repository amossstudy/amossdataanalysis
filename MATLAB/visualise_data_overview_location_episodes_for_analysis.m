function visualise_data_overview_location_episodes_for_analysis(participant, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 05-May-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location-episode-epochs';
    EPOCHS_PER_GRAPH = 10;
    
    data = load_location_preprocessed(participant, varargin{:});
    episodes_for_analysis = load_episodes_for_analysis(participant);
    
    if ~isempty(data) && ~isempty(data.time) && ~isempty(episodes_for_analysis) && ~isempty(episodes_for_analysis.epochs)
        
        [~, epochs_sorted_idx] = sort([episodes_for_analysis.epochs.start]);
        
        epochs_sorted = episodes_for_analysis.epochs(epochs_sorted_idx);
        
        for i = 1:EPOCHS_PER_GRAPH:length(epochs_sorted)
            close all;
            
            epochs_beginning = epochs_sorted(i).start;
            
            epochs_to_plot = epochs_sorted(i:min(i + EPOCHS_PER_GRAPH - 1, length(epochs_sorted)));
            
            if length(epochs_to_plot) < EPOCHS_PER_GRAPH
                epochs_blank = struct();
                
                for f = fieldnames(epochs_to_plot)'
                    epochs_blank.(f{:}) = [];
                end
                
                epochs_blank = repmat(epochs_blank, 1, EPOCHS_PER_GRAPH - length(epochs_to_plot));
                
                epochs_to_plot = [epochs_to_plot epochs_blank]; %#ok<AGROW>
            end
            
            fprintf(['  Plotting epochs beginning ' datestr(epochs_beginning, 'dd-mmm-yyyy') ': ']);
            tic;
            plot_location_epoch(data.validated, epochs_to_plot, varargin{:})
            toc;
            
            fprintf('  Saving: ');
            tic;
            save_figure([participant '-' datestr(epochs_beginning, 'yyyy-mm-dd')], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
            toc;
         end
    else
        fprintf('  No data loaded\n');
    end
end