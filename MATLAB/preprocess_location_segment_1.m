function preprocess_location_segment_1(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 23-Jul-2015

    paths = get_paths(varargin{:});
    
    variant = get_argument('variant', '', varargin{:});
    combine_variants = get_argument({'combinevariants', 'combine_variants'}, {}, varargin{:});
    
    if isempty(variant) && ~isempty(combine_variants)
        variant = 'combined';

        for i = 1:length(combine_variants)
            this_variant_name = combine_variants{i};

            if isempty(this_variant_name)
                this_variant_name = 'std';
            end

            variant = [variant '_' this_variant_name]; %#ok<AGROW>
        end
    end

    variant_out = '';
    
    if ~isempty(variant)
        variant_out = ['_' variant];
    end

    for file = get_participants(varargin{:})'
        fprintf('Processing %s\n', file.name)

        output_path = [paths.working_path file.name '-'];

        reporting = load_reporting_data(file.name, varargin{:});

        reporting_versions_idx = strcmp([reporting.setting], 'version');
        reporting_versions = cellfun(@str2num, reporting.value(reporting_versions_idx));
        reporting_versions_upgraded = reporting_versions >= 1031;
        
        if sum(reporting_versions_upgraded) > 0
            upgrade_idx = find(diff([0; reporting_versions_upgraded]), 1, 'last');

            reporting_versions_idx = find(reporting_versions_idx);

            upgrade_time = reporting.time(reporting_versions_idx(upgrade_idx));
            
            t_min = floor(upgrade_time) + 1;
        else
            fprintf('  Not upgraded\n');
            
            continue;
        end
        
        data = load_location_preprocessed(file.name, varargin{:}, 'variant', variant);

        if ~isempty(data) && isfield(data, 'normalised') && ~isempty(data.normalised.time)
            fprintf('  Segmenting data: ');
            tic;

            data.normalised.segmented = struct();
            
            valid_time = data.normalised.time >= t_min;
            valid_time = valid_time & [true; (diff(data.normalised.time) > 0)];
            data.normalised.segmented.time = data.normalised.time(valid_time);
            data.normalised.segmented.data = data.normalised.data(valid_time);
            data.normalised.segmented.lat = data.normalised.lat(valid_time);
            data.normalised.segmented.lon = data.normalised.lon(valid_time);
            
            location_data = [data.normalised.segmented.lat data.normalised.segmented.lon]; % for normalised coordinates, this is in km from home
            location_dt = [NaN; diff(data.normalised.segmented.time)];
            
            location_dt(location_dt > (1 / (24 * 4))) = NaN;
            
            location_dxy = [0; sqrt(sum((location_data(2:end, :) - location_data(1:end-1, :)).^2,2))];
            
            location_dxydt = location_dxy ./ (location_dt * 24);
            
            location_dxydt_avg_fwd = NaN(size(location_dxydt));
            
            % States: 1 = Static location; 2 = Travelling
            INITIAL =    [ 0.8  0.2 ];
            TRANSITION = [ 0.9  0.1 ;
                           0.1  0.9 ];
            
            % Validate configurations
            assert(abs(sum(INITIAL) - 1) < 1e-10);
            assert(abs(sum(sum(TRANSITION, 2) - [1 1]')) < 1e-10);
            
            PARTICLES = 50;
            
            T = length(data.normalised.segmented.time);
            
            p_state = zeros(PARTICLES, T);
            p_log_lik = zeros(1, PARTICLES);
            p_rand = rand(PARTICLES, T);
            p_rand_resample = rand(PARTICLES, T);

%             close all;
%             
%             for s1 = 1:2
%                 for s2 = 1:2
%                     figure;
%                     subplot(3, 3, 1:2)
%                     dxy = 0:0.01:20;
%                     p_dxy = get_p_dxy(dxy, s1)';
%                     h = area(dxy, p_dxy);
%                     h(1).FaceColor = [0.5 1 0.5];
%                     h(2).FaceColor = [0.5 0.5 1];
%                     set(gca, 'ylim', [0 1]);
%                     xlabel('${\Delta}xy$', 'interpreter', 'latex')
%                     title(sprintf('Previous State: %i; Current State: %i', s1, s2));
% 
%                     subplot(3, 3, 3)
%                     plot(dxy, p_dxy(:, s2), 'linewidth', 2);
%                     set(gca, 'ylim', [0 1]);
%                     
%                     subplot(3, 3, 4:5)
%                     dxydt = 0:0.1:200;
%                     p_dxydt = get_p_dxydt(dxydt, s1)';
%                     h = area(dxydt, p_dxydt);
%                     h(1).FaceColor = [0.5 1 0.5];
%                     h(2).FaceColor = [0.5 0.5 1];
%                     set(gca, 'ylim', [0 1]);
%                     xlabel('$\displaystyle{{\Delta}xy}/{{\Delta}t}$', 'interpreter', 'latex')
%                     
%                     subplot(3, 3, 6)
%                     plot(dxydt, p_dxydt(:, s2), 'linewidth', 2);
%                     set(gca, 'ylim', [0 1]);
%                     
%                     subplot(3, 3, 7:8)
%                     dxydt_avg_fwd = 0:0.1:200;
%                     p_dxydt_avg_fwd = get_p_dxydt_avg_fwd(dxydt_avg_fwd, s1)';
%                     h = area(dxydt_avg_fwd, p_dxydt_avg_fwd);
%                     h(1).FaceColor = [0.5 1 0.5];
%                     h(2).FaceColor = [0.5 0.5 1];
%                     set(gca, 'ylim', [0 1]);
%                     xlabel('$\displaystyle\overline{{{\Delta}xy}/{{\Delta}t}}$', 'interpreter', 'latex')
%                     
%                     subplot(3, 3, 9)
%                     plot(dxydt_avg_fwd, p_dxydt_avg_fwd(:, s2), 'linewidth', 2);
%                     set(gca, 'ylim', [0 1]);
%                     
%                     save_figure(sprintf('output_dist_%i_%i', s1, s2), 'output_sub_dir', 'location-segmented-1/dist', varargin{:});
%                 end
%             end
            
            t2 = 1;
            
            last_status_text = '';
            
            for t = 1:T
                if (t == 1) || (rem(t, 100) == 0)
                    if ~isempty(last_status_text)
                        fprintf(repmat('\b', 1, length(last_status_text) - 1))
                    end
                    last_status_text = sprintf('%.2f%%%%', (t / T) * 100);
                    fprintf(last_status_text);
                end
                if isnan(location_dt(t))
                    p_state(:, t) = draw_state(INITIAL, p_rand(:, t));
                    p_weights = INITIAL(p_state(:, t));
                else
                    p_state(:, t) = draw_state(TRANSITION(p_state(:, t - 1), :), p_rand(:, t));
                    p_weights = TRANSITION(sub2ind(size(TRANSITION), p_state(:, t - 1), p_state(:, t)))';
                end

                if ~isnan(location_dt(t))
                    if t2 < t
                        t2 = t;
                    end
                    while t2 <= length(data.normalised.segmented.time) && ...
                        ~isnan(location_dxydt(t2)) && ...
                        (data.normalised.segmented.time(t2) - data.normalised.segmented.time(t)) < (1 / (24 * 4))
                        t2 = t2 + 1;
                    end

                    location_dxydt_avg_fwd(t) = mean(location_dxydt(t:(t2 - 1)));

                    [state_transitions, ~, state_transition_idx] = unique([p_state(:, t - 1), p_state(:, t)], 'rows');

                    for s_i = 1:size(state_transitions, 1)
                        p_dxy = get_p_dxy(location_dxy(t), state_transitions(s_i, 1));
                        p_dxydt = get_p_dxydt(location_dxydt(t), state_transitions(s_i, 1));
                        p_dxydt_avg_fwd = get_p_dxydt_avg_fwd(location_dxydt_avg_fwd(t), state_transitions(s_i, 1));

                        p_weights(state_transition_idx == s_i) = p_weights(state_transition_idx == s_i) * p_dxy(state_transitions(s_i, 2)) * p_dxydt(state_transitions(s_i, 2)) * p_dxydt_avg_fwd(state_transitions(s_i, 2));
                    end
                end
                
                p_log_lik = p_log_lik + log(p_weights);
                
                p_weights = p_weights / sum(p_weights);
                
                p_resample = draw_state(p_weights, p_rand_resample(:, t));
                
                p_state = p_state(p_resample, :);
                p_log_lik = p_log_lik(p_resample);
            end
            
            [~, p_max_log_lik_i] = max(p_log_lik);
            
            data.normalised.segmented.state = p_state(p_max_log_lik_i, :);
            
%             [a1, b1] = stairs(1:T, double(data.normalised.segmented.state(1:T) == 1));
%             [a2, b2] = stairs(1:T, double(data.normalised.segmented.state(1:T) == 2));
%             [a3, b3] = stairs(1:T, double(data.normalised.segmented.state(1:T) == 3));
%             
%             figure;
%             subplot(4, 1, 1)
%             hold on;
%             h = plot(1:T, data.normalised.segmented.data(1:T), 'k', 'linewidth', 2);
%             y_lim = get(gca, 'ylim');
%             patch([a1(1); a1; a1(end)], double([0; b1; 0]) * y_lim(2), [0.5 1 0.5]);
%             patch([a2(1); a2; a2(end)], double([0; b2; 0]) * y_lim(2), [0.5 0.5 1]);
%             patch([a3(1); a3; a3(end)], double([0; b3; 0]) * y_lim(2), [1 0.5 0.5]);
%             uistack(h, 'top')
%             set(gca, 'xticklabel', []);
%             p = get(gca, 'Position');
%             p_diff = p(4) * 0.16;
%             p(4) = p(4) + p_diff;
%             p(2) = p(2) - ((4 - 1) * (p_diff / (4 - 1)));
%             set(gca, 'Position', p);
%             set(gca, 'Layer','top')
%             hold off;
%             
% %             subplot(4, 1, 2)
% %             hold on;
% %             h = plot(1:T, location_dt(1:T), 'k', 'linewidth', 2);
% %             y_lim = get(gca, 'ylim');
% %             patch([a1(1); a1; a1(end)], double([0; b1; 0]) * y_lim(2), [0.5 1 0.5]);
% %             patch([a2(1); a2; a2(end)], double([0; b2; 0]) * y_lim(2), [0.5 0.5 1]);
% %             patch([a3(1); a3; a3(end)], double([0; b3; 0]) * y_lim(2), [1 0.5 0.5]);
% %             uistack(h, 'top')
% %             set(gca, 'xticklabel', []);
% %             p = get(gca, 'Position');
% %             p_diff = p(4) * 0.16;
% %             p(4) = p(4) + p_diff;
% %             p(2) = p(2) - ((4 - 2) * (p_diff / (4 - 1)));
% %             set(gca, 'Position', p);
% %             set(gca, 'Layer','top')
% %             ylabel('${\Delta}t$', 'interpreter', 'latex');
% %             hold off;
%             
%             subplot(4, 1, 2)
%             hold on;
%             h = plot(1:T, location_dxy(1:T), 'k', 'linewidth', 2);
%             y_lim = get(gca, 'ylim');
%             patch([a1(1); a1; a1(end)], double([0; b1; 0]) * y_lim(2), [0.5 1 0.5]);
%             patch([a2(1); a2; a2(end)], double([0; b2; 0]) * y_lim(2), [0.5 0.5 1]);
%             patch([a3(1); a3; a3(end)], double([0; b3; 0]) * y_lim(2), [1 0.5 0.5]);
%             uistack(h, 'top')
%             set(gca, 'xticklabel', []);
%             p = get(gca, 'Position');
%             p_diff = p(4) * 0.16;
%             p(4) = p(4) + p_diff;
%             p(2) = p(2) - ((4 - 3) * (p_diff / (4 - 1)));
%             set(gca, 'Position', p);
%             set(gca, 'Layer','top')
%             ylabel('${\Delta}xy$', 'interpreter', 'latex');
%             hold off;
%             
%             subplot(4, 1, 3)
%             hold on;
%             h = plot(1:T, location_dxydt(1:T), 'k', 'linewidth', 2);
%             y_lim = get(gca, 'ylim');
%             patch([a1(1); a1; a1(end)], double([0; b1; 0]) * y_lim(2), [0.5 1 0.5]);
%             patch([a2(1); a2; a2(end)], double([0; b2; 0]) * y_lim(2), [0.5 0.5 1]);
%             patch([a3(1); a3; a3(end)], double([0; b3; 0]) * y_lim(2), [1 0.5 0.5]);
%             uistack(h, 'top')
%             set(gca, 'xticklabel', []);
%             p = get(gca, 'Position');
%             p_diff = p(4) * 0.16;
%             p(4) = p(4) + p_diff;
%             p(2) = p(2) - ((4 - 4) * (p_diff / (4 - 1)));
%             set(gca, 'Position', p);
%             set(gca, 'Layer','top')
%             ylabel('$\displaystyle\frac{{\Delta}xy}{{\Delta}t}$', 'interpreter', 'latex');
%             hold off;
%             
%             subplot(4, 1, 4)
%             hold on;
%             h = plot(1:T, location_dxydt_avg_fwd(1:T), 'k', 'linewidth', 2);
%             y_lim = get(gca, 'ylim');
%             patch([a1(1); a1; a1(end)], double([0; b1; 0]) * y_lim(2), [0.5 1 0.5]);
%             patch([a2(1); a2; a2(end)], double([0; b2; 0]) * y_lim(2), [0.5 0.5 1]);
%             patch([a3(1); a3; a3(end)], double([0; b3; 0]) * y_lim(2), [1 0.5 0.5]);
%             uistack(h, 'top')
%             p = get(gca, 'Position');
%             p_diff = p(4) * 0.16;
%             p(4) = p(4) + p_diff;
%             p(2) = p(2) - ((4 - 4) * (p_diff / (4 - 1)));
%             set(gca, 'Position', p);
%             set(gca, 'Layer','top')
%             ylabel('$\displaystyle\overline{\frac{{\Delta}xy}{{\Delta}t}}$', 'interpreter', 'latex');
%             hold off;
%             
%             save_figure('result', 'output_sub_dir', 'location-segmented-1/dist', varargin{:});
%             
% %             is_valid = (data.normalised.segmented.state == 1);
% %             
% %             if sum(~is_valid) > 0
% %                 data.normalised.segmented.time = data.normalised.segmented.time(is_valid);
% %                 data.normalised.segmented.data = data.normalised.segmented.data(is_valid);
% %                 data.normalised.segmented.lat = data.normalised.segmented.lat(is_valid);
% %                 data.normalised.segmented.lon = data.normalised.segmented.lon(is_valid);
% %             end
            
            save([output_path 'location' variant_out '-preprocessed' ], 'data');

            if ~isempty(last_status_text)
                fprintf(repmat('\b', 1, length(last_status_text) - 1))
            end

            toc;
        else
            fprintf('  No data loaded\n');
        end
    end
    
    function s = draw_state(p_dist, p_rand)
        c_dist = cumsum(p_dist, 2);
        s = ones(size(p_rand));
        for d = 1:(size(c_dist, 2) - 1)
            s = s + (p_rand > c_dist(:, d));
        end
    end
  
    function p_dxy = get_p_dxy(dxy, state_previous)
        p_dxy = zeros(2, length(dxy));
        
        if (state_previous == 1)
            p_dxy(1, :) = exppdf(dxy, 0.2) * 0.2;
            p_dxy(2, :) = 1 - exppdf(dxy, 0.2) * 0.2;
        else
            p_dxy(1, :) = exppdf(dxy, 0.2) * 0.05;
            p_dxy(2, :) = 1 - exppdf(dxy, 0.2) * 0.05;
        end

        p_dxy = p_dxy ./ repmat(sum(p_dxy), 2, 1);
    end
  
    function p_dxydt = get_p_dxydt(dxydt, state_previous)
        p_dxydt = zeros(2, length(dxydt));
        
        if (state_previous == 1)
            p_dxydt(1, :) = exppdf(dxydt, 15) * 15;
            p_dxydt(2, :) = gampdf(dxydt, 2, 20) * 20 * 2;
        else
            p_dxydt(1, :) = exppdf(dxydt, 15) * 7;
            p_dxydt(2, :) = (gampdf(dxydt, 2, 20) * 20 * 2) + (exppdf(dxydt, 15) * 5);
        end

        p_dxydt = p_dxydt ./ repmat(sum(p_dxydt), 2, 1);
    end
  
    function p_dxydt_avg_fwd = get_p_dxydt_avg_fwd(dxydt_avg_fwd, state_previous)
        p_dxydt_avg_fwd = zeros(2, length(dxydt_avg_fwd));
        
        if (state_previous == 1)
            p_dxydt_avg_fwd(1, :) = exppdf(dxydt_avg_fwd, 15) * 15;
            p_dxydt_avg_fwd(2, :) = (gampdf(dxydt_avg_fwd, 2, 20) * 20 * 2) + (exppdf(dxydt_avg_fwd, 15) * 10);
        else
            p_dxydt_avg_fwd(1, :) = exppdf(dxydt_avg_fwd, 15) * 15;
            p_dxydt_avg_fwd(2, :) = (gampdf(dxydt_avg_fwd, 2, 20) * 20 * 2) + (exppdf(dxydt_avg_fwd, 15) * 5);
        end
        
        p_dxydt_avg_fwd = p_dxydt_avg_fwd ./ repmat(sum(p_dxydt_avg_fwd), 2, 1);
    end
end
