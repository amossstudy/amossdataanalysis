function visualise_data_overview_true_colours_episodes(participant, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 27-Mar-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-overview/true-colours';
    
    data = load_true_colours_data(participant, varargin{:});
    episodes = load_episodes(participant, varargin{:});
    
    questionaires = get_configuration('questionaires');
    questionaire_names = fieldnames(questionaires);
    
    episodes_types = get_configuration('episode_types');
    
    t_lim = get_argument({'tlim', 't_lim'}, [], varargin{:});
    
    t_min = inf;
    t_max = 0;
    
    subplots = numel(questionaire_names) + 2;
    
    h_plot = zeros(subplots, 1);
    q_min = zeros(subplots, 1);
    q_max = ones(subplots, 1);
    
    if ~isempty(data) && isstruct(data) && ~isempty(fields(data))
        episodes_time = [episodes.time; (episodes.time(end) + 1)];
        
        for q = 1:subplots
            h_plot(q) = subplot(subplots, 1, q);
            box on;
            
            if q <= numel(questionaire_names)
                this_questionaire = questionaires.(questionaire_names{q});

                q_min(q) = this_questionaire.min;
                q_max(q) = this_questionaire.max;

                if isfield(data, questionaire_names{q}) && isfield(data.(questionaire_names{q}), 'data')
                    this_questionnaire_data = data.(questionaire_names{q}).data;
                    this_questionnaire_time = data.(questionaire_names{q}).time;
                    this_questionnaire_time = this_questionnaire_time(this_questionnaire_data > -1);
                    this_questionnaire_data = this_questionnaire_data(this_questionnaire_data > -1);
                    if ~isempty(this_questionnaire_time) && ~isempty(this_questionnaire_data)
                        this_questionnaire_time_diff = [1; diff(this_questionnaire_time)];
                        this_questionnaire_time = this_questionnaire_time(this_questionnaire_time_diff > (1/24));
                        this_questionnaire_data = this_questionnaire_data(this_questionnaire_time_diff > (1/24));

                        t_min = min(t_min, floor(min(this_questionnaire_time)));
                        t_max = max(t_max, ceil(max(this_questionnaire_time)));
                    end

                    hold on;
                    
                    threshold_line_properties = {'--' 'Color', ones(1, 3) * 0.35};
                    
                    plot([t_min, t_max], [q_min(q), q_min(q)], '-', threshold_line_properties{2:end});
                    plot([t_min, t_max], [q_max(q), q_max(q)], '-', threshold_line_properties{2:end});
                    
                    if strcmp(questionaire_names{q}, 'ALTMAN') == 1
                        plot([t_min, t_max], [6, 6], threshold_line_properties{:});
                        plot([t_min, t_max], [10, 10], threshold_line_properties{:});
                    elseif strcmp(questionaire_names{q}, 'QIDS') == 1
                        plot([t_min, t_max], [6, 6], ':', threshold_line_properties{2:end});
                        plot([t_min, t_max], [11, 11], threshold_line_properties{:});
                        plot([t_min, t_max], [16, 16], threshold_line_properties{:});
                        plot([t_min, t_max], [21, 21], threshold_line_properties{:});
                    end
                    
                    stairs(this_questionnaire_time, this_questionnaire_data, 'Color', this_questionaire.line_colour);
                    scatter(this_questionnaire_time, this_questionnaire_data, 'x', 'MarkerEdgeColor', this_questionaire.line_colour);
                    hold off;
                end

                ylabel(this_questionaire.display_name);
            elseif q == (numel(questionaire_names) + 1)
                severity = struct();
                
                severity.manic = struct();
                severity.manic.time = episodes_time;
                severity.manic.severity = [episodes.manic_severity; episodes.manic_severity(end)];
                
                severity.depressed = struct();
                severity.depressed.time = episodes_time;
                severity.depressed.severity = [episodes.depressed_severity; episodes.depressed_severity(end)];
                
                for mood_cell = {'manic', 'depressed'}
                    severity_changes = (diff(severity.(mood_cell{:}).severity) ~= 0) & (~isnan(diff(severity.(mood_cell{:}).severity)));

                    if sum(severity_changes) > 0
                        severity_new = NaN(size(severity.(mood_cell{:}).severity) + [(sum(severity_changes) * 2) 0]);
                        time_new = NaN(size(severity.(mood_cell{:}).time) + [(sum(severity_changes) * 2) 0]);
                        change_last = 1;
                        i = 0;
                        for change = find(severity_changes)'
                            severity_new((change_last:change) + i) = severity.(mood_cell{:}).severity(change_last:change);
                            severity_new(change + i + 1) = severity_new(change + i);
                            time_new((change_last:change) + i) = severity.(mood_cell{:}).time(change_last:change);
                            time_new(change + i + 1) = time_new(change + i) + 1;
                            i = i + 2;
                            change_last = change + 1;
                        end
                        severity_new((change_last + i):end) = severity.(mood_cell{:}).severity(change_last:end);
                        time_new((change_last + i):end) = severity.(mood_cell{:}).time(change_last:end);
                        severity.(mood_cell{:}).severity = severity_new;
                        severity.(mood_cell{:}).time = time_new;
                    end
                end
                
                hold on;

                stairs(severity.manic.time, severity.manic.severity, 'Color', episodes_types.manic.line_colour, 'LineWidth', 1.5);
                stairs(severity.depressed.time, -severity.depressed.severity, 'Color', episodes_types.depressed.line_colour, 'LineWidth', 1.5);

                hold off;
                
                q_min(q) = -3;
                q_max(q) = 3;

                ylabel('Severity');
                set(gca, 'ytick', -3:3:3);
            elseif q == (numel(questionaire_names) + 2)
                episodes_manic = [episodes.manic; episodes.manic(end)];
                episodes_depressed = [episodes.depressed; episodes.depressed(end)];
                
                hold on;
                
                stairs(episodes_time, episodes_manic, 'Color', episodes_types.manic.line_colour, 'LineWidth', 1.5);
                stairs(episodes_time, -episodes_depressed, 'Color', episodes_types.depressed.line_colour, 'LineWidth', 1.5);
                
                hold off;
                
                q_min(q) = -1;
                q_max(q) = 1;
                
                ylabel('Episode');
                set(gca, 'yticklabel', {'D', 'E', 'M'});
                
                subplot(h_plot(1));
                
                hold on;
                
                episode_changes = diff([0; episodes.manic]);
                episode_changes_idx = find(abs(episode_changes) > 0);
                
                i = 1;
                
                episode_min = t_min;
                
                while i <= length(episode_changes_idx)
                    if episode_changes(episode_changes_idx(i)) > 0
                        episode_min = episodes.time(episode_changes_idx(i));
                        
                        if i == length(episode_changes_idx)
                            episode_max = t_max;
                        end
                    else
                        episode_max = episodes.time(episode_changes_idx(i));
                    end
                    if (episode_changes(episode_changes_idx(i)) < 0) || (i == length(episode_changes_idx))
                        uistack(patch([episode_min episode_min episode_max episode_max], [q_min(1) q_max(1) q_max(1) q_min(1)], episodes_types.manic.episode_colour, 'edgecolor', 'none'), 'bottom');
                    end    
                    
                    i = i + 1;
                end
                
                hold off;
                
                subplot(h_plot(3));
                
                hold on;
                
                episode_changes = diff([0; episodes.depressed]);
                episode_changes_idx = find(abs(episode_changes) > 0);
                
                i = 1;
                
                episode_min = t_min;
                
                while i <= length(episode_changes_idx)
                    if episode_changes(episode_changes_idx(i)) > 0
                        episode_min = episodes.time(episode_changes_idx(i));
                        
                        if i == length(episode_changes_idx)
                            episode_max = t_max;
                        end
                    else
                        episode_max = episodes.time(episode_changes_idx(i));
                    end
                    if (episode_changes(episode_changes_idx(i)) < 0) || (i == length(episode_changes_idx))
                        uistack(patch([episode_min episode_min episode_max episode_max], [q_min(3) q_max(3) q_max(3) q_min(3)], episodes_types.depressed.episode_colour, 'edgecolor', 'none'), 'bottom');
                    end    
                    
                    i = i + 1;
                end
                
                hold off;
                
                subplot(h_plot(q));
            end
            
            q_range = q_max(q) - q_min(q);
            
            q_min(q) = q_min(q) - q_range * 0.07;
            q_max(q) = q_max(q) + q_range * 0.07;
            
            ylim([q_min(q) q_max(q)]);
            
            if q == 1;
                title('True Colours');
            end
            
            if ~isempty(t_lim)
                t_min = t_lim(1);
                t_max = t_lim(2);
            end
            
            if q < subplots
                set(gca, 'xticklabel', []);
            else
                [xticks, xticks_str] = get_xticks(t_min, t_max, varargin{:});
                
                set(gca, 'xticklabel', xticks_str);
                
                for h = h_plot'
                    set(h, 'xtick', xticks);
                    if ~isempty(xticks)
                        xlim(h, [t_min, t_max]);
                    end
                end
            end
        end
        
        date_entered = get_participant_property(participant, 'date_entered', varargin{:});
        date_left = get_participant_property(participant, 'date_left', varargin{:});

        for q = 1:subplots
            subplot(h_plot(q));
            
            if ~isempty(date_entered)
                hold on;
                plot([datenum(date_entered), datenum(date_entered)], [q_min(q), q_max(q)], 'Color', [0 0 0], 'LineWidth', 1.5);
                if ~isempty(date_left)
                    uistack(patch([datenum(date_entered) datenum(date_entered) datenum(date_left) datenum(date_left)], [q_min(q) q_max(q) q_max(q) q_min(q)], ones(1, 3) * 0.9, 'edgecolor', 'none'), 'bottom');
                else
                    uistack(patch([datenum(date_entered) datenum(date_entered) t_max t_max], [q_min(q) q_max(q) q_max(q) q_min(q)], ones(1, 3) * 0.9, 'edgecolor', 'none'), 'bottom');
                end
                hold off;
            end
            
            if ~isempty(date_left)
                hold on;
                plot([datenum(date_left), datenum(date_left)], [q_min(q), q_max(q)], 'Color', [0 0 0], 'LineWidth', 1.5);
                hold off;
            end

            p = get(gca, 'Position');
            p_diff = p(4) * 0.2;
            p(4) = p(4) + p_diff;
            p(2) = p(2) - ((subplots - q) * (p_diff / (subplots - 1)));
            set(gca, 'Position', p);
            
            set(gca, 'Layer','top')
        end
        
        save_figure([participant '-true-colours-episodes'], 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    else
        fprintf('  No data loaded\n');
    end
end