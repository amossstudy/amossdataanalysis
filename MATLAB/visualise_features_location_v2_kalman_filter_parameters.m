function visualise_features_location_v2_kalman_filter_parameters(varargin)
% Copyright (c) 2017, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 07-Mar-2017

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/stats/kalman_filter_parameters';
    
    params = get_configuration('features_location_v2', varargin{:});
    
    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    
    label_opts = {};
    if latex_labels
        label_opts = {'Interpreter', 'latex'};
    end
    
    kalman_window_backwards = get_argument({'kalman_window_backwards', 'kalmanwindowbackwards'}, 3, varargin{:});
    kalman_window_forwards = get_argument({'kalman_window_forwards', 'kalmanwindowforwards'}, 3, varargin{:});
    
    [data_no_window_raw, prop_no_window] = load_features_location_v2(varargin{:}, 'average_data_backwards', 0, 'average_data_forwards', 0, 'kalman_filter', false);
    [data_window_raw, ~]                 = load_features_location_v2(varargin{:}, 'average_data_backwards', kalman_window_backwards, 'average_data_forwards', kalman_window_forwards, 'kalman_filter', false);
    
    FEATURES = size(data_no_window_raw, 1);
    
    diff_weeks_max = get_argument({'diff_weeks_max', 'diffweeksmax'}, 10, varargin{:});
    diff_weeks_hist = get_argument({'diff_weeks_hist', 'diffweekshist'}, 1:diff_weeks_max, varargin{:});
    
    features_to_show_display_idx = 1:FEATURES;
    features_to_show_display_idx = get_argument({'features_to_show', 'featurestoshow', 'features_to_show_display_idx', 'featurestoshowdisplayidx'}, features_to_show_display_idx, varargin{:});
    
    features_to_show = params.feature_order(params.feature_display_order(features_to_show_display_idx));
    
    var_diff_weeks = NaN(FEATURES, diff_weeks_max);
    
    bins = 75;
    
    for f = features_to_show
        this_feature_name = params.feature_name{f};
        this_feature_name_file = strrep(lower(this_feature_name), ' ', '_');

        this_f_data_diff = data_no_window_raw(f, :) - data_window_raw(f, :);

        figure;
        
        histogram(this_f_data_diff);
        
        this_x_lim = get(gca, 'xlim');
        this_x_tick = get(gca, 'xtick');
        
        max_x_lim_abs = max(abs(this_x_lim));
        max_x_lim_axis = max_x_lim_abs + (3 * ((2*max_x_lim_abs) / (bins - 1)));
        
        histogram(this_f_data_diff, linspace(-max_x_lim_abs, max_x_lim_abs, bins));
        
        set(gca, 'xlim', [-max_x_lim_axis max_x_lim_axis]);
        set(gca, 'xtick', this_x_tick);
        
        if latex_ticks
            set(gca, 'TickLabelInterpreter', 'latex')
        end
        
        p = get(gcf, 'PaperPosition');
        p(4) = p(4) / 2;
        set(gcf, 'PaperPosition', p);
        
        save_figure(sprintf('R_%.2i_%s', f, this_feature_name_file), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        
        close
        
        for w = diff_weeks_max:-1:1
            
            this_week_sequential_diff = NaN(size(data_no_window_raw, 2), 1);
            
            for i = (w + 1):size(data_no_window_raw, 2)
                if strcmp(prop_no_window.participant{i}, prop_no_window.participant{i - w})
                    if (prop_no_window.week_start(i) - prop_no_window.week_start(i - w) == (7 * w))
                        this_week_sequential_diff(i) = data_window_raw(f, i) - data_window_raw(f, i - w);
                    end
                end
            end
            
            var_diff_weeks(f, w) = nanvar(this_week_sequential_diff);
            
            if sum(diff_weeks_hist == w) > 0
                figure;

                if w == diff_weeks_hist(end)
                    histogram(this_week_sequential_diff);

                    this_x_lim = get(gca, 'xlim');
                    max_x_lim_abs = max(abs(this_x_lim));
                    max_x_lim_axis = max_x_lim_abs + (3 * ((2*max_x_lim_abs) / (bins - 1)));
                end

                histogram(this_week_sequential_diff, linspace(-max_x_lim_abs, max_x_lim_abs, bins));

                if latex_ticks
                    set(gca, 'TickLabelInterpreter', 'latex')
                end

                if w ~= diff_weeks_hist(end)
                    set(gca, 'xticklabel', ' ')
                end

                hold on;
                h = histogram(NaN, linspace(-max_x_lim_abs, max_x_lim_abs, 100));
                hold off;

                set(gca, 'xlim', [-max_x_lim_abs max_x_lim_abs]);
                this_x_tick = get(gca, 'xtick');
                set(gca, 'xlim', [-max_x_lim_axis max_x_lim_axis]);
                set(gca, 'xtick', this_x_tick);

                set(h, 'FaceAlpha', 0);
                set(h, 'EdgeColor', 'none');

                str = ['$\delta=' num2str(w) '$ week'];
                if w > 1
                    str = [str 's']; %#ok<AGROW>
                end

                h = legend(h, str);
                set(h, 'Box', 'off', label_opts{:});

                p = get(gcf, 'PaperPosition');
                p(4) = p(4) / 3;
                set(gcf, 'PaperPosition', p);

                save_figure(sprintf('Q_%.2i_%s_%.2i_weeks', f, this_feature_name_file, w), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

                close
            end
        end
        
        var_diff_weeks(f, :) = var_diff_weeks(f, :) / var_diff_weeks(f, 1);
        
        figure
        plot(1:diff_weeks_max, var_diff_weeks(f, :), '-+', 'linewidth', 2, 'Color', [1 1 1] * 0.5, 'MarkerSize', 10)
        
        set(gca, 'xlim', [0.5 diff_weeks_max + 0.5]);
        xlabel('$\delta$', label_opts{:});
        ylabel('$\sigma^2_{\delta}$ (\% of $\sigma^2_1)$', label_opts{:});
        if latex_ticks
            set(gca, 'TickLabelInterpreter', 'latex')
        end
        p = get(gcf, 'PaperPosition');
        p(4) = p(4) / 2;
        set(gcf, 'PaperPosition', p);
        
        save_figure(sprintf('Q_%.2i_%s_00_overview', f, this_feature_name_file), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    end
    figure
    plot(1:diff_weeks_max, var_diff_weeks, 'linewidth', 1, 'Color', [1 1 1] * 0.5)
    set(gca, 'xlim', [0.5 diff_weeks_max + 0.5]);
    xlabel('$\delta$', label_opts{:});
    ylabel('$\sigma^2_{\delta}$ (\% of $\sigma^2_1)$', label_opts{:});
    if latex_ticks
        set(gca, 'TickLabelInterpreter', 'latex')
    end
    p = get(gcf, 'PaperPosition');
    p(4) = p(4) / 3;
    set(gcf, 'PaperPosition', p);
    
    save_figure('Q_00_overview', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
end
