function visualise_data_overview_all_true_colours_combined(varargin)
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 28-Dec-2014

    close all;
    
    OUTPUT_SUB_DIR = 'data-quantity';
    
    files = get_participants(varargin{:});
    
    data_dates = datenum([2014, 1, 1]):datenum([2015, 3, 1]);
    
    questionaires = {'ALTMAN', 'EQ_5D', 'GAD_7', 'QIDS'};
    
    fig_idx = 1;
    
    figure;
    
    for this_questionaire_cell = questionaires
        this_questionaire = this_questionaire_cell{1};
        this_questionaire_str = strrep(this_questionaire, '_', '\_');
        
        data_matrix = NaN(length(files), length(data_dates));
        data_matrix_class = cell(3, 1);
        for i = 1:3
          data_matrix_class{i} = struct('data', NaN(length(files), length(data_dates)), 'j', 1);
        end

        j = 1;

        for file = files'
            fprintf('Loading %s\n', file.name)

            class = get_participant_property(file.name, 'classification', varargin{:});

            if ~isempty(class)
                data_tc = load_true_colours_preprocessed(file.name);

                if ~isempty(data_tc) && isfield(data_tc, this_questionaire) && isfield(data_tc.(this_questionaire), 'regularised') && isfield(data_tc.(this_questionaire), 'weekly')
                    A = ismember(data_dates, data_tc.(this_questionaire).regularised.time);
                    B = ismember(data_tc.(this_questionaire).regularised.time, data_dates);

                    data_matrix(j, A) = data_tc.(this_questionaire).regularised.data(B);
                end
                j = j + 1;
            end
        end

        t_min = data_dates(1);
        t_max = data_dates(end);

        [xticks, xticks_str] = get_xticks(t_min, t_max, 'custom_format', 'mmm', 'interval_multiplier', 4, varargin{:});

        data_matrix(isnan(data_matrix)) = -1;

        data_range = 1:(j - 1);
        data_order = data_range;

        if strcmp(this_questionaire, 'ALTMAN') == 1
            q_max = 20;
        elseif strcmp(this_questionaire, 'EQ_5D') == 1
            q_max = 100;
        elseif strcmp(this_questionaire, 'GAD_7') == 1
            q_max = 21;
        elseif strcmp(this_questionaire, 'QIDS') == 1
            q_max = 27;
        end
        
        clr = hot(ceil(q_max * 1.1));
        
        if strcmp(this_questionaire, 'EQ_5D') == 1
            clr(1, :) = 1;
            clr = clr(1:(q_max + 1), :);
        else
            clr = flipud(clr(1:(q_max + 1), :));
            clr(1, :) = 1;
        end

        subplot(1, numel(questionaires), fig_idx);
        
        imagesc(data_dates, data_range, data_matrix(data_order, :));
        title(this_questionaire_str);
        colormap(gca, clr);
        set(gca, 'xtick', xticks);
        set(gca, 'xticklabel', xticks_str);
        set(gca, 'ytick', []);
        set(gca, 'clim', [-1.5 (q_max + 0.5)]);
        
        p = get(gca, 'Position');
        p_diff = p(3) * 0.15;
        p(3) = p(3) + p_diff;
         if fig_idx == numel(questionaires)
             p(1) = p(1) - p_diff;
         elseif fig_idx > 1 
             p(1) = p(1) - ((fig_idx - 1) * (p_diff / (numel(questionaires) - 1)));
         end
        set(gca, 'Position', p);
        
        fig_idx = fig_idx + 1;
    end
    
    save_figure('true_colours_combined_all', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
end
