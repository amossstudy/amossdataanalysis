function visualise_data_stats_mood_zoom(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Mar-2015

    close all;
    
    OUTPUT_SUB_DIR = 'data-stats';
    
    files = get_participants(varargin{:});
    
    data_dates = [2014, 1, 1; 2015, 3, 1];
    
    data_class = cell(3, 1);
    for i = 1:3
      data_class{i} = struct('j', 1);
    end
    
    data_moods = struct;

    for file = files'
        fprintf('Loading %s\n', file.name)

        class = get_participant_property(file.name, 'classification', varargin{:});

        if ~isempty(class)
            data_mz = load_mood_zoom_data(file.name);

            if ~isempty(data_mz)
                for mood = fieldnames(data_mz)'
                    if strcmp(mood, 'time') == 0
                        data_moods.(mood{:}) = true;
                        if ~isfield(data_class{class + 1}, mood)
                            data_class{class + 1}.(mood{:}) = NaN(length(files), (datenum(data_dates(2,:)) - datenum(data_dates(1,:))) * 10);
                        end
                        
                        this_mood_data = data_mz.(mood{:});

                        if ~isempty(this_mood_data)
                            data_range = 1:length(this_mood_data);

                            data_class{class + 1}.(mood{:})(data_class{class + 1}.j, data_range) = this_mood_data;
                        end
                    end
                end
                data_class{class + 1}.j = data_class{class + 1}.j + 1;
            end
        end
    end

    data_moods_list = fieldnames(data_moods);
    moods = numel(data_moods_list);
    
    q_max = 7;

    figure;

    d_hist = 1;

    for i = 1:3
        j_range_class = 1:(data_class{i}.j - 1);
        
        for m = 1:moods
            mood = data_moods_list{m};
            mood_str = mood;
            mood_str(1) = upper(mood_str(1));
            
            class_data = data_class{i}.(mood)(j_range_class, :);

            this_data = class_data(:);
            hist_edges = (-d_hist / 2):d_hist:((ceil(q_max / 2) * 2) + (d_hist / 2));

            subplot(3, moods, ((i - 1) * moods) + m);

            histogram(this_data, hist_edges);

            hold on;
            axis manual;

            fit_distributions(this_data, hist_edges, varargin{:}, 'show_legend', false, 'offset', d_hist / 2);

            if i < 3
              set(gca, 'xticklabel', []);
            end
            set(gca, 'ytick', []);
            set(gca, 'xlim', hist_edges([1 end]));
            if i == 1
                title(mood_str);
            elseif i == 3
                xlabel('Value');
            end
            if m == 1
                if i == 1
                    ylabel('HC');
                elseif i == 2
                    ylabel('BD');
                elseif i == 3
                    ylabel('BPD');
                end
            end
            p = get(gca, 'Position');
            p_diff = p(4) * 0.2;
            p(4) = p(4) + p_diff;
            if i == 1
                p(2) = p(2) - p_diff;
            elseif i < 3
                p(2) = p(2) - (p_diff / 2);
            end
            p_diff = p(3) * 0.15;
            p(3) = p(3) + p_diff;
            if m == moods
                p(1) = p(1) - p_diff;
            elseif m > 1
                p(1) = p(1) - ((m - 1) * (p_diff / (moods - 1)));
            end
            set(gca, 'Position', p);
        end
    end

    p = get(gcf, 'Position');
    p(3) = (p(3) / 5) * (moods + 1);
    set(gcf, 'Position', p);

    p = get(gcf, 'PaperPosition');
    p(3) = (p(3) / 5) * (moods + 1);
    set(gcf, 'PaperPosition', p);
    
    save_figure('mood_zoom_hist', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

    figure;

    d_hist = 0.5;

    for i = 1:3
        j_range_class = 1:(data_class{i}.j - 1);
        
        for m = 1:moods
            mood = data_moods_list{m};
            mood_str = mood;
            mood_str(1) = upper(mood_str(1));
            
            class_data = data_class{i}.(mood)(j_range_class, :);

            this_data = nanmean(class_data, 2);
            hist_edges = 0:d_hist:(ceil(q_max / 2) * 2);

            subplot(3, moods, ((i - 1) * moods) + m);

            histogram(this_data, hist_edges);

            hold on;
            axis manual;

            fit_distributions(this_data, hist_edges, varargin{:}, 'show_legend', false);

            if i < 3
              set(gca, 'xticklabel', []);
            end
            set(gca, 'ytick', []);
            set(gca, 'xlim', hist_edges([1 end]));
            if i == 1
                title(mood_str);
            elseif i == 3
                xlabel('Mean');
            end
            if m == 1
                if i == 1
                    ylabel('HC');
                elseif i == 2
                    ylabel('BD');
                elseif i == 3
                    ylabel('BPD');
                end
            end
            p = get(gca, 'Position');
            p_diff = p(4) * 0.2;
            p(4) = p(4) + p_diff;
            if i == 1
                p(2) = p(2) - p_diff;
            elseif i < 3
                p(2) = p(2) - (p_diff / 2);
            end
            p_diff = p(3) * 0.15;
            p(3) = p(3) + p_diff;
            if m == moods
                p(1) = p(1) - p_diff;
            elseif m > 1
                p(1) = p(1) - ((m - 1) * (p_diff / (moods - 1)));
            end
            set(gca, 'Position', p);
        end
    end

    p = get(gcf, 'Position');
    p(3) = (p(3) / 5) * (moods + 1);
    set(gcf, 'Position', p);

    p = get(gcf, 'PaperPosition');
    p(3) = (p(3) / 5) * (moods + 1);
    set(gcf, 'PaperPosition', p);
    
    save_figure('mood_zoom_mean_hist', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

    figure;

    d_hist = 0.5;

    for i = 1:3
        j_range_class = 1:(data_class{i}.j - 1);
        
        for m = 1:moods
            mood = data_moods_list{m};
            mood_str = mood;
            mood_str(1) = upper(mood_str(1));
            
            class_data = data_class{i}.(mood)(j_range_class, :);

            this_data = nanstd(class_data, 0, 2);
            hist_edges = 0:d_hist:(ceil(q_max / 2) * 2);

            subplot(3, moods, ((i - 1) * moods) + m);

            histogram(this_data, hist_edges);

            hold on;
            axis manual;

            fit_distributions(this_data, hist_edges, varargin{:}, 'show_legend', false);

            if i < 3
              set(gca, 'xticklabel', []);
            end
            set(gca, 'ytick', []);
            set(gca, 'xlim', hist_edges([1 end]));
            if i == 1
                title(mood_str);
            elseif i == 3
                xlabel('Std');
            end
            if m == 1
                if i == 1
                    ylabel('HC');
                elseif i == 2
                    ylabel('BD');
                elseif i == 3
                    ylabel('BPD');
                end
            end
            p = get(gca, 'Position');
            p_diff = p(4) * 0.2;
            p(4) = p(4) + p_diff;
            if i == 1
                p(2) = p(2) - p_diff;
            elseif i < 3
                p(2) = p(2) - (p_diff / 2);
            end
            p_diff = p(3) * 0.15;
            p(3) = p(3) + p_diff;
            if m == moods
                p(1) = p(1) - p_diff;
            elseif m > 1
                p(1) = p(1) - ((m - 1) * (p_diff / (moods - 1)));
            end
            set(gca, 'Position', p);
        end
    end

    p = get(gcf, 'Position');
    p(3) = (p(3) / 5) * (moods + 1);
    set(gcf, 'Position', p);

    p = get(gcf, 'PaperPosition');
    p(3) = (p(3) / 5) * (moods + 1);
    set(gcf, 'PaperPosition', p);
    
    save_figure('mood_zoom_std_hist', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});

    figure;

    d_hist = 1;

    for i = 1:3
        j_range_class = 1:(data_class{i}.j - 1);
        
        for m = 1:moods
            mood = data_moods_list{m};
            mood_str = mood;
            mood_str(1) = upper(mood_str(1));
            
            class_data = data_class{i}.(mood)(j_range_class, :);

            this_data = iqr(class_data, 2);
            hist_edges = 0:d_hist:(ceil(q_max / 2) * 2);

            subplot(3, moods, ((i - 1) * moods) + m);

            histogram(this_data, hist_edges);

            hold on;
            axis manual;

            fit_distributions(this_data, hist_edges, varargin{:}, 'show_legend', false);

            if i < 3
              set(gca, 'xticklabel', []);
            end
            set(gca, 'ytick', []);
            set(gca, 'xlim', hist_edges([1 end]));
            if i == 1
                title(mood_str);
            elseif i == 3
                xlabel('IQR');
            end
            if m == 1
                if i == 1
                    ylabel('HC');
                elseif i == 2
                    ylabel('BD');
                elseif i == 3
                    ylabel('BPD');
                end
            end
            p = get(gca, 'Position');
            p_diff = p(4) * 0.2;
            p(4) = p(4) + p_diff;
            if i == 1
                p(2) = p(2) - p_diff;
            elseif i < 3
                p(2) = p(2) - (p_diff / 2);
            end
            p_diff = p(3) * 0.15;
            p(3) = p(3) + p_diff;
            if m == moods
                p(1) = p(1) - p_diff;
            elseif m > 1
                p(1) = p(1) - ((m - 1) * (p_diff / (moods - 1)));
            end
            set(gca, 'Position', p);
        end
    end

    p = get(gcf, 'Position');
    p(3) = (p(3) / 5) * (moods + 1);
    set(gcf, 'Position', p);

    p = get(gcf, 'PaperPosition');
    p(3) = (p(3) / 5) * (moods + 1);
    set(gcf, 'PaperPosition', p);
    
    save_figure('mood_zoom_iqr_hist', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
end
