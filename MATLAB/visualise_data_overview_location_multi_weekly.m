function visualise_data_overview_location_multi_weekly(participant, varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 09-Feb-2015

    close all;
    
    OUTPUT_SUB_DIR = 'location-multi-weekly';
    
    data_type = get_argument({'data_type', 'datatype'}, 'validated', varargin{:});
    t_min = get_argument({'t_min', 'tmin'}, NaN, varargin{:});
    t_max = get_argument({'t_max', 'tmax'}, NaN, varargin{:});
    show_battery = get_argument({'show_battery', 'showbattery'}, false, varargin{:});
    variant = get_argument('variant', '', varargin{:});
    
    if ~isempty(variant)
        variant = ['_' variant];
    end
    
    data = load_location_preprocessed(participant, varargin{:});
    
    if ~isempty(data) && ~isempty(data.time)
        plotargs = {};
        if show_battery
            battery_data = load_battery_data(participant);
            plotargs = {'battery_data', battery_data};
        end
        if isnan(t_min)
            t_min = min(data.time);
        end
        if isnan(t_max)
            t_max = max(data.time);
        end
        
        t_min = floor(t_min);
        t_max = floor(t_max);
        
        period_beginning = (t_min - 1) - weekday(t_min - 1) + 2;
        
        while period_beginning < t_max
            close all;
            
            fprintf(['  Plotting period beginning ' datestr(period_beginning, 'dd-mmm-yyyy') ': ']);
            tic;
            if isempty(data_type)
                plot_location_multi_weekly(data, period_beginning, varargin{:}, plotargs{:})
            else
                plot_location_multi_weekly(data.(data_type), period_beginning, varargin{:}, plotargs{:})
            end
            toc;
            
            fprintf('  Saving: ');
            tic;
            save_figure([participant '-' datestr(period_beginning, 'yyyy-mm-dd') variant], 'output_sub_dir', OUTPUT_SUB_DIR, 'FontSize', [], varargin{:});
            toc;
            
            period_beginning = period_beginning + (8 * 7);
         end
    else
        fprintf('  No data loaded\n');
    end
end