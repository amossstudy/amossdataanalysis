function preprocess_location_resample(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 15-Jul-2015

    variant = get_argument('variant', '', varargin{:});
    combine_variants = get_argument({'combinevariants', 'combine_variants'}, {}, varargin{:});
    
    if isempty(variant) && ~isempty(combine_variants)
        variant = 'combined';

        for i = 1:length(combine_variants)
            this_variant_name = combine_variants{i};

            if isempty(this_variant_name)
                this_variant_name = 'std';
            end

            variant = [variant '_' this_variant_name]; %#ok<AGROW>
        end
    end

    variant_out = '';
    
    if ~isempty(variant)
        variant_out = ['_' variant];
    end

    for file = get_participants(varargin{:})'
        fprintf('Resampling %s\n', file.name)

        data = load_location_preprocessed(file.name, varargin{:}, 'variant', variant);

        if ~isempty(data) && isfield(data, 'validated') && ~isempty(data.validated.time)
            fprintf('  Generating regularised data: ');
            tic;

            data_days = floor(data.validated.time);

            location_data_validated = [data.validated.lat data.validated.lon];

            time_regularised_diff = 10 / (24 * 60); % 10 minutes resolution
            time_regularised_start = data_days(1);
            time_regularised_end = data_days(end) - time_regularised_diff;
            time_regularised_nearest_point = 20 / (24 * 60); % Use nearest point within +/-20 minutes

            time_regularised = time_regularised_start:time_regularised_diff:time_regularised_end;
            location_data_regularised = NaN(length(time_regularised), 2);

            for t = 1:length(time_regularised)
                data_times_idx = data.validated.time >= (time_regularised(t) - (time_regularised_diff / 2)) & ...
                    data.validated.time < (time_regularised(t) + (time_regularised_diff / 2));

                data_locations = location_data_validated(data_times_idx, :);

                if sum(data_times_idx) > 0
                    data_locations_valid = ~isnan(data_locations(:, 1)) & ~isnan(data_locations(:, 2));
                    data_locations = data_locations(data_locations_valid, :);
                end

                if size(data_locations, 1) > 0
                    location_data_regularised(t, :) = mean(data_locations, 1);
                else
                    data_times_idx = data.validated.time >= (time_regularised(t) - time_regularised_nearest_point) & ...
                        data.validated.time < (time_regularised(t) + time_regularised_nearest_point);

                    data_times = data.validated.time(data_times_idx);

                    data_locations = location_data_validated(data_times_idx, :);

                    data_locations_valid = ~isnan(data_locations(:, 1)) & ~isnan(data_locations(:, 2));

                    if sum(data_locations_valid) > 0
                        data_times = abs(time_regularised(t) - data_times(data_locations_valid));
                        data_locations = data_locations(data_locations_valid, :);

                        [~, data_times_sort_idx] = sort(data_times);

                        location_data_regularised(t, :) = data_locations(data_times_sort_idx(1), :);
                    end
                end
            end

            data.regularised.time = time_regularised';
            data.regularised.data = sqrt(sum(location_data_regularised.^2,2));
            data.regularised.lat = location_data_regularised(:, 1);
            data.regularised.lon = location_data_regularised(:, 2);

            data.regularised.location_group = zeros(size(data.regularised.time));

            %for l = 1:length(location_groups)
            %    data.regularised.location_group(ismember(location_data_regularised, locations_unique(l, :), 'rows')) = location_groups(l);
            %end

            toc;
            
            save_data([file.name '-location' variant_out '-preprocessed'], data, varargin{:});
        else
            fprintf('  No data loaded\n');
        end
    end
end