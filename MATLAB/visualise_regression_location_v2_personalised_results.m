function visualise_regression_location_v2_personalised_results(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 07-Apr-2016

    close all;
    
    OUTPUT_SUB_DIR = 'location/v2/regression/personalised/';
    
    params = get_configuration('features_location_v2', varargin{:});
    
    regression_data = load_regression_data_location_v2_personalised(varargin{:});
    
    valid_participants = false(size(regression_data.id));
    
    for participant_idx = 1:numel(regression_data.id)
        fprintf(' Processing participant %i of %i (%s): ', participant_idx, numel(regression_data.id), regression_data.id{participant_idx});
        
        if isnan(regression_data.class(participant_idx))
            fprintf('No data.\n');
            continue;
        end
        
        if sum(~isnan(regression_data.qids.data{participant_idx})) >= 10
            if range(regression_data.qids.data{participant_idx}) >= 10
                fprintf('Ok.\n');
                valid_participants(participant_idx) = true;
            else
                fprintf('Invalid (range < 10).\n');
            end
        else
            fprintf('Invalid (n < 6).\n');
        end
    end
    
    fprintf('Found %i valid participants', sum(valid_participants));

    for c_i = 1:4;
        if c_i == 1
            valid_idx = valid_participants & ~isnan(regression_data.class);
        elseif c_i <= 4
            fprintf('      %i %s participants', sum(regression_data.class(valid_participants) == c_i - 2), params.test_description{c_i});
            valid_idx = valid_participants & ~isnan(regression_data.class) & (regression_data.class == (c_i - 2));
        end
        
        if sum(valid_idx) > 0
            fprintf(', weeks = %.4f%s%.4f', mean(regression_data.qids.count(valid_idx)), char(177), std(regression_data.qids.count(valid_idx)));
            for result_type_cell = fieldnames(regression_data.results)'
                if ~strcmp(result_type_cell{:}, 'pred')
                    figure;

                    histogram(regression_data.results.(result_type_cell{:})(valid_idx), 0:0.5:10)

                    title(sprintf('%s (%s)', upper(result_type_cell{:}), params.test_description{c_i}));

                    save_figure(sprintf('results_%s_%s', result_type_cell{:}, params.test_description_filename{c_i}), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
                    
                    fprintf(', %s = %.4f%s%.4f', upper(result_type_cell{:}), mean(regression_data.results.(result_type_cell{:})(valid_idx)), char(177), std(regression_data.results.(result_type_cell{:})(valid_idx)));
                end
            end
        end
        fprintf('.\n');
    end
end
