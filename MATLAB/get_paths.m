function paths = get_paths(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 26-Jan-2015

    this_path = fileparts(mfilename('fullpath'));
    
    paths = struct();
    
    paths.app_name = get_argument({'appname', 'app_name', 'app'}, '', varargin{:});
    
    paths.script_path = [this_path '/'];
    this_path = get_argument({'rootpath', 'root_path'}, [this_path '/..'], varargin{:});
    while (numel(this_path) > 0) && ((this_path(end) == '/') || (this_path(end) == '\'))
        this_path = this_path(1:end-1);
    end
    
    paths.unprocessed_data_path = get_argument({'unprocessed_path', 'unprocessedpath', 'unprocessed_data_path', 'unprocesseddatapath'}, [paths.script_path '../../AMoSSDataSync/extracted/'], varargin{:});
    if (~isempty(paths.unprocessed_data_path)) && (paths.unprocessed_data_path(end) ~= '/') && (paths.unprocessed_data_path(end) ~= '\')
        paths.unprocessed_data_path = [paths.unprocessed_data_path '/'];
    end
    
    paths.processed_data_path = get_argument({'processed_path', 'processedpath', 'processed_data_path', 'processeddatapath'}, [paths.script_path '../../AMoSSDataSync/processed/'], varargin{:});
    if (~isempty(paths.processed_data_path)) && (paths.processed_data_path(end) ~= '/') && (paths.processed_data_path(end) ~= '\')
        paths.processed_data_path = [paths.processed_data_path '/'];
    end
    
    paths.working_path = get_argument({'working_path', 'workingpath', 'working_dir', 'workingdir'}, [this_path '/working/'], varargin{:});
    
    if ~isempty(paths.app_name)
        paths.processed_data_path = [paths.processed_data_path paths.app_name '/'];
        paths.working_path = [paths.working_path paths.app_name '/'];
    end
    
    paths.output_path = get_argument({'output_path', 'outputpath', 'output_dir', 'outputdir'}, [paths.working_path 'output/'], varargin{:});
    paths.temp_path = '';
    
    paths.prefer_working = get_argument({'preferworking', 'prefer_working'}, true, varargin{:});
    paths.output_sub_dir = get_argument({'outputsubdir', 'output_subdir', 'output_sub_dir'}, '', varargin{:});
    paths.working_sub_dir = get_argument({'workingsubdir', 'working_subdir', 'working_sub_dir'}, '', varargin{:});
    output_temp = get_argument({'output_temp', 'outputtemp'}, false, varargin{:});

    if output_temp == true
        if ~isempty(paths.temp_path)
            paths.output_path = paths.temp_path;
        end
    end

    output_subdir_suffix = get_argument({'outputsubdirsuffix', 'output_subdir_suffix', 'output_sub_dir_suffix', 'outputdirsuffix', 'output_dir_suffix'}, '', varargin{:});
    
    if ~isempty(output_subdir_suffix)
        while (numel(paths.output_sub_dir) > 0) && (paths.output_sub_dir(end) == '/') || (paths.output_sub_dir(end) == '\')
            paths.output_sub_dir = paths.output_sub_dir(1:end-1);
        end
        paths.output_sub_dir = [paths.output_sub_dir output_subdir_suffix];
    end
    
    if (~isempty(paths.output_sub_dir)) && (paths.output_sub_dir(end) ~= '/') && (paths.output_sub_dir(end) ~= '\')
        paths.output_sub_dir = [paths.output_sub_dir '/'];
    end

    paths.output_path = [paths.output_path paths.output_sub_dir];

    if (~isempty(paths.working_sub_dir)) && (paths.working_sub_dir(end) ~= '/') && (paths.working_sub_dir(end) ~= '\')
        paths.working_sub_dir = [paths.working_sub_dir '/'];
    end

    paths.working_path = [paths.working_path paths.working_sub_dir];
end
