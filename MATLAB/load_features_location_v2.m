function [classification_data, classification_properties] = load_features_location_v2(varargin)
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Aug-2015

    feature_data = get_argument({'usefeaturedata', 'use_feature_data'}, [], varargin{:});
    
    if ~isempty(feature_data)
        assert(isstruct(feature_data), 'Feature data must be a struct.');
        assert(isfield(feature_data, 'classification_data') && isfield(feature_data, 'classification_properties'), 'Feature data must have classification_data and classification_properties fields.');
        
        classification_data = feature_data.classification_data;
        classification_properties = feature_data.classification_properties;
        
        return;
    end
    
    questionnaires = fieldnames(get_configuration('questionaires'))';
    
    classification_data = [];
    classification_properties = struct();
    classification_properties.participant = {};
    classification_properties.variant = {};
    classification_properties.week_start = [];
    classification_properties.class = [];
    for this_questionnaire_cell = questionnaires
        classification_properties.(this_questionnaire_cell{:}) = [];
    end
    classification_properties.BIS = [];
    classification_properties.IPDE = [];
    classification_properties.employment_status = [];
    
    data_subsets = {'basic', 'weekday', 'weekend', 'median', 'valid'};
    data_subsets = get_argument({'load_feature_types', 'loadfeaturetypes', 'load_feature_type', 'loadfeaturetype', ...
                                 'load_data_subsets', 'loaddatasubsets', 'load_data_subset', 'loaddatasubset'}, data_subsets, varargin{:});
    allow_nan_inf = get_argument({'allow_nan_inf', 'allownaninf'}, false, varargin{:});
    
    study_period_start = get_argument({'studyperiodstart', 'study_period_start'}, [], varargin{:});
    study_period_end = get_argument({'studyperiodend', 'study_period_end'}, [], varargin{:});
    
    average_data_backwards = get_argument('average_data_backwards', 0, varargin{:});
    average_data_forwards = get_argument('average_data_forwards', 0, varargin{:});
    
    filter_max_lt_250m = get_argument('filter_max_lt_250m', NaN, varargin{:});
    filter_max_gt_50km = get_argument('filter_max_gt_50km', NaN, varargin{:});
    filter_max_missing = get_argument('filter_max_missing', NaN, varargin{:});
    filter_max_lt_250m_missing = get_argument('filter_max_lt_250m_missing', NaN, varargin{:});
    filter_max_gt_50km_missing = get_argument('filter_max_gt_50km_missing', NaN, varargin{:});
    
    match_participant_properties = get_argument('match_participant_properties', struct(), varargin{:});
    
    opts = {};

    if ~isempty(study_period_start)
        opts = [opts, 'left_after', datenum(study_period_start) + 7];
    end

    if ~isempty(study_period_end)
        opts = [opts, 'entered_before', datenum(study_period_end) - 7];
    end
    
    if ~iscell(data_subsets) && ischar(data_subsets)
        data_subsets = {data_subsets};
    end
    
    data_subset_length = NaN(size(data_subsets));

    for file = get_participants(varargin{:}, opts{:})'
        this_participant_properties = get_participant_property(file.name, [], varargin{:});
        
        if isfield(this_participant_properties, 'classification') && ~isempty(this_participant_properties.classification)
            class = this_participant_properties.classification;
            
            for variant_cell = {'', 'gms', 'combined_std_gms'}
                annotations = load_location_annotations(file.name, varargin{:}, 'variant', variant_cell{:});

                if ~isempty(fieldnames(annotations)) && isfield(annotations, 'week_annotations') && (sum(annotations.week_annotations.valid) > 0)
                    features = load_location_features(file.name, varargin{:}, 'variant', variant_cell{:});

                    if ~isempty(fieldnames(features)) && isfield(features, 'v2') && sum(features.v2.is_valid) > 0
                        valid_weeks = find(annotations.week_annotations.valid);
                        
                        if ~isempty(study_period_start)
                            valid_weeks = valid_weeks(annotations.week_annotations.start(valid_weeks) >= datenum(study_period_start));
                        end
                        
                        if ~isempty(study_period_end)
                            valid_weeks = valid_weeks(annotations.week_annotations.start(valid_weeks) <= datenum(study_period_end));
                        end
                        
                        if (numel(valid_weeks) > 0) && ...
                                (~isnan(filter_max_lt_250m) || ~isnan(filter_max_gt_50km) || ...
                                 ~isnan(filter_max_missing) || ~isnan(filter_max_lt_250m_missing) || ...
                                 ~isnan(filter_max_gt_50km_missing))
                            this_data_preprocessed_full = load_location_preprocessed(file.name, varargin{:}, 'variant', variant_cell{:});
                            
                            this_data_preprocessed = this_data_preprocessed_full.normalised.filtered.segmented;
                            
                            this_data_day = floor(this_data_preprocessed.time);
                            this_data_location_time = min([1/(24 * 60); diff(this_data_preprocessed.time)], 1/(24 * 12));
                            this_data_gt_50km = this_data_preprocessed.data > 50;
                            this_data_lt_250m = this_data_preprocessed.data < 0.250;
                            
                            this_data_valid = true(numel(valid_weeks), 1);

                            for w = 1:numel(valid_weeks)
                                wb = annotations.week_annotations.start(valid_weeks(w));

                                this_f = NaN(7, 2);

                                for d = 1:7
                                    wd = wb + d - 1;

                                    d_idx = (this_data_day == wd);

                                    t_day = min(sum(this_data_location_time(d_idx)), 1);

                                    this_f(d, 1) = min(sum(this_data_location_time(d_idx & this_data_gt_50km)), 1);
                                    this_f(d, 2) = min(sum(this_data_location_time(d_idx & this_data_lt_250m)), 1);
                                    this_f(d, 3) = 1 - t_day;
                                end
                                
                                % this_f(:, 1): percentage of time > 50km from home
                                % this_f(:, 2): percentage of time < 250m from home
                                % this_f(:, 3): percentage of data missing
                                this_f_week = sum(this_f, 1);
                                
                                if ~isnan(filter_max_lt_250m) && (this_f_week(2) > filter_max_lt_250m)
                                    this_data_valid(w) = false;
                                end
                                if ~isnan(filter_max_gt_50km) && (this_f_week(1) > filter_max_gt_50km)
                                    this_data_valid(w) = false;
                                end
                                if ~isnan(filter_max_missing) && (this_f_week(3) > filter_max_missing)
                                    this_data_valid(w) = false;
                                end
                                if ~isnan(filter_max_lt_250m_missing) && ((this_f_week(2) + this_f_week(3)) > filter_max_lt_250m_missing)
                                    this_data_valid(w) = false;
                                end
                                if ~isnan(filter_max_gt_50km_missing) && ((this_f_week(1) + this_f_week(3)) > filter_max_gt_50km_missing)
                                    this_data_valid(w) = false;
                                end
                            end
                            
                            valid_weeks = valid_weeks(this_data_valid);
                        end
                        
                        if numel(valid_weeks) == 0
                            continue;
                        end
                        
                        tc_data = load_true_colours_data(file.name, varargin{:});
                        
                        this_participant_classification_data = [];
                        
                        this_participant_name = repmat({file.name}, numel(valid_weeks), 1);
                        this_participant_variant = repmat(variant_cell, numel(valid_weeks), 1);
                        this_participant_week_start = annotations.week_annotations.start(valid_weeks)';
                        this_participant_class = ones(numel(valid_weeks), 1) * class;
                        this_participant_questionnaires = struct();
                        for this_questionnaire_cell = questionnaires
                            this_participant_questionnaires.(this_questionnaire_cell{:}) = NaN(numel(valid_weeks), 1);
                        end
                        this_participant_bis = struct('attentional', NaN, 'motor', NaN, 'nonplanning', NaN, 'total', NaN);
                        this_participant_ipde = struct('dimensional', NaN);
                        this_participant_employment_status = NaN(numel(valid_weeks), 1);
                        
                        for bis_field = {'attentional', 'motor', 'nonplanning', 'total'}
                            if isfield(this_participant_properties, ['bis_' bis_field{:}]) && ~isempty(this_participant_properties.(['bis_' bis_field{:}]))
                                this_participant_bis.(bis_field{:}) = this_participant_properties.(['bis_' bis_field{:}]);
                            end
                        end
                        
                        if isfield(this_participant_properties, 'ipde_dimensional') && ~isempty(this_participant_properties.ipde_dimensional)
                            this_participant_ipde.dimensional = this_participant_properties.ipde_dimensional;
                        end
                        
                        if isfield(this_participant_properties, 'employment_status')
                            if isempty(this_participant_properties.employment_status)
                                this_participant_employment_status(:) = 0;
                            else
                                this_participant_employment_status(:) = this_participant_properties.employment_status;
                            end
                        end
                        
                        this_participant_bis = repmat(this_participant_bis, numel(valid_weeks), 1);
                        this_participant_ipde = repmat(this_participant_ipde, numel(valid_weeks), 1);
                        
                        for w = 1:numel(valid_weeks)
                            this_week_start = annotations.week_annotations.start(valid_weeks(w));

                            this_week_data = [];
                            
                            for ft_i = 1:numel(data_subsets)
                                this_data_temp = features.v2.(data_subsets{ft_i})(:, features.v2.week_start == this_week_start);
                                
                                if isnan(data_subset_length(ft_i))
                                    data_subset_length(ft_i) = size(this_data_temp, 1);
                                elseif size(this_data_temp, 1) ~= data_subset_length(ft_i)
                                    warning('Invalid feature length.');
                                    this_data_temp = NaN(data_subset_length(ft_i), 1);
                                end
                                
                                this_week_data = [this_week_data; this_data_temp]; %#ok<AGROW>
                            end
                            
                            this_participant_classification_data = [this_participant_classification_data this_week_data]; %#ok<AGROW>
                            
                            for this_questionnaire_cell = questionnaires
                                this_questionnaire = this_questionnaire_cell{:};
                                if isfield(tc_data, this_questionnaire)
                                    valid_q = (tc_data.(this_questionnaire).time >= (this_week_start - 14)) & ...
                                              (tc_data.(this_questionnaire).time <= (this_week_start + 7 + 14)) & ...
                                              (tc_data.(this_questionnaire).data > -1);

                                    valid_q_time = tc_data.(this_questionnaire).time(valid_q);
                                    valid_q_data = tc_data.(this_questionnaire).data(valid_q);

                                    this_week_q_value = NaN;
                                    
                                    if (sum(valid_q) == 1) && ...
                                            (valid_q_time <= (this_week_start + 7 + 3.5)) && ...
                                            (valid_q_time >= (this_week_start - 3.5))

                                        this_week_q_value = valid_q_data;
                                    elseif (sum(valid_q) > 1) && ...
                                            (valid_q_time(1) >= (this_week_start + 7)) && ...
                                            (valid_q_time(1) <= (this_week_start + 7 + 3.5))

                                        this_week_q_value = valid_q_data(1);
                                    elseif (sum(valid_q) > 1) && ...
                                            (valid_q_time(end) <= this_week_start) && ...
                                            (valid_q_time(end) >= (this_week_start - 3.5))

                                        this_week_q_value = valid_q_data(end);
                                    elseif (sum(valid_q) > 1) && ...
                                              (valid_q_time(1) <= (this_week_start + 7 + 7)) && ...
                                              (valid_q_time(end) >= (this_week_start - 3.5))

                                        while sum(diff(valid_q_time) < (1 / (24 * 60))) > 0
                                            i_equal = find(diff(valid_q_time) < (1 / (24 * 60)));
                                            if numel(i_equal) > 1
                                                i_last_equal = i_equal(find([diff(i_equal); 2] > 1, 1, 'first')) + 1;
                                            else
                                                i_last_equal = i_equal + 1;
                                            end

                                            valid_q_data(i_equal(1)) = mean(valid_q_data(i_equal(1):i_last_equal));

                                            if i_last_equal == numel(valid_q_time)
                                                r_valid = 1:i_equal(1);
                                            else
                                                r_valid = [1:i_equal(1) (i_last_equal + 1):numel(valid_q_time)];
                                            end

                                            valid_q_data = valid_q_data(r_valid);
                                            valid_q_time = valid_q_time(r_valid);
                                        end

                                        interp_time = (0:0.5:7) + this_week_start;

                                        interp_time = interp_time(interp_time >= valid_q_time(1) & interp_time <= valid_q_time(end));

                                        if valid_q_time(1) > this_week_start
                                            interp_time = [valid_q_time(1), interp_time]; %#ok<AGROW>
                                        end

                                        if valid_q_time(end) < this_week_start + 7
                                            interp_time = [interp_time, valid_q_time(end)]; %#ok<AGROW>
                                        end
                                        
                                        q_val = interp1(valid_q_time, valid_q_data, interp_time, 'linear');

                                        this_week_q_value = mean(q_val);
                                    end
                                    
                                    this_participant_questionnaires.(this_questionnaire)(w) = this_week_q_value;
                                end
                            end
                        end

                        if (average_data_backwards > 0) || (average_data_forwards > 0)
                            this_participant_classification_data_old = this_participant_classification_data;
                            for w = 1:numel(this_participant_week_start)
                                valid_features = ~isnan(this_participant_classification_data(:, w));
                                if sum(valid_features) > 0
                                    this_week_start = this_participant_week_start(w);
                                    this_period_start = this_week_start - (average_data_backwards * 7);
                                    this_period_end = this_week_start + (average_data_forwards * 7);

                                    valid_period_weeks_idx = (this_participant_week_start >= this_period_start) & (this_participant_week_start <= this_period_end);
                                    
                                    if sum(valid_period_weeks_idx) > 1
                                        this_period_data = this_participant_classification_data_old(valid_features, valid_period_weeks_idx);
                                        this_period_data(isinf(this_period_data)) = NaN;
                                        this_period_data(:, find(valid_period_weeks_idx) == w) = this_participant_classification_data_old(valid_features, w);
                                        this_participant_classification_data(valid_features, w) = nanmean(this_period_data, 2); %#ok<AGROW>
                                    end
                                end
                            end
                        end

                        classification_data = [classification_data this_participant_classification_data]; %#ok<AGROW>

                        classification_properties.participant = [classification_properties.participant; this_participant_name];
                        classification_properties.variant = [classification_properties.variant; this_participant_variant];
                        classification_properties.week_start = [classification_properties.week_start; this_participant_week_start];
                        classification_properties.class = [classification_properties.class; this_participant_class];
                        for this_questionnaire_cell = questionnaires
                            this_q = this_questionnaire_cell{:};
                            classification_properties.(this_q) = [classification_properties.(this_q); this_participant_questionnaires.(this_q)];
                        end
                        classification_properties.BIS = [classification_properties.BIS; this_participant_bis];
                        classification_properties.IPDE = [classification_properties.IPDE; this_participant_ipde];
                        classification_properties.employment_status = [classification_properties.employment_status; this_participant_employment_status];
                    end
                end
            end
        end
    end
    
    recalculate_features = get_argument({'recalculate_features', 'recalculatefeatures'}, false, varargin{:});
    
    if recalculate_features
        fprintf('  Recaculating features:\n');
        t_start = tic;

        for participant_cell = unique(classification_properties.participant)'
            participant_idx = strcmp(classification_properties.participant, participant_cell{:});
            fprintf('    %s: ', participant_cell{:});
            tic;
            for variant_cell = unique(classification_properties.variant(participant_idx))'
                variant_idx = strcmp(classification_properties.variant, variant_cell{:});

                data = load_location_preprocessed(participant_cell{:}, varargin{:}, 'variant', variant_cell{:});

                for week_idx = find(participant_idx & variant_idx)'
                    new_features = calculate_features_location_v2(data.normalised.filtered.segmented, classification_properties.week_start(week_idx), varargin{:});

                    for ft_i = 1:numel(data_subsets)
                        if new_features.([data_subsets{ft_i} '_calculated'])
                            this_data_temp = new_features.(data_subsets{ft_i});

                            if isnan(data_subset_length(ft_i))
                                warning('Recalculated feature type not previously loaded.');
                                this_data_temp = [];
                            elseif size(this_data_temp, 1) ~= data_subset_length(ft_i)
                                warning('Invalid recalculated feature length.');
                                this_data_temp = NaN(data_subset_length(ft_i), 1);
                            end

                            i_start = 1 + sum(data_subset_length(1:(ft_i - 1)));
                            i_end = i_start + data_subset_length(ft_i) - 1;
                            classification_data(i_start:i_end, week_idx) = this_data_temp; %#ok<AGROW>
                        end
                    end
                end
            end
            toc;
        end

        fprintf('  Recalculated features: ');
        toc(t_start);
    end
    
    % Custom hack to remove NaNs in NENT field caused by NC == 1. This
    % should only happen when ENT == 0. In this case, NENT should be 0.
    for j = 1:numel(data_subsets)
        nent_nan = isnan(classification_data(4 + nansum(data_subset_length(1:(j - 1))), :));
        if sum(nent_nan) > 0
            for i = find(nent_nan)
                if (classification_data(2 + nansum(data_subset_length(1:(j - 1))), i) == 1) && (classification_data(3 + nansum(data_subset_length(1:(j - 1))), i) == 0)
                    classification_data(4 + nansum(data_subset_length(1:(j - 1))), i) = 0; %#ok<AGROW>
                end
            end
        end
    end
    
    has_nan_inf = sum(isnan(classification_data)) > 0 | sum(isinf(classification_data)) > 0;
    
    if ~allow_nan_inf && (sum(has_nan_inf) > 0)
        fprintf('  Removing %i records with NaN or Inf values: ', sum(has_nan_inf));
        tic;

        classification_data = classification_data(:, ~has_nan_inf);

        for field = fieldnames(classification_properties)'
            classification_properties.(field{:}) = classification_properties.(field{:})(~has_nan_inf, :);
        end
        toc;
    elseif allow_nan_inf && (sum(has_nan_inf) > 0)
        fprintf('  Allowed %i records with NaN or Inf values\n', sum(has_nan_inf));
    end
    
    if isstruct(match_participant_properties) && numel(fieldnames(match_participant_properties)) > 0
        if isfield(match_participant_properties, 'participant') && isfield(match_participant_properties, 'week_start')
            isvalid = false(size(classification_properties.participant));
            for p = unique(match_participant_properties.participant)'
                p_idx = strcmp(classification_properties.participant, p);
                p_idx_match = strcmp(match_participant_properties.participant, p);
                match_idx = ismember(classification_properties.week_start(p_idx), unique(match_participant_properties.week_start(p_idx_match)));
                isvalid(p_idx) = match_idx;
            end
            if sum(~isvalid) > 0
                for f = fieldnames(classification_properties)'
                    classification_properties.(f{:}) = classification_properties.(f{:})(isvalid, :);
                end
                classification_data = classification_data(:, isvalid);
            end
        end
    end
    
    if get_argument({'kalman_filter', 'kalmanfilter', 'kalman_smooth', 'kalmansmooth'}, false, varargin{:})
        kalman_params = get_kalman_filter_params_location_v2(varargin{:});
        
        classification_data_kalman_filtered = NaN(size(classification_data));

        for p_cell = unique(classification_properties.participant)'
            p_idx = strcmp(classification_properties.participant, p_cell);
            [p_wb_sorted, p_wb_sort_idx] = sort(classification_properties.week_start(p_idx));
            [~, p_wb_sort_idx_rev] = sort(p_wb_sort_idx);

            for i = 1:size(classification_data, 1)
                this_data_measured = classification_data(i, p_idx)';
                this_data_measured = this_data_measured(p_wb_sort_idx);

                assert(sum(isnan(this_data_measured)) == 0);

                this_data_filtered = kalman_filter_location_v2(p_wb_sorted, this_data_measured, kalman_params.R(i), kalman_params.Q(i, :), true(size(p_wb_sorted)));

                classification_data_kalman_filtered(i, p_idx) = this_data_filtered(p_wb_sort_idx_rev)';
            end
        end
        
        classification_data = classification_data_kalman_filtered;
    end
end
