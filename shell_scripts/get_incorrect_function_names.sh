#!/bin/bash

cd ../MATLAB && grep -a '^[ ]*function' *.m | \
  sed s/function// | \
  sed 's/:[ ]*/:/' | \
  sed 's/:[^=]*= */:/' | \
  sed 's/([^)]*)//' | \
  sed 's/\.m:/:/' | \
  awk -F : '{
    if (!($1 in files) && ($1 != $2)) {
      print $1 ":" $2;
    }
    files[$1] = true;
  }'
