#!/bin/bash

grep 'INFO,Phone:' $(find ../../AMoSSDataSync/processed/*/events*.csv) | awk 'BEGIN {
  FS = ","
}
{
  split($1,a,"/")
  participant = a[5]
  phone_details = substr($3, 8)
  gsub(/ \(AMoSS[^\)]*\)/, "", phone_details)
  if (last_phone[participant] != phone_details) {
    sub(/[^:]+:/, "", $1)
    last_phone[participant] = phone_details

    phone = ""
    version = phone_details

    while (index(version, " ") > 0) {
      if (length(phone) > 0) {
        phone = phone " "
      }
      phone = phone substr(version, 1, index(version, " ") - 1)
      version = substr(version, index(version, " ") + 1)
    }

    print participant "," $1 "," phone "," version
  }
}'
